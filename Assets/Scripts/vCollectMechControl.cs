﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vCollectMechControl : vCollectMeleeControl
{
    public override void HandleCollectableInput(vCollectableStandalone collectableStandAlone)
    {
        if (!meleeManager) return;
        if (collectableStandAlone != null && collectableStandAlone.weapon != null)
        {
            var weapon = collectableStandAlone.weapon.GetComponent<vMeleeWeapon>();
            if (weapon)
            {
                Transform p;
                switch (collectableStandAlone.GetComponentInParent<MechPart>().slot)
                {
                    case MechSetup.LIMBSLOT.RIGHT:
                        p = GetEquipPoint(rightHandler, collectableStandAlone.targetEquipPoint);
                        if (!p) return;
                        if (rightWeapon && rightWeapon != weapon.gameObject)
                            RemoveRightWeapon();

                        //meleeManager.SetRightWeapon(weapon);
                        UpdateRightDisplay(collectableStandAlone);
                        collectableStandAlone.OnEquip.Invoke();
                        gameObject.GetComponent<MechSetup>().SetPartColor(MechSetup.LIMBSLOT.RIGHT, collectableStandAlone.GetComponentInParent<MechPart>().color);
                        break;

                    case MechSetup.LIMBSLOT.LEFT:
                        p = GetEquipPoint(leftHandler, collectableStandAlone.targetEquipPoint);
                        if (!p) return;
                        if (leftWeapon && leftWeapon != weapon.gameObject)
                            RemoveLeftWeapon();

                        //meleeManager.SetLeftWeapon(weapon);
                        UpdateLeftDisplay(collectableStandAlone);
                        collectableStandAlone.OnEquip.Invoke();
                        gameObject.GetComponent<MechSetup>().SetPartColor(MechSetup.LIMBSLOT.LEFT, collectableStandAlone.GetComponentInParent<MechPart>().color);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                var armor = collectableStandAlone.armor.GetComponent<vMeleeArmor>();
                if (armor)
                {
                    switch (collectableStandAlone.GetComponentInParent<MechPart>().slot)
                    {
                        case MechSetup.LIMBSLOT.HEAD:
                            break;
                        case MechSetup.LIMBSLOT.BACK:
                            break;
                        case MechSetup.LIMBSLOT.TORSO:
                            break;
                        case MechSetup.LIMBSLOT.LEGS:
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        Destroy(collectableStandAlone.transform.parent.gameObject);
    }
}
