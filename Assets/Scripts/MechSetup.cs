﻿using Invector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MechSetup : MonoBehaviour {
    // Use this for initialization 
    public Color color;
    public bool randomizeColor;
    public PartsRegistry partsRegistry;

    public int rightArmIndex = 0;
    public int leftArmIndex = 0;
    public int headIndex = 0;
    public int backIndex = 0;
    public int torsoIndex = 0;
    public int legsIndex = 0;
    public GameObject boneRoot;
    public GameObject defaultModel;
    GameObject rightArm = null;
    GameObject leftArm = null;
    GameObject head = null;
    GameObject back = null;
    GameObject torso = null;
    GameObject legs = null;
    public enum LIMBSLOT : int
    {
        RIGHT = 0,
        LEFT = 1,
        HEAD = 2,
        BACK = 3,
        TORSO = 4,
        LEGS = 5
    }

    void Start()
    {
        Startup();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDebugKey();
    }

    internal void SetPartColor(LIMBSLOT sl, Color col)
    {
        switch (sl)
        {
            case LIMBSLOT.RIGHT:
                rightArm.GetComponent<MechPart>().color = col;
                break;
            case LIMBSLOT.LEFT:
                leftArm.GetComponent<MechPart>().color = col;
                break;
            case LIMBSLOT.HEAD:
                head.GetComponent<MechPart>().color = col;
                break;
            case LIMBSLOT.BACK:
                back.GetComponent<MechPart>().color = col;
                break;
            case LIMBSLOT.TORSO:
                torso.GetComponent<MechPart>().color = col;
                break;
            case LIMBSLOT.LEGS:
                legs.GetComponent<MechPart>().color = col;
                break;
            default:
                break;
        }
    }

    protected void UpdateDebugKey()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            leftArmIndex++;
            if (leftArmIndex >= partsRegistry.leftArmList.Length)
            {
                leftArmIndex = 0;
            }
            if (partsRegistry.leftArmList[leftArmIndex] != null)
            {
                Color oldCol = leftArm.GetComponent<MechPart>().color;
                AddLimb(partsRegistry.leftArmList[leftArmIndex]);
                SetPartColor(LIMBSLOT.LEFT, oldCol);
            }
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            DropLimb((int)LIMBSLOT.LEFT);
            DropLimb((int)LIMBSLOT.RIGHT);
            //DropLimb((int)LIMBSLOT.TORSO);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            rightArmIndex++;
            if (rightArmIndex >= partsRegistry.rightArmList.Length)
            {
                rightArmIndex = 0;
            }
            if (partsRegistry.rightArmList[rightArmIndex] != null)
            {
                Color oldCol = rightArm.GetComponent<MechPart>().color;
                AddLimb(partsRegistry.rightArmList[rightArmIndex]);
                SetPartColor(LIMBSLOT.RIGHT, oldCol);
            }
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            torsoIndex++;
            if (torsoIndex >= partsRegistry.torsoList.Length)
            {
                torsoIndex = 0;
            }
            if (partsRegistry.torsoList[torsoIndex] != null)
            {
                Color oldCol = torso.GetComponent<MechPart>().color;
                AddLimb(partsRegistry.torsoList[torsoIndex]);
                SetPartColor(LIMBSLOT.TORSO, oldCol);
            }
            backIndex++;
            if (backIndex >= partsRegistry.backList.Length)
            {
                backIndex = 0;
            }
            if (partsRegistry.backList[backIndex] != null)
            {
                Color oldCol = back.GetComponent<MechPart>().color;
                AddLimb(partsRegistry.backList[backIndex]);
                SetPartColor(LIMBSLOT.BACK, oldCol);
            }
            legsIndex++;
            if (legsIndex >= partsRegistry.legsList.Length)
            {
                legsIndex = 0;
            }
            if (partsRegistry.legsList[legsIndex] != null)
            {
                Color oldCol = legs.GetComponent<MechPart>().color;
                AddLimb(partsRegistry.legsList[legsIndex]);
                SetPartColor(LIMBSLOT.LEGS, oldCol);
            }
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            headIndex++;
            if (headIndex >= partsRegistry.headList.Length)
            {
                headIndex = 0;
            }
            if (partsRegistry.headList[headIndex] != null)
            {
                Color oldCol = head.GetComponent<MechPart>().color;
                AddLimb(partsRegistry.headList[headIndex]);
                SetPartColor(LIMBSLOT.HEAD, oldCol);
            }
        }
    }

    protected void Startup()
    {
        if (partsRegistry.rightArmList[rightArmIndex] != null)
        {
            AddLimb(partsRegistry.rightArmList[rightArmIndex]);
        }
        if (partsRegistry.leftArmList[leftArmIndex] != null)
        {
            AddLimb(partsRegistry.leftArmList[leftArmIndex]);
        }
        if (partsRegistry.headList[headIndex] != null)
        {
            AddLimb(partsRegistry.headList[headIndex]);
        }
        if (partsRegistry.backList[backIndex] != null)
        {
            AddLimb(partsRegistry.backList[backIndex]);
        }
        if (partsRegistry.torsoList[torsoIndex] != null)
        {
            AddLimb(partsRegistry.torsoList[torsoIndex]);
        }
        if (partsRegistry.legsList[legsIndex] != null)
        {
            AddLimb(partsRegistry.legsList[legsIndex]);
        }
        defaultModel.gameObject.SetActive(false);
        color = randomizeColor ? Random.ColorHSV() : color;
        SetColors();
    }

    private void SetColors()
    {
        foreach (var mechPart in gameObject.GetComponentsInChildren<MechPart>())
        {
            mechPart.color = color;
        }
    }

    public void AddLimb(GameObject BonedObj)
    {
        AddLimb(BonedObj, this);
    }
    public void AddLimb(GameObject BonedObj, MechSetup stitcher)
    {
        var newObj = Instantiate(BonedObj, stitcher.transform, false);// new GameObject(BonedObj.name);//
        //newObj.transform.parent = transform;
        //newObj.AddComponent<MechPart>();
        
        var part = newObj.GetComponent<MechPart>();//.Copy(BonedObj.GetComponent<MechPart>());
        if (part != null)
        {
            switch (part.slot)
            {
                case LIMBSLOT.RIGHT:
                    Destroy(stitcher.rightArm);
                    stitcher.rightArm = newObj;
                    GetComponent<vMeleeManager>().SetRightWeapon(newObj);
                    if(GetComponent<vCollectMechControl>()) GetComponent<vCollectMechControl>().rightWeapon = newObj;
                    break;
                case LIMBSLOT.LEFT:
                    Destroy(stitcher.leftArm);
                    stitcher.leftArm = newObj;
                    GetComponent<vMeleeManager>().SetLeftWeapon(newObj);
                    if (GetComponent<vCollectMechControl>()) GetComponent<vCollectMechControl>().leftWeapon = newObj;
                    break;
                case LIMBSLOT.HEAD:
                    Destroy(stitcher.head);
                    stitcher.head = newObj;
                    break;
                case LIMBSLOT.BACK:
                    Destroy(stitcher.back);
                    stitcher.back = newObj;
                    break;
                case LIMBSLOT.TORSO:
                    Destroy(stitcher.torso);
                    stitcher.torso = newObj;
                    break;
                case LIMBSLOT.LEGS:
                    Destroy(stitcher.legs);
                    stitcher.legs = newObj;
                    break;
            }

        }
    }
    /*public void AddLimb(GameObject BonedObj)
    {
        var newObj = (BonedObj);
        var BonedObjects = newObj.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (SkinnedMeshRenderer SkinnedRenderer in BonedObjects)
        {
            if (SkinnedRenderer != null)
            {
                var newRender = ProcessBonedObject(SkinnedRenderer, boneRoot);
                //Destroy(BonedObjects.gameObject);
                newRender.transform.parent = boneRoot.transform.parent ;

            }
        }
        var part = newObj.GetComponent<MechPart>();
        if (part != null)
        {
            switch (part.slot)
            {
                default:
                case LIMBSLOT.RIGHT:

                    //if (rightArm!= null)
                    //{
                    //    DropLimb(0);
                    //}
                    Destroy(rightArm);
                    rightArm = newObj;
                    break;
                case LIMBSLOT.LEFT:
                    Destroy(leftArm);
                    leftArm = newObj;
                    break;
                case LIMBSLOT.HEAD:
                    head = newObj;
                    break;
                case LIMBSLOT.BACK:
                    back = newObj;
                    break;
                case LIMBSLOT.TORSO:
                    torso = newObj;
                    break;
                case LIMBSLOT.LEGS:
                    legs = newObj;
                    break;
            }

        }
    }*/
    // for use with invector equip
    public void ReplaceLimb(MechPart part)
    {
        var player = vGameController.instance.currentPlayer.GetComponent<MechSetup>();
        switch (part.slot)
        {
            case LIMBSLOT.RIGHT:
                player.rightArmIndex = part.index;
                AddLimb(partsRegistry.rightArmList[part.index], player);
                break;
            case LIMBSLOT.LEFT:
                player.leftArmIndex = part.index;
                AddLimb(partsRegistry.leftArmList[part.index], player);
                break;
            case LIMBSLOT.HEAD:
                Destroy(player.head);
                break;
            case LIMBSLOT.BACK:
                Destroy(player.back);
                break;
            case LIMBSLOT.TORSO:
                Destroy(player.torso);
                break;
            case LIMBSLOT.LEGS:
                Destroy(player.legs);
                break;
            default:
                break;
        }
    }
    public void DropLimbRand()
    {
        DropLimb(Mathf.Min(Mathf.FloorToInt(Random.value * 6), 5));
    }
    public void DropLimbAll()
    {
        foreach (var item in System.Enum.GetValues(typeof(LIMBSLOT)).Cast<int>())
        {
            DropLimb(item);
        }
        
    }
    public void DropLimb(int limbslot)
    {
        //var player = getPlayerInstance().GetComponent<MechStitcher>();
        GameObject limb;
        switch ((LIMBSLOT)limbslot)
        {
            default:
            case LIMBSLOT.RIGHT:
                if (rightArmIndex != 0)
                {
                    limb = Instantiate(partsRegistry.rightArmList[rightArmIndex]) as GameObject;
                    limb.transform.position = transform.position;// + new Vector3(0, 2, 0);
                    limb.GetComponent<MechPart>().color = color;
                    //limb.transform.rotation = Random.rotation;
                    ////limb.AddComponent(typeof(Rigidbody));
                    //limb.GetComponentInChildren<SkinnedMeshRenderer>().gameObject.AddComponent(typeof(BoxCollider));
                    Destroy(rightArm);
                    rightArm = null;
                    rightArmIndex = 0;
                    AddLimb(partsRegistry.rightArmList[rightArmIndex]);
                }
                break;
            case LIMBSLOT.LEFT:
                if (leftArmIndex != 0)
                {
                    limb = Instantiate(partsRegistry.leftArmList[leftArmIndex]) as GameObject;
                    limb.transform.position = transform.position;// + new Vector3(0, 2, 0);
                    limb.GetComponent<MechPart>().color = color;
                    Destroy(leftArm);
                    leftArm = null;
                    leftArmIndex = 0;
                    AddLimb(partsRegistry.leftArmList[leftArmIndex]);
                }
                break;
            case LIMBSLOT.HEAD:
                if (headIndex != 0)
                {
                    limb = Instantiate(partsRegistry.headList[headIndex]) as GameObject;
                    limb.transform.position = transform.position;// + new Vector3(0, 2, 0);
                    limb.GetComponent<MechPart>().color = color;
                    Destroy(head);
                    head = null;
                    headIndex = 0;
                    AddLimb(partsRegistry.headList[headIndex]);
                }
                break;
            case LIMBSLOT.BACK:
                if (backIndex != 0)
                {
                    limb = Instantiate(partsRegistry.backList[backIndex]) as GameObject;
                    limb.transform.position = transform.position;// + new Vector3(0, 2, 0);
                    limb.GetComponent<MechPart>().color = color;
                    Destroy(back);
                    back = null;
                    backIndex = 0;
                    AddLimb(partsRegistry.backList[backIndex]);
                }
                break;
            case LIMBSLOT.TORSO:
                if (torsoIndex != 0)
                {
                    limb = Instantiate(partsRegistry.torsoList[torsoIndex]) as GameObject;
                    limb.transform.position = transform.position;// + new Vector3(0, 2, 0);
                    limb.GetComponent<MechPart>().color = color;
                    Destroy(torso);
                    torso = null;
                    torsoIndex = 0;
                    AddLimb(partsRegistry.torsoList[torsoIndex]);
                }
                break;
            case LIMBSLOT.LEGS:
                if (legsIndex != 0)
                {
                    limb = Instantiate(partsRegistry.legsList[legsIndex]) as GameObject;
                    limb.transform.position = transform.position;// + new Vector3(0, 2, 0);
                    limb.GetComponent<MechPart>().color = color;
                    Destroy(legs);
                    legs = null;
                    legsIndex = 0;
                    AddLimb(partsRegistry.legsList[legsIndex]);
                }
                break;
        }
        //Debug.Log("Dropped " + limb.name);
    }

    private GameObject ProcessBonedObject(SkinnedMeshRenderer ThisRenderer, GameObject RootObj)
    {
        Debug.Log(ThisRenderer.gameObject.name);
        /*      Create the SubObject        */
        var NewObj = new GameObject(ThisRenderer.gameObject.name);
        /*      Add the renderer        */
        NewObj.AddComponent<SkinnedMeshRenderer>();
        var NewRenderer = NewObj.GetComponent<SkinnedMeshRenderer>();
        /*      Assemble Bone Structure     */
        var MyBones = new Transform[ThisRenderer.bones.Length];
        for (var i = 0; i < ThisRenderer.bones.Length; i++)
        {
            MyBones[i] = FindChildByName(ThisRenderer.bones[i].name, RootObj.transform);
            /*      Assemble Renderer       */
            //Debug.Log(MyBones[i].name);
        }
        NewRenderer.bones = MyBones;
        NewRenderer.sharedMesh = ThisRenderer.sharedMesh;
        NewRenderer.sharedMaterials = ThisRenderer.sharedMaterials;
        return NewObj;
    }
    //   private GameObject ProcessBonedObject(GameObject ThisRenderer, GameObject RootObj)
    //   {
    //       /*      Create the SubObject        */
    //       var NewObj = Instantiate(ThisRenderer) as GameObject;
    //       NewObj.transform.parent = RootObj.transform.parent;
    //       /*      Add the renderer        */
    //       //NewObj.AddComponent<SkinnedMeshRenderer>();
    //       var NewRenderer = ThisRenderer.GetComponentInChildren<SkinnedMeshRenderer>();
    //       /*      Assemble Bone Structure     */
    //       var MyBones = new Transform[NewRenderer.bones.Length];
    //       for (var i = 0; i < NewRenderer.bones.Length; i++)
    //           MyBones[i] = FindChildByName(NewRenderer.bones[i].name, RootObj.transform);
    //       /*      Assemble Renderer       */
    //       Debug.Log(RootObj.name);
    //       NewRenderer.bones = MyBones;
    //       return NewObj;
    //   }


    //private void ProcessBonedObject(SkinnedMeshRenderer ThisRenderer, GameObject RootObj)
    //{
    //    /*      Create the SubObject        */
    //    var NewObj = new GameObject(ThisRenderer.gameObject.name);
    //    NewObj.transform.parent = RootObj.transform.parent;
    //    /*      Add the renderer        */
    //    NewObj.AddComponent<SkinnedMeshRenderer>();
    //    var NewRenderer = NewObj.GetComponent<SkinnedMeshRenderer>();
    //    /*      Assemble Bone Structure     */
    //    var MyBones = new Transform[ThisRenderer.bones.Length];
    //    for (var i = 0; i < ThisRenderer.bones.Length; i++)
    //        MyBones[i] = FindChildByName(ThisRenderer.bones[i].name, RootObj.transform);
    //    /*      Assemble Renderer       */
    //    Debug.Log(RootObj.name);
    //    NewRenderer.bones = MyBones;
    //    NewRenderer.sharedMesh = ThisRenderer.sharedMesh;
    //    NewRenderer.sharedMaterials = ThisRenderer.sharedMaterials;
    //    //return NewObj;
    //}



    private Transform FindChildByName(string ThisName, Transform ThisGObj)
    {
        Transform ReturnObj;
        if (ThisGObj.name == ThisName)
            return ThisGObj.transform;
        foreach (Transform child in ThisGObj)
        {
            ReturnObj = FindChildByName(ThisName, child);
            if (ReturnObj)
                return ReturnObj;
        }
        return null;
    }

    public GameObject getPlayerInstance()
    {
        var player = vGameController.instance.currentPlayer;

        return gameObject == player ? gameObject : player;
    }
}
