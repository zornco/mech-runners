﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartsRegistry : MonoBehaviour {
    public GameObject[] leftArmList;
    public GameObject[] rightArmList;
    public GameObject[] headList;
    public GameObject[] backList;
    public GameObject[] torsoList;
    public GameObject[] legsList;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public int GetPartIndex(GameObject getPart, GameObject[] partList)
    {
        for (int i = 0; i < partList.Length; i++)
        {
            if (getPart.name.Contains(partList[i].name))
            {
                Debug.Log(i);
                return i;
            }
        }
        return 0;
    }
}
