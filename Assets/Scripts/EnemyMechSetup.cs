﻿using Invector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMechSetup : MechSetup
{
	// Use this for initialization
	void Start ()
    {
        rightArmIndex = Random.Range(1, partsRegistry.rightArmList.Length);
        leftArmIndex = Random.Range(1, partsRegistry.leftArmList.Length);
        torsoIndex = headIndex = Random.Range(1, partsRegistry.headList.Length);
        legsIndex = Random.Range(1, partsRegistry.legsList.Length);
        backIndex = Random.Range(1, partsRegistry.backList.Length);
        //Debug.Log("Enemy Spawned with R:" + rightArmIndex + " L:" + leftArmIndex);
        Startup();
    }
    void Update()
    {

    }
    public void onDead()
    {
        vGameController.instance.GetComponent<EnemySpawner>().SpawnEnemy();
    }
}
