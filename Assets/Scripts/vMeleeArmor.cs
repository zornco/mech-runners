﻿using Invector.ItemManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vMeleeArmor : MonoBehaviour, vIEquipment
{

    [Header("Defense Settings")]
    [Range(0, 100)]
    public int defenseRate = 100;
    [Range(0, 180)]
    public float defenseRange = 90;

    public void OnEquip(vItem item)
    {
    }

    public void OnUnequip(vItem item)
    {
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
