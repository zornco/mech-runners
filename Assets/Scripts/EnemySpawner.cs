﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public GameObject enemyPrefab;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SpawnEnemy();
        }

    }

    public void SpawnEnemy()
    {
        GameObject enemy = Instantiate(enemyPrefab) as GameObject;
        Vector3 pos = new Vector3((UnityEngine.Random.value - 0.5f) * 10, 0, (UnityEngine.Random.value - 0.5f) * 10);
        enemy.transform.position = pos;
        enemy.transform.Rotate(Vector3.up, Random.Range(0, 359.99f));
        enemy.SetActive(true);
        Debug.Log(pos);
    }
}
