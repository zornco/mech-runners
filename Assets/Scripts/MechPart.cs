﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechPart : MonoBehaviour {
    public MechSetup.LIMBSLOT slot;
    public Color color;
    public int index;
    public SkinnedMeshRenderer mesh;
    public GameObject boneRootPrefab;
    protected GameObject boneRoot;
    public GameObject worldModel;

    public MechPart Copy(MechPart old)
    {
        slot = old.slot;
        mesh = old.mesh;
        boneRootPrefab = old.boneRoot;
        boneRoot = old.boneRoot;
        color = old.color;
        return this;
    }
    public void Equip()
    {
        worldModel.SetActive(false);
        var rigid = GetComponent<Rigidbody>();
        if (rigid != null)
        {
            Destroy(rigid);
            Destroy(GetComponent<MeshCollider>());
        }
        var roots = GetComponentInChildren<BoneRoot>();
        if (roots != null)
        {
            Destroy(roots.gameObject);
            Destroy(GetComponentInChildren<SkinnedMeshRenderer>());
            var bone = transform.parent.GetComponentInChildren<BoneRoot>();
            ProcessBonedObject(mesh, bone.gameObject);
        }
        boneRoot = Instantiate(boneRootPrefab, transform, false);
        boneRoot.AddComponent<BoneRoot>();
        var BonedObjects = mesh.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        ProcessBonedObject(mesh, boneRoot);
    }
    // Use this for initialization
    void Start () {
        if (mesh != null)
        {
            if (transform.parent == null)
            {
                //boneRoot = Instantiate(boneRootPrefab, transform, false);
                //boneRoot.AddComponent<BoneRoot>();
                //var BonedObjects = mesh.gameObject.GetComponentInChildren<MeshRenderer>();
                //var newRender = ProcessBonedObject(mesh, boneRoot);
                var meshColl = gameObject.AddComponent<MeshCollider>();
                meshColl.sharedMesh = mesh.sharedMesh;
                meshColl.convex = true;
                var rigbod = gameObject.AddComponent<Rigidbody>();
                //rigbod.isKinematic = true;
            }
            else
            {
                if(worldModel != null) worldModel.SetActive(false);
                var collecter = GetComponentInChildren<vCollectableStandalone>();
                if (collecter)
                {
                    collecter.gameObject.SetActive(false);
                }
                var bone = transform.parent.GetComponentInChildren<BoneRoot>();
                ProcessBonedObject(mesh, bone.gameObject);
                var collectable = GetComponentInChildren<vCollectableStandalone>();
                if (collectable)
                {
                    collectable.gameObject.SetActive(false);
                }
                var melee = GetComponent<vMeleeWeapon>();
                if (melee)
                {
                    var contr = GetComponentInParent<vCollectMechControl>();
                    if (contr)
                        switch (slot)
                        {
                            //Set Weapon
                            case MechSetup.LIMBSLOT.RIGHT:

                                GetComponentInParent<vMeleeManager>().SetRightWeapon(gameObject);
                                gameObject.GetComponent<vMeleeWeapon>().hitBoxes = GetComponentInParent<vMeleeManager>().Members.Find(member => member.bodyPart == "RightHand").attackObject.hitBoxes;
                                contr.rightWeapon = gameObject;
                                break;
                            case MechSetup.LIMBSLOT.LEFT:
                                GetComponentInParent<vMeleeManager>().SetLeftWeapon(gameObject);
                                gameObject.GetComponent<vMeleeWeapon>().hitBoxes = GetComponentInParent<vMeleeManager>().Members.Find(member => member.bodyPart == "LeftHand").attackObject.hitBoxes;
                                contr.leftWeapon = gameObject;
                                break;
                                //TO BE USED LATER
                            case MechSetup.LIMBSLOT.HEAD:
                                break;
                            case MechSetup.LIMBSLOT.BACK:
                                break;
                            case MechSetup.LIMBSLOT.TORSO:
                                break;
                            case MechSetup.LIMBSLOT.LEGS:
                                break;
                            default:
                                break;
                        }

                }
            }
        }
        //Destroy(BonedObjects.gameObject);
        //newRender.transform.parent = boneRoot.transform.parent;
    }
	
	// Update is called once per frame
	void Update () {
        var renderers = GetComponentsInChildren<Renderer>();
        var propBlock = new MaterialPropertyBlock();
        foreach (var ren in renderers)
        {
            if(ren.gameObject.activeSelf)
            {
                ren.GetPropertyBlock(propBlock);
                propBlock.SetColor("_Colorable",color);
                ren.SetPropertyBlock(propBlock);
            }
        }
	}
    public void SetColor(Color col)
    {
        color = col;
    }
    private GameObject ProcessBonedObject(SkinnedMeshRenderer ThisRenderer, GameObject RootObj)
    {
        //Debug.Log(RootObj.name);
        /*      Create the SubObject        */
        var NewObj = new GameObject(ThisRenderer.gameObject.name);
        NewObj.transform.parent = transform;
        /*      Add the renderer        */
        NewObj.AddComponent<SkinnedMeshRenderer>();
        var NewRenderer = NewObj.GetComponent<SkinnedMeshRenderer>();
        /*      Assemble Bone Structure     */
        var MyBones = new Transform[ThisRenderer.bones.Length];
        for (var i = 0; i < ThisRenderer.bones.Length; i++)
        {
            MyBones[i] = FindChildByName(ThisRenderer.bones[i].name, RootObj.transform);
            //if(slot == MechSetup.LIMBSLOT.LEGS)Debug.Log(MyBones[i]);
        }
        /*      Assemble Renderer       */
        
        NewRenderer.bones = MyBones;
        NewRenderer.sharedMesh = ThisRenderer.sharedMesh;
        NewRenderer.sharedMaterials = ThisRenderer.sharedMaterials;
        //NewRenderer.materials[0] = Instantiate<Material>(ThisRenderer.material);
        return NewObj;
    }
    private Transform FindChildByName(string ThisName, Transform ThisGObj)
    {
        Transform ReturnObj;
        if (ThisGObj.name == ThisName)
            return ThisGObj.transform;
        foreach (Transform child in ThisGObj)
        {
            ReturnObj = FindChildByName(ThisName, child);
            if (ReturnObj)
                return ReturnObj;
        }
        return null;
    }
}
