﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechColor : MonoBehaviour {
	public Color color;
    public bool randomizeColor;

	// Use this for initialization
	void Start () {
        foreach (var item in gameObject.GetComponentsInChildren<Renderer>())
        {
            foreach (var mat in item.materials)
                if (mat.name.Contains("Color"))
                    mat.color = randomizeColor?Random.ColorHSV():color;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
