//Maya ASCII 2017ff05 scene
//Name: Basic_Mecha.0003.ma
//Last modified: Fri, Feb 02, 2018 08:17:23 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "57F02CE5-4F31-A6EC-5D05-8897BB4CC603";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -54.932260023676946 -2.0871829391858387 15.450327409359627 ;
	setAttr ".r" -type "double3" -15.338352730453902 -434.99999999986079 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "8DA41C8D-4BCD-E8D6-9D20-B899EBCA6DC4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 58.970557961209188;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 -17.685995180364522 0.73127269744873047 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "DEFBBE81-4D59-C1DB-60E7-FC96822C4ACA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BEC4DFEB-4727-263C-1835-5A976D6A6F5E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 25.971735514366536;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "90496FA8-4FA9-6A33-DE77-0AAC0D1B9DAD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "D197ED46-4D10-44A6-2CAA-A896A1A3D1A8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 109.01391833912356;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "EE308D80-4811-9364-4FA8-00B92704BA63";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.2584937751254 -17.685995180364522 2.0615391731264463 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "5518C0C6-48E8-F804-B673-848FBDEA0A7C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.2584937751252;
	setAttr ".ow" 62.262485447458786;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 8.8817841970012523e-016 -17.685995180364522 2.0615391731262243 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "Rootroot";
	rename -uid "36260844-4F53-3ABA-4FAF-13818B63E76D";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "rootTorso" -p "Rootroot";
	rename -uid "479E03A7-4499-3560-C05C-429ED5C2467F";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "rootHead" -p "Rootroot";
	rename -uid "6A860384-462E-B055-A6AC-20B0C72C74A0";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "rootBack" -p "Rootroot";
	rename -uid "A557B44D-492B-0C6F-C530-2CB7D5EC7B85";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "rootArmLeft" -p "Rootroot";
	rename -uid "8A14F95D-43CC-01EA-BBFA-17BDF6BFCF11";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
createNode joint -n "clavacleLeft" -p "rootArmLeft";
	rename -uid "0A945317-4EE3-EB7F-2AB5-03AC8DC39B66";
	setAttr ".t" -type "double3" 12 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.9627599675931779e-032 9.4623222080256131 -3.5798325122702445e-031 ;
	setAttr ".radi" 0.5;
createNode joint -n "shoulderLeft" -p "clavacleLeft";
	rename -uid "E0200340-44B4-E4FC-33E2-6D827B1DC23D";
	setAttr ".t" -type "double3" 6.0827625302982185 2.2204460492503106e-016 -6.6613381477509392e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 260.53767779197437 0 ;
	setAttr ".radi" 0.75862068965517249;
createNode joint -n "upperArmLeft" -p "shoulderLeft";
	rename -uid "22CC745B-4EB7-13A7-11E4-90A2A69841EE";
	setAttr ".t" -type "double3" 5.9999999999999991 -1.3322676295501859e-015 -3.5527136788005009e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.5201820811150655e-031 -4.1177829190967271e-030 
		12.52880770915152 ;
	setAttr ".radi" 0.92514885123928725;
createNode joint -n "elbowLeft" -p "|Rootroot|rootArmLeft|clavacleLeft|shoulderLeft|upperArmLeft";
	rename -uid "C2099CCD-4838-926F-D6B9-91A53F63CD9C";
	setAttr ".t" -type "double3" 9.2195444572928835 -3.9968028886505635e-015 -3.5527136788005009e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.0271593940183435e-030 -2.1580315938533606e-030 
		50.906141113770495 ;
	setAttr ".radi" 0.91091061603443957;
createNode joint -n "wristLeft" -p "elbowLeft";
	rename -uid "77D0BB3F-4F80-6EA5-945C-FC83CB2AC823";
	setAttr ".t" -type "double3" 8.9442719099991557 -5.3290705182007514e-015 -7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -26.565051177078036 89.999999999999957 0 ;
	setAttr ".radi" 0.91091061603443957;
createNode joint -n "rootArmRight" -p "Rootroot";
	rename -uid "49B0CCC9-486E-087B-CC8D-12B7D23365C8";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
createNode joint -n "clavacleRight" -p "rootArmRight";
	rename -uid "25F5D630-4D5A-3AA1-28AD-02BCC6A52E6D";
	setAttr ".t" -type "double3" -12 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.9627599675931779e-032 9.4623222080256131 -3.5798325122702445e-031 ;
	setAttr ".radi" 0.5;
createNode joint -n "shoulderRight" -p "clavacleRight";
	rename -uid "7351C74A-4037-5BD7-B967-25A1079A1699";
	setAttr ".t" -type "double3" -5.753964555687503 2.2204460492503116e-016 -1.9727878476642866 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 260.53767779197437 0 ;
	setAttr ".radi" 0.75862068965517249;
createNode joint -n "upperArmRight" -p "shoulderRight";
	rename -uid "767B0B5C-40FC-4E3A-0BBD-CEBF936F3BAB";
	setAttr ".t" -type "double3" 5.9999999999999991 -1.3322676295501859e-015 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159366e-007 -1.1848489498583662e-023 12.528807709151522 ;
	setAttr ".radi" 0.92514885123928725;
createNode joint -n "elbowRight" -p "|Rootroot|rootArmRight|clavacleRight|shoulderRight|upperArmRight";
	rename -uid "8550BA6D-4204-420E-7222-1D882F7D9221";
	setAttr ".t" -type "double3" 9.2195444572928835 -3.7747582837255322e-015 -3.5527136788005009e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.6284689768358047e-030 -3.4213652935339491e-030 
		50.906141113770495 ;
	setAttr ".radi" 0.91091061603443957;
createNode joint -n "wristRight" -p "elbowRight";
	rename -uid "83A0B628-42FF-E34E-AE26-4BB8FAB543BB";
	setAttr ".t" -type "double3" 8.9442719099991539 -7.1054273576010019e-015 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -26.565051177078008 89.999999999999943 0 ;
	setAttr ".radi" 0.91091061603443957;
createNode joint -n "rootLegsJnt" -p "Rootroot";
	rename -uid "404F0E79-428F-F754-F6EA-0ABC3B751C23";
	setAttr ".t" -type "double3" 0 -22 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 1.5865543914200564;
createNode joint -n "hipsJnt" -p "rootLegsJnt";
	rename -uid "F222B8CA-46BD-39D1-B6EF-BFB5F8060C79";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 4.9303806576313227e-032 2.2204460492503131e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
createNode joint -n "upperLegLeftJnt" -p "hipsJnt";
	rename -uid "292AC345-4BCF-8FC5-27D3-DCBBBE5F9382";
	setAttr ".t" -type "double3" 7.9999999999999982 8.8817841970012523e-016 1.9721522630525291e-031 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 -68.198590513648213 -89.999999999999972 ;
createNode joint -n "lowerLegLeftJnt" -p "upperLegLeftJnt";
	rename -uid "0E8B7813-4C3D-3D46-B1FF-44AE78FE8FE1";
	setAttr ".t" -type "double3" 5.385164807134502 -2.2463410722767539e-015 1.5378418617811802e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 21.801409486352004 ;
createNode joint -n "footLeftJnt" -p "lowerLegLeftJnt";
	rename -uid "0B3D773A-45B5-A820-0FAD-22A43471D445";
	setAttr ".t" -type "double3" 4.0000000000000027 4.4408920985005631e-016 7.460698725481052e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 68.198590513648085 ;
createNode joint -n "joint1" -p "footLeftJnt";
	rename -uid "E2F77DF6-499F-3965-E3CA-F3A215E21165";
	setAttr ".t" -type "double3" 5.3851648071344904 2.2204460492503131e-015 -7.7715611723760958e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 21.801409486351922 -90 0 ;
createNode joint -n "upperLegRightJnt" -p "hipsJnt";
	rename -uid "FDCED3EE-4275-E132-3983-19A04AE62FE4";
	setAttr ".t" -type "double3" -7.9999999999999982 -8.8817841970012523e-016 -1.9721522630525291e-031 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000043 -68.198590513648227 -90.000000000000071 ;
createNode joint -n "lowerLegRightJnt" -p "upperLegRightJnt";
	rename -uid "A0CA8E94-4496-707A-ACDF-8093C37D5E34";
	setAttr ".t" -type "double3" 5.3851648071345037 -7.3439423352200738e-016 -4.4408920985006222e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 21.801409486351794 ;
createNode joint -n "footRightJnt" -p "lowerLegRightJnt";
	rename -uid "A251C16D-4684-41C3-943D-26AE5B13DD4F";
	setAttr ".t" -type "double3" 3.9999999999999973 1.1102230246251565e-015 -4.3520742565306136e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 68.198590513648156 ;
createNode joint -n "joint2" -p "footRightJnt";
	rename -uid "6F3D4236-40B6-DEAC-4BF9-6284F0ABB3BB";
	setAttr ".t" -type "double3" 5.3851648071345037 2.2204460492503131e-016 5.340172748447003e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -21.801409486351837 89.999999999999972 0 ;
createNode transform -n "BasicMechGrouped";
	rename -uid "0177049C-4D00-9488-267F-B69328B58F5D";
	setAttr ".rp" -type "double3" 0 -13 1.0000000000000009 ;
	setAttr ".sp" -type "double3" 0 -13 1.0000000000000009 ;
createNode transform -n "Basic_Torso" -p "BasicMechGrouped";
	rename -uid "38150AC2-4E38-3106-AF69-2AB392F4167C";
	setAttr ".rp" -type "double3" 0 5 -1 ;
	setAttr ".sp" -type "double3" 0 5 -1 ;
createNode transform -n "base" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "5687955E-4776-20EC-B431-EB86A683BF5F";
	setAttr ".rp" -type "double3" 0 -17 -1 ;
	setAttr ".sp" -type "double3" 0 -17 -1 ;
createNode mesh -n "baseShape" -p "|BasicMechGrouped|Basic_Torso|base";
	rename -uid "B20F1ADE-4BB5-52FE-2AD2-6FB2C764C8AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  -3 0 0 3 0 0 -3 0 0 3 0 0;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "backSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "D459BBFE-443F-93C5-3C8E-498F2B118BF5";
	setAttr ".rp" -type "double3" 0 -7.5 -11.5 ;
	setAttr ".sp" -type "double3" 0 -7.5 -11.5 ;
createNode mesh -n "backSideShape" -p "|BasicMechGrouped|Basic_Torso|backSide";
	rename -uid "8C90774E-404D-A119-5C13-11B5F4956669";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[0:1]" -type "float3"  0 -4 0 0 -4 0;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "FE9902B7-4B05-B77A-9454-A09CCB9ADCF3";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "rightSideShape" -p "|BasicMechGrouped|Basic_Torso|rightSide";
	rename -uid "E0DE3407-41EB-E6C2-CEAD-018F87BE1007";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -26 0 0 -26 0 0 -26 0 0 -26 
		0 0 -20 0 0 -20 0 0 -20 0 0 -20 0 0;
	setAttr -s 8 ".vt[0:7]"  13 -15 8 13 -15 -10 13 0 8 13 0 -10 10 0 8
		 10 0 -10 10 -15 8 10 -15 -10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightNozzle" -p "|BasicMechGrouped|Basic_Torso|rightSide";
	rename -uid "6E3AAC7B-4D89-5CFF-9166-978B87C5BE70";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "rightNozzleShape" -p "|BasicMechGrouped|Basic_Torso|rightSide|rightNozzle";
	rename -uid "EA8B82A3-45D0-60B5-8430-1C8E0660895D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -22.205099 0 0 -25.999722 
		0 0 -24.102966 0 0 -27.897589 0 0 -24.102966 0 0 -27.897589 0 0 -22.205099 0 0 -25.999722 
		0 0;
	setAttr -s 8 ".vt[0:7]"  11.10254955 -15.94851685 -2.5 12.99986076 -16.58113861 -2.5
		 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5 12.051483154 -13.10255051 -5.5
		 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5 12.99986076 -16.58113861 -5.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "06C37FDC-4EEB-D5E2-F652-A48C6BCF8552";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "leftSideShape" -p "|BasicMechGrouped|Basic_Torso|leftSide";
	rename -uid "1290F97F-4AEF-7373-D2B6-EFA594447191";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftNozzle" -p "|BasicMechGrouped|Basic_Torso|leftSide";
	rename -uid "9E258720-4205-43CC-E9AA-B6ACF03285C0";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "leftNozzleShape" -p "|BasicMechGrouped|Basic_Torso|leftSide|leftNozzle";
	rename -uid "756ED554-4D39-2719-C8E7-15A011104AA8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "4EF332D3-40DD-D9F6-22A4-E29160E69659";
	setAttr ".rp" -type "double3" 0 -1.5 11.5 ;
	setAttr ".sp" -type "double3" 0 -1.5 11.5 ;
createNode mesh -n "frontSideShape" -p "|BasicMechGrouped|Basic_Torso|frontSide";
	rename -uid "240FBBC4-4A64-C047-B4B0-A49AA879F1E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft1" -p "|BasicMechGrouped|Basic_Torso|frontSide";
	rename -uid "DA5A97AE-4AB6-D808-737D-48A54CC76B56";
	setAttr ".rp" -type "double3" 0.99999999999999978 0 13 ;
	setAttr ".sp" -type "double3" 0.99999999999999978 0 13 ;
createNode mesh -n "frontLeftShape1" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1";
	rename -uid "8981AC15-446A-F817-6A7C-2BA9D2BE092E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft2" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1";
	rename -uid "EC447CBF-4B7A-68CE-5D43-3387389E5D50";
	setAttr ".rp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
createNode mesh -n "frontLeftShape2" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2";
	rename -uid "C4FBF2AC-4AEF-1CD1-8BE8-D1AA68F5582A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[0]" -type "float3" -0.075828865 0 0 ;
	setAttr ".pt[2]" -type "float3" -0.38835922 8.8817842e-016 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2";
	rename -uid "8F4E599B-471A-D845-EC15-50AA27EF9227";
	setAttr ".rp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "F23315E2-4483-29C2-F6AB-4CA8057855AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "01186FCA-494E-584F-5C95-228E0CF99493";
	setAttr ".rp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8";
	rename -uid "D96C6124-421A-9B22-8816-5CB8F1492E86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "E59C2CDB-44D4-3824-3860-81A55AC3D276";
	setAttr ".rp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11";
	rename -uid "A4C79538-4683-2A6D-69DD-DF9B10E5FD1A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "B5364B96-4B40-02BE-DD0C-38A4F8490A73";
	setAttr ".rp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10";
	rename -uid "49274F99-4C74-D279-E5C0-E5A7D437EE54";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "C5F9ED8E-4ADB-B6FE-0679-38816DDCE302";
	setAttr ".rp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9";
	rename -uid "A6E870DD-4579-55CB-8072-7CB118EF453B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "A2942867-4BB8-B9AF-4995-66AA739642DC";
	setAttr ".rp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube15";
	rename -uid "A54AB347-497D-39BE-BBCC-77960C5B8ED5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  8.7661715 -10.002368 11.40912 
		6.8029666 -10.002368 9.7271099 8.7687492 -10.004949 11.415309 6.8055449 -10.004949 
		9.7332993 8.1979952 -8.6640949 12.045508 6.2347908 -8.6640949 10.3635 8.1954165 -8.6615152 
		12.03932 6.2322121 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "23A77EF9-4783-E752-C980-89A4DCF51C92";
	setAttr ".rp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube14";
	rename -uid "9462CCF8-4905-AC44-3704-0BAC8F8F7885";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.3451328 -9.0245857 12.22831 
		6.8835888 -9.0245857 9.9206047 7.3477111 -9.0271645 12.234498 6.886168 -9.0271654 
		9.9267941 6.7769575 -7.6863122 12.864698 6.3154144 -7.6863122 10.556993 6.7743788 
		-7.6837316 12.85851 6.3128357 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "5764FA3A-4D93-9542-2DBF-BABA02737ED2";
	setAttr ".rp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube13";
	rename -uid "94145998-4709-D2FF-3583-32AEB55EBF4D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.4257555 -8.0468016 12.421804 
		6.9642124 -8.0468016 10.114099 7.4283352 -8.0493822 12.427992 6.9667902 -8.0493822 
		10.120287 6.8575802 -6.708529 13.058191 6.3960366 -6.7085285 10.750487 6.8550024 
		-6.7059488 13.052003 6.3934579 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "5310FC2F-4553-4570-E395-86979156798A";
	setAttr ".rp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12";
	rename -uid "36B47657-41C2-2C16-9608-83A187855E82";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft3" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1";
	rename -uid "547EA155-44AE-5549-0CE7-2AAF174DF833";
	setAttr ".rp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
createNode mesh -n "frontLeftShape3" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3";
	rename -uid "3DD55ED4-46B0-DDAB-C522-869D1C2E88B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.076490089 0 0 -25.998447 
		0 0 -0.38087735 8.8817842e-016 0 -26.615368 0 0 0.76427841 0 0 -25.081852 0 0 1.381197 
		0 0 -24.464935 0 0;
	setAttr -s 8 ".vt[0:7]"  0.076159477 -13.017789841 13.38278198 12.99922371 -13.017789841 7.99813509
		 0.38461828 -3.050000191 14.1230793 13.30768394 -3.050000191 8.73843193 -0.38213921 -2.88960266 12.28287315
		 12.54092598 -2.88960218 6.89822626 -0.69059849 -12.85739136 11.54257584 12.23246765 -12.85739136 6.1579299;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3";
	rename -uid "18C07746-4664-7A3E-40AC-5D91CD75FCEC";
	setAttr ".rp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "6256EB2D-474C-CFDC-4C55-80BEA832B066";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.3908882 0 0 -22.160103 
		0 0 -7.5521345 0 0 -22.321348 0 0 -6.0478439 0 0 -20.817059 0 0 -5.8865986 0 0 -20.655813 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.69544411 -6.36505508 14.26905155 11.080051422 -6.36505508 11.19211197
		 3.77606726 -5.38727188 14.46254539 11.1606741 -5.38727188 11.38560486 3.023921967 -4.96803474 12.65740871
		 10.40852928 -4.96803474 9.58046818 2.94329929 -5.94581842 12.46391582 10.32790661 -5.94581842 9.38697529;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "1668818A-4BB5-549C-6343-30A3FEE4547F";
	setAttr ".rp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube8";
	rename -uid "F7A4CEAE-434C-3AE8-2A92-1BAFBD6E3A42";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -8.9145479 
		0 0 -7.3908882 0 0 -9.2370405 0 0 -5.8865986 0 0 -7.7327509 0 0 -5.564106 0 0 -7.4102583 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 4.45727396 -8.32062149 13.49744701
		 3.69544411 -6.36505508 14.26905155 4.61852026 -6.36505508 13.8844347 2.94329929 -5.94581842 12.46391582
		 3.86637545 -5.94581842 12.079298973 2.78205299 -7.90138435 12.076927185 3.70512915 -7.90138435 11.69231033;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "CC8ACE55-4EB5-FDE7-E18B-689B796CA117";
	setAttr ".rp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube11";
	rename -uid "1385D34A-4516-DBA4-9F76-DAA67AB24F03";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -13.969256 
		0 0 -8.3050871 0 0 -15.205947 0 0 -6.8007975 0 0 -13.701655 0 0 -5.564106 0 0 -12.464966 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 6.9846282 -11.25397968 11.76311302
		 4.15254354 -7.53839588 13.80608845 7.60297346 -10.47175312 11.68713665 3.40039873 -7.11915922 12.00095272064
		 6.85082769 -10.052515984 9.88199997 2.78205299 -7.90138435 12.076927185 6.23248291 -10.83474159 9.95797634;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "34D68EA8-4803-4E79-C904-A684F4BD506D";
	setAttr ".rp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube10";
	rename -uid "396814A8-4D2E-D144-C8D0-2A9FD7C569D4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -21.353872 0 0 -21.51512 
		0 0 -13.969266 0 0 -14.130511 0 0 -12.464975 0 0 -12.626222 0 0 -19.849583 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  10.67693615 -11.2539711 10.2246418 10.75755978 -10.27618885 10.4181366
		 6.98463297 -11.2539711 11.76311302 7.065255642 -10.27618885 11.95660782 6.23248768 -10.83473492 9.95797539
		 6.31311083 -9.85695076 10.15146923 9.92479134 -10.83473492 8.41950512 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "8522D05E-4841-D0FF-366B-F5A8D9869DBB";
	setAttr ".rp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube9";
	rename -uid "D38F2CC7-4397-B5F8-0172-CABCEE855294";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -19.668966 0 0 -21.51512 
		0 0 -20.313951 0 0 -22.160103 0 0 -18.809662 0 0 -20.655813 0 0 -18.164677 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  9.83448315 -10.27618885 10.8027544 10.75755978 -10.27618885 10.4181366
		 10.15697575 -6.36505508 11.57672882 11.080051422 -6.36505508 11.19211197 9.40483093 -5.94581842 9.77159309
		 10.32790661 -5.94581842 9.38697529 9.082338333 -9.85695076 8.99761677 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "19710E1A-469E-9039-EA80-70A346E1E157";
	setAttr ".rp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube15";
	rename -uid "D8143C51-4CEB-F78A-FE7C-67BD22386525";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.7661715 -10.002368 11.40912 
		-12.802967 -10.002368 9.7271099 -2.7687492 -10.004949 11.415309 -12.805545 -10.004949 
		9.7332993 -2.1979952 -8.6640949 12.045508 -12.234791 -8.6640949 10.3635 -2.1954165 
		-8.6615152 12.03932 -12.232212 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "97D92174-410F-884E-8D68-1385374252ED";
	setAttr ".rp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube14";
	rename -uid "6D077C63-4F2E-29B8-D239-BB905E4595F0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.3451328 -9.0245857 12.22831 
		-12.883589 -9.0245857 9.9206047 -1.3477111 -9.0271645 12.234498 -12.886168 -9.0271654 
		9.9267941 -0.77695751 -7.6863122 12.864698 -12.315414 -7.6863122 10.556993 -0.77437878 
		-7.6837316 12.85851 -12.312836 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "35D6485E-4C33-D003-E0E1-919F8F1C20D7";
	setAttr ".rp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube13";
	rename -uid "DDA181E2-4B40-A65D-2F5C-35AEF94596B8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.4257555 -8.0468016 12.421804 
		-12.964212 -8.0468016 10.114099 -1.4283352 -8.0493822 12.427992 -12.96679 -8.0493822 
		10.120287 -0.85758018 -6.708529 13.058191 -12.396036 -6.7085285 10.750487 -0.8550024 
		-6.7059488 13.052003 -12.393457 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "9BB721EF-4A61-C40A-CE46-D291E9E0E748";
	setAttr ".rp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube12";
	rename -uid "2F531F50-4CFC-CA61-6F5E-3486BFB98488";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9.0127592 0 0 -20.08967 
		0 0 -9.0179157 0 0 -20.094828 0 0 -7.8764067 0 0 -18.953318 0 0 -7.8712502 0 0 -18.94816 
		0 0;
	setAttr -s 8 ".vt[0:7]"  4.5063796 -7.074018478 13.61529922 10.044835091 -7.074018478 11.30759335
		 4.50895786 -7.066599846 13.62148666 10.047413826 -7.066599846 11.31378174 3.93820333 -5.72574568 12.2516861
		 9.47665882 -5.72574568 9.94398022 3.93562508 -5.73316574 12.2454977 9.47408009 -5.73316574 9.93779278;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontRight1" -p "|BasicMechGrouped|Basic_Torso|frontSide";
	rename -uid "4597C8EE-4951-8DB2-268E-CF8D4274CC89";
	setAttr ".rp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
	setAttr ".sp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
createNode mesh -n "frontRightShape1" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontRight1";
	rename -uid "65BFBE3B-40CA-3AE2-BC95-7FA64C9617C9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -6.4999886 -1.5 6.4999719 
		-7.5 -1.5 11.5 -6.4999886 -1.5 6.4999719 -7.5 -1.5 11.5 -5.3461356 -1.5 6.7307439 
		-6.3461475 -1.5 11.730772 -5.3461356 -1.5 6.7307439 -6.3461475 -1.5 11.730772;
	setAttr -s 8 ".vt[0:7]"  -6.5 -1.5 1.5 6.5 -1.5 1.5 -6.5 1.5 1.5 6.5 1.5 1.5
		 -6.5 1.5 -1.5 6.5 1.5 -1.5 -6.5 -1.5 -1.5 6.5 -1.5 -1.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "jawMid" -p "|BasicMechGrouped|Basic_Torso|frontSide";
	rename -uid "89DD58D2-4EBA-FEA5-4620-779AD1CDDDB9";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "jawMidShape" -p "|BasicMechGrouped|Basic_Torso|frontSide|jawMid";
	rename -uid "BBF1F4BB-4B86-E8BB-08F6-36B6004C7BDD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube6" -p "|BasicMechGrouped|Basic_Torso|frontSide|jawMid";
	rename -uid "244AC02B-40FF-1E35-DA16-9E87AD3FC4B0";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape6" -p "|BasicMechGrouped|Basic_Torso|frontSide|jawMid|pCube6";
	rename -uid "2D86932E-400D-DFAB-892B-19933F24CE01";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  30.304123 -18.700001 12.7 
		6.4461813 -18.700001 12.7 26.457947 -18.700001 12.7 2.6000054 -18.700001 12.7 24.327759 
		-18.700001 12.7 0.46981788 -18.700001 12.7 28.173935 -18.700001 12.7 4.3159933 -18.700001 
		12.7;
	setAttr -s 8 ".vt[0:7]"  -15.15206146 1.20867193 -5.36155796 -3.22309065 -4.54291534 -0.82075399
		 -13.22897339 5.76722908 -4.63955355 -1.30000269 0.01564152 -0.098749459 -12.16387939 5.76210356 -7.4441123
		 -0.23490894 0.01051604 -2.90330839 -14.086967468 1.20354652 -8.16611671 -2.15799665 -4.54804087 -3.62531281;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube5" -p "|BasicMechGrouped|Basic_Torso|frontSide|jawMid";
	rename -uid "5C15DB6A-40CF-A28D-8F27-77A9656A0CDB";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape5" -p "|BasicMechGrouped|Basic_Torso|frontSide|jawMid|pCube5";
	rename -uid "39E10765-44E7-234D-22B2-F2BA48648DB0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethmid" -p "|BasicMechGrouped|Basic_Torso|frontSide";
	rename -uid "298B6792-4E41-C0F5-614B-86BFC82313CE";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11 ;
createNode mesh -n "teethmidShape" -p "|BasicMechGrouped|Basic_Torso|frontSide|teethmid";
	rename -uid "FA1D085F-4E2E-5401-EC68-34BB66F7FDCB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethLeft" -p "|BasicMechGrouped|Basic_Torso|frontSide|teethmid";
	rename -uid "BDFBD9ED-489C-8419-DE9B-FE899DE6FE67";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethLeftShape" -p "|BasicMechGrouped|Basic_Torso|frontSide|teethmid|teethLeft";
	rename -uid "85E2D2E6-4D90-2E60-D420-6A86E5506DE2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 3 ".pt";
	setAttr ".pt[1]" -type "float3" -1.4901161e-008 2.3436136 0 ;
	setAttr ".pt[3]" -type "float3" -1.4901161e-008 0 0 ;
	setAttr ".pt[7]" -type "float3" 0 2.3436136 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethRight" -p "|BasicMechGrouped|Basic_Torso|frontSide|teethmid";
	rename -uid "0BBCA9C6-4D6B-0074-7A2F-C0A378F8E720";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethRightShape" -p "|BasicMechGrouped|Basic_Torso|frontSide|teethmid|teethRight";
	rename -uid "2DC87D9D-46ED-BB5C-9CC4-77A69DB4A2E3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1 0 0 -25.992804 2.3436136 
		0 -1 0 0 -25.992804 0 0 -0.44872528 0 0 -25.441528 0 0 -0.44872528 0 0 -25.441528 
		2.3436136 0;
	setAttr -s 8 ".vt[0:7]"  0.5 -19.5 11.5 12.99640179 -19.5 7.91671467
		 0.5 -12.5 11.5 12.99640179 -12.5 7.91671467 0.22436264 -12.5 10.53873825 12.72076416 -12.5 6.95545292
		 0.22436264 -19.5 10.53873825 12.72076416 -19.5 6.95545292;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Legs" -p "BasicMechGrouped";
	rename -uid "0E955BC1-4964-2BAE-1301-8C89317036A7";
	setAttr ".rp" -type "double3" 0 -25.5 -0.26776695251464844 ;
	setAttr ".sp" -type "double3" 0 -25.5 -0.26776695251464844 ;
createNode transform -n "hips" -p "|BasicMechGrouped|Basic_Legs";
	rename -uid "FF221B08-4D2C-B8C4-D3E0-6A9FB22B3148";
	setAttr ".rp" -type "double3" 0 -22 0 ;
	setAttr ".sp" -type "double3" 0 -22 0 ;
createNode mesh -n "hipsShape" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "77109185-401B-E0D8-99D4-9E8E8A364F90";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegLeft" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "42BBF380-45E3-7FA5-B896-15BC05AED86A";
	setAttr ".rp" -type "double3" 8 -22 0 ;
	setAttr ".sp" -type "double3" 8 -22 0 ;
createNode mesh -n "upperLegLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "5EA57348-475E-14C4-5813-288368A89BE5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegLeft" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "228A07EC-48C7-D1B5-C59A-CA93721A23C5";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "lowerLegLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft";
	rename -uid "2F94FD93-495B-848A-C8EC-43804F051E67";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeLeft" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "8C34651A-4401-EFE8-7775-988B897B7665";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "kneeLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft";
	rename -uid "DC8270F5-4118-DFD7-DFA5-80B685231A9C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegRight" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "9B70C2EC-4F64-7511-B019-008BE4895002";
	setAttr ".rp" -type "double3" -8 -22 0 ;
	setAttr ".sp" -type "double3" -8 -22 0 ;
createNode mesh -n "upperLegRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "5F05A66E-43B5-DCB9-ACE4-44B565BA7A15";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10 -22 0 -22 -22 0 -10 -22 
		0 -22 -22 0 -10 -22 0 -22 -22 0 -10 -22 0 -22 -22 0;
	setAttr -s 8 ".vt[0:7]"  5 -5 3 11 -5 3 5 3 3 11 3 3 5 3 -3 11 3 -3
		 5 -5 -3 11 -5 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegRight" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "F8F9085D-4AAD-48B5-138A-9A97137A60CF";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "lowerLegRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|lowerLegRight";
	rename -uid "CFEB7661-434F-3C86-89D4-ABBEC9BFE28F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9 -22 0 -23 -22 0 -9 -22 
		0 -23 -22 0 -9 -22 0 -23 -22 0 -9 -22 0 -23 -22 0;
	setAttr -s 8 ".vt[0:7]"  4.5 -11 3 11.5 -11 3 4.5 -5 3 11.5 -5 3 4.5 -5 -4
		 11.5 -5 -4 4.5 -11 -4 11.5 -11 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeRight" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "8CE62129-4288-F4FE-0934-6FBF0BAFC905";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "kneeRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|kneeRight";
	rename -uid "D4AE6C18-46FD-A0C5-74F7-55B272B7B015";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8 -22 0 -24 -22 0 -8 -22 
		0 -24 -22 0 -8 -22 0 -24 -22 0 -8 -22 0 -24 -22 0;
	setAttr -s 8 ".vt[0:7]"  4 -8.53553391 -2 12 -8.53553391 -2 4 -5 1.53553391
		 12 -5 1.53553391 4 -1.46446609 -2 12 -1.46446609 -2 4 -5 -5.53553391 12 -5 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft1" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "06472B54-43CE-28A3-B943-D9BDAB2EA2FA";
	setAttr ".rp" -type "double3" 12 -20 -1 ;
	setAttr ".sp" -type "double3" 12 -20 -1 ;
createNode mesh -n "panelLeftShape1" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1";
	rename -uid "DBFD30E4-4851-2760-62DE-C197E02BF610";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft2" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1";
	rename -uid "E32EE436-4527-A180-E176-B59AE16172E1";
	setAttr ".rp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
	setAttr ".sp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
createNode mesh -n "panelLeftShape2" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2";
	rename -uid "8B9BCF4D-47FC-F698-98BB-6DACEA0D097E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight1" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "F68B51CC-4D47-0E73-FF16-88A6A326CC32";
	setAttr ".rp" -type "double3" -12.000000000000004 -20 -1 ;
	setAttr ".sp" -type "double3" -12.000000000000004 -20 -1 ;
createNode mesh -n "panelRightShape1" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1";
	rename -uid "94705DCE-447C-DE62-7C01-7987E04AA3E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.462697 -20.005127 3.5 
		-17.462698 -20.005127 -6.5 -5.5743756 -20.264633 3.5 -15.574375 -20.264633 -6.5 -6.537303 
		-19.994873 4.5 -16.537302 -19.994873 -5.5 -8.4256248 -19.735367 4.5 -18.425625 -19.735367 
		-5.5;
	setAttr -s 8 ".vt[0:7]"  -5 -3.5 0.5 5 -3.5 0.5 -5 3.5 0.5 5 3.5 0.5
		 -5 3.5 -0.5 5 3.5 -0.5 -5 -3.5 -0.5 5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight2" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1";
	rename -uid "83C52A54-4F1F-904E-B235-1FA276B726FB";
	setAttr ".rp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
createNode mesh -n "panelRightShape2" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRight2";
	rename -uid "3E1D1CC0-4F0D-A0E9-D73E-D4A0EA4779E2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8.8865137 -19.740492 5.1672893 
		-9.7058802 -19.740492 8.4535904 -8 -20 3.5 -8.8193665 -20 6.7863011 -7.5479331 -20.26976 
		3.6497855 -8.3673 -20.26976 6.9360862 -8.4344463 -20.010252 5.3170743 -9.2538128 
		-20.010252 8.6033754;
	setAttr -s 8 ".vt[0:7]"  -3.5 -3.5 0.5 3.5 -3.5 0.5 -3.5 3.5 0.5 3.5 3.5 0.5
		 -3.5 3.5 -0.5 3.5 3.5 -0.5 -3.5 -3.5 -0.5 3.5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Right_Arm" -p "BasicMechGrouped";
	rename -uid "F3B002F6-4980-5CC0-B5B0-FB934E143A8D";
	setAttr ".rp" -type "double3" -12 -7.5 4 ;
	setAttr ".sp" -type "double3" -12 -7.5 4 ;
createNode transform -n "armRight1" -p "|BasicMechGrouped|Basic_Right_Arm";
	rename -uid "BD74FAE6-410F-1FC1-D37E-9DA76F5EBB34";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armRightShape1" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1";
	rename -uid "F9CA20B8-4D87-D5E4-A152-A59C959A712A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmRight" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1";
	rename -uid "55D5A9D1-42CF-366A-9909-E39182F42194";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmRightShape" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight";
	rename -uid "74D767FC-49D7-8FF4-1FA1-1A841375FD7D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRight" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight";
	rename -uid "124826B8-4A44-F82F-FF5C-BF8E08AF2D8B";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmRightShape" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight";
	rename -uid "7F70E54D-4C9C-07D2-9469-0CA2707F78A2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armRight2" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1";
	rename -uid "5FB98D3E-4CD7-A706-BE56-3E8AC5A0CE51";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armRightShape2" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRight2";
	rename -uid "0473C47A-4B4E-11A3-D0CB-0C8D83367D91";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Left_Arm" -p "BasicMechGrouped";
	rename -uid "DB84E1AA-438F-A86B-DDBC-73B4CCC1BA99";
	setAttr ".rp" -type "double3" 18 -7.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" 18 -7.5 4.0000000000000018 ;
createNode transform -n "armLeft1" -p "|BasicMechGrouped|Basic_Left_Arm";
	rename -uid "5368470F-49AB-1828-DF8C-DFAA22B8B83D";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".rpt" -type "double3" 36 0 2.2043642384652358e-015 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armLeftShape1" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "3E95B1B2-4291-169C-A142-8995F4B3B91B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmLeft" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "A85A8278-406C-58A5-0534-F4BB74D2C79C";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmLeftShape" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "DE602BD4-425D-C2F9-FB0D-A283FC4BDE5E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmLeft" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "40E8FABA-44ED-FEEC-C5DD-8B905F34991C";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmLeftShape" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft";
	rename -uid "09F812BB-4C5F-0F18-33FB-4397A9EA1DB5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armLeft2" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "3AC2A0E7-42FD-1EB0-DABF-8C97ADFE498B";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armLeftShape2" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeft2";
	rename -uid "9E99AC44-4840-37B9-5266-8FAC8A536702";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5
		 -13 -2 -5 -23 -5 -5 -13 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "BasicMechGrouped1";
	rename -uid "3976F5F6-4C7B-341A-4D60-AAB8DA883134";
	setAttr ".rp" -type "double3" 0 -13 1.0000000000000009 ;
	setAttr ".sp" -type "double3" 0 -13 1.0000000000000009 ;
createNode transform -n "Basic_Torso" -p "BasicMechGrouped1";
	rename -uid "1CEE4AAC-4FC9-F84D-BE9C-E8A579ACEC8D";
	setAttr ".rp" -type "double3" 0 5 -1 ;
	setAttr ".sp" -type "double3" 0 5 -1 ;
createNode transform -n "base" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "0567C14B-4F39-0935-E1E6-4C8C0D80AC85";
	setAttr ".rp" -type "double3" 0 -17 -1 ;
	setAttr ".sp" -type "double3" 0 -17 -1 ;
createNode mesh -n "baseShape" -p "|BasicMechGrouped1|Basic_Torso|base";
	rename -uid "37841105-4FF6-99D5-E1BE-CC8874CD946B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  -3 0 0 3 0 0 -3 0 0 3 0 0;
	setAttr -s 8 ".vt[0:7]"  -10 -19 8 10 -19 8 -10 -15 8 10 -15 8 -10 -15 -10
		 10 -15 -10 -10 -19 -10 10 -19 -10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "backSide" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "07BB5A3F-4CC6-A04B-1225-AE9702803420";
	setAttr ".rp" -type "double3" 0 -7.5 -11.5 ;
	setAttr ".sp" -type "double3" 0 -7.5 -11.5 ;
createNode mesh -n "backSideShape" -p "|BasicMechGrouped1|Basic_Torso|backSide";
	rename -uid "13B9CED1-483C-AE50-87ED-7DB7E596838A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[0:1]" -type "float3"  0 -4 0 0 -4 0;
	setAttr -s 8 ".vt[0:7]"  -10 -15 -10 10 -15 -10 -10 0 -10 10 0 -10
		 -9 0 -13 9 0 -13 -9 -15 -13 9 -15 -13;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightSide" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "A3179E9F-45EE-F8AF-C7CB-BDAD05B52F94";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "rightSideShape" -p "|BasicMechGrouped1|Basic_Torso|rightSide";
	rename -uid "21B6ED6B-4294-C966-DC7D-6BAA660117D5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -26 0 0 -26 0 0 -26 0 0 -26 
		0 0 -20 0 0 -20 0 0 -20 0 0 -20 0 0;
	setAttr -s 8 ".vt[0:7]"  13 -15 8 13 -15 -10 13 0 8 13 0 -10 10 0 8
		 10 0 -10 10 -15 8 10 -15 -10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftSide" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "A00CB26A-4B03-0F8A-623C-3DBA3F940AFF";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "leftSideShape" -p "|BasicMechGrouped1|Basic_Torso|leftSide";
	rename -uid "05F00ED8-4B37-442C-30EC-7C9B99366A7C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  13 -15 8 13 -15 -10 13 0 8 13 0 -10 10 0 8
		 10 0 -10 10 -15 8 10 -15 -10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftNozzle" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "3A8A4A29-4F0C-7322-DB8A-1AAFED654B13";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "leftNozzleShape" -p "|BasicMechGrouped1|Basic_Torso|leftNozzle";
	rename -uid "6165D1EA-4A99-0ABF-9C21-2E89510E26C8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  11.10254955 -15.94851685 -2.5 12.99986076 -16.58113861 -2.5
		 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5 12.051483154 -13.10255051 -5.5
		 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5 12.99986076 -16.58113861 -5.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontSide" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "76741453-4993-2150-9336-32A2867336C3";
	setAttr ".rp" -type "double3" 0 -1.5 11.5 ;
	setAttr ".sp" -type "double3" 0 -1.5 11.5 ;
createNode mesh -n "frontSideShape" -p "|BasicMechGrouped1|Basic_Torso|frontSide";
	rename -uid "D1FD7493-4353-8DB8-CEA0-BEB06A8B48FE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -1 -3 13 1 -3 13 -1 0 13 1 0 13 -1 0 10 1 0 10
		 -1 -3 10 1 -3 10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft1" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "25357077-4BCE-B379-35DB-EFA6922B32EB";
	setAttr ".rp" -type "double3" 0.99999999999999978 0 13 ;
	setAttr ".sp" -type "double3" 0.99999999999999978 0 13 ;
createNode mesh -n "frontLeftShape1" -p "|BasicMechGrouped1|Basic_Torso|frontLeft1";
	rename -uid "726185E1-4146-4F21-8103-DDBA8DA5B217";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  1 -3 13 12.99998856 -3 7.99997139 1 0 13
		 12.99998856 0 7.99997139 -0.1538527 0 10.23077202 11.84613609 0 5.23074341 -0.1538527 -3 10.23077202
		 11.84613609 -3 5.23074341;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontRight1" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "D5054794-4DE2-21E2-3C09-619E75BCB497";
	setAttr ".rp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
	setAttr ".sp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
createNode mesh -n "frontRightShape1" -p "|BasicMechGrouped1|Basic_Torso|frontRight1";
	rename -uid "BCB52CE7-43FD-8137-8188-038542772B06";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -6.4999886 -1.5 6.4999719 
		-7.5 -1.5 11.5 -6.4999886 -1.5 6.4999719 -7.5 -1.5 11.5 -5.3461356 -1.5 6.7307439 
		-6.3461475 -1.5 11.730772 -5.3461356 -1.5 6.7307439 -6.3461475 -1.5 11.730772;
	setAttr -s 8 ".vt[0:7]"  -6.5 -1.5 1.5 6.5 -1.5 1.5 -6.5 1.5 1.5 6.5 1.5 1.5
		 -6.5 1.5 -1.5 6.5 1.5 -1.5 -6.5 -1.5 -1.5 6.5 -1.5 -1.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "jawMid" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "A7CC38AC-45C5-0445-59AB-008FB9096C62";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "jawMidShape" -p "|BasicMechGrouped1|Basic_Torso|jawMid";
	rename -uid "C04BDB51-432C-69BF-7D29-629A94224E95";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -3.5 -23.63844299 11.91782761 3.5 -23.63844299 11.91782761
		 -3.5 -18.70000076 12.69999981 3.5 -18.70000076 12.69999981 -3.5 -18.23069763 9.73693466
		 3.5 -18.23069763 9.73693466 -3.5 -23.16913986 8.95476246 3.5 -23.16913986 8.95476246;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethmid" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "7093D537-4E96-C631-C0A5-D5B8F4C186A0";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11 ;
createNode mesh -n "teethmidShape" -p "|BasicMechGrouped1|Basic_Torso|teethmid";
	rename -uid "C7A6626B-4F3C-2F82-D4A5-988DF795AEC0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -19.5 11.5 0.5 -19.5 11.5 -0.5 -12.5 11.5
		 0.5 -12.5 11.5 -0.5 -12.5 10.5 0.5 -12.5 10.5 -0.5 -19.5 10.5 0.5 -19.5 10.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightNozzle" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "7D1AA9BE-4F3C-9F71-E420-C09827900D52";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "rightNozzleShape" -p "|BasicMechGrouped1|Basic_Torso|rightNozzle";
	rename -uid "9F5CEB29-4696-C068-8F71-84BC72A92856";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -22.205099 0 0 -25.999722 
		0 0 -24.102966 0 0 -27.897589 0 0 -24.102966 0 0 -27.897589 0 0 -22.205099 0 0 -25.999722 
		0 0;
	setAttr -s 8 ".vt[0:7]"  11.10254955 -15.94851685 -2.5 12.99986076 -16.58113861 -2.5
		 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5 12.051483154 -13.10255051 -5.5
		 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5 12.99986076 -16.58113861 -5.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft2" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "07663734-4BE5-764A-0286-BC86C913633C";
	setAttr ".rp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
createNode mesh -n "frontLeftShape2" -p "|BasicMechGrouped1|Basic_Torso|frontLeft2";
	rename -uid "2225F4BE-423B-03EB-F927-DCAAB605D7F3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[0]" -type "float3" -0.075828865 0 0 ;
	setAttr ".pt[2]" -type "float3" -0.38835922 8.8817842e-016 0 ;
	setAttr -s 8 ".vt[0:7]"  0.076159477 -13.017789841 13.38278198 12.99922371 -13.017789841 7.99813509
		 0.38461828 -3.050000191 14.1230793 13.30768394 -3.050000191 8.73843193 -0.38213921 -2.88960266 12.28287315
		 12.54092598 -2.88960218 6.89822626 -0.69059849 -12.85739136 11.54257584 12.23246765 -12.85739136 6.1579299;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "EE0372F6-4AD9-9C42-59EC-01A38333A2DE";
	setAttr ".rp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped1|Basic_Torso|pCube7";
	rename -uid "86651BC6-4161-AD57-578C-EEB7EC95EFB3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  3.69544411 -6.36505508 14.26905155 11.080051422 -6.36505508 11.19211197
		 3.77606726 -5.38727188 14.46254539 11.1606741 -5.38727188 11.38560486 3.023921967 -4.96803474 12.65740871
		 10.40852928 -4.96803474 9.58046818 2.94329929 -5.94581842 12.46391582 10.32790661 -5.94581842 9.38697529;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "DDE53E2E-4726-1DAB-09D3-989B1E1E5A07";
	setAttr ".rp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped1|Basic_Torso|pCube8";
	rename -uid "CEEA7DA9-45B0-8B61-465B-EABD139A6F05";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 4.45727396 -8.32062149 13.49744701
		 3.69544411 -6.36505508 14.26905155 4.61852026 -6.36505508 13.8844347 2.94329929 -5.94581842 12.46391582
		 3.86637545 -5.94581842 12.079298973 2.78205299 -7.90138435 12.076927185 3.70512915 -7.90138435 11.69231033;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "D72C1250-4A9A-5D47-1A3E-DB80A62DA2E2";
	setAttr ".rp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped1|Basic_Torso|pCube11";
	rename -uid "C716CF90-4D42-6B00-3871-20BB61532EE9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 6.9846282 -11.25397968 11.76311302
		 4.15254354 -7.53839588 13.80608845 7.60297346 -10.47175312 11.68713665 3.40039873 -7.11915922 12.00095272064
		 6.85082769 -10.052515984 9.88199997 2.78205299 -7.90138435 12.076927185 6.23248291 -10.83474159 9.95797634;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "3F37E10A-425C-D01B-E46F-21BB7C8D8216";
	setAttr ".rp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped1|Basic_Torso|pCube10";
	rename -uid "AC7AF754-4FF4-F5CD-834D-0E9846F2D01A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  10.67693615 -11.2539711 10.2246418 10.75755978 -10.27618885 10.4181366
		 6.98463297 -11.2539711 11.76311302 7.065255642 -10.27618885 11.95660782 6.23248768 -10.83473492 9.95797539
		 6.31311083 -9.85695076 10.15146923 9.92479134 -10.83473492 8.41950512 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "7A3C4159-473A-91DD-D004-59B0F7CF87D5";
	setAttr ".rp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped1|Basic_Torso|pCube9";
	rename -uid "AD7E6732-4105-DB4E-1C5C-AAB4061D8038";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  9.83448315 -10.27618885 10.8027544 10.75755978 -10.27618885 10.4181366
		 10.15697575 -6.36505508 11.57672882 11.080051422 -6.36505508 11.19211197 9.40483093 -5.94581842 9.77159309
		 10.32790661 -5.94581842 9.38697529 9.082338333 -9.85695076 8.99761677 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "308E4690-442F-E7CD-A154-299EF2550F5C";
	setAttr ".rp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped1|Basic_Torso|pCube15";
	rename -uid "FA19DCBA-4AFF-116D-5277-B19DE78956A4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  8.7661715 -10.002368 11.40912 
		6.8029666 -10.002368 9.7271099 8.7687492 -10.004949 11.415309 6.8055449 -10.004949 
		9.7332993 8.1979952 -8.6640949 12.045508 6.2347908 -8.6640949 10.3635 8.1954165 -8.6615152 
		12.03932 6.2322121 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "5EDFDB11-465C-330C-2852-74BE88FBCF70";
	setAttr ".rp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped1|Basic_Torso|pCube14";
	rename -uid "0FD83A20-42F9-ADAD-B136-C889E7A97A2E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.3451328 -9.0245857 12.22831 
		6.8835888 -9.0245857 9.9206047 7.3477111 -9.0271645 12.234498 6.886168 -9.0271654 
		9.9267941 6.7769575 -7.6863122 12.864698 6.3154144 -7.6863122 10.556993 6.7743788 
		-7.6837316 12.85851 6.3128357 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "EF0ABBC4-4C59-A851-D272-FAB066443EE9";
	setAttr ".rp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped1|Basic_Torso|pCube13";
	rename -uid "99C4921B-4550-5DC6-09C2-E9B0C7B7F7F9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.4257555 -8.0468016 12.421804 
		6.9642124 -8.0468016 10.114099 7.4283352 -8.0493822 12.427992 6.9667902 -8.0493822 
		10.120287 6.8575802 -6.708529 13.058191 6.3960366 -6.7085285 10.750487 6.8550024 
		-6.7059488 13.052003 6.3934579 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "098168ED-4966-AB2D-8034-81985484612C";
	setAttr ".rp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped1|Basic_Torso|pCube12";
	rename -uid "DD226CB7-4DFE-E0E8-405B-5FB91DE8A4FC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  4.5063796 -7.074018478 13.61529922 10.044835091 -7.074018478 11.30759335
		 4.50895786 -7.066599846 13.62148666 10.047413826 -7.066599846 11.31378174 3.93820333 -5.72574568 12.2516861
		 9.47665882 -5.72574568 9.94398022 3.93562508 -5.73316574 12.2454977 9.47408009 -5.73316574 9.93779278;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft3" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "DB32FCBB-4D36-C19D-CFDE-6290883C94F3";
	setAttr ".rp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
createNode mesh -n "frontLeftShape3" -p "|BasicMechGrouped1|Basic_Torso|frontLeft3";
	rename -uid "96E3659D-4277-DF23-824D-0682AC66BDAC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.076490089 0 0 -25.998447 
		0 0 -0.38087735 8.8817842e-016 0 -26.615368 0 0 0.76427841 0 0 -25.081852 0 0 1.381197 
		0 0 -24.464935 0 0;
	setAttr -s 8 ".vt[0:7]"  0.076159477 -13.017789841 13.38278198 12.99922371 -13.017789841 7.99813509
		 0.38461828 -3.050000191 14.1230793 13.30768394 -3.050000191 8.73843193 -0.38213921 -2.88960266 12.28287315
		 12.54092598 -2.88960218 6.89822626 -0.69059849 -12.85739136 11.54257584 12.23246765 -12.85739136 6.1579299;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube16" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "5445790B-4BB9-DD43-9F2E-D5B523F92F18";
	setAttr ".rp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape16" -p "pCube16";
	rename -uid "2F443DB2-4348-2F9E-66B8-66871BE0E45F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.3908882 0 0 -22.160103 
		0 0 -7.5521345 0 0 -22.321348 0 0 -6.0478439 0 0 -20.817059 0 0 -5.8865986 0 0 -20.655813 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.69544411 -6.36505508 14.26905155 11.080051422 -6.36505508 11.19211197
		 3.77606726 -5.38727188 14.46254539 11.1606741 -5.38727188 11.38560486 3.023921967 -4.96803474 12.65740871
		 10.40852928 -4.96803474 9.58046818 2.94329929 -5.94581842 12.46391582 10.32790661 -5.94581842 9.38697529;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube17" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "77A5B0A0-429A-6179-4AC0-849F054C9869";
	setAttr ".rp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape17" -p "pCube17";
	rename -uid "EA1C5E99-4E7D-084F-3F14-9CB6B89A33E8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -8.9145479 
		0 0 -7.3908882 0 0 -9.2370405 0 0 -5.8865986 0 0 -7.7327509 0 0 -5.564106 0 0 -7.4102583 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 4.45727396 -8.32062149 13.49744701
		 3.69544411 -6.36505508 14.26905155 4.61852026 -6.36505508 13.8844347 2.94329929 -5.94581842 12.46391582
		 3.86637545 -5.94581842 12.079298973 2.78205299 -7.90138435 12.076927185 3.70512915 -7.90138435 11.69231033;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube18" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "6BDA3FDA-4472-54C8-89CF-DEAEE80D99E1";
	setAttr ".rp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape18" -p "pCube18";
	rename -uid "0696AD44-4F83-14CB-9254-3C822CAE1192";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -13.969256 
		0 0 -8.3050871 0 0 -15.205947 0 0 -6.8007975 0 0 -13.701655 0 0 -5.564106 0 0 -12.464966 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 6.9846282 -11.25397968 11.76311302
		 4.15254354 -7.53839588 13.80608845 7.60297346 -10.47175312 11.68713665 3.40039873 -7.11915922 12.00095272064
		 6.85082769 -10.052515984 9.88199997 2.78205299 -7.90138435 12.076927185 6.23248291 -10.83474159 9.95797634;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube19" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "20E934C1-4A1D-ECD4-F166-03BF5CE49645";
	setAttr ".rp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape19" -p "pCube19";
	rename -uid "431B0C9A-4066-865C-8739-C08004CE380E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -21.353872 0 0 -21.51512 
		0 0 -13.969266 0 0 -14.130511 0 0 -12.464975 0 0 -12.626222 0 0 -19.849583 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  10.67693615 -11.2539711 10.2246418 10.75755978 -10.27618885 10.4181366
		 6.98463297 -11.2539711 11.76311302 7.065255642 -10.27618885 11.95660782 6.23248768 -10.83473492 9.95797539
		 6.31311083 -9.85695076 10.15146923 9.92479134 -10.83473492 8.41950512 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube20" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "DBC57DBE-44CC-44E0-D610-499551728EF3";
	setAttr ".rp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape20" -p "pCube20";
	rename -uid "24CEED3C-45A8-789C-16EA-CF80CC61B52E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -19.668966 0 0 -21.51512 
		0 0 -20.313951 0 0 -22.160103 0 0 -18.809662 0 0 -20.655813 0 0 -18.164677 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  9.83448315 -10.27618885 10.8027544 10.75755978 -10.27618885 10.4181366
		 10.15697575 -6.36505508 11.57672882 11.080051422 -6.36505508 11.19211197 9.40483093 -5.94581842 9.77159309
		 10.32790661 -5.94581842 9.38697529 9.082338333 -9.85695076 8.99761677 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube21" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "57904C7C-446F-EAEB-B5E3-178A86AD484D";
	setAttr ".rp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape21" -p "pCube21";
	rename -uid "23215AB3-4424-4F5B-3FBD-6AB8264335A5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.7661715 -10.002368 11.40912 
		-12.802967 -10.002368 9.7271099 -2.7687492 -10.004949 11.415309 -12.805545 -10.004949 
		9.7332993 -2.1979952 -8.6640949 12.045508 -12.234791 -8.6640949 10.3635 -2.1954165 
		-8.6615152 12.03932 -12.232212 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube22" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "E8C459FA-4218-9974-FA4A-20A6522E9291";
	setAttr ".rp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape22" -p "pCube22";
	rename -uid "184ED756-410B-B92F-8A00-7892E258C488";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.3451328 -9.0245857 12.22831 
		-12.883589 -9.0245857 9.9206047 -1.3477111 -9.0271645 12.234498 -12.886168 -9.0271654 
		9.9267941 -0.77695751 -7.6863122 12.864698 -12.315414 -7.6863122 10.556993 -0.77437878 
		-7.6837316 12.85851 -12.312836 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube23" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "4829372B-4EA9-E4B8-88D7-838170655892";
	setAttr ".rp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape23" -p "pCube23";
	rename -uid "E7EF038C-4773-9EA6-13E2-3F9AFDDA70A1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.4257555 -8.0468016 12.421804 
		-12.964212 -8.0468016 10.114099 -1.4283352 -8.0493822 12.427992 -12.96679 -8.0493822 
		10.120287 -0.85758018 -6.708529 13.058191 -12.396036 -6.7085285 10.750487 -0.8550024 
		-6.7059488 13.052003 -12.393457 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube24" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "95589DF2-4236-644D-B4C4-8DB1B59401F8";
	setAttr ".rp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape24" -p "pCube24";
	rename -uid "A5308CF5-4F23-732B-88D9-B0B21240FCD1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9.0127592 0 0 -20.08967 
		0 0 -9.0179157 0 0 -20.094828 0 0 -7.8764067 0 0 -18.953318 0 0 -7.8712502 0 0 -18.94816 
		0 0;
	setAttr -s 8 ".vt[0:7]"  4.5063796 -7.074018478 13.61529922 10.044835091 -7.074018478 11.30759335
		 4.50895786 -7.066599846 13.62148666 10.047413826 -7.066599846 11.31378174 3.93820333 -5.72574568 12.2516861
		 9.47665882 -5.72574568 9.94398022 3.93562508 -5.73316574 12.2454977 9.47408009 -5.73316574 9.93779278;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube6" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "DA888CD0-42E6-56EA-D9FC-2E9617C72B61";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape6" -p "|BasicMechGrouped1|Basic_Torso|pCube6";
	rename -uid "701A85CF-47A3-8F52-EB98-1FA817AC12B1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  30.304123 -18.700001 12.7 
		6.4461813 -18.700001 12.7 26.457947 -18.700001 12.7 2.6000054 -18.700001 12.7 24.327759 
		-18.700001 12.7 0.46981788 -18.700001 12.7 28.173935 -18.700001 12.7 4.3159933 -18.700001 
		12.7;
	setAttr -s 8 ".vt[0:7]"  -15.15206146 1.20867193 -5.36155796 -3.22309065 -4.54291534 -0.82075399
		 -13.22897339 5.76722908 -4.63955355 -1.30000269 0.01564152 -0.098749459 -12.16387939 5.76210356 -7.4441123
		 -0.23490894 0.01051604 -2.90330839 -14.086967468 1.20354652 -8.16611671 -2.15799665 -4.54804087 -3.62531281;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube5" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "56EDF81A-4FF4-2EC0-4A28-F2ACFE948C6E";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape5" -p "|BasicMechGrouped1|Basic_Torso|pCube5";
	rename -uid "2CAFDDE1-4EDE-A230-D700-C0812F8F69D9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -15.15206146 -17.49132919 7.33844185 -3.22309065 -23.24291611 11.87924576
		 -13.22897339 -12.93277168 8.060445786 -1.30000269 -18.6843586 12.60125065 -12.16387939 -12.93789673 5.25588751
		 -0.23490894 -18.68948555 9.79669189 -14.086967468 -17.49645424 4.53388309 -2.15799665 -23.24804115 9.074687004;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethLeft" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "94FF8AA0-40B0-DD99-EE56-50A1C9FE5C2A";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethLeftShape" -p "|BasicMechGrouped1|Basic_Torso|teethLeft";
	rename -uid "18255242-44BA-553B-0354-A1AC5FD827DE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 3 ".pt";
	setAttr ".pt[1]" -type "float3" -1.4901161e-008 2.3436136 0 ;
	setAttr ".pt[3]" -type "float3" -1.4901161e-008 0 0 ;
	setAttr ".pt[7]" -type "float3" 0 2.3436136 0 ;
	setAttr -s 8 ".vt[0:7]"  0.5 -19.5 11.5 12.99640179 -19.5 7.91671467
		 0.5 -12.5 11.5 12.99640179 -12.5 7.91671467 0.22436264 -12.5 10.53873825 12.72076416 -12.5 6.95545292
		 0.22436264 -19.5 10.53873825 12.72076416 -19.5 6.95545292;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethRight" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "25441E3D-4CD1-0834-16D5-F68F0A69C969";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethRightShape" -p "|BasicMechGrouped1|Basic_Torso|teethRight";
	rename -uid "62B6C783-4982-D57B-B2A7-85AD462FEEFF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1 0 0 -25.992804 2.3436136 
		0 -1 0 0 -25.992804 0 0 -0.44872528 0 0 -25.441528 0 0 -0.44872528 0 0 -25.441528 
		2.3436136 0;
	setAttr -s 8 ".vt[0:7]"  0.5 -19.5 11.5 12.99640179 -19.5 7.91671467
		 0.5 -12.5 11.5 12.99640179 -12.5 7.91671467 0.22436264 -12.5 10.53873825 12.72076416 -12.5 6.95545292
		 0.22436264 -19.5 10.53873825 12.72076416 -19.5 6.95545292;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Legs" -p "BasicMechGrouped1";
	rename -uid "49793D68-434D-3303-859B-0A82D42068D6";
	setAttr ".rp" -type "double3" 0 -25.5 -0.26776695251464844 ;
	setAttr ".sp" -type "double3" 0 -25.5 -0.26776695251464844 ;
createNode transform -n "hips" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "F2146221-48BC-34A1-6418-42A6017C8FA0";
	setAttr ".rp" -type "double3" 0 -22 0 ;
	setAttr ".sp" -type "double3" 0 -22 0 ;
createNode mesh -n "hipsShape" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "FB2CC886-4418-C822-AD21-A9B3C49A12E0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -5 -26 5 5 -26 5 -5 -18 5 5 -18 5 -5 -18 -5
		 5 -18 -5 -5 -26 -5 5 -26 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegLeft" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "C7BF1E48-4F68-9874-67E9-07A599B2027E";
	setAttr ".rp" -type "double3" 8 -22 0 ;
	setAttr ".sp" -type "double3" 8 -22 0 ;
createNode mesh -n "upperLegLeftShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft";
	rename -uid "674BACB9-4BDB-E751-94F5-E687FEFB98C1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  5 -27 3 11 -27 3 5 -19 3 11 -19 3 5 -19 -3
		 11 -19 -3 5 -27 -3 11 -27 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegLeft" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft";
	rename -uid "E8FCBD04-4411-1EB3-B1B5-C7B00E0FB20C";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "lowerLegLeftShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|lowerLegLeft";
	rename -uid "9BF0B797-429E-4920-E93F-A9AA77E27EAF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  4.5 -33 3 11.5 -33 3 4.5 -27 3 11.5 -27 3
		 4.5 -27 -4 11.5 -27 -4 4.5 -33 -4 11.5 -33 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeLeft" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft";
	rename -uid "C8D2C976-4FCD-C2DE-F529-03A59814EA27";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "kneeLeftShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|kneeLeft";
	rename -uid "4FB9781C-41FC-B052-1E4F-5EA921AB7B23";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  4 -30.53553391 -2 12 -30.53553391 -2 4 -27 1.53553391
		 12 -27 1.53553391 4 -23.46446609 -2 12 -23.46446609 -2 4 -27 -5.53553391 12 -27 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegRight" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "B1375159-4271-3413-F6CA-32B63234581A";
	setAttr ".rp" -type "double3" -8 -22 0 ;
	setAttr ".sp" -type "double3" -8 -22 0 ;
createNode mesh -n "upperLegRightShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight";
	rename -uid "5261C5C8-470E-9384-6439-DCBB759F671C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10 -22 0 -22 -22 0 -10 -22 
		0 -22 -22 0 -10 -22 0 -22 -22 0 -10 -22 0 -22 -22 0;
	setAttr -s 8 ".vt[0:7]"  5 -5 3 11 -5 3 5 3 3 11 3 3 5 3 -3 11 3 -3
		 5 -5 -3 11 -5 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegRight" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight";
	rename -uid "200CB4C4-4A61-2209-3E7C-2D8D6AF20A0F";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "lowerLegRightShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|lowerLegRight";
	rename -uid "36AD71C2-41C9-C5B7-2FE8-CA8157B7CF6A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9 -22 0 -23 -22 0 -9 -22 
		0 -23 -22 0 -9 -22 0 -23 -22 0 -9 -22 0 -23 -22 0;
	setAttr -s 8 ".vt[0:7]"  4.5 -11 3 11.5 -11 3 4.5 -5 3 11.5 -5 3 4.5 -5 -4
		 11.5 -5 -4 4.5 -11 -4 11.5 -11 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeRight" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight";
	rename -uid "F539CEA7-4251-9CFB-449D-B0B027D37CD8";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "kneeRightShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|kneeRight";
	rename -uid "A7A5AFD3-49B6-1F46-B745-8FB22975AE62";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8 -22 0 -24 -22 0 -8 -22 
		0 -24 -22 0 -8 -22 0 -24 -22 0 -8 -22 0 -24 -22 0;
	setAttr -s 8 ".vt[0:7]"  4 -8.53553391 -2 12 -8.53553391 -2 4 -5 1.53553391
		 12 -5 1.53553391 4 -1.46446609 -2 12 -1.46446609 -2 4 -5 -5.53553391 12 -5 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft1" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "57727B67-4FF9-F9C0-5661-34AC29CC31E5";
	setAttr ".rp" -type "double3" 12 -20 -1 ;
	setAttr ".sp" -type "double3" 12 -20 -1 ;
createNode mesh -n "panelLeftShape1" -p "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1";
	rename -uid "FC7A0B73-44EA-DE61-0273-9F9CA4F13933";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  12.46269703 -23.50512695 -6 12.46269703 -23.50512695 4
		 10.57437515 -16.76463318 -6 10.57437515 -16.76463318 4 11.53730297 -16.49487305 -6
		 11.53730297 -16.49487305 4 13.42562485 -23.23536682 -6 13.42562485 -23.23536682 4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft2" -p "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1";
	rename -uid "AE85FF9F-4B11-5F63-BE9D-F299F8F019EF";
	setAttr ".rp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
	setAttr ".sp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
createNode mesh -n "panelLeftShape2" -p "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1|panelLeft2";
	rename -uid "ABA9A7F9-4F06-4742-60F1-CCA5B512945A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  6.20588017 -23.24049377 8.95359039 12.38651276 -23.24049377 5.66728878
		 5.31936646 -16.5 7.28630066 11.5 -16.5 4 4.86730003 -16.76976013 6.43608665 11.047932625 -16.76976013 3.14978504
		 5.75381279 -23.51025391 8.10337543 11.93444633 -23.51025391 4.8170743;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight1" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "F533770A-4025-13D4-5B03-78BAA7EA95F4";
	setAttr ".rp" -type "double3" -12.000000000000004 -20 -1 ;
	setAttr ".sp" -type "double3" -12.000000000000004 -20 -1 ;
createNode mesh -n "panelRightShape1" -p "|BasicMechGrouped1|Basic_Legs|hips|panelRight1";
	rename -uid "A330C60F-4406-14F5-23E7-379004AA358F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.462697 -20.005127 3.5 
		-17.462698 -20.005127 -6.5 -5.5743756 -20.264633 3.5 -15.574375 -20.264633 -6.5 -6.537303 
		-19.994873 4.5 -16.537302 -19.994873 -5.5 -8.4256248 -19.735367 4.5 -18.425625 -19.735367 
		-5.5;
	setAttr -s 8 ".vt[0:7]"  -5 -3.5 0.5 5 -3.5 0.5 -5 3.5 0.5 5 3.5 0.5
		 -5 3.5 -0.5 5 3.5 -0.5 -5 -3.5 -0.5 5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight2" -p "|BasicMechGrouped1|Basic_Legs|hips|panelRight1";
	rename -uid "335E1BDB-4BDD-37B8-D84F-DEA691203784";
	setAttr ".rp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
createNode mesh -n "panelRightShape2" -p "|BasicMechGrouped1|Basic_Legs|hips|panelRight1|panelRight2";
	rename -uid "35D0D6F9-4474-B10C-BCE5-83A5BC816EB1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8.8865137 -19.740492 5.1672893 
		-9.7058802 -19.740492 8.4535904 -8 -20 3.5 -8.8193665 -20 6.7863011 -7.5479331 -20.26976 
		3.6497855 -8.3673 -20.26976 6.9360862 -8.4344463 -20.010252 5.3170743 -9.2538128 
		-20.010252 8.6033754;
	setAttr -s 8 ".vt[0:7]"  -3.5 -3.5 0.5 3.5 -3.5 0.5 -3.5 3.5 0.5 3.5 3.5 0.5
		 -3.5 3.5 -0.5 3.5 3.5 -0.5 -3.5 -3.5 -0.5 3.5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Right_Arm" -p "BasicMechGrouped1";
	rename -uid "95CA430A-48C9-4855-B955-3293A8CB5802";
	setAttr ".rp" -type "double3" -12 -7.5 4 ;
	setAttr ".sp" -type "double3" -12 -7.5 4 ;
createNode transform -n "armRight1" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "08ABD1DE-4F7E-9280-708B-35A7D88D0355";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armRightShape1" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight1";
	rename -uid "2206BA41-4B14-DA7D-2608-139BE80ECA8D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmRight" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight1";
	rename -uid "B583A5A7-4C28-D4A8-2F97-1DA3210CE6AB";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmRightShape" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight1|upperArmRight";
	rename -uid "E7A06FC2-4E9A-DFB8-0656-369F10E688DC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRight" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight1|upperArmRight";
	rename -uid "D0892255-41ED-B671-EF05-F1BFB8E1BDB9";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmRightShape" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight";
	rename -uid "B653893E-4783-7D83-53D3-158BBEB4512F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armRight2" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight1";
	rename -uid "AFC980FE-4F11-5357-67DC-5D9A73FFD9A7";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armRightShape2" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight1|armRight2";
	rename -uid "538766D8-4460-752B-3E9E-BA8A0A1907F7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5
		 -13 -2 -5 -23 -5 -5 -13 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Left_Arm" -p "BasicMechGrouped1";
	rename -uid "92041E3B-4226-31AA-9733-E49D916D956C";
	setAttr ".rp" -type "double3" 18 -7.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" 18 -7.5 4.0000000000000018 ;
createNode transform -n "armLeft1" -p "|BasicMechGrouped1|Basic_Left_Arm";
	rename -uid "5B9DCD14-44F5-1C38-B310-848478A8BC51";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".rpt" -type "double3" 36 0 2.2043642384652358e-015 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armLeftShape1" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1";
	rename -uid "81D8B622-4F8B-CA6E-83C7-A8B8D3C6292C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmLeft" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1";
	rename -uid "C6D45555-4ECD-AE23-5DFF-57999D3DE5C9";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmLeftShape" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "71DFA954-4703-85CD-B8BB-4197C1882155";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmLeft" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "04357830-4F2C-6711-B217-C79CEBAE082A";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmLeftShape" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft";
	rename -uid "852181E5-44AA-1FAB-EC0F-EAA6DC33CFFB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armLeft2" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1";
	rename -uid "AF3BF46A-42C5-3C23-11AA-FFA31517B656";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armLeftShape2" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|armLeft2";
	rename -uid "AC56E6E0-4F2C-247D-F832-5ABE3754A6C1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5
		 -13 -2 -5 -23 -5 -5 -13 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "37602909-428B-3375-0327-C0A8E1C6446B";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F595F706-4BE4-BB47-9CB0-D4902F5F1BED";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "E17A4C4F-4C43-D526-9699-3C823EB0C191";
createNode displayLayerManager -n "layerManager";
	rename -uid "A60411A8-4336-2154-8368-72BE843B39BB";
	setAttr -s 5 ".dli[3:4]"  1 2;
	setAttr -s 3 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "4085AE31-4CEA-0975-1FB7-E181F8E20DAE";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "67CAC659-452B-C273-57E3-6B807564F678";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "1D0F1E37-4A8A-A123-D6A9-679337D5783E";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "5CB01404-49E4-D27F-80CC-5590DD35CDFF";
	setAttr ".w" 10;
	setAttr ".h" 8;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube2";
	rename -uid "80215462-4720-5811-BDBF-3CABF39A8D0D";
	setAttr ".w" 6;
	setAttr ".h" 8;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube3";
	rename -uid "485774BA-41D5-6B47-1A0B-2D9BCAEA4729";
	setAttr ".w" 7;
	setAttr ".h" 6;
	setAttr ".d" 7;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "54C76ABA-4E87-6A38-5E51-09801C8F20AE";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1 0 1;
createNode polyCube -n "polyCube4";
	rename -uid "8D8EA23B-4BD4-C7A9-6418-8BB72A958F8A";
	setAttr ".w" 8;
	setAttr ".h" 5;
	setAttr ".d" 5;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry2";
	rename -uid "2667C77D-4AE1-F49D-EB9A-B89717893460";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 0 0 1;
createNode transformGeometry -n "transformGeometry3";
	rename -uid "CBBB4418-47D3-B1D5-C7F1-B88061D1E0DC";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 -8 -0.5 1;
createNode transformGeometry -n "transformGeometry4";
	rename -uid "1581101F-4397-622B-69AB-43A374D0BF73";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.70710678118654746 0.70710678118654768 0
		 0 -0.70710678118654768 0.70710678118654746 0 8 -5 -2 1;
createNode polyCube -n "polyCube5";
	rename -uid "254C7E88-43BD-5F96-FE35-E5BABE340230";
	setAttr ".w" 10;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube6";
	rename -uid "F9C7DB55-488F-B8D7-CF89-0D81FA44309D";
	setAttr ".w" 7;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "3E1FE795-4A57-0096-2DC6-70B3AE2BE52A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 455\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 454\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 455\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 454\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n"
		+ "            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n"
		+ "                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n"
		+ "\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 455\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 455\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 454\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 454\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 454\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 454\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 455\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 455\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D359CC2C-4330-3F25-4DC2-51AEBFAC6971";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube7";
	rename -uid "559D9D51-47BD-434F-417F-9FAC6505AF5B";
	setAttr ".w" 20;
	setAttr ".h" 4;
	setAttr ".d" 18;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry5";
	rename -uid "E9E077C0-4E29-FA31-7748-D19AC1391C4C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry6";
	rename -uid "EC9EFFAE-46F3-4480-4BB4-D68D307AACDF";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry7";
	rename -uid "E2F4CE49-4274-17CB-7130-C7AE84E9341C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry8";
	rename -uid "6631E2D1-4860-AEF4-DB1C-7098D054CA4A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry9";
	rename -uid "1163723F-4DD1-B842-E152-5CAC6D624D8C";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 -2.7755575615628914e-017 1 0
		 -0.26976023601264199 0.96292752326766717 5.5511151231257827e-017 0 -0.96292752326766728 -0.26976023601264193 1.1102230246251565e-016 0
		 12 -20 0 1;
createNode transformGeometry -n "transformGeometry10";
	rename -uid "36814056-4C2E-DE67-205C-B98EC7CE1DB7";
	setAttr ".txf" -type "matrix" 0.88294759285892688 -1.3877787807814457e-016 -0.46947156278589086 0
		 -0.1266447595783457 0.96292752326766717 -0.23818415103641816 0 0.45206708919801941 0.26976023601264182 0.85021453876679487 0
		 8.6269065389189556 -20.005126449443157 7.0516877289946809 1;
createNode polyCube -n "polyCube8";
	rename -uid "6738B210-4080-16FE-2F51-0AA44FB0D2B0";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube9";
	rename -uid "C40BE38B-4975-7B08-D713-59897C1E67F0";
	setAttr ".w" 13;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube10";
	rename -uid "398A8CF3-423D-51BB-DFD7-16A4761E016E";
	setAttr ".w" 14;
	setAttr ".h" 10;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak1";
	rename -uid "C313D164-4FA5-DC5A-DFFE-60BC6EF213E9";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  7 -5 -1 7 -5 -1 7 -5 -1 7
		 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1;
createNode transformGeometry -n "transformGeometry11";
	rename -uid "1431F1F0-4C93-7C38-38B9-92BD65356955";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 0.080198924328858931 0
		 0 -0.080198924328858931 0.99677887845624713 0 -1 -3.0499999999999949 0.79999999999998117 1;
createNode transformGeometry -n "transformGeometry12";
	rename -uid "60349C85-4613-42DE-5B69-56B668AF13BA";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018336 0 0 1 0 0
		 0.38461756040018336 0 0.92307601649691418 0 0.99999999999999778 0 13 1;
createNode polyCube -n "polyCube11";
	rename -uid "ED30530E-4D51-D1A7-9F47-92AD1D4F4E58";
	setAttr ".w" 7;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube12";
	rename -uid "9A96DE03-43F3-3B18-9C72-418ED6F3B382";
	setAttr ".w" 14;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak2";
	rename -uid "33334318-4531-A1A4-0802-7CACC4E28DA7";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093;
createNode transformGeometry -n "transformGeometry13";
	rename -uid "B0B2AE7A-4047-F186-D1F6-F599466439B3";
	setAttr ".txf" -type "matrix" 0.85206933223181147 -0.41082766406220717 0.32434315702851696 0
		 0.38461756040018319 0.91171141897700703 0.14440090283216203 0 -0.35503124552896226 0.0017084929389307166 0.93485287385236782 0
		 -1.3 0.015643446504023089 -0.098768834059513783 1;
createNode polyCube -n "polyCube13";
	rename -uid "9563A64B-43D0-45BF-39B9-E6A5C9C06430";
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube14";
	rename -uid "EF19D4A1-4F7E-B8CD-8282-388E4678AAE7";
	setAttr ".w" 13;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak3";
	rename -uid "6D576173-45BA-8565-1798-84B6B017B978";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -3.5 -0.5 6.5 -3.5 -0.5
		 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5;
createNode transformGeometry -n "transformGeometry14";
	rename -uid "73F384E1-440B-A631-EE57-2D97854B1320";
	setAttr ".txf" -type "matrix" 0.96126169593831889 0 -0.27563735581699905 0 0 1 0 0
		 0.27563735581699905 0 0.96126169593831889 0 0.5 -12.499999999999998 11.5 1;
createNode polyCube -n "polyCube15";
	rename -uid "1888269C-410C-44B7-4915-4ABB8EDA545D";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry15";
	rename -uid "87CD74CE-4C81-39F2-7622-37ABB979F889";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 0 -1 0 0 1 0 0 1 0 2.2204460492503131e-016 0
		 11.499999999999979 -7.5 -0.99999999999999012 1;
createNode polyCube -n "polyCube16";
	rename -uid "6EE74D4C-4E8E-AA70-C5C7-5C8BC768DB57";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube17";
	rename -uid "8878502D-46DA-DDA8-1039-B7A7CD249DD4";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry16";
	rename -uid "4C0842CC-4A13-91C5-95FB-49AA6B865820";
	setAttr ".txf" -type "matrix" 0.94865541582805746 -0.31631140039539413 0 0 0.31631140039539413 0.94865541582805746 0 0
		 0 0 1 0 12.525672292085968 -14.841844299802286 -4 1;
createNode polyCube -n "polyCube18";
	rename -uid "EECFD6F9-4BD1-ACB9-F770-6C938581FF5A";
	setAttr ".w" 8;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube19";
	rename -uid "832ECCF9-41FE-590A-C000-199E07CA5835";
	setAttr ".h" 2;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube20";
	rename -uid "E381CF92-4DBB-5E80-EEE7-75B9B372EC9E";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube21";
	rename -uid "F962A912-4E24-4930-03AF-A68648612EE2";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube22";
	rename -uid "B892D7B7-4364-18F0-F668-B680F093092C";
	setAttr ".w" 5;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube23";
	rename -uid "9CFE9CB5-48DE-C5EF-AAC1-0C8AE95CE1EB";
	setAttr ".w" 6;
	setAttr ".h" 0.01;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry17";
	rename -uid "BB4F6B63-4859-0B98-652A-678E093BA62F";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4 -0.5 -1 1;
createNode transformGeometry -n "transformGeometry18";
	rename -uid "0EB95C3C-49D2-7B4E-B5B5-C28661B0243D";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 -2 -1 1;
createNode transformGeometry -n "transformGeometry19";
	rename -uid "5746B87C-4BF9-75CB-795B-EAB11D3E005E";
	setAttr ".txf" -type "matrix" 0.79999892814850848 -0.60000142913266252 0 0 0.60000142913266252 0.79999892814850848 0 0
		 0 0 1 0 2.2999980349376026 -4.1000041087574015 -1 1;
createNode transformGeometry -n "transformGeometry20";
	rename -uid "D1E40504-4CF2-BF08-21AB-188785E56B1B";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 1 0 5.999999999999992 -5.4999999999999991 -1 1;
createNode transformGeometry -n "transformGeometry21";
	rename -uid "BD7A6EFF-4F93-AF13-B36D-CE840CFC740E";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 7.5 -3 -1 1;
createNode transformGeometry -n "transformGeometry22";
	rename -uid "606417B4-49C7-E882-F47A-6ABF96B731AD";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.86602540378443871 0.49999999999999994 0
		 0 -0.49999999999999994 0.86602540378443871 0 4 -1.25 -1 1;
createNode transformGeometry -n "transformGeometry23";
	rename -uid "29400EF9-4901-32F6-5A09-CF8C6B014F7D";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -17 -1 1;
createNode polyTweak -n "polyTweak4";
	rename -uid "29CDD8A8-4D14-BEF2-2931-BCA13CBF62C2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[0:3]" -type "float3"  -1 0 0 1 0 0 -1 0 0 1 0 0;
createNode transformGeometry -n "transformGeometry24";
	rename -uid "D72E8F73-4A5F-98E7-E544-84B95DC0C4D9";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -7.5 -11.5 1;
createNode transformGeometry -n "transformGeometry25";
	rename -uid "828F6D3A-4037-5063-5122-58BFD6251C9C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1.5 11.5 1;
createNode polyTweak -n "polyTweak5";
	rename -uid "8B3D4378-48B5-81F5-78E3-438A83A3390F";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -1.5 -1.5 6.5 -1.5 -1.5
		 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5;
createNode transformGeometry -n "transformGeometry26";
	rename -uid "5596D9A9-47D5-B5B8-AF0E-47B82DB0EB97";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018324 0 0 1 0 0
		 0.38461756040018324 0 0.92307601649691418 0 0.99999999999999978 0 13 1;
createNode transformGeometry -n "transformGeometry27";
	rename -uid "2BDC3B3E-486C-863E-3B6D-4BACA1607B15";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 0 1 0 0 -1.1102230246251565e-016 0 1 0
		 -2.2204460492503131e-016 0 0 1;
createNode polyTweak -n "polyTweak6";
	rename -uid "CCB74E53-47F8-F2CD-9D28-1A9C9595D025";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -2.5 -1.5 0 -2.5 -1.5 0
		 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5;
createNode transformGeometry -n "transformGeometry28";
	rename -uid "B6511720-4F37-D088-9FB2-898C38D27F54";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.98768834059513777 0.15643446504023087 0
		 0 -0.15643446504023087 0.98768834059513777 0 0 -18.699999999999999 12.699999999999999 1;
createNode transformGeometry -n "transformGeometry29";
	rename -uid "392882F0-46CD-F60F-33D8-1091A26B9F95";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -18.699999999999999 12.699999999999999 1;
createNode polyTweak -n "polyTweak7";
	rename -uid "E614DEAC-42E3-53CE-3F64-A9A09A54DF8D";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -3.5 0 0 -3.5 0 0 -3.5 0
		 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0;
createNode transformGeometry -n "transformGeometry30";
	rename -uid "CFD69E13-4F33-EC74-B5CA-828583EBD4D5";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -12.499999999999998 11 1;
createNode transformGeometry -n "transformGeometry31";
	rename -uid "9D765ED2-472F-4D44-5389-D992D179610E";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 0.38461756040018324 0 0 1 0 0
		 -0.38461756040018324 0 0.92307601649691418 0 5.4615702909401849 0 0.93847259198938104 1;
createNode transformGeometry -n "transformGeometry32";
	rename -uid "30D32989-491A-A765-150A-D59ABBA844EA";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry33";
	rename -uid "A44105B6-4D94-3481-B4E6-DA9E268AC258";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry34";
	rename -uid "86ECE2E6-42DD-4A14-B968-5DB94CB5DAAA";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry35";
	rename -uid "CA99D8E4-4C1F-C762-EB58-C0A971D8BDD0";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry36";
	rename -uid "D9DBB557-4A6A-D709-1CAD-BFA4E9268A76";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry37";
	rename -uid "40BBD014-4BD5-6672-ED4E-6EA1F89B2E28";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry38";
	rename -uid "D49FA4B9-4AEE-8A36-7EF0-C1934BE46969";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry39";
	rename -uid "46E3DB6E-4F9A-DF0B-27A1-C6B62C6C6F59";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry40";
	rename -uid "39E81AFD-4D23-D118-9C41-84AA1FFC680C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry41";
	rename -uid "1E72E26A-4F3B-B7A2-DE2B-799468144F8A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry42";
	rename -uid "ED701055-4E8B-F5DA-13FE-AF8555FB8F1A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry43";
	rename -uid "F02C6DA8-43F2-9347-5C73-B6AC1CC237E8";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry44";
	rename -uid "D7D4F962-4421-8E33-31E6-26A6E0CC6737";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry45";
	rename -uid "D0660701-4415-4B05-4EA3-CF881D9BAEC0";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.030845914622084619 0.99677887845624713 0.074029703596820554 0
		 0.38337866049027253 -0.080198924328858931 0.92010267645365451 0 -5.2908204681950295 1.1228312710267943 1.5021176062717214 1;
createNode transformGeometry -n "transformGeometry46";
	rename -uid "7AAA0379-4F05-C997-78D0-8685D3BE0528";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry47";
	rename -uid "88D074D8-4F7C-47D8-A486-63B28662117D";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry48";
	rename -uid "3372E96A-40CB-8480-842B-DFAD7DD3B53B";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry49";
	rename -uid "0ED7BCFF-4418-1AAC-754A-5EBD6EE84A24";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry50";
	rename -uid "C436AC16-4450-C1B8-AFDF-36B65C240D06";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry51";
	rename -uid "BBEAF9E9-48AC-B1EA-F009-FFA53D405F35";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode polyCube -n "polyCube24";
	rename -uid "A240EF2B-40E3-C65B-678C-08B4636D5C21";
	setAttr ".w" 12;
	setAttr ".h" 9;
	setAttr ".d" 12;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube25";
	rename -uid "70CE1749-418E-FEB1-3E7C-9ABD21534D0A";
	setAttr ".w" 10;
	setAttr ".h" 3;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube26";
	rename -uid "E508D78D-41CD-B69F-13C2-B7B42A8CF99A";
	setAttr ".w" 6;
	setAttr ".h" 12;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube27";
	rename -uid "81205D2E-47AE-F32A-A780-748093631382";
	setAttr ".w" 8;
	setAttr ".h" 8;
	setAttr ".d" 22;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak8";
	rename -uid "3F94CF60-4EE7-A771-A1EA-39823B5FC1FB";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  8.8817842e-016 1.5 0 8.8817842e-016
		 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016
		 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0;
createNode transformGeometry -n "transformGeometry52";
	rename -uid "E2491441-426A-2AC1-9FB0-ACA329B64218";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 1 0 1;
createNode transformGeometry -n "transformGeometry53";
	rename -uid "6AC1074D-4D5E-FAE8-9CD2-F5893AE64E3A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -11 0 1;
createNode transformGeometry -n "transformGeometry54";
	rename -uid "82FC2D08-4756-0F3A-2D90-5FB6B8AAF1FD";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -18 4 1;
createNode transformGeometry -n "transformGeometry55";
	rename -uid "E06C9A47-4190-ABEB-0F3C-59854B67D17A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -3.5 0 1;
createNode transformGeometry -n "transformGeometry56";
	rename -uid "4F66D127-4A26-B67E-C222-C78A3D8D1396";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -1 1;
createNode transformGeometry -n "transformGeometry57";
	rename -uid "16CB2CEE-4C7E-B65F-8641-95A6FEC5765A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -1 1;
createNode displayLayer -n "layer1";
	rename -uid "C9D462A5-4D4A-DC2B-E7F9-CBB0766BF300";
	setAttr ".v" no;
	setAttr ".do" 1;
createNode displayLayer -n "layer2";
	rename -uid "F09E0FF1-4746-6336-25B0-05B9D4DB9C8A";
	setAttr ".v" no;
	setAttr ".do" 2;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 108 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "layer2.di" "Rootroot.do";
connectAttr "Rootroot.s" "rootTorso.is";
connectAttr "Rootroot.s" "rootHead.is";
connectAttr "Rootroot.s" "rootBack.is";
connectAttr "Rootroot.s" "rootArmLeft.is";
connectAttr "rootArmLeft.s" "clavacleLeft.is";
connectAttr "clavacleLeft.s" "shoulderLeft.is";
connectAttr "shoulderLeft.s" "|Rootroot|rootArmLeft|clavacleLeft|shoulderLeft|upperArmLeft.is"
		;
connectAttr "|Rootroot|rootArmLeft|clavacleLeft|shoulderLeft|upperArmLeft.s" "elbowLeft.is"
		;
connectAttr "elbowLeft.s" "wristLeft.is";
connectAttr "Rootroot.s" "rootArmRight.is";
connectAttr "rootArmRight.s" "clavacleRight.is";
connectAttr "clavacleRight.s" "shoulderRight.is";
connectAttr "shoulderRight.s" "|Rootroot|rootArmRight|clavacleRight|shoulderRight|upperArmRight.is"
		;
connectAttr "|Rootroot|rootArmRight|clavacleRight|shoulderRight|upperArmRight.s" "elbowRight.is"
		;
connectAttr "elbowRight.s" "wristRight.is";
connectAttr "Rootroot.s" "rootLegsJnt.is";
connectAttr "rootLegsJnt.s" "hipsJnt.is";
connectAttr "hipsJnt.s" "upperLegLeftJnt.is";
connectAttr "upperLegLeftJnt.s" "lowerLegLeftJnt.is";
connectAttr "lowerLegLeftJnt.s" "footLeftJnt.is";
connectAttr "footLeftJnt.s" "joint1.is";
connectAttr "hipsJnt.s" "upperLegRightJnt.is";
connectAttr "upperLegRightJnt.s" "lowerLegRightJnt.is";
connectAttr "lowerLegRightJnt.s" "footRightJnt.is";
connectAttr "footRightJnt.s" "joint2.is";
connectAttr "layer1.di" "BasicMechGrouped.do";
connectAttr "transformGeometry23.og" "|BasicMechGrouped|Basic_Torso|base|baseShape.i"
		;
connectAttr "transformGeometry24.og" "|BasicMechGrouped|Basic_Torso|backSide|backSideShape.i"
		;
connectAttr "transformGeometry15.og" "|BasicMechGrouped|Basic_Torso|leftSide|leftSideShape.i"
		;
connectAttr "transformGeometry16.og" "|BasicMechGrouped|Basic_Torso|leftSide|leftNozzle|leftNozzleShape.i"
		;
connectAttr "transformGeometry25.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontSideShape.i"
		;
connectAttr "transformGeometry26.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeftShape1.i"
		;
connectAttr "transformGeometry45.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|frontLeftShape2.i"
		;
connectAttr "transformGeometry46.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCubeShape7.i"
		;
connectAttr "transformGeometry47.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8|pCubeShape8.i"
		;
connectAttr "transformGeometry48.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11|pCubeShape11.i"
		;
connectAttr "transformGeometry49.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10|pCubeShape10.i"
		;
connectAttr "transformGeometry50.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9|pCubeShape9.i"
		;
connectAttr "transformGeometry51.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12|pCubeShape12.i"
		;
connectAttr "transformGeometry28.og" "|BasicMechGrouped|Basic_Torso|frontSide|jawMid|jawMidShape.i"
		;
connectAttr "transformGeometry29.og" "|BasicMechGrouped|Basic_Torso|frontSide|jawMid|pCube5|pCubeShape5.i"
		;
connectAttr "transformGeometry30.og" "|BasicMechGrouped|Basic_Torso|frontSide|teethmid|teethmidShape.i"
		;
connectAttr "transformGeometry14.og" "|BasicMechGrouped|Basic_Torso|frontSide|teethmid|teethLeft|teethLeftShape.i"
		;
connectAttr "transformGeometry5.og" "|BasicMechGrouped|Basic_Legs|hips|hipsShape.i"
		;
connectAttr "transformGeometry6.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|upperLegLeftShape.i"
		;
connectAttr "transformGeometry7.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft|lowerLegLeftShape.i"
		;
connectAttr "transformGeometry8.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft|kneeLeftShape.i"
		;
connectAttr "transformGeometry56.og" "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeftShape1.i"
		;
connectAttr "transformGeometry57.og" "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2|panelLeftShape2.i"
		;
connectAttr "transformGeometry52.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRightShape1.i"
		;
connectAttr "transformGeometry53.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|upperArmRightShape.i"
		;
connectAttr "transformGeometry54.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight|lowerArmRightShape.i"
		;
connectAttr "transformGeometry55.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRight2|armRightShape2.i"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube2.out" "transformGeometry1.ig";
connectAttr "transformGeometry1.og" "transformGeometry2.ig";
connectAttr "polyCube3.out" "transformGeometry3.ig";
connectAttr "polyCube4.out" "transformGeometry4.ig";
connectAttr "polyCube1.out" "transformGeometry5.ig";
connectAttr "transformGeometry2.og" "transformGeometry6.ig";
connectAttr "transformGeometry3.og" "transformGeometry7.ig";
connectAttr "transformGeometry4.og" "transformGeometry8.ig";
connectAttr "polyCube5.out" "transformGeometry9.ig";
connectAttr "polyCube6.out" "transformGeometry10.ig";
connectAttr "polyCube10.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "transformGeometry11.ig";
connectAttr "transformGeometry11.og" "transformGeometry12.ig";
connectAttr "polyCube12.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "transformGeometry13.ig";
connectAttr "polyCube14.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "transformGeometry14.ig";
connectAttr "polyCube15.out" "transformGeometry15.ig";
connectAttr "polyCube17.out" "transformGeometry16.ig";
connectAttr "polyCube18.out" "transformGeometry17.ig";
connectAttr "polyCube19.out" "transformGeometry18.ig";
connectAttr "polyCube22.out" "transformGeometry19.ig";
connectAttr "polyCube21.out" "transformGeometry20.ig";
connectAttr "polyCube20.out" "transformGeometry21.ig";
connectAttr "polyCube23.out" "transformGeometry22.ig";
connectAttr "polyCube7.out" "transformGeometry23.ig";
connectAttr "polyCube16.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "transformGeometry24.ig";
connectAttr "polyCube8.out" "transformGeometry25.ig";
connectAttr "polyCube9.out" "polyTweak5.ip";
connectAttr "polyTweak5.out" "transformGeometry26.ig";
connectAttr "transformGeometry12.og" "transformGeometry27.ig";
connectAttr "polyCube11.out" "polyTweak6.ip";
connectAttr "polyTweak6.out" "transformGeometry28.ig";
connectAttr "transformGeometry13.og" "transformGeometry29.ig";
connectAttr "polyCube13.out" "polyTweak7.ip";
connectAttr "polyTweak7.out" "transformGeometry30.ig";
connectAttr "transformGeometry27.og" "transformGeometry31.ig";
connectAttr "transformGeometry17.og" "transformGeometry32.ig";
connectAttr "transformGeometry18.og" "transformGeometry33.ig";
connectAttr "transformGeometry19.og" "transformGeometry34.ig";
connectAttr "transformGeometry20.og" "transformGeometry35.ig";
connectAttr "transformGeometry21.og" "transformGeometry36.ig";
connectAttr "transformGeometry22.og" "transformGeometry37.ig";
connectAttr "transformGeometry31.og" "transformGeometry38.ig";
connectAttr "transformGeometry32.og" "transformGeometry39.ig";
connectAttr "transformGeometry33.og" "transformGeometry40.ig";
connectAttr "transformGeometry34.og" "transformGeometry41.ig";
connectAttr "transformGeometry35.og" "transformGeometry42.ig";
connectAttr "transformGeometry36.og" "transformGeometry43.ig";
connectAttr "transformGeometry37.og" "transformGeometry44.ig";
connectAttr "transformGeometry38.og" "transformGeometry45.ig";
connectAttr "transformGeometry39.og" "transformGeometry46.ig";
connectAttr "transformGeometry40.og" "transformGeometry47.ig";
connectAttr "transformGeometry41.og" "transformGeometry48.ig";
connectAttr "transformGeometry42.og" "transformGeometry49.ig";
connectAttr "transformGeometry43.og" "transformGeometry50.ig";
connectAttr "transformGeometry44.og" "transformGeometry51.ig";
connectAttr "polyCube24.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "transformGeometry52.ig";
connectAttr "polyCube26.out" "transformGeometry53.ig";
connectAttr "polyCube27.out" "transformGeometry54.ig";
connectAttr "polyCube25.out" "transformGeometry55.ig";
connectAttr "transformGeometry9.og" "transformGeometry56.ig";
connectAttr "transformGeometry10.og" "transformGeometry57.ig";
connectAttr "layerManager.dli[3]" "layer1.id";
connectAttr "layerManager.dli[4]" "layer2.id";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|hipsShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|upperLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft|lowerLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft|kneeLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|upperLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|lowerLegRight|lowerLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|kneeRight|kneeRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2|panelLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRight2|panelRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|base|baseShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontSideShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontRight1|frontRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|frontLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|jawMid|jawMidShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|jawMid|pCube5|pCubeShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|jawMid|pCube6|pCubeShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|teethmid|teethmidShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|teethmid|teethLeft|teethLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|leftSide|leftSideShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|rightSide|rightSideShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|backSide|backSideShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|leftSide|leftNozzle|leftNozzleShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|rightSide|rightNozzle|rightNozzleShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|teethmid|teethRight|teethRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|frontLeftShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRight2|armRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|upperArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight|lowerArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|upperArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft|lowerArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeft2|armLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|base|baseShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|backSide|backSideShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|rightSide|rightSideShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|rightNozzle|rightNozzleShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|leftSide|leftSideShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|leftNozzle|leftNozzleShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|frontSide|frontSideShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|frontLeft1|frontLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|frontLeft2|frontLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|frontLeft3|frontLeftShape3.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "pCubeShape16.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape17.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape18.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape19.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape20.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape21.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape22.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape23.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape24.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|frontRight1|frontRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|jawMid|jawMidShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube6|pCubeShape6.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|pCube5|pCubeShape5.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|teethmid|teethmidShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|teethLeft|teethLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Torso|teethRight|teethRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|upperLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|lowerLegLeft|lowerLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|kneeLeft|kneeLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|upperLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|lowerLegRight|lowerLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|kneeRight|kneeRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1|panelLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1|panelLeft2|panelLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|panelRight1|panelRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|panelRight1|panelRight2|panelRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|armRight1|armRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|armRight1|upperArmRight|upperArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight|lowerArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|armRight1|armRight2|armRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|armLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft|upperArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft|lowerArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|armLeft2|armLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
// End of Basic_Mecha.0003.ma
