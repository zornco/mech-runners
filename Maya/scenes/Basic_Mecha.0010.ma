//Maya ASCII 2017ff05 scene
//Name: Basic_Mecha.0010.ma
//Last modified: Sun, Feb 04, 2018 11:19:52 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "57F02CE5-4F31-A6EC-5D05-8897BB4CC603";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -56.698058821560906 78.585035726691387 86.107085954498203 ;
	setAttr ".r" -type "double3" -390.93835239698205 -2544.1999999989339 0 ;
	setAttr ".rp" -type "double3" -1.5543122344752192e-014 -7.4829031859735551e-014 
		0 ;
	setAttr ".rpt" -type "double3" 7.1649883269682367e-014 3.4361230584639746e-014 -3.2486321733328545e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "8DA41C8D-4BCD-E8D6-9D20-B899EBCA6DC4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 110.0628702737752;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -18 22.000007629394531 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "DEFBBE81-4D59-C1DB-60E7-FC96822C4ACA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -18 1000.4180650227833 7.5914223318082907 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BEC4DFEB-4727-263C-1835-5A976D6A6F5E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1018.4180640691087;
	setAttr ".ow" 32.614685658469995;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -18 -17.999999046325684 7.5914223318080651 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "90496FA8-4FA9-6A33-DE77-0AAC0D1B9DAD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -18 -17.999999046325684 1001.2820047700699 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "D197ED46-4D10-44A6-2CAA-A896A1A3D1A8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 991.19058577612191;
	setAttr ".ow" 89.688651464884558;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" -18 -17.999999046325684 10.091418993947958 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "EE308D80-4811-9364-4FA8-00B92704BA63";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.8579817490331 -18 8.399999618530499 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "5518C0C6-48E8-F804-B673-848FBDEA0A7C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1018.8579817490329;
	setAttr ".ow" 62.056461063802537;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -18 -18 8.3999996185302734 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "root";
	rename -uid "36260844-4F53-3ABA-4FAF-13818B63E76D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 14 3.6637368282959638e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 6.3611093629270335e-015 -90 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0 -19.000007629394531 -0 1;
createNode joint -n "hipsJnt" -p "root";
	rename -uid "F222B8CA-46BD-39D1-B6EF-BFB5F8060C79";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.9999923706054687 2.2204460492503146e-016 -4.4408751578416811e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".bps" -type "matrix" 1 -1.2325951644078318e-031 -2.7369110631344103e-047 0
		 0 -2.2204460492503131e-016 1 0 6.1629758220391547e-031 -1 -2.2204460492503131e-016 0
		 -2.2204460492503491e-016 -22 2.2204460492503111e-016 1;
createNode joint -n "leftUpperLeg" -p "hipsJnt";
	rename -uid "292AC345-4BCF-8FC5-27D3-DCBBBE5F9382";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 8.0000000000000018 8.8817841970012602e-016 -1.776356839400252e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -90.000000000002558 -82.573494449265709 90.000000000002558 ;
	setAttr ".bps" -type "matrix" -4.5519144009631418e-015 -0.99161147425390395 0.1292543389136481 0
		 3.6082248300317588e-016 0.12925433891364807 0.99161147425389928 0 -1 5.0792290448036688e-015 -3.608224830031738e-016 0
		 7.9999999999999911 -22.000000000000018 -7.4642277363188622e-016 1;
createNode joint -n "leftLowerLeg" -p "leftUpperLeg";
	rename -uid "0E8B7813-4C3D-3D46-B1FF-44AE78FE8FE1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.0422974419109217 -1.3995186792545075e-016 -2.2392298868072219e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -40.968284869318971 ;
	setAttr ".bps" -type "matrix" -4.6074255521943996e-015 -0.83348313531723095 -0.55254489694662756 -3.4694469519536142e-018
		 -3.524958103184872e-015 -0.55254489694662878 0.83348313531722984 -3.4694469519536142e-018
		 -1.0000000000000002 5.8079467635403737e-015 5.5904905830095459e-016 2.4651903288156619e-032
		 7.9999999999999707 -26.999999999999964 0.65173882246016568 0.99999999999999989;
createNode joint -n "leftFoot" -p "leftLowerLeg";
	rename -uid "0B3D773A-45B5-A820-0FAD-22A43471D445";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.0800818929968505 1.3860322478468738 -3.0908609005564358e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 101.7403698322329 ;
	setAttr ".bps" -type "matrix" -2.4702462297909733e-015 -0.37139067635410128 0.92847669088525731 0
		 5.2180482157382357e-015 0.92847669088525753 0.37139067635410317 0 -1 5.3765500568917864e-015 -3.9307796072068527e-016 0
		 8.0000000000002558 -32.000007629394531 -0.99999999999999578 1;
createNode joint -n "leftToe" -p "leftFoot";
	rename -uid "E2F77DF6-499F-3965-E3CA-F3A215E21165";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.0852946064091329 0.55709309824616038 2.8066438062523957e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 21.801409486351922 -90 0 ;
	setAttr ".bps" -type "matrix" -1 5.4956039718945209e-015 -2.2204460492503096e-016 0
		 5.7176485768195562e-015 1 -2.2620794126737565e-015 0 2.773879273148424e-016 -2.3314683517128311e-015 -0.99999999999999956 0
		 7.9999999999999636 -32.999999999999972 2.9999999999999973 1;
createNode joint -n "rightUpperLeg" -p "hipsJnt";
	rename -uid "FDCED3EE-4275-E132-3983-19A04AE62FE4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -8.0000000000000018 -8.8817841970012523e-016 1.598721155460226e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -90.000000000000497 -82.57349444926578 90.000000000000497 ;
	setAttr ".bps" -type "matrix" -1.1102230246251565e-016 -0.99161147425389629 0.12925433891364699 -1.3183898417423734e-016
		 9.0205620750793969e-017 0.12925433891364643 0.99161147425389928 0 -1 1.1102145542957049e-015 1.3877787807814139e-017 3.0814879110195774e-032
		 -8.0000000000000053 -22.000000000000007 -2.3109794559756727e-015 1.0000000000000004;
createNode joint -n "rightLowerLeg" -p "rightUpperLeg";
	rename -uid "A0CA8E94-4496-707A-ACDF-8093C37D5E34";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.0422974419109474 -3.9562199770264667e-016 2.8959717828038622e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -40.968284869319199 ;
	setAttr ".bps" -type "matrix" -8.8817841970012523e-016 -0.83348313531722751 -0.55254489694663178 0
		 -6.106226635438361e-016 -0.55254489694663167 0.83348313531722762 0 -1 3.8328615309462828e-016 -2.1074226528907866e-016 0
		 -8.0000000000000036 -27 0.6517388224601689 1;
createNode joint -n "rightFoot" -p "rightLowerLeg";
	rename -uid "A251C16D-4684-41C3-943D-26AE5B13DD4F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.0800818929968177 1.3860322478468778 -4.5297099404706387e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 101.74036983223299 ;
	setAttr ".bps" -type "matrix" -4.5796699765787707e-016 -0.37139067635410661 0.92847669088525775 0
		 9.4368957093138306e-016 0.92847669088525542 0.37139067635410616 0 -0.99999999999999989 1.1590451349285306e-015 -1.0085716498037777e-016 0
		 -7.9999999999999716 -32.000007629394517 -1.0000000000000069 1;
createNode joint -n "rightToe" -p "rightFoot";
	rename -uid "6F3D4236-40B6-DEAC-4BF9-6284F0ABB3BB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.0852946064091533 0.5570930982461525 5.2402526762307389e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -21.801409486351837 89.999999999999972 0 ;
	setAttr ".bps" -type "matrix" 1 -9.1593399531575355e-016 -8.3266726846886617e-017 0
		 1.27675647831893e-015 0.99999999999999989 2.8449465006019636e-015 0 -3.3895431420728687e-016 -2.8310687127941492e-015 0.99999999999999978 0
		 -8.0000000000000124 -33 3.0000000000000036 1;
createNode joint -n "spine" -p "hipsJnt";
	rename -uid "A557B44D-492B-0C6F-C530-2CB7D5EC7B85";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.7715611723760989e-015 2.229708619922933e-015 -14.000000000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 89.999999999999972 0 ;
	setAttr ".bps" -type "matrix" 7.7715611723760958e-016 0.99999999999999978 5.551115123125763e-017 0
		 1.1102230246251565e-016 4.4373425918681905e-031 1 0 1 -7.2164496600635086e-016 4.3790577010150533e-047 0
		 2.886579864025394e-015 -8.0000000000000284 4.0060654593231905e-015 1;
createNode joint -n "chest" -p "spine";
	rename -uid "479E03A7-4499-3560-C05C-429ED5C2467F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 8 1.6223788476481007e-015 1.5397799175215213e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 6.6613381477509392e-016 1 -1.1102230246251588e-016 0
		 -3.3306690738754696e-016 0 1 0 1 -7.7715611723760958e-016 2.2204460492503131e-016 0
		 7.4814499542781845e-015 2.6645352591003757e-015 5.1843550971212136e-015 1;
createNode joint -n "head" -p "chest";
	rename -uid "6A860384-462E-B055-A6AC-20B0C72C74A0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3 1.3322676295501878e-015 -1.3322676295501908e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -89.999999999999972 -89.999999999999972 0 ;
	setAttr ".bps" -type "matrix" 1 -3.3306690738754696e-016 2.2204460492503126e-016 0
		 2.2204460492503111e-016 1 3.3306690738754686e-016 0 -3.3306690738754726e-016 -4.4408920985006262e-016 1 0
		 8.1475837690532753e-015 3.0000000000000027 6.1835558192838616e-015 1;
createNode joint -n "rightClavacle" -p "chest";
	rename -uid "25F5D630-4D5A-3AA1-28AD-02BCC6A52E6D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.366582715663034e-030 -3.1554436208840472e-030 -11.999999999999996 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 80.537677791974431 0 ;
	setAttr ".bps" -type "matrix" -0.9863939238321453 0.16439898730536212 -2.3727540321090594e-016 0
		 -3.3306690738754711e-016 -1.9969915453272865e-030 1 0 0.16439898730535776 0.98639392383214231 -7.3007816372586003e-017 0
		 -12.000000000000028 2.0040645594275043e-014 2.5198198380208249e-015 1;
createNode joint -n "rightShoulder" -p "rightClavacle";
	rename -uid "7351C74A-4037-5BD7-B967-25A1079A1699";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.0827625302982309 1.0214051826551456e-014 -3.9895312924438056e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 99.462322208025597 0 ;
	setAttr ".bps" -type "matrix" 0 -1 1.1102230246251565e-016 0 2.7755575615628904e-016 2.2204460492503077e-016 1 0
		 -1 -1.4918621893400541e-015 3.3306690738754854e-016 0 -18 1.0000000000000251 1.0846492522709551e-014 1;
createNode joint -n "rightUpperArm" -p "rightShoulder";
	rename -uid "767B0B5C-40FC-4E3A-0BBD-CEBF936F3BAB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.9999999999999991 -1.3305329060742126e-015 2.3869795029440885e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 12.528807709151492 ;
	setAttr ".bps" -type "matrix" 9.9920072216264049e-016 -0.97618706018395507 0.21693045781865541 6.1629758220391547e-033
		 3.7470027081099018e-016 0.21693045781865555 0.97618706018395307 3.0814879110195774e-033
		 -0.99999999999999989 -7.7715611723760958e-016 3.3306690738754617e-016 -6.9388939039072284e-018
		 -17.999999999999993 -4.9999999999999725 1.6562111500910083e-014 0.99999999999999989;
createNode joint -n "rightElbow" -p "rightUpperArm";
	rename -uid "8550BA6D-4204-420E-7222-1D882F7D9221";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 9.2195444572928693 -1.8819544652401795e-016 -3.5527136788004997e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 38.675257094284511 ;
	setAttr ".bps" -type "matrix" 2.4980018054066017e-016 -0.62654852027652774 0.77938241687844989 0
		 3.7470027081099023e-016 0.77938241687845111 0.62654852027652974 0 -0.99999999999999978 4.4408920985006262e-016 3.3306690738754696e-016 0
		 -17.999999999999996 -13.999999999999931 2.0000000000000049 1;
createNode joint -n "rightWrist" -p "rightElbow";
	rename -uid "83A0B628-42FF-E34E-AE26-4BB8FAB543BB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.4031242374328281 -2.4872464351935949e-015 -2.0224597329764142e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -38.659808254089789 89.999999999999943 0 ;
	setAttr ".bps" -type "matrix" 0.99999999999999967 2.2204460492503131e-016 9.4368957093138306e-016 6.9388939039072284e-018
		 9.4434775147824415e-016 0.99999717763800644 0.0023758611116653791 6.1629758220391547e-033
		 -3.8963393626541514e-016 -0.0023758611116653236 0.99999717763800633 -3.0814879110195774e-033
		 -17.999999999999797 -18.01186801611026 6.9904824437433826 0.99999999999999989;
createNode joint -n "leftClavacle" -p "chest";
	rename -uid "0A945317-4EE3-EB7F-2AB5-03AC8DC39B66";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.3665827156630368e-030 9.4663308626521417e-030 12.000000000000011 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -80.537677791974431 0 ;
	setAttr ".bps" -type "matrix" 0.98639392383214608 0.16439898730535832 2.0077149502461318e-016 0
		 -3.3306690738754721e-016 2.66265539376972e-030 1 0 0.1643989873053554 -0.98639392383214386 1.460156327451723e-016 0
		 12.000000000000053 2.8911095283544439e-014 7.8488903562216169e-015 1;
createNode joint -n "leftShoulder" -p "leftClavacle";
	rename -uid "E0200340-44B4-E4FC-33E2-6D827B1DC23D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.082762530298222 -5.7731597280508187e-015 4.2245981649692971e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 260.53767779197443 0 ;
	setAttr ".bps" -type "matrix" 2.7755575615629574e-016 -1 1.1102230246251554e-016 0
		 3.3306690738754696e-016 4.4373425918681914e-031 1 0 -1 -3.0114799542957371e-015 2.220446049250321e-016 0
		 18.000000000000025 0.99999999999998845 2.8528867454083857e-015 1;
createNode joint -n "leftUpperArm" -p "leftShoulder";
	rename -uid "22CC745B-4EB7-13A7-11E4-90A2A69841EE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.9999999999999982 -1.3305329060742126e-015 8.8746851837363811e-031 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 12.528807709151492 ;
	setAttr ".bps" -type "matrix" -4.9960036108132103e-016 -0.97618706018395307 0.21693045781865572 0
		 4.9960036108132103e-016 0.21693045781865558 0.97618706018395296 0 -0.99999999999999989 -1.1102230246251565e-016 2.2204460492503131e-016 0
		 18.000000000000021 -5.0000000000000133 4.6734799158084061e-015 1;
createNode joint -n "leftElbow" -p "leftUpperArm";
	rename -uid "C2099CCD-4838-926F-D6B9-91A53F63CD9C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 9.2195444572928746 1.9984014443252814e-015 1.136396591610222e-031 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 38.811384036758554 ;
	setAttr ".bps" -type "matrix" -1.3739009929736328e-015 -0.62469504755442351 0.78086880944303083 0
		 2.1233015345956143e-015 0.78086880944303283 0.62469504755442129 0 -0.999999999999998 -2.2204460492503131e-016 3.4694469519536142e-016 0
		 18.000000000000021 -13.999999999999989 1.9999999999999953 1;
createNode joint -n "leftWrist" -p "leftElbow";
	rename -uid "77D0BB3F-4F80-6EA5-945C-FC83CB2AC823";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.4031242374328468 5.329070518200753e-015 -1.0658141036401503e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -38.659808254090116 89.999999999999986 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000004 -5.5511151231257827e-016 -1.1102230246251565e-016 0
		 5.5511151231257758e-016 1.0000000000000002 -2.8310687127941464e-015 0 1.1102230246251654e-016 2.7200464103316343e-015 1.0000000000000002 0
		 18.000000000000007 -17.999999999999986 7.0000000000000107 1;
createNode transform -n "BasicMechGrouped";
	rename -uid "0177049C-4D00-9488-267F-B69328B58F5D";
	setAttr ".rp" -type "double3" 0 -13 1.0000000000000009 ;
	setAttr ".sp" -type "double3" 0 -13 1.0000000000000009 ;
createNode transform -n "Basic_Torso" -p "BasicMechGrouped";
	rename -uid "38150AC2-4E38-3106-AF69-2AB392F4167C";
	setAttr ".rp" -type "double3" 0 5 -1 ;
	setAttr ".sp" -type "double3" 0 5 -1 ;
createNode transform -n "base" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "5687955E-4776-20EC-B431-EB86A683BF5F";
	setAttr ".rp" -type "double3" 0 -17 -1 ;
	setAttr ".sp" -type "double3" 0 -17 -1 ;
createNode mesh -n "baseShape" -p "base";
	rename -uid "B20F1ADE-4BB5-52FE-2AD2-6FB2C764C8AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  -3 0 0 3 0 0 -3 0 0 3 0 0;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "backSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "D459BBFE-443F-93C5-3C8E-498F2B118BF5";
	setAttr ".rp" -type "double3" 0 -7.5 -11.5 ;
	setAttr ".sp" -type "double3" 0 -7.5 -11.5 ;
createNode mesh -n "backSideShape" -p "backSide";
	rename -uid "8C90774E-404D-A119-5C13-11B5F4956669";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[0:1]" -type "float3"  0 -4 0 0 -4 0;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "FE9902B7-4B05-B77A-9454-A09CCB9ADCF3";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "rightSideShape" -p "rightSide";
	rename -uid "E0DE3407-41EB-E6C2-CEAD-018F87BE1007";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -26 0 0 -26 0 0 -26 0 0 -26 
		0 0 -20 0 0 -20 0 0 -20 0 0 -20 0 0;
	setAttr -s 8 ".vt[0:7]"  13 -15 8 13 -15 -10 13 0 8 13 0 -10 10 0 8
		 10 0 -10 10 -15 8 10 -15 -10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightNozzle" -p "rightSide";
	rename -uid "6E3AAC7B-4D89-5CFF-9166-978B87C5BE70";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "rightNozzleShape" -p "rightNozzle";
	rename -uid "EA8B82A3-45D0-60B5-8430-1C8E0660895D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -22.205099 0 0 -25.999722 
		0 0 -24.102966 0 0 -27.897589 0 0 -24.102966 0 0 -27.897589 0 0 -22.205099 0 0 -25.999722 
		0 0;
	setAttr -s 8 ".vt[0:7]"  11.10254955 -15.94851685 -2.5 12.99986076 -16.58113861 -2.5
		 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5 12.051483154 -13.10255051 -5.5
		 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5 12.99986076 -16.58113861 -5.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "06C37FDC-4EEB-D5E2-F652-A48C6BCF8552";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "leftSideShape" -p "leftSide";
	rename -uid "1290F97F-4AEF-7373-D2B6-EFA594447191";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftNozzle" -p "leftSide";
	rename -uid "9E258720-4205-43CC-E9AA-B6ACF03285C0";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "leftNozzleShape" -p "leftNozzle";
	rename -uid "756ED554-4D39-2719-C8E7-15A011104AA8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "4EF332D3-40DD-D9F6-22A4-E29160E69659";
	setAttr ".rp" -type "double3" 0 -1.5 11.5 ;
	setAttr ".sp" -type "double3" 0 -1.5 11.5 ;
createNode mesh -n "frontSideShape" -p "frontSide";
	rename -uid "240FBBC4-4A64-C047-B4B0-A49AA879F1E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft1" -p "frontSide";
	rename -uid "DA5A97AE-4AB6-D808-737D-48A54CC76B56";
	setAttr ".rp" -type "double3" 0.99999999999999978 0 13 ;
	setAttr ".sp" -type "double3" 0.99999999999999978 0 13 ;
createNode mesh -n "frontLeftShape1" -p "frontLeft1";
	rename -uid "8981AC15-446A-F817-6A7C-2BA9D2BE092E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft2" -p "frontLeft1";
	rename -uid "EC447CBF-4B7A-68CE-5D43-3387389E5D50";
	setAttr ".rp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
createNode mesh -n "frontLeftShape2" -p "frontLeft2";
	rename -uid "C4FBF2AC-4AEF-1CD1-8BE8-D1AA68F5582A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[0]" -type "float3" -0.075828865 0 0 ;
	setAttr ".pt[2]" -type "float3" -0.38835922 8.8817842e-016 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "frontLeft2";
	rename -uid "8F4E599B-471A-D845-EC15-50AA27EF9227";
	setAttr ".rp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "F23315E2-4483-29C2-F6AB-4CA8057855AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "01186FCA-494E-584F-5C95-228E0CF99493";
	setAttr ".rp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8";
	rename -uid "D96C6124-421A-9B22-8816-5CB8F1492E86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "E59C2CDB-44D4-3824-3860-81A55AC3D276";
	setAttr ".rp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11";
	rename -uid "A4C79538-4683-2A6D-69DD-DF9B10E5FD1A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "B5364B96-4B40-02BE-DD0C-38A4F8490A73";
	setAttr ".rp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10";
	rename -uid "49274F99-4C74-D279-E5C0-E5A7D437EE54";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "C5F9ED8E-4ADB-B6FE-0679-38816DDCE302";
	setAttr ".rp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9";
	rename -uid "A6E870DD-4579-55CB-8072-7CB118EF453B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "A2942867-4BB8-B9AF-4995-66AA739642DC";
	setAttr ".rp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube15";
	rename -uid "A54AB347-497D-39BE-BBCC-77960C5B8ED5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  8.7661715 -10.002368 11.40912 
		6.8029666 -10.002368 9.7271099 8.7687492 -10.004949 11.415309 6.8055449 -10.004949 
		9.7332993 8.1979952 -8.6640949 12.045508 6.2347908 -8.6640949 10.3635 8.1954165 -8.6615152 
		12.03932 6.2322121 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "23A77EF9-4783-E752-C980-89A4DCF51C92";
	setAttr ".rp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube14";
	rename -uid "9462CCF8-4905-AC44-3704-0BAC8F8F7885";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.3451328 -9.0245857 12.22831 
		6.8835888 -9.0245857 9.9206047 7.3477111 -9.0271645 12.234498 6.886168 -9.0271654 
		9.9267941 6.7769575 -7.6863122 12.864698 6.3154144 -7.6863122 10.556993 6.7743788 
		-7.6837316 12.85851 6.3128357 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "5764FA3A-4D93-9542-2DBF-BABA02737ED2";
	setAttr ".rp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube13";
	rename -uid "94145998-4709-D2FF-3583-32AEB55EBF4D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.4257555 -8.0468016 12.421804 
		6.9642124 -8.0468016 10.114099 7.4283352 -8.0493822 12.427992 6.9667902 -8.0493822 
		10.120287 6.8575802 -6.708529 13.058191 6.3960366 -6.7085285 10.750487 6.8550024 
		-6.7059488 13.052003 6.3934579 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "5310FC2F-4553-4570-E395-86979156798A";
	setAttr ".rp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12";
	rename -uid "36B47657-41C2-2C16-9608-83A187855E82";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft3" -p "frontLeft1";
	rename -uid "547EA155-44AE-5549-0CE7-2AAF174DF833";
	setAttr ".rp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
createNode mesh -n "frontLeftShape3" -p "frontLeft3";
	rename -uid "3DD55ED4-46B0-DDAB-C522-869D1C2E88B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.076490089 0 0 -25.998447 
		0 0 -0.38087735 8.8817842e-016 0 -26.615368 0 0 0.76427841 0 0 -25.081852 0 0 1.381197 
		0 0 -24.464935 0 0;
	setAttr -s 8 ".vt[0:7]"  0.076159477 -13.017789841 13.38278198 12.99922371 -13.017789841 7.99813509
		 0.38461828 -3.050000191 14.1230793 13.30768394 -3.050000191 8.73843193 -0.38213921 -2.88960266 12.28287315
		 12.54092598 -2.88960218 6.89822626 -0.69059849 -12.85739136 11.54257584 12.23246765 -12.85739136 6.1579299;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "frontLeft3";
	rename -uid "18C07746-4664-7A3E-40AC-5D91CD75FCEC";
	setAttr ".rp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "6256EB2D-474C-CFDC-4C55-80BEA832B066";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.3908882 0 0 -22.160103 
		0 0 -7.5521345 0 0 -22.321348 0 0 -6.0478439 0 0 -20.817059 0 0 -5.8865986 0 0 -20.655813 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.69544411 -6.36505508 14.26905155 11.080051422 -6.36505508 11.19211197
		 3.77606726 -5.38727188 14.46254539 11.1606741 -5.38727188 11.38560486 3.023921967 -4.96803474 12.65740871
		 10.40852928 -4.96803474 9.58046818 2.94329929 -5.94581842 12.46391582 10.32790661 -5.94581842 9.38697529;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "1668818A-4BB5-549C-6343-30A3FEE4547F";
	setAttr ".rp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube8";
	rename -uid "F7A4CEAE-434C-3AE8-2A92-1BAFBD6E3A42";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -8.9145479 
		0 0 -7.3908882 0 0 -9.2370405 0 0 -5.8865986 0 0 -7.7327509 0 0 -5.564106 0 0 -7.4102583 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 4.45727396 -8.32062149 13.49744701
		 3.69544411 -6.36505508 14.26905155 4.61852026 -6.36505508 13.8844347 2.94329929 -5.94581842 12.46391582
		 3.86637545 -5.94581842 12.079298973 2.78205299 -7.90138435 12.076927185 3.70512915 -7.90138435 11.69231033;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "CC8ACE55-4EB5-FDE7-E18B-689B796CA117";
	setAttr ".rp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube11";
	rename -uid "1385D34A-4516-DBA4-9F76-DAA67AB24F03";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -13.969256 
		0 0 -8.3050871 0 0 -15.205947 0 0 -6.8007975 0 0 -13.701655 0 0 -5.564106 0 0 -12.464966 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 6.9846282 -11.25397968 11.76311302
		 4.15254354 -7.53839588 13.80608845 7.60297346 -10.47175312 11.68713665 3.40039873 -7.11915922 12.00095272064
		 6.85082769 -10.052515984 9.88199997 2.78205299 -7.90138435 12.076927185 6.23248291 -10.83474159 9.95797634;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "34D68EA8-4803-4E79-C904-A684F4BD506D";
	setAttr ".rp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube10";
	rename -uid "396814A8-4D2E-D144-C8D0-2A9FD7C569D4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -21.353872 0 0 -21.51512 
		0 0 -13.969266 0 0 -14.130511 0 0 -12.464975 0 0 -12.626222 0 0 -19.849583 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  10.67693615 -11.2539711 10.2246418 10.75755978 -10.27618885 10.4181366
		 6.98463297 -11.2539711 11.76311302 7.065255642 -10.27618885 11.95660782 6.23248768 -10.83473492 9.95797539
		 6.31311083 -9.85695076 10.15146923 9.92479134 -10.83473492 8.41950512 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "8522D05E-4841-D0FF-366B-F5A8D9869DBB";
	setAttr ".rp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube9";
	rename -uid "D38F2CC7-4397-B5F8-0172-CABCEE855294";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -19.668966 0 0 -21.51512 
		0 0 -20.313951 0 0 -22.160103 0 0 -18.809662 0 0 -20.655813 0 0 -18.164677 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  9.83448315 -10.27618885 10.8027544 10.75755978 -10.27618885 10.4181366
		 10.15697575 -6.36505508 11.57672882 11.080051422 -6.36505508 11.19211197 9.40483093 -5.94581842 9.77159309
		 10.32790661 -5.94581842 9.38697529 9.082338333 -9.85695076 8.99761677 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "19710E1A-469E-9039-EA80-70A346E1E157";
	setAttr ".rp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube15";
	rename -uid "D8143C51-4CEB-F78A-FE7C-67BD22386525";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.7661715 -10.002368 11.40912 
		-12.802967 -10.002368 9.7271099 -2.7687492 -10.004949 11.415309 -12.805545 -10.004949 
		9.7332993 -2.1979952 -8.6640949 12.045508 -12.234791 -8.6640949 10.3635 -2.1954165 
		-8.6615152 12.03932 -12.232212 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "97D92174-410F-884E-8D68-1385374252ED";
	setAttr ".rp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube14";
	rename -uid "6D077C63-4F2E-29B8-D239-BB905E4595F0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.3451328 -9.0245857 12.22831 
		-12.883589 -9.0245857 9.9206047 -1.3477111 -9.0271645 12.234498 -12.886168 -9.0271654 
		9.9267941 -0.77695751 -7.6863122 12.864698 -12.315414 -7.6863122 10.556993 -0.77437878 
		-7.6837316 12.85851 -12.312836 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "35D6485E-4C33-D003-E0E1-919F8F1C20D7";
	setAttr ".rp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube13";
	rename -uid "DDA181E2-4B40-A65D-2F5C-35AEF94596B8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.4257555 -8.0468016 12.421804 
		-12.964212 -8.0468016 10.114099 -1.4283352 -8.0493822 12.427992 -12.96679 -8.0493822 
		10.120287 -0.85758018 -6.708529 13.058191 -12.396036 -6.7085285 10.750487 -0.8550024 
		-6.7059488 13.052003 -12.393457 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "9BB721EF-4A61-C40A-CE46-D291E9E0E748";
	setAttr ".rp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube12";
	rename -uid "2F531F50-4CFC-CA61-6F5E-3486BFB98488";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9.0127592 0 0 -20.08967 
		0 0 -9.0179157 0 0 -20.094828 0 0 -7.8764067 0 0 -18.953318 0 0 -7.8712502 0 0 -18.94816 
		0 0;
	setAttr -s 8 ".vt[0:7]"  4.5063796 -7.074018478 13.61529922 10.044835091 -7.074018478 11.30759335
		 4.50895786 -7.066599846 13.62148666 10.047413826 -7.066599846 11.31378174 3.93820333 -5.72574568 12.2516861
		 9.47665882 -5.72574568 9.94398022 3.93562508 -5.73316574 12.2454977 9.47408009 -5.73316574 9.93779278;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontRight1" -p "frontSide";
	rename -uid "4597C8EE-4951-8DB2-268E-CF8D4274CC89";
	setAttr ".rp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
	setAttr ".sp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
createNode mesh -n "frontRightShape1" -p "frontRight1";
	rename -uid "65BFBE3B-40CA-3AE2-BC95-7FA64C9617C9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -6.4999886 -1.5 6.4999719 
		-7.5 -1.5 11.5 -6.4999886 -1.5 6.4999719 -7.5 -1.5 11.5 -5.3461356 -1.5 6.7307439 
		-6.3461475 -1.5 11.730772 -5.3461356 -1.5 6.7307439 -6.3461475 -1.5 11.730772;
	setAttr -s 8 ".vt[0:7]"  -6.5 -1.5 1.5 6.5 -1.5 1.5 -6.5 1.5 1.5 6.5 1.5 1.5
		 -6.5 1.5 -1.5 6.5 1.5 -1.5 -6.5 -1.5 -1.5 6.5 -1.5 -1.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "jawMid" -p "frontSide";
	rename -uid "89DD58D2-4EBA-FEA5-4620-779AD1CDDDB9";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "jawMidShape" -p "jawMid";
	rename -uid "BBF1F4BB-4B86-E8BB-08F6-36B6004C7BDD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube6" -p "jawMid";
	rename -uid "244AC02B-40FF-1E35-DA16-9E87AD3FC4B0";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "2D86932E-400D-DFAB-892B-19933F24CE01";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  30.304123 -18.700001 12.7 
		6.4461813 -18.700001 12.7 26.457947 -18.700001 12.7 2.6000054 -18.700001 12.7 24.327759 
		-18.700001 12.7 0.46981788 -18.700001 12.7 28.173935 -18.700001 12.7 4.3159933 -18.700001 
		12.7;
	setAttr -s 8 ".vt[0:7]"  -15.15206146 1.20867193 -5.36155796 -3.22309065 -4.54291534 -0.82075399
		 -13.22897339 5.76722908 -4.63955355 -1.30000269 0.01564152 -0.098749459 -12.16387939 5.76210356 -7.4441123
		 -0.23490894 0.01051604 -2.90330839 -14.086967468 1.20354652 -8.16611671 -2.15799665 -4.54804087 -3.62531281;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube5" -p "jawMid";
	rename -uid "5C15DB6A-40CF-A28D-8F27-77A9656A0CDB";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "39E10765-44E7-234D-22B2-F2BA48648DB0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethmid" -p "frontSide";
	rename -uid "298B6792-4E41-C0F5-614B-86BFC82313CE";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11 ;
createNode mesh -n "teethmidShape" -p "teethmid";
	rename -uid "FA1D085F-4E2E-5401-EC68-34BB66F7FDCB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethLeft" -p "teethmid";
	rename -uid "BDFBD9ED-489C-8419-DE9B-FE899DE6FE67";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethLeftShape" -p "teethLeft";
	rename -uid "85E2D2E6-4D90-2E60-D420-6A86E5506DE2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 3 ".pt";
	setAttr ".pt[1]" -type "float3" -1.4901161e-008 2.3436136 0 ;
	setAttr ".pt[3]" -type "float3" -1.4901161e-008 0 0 ;
	setAttr ".pt[7]" -type "float3" 0 2.3436136 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethRight" -p "teethmid";
	rename -uid "0BBCA9C6-4D6B-0074-7A2F-C0A378F8E720";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethRightShape" -p "teethRight";
	rename -uid "2DC87D9D-46ED-BB5C-9CC4-77A69DB4A2E3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1 0 0 -25.992804 2.3436136 
		0 -1 0 0 -25.992804 0 0 -0.44872528 0 0 -25.441528 0 0 -0.44872528 0 0 -25.441528 
		2.3436136 0;
	setAttr -s 8 ".vt[0:7]"  0.5 -19.5 11.5 12.99640179 -19.5 7.91671467
		 0.5 -12.5 11.5 12.99640179 -12.5 7.91671467 0.22436264 -12.5 10.53873825 12.72076416 -12.5 6.95545292
		 0.22436264 -19.5 10.53873825 12.72076416 -19.5 6.95545292;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Legs" -p "BasicMechGrouped";
	rename -uid "0E955BC1-4964-2BAE-1301-8C89317036A7";
	setAttr ".rp" -type "double3" 0 -25.5 -0.26776695251464844 ;
	setAttr ".sp" -type "double3" 0 -25.5 -0.26776695251464844 ;
createNode transform -n "hips" -p "|BasicMechGrouped|Basic_Legs";
	rename -uid "FF221B08-4D2C-B8C4-D3E0-6A9FB22B3148";
	setAttr ".rp" -type "double3" 0 -22 0 ;
	setAttr ".sp" -type "double3" 0 -22 0 ;
createNode mesh -n "hipsShape" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "77109185-401B-E0D8-99D4-9E8E8A364F90";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegLeft" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "42BBF380-45E3-7FA5-B896-15BC05AED86A";
	setAttr ".rp" -type "double3" 8 -22 0 ;
	setAttr ".sp" -type "double3" 8 -22 0 ;
createNode mesh -n "upperLegLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "5EA57348-475E-14C4-5813-288368A89BE5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegLeft" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "228A07EC-48C7-D1B5-C59A-CA93721A23C5";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "lowerLegLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft";
	rename -uid "2F94FD93-495B-848A-C8EC-43804F051E67";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeLeft" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "8C34651A-4401-EFE8-7775-988B897B7665";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "kneeLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft";
	rename -uid "DC8270F5-4118-DFD7-DFA5-80B685231A9C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegRight" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "9B70C2EC-4F64-7511-B019-008BE4895002";
	setAttr ".rp" -type "double3" -8 -22 0 ;
	setAttr ".sp" -type "double3" -8 -22 0 ;
createNode mesh -n "upperLegRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "5F05A66E-43B5-DCB9-ACE4-44B565BA7A15";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10 -22 0 -22 -22 0 -10 -22 
		0 -22 -22 0 -10 -22 0 -22 -22 0 -10 -22 0 -22 -22 0;
	setAttr -s 8 ".vt[0:7]"  5 -5 3 11 -5 3 5 3 3 11 3 3 5 3 -3 11 3 -3
		 5 -5 -3 11 -5 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegRight" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "F8F9085D-4AAD-48B5-138A-9A97137A60CF";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "lowerLegRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|lowerLegRight";
	rename -uid "CFEB7661-434F-3C86-89D4-ABBEC9BFE28F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9 -22 0 -23 -22 0 -9 -22 
		0 -23 -22 0 -9 -22 0 -23 -22 0 -9 -22 0 -23 -22 0;
	setAttr -s 8 ".vt[0:7]"  4.5 -11 3 11.5 -11 3 4.5 -5 3 11.5 -5 3 4.5 -5 -4
		 11.5 -5 -4 4.5 -11 -4 11.5 -11 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeRight" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "8CE62129-4288-F4FE-0934-6FBF0BAFC905";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "kneeRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|kneeRight";
	rename -uid "D4AE6C18-46FD-A0C5-74F7-55B272B7B015";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8 -22 0 -24 -22 0 -8 -22 
		0 -24 -22 0 -8 -22 0 -24 -22 0 -8 -22 0 -24 -22 0;
	setAttr -s 8 ".vt[0:7]"  4 -8.53553391 -2 12 -8.53553391 -2 4 -5 1.53553391
		 12 -5 1.53553391 4 -1.46446609 -2 12 -1.46446609 -2 4 -5 -5.53553391 12 -5 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft1" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "06472B54-43CE-28A3-B943-D9BDAB2EA2FA";
	setAttr ".rp" -type "double3" 12 -20 -1 ;
	setAttr ".sp" -type "double3" 12 -20 -1 ;
createNode mesh -n "panelLeftShape1" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1";
	rename -uid "DBFD30E4-4851-2760-62DE-C197E02BF610";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft2" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1";
	rename -uid "E32EE436-4527-A180-E176-B59AE16172E1";
	setAttr ".rp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
	setAttr ".sp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
createNode mesh -n "panelLeftShape2" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2";
	rename -uid "8B9BCF4D-47FC-F698-98BB-6DACEA0D097E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight1" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "F68B51CC-4D47-0E73-FF16-88A6A326CC32";
	setAttr ".rp" -type "double3" -12.000000000000004 -20 -1 ;
	setAttr ".sp" -type "double3" -12.000000000000004 -20 -1 ;
createNode mesh -n "panelRightShape1" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1";
	rename -uid "94705DCE-447C-DE62-7C01-7987E04AA3E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.462697 -20.005127 3.5 
		-17.462698 -20.005127 -6.5 -5.5743756 -20.264633 3.5 -15.574375 -20.264633 -6.5 -6.537303 
		-19.994873 4.5 -16.537302 -19.994873 -5.5 -8.4256248 -19.735367 4.5 -18.425625 -19.735367 
		-5.5;
	setAttr -s 8 ".vt[0:7]"  -5 -3.5 0.5 5 -3.5 0.5 -5 3.5 0.5 5 3.5 0.5
		 -5 3.5 -0.5 5 3.5 -0.5 -5 -3.5 -0.5 5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight2" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1";
	rename -uid "83C52A54-4F1F-904E-B235-1FA276B726FB";
	setAttr ".rp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
createNode mesh -n "panelRightShape2" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRight2";
	rename -uid "3E1D1CC0-4F0D-A0E9-D73E-D4A0EA4779E2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8.8865137 -19.740492 5.1672893 
		-9.7058802 -19.740492 8.4535904 -8 -20 3.5 -8.8193665 -20 6.7863011 -7.5479331 -20.26976 
		3.6497855 -8.3673 -20.26976 6.9360862 -8.4344463 -20.010252 5.3170743 -9.2538128 
		-20.010252 8.6033754;
	setAttr -s 8 ".vt[0:7]"  -3.5 -3.5 0.5 3.5 -3.5 0.5 -3.5 3.5 0.5 3.5 3.5 0.5
		 -3.5 3.5 -0.5 3.5 3.5 -0.5 -3.5 -3.5 -0.5 3.5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Right_Arm" -p "BasicMechGrouped";
	rename -uid "F3B002F6-4980-5CC0-B5B0-FB934E143A8D";
	setAttr ".rp" -type "double3" -12 -7.5 4 ;
	setAttr ".sp" -type "double3" -12 -7.5 4 ;
createNode transform -n "armRight1" -p "|BasicMechGrouped|Basic_Right_Arm";
	rename -uid "BD74FAE6-410F-1FC1-D37E-9DA76F5EBB34";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armRightShape1" -p "armRight1";
	rename -uid "F9CA20B8-4D87-D5E4-A152-A59C959A712A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmRight" -p "armRight1";
	rename -uid "55D5A9D1-42CF-366A-9909-E39182F42194";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmRightShape" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight";
	rename -uid "74D767FC-49D7-8FF4-1FA1-1A841375FD7D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRight" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight";
	rename -uid "124826B8-4A44-F82F-FF5C-BF8E08AF2D8B";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmRightShape" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight";
	rename -uid "7F70E54D-4C9C-07D2-9469-0CA2707F78A2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armRight2" -p "armRight1";
	rename -uid "5FB98D3E-4CD7-A706-BE56-3E8AC5A0CE51";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armRightShape2" -p "armRight2";
	rename -uid "0473C47A-4B4E-11A3-D0CB-0C8D83367D91";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Left_Arm" -p "BasicMechGrouped";
	rename -uid "DB84E1AA-438F-A86B-DDBC-73B4CCC1BA99";
	setAttr ".rp" -type "double3" 18 -7.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" 18 -7.5 4.0000000000000018 ;
createNode transform -n "armLeft1" -p "|BasicMechGrouped|Basic_Left_Arm";
	rename -uid "5368470F-49AB-1828-DF8C-DFAA22B8B83D";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".rpt" -type "double3" 36 0 2.2043642384652358e-015 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armLeftShape1" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "3E95B1B2-4291-169C-A142-8995F4B3B91B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmLeft" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "A85A8278-406C-58A5-0534-F4BB74D2C79C";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmLeftShape" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "DE602BD4-425D-C2F9-FB0D-A283FC4BDE5E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmLeft" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "40E8FABA-44ED-FEEC-C5DD-8B905F34991C";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmLeftShape" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft";
	rename -uid "09F812BB-4C5F-0F18-33FB-4397A9EA1DB5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armLeft2" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "3AC2A0E7-42FD-1EB0-DABF-8C97ADFE498B";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armLeftShape2" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeft2";
	rename -uid "9E99AC44-4840-37B9-5266-8FAC8A536702";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5
		 -13 -2 -5 -23 -5 -5 -13 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "BasicMechGrouped1";
	rename -uid "3976F5F6-4C7B-341A-4D60-AAB8DA883134";
	setAttr ".rp" -type "double3" 0 20.000007629394531 -0.7107086181640625 ;
	setAttr ".sp" -type "double3" 0 20.000007629394531 -0.7107086181640625 ;
createNode transform -n "Basic_Torso" -p "BasicMechGrouped1";
	rename -uid "1CEE4AAC-4FC9-F84D-BE9C-E8A579ACEC8D";
	setAttr ".rp" -type "double3" 0 20.830233573913574 1.0105056762695312 ;
	setAttr ".sp" -type "double3" 0 20.830233573913574 1.0105056762695312 ;
createNode transform -n "BasicMechGrouped1_Basic_Torso_base1" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "2209FA7F-40A5-2C7F-0650-14A3774CB749";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 20.830233573913574 1.0105056762695312 ;
	setAttr ".sp" -type "double3" 0 20.830233573913574 1.0105056762695312 ;
createNode mesh -n "BasicMechGrouped1_Basic_Torso_base1Shape" -p "BasicMechGrouped1_Basic_Torso_base1";
	rename -uid "4380B87F-463B-D4B4-B353-34B8A6D7E718";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr -av ".iog[0].og[10].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "BasicMechGrouped1_Basic_Torso_base1ShapeOrig" -p "BasicMechGrouped1_Basic_Torso_base1";
	rename -uid "37D9A6F1-42A9-3356-C92F-CEBCB8403DDA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 190 ".uvst[0].uvsp[0:189]" -type "float2" 0.375 0.75 0.625
		 0.75 0.375 1 0.625 1 0.37500003 0.31083804 0.62499994 0.31083804 0.625 0.47036067
		 0.60124928 0.5 0.39875072 0.5 0.375 0.47036073 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5
		 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1
		 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 202 ".uvst[1].uvsp[0:201]" -type "float2" 0.6217227 0.49701881
		 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.375 0.1 0.39875072 0.25 0.375 0.25 0.38120484
		 0.48778203 0.35888484 0.28088546 0.60124928 0.25 0.39875072 0.1 0.60124928 0.1 0.39875072
		 0 0.60124928 0 0.375 0.875 0.625 0.875 0.37004483 0.38433373 0.37996387 0.54022563
		 0.62237817 0.54761505 0.37589696 0.71209508 0.62452626 0.71343029 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr -s 202 ".uvst[2].uvsp[0:201]" -type "float2" 0.375 0 0.63006991
		 0.25000107 0.625 0 0.625 0.75 0.375 1 0.625 1 0.375 0.75 0.375 0.25 0.5953607 0.5
		 0.625 0.75 0.625 0.65000004 0.375 0.5 0.5953607 0.65000004 0.375 0.64999998 0.43583804
		 0.74999982 0.59536076 0.74999994 0.375 0.875 0.625 0.875 0.375 0.2 0.62905592 0.20000087
		 0.37467626 0.036138751 0.6257329 0.036138956 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5
		 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1
		 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr -s 205 ".uvst[3].uvsp[0:204]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.75 0.625 1 0.375 1 0.375 0.75 0.62899977 0.26061082 0.375 0.5 0.625
		 0.75 0.5953607 0.75 0.625 0.65000004 0.59536076 0.5 0.625 0.5 0.5953607 0.65000004
		 0.375 0.64999998 0.43583804 0.74999976 0.64705247 0.47337312 0.625 0.875 0.375 0.875
		 0.63802612 0.36699197 0.62819982 0.20848866 0.375 0.2 0.62557817 0.03767265 0.37474459
		 0.035461202 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0
		 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1
		 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr -s 192 ".uvst[4].uvsp[0:191]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0
		 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1
		 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr -s 190 ".uvst[5].uvsp[0:189]" -type "float2" 0.37500313 1.3590889e-006
		 0.62499768 1.7585622e-006 0.625 0.25 0.375 0.25 0.55288422 0.49038455 0.44711643
		 0.49038455 0.55288422 0.7596153 0.44711661 0.75961542 0.625 1 0.375 1 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr -s 195 ".uvst[6].uvsp[0:194]" -type "float2" 0.37500006 1.4187806e-006
		 0.62499994 1.7139804e-006 0.625 0.25 0.375 0.25 0.625 0.5 0.39279035 0.49073815 0.625
		 0.75 0.3935003 0.75743693 0.6226567 0.99932677 0.375 1 0 0 1 0 0.5 1 0.5 1 0 0 1
		 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25
		 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.49882835
		 0.99966335 0.50925016 0.7537185 0.50889516 0.49536908 0.5 0.25 0.5 1.5663804e-006;
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr -s 195 ".uvst[7].uvsp[0:194]" -type "float2" 0.37500006 1.3887484e-006
		 0.62499994 1.7361281e-006 0.625 0.25 0.375 0.25 0.60720968 0.49073815 0.375 0.5 0.60649967
		 0.75743681 0.375 0.75 0.625 1 0.37734324 0.99932677 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1
		 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1
		 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.5
		 1.5624382e-006 0.5 0.25 0.49110484 0.49536908 0.49074984 0.75371838 0.50117159 0.99966335;
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr -s 198 ".uvst[8].uvsp[0:197]" -type "float2" 0.36012951 4.4266707e-007
		 0.65064508 4.9997476e-007 0.56850398 0.25000054 0.40680742 0.25000066 0.56942832
		 0.49999955 0.40741879 0.49999931 0.63101113 0.75000042 0.37199745 0.75000048 0.62725317
		 0.99999857 0.37331015 0.99999827 0.65064508 4.9997476e-007 0.56850398 0.25000054
		 0.40680742 0.25000066 0.36012951 4.4266707e-007 0.37199745 0.75000048 0.63101113
		 0.75000042 0.62725317 0.99999857 0.37331015 0.99999827 0 0 1 0 0.5 1 0.5 1 0 0 1
		 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25
		 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr -s 201 ".uvst[9].uvsp[0:200]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.62499374 0.49999389 0.37500557 0.49999169 0.62499297 0.75000536
		 0.37500596 0.75000721 0.625 1 0.375 1 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1
		 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1
		 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.375 0.125 0.625 0.125 0.62499332 0.62499964
		 0.37500578 0.62499946 0.5 0 0.5 1 0.49999946 0.75000632 0.49999955 0.62499952 0.49999964
		 0.49999279 0.5 0.25 0.5 0.125 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5
		 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr -s 192 ".uvst[10].uvsp[0:191]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1
		 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1
		 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr -s 205 ".uvst[11].uvsp[0:204]" -type "float2" 0.625 0 0.64846659
		 0.0043042195 0.72517979 0.24964188 0.62606341 0.34817922 0.36960733 0.36637074 0.39229006
		 0.35648113 0.37326795 0.40686047 0.36242685 0.28458983 0.38207865 0.28329083 0.3803657
		 0.98767757 0.37383628 0.94214058 0.38569883 0.77725297 0.37463272 -1.5854432e-005
		 0.36301091 0.27097076 0.38424778 -1.5216793e-005 0.625 0.25 0.37283492 0.27034244
		 0.39533868 0.78538406 0.63669759 0.98896617 0.625 1 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1
		 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1
		 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.50462389 -7.6083966e-006
		 0.50268286 0.99383879 0.51601815 0.88717508 0.5 0 0.5 1 0.5 1 0.5 0 0.50917673 0.35233018
		 0.50353932 0.26664543;
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr -s 200 ".uvst[12].uvsp[0:199]" -type "float2" 0.375 0 0.60848302
		 1.3643329e-005 0.62962103 0.24999531 0.37136903 0.24999572 0.63103151 0.50000489
		 0.37088689 0.5000037 0.61736715 0.7499947 0.375 0.75 0.62660497 0.99999541 0.375
		 1 0.375 0 0.60848302 1.3643329e-005 0.62962103 0.24999531 0.37136903 0.24999572 0.63103151
		 0.50000489 0.37088689 0.5000037 0.61736715 0.7499947 0.375 0.75 0.62660497 0.99999541
		 0.375 1 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1
		 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75
		 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr -s 204 ".uvst[13].uvsp[0:203]" -type "float2" 0.375 0 0.6193431
		 -0.0060015284 0.625 0.25 0.37603259 0.38933682 0.625 0.5 0.37729686 0.34872243 0.61946499
		 0.7426793 0.375 0.75 0.87499684 -0.026260916 0.875 0.25 0.125 0 0.12548056 0.38161317
		 0.375 0 0.6193431 -0.0060015284 0.625 0.25 0.37603259 0.38933682 0.37729686 0.34872243
		 0.625 0.5 0.61946499 0.7426793 0.375 0.75 0.87499684 -0.026260916 0.875 0.25 0.125
		 0 0.12548056 0.38161317 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0
		 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1
		 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5
		 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr -s 200 ".uvst[14].uvsp[0:199]" -type "float2" 0.375 0 0.63427842
		 -0.00022588279 0.6201672 0.25008345 0.37909874 0.24983679 0.61878538 0.49991286 0.37866652
		 0.50018489 0.62929565 0.75010157 0.375 0.75 0.62499952 0.9999969 0.375 1 0.375 0
		 0.63427842 -0.00022588279 0.6201672 0.25008345 0.37909874 0.24983679 0.61878538 0.49991286
		 0.37866652 0.50018489 0.62929565 0.75010157 0.375 0.75 0.62499952 0.9999969 0.375
		 1 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25
		 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr -s 204 ".uvst[15].uvsp[0:203]" -type "float2" 0.37499043 -0.0081199696
		 0.62484956 0.0034516717 0.62577444 0.26952696 0.37500215 0.26897159 0.62576842 0.48323262
		 0.37500995 0.47689846 0.62480825 0.73139822 0.37497076 0.76523322 0.87481654 -0.023973627
		 0.87523842 0.27280691 0.12500358 -0.00066387275 0.12500387 0.26631272 0.37499043
		 -0.0081199696 0.62484956 0.0034516717 0.62577444 0.26952696 0.37500215 0.26897159
		 0.37500995 0.47689846 0.62576842 0.48323262 0.62480825 0.73139822 0.37497076 0.76523322
		 0.87481654 -0.023973627 0.87523842 0.27280691 0.12500358 -0.00066387275 0.12500387
		 0.26631272 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0
		 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1
		 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr -s 204 ".uvst[16].uvsp[0:203]" -type "float2" 0.37500504 4.8307976e-007
		 0.62499589 -0.062499121 0.6249947 0.31249923 0.37500593 0.24999952 0.62498635 0.43750143
		 0.37501699 0.50000072 0.62499225 0.81249857 0.37501249 0.74999923 0.87499684 -0.06249886
		 0.87499553 0.31249899 0.12500305 7.9726237e-007 0.1250027 0.24999928 0.37500504 4.8307976e-007
		 0.62499589 -0.062499121 0.6249947 0.31249923 0.37500593 0.24999952 0.37501699 0.50000072
		 0.62498635 0.43750143 0.62499225 0.81249857 0.37501249 0.74999923 0.87499684 -0.06249886
		 0.87499553 0.31249899 0.12500305 7.9726237e-007 0.1250027 0.24999928 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr -s 188 ".uvst[17].uvsp[0:187]" -type "float2" 0.625 0.25 0.375
		 0.25 0.625 0.5 0.375 0.5 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0 0 1 0 0.5 1
		 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr -s 188 ".uvst[18].uvsp[0:187]" -type "float2" 0.625 0.25 0.375
		 0.25 0.625 0.5 0.375 0.5 0.625 0.25 0.375 0.25 0.625 0.625 0.375 0.625 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr -s 188 ".uvst[19].uvsp[0:187]" -type "float2" 0.625 0.25 0.375
		 0.25 0.625 0.5 0.375 0.5 0.375 0.125 0.625 0.125 0.625 0.5 0.375 0.625 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr -s 188 ".uvst[20].uvsp[0:187]" -type "float2" 0.625 0.25 0.375
		 0.25 0.625 0.5 0.375 0.5 0.375 0.125 0.625 0.125 0.625 0.625 0.375 0.5 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr -s 198 ".uvst[21].uvsp[0:197]" -type "float2" 0.72517967 0.24964188
		 0.64846659 0.004304219 0.39695302 0.33819023 0.62643218 0.34612468 0.39238954 0.25400087
		 0.625 0.25 0.38463607 1.6688089e-006 0.38300347 0.24142848 0.625 0 0.3957372 0.78521168
		 0.38430235 0.99912709 0.625 1 0.62347412 0.94520628 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1
		 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1
		 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.50960565
		 0.86520898 0.50481802 8.3440443e-007 0.50465119 0.99956357 0.50869477 0.25200045
		 0.51169258 0.34215745;
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr -s 180 ".uvst[22].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr -s 180 ".uvst[23].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr -s 180 ".uvst[24].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr -s 180 ".uvst[25].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr -s 180 ".uvst[26].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr -s 180 ".uvst[27].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr -s 180 ".uvst[28].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr -s 180 ".uvst[29].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr -s 180 ".uvst[30].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr -s 197 ".uvst[31].uvsp[0:196]" -type "float2" 0.375 0 0.375 0.25
		 0.62568665 0.24361479 0.62391174 -0.023076372 0.375 0.5 0.60508215 0.47307804 0.375
		 0.75 0.60330725 0.80107921 0.375 1 0.62391186 1.03181994 0.125 0 0.125 0.25 0 0 1
		 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5
		 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.49945593
		 1.01590991 0.49945587 -0.011538186 0.50034332 0.2468074 0.49004108 0.48653901 0.48915362
		 0.77553964 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr -s 197 ".uvst[32].uvsp[0:196]" -type "float2" 0.375 0 0.62391174
		 -0.023076372 0.62568665 0.24361479 0.375 0.25 0.60508215 0.47307804 0.375 0.5 0.60330725
		 0.80107921 0.375 0.75 0.62391186 1.03181994 0.375 1 0.125 0 0.125 0.25 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.49945587
		 -0.011538186 0.49945593 1.01590991 0.48915362 0.77553964 0.49004108 0.48653901 0.50034332
		 0.2468074 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr -s 196 ".uvst[33].uvsp[0:195]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.38030076 0.48228449 0.625 0.75 0.38030076 0.76775652
		 0.625 1 0.375 1 0.875 0 0.875 0.25 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0
		 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0
		 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.375 0.125 0.625 0.125 0.75 0.375 0.38030076
		 0.6250205 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75
		 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr -s 196 ".uvst[34].uvsp[0:195]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.38030076 0.48228449 0.625 0.5 0.38030076 0.76775652 0.625 0.75
		 0.375 1 0.625 1 0.875 0.25 0.875 0 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0
		 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0
		 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.375 0.125 0.38030076 0.6250205 0.75 0.375
		 0.625 0.125 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75
		 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 314 ".vt";
	setAttr ".vt[0:165]"  -10 -19 6.56282711 -9.99999905 -15 2.12642479 9.99999905 -14.99999619 2.12642431
		 -10 -19.000007629395 -10 10 -19.00001335144 -10 -9 0 -13 9 0 -13 -9 -15 -13 9 -15 -13
		 -13 -15 7.70090914 -13 -15 -10 -12.99999428 0 7.99998569 -13 0 -10 -10 0 6.026508808
		 13 -15 7.70090914 13 -15 -10 12.99999428 0 7.99998569 13 0 -10 10 0 6.026508808 11.10254955 -15.94851685 -2.5
		 12.99986076 -16.58113861 -2.5 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5
		 12.051483154 -13.10255051 -5.5 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5
		 12.99986076 -16.58113861 -5.5 -2.066627502 -15 5.48226643 2.066627502 -14.99999809 5.48226643
		 -1 0 13 1 0 13 -1.85810363 0 10.11538601 1.85810351 0 10.11538601 -1.85810363 -9.52088261 10.11538601
		 1.85810351 -9.52088165 10.11538601 10 -9.52088165 6.026508808 -10 -9.52088261 6.026508808
		 -3.5 -23.63844299 11.91782761 3.5 -23.63844299 11.91782761 -1.30000257 -18.70000076 12.69999981
		 1.30000257 -18.70000076 12.69999981 -1.30000257 -18.23069382 9.73693466 1.30000257 -18.23069382 9.73693466
		 -3.5 -23.16913605 8.95476151 3.5 -23.16913605 8.95476151 -5.19458866 -18.86341095 9.87556839
		 5.19458866 -18.86341095 9.87556934 -5.53147793 -12.49999905 10.52668285 5.53147793 -12.49999905 10.52668285
		 -5.45742893 -12.5 9.53720474 5.45742893 -12.5 9.53720474 -5.12768555 -18.86341095 8.88609219
		 5.12768555 -18.86341095 8.88609219 -11.10254955 -15.94851685 -2.5 -12.99986076 -16.58113861 -2.5
		 -12.051483154 -13.10255051 -2.5 -13.94879436 -13.73517227 -2.5 -12.051483154 -13.10255051 -5.5
		 -13.94879436 -13.73517227 -5.5 -11.10254955 -15.94851685 -5.5 -12.99986076 -16.58113861 -5.5
		 12.99922371 -12.83167171 8.19139481 13.30768394 -3.050000429 8.73843193 12.99999237 -3 7.99997997
		 12.99999905 -12.83167171 7.99999666 4.61852026 -5.72479725 14.44290066 10.15697575 -5.72479725 12.13519573
		 3.77606726 -4.74701405 15.021012306 11.1606741 -4.74701405 11.94407177 3.023921967 -4.32777691 12.94467926
		 10.40852928 -4.32777691 9.86773872 3.86637545 -5.30556107 12.36656857 9.40483093 -5.30556059 10.05886364
		 3.53419828 -7.68036366 14.44053078 4.49436903 -7.19515514 14.15314579 2.78205299 -7.26112652 12.36419773
		 3.74222422 -6.77591848 12.076813698 6.9846282 -10.61372185 12.32157993 7.36878347 -9.63288403 12.38930988
		 6.61663771 -9.21364689 10.31297684 6.23248291 -10.19448376 10.24524689 10.67693615 -10.61371326 10.78310871
		 9.83448315 -9.63593102 11.36122131 9.92479134 -10.19447708 8.70677567 9.082338333 -9.21669292 9.28488731
		 6.75969124 -9.35969067 12.56088161 9.80554485 -9.35969067 11.29176617 5.19799519 -8.018836975 11.33277893
		 9.2347908 -8.018836975 9.65077019 5.7072773 -8.38190651 13.22647572 9.88616753 -8.38190746 11.48526096
		 3.77695751 -7.041054249 12.15196896 9.31541443 -7.041054249 9.84426308 4.42833519 -7.40412426 13.98645878
		 9.9667902 -7.40412426 11.67875385 3.85758018 -6.063271046 12.34546185 9.39603615 -6.063270569 10.037757874
		 4.50895786 -6.42634201 14.17995358 10.047413826 -6.42634201 11.87224865 3.93820333 -5.085487843 12.53895664
		 9.47665882 -5.085487843 10.23125076 -12.99922371 -12.83167171 8.19139481 -13.30768394 -3.050000191 8.73843193
		 -12.99999237 -3 7.99997997 -12.99999905 -12.83167171 7.99999666 15.15206146 -17.49132919 7.33844185
		 13.22897339 -12.93277264 8.060445786 12.16387939 -12.93789768 5.25588751 14.086967468 -17.49645615 4.53388405
		 -15.15206146 -17.49132919 7.33844185 -13.22897339 -12.93277264 8.060445786 -12.16387939 -12.93789768 5.25588751
		 -14.086967468 -17.49645615 4.53388405 11.6134901 -17.15638733 7.28728104 12.85375309 -12.5 7.91671467
		 12.58114052 -12.5 6.95545292 11.36718273 -17.15638733 6.32601929 -11.6134901 -17.15638733 7.28728104
		 -12.85375309 -12.5 7.91671467 -12.58114052 -12.5 6.95545292 -11.36718273 -17.15638733 6.32601929
		 10 -19.000009536743 6.56282711 -1 -3 13 1 -2.99999857 13 -4.61852026 -5.72479725 14.44290066
		 -10.15697575 -5.72479725 12.13519573 -11.1606741 -4.74701405 11.94407177 -3.77606726 -4.74701405 15.021012306
		 -10.40852928 -4.32777691 9.86773872 -3.023921967 -4.32777691 12.94467926 -9.40483093 -5.30556059 10.05886364
		 -3.86637545 -5.30556107 12.36656857 -3.53419828 -7.68036366 14.44053078 -4.49436903 -7.19515514 14.15314579
		 -3.74222422 -6.77591848 12.076813698 -2.78205299 -7.26112652 12.36419773 -6.9846282 -10.61372185 12.32157993
		 -7.36878347 -9.63288403 12.38930988 -6.61663771 -9.21364689 10.31297684 -6.23248291 -10.19448376 10.24524689
		 -10.67693615 -10.61371326 10.78310871 -9.83448315 -9.63593102 11.36122131 -9.082338333 -9.21669292 9.28488731
		 -9.92479134 -10.19447708 8.70677567 -9.80554485 -9.35969067 11.29176617 -6.75969124 -9.35969067 12.56088161
		 -9.2347908 -8.018836975 9.65077019 -5.19799519 -8.018836975 11.33277893 -9.88616753 -8.38190746 11.48526096
		 -5.7072773 -8.38190651 13.22647572 -9.31412506 -7.044764042 9.8411684 -3.77566814 -7.044764042 12.14887524
		 -4.42704535 -7.40783405 13.98336506 -9.96550179 -7.40783405 11.67566013 -9.39603615 -6.063270569 10.037757874
		 -3.85629129 -6.066981316 12.34236813 -4.5076685 -6.43005133 14.17685986 -10.046124458 -6.43005133 11.86915493
		 -9.47536945 -5.089198112 10.22815704 -3.93820333 -5.085487843 12.53895664 -3.5 -24.33954811 12.061686516
		 3.5 -24.33954811 12.061686516 1.30000257 -19.68410873 14.32386112 -1.30000257 -19.68410873 14.32386112
		 -3.5 -23.5110054 8.9006176 3.5 -23.5110054 8.9006176;
	setAttr ".vt[166:313]" -0.99641889 -3.050000668 13.71850109 1.0022703409 -3.050000668 13.71893406
		 -10 -15 -8.099942207 -8.099942207 -15 -10 -10 -9.52088261 -8.099942207 -8.099942207 -9.52088261 -10
		 -8.099942207 0 -10 -10 0 -8.099942207 8.099942207 -15 -10 10 -14.99999905 -8.099942207
		 10 -9.52088261 -8.099942207 8.099942207 -9.52088261 -10 8.099942207 0 -10 10 0 -8.099942207
		 -11.93161964 -17.31902313 7.31981564 -11.93161964 -17.31902695 -10.079131126 -9.42501545 -17.31902695 -11.86593819
		 9.42501545 -17.31903076 -11.86593819 11.93161964 -17.31903076 -10.079131126 11.93161964 -17.31902695 7.31981564
		 11.41285324 -0.027551472 -11.86593819 11.41285324 -14.89738369 -11.86593819 11.03839016 -16.64541054 -11.30424309
		 -11.41285324 -0.027551472 -11.86593819 -11.41285324 -14.89738369 -11.86593819 -11.03839016 -16.64540863 -11.30424309
		 -13 -2.99999976 -10 -11.41285324 -3.0015177727 -11.86593914 -9 -3 -13.000000953674
		 9 -3 -13.000000953674 11.41285324 -3.0015177727 -11.86593914 13 -2.99999976 -10 -13 -12.83167171 -10
		 -11.41285324 -12.74787235 -11.86593819 -9 -12.83167171 -13 9 -12.83167171 -13 11.41285324 -12.74787235 -11.86593819
		 13 -12.83167171 -10 0.49890172 -12.83167171 13.36925125 -0.49956685 -12.83167171 13.36923885
		 0.50275213 -3.54834914 13.88401508 -0.49571684 -3.54912233 13.88405514 0.49925989 -12.83167171 11.5923624
		 -0.49920872 -12.83167171 11.59237766 -5.35646105 -22.046480179 9.56723404 5.35646105 -22.046487808 9.56723404
		 -10.185462 -15 7.70091105 -10.18546104 -12.83167171 7.92442226 -6.39112616 -21.0060977936 10.11073971
		 10.185462 -15 7.70091105 10.18546104 -12.83167171 7.92442226 6.39112616 -21.0061016083 10.11073971
		 0.39116865 -12.83167171 10.027101517 -0.39112854 -12.83167171 10.027104378 6.71670723 -9.4833746 12.45772076
		 9.76256084 -9.4833746 11.18860531 5.15501118 -8.1425209 11.22961807 9.19180679 -8.1425209 9.54760933
		 5.66429329 -8.50559044 13.12331676 9.84318352 -8.50559139 11.38210106 3.73397398 -7.16473818 12.048810005
		 9.27243042 -7.16473818 9.74110317 4.38535118 -7.52780819 13.88329792 9.92380619 -7.52780819 11.57559299
		 3.81459665 -6.18695498 12.24230099 9.35305214 -6.1869545 9.93459702 4.46597385 -6.55002594 14.076792717
		 10.0044298172 -6.55002594 11.76908779 3.8952198 -5.20917177 12.43579578 9.43367481 -5.20917177 10.1280899
		 -6.71670723 -9.4833746 12.45772076 -9.76256084 -9.4833746 11.18860531 -9.19180679 -8.1425209 9.54760933
		 -5.15501118 -8.1425209 11.22961807 -5.6644125 -8.50586891 13.12360096 -9.84330368 -8.50586987 11.38238621
		 -9.27126122 -7.16872644 9.73829365 -3.73280358 -7.16872644 12.046000481 -4.3840642 -7.53144836 13.88012028
		 -9.92246151 -7.53130817 11.57227325 -9.35305977 -6.18674421 9.93434429 -3.81337404 -6.19059515 12.23909664
		 -4.46456385 -6.55352497 14.07349968 -10.0030794144 -6.55366516 11.76593781 -9.43226051 -5.21281195 10.12496662
		 -3.89503431 -5.20896149 12.43562317 -5.36086655 -15.88647175 10.52668285 5.36086655 -15.88647175 10.52668285
		 12.22564507 -15.032959938 7.91671467 11.96635342 -15.032959938 6.95545292 5.29043674 -15.88647175 9.53720474
		 -5.29043674 -15.88647175 9.53720474 -11.96635342 -15.032959938 6.95545292 -12.22564507 -15.032959938 7.91671467
		 0 -19.5 10.96953487 0 -19.5 9.96953678 0 -16.20476532 10.62873363 0 -12.5 10.62873363
		 0 -12.49999905 11.62873363 0 -16.20476532 11.62873363 -9.47807503 -17.31902504 7.31981564
		 9.47807503 -17.31902695 7.31981564 -6.4882741 -19.16256142 8.71527767 6.4882741 -19.16256332 8.71527767
		 -9.94521809 -14.8853159 6.62630224 -9.94521713 -14.029500008 7.10106039 9.94521809 -14.8853159 6.62630224
		 9.94521713 -14.029500008 7.10106039 -9.2545166 -17.29934311 6.86615467 9.2545166 -17.29934311 6.86615467
		 -6.33523607 -18.5134964 6.91899204 6.33523607 -18.51349258 6.91899014 0 -16 11.62873363
		 5.36086655 -15.68170547 10.52668285 -5.36086655 -15.68170547 10.52668285 -12.22564507 -14.82819366 7.91671467
		 -11.96635342 -14.82819366 6.95545292 -5.29043674 -15.68170547 9.53720474 0 -16 10.62873363
		 5.29043674 -15.68170547 9.53720474 11.96635342 -14.82819366 6.95545292 12.22564507 -14.82819366 7.91671467
		 9.32603073 -22.14331818 10.34613228 7.26448822 -17.39481926 11.098219872 6.73194122 -17.16272736 8.21440887
		 8.79348373 -21.91122818 7.46232033 -9.32603073 -22.14331818 10.34613228 -8.79348373 -21.91122818 7.46232033
		 -6.73194122 -17.16272736 8.21440887 -7.26448822 -17.39481926 11.098219872 6.74906254 -12.83167171 11.76489067
		 6.7496295 -12.83167171 9.79617977 5.28831482 -12.83167171 8.97576141 -5.28829479 -12.83167171 8.97576332
		 -6.74960375 -12.83167171 9.7961874 -6.74939537 -12.83167171 11.76488495 -7.15205145 -3.050000429 12.21303368
		 -6.99999619 -3 11.48455811 -6.99999714 0 11.48456001 -6.26828623 0 9.21302605 -6.27162552 -9.52088261 9.21017456
		 -5.94319296 -15 3.84246659 5.94319344 -14.99999714 3.84246635 6.27162361 -9.52088165 9.21017647
		 6.2682848 0 9.213027 6.99999714 0 11.48456001 6.99999619 -2.99999928 11.48455811
		 7.15497732 -3.050000668 12.21325111;
	setAttr -s 569 ".ed";
	setAttr ".ed[0:165]"  0 121 0 1 2 0 3 4 0 1 168 0 2 175 0 3 0 0 4 121 0 5 6 0
		 7 8 0 5 194 0 6 195 0 7 182 0 8 183 0 9 10 0 11 12 0 13 173 0 9 104 0 10 198 0 11 13 0
		 0 180 0 3 181 0 14 15 0 16 17 0 18 179 0 14 64 0 15 203 0 16 18 0 18 35 0 121 185 0
		 4 184 0 19 20 0 21 22 0 23 24 0 25 26 0 19 21 0 20 22 0 21 23 0 22 24 0 23 25 0 24 26 0
		 25 19 0 26 20 0 27 28 0 29 30 0 31 32 0 33 34 0 122 29 0 123 30 0 29 31 0 30 32 0
		 31 33 0 32 34 0 33 27 0 34 28 0 28 308 0 30 311 0 32 310 0 34 309 0 63 16 0 35 2 0
		 1 307 0 11 304 0 13 305 0 36 306 0 103 11 0 36 1 0 39 40 1 41 42 0 43 44 1 37 39 0
		 38 40 0 39 41 0 40 42 0 41 43 0 42 44 0 43 37 0 44 38 0 45 260 0 47 264 0 49 263 0
		 51 261 0 45 252 0 46 253 0 47 49 0 48 50 0 49 283 0 50 285 0 51 45 0 52 46 0 53 54 0
		 55 56 0 57 58 0 59 60 0 53 55 0 54 56 0 55 57 0 56 58 0 57 59 0 58 60 0 59 53 0 60 54 0
		 61 62 0 62 63 0 63 64 0 64 61 0 65 66 0 67 68 0 69 70 0 71 72 0 65 67 0 66 68 0 67 69 0
		 68 70 0 69 71 0 70 72 0 71 65 0 72 66 0 73 74 0 75 76 0 73 67 0 74 65 0 69 75 0 71 76 0
		 75 73 0 76 74 0 73 77 0 74 78 0 76 79 0 75 80 0 77 78 0 78 79 0 79 80 0 80 77 0 81 82 0
		 83 84 0 81 77 0 82 78 0 80 83 0 79 84 0 83 81 0 84 82 0 82 66 0 81 68 0 72 84 0 70 83 0
		 85 86 0 87 88 0 85 87 0 86 88 0 89 90 0 91 92 0 89 91 0 90 92 0 93 94 0 95 96 0 93 95 0
		 94 96 0 97 98 0 99 100 0 97 99 0 98 100 0 101 102 0 102 103 0 103 104 0 104 101 0
		 105 288 0;
	setAttr ".ed[166:331]" 106 289 0 107 290 0 108 291 0 105 106 0 106 107 0 107 108 0
		 108 105 0 109 292 0 110 295 0 111 294 0 112 293 0 109 110 0 110 111 0 111 112 0 112 109 0
		 46 113 0 48 114 0 50 115 0 52 116 0 113 254 0 114 115 0 115 286 0 116 113 0 45 117 0
		 47 118 0 49 119 0 51 120 0 117 259 0 118 119 0 119 282 0 120 117 0 6 186 0 8 187 0
		 7 190 0 5 189 0 13 36 0 122 123 0 103 303 0 124 125 0 125 126 0 127 126 0 124 127 0
		 126 128 0 129 128 0 127 129 0 128 130 0 131 130 0 129 131 0 130 125 0 131 124 0 132 133 0
		 133 124 0 132 127 0 131 134 0 135 134 0 129 135 0 134 133 0 135 132 0 132 136 0 136 137 0
		 133 137 0 137 138 0 134 138 0 138 139 0 135 139 0 139 136 0 140 141 0 141 137 0 140 136 0
		 138 142 0 143 142 0 139 143 0 142 141 0 143 140 0 140 126 0 141 125 0 128 143 0 130 142 0
		 145 144 0 144 146 0 147 146 0 145 147 0 149 148 0 148 150 0 151 150 0 149 151 0 152 153 0
		 153 154 0 155 154 0 152 155 0 156 157 0 157 158 0 159 158 0 156 159 0 37 160 1 38 161 1
		 160 161 0 40 162 0 161 162 0 39 163 0 163 162 0 160 163 0 43 164 0 44 165 0 164 165 0
		 165 161 0 164 160 0 166 302 0 167 313 0 167 206 0 123 312 0 166 122 1 123 167 1 166 167 0
		 169 174 0 169 168 0 172 178 0 172 5 1 173 12 1 169 171 0 171 170 1 170 168 0 171 172 0
		 172 173 0 173 170 0 36 170 1 175 174 0 178 6 1 17 179 1 175 176 0 176 177 1 177 174 0
		 176 179 0 179 178 0 178 177 0 176 35 1 171 177 1 180 9 0 181 10 0 182 3 0 183 4 0
		 184 15 0 185 14 0 180 181 1 181 191 1 182 183 1 183 188 1 184 185 1 186 17 0 187 15 0
		 188 184 1 186 196 1 187 188 1 189 12 0 190 10 0 191 182 1 189 193 1 190 191 1 192 12 0
		 193 199 1 194 200 0 195 201 0 196 202 1 197 17 0 103 192 1 192 193 1;
	setAttr ".ed[332:497]" 193 194 1 194 195 1 195 196 1 196 197 1 197 63 1 198 192 0
		 199 190 1 200 7 0 201 8 0 202 187 1 203 197 0 104 198 1 198 199 1 199 200 1 200 201 1
		 201 202 1 202 203 1 203 64 1 207 166 0 207 206 0 208 297 1 209 300 1 204 205 0 205 209 1
		 209 208 1 208 204 1 204 206 0 207 205 0 204 296 0 101 301 0 0 210 0 121 211 0 210 211 0
		 9 212 1 104 213 0 212 213 0 180 214 0 210 214 0 14 215 1 64 216 0 215 216 0 185 217 0
		 211 217 0 214 268 1 217 269 1 208 218 1 218 298 1 209 219 1 219 299 1 219 218 1 213 216 0
		 214 217 0 85 220 0 86 221 0 220 221 0 87 222 0 88 223 0 222 223 0 220 222 0 221 223 0
		 89 224 0 90 225 0 224 225 0 91 226 0 92 227 0 226 227 0 224 226 0 225 227 0 93 228 0
		 94 229 0 228 229 0 95 230 0 96 231 0 230 231 0 228 230 0 229 231 0 97 232 0 98 233 0
		 232 233 0 99 234 0 100 235 0 234 235 0 232 234 0 233 235 0 145 236 0 144 237 0 236 237 0
		 146 238 0 237 238 0 147 239 0 239 238 0 236 239 0 149 240 0 148 241 0 240 241 0 150 242 0
		 241 242 0 151 243 0 243 242 0 240 243 0 152 244 0 153 245 0 244 245 0 154 246 0 245 246 0
		 155 247 0 247 246 0 244 247 0 156 248 0 157 249 0 248 249 0 158 250 0 249 250 0 159 251 0
		 251 250 0 248 251 0 280 47 0 279 48 0 287 114 0 255 116 0 256 52 0 257 51 0 258 120 0
		 281 118 0 252 265 0 253 254 0 254 255 0 255 256 0 256 262 0 257 258 0 258 259 0 259 252 0
		 260 46 0 261 52 0 262 257 0 263 50 0 264 48 0 265 253 0 260 261 0 261 262 0 284 263 0
		 263 264 0 264 278 0 265 260 0 266 212 0 267 215 0 180 266 0 267 185 0 268 266 0 269 267 0
		 268 269 0 212 270 1 213 271 0 270 271 0 215 272 1 270 272 1 216 273 0 272 273 0 271 273 0
		 266 274 0 274 270 0 267 275 0 274 275 0 275 272 0 268 276 1 276 274 0;
	setAttr ".ed[498:568]" 269 277 1 276 277 0 277 275 0 278 279 0 280 278 0 281 280 0
		 282 281 0 283 282 0 284 283 0 285 284 0 286 285 0 287 286 0 279 287 0 252 257 0 265 262 0
		 253 256 0 279 285 0 278 284 0 280 283 0 288 38 0 289 40 0 290 42 0 291 44 0 288 289 1
		 289 290 1 290 291 1 291 288 1 292 37 0 293 43 0 294 41 0 295 39 0 292 293 1 293 294 1
		 294 295 1 295 292 1 296 61 0 297 64 1 298 216 1 299 213 1 300 104 1 301 205 0 296 297 1
		 297 298 1 298 299 1 299 300 1 300 301 1 302 102 0 303 122 0 304 29 0 305 31 0 306 33 0
		 307 27 0 308 2 0 309 35 0 310 18 0 311 16 0 312 63 0 313 62 0 302 303 1 303 304 1
		 304 305 1 305 306 1 306 307 1 307 308 1 308 309 1 309 310 1 310 311 1 311 312 1 312 313 1
		 302 301 1 313 296 1;
	setAttr -s 281 -ch 1130 ".fc[0:280]" -type "polyFaces" 
		f 6 1 4 292 -281 281 -4
		mu 0 6 4 5 6 7 8 9
		f 4 2 6 -1 -6
		mu 0 4 0 1 3 2
		f 4 346 340 -9 -340
		mu 1 4 20 21 1 2
		f 4 311 306 -3 -306
		mu 1 4 15 16 3 4
		f 4 16 343 -18 -14
		mu 2 4 0 20 21 2
		f 4 18 15 284 -15
		mu 2 4 7 11 8 1
		f 4 19 309 -21 5
		mu 2 4 6 16 17 3
		f 4 21 25 349 -25
		mu 3 4 0 1 23 24
		f 4 22 294 -24 -27
		mu 3 4 2 7 12 8
		f 4 -7 29 313 -29
		mu 3 4 6 3 18 19
		f 4 30 35 -32 -35
		mu 4 4 0 1 2 3
		f 4 31 37 -33 -37
		mu 4 4 3 2 4 5
		f 4 32 39 -34 -39
		mu 4 4 5 4 6 7
		f 4 33 41 -31 -41
		mu 4 4 7 6 8 9
		f 4 -42 -40 -38 -36
		mu 4 4 1 10 11 2
		f 4 202 47 -44 -47
		mu 5 4 0 1 2 3
		f 4 43 49 -45 -49
		mu 5 4 3 2 4 5
		f 4 44 51 -46 -51
		mu 5 4 5 4 6 7
		f 4 45 53 -43 -53
		mu 5 4 7 6 8 9
		f 4 565 554 58 -554
		mu 6 4 193 194 1 2
		f 4 564 553 26 -553
		mu 6 4 192 193 2 4
		f 4 563 552 27 -552
		mu 6 4 191 192 4 6
		f 4 562 551 59 -551
		mu 6 4 190 191 6 8
		f 4 203 557 -62 -65
		mu 7 4 0 190 191 3
		f 4 61 558 -63 -19
		mu 7 4 3 191 192 5
		f 4 62 559 -64 -202
		mu 7 4 5 192 193 7
		f 4 63 560 -61 -66
		mu 7 4 7 193 194 9
		f 4 262 264 -267 -268
		mu 8 4 13 10 11 12
		f 4 66 72 -68 -72
		mu 8 4 3 2 4 5
		f 4 67 74 -69 -74
		mu 8 4 5 4 6 7
		f 4 270 271 -263 -273
		mu 8 4 14 15 16 17
		f 4 474 501 449 -469
		mu 9 4 181 182 173 2
		f 4 473 468 84 -468
		mu 9 4 180 181 2 4
		f 4 472 467 86 507
		mu 9 4 179 180 4 174
		f 4 470 465 88 -465
		mu 9 4 177 178 6 8
		f 4 93 90 -95 -90
		mu 10 4 0 1 2 3
		f 4 95 91 -97 -91
		mu 10 4 1 4 5 2
		f 4 97 92 -99 -92
		mu 10 4 4 6 7 5
		f 4 99 89 -101 -93
		mu 10 4 6 8 9 7
		f 4 94 96 98 100
		mu 10 4 3 2 10 11
		f 4 566 555 102 -555
		mu 11 4 203 204 15 3
		f 4 -105 -104 -103 -102
		mu 11 4 0 1 2 15
		f 4 105 110 -107 -110
		mu 12 4 0 1 2 3
		f 4 106 112 -108 -112
		mu 12 4 3 2 4 5
		f 4 107 114 -109 -114
		mu 12 4 5 4 6 7
		f 4 108 116 -106 -116
		mu 12 4 7 6 8 9
		f 4 117 120 109 -120
		mu 13 4 0 1 2 3
		f 4 113 122 -119 -122
		mu 13 4 5 4 6 7
		f 4 -125 -123 115 -121
		mu 13 4 1 8 9 2
		f 4 123 119 111 121
		mu 13 4 10 0 3 11
		f 4 125 129 -127 -118
		mu 14 4 0 1 2 3
		f 4 126 130 -128 124
		mu 14 4 3 2 4 5
		f 4 127 131 -129 118
		mu 14 4 5 4 6 7
		f 4 128 132 -126 -124
		mu 14 4 7 6 8 9
		f 4 133 136 -130 -136
		mu 15 4 0 1 2 3
		f 4 -132 138 -135 -138
		mu 15 4 5 4 6 7
		f 4 -141 -139 -131 -137
		mu 15 4 1 8 9 2
		f 4 139 135 -133 137
		mu 15 4 10 0 3 11
		f 4 -134 142 -111 -142
		mu 16 4 0 1 2 3
		f 4 -115 144 134 -144
		mu 16 4 5 4 6 7
		f 4 -140 -145 -113 -143
		mu 16 4 1 8 9 2
		f 4 140 141 -117 143
		mu 16 4 10 0 3 11
		f 4 145 148 -147 -148
		mu 17 4 1 0 2 3
		f 4 149 152 -151 -152
		mu 18 4 1 0 2 3
		f 4 153 156 -155 -156
		mu 19 4 1 0 2 3
		f 4 157 160 -159 -160
		mu 20 4 1 0 2 3
		f 4 556 -204 -163 -545
		mu 21 4 196 197 3 5
		f 4 161 162 163 164
		mu 21 4 8 5 0 1
		f 4 521 518 -71 -518
		mu 31 4 189 190 2 3
		f 4 522 519 -73 -519
		mu 31 4 190 191 5 2
		f 4 523 520 -75 -520
		mu 31 4 191 192 7 5
		f 4 524 517 -77 -521
		mu 31 4 192 188 9 7
		f 4 -172 -171 -170 -173
		mu 31 4 10 11 1 0
		f 4 532 525 69 -529
		mu 32 4 192 188 1 2
		f 4 531 528 71 -528
		mu 32 4 191 192 2 4
		f 4 530 527 73 -527
		mu 32 4 190 191 4 6
		f 4 529 526 75 -526
		mu 32 4 189 190 6 8
		f 4 180 177 178 179
		mu 32 4 10 0 3 11
		f 4 510 450 -183 -450
		mu 33 4 174 175 2 3
		f 4 182 186 -184 -85
		mu 33 4 3 2 4 5
		f 4 183 187 508 -87
		mu 33 4 5 4 176 177
		f 4 184 188 -182 -89
		mu 33 4 7 6 8 9
		f 4 509 -188 -187 -451
		mu 33 4 175 176 11 2
		f 4 503 448 190 -456
		mu 34 4 177 174 1 2
		f 4 83 191 -195 -191
		mu 34 4 1 4 5 2
		f 4 85 505 -196 -192
		mu 34 4 4 175 176 5
		f 4 87 189 -197 -193
		mu 34 4 6 8 9 7
		f 4 504 455 194 195
		mu 34 4 176 177 2 10
		f 4 -341 347 341 -199
		f 4 344 338 320 17
		f 4 -2 60 561 550
		f 4 207 206 -206 -205
		mu 12 4 10 13 12 11
		f 4 210 209 -209 -207
		mu 12 4 13 15 14 12
		f 4 213 212 -212 -210
		mu 12 4 15 17 16 14
		f 4 215 204 -215 -213
		mu 12 4 17 19 18 16
		f 4 218 -208 -218 -217
		mu 13 4 12 15 14 13
		f 4 221 220 -220 -214
		mu 13 4 16 19 18 17
		f 4 217 -216 219 222
		mu 13 4 13 14 21 20
		f 4 -222 -211 -219 -224
		mu 13 4 22 23 15 12
		f 4 216 226 -226 -225
		mu 14 4 10 13 12 11
		f 4 -223 228 -228 -227
		mu 14 4 13 15 14 12
		f 4 -221 230 -230 -229
		mu 14 4 15 17 16 14
		f 4 223 224 -232 -231
		mu 14 4 17 19 18 16
		f 4 234 225 -234 -233
		mu 15 4 12 15 14 13
		f 4 237 236 -236 229
		mu 15 4 16 19 18 17
		f 4 233 227 235 238
		mu 15 4 13 14 21 20
		f 4 -238 231 -235 -240
		mu 15 4 22 23 15 12
		f 4 241 205 -241 232
		mu 16 4 12 15 14 13
		f 4 243 -237 -243 211
		mu 16 4 16 19 18 17
		f 4 240 208 242 239
		mu 16 4 13 14 21 20
		f 4 -244 214 -242 -239
		mu 16 4 22 23 15 12
		f 4 247 246 -246 -245
		mu 17 4 5 7 6 4
		f 4 251 250 -250 -249
		mu 18 4 5 7 6 4
		f 4 255 254 -254 -253
		mu 19 4 4 7 6 5
		f 4 259 258 -258 -257
		mu 20 4 4 7 6 5
		f 4 70 263 -265 -262
		mu 8 4 1 2 11 10
		f 4 -67 265 266 -264
		mu 8 4 2 3 12 11
		f 4 -70 260 267 -266
		mu 8 4 3 0 13 12
		f 4 68 269 -271 -269
		mu 8 4 7 6 15 14
		f 4 76 261 -272 -270
		mu 8 4 6 8 16 15
		f 4 -76 268 272 -261
		mu 8 4 9 7 14 17
		f 4 279 -279 -203 -278
		mu 11 4 4 8 5 6
		f 4 -351 351 -276 -280
		mu 11 4 7 13 16 8
		f 4 -307 312 316 -30
		f 4 20 310 321 305
		f 4 -284 282 293 -8
		mu 1 4 8 6 10 0
		f 4 -282 285 286 287
		mu 2 4 15 9 10 12
		f 4 -287 288 289 290
		mu 1 4 5 11 6 7
		f 4 -291 -16 201 291
		mu 2 4 12 8 11 13
		f 4 -292 65 3 -288
		mu 2 4 12 13 14 15
		f 5 283 200 319 -285 -290
		mu 1 5 6 8 17 9 7
		f 4 -293 295 296 297
		mu 3 4 9 10 14 11
		f 4 -297 298 299 300
		mu 3 4 11 14 12 13
		f 4 301 -28 23 -299
		mu 3 4 14 15 8 12
		f 4 -60 -302 -296 -5
		mu 3 4 16 15 14 10
		f 4 -301 -283 -289 302
		mu 1 4 12 10 6 11
		f 4 -303 -286 280 -298
		mu 1 4 12 11 13 14
		f 5 -300 -295 -315 -198 -294
		mu 3 5 13 12 7 20 17
		f 4 -310 303 13 -305
		mu 2 4 17 16 4 5
		f 4 323 -311 304 -321
		f 4 8 12 -312 -12
		mu 1 4 2 1 16 15
		f 4 318 -313 -13 198
		f 4 -314 307 -22 -309
		mu 3 4 19 18 4 5
		f 4 -342 348 -26 -316
		f 4 -317 -319 315 -308
		f 4 345 339 199 -339
		f 4 -322 -324 -200 11
		f 4 -331 64 14 -325
		mu 2 4 19 18 7 1
		f 4 322 -332 324 -320
		f 4 9 -333 -323 -201
		f 4 7 10 -334 -10
		mu 1 4 8 0 19 18
		f 4 -335 -11 197 317
		f 4 -336 -318 314 -330
		f 4 -337 329 -23 -59
		mu 3 4 22 21 7 2
		f 4 -344 -164 330 -338
		mu 2 4 21 20 18 19
		f 4 331 325 -345 337
		f 4 332 326 -346 -326
		f 4 333 327 -347 -327
		mu 1 4 18 19 21 20
		f 4 -348 -328 334 328
		f 4 -349 -329 335 -343
		f 4 -350 342 336 103
		mu 3 4 24 23 21 22
		f 4 354 355 356 357
		mu 11 4 9 10 11 17
		f 4 -355 358 -352 359
		mu 11 4 12 14 16 13
		f 4 568 533 101 -556
		mu 11 4 204 196 0 15
		f 4 539 534 104 -534
		mu 11 4 197 198 18 19
		f 5 -360 350 273 567 538
		mu 21 5 6 7 4 196 194
		f 4 543 -362 -165 -538
		mu 21 4 193 195 11 12
		f 4 0 363 -365 -363
		mu 0 4 10 11 12 13
		mu 1 4 22 23 24 25
		mu 2 4 22 23 24 25
		mu 3 4 25 26 27 28
		mu 4 4 12 13 14 15
		mu 5 4 10 11 12 13
		mu 6 4 10 11 12 13
		mu 7 4 10 11 12 13
		mu 8 4 18 19 20 21
		mu 9 4 10 11 12 13
		mu 10 4 12 13 14 15
		mu 11 4 20 21 22 23
		mu 12 4 20 21 22 23
		mu 13 4 24 25 26 27
		mu 14 4 20 21 22 23
		mu 15 4 24 25 26 27
		mu 16 4 24 25 26 27
		mu 17 4 8 9 10 11
		mu 18 4 8 9 10 11
		mu 19 4 8 9 10 11
		mu 20 4 8 9 10 11
		mu 21 4 13 14 15 16
		mu 22 4 0 1 2 3
		mu 23 4 0 1 2 3
		mu 24 4 0 1 2 3
		mu 25 4 0 1 2 3
		mu 26 4 0 1 2 3
		mu 27 4 0 1 2 3
		mu 28 4 0 1 2 3
		mu 29 4 0 1 2 3
		mu 30 4 0 1 2 3
		mu 31 4 12 13 14 15
		mu 32 4 12 13 14 15
		mu 33 4 12 13 14 15
		mu 34 4 12 13 14 15
		f 4 -17 365 367 -367
		mu 0 4 14 15 16 17
		mu 1 4 26 27 28 29
		mu 2 4 26 27 28 29
		mu 3 4 29 30 31 32
		mu 4 4 16 17 18 19
		mu 5 4 14 15 16 17
		mu 6 4 14 15 16 17
		mu 7 4 14 15 16 17
		mu 8 4 22 23 24 25
		mu 9 4 14 15 16 17
		mu 10 4 16 17 18 19
		mu 11 4 24 25 26 27
		mu 12 4 24 25 26 27
		mu 13 4 28 29 30 31
		mu 14 4 24 25 26 27
		mu 15 4 28 29 30 31
		mu 16 4 28 29 30 31
		mu 17 4 12 13 14 15
		mu 18 4 12 13 14 15
		mu 19 4 12 13 14 15
		mu 20 4 12 13 14 15
		mu 21 4 17 18 19 20
		mu 22 4 4 5 6 7
		mu 23 4 4 5 6 7
		mu 24 4 4 5 6 7
		mu 25 4 4 5 6 7
		mu 26 4 4 5 6 7
		mu 27 4 4 5 6 7
		mu 28 4 4 5 6 7
		mu 29 4 4 5 6 7
		mu 30 4 4 5 6 7
		mu 31 4 16 17 18 19
		mu 32 4 16 17 18 19
		mu 33 4 16 17 18 19
		mu 34 4 16 17 18 19
		f 4 -20 362 369 -369
		mu 0 4 18 19 13 20
		mu 1 4 30 31 25 32
		mu 2 4 30 31 25 32
		mu 3 4 33 34 28 35
		mu 4 4 20 21 15 22
		mu 5 4 18 19 13 20
		mu 6 4 18 19 13 20
		mu 7 4 18 19 13 20
		mu 8 4 26 27 21 28
		mu 9 4 18 19 13 20
		mu 10 4 20 21 15 22
		mu 11 4 28 29 23 30
		mu 12 4 28 29 23 30
		mu 13 4 32 33 27 34
		mu 14 4 28 29 23 30
		mu 15 4 32 33 27 34
		mu 16 4 32 33 27 34
		mu 17 4 16 17 11 18
		mu 18 4 16 17 11 18
		mu 19 4 16 17 11 18
		mu 20 4 16 17 11 18
		mu 21 4 21 22 16 23
		mu 22 4 8 9 3 10
		mu 23 4 8 9 3 10
		mu 24 4 8 9 3 10
		mu 25 4 8 9 3 10
		mu 26 4 8 9 3 10
		mu 27 4 8 9 3 10
		mu 28 4 8 9 3 10
		mu 29 4 8 9 3 10
		mu 30 4 8 9 3 10
		mu 31 4 20 21 15 22
		mu 32 4 20 21 15 22
		mu 33 4 20 21 15 22
		mu 34 4 20 21 15 22
		f 4 24 371 -373 -371
		mu 0 4 21 22 23 24
		mu 1 4 33 34 35 36
		mu 2 4 33 34 35 36
		mu 3 4 36 37 38 39
		mu 4 4 23 24 25 26
		mu 5 4 21 22 23 24
		mu 6 4 21 22 23 24
		mu 7 4 21 22 23 24
		mu 8 4 29 30 31 32
		mu 9 4 21 22 23 24
		mu 10 4 23 24 25 26
		mu 11 4 31 32 33 34
		mu 12 4 31 32 33 34
		mu 13 4 35 36 37 38
		mu 14 4 31 32 33 34
		mu 15 4 35 36 37 38
		mu 16 4 35 36 37 38
		mu 17 4 19 20 21 22
		mu 18 4 19 20 21 22
		mu 19 4 19 20 21 22
		mu 20 4 19 20 21 22
		mu 21 4 24 25 26 27
		mu 22 4 11 12 13 14
		mu 23 4 11 12 13 14
		mu 24 4 11 12 13 14
		mu 25 4 11 12 13 14
		mu 26 4 11 12 13 14
		mu 27 4 11 12 13 14
		mu 28 4 11 12 13 14
		mu 29 4 11 12 13 14
		mu 30 4 11 12 13 14
		mu 31 4 23 24 25 26
		mu 32 4 23 24 25 26
		mu 33 4 23 24 25 26
		mu 34 4 23 24 25 26
		f 4 28 373 -375 -364
		mu 0 4 25 26 27 12
		mu 1 4 37 38 39 24
		mu 2 4 37 38 39 24
		mu 3 4 40 41 42 27
		mu 4 4 27 28 29 14
		mu 5 4 25 26 27 12
		mu 6 4 25 26 27 12
		mu 7 4 25 26 27 12
		mu 8 4 33 34 35 20
		mu 9 4 25 26 27 12
		mu 10 4 27 28 29 14
		mu 11 4 35 36 37 22
		mu 12 4 35 36 37 22
		mu 13 4 39 40 41 26
		mu 14 4 35 36 37 22
		mu 15 4 39 40 41 26
		mu 16 4 39 40 41 26
		mu 17 4 23 24 25 10
		mu 18 4 23 24 25 10
		mu 19 4 23 24 25 10
		mu 20 4 23 24 25 10
		mu 21 4 28 29 30 15
		mu 22 4 15 16 17 2
		mu 23 4 15 16 17 2
		mu 24 4 15 16 17 2
		mu 25 4 15 16 17 2
		mu 26 4 15 16 17 2
		mu 27 4 15 16 17 2
		mu 28 4 15 16 17 2
		mu 29 4 15 16 17 2
		mu 30 4 15 16 17 2
		mu 31 4 27 28 29 14
		mu 32 4 27 28 29 14
		mu 33 4 27 28 29 14
		mu 34 4 27 28 29 14
		f 4 -304 478 476 -366
		mu 0 4 28 29 172 31
		mu 1 4 40 41 184 43
		mu 2 4 40 41 184 43
		mu 3 4 43 44 187 46
		mu 4 4 30 31 174 33
		mu 5 4 28 29 172 31
		mu 6 4 28 29 172 31
		mu 7 4 28 29 172 31
		mu 8 4 36 37 180 39
		mu 9 4 28 29 183 31
		mu 10 4 30 31 174 33
		mu 11 4 38 39 182 41
		mu 12 4 38 39 182 41
		mu 13 4 42 43 186 45
		mu 14 4 38 39 182 41
		mu 15 4 42 43 186 45
		mu 16 4 42 43 186 45
		mu 17 4 26 27 170 29
		mu 18 4 26 27 170 29
		mu 19 4 26 27 170 29
		mu 20 4 26 27 170 29
		mu 21 4 31 32 175 34
		mu 22 4 18 19 162 21
		mu 23 4 18 19 162 21
		mu 24 4 18 19 162 21
		mu 25 4 18 19 162 21
		mu 26 4 18 19 162 21
		mu 27 4 18 19 162 21
		mu 28 4 18 19 162 21
		mu 29 4 18 19 162 21
		mu 30 4 18 19 162 21
		mu 31 4 30 31 174 33
		mu 32 4 30 31 174 33
		mu 33 4 30 31 178 33
		mu 34 4 30 31 178 33
		f 4 308 370 -478 479
		mu 0 4 32 33 34 173
		mu 1 4 44 45 46 185
		mu 2 4 44 45 46 185
		mu 3 4 47 48 49 188
		mu 4 4 34 35 36 175
		mu 5 4 32 33 34 173
		mu 6 4 32 33 34 173
		mu 7 4 32 33 34 173
		mu 8 4 40 41 42 181
		mu 9 4 32 33 34 184
		mu 10 4 34 35 36 175
		mu 11 4 42 43 44 183
		mu 12 4 42 43 44 183
		mu 13 4 46 47 48 187
		mu 14 4 42 43 44 183
		mu 15 4 46 47 48 187
		mu 16 4 46 47 48 187
		mu 17 4 30 31 32 171
		mu 18 4 30 31 32 171
		mu 19 4 30 31 32 171
		mu 20 4 30 31 32 171
		mu 21 4 35 36 37 176
		mu 22 4 22 23 24 163
		mu 23 4 22 23 24 163
		mu 24 4 22 23 24 163
		mu 25 4 22 23 24 163
		mu 26 4 22 23 24 163
		mu 27 4 22 23 24 163
		mu 28 4 22 23 24 163
		mu 29 4 22 23 24 163
		mu 30 4 22 23 24 163
		mu 31 4 34 35 36 175
		mu 32 4 34 35 36 175
		mu 33 4 34 35 36 179
		mu 34 4 34 35 36 179
		f 4 -535 540 535 -372
		mu 0 4 36 186 187 23
		mu 1 4 48 198 199 35
		mu 2 4 48 198 199 35
		mu 3 4 51 201 202 38
		mu 4 4 38 188 189 25
		mu 5 4 36 186 187 23
		mu 6 4 36 186 187 23
		mu 7 4 36 186 187 23
		mu 8 4 44 194 195 31
		mu 9 4 36 197 198 23
		mu 10 4 38 188 189 25
		mu 11 4 46 199 200 33
		mu 12 4 46 196 197 33
		mu 13 4 50 200 201 37
		mu 14 4 46 196 197 33
		mu 15 4 50 200 201 37
		mu 16 4 50 200 201 37
		mu 17 4 34 184 185 21
		mu 18 4 34 184 185 21
		mu 19 4 34 184 185 21
		mu 20 4 34 184 185 21
		mu 21 4 39 189 190 26
		mu 22 4 26 176 177 13
		mu 23 4 26 176 177 13
		mu 24 4 26 176 177 13
		mu 25 4 26 176 177 13
		mu 26 4 26 176 177 13
		mu 27 4 26 176 177 13
		mu 28 4 26 176 177 13
		mu 29 4 26 176 177 13
		mu 30 4 26 176 177 13
		mu 31 4 38 193 194 25
		mu 32 4 38 193 194 25
		mu 33 4 38 192 193 25
		mu 34 4 38 192 193 25
		f 4 542 537 366 -537
		mu 0 4 188 189 40 17
		mu 1 4 200 201 52 29
		mu 2 4 200 201 52 29
		mu 3 4 203 204 55 32
		mu 4 4 190 191 42 19
		mu 5 4 188 189 40 17
		mu 6 4 188 189 40 17
		mu 7 4 188 189 40 17
		mu 8 4 196 197 48 25
		mu 9 4 199 200 40 17
		mu 10 4 190 191 42 19
		mu 11 4 201 202 50 27
		mu 12 4 198 199 50 27
		mu 13 4 202 203 54 31
		mu 14 4 198 199 50 27
		mu 15 4 202 203 54 31
		mu 16 4 202 203 54 31
		mu 17 4 186 187 38 15
		mu 18 4 186 187 38 15
		mu 19 4 186 187 38 15
		mu 20 4 186 187 38 15
		mu 21 4 191 192 43 20
		mu 22 4 178 179 30 7
		mu 23 4 178 179 30 7
		mu 24 4 178 179 30 7
		mu 25 4 178 179 30 7
		mu 26 4 178 179 30 7
		mu 27 4 178 179 30 7
		mu 28 4 178 179 30 7
		mu 29 4 178 179 30 7
		mu 30 4 178 179 30 7
		mu 31 4 195 196 42 19
		mu 32 4 195 196 42 19
		mu 33 4 194 195 42 19
		mu 34 4 194 195 42 19
		f 4 -357 379 381 -378
		mu 0 4 42 43 41 38
		mu 1 4 54 55 53 50
		mu 2 4 54 55 53 50
		mu 3 4 57 58 56 53
		mu 4 4 44 45 43 40
		mu 5 4 42 43 41 38
		mu 6 4 42 43 41 38
		mu 7 4 42 43 41 38
		mu 8 4 50 51 49 46
		mu 9 4 42 43 41 38
		mu 10 4 44 45 43 40
		mu 11 4 52 53 51 48
		mu 12 4 52 53 51 48
		mu 13 4 56 57 55 52
		mu 14 4 52 53 51 48
		mu 15 4 56 57 55 52
		mu 16 4 56 57 55 52
		mu 17 4 40 41 39 36
		mu 18 4 40 41 39 36
		mu 19 4 40 41 39 36
		mu 20 4 40 41 39 36
		mu 21 4 45 46 44 41
		mu 22 4 32 33 31 28
		mu 23 4 32 33 31 28
		mu 24 4 32 33 31 28
		mu 25 4 32 33 31 28
		mu 26 4 32 33 31 28
		mu 27 4 32 33 31 28
		mu 28 4 32 33 31 28
		mu 29 4 32 33 31 28
		mu 30 4 32 33 31 28
		mu 31 4 44 45 43 40
		mu 32 4 44 45 43 40
		mu 33 4 44 45 43 40
		mu 34 4 44 45 43 40
		f 4 -486 487 489 -491
		mu 0 4 176 177 178 179
		mu 1 4 188 189 190 191
		mu 2 4 188 189 190 191
		mu 3 4 191 192 193 194
		mu 4 4 178 179 180 181
		mu 5 4 176 177 178 179
		mu 6 4 176 177 178 179
		mu 7 4 176 177 178 179
		mu 8 4 184 185 186 187
		mu 9 4 187 188 189 190
		mu 10 4 178 179 180 181
		mu 11 4 186 187 188 189
		mu 12 4 186 187 188 189
		mu 13 4 190 191 192 193
		mu 14 4 186 187 188 189
		mu 15 4 190 191 192 193
		mu 16 4 190 191 192 193
		mu 17 4 174 175 176 177
		mu 18 4 174 175 176 177
		mu 19 4 174 175 176 177
		mu 20 4 174 175 176 177
		mu 21 4 179 180 181 182
		mu 22 4 166 167 168 169
		mu 23 4 166 167 168 169
		mu 24 4 166 167 168 169
		mu 25 4 166 167 168 169
		mu 26 4 166 167 168 169
		mu 27 4 166 167 168 169
		mu 28 4 166 167 168 169
		mu 29 4 166 167 168 169
		mu 30 4 166 167 168 169
		mu 31 4 178 179 180 181
		mu 32 4 178 179 180 181
		mu 33 4 182 183 184 185
		mu 34 4 182 183 184 185
		f 4 -493 494 495 -488
		mu 0 4 180 181 182 183
		mu 1 4 192 193 194 195
		mu 2 4 192 193 194 195
		mu 3 4 195 196 197 198
		mu 4 4 182 183 184 185
		mu 5 4 180 181 182 183
		mu 6 4 180 181 182 183
		mu 7 4 180 181 182 183
		mu 8 4 188 189 190 191
		mu 9 4 191 192 193 194
		mu 10 4 182 183 184 185
		mu 11 4 190 191 192 193
		mu 12 4 190 191 192 193
		mu 13 4 194 195 196 197
		mu 14 4 190 191 192 193
		mu 15 4 194 195 196 197
		mu 16 4 194 195 196 197
		mu 17 4 178 179 180 181
		mu 18 4 178 179 180 181
		mu 19 4 178 179 180 181
		mu 20 4 178 179 180 181
		mu 21 4 183 184 185 186
		mu 22 4 170 171 172 173
		mu 23 4 170 171 172 173
		mu 24 4 170 171 172 173
		mu 25 4 170 171 172 173
		mu 26 4 170 171 172 173
		mu 27 4 170 171 172 173
		mu 28 4 170 171 172 173
		mu 29 4 170 171 172 173
		mu 30 4 170 171 172 173
		mu 31 4 182 183 184 185
		mu 32 4 182 183 184 185
		mu 33 4 186 187 188 189
		mu 34 4 186 187 188 189
		f 4 -370 364 374 -384
		mu 0 4 20 13 12 27
		mu 1 4 32 25 24 39
		mu 2 4 32 25 24 39
		mu 3 4 35 28 27 42
		mu 4 4 22 15 14 29
		mu 5 4 20 13 12 27
		mu 6 4 20 13 12 27
		mu 7 4 20 13 12 27
		mu 8 4 28 21 20 35
		mu 9 4 20 13 12 27
		mu 10 4 22 15 14 29
		mu 11 4 30 23 22 37
		mu 12 4 30 23 22 37
		mu 13 4 34 27 26 41
		mu 14 4 30 23 22 37
		mu 15 4 34 27 26 41
		mu 16 4 34 27 26 41
		mu 17 4 18 11 10 25
		mu 18 4 18 11 10 25
		mu 19 4 18 11 10 25
		mu 20 4 18 11 10 25
		mu 21 4 23 16 15 30
		mu 22 4 10 3 2 17
		mu 23 4 10 3 2 17
		mu 24 4 10 3 2 17
		mu 25 4 10 3 2 17
		mu 26 4 10 3 2 17
		mu 27 4 10 3 2 17
		mu 28 4 10 3 2 17
		mu 29 4 10 3 2 17
		mu 30 4 10 3 2 17
		mu 31 4 22 15 14 29
		mu 32 4 22 15 14 29
		mu 33 4 22 15 14 29
		mu 34 4 22 15 14 29
		f 4 541 536 382 -536
		mu 0 4 187 188 17 23
		mu 1 4 199 200 29 35
		mu 2 4 199 200 29 35
		mu 3 4 202 203 32 38
		mu 4 4 189 190 19 25
		mu 5 4 187 188 17 23
		mu 6 4 187 188 17 23
		mu 7 4 187 188 17 23
		mu 8 4 195 196 25 31
		mu 9 4 198 199 17 23
		mu 10 4 189 190 19 25
		mu 11 4 200 201 27 33
		mu 12 4 197 198 27 33
		mu 13 4 201 202 31 37
		mu 14 4 197 198 27 33
		mu 15 4 201 202 31 37
		mu 16 4 201 202 31 37
		mu 17 4 185 186 15 21
		mu 18 4 185 186 15 21
		mu 19 4 185 186 15 21
		mu 20 4 185 186 15 21
		mu 21 4 190 191 20 26
		mu 22 4 177 178 7 13
		mu 23 4 177 178 7 13
		mu 24 4 177 178 7 13
		mu 25 4 177 178 7 13
		mu 26 4 177 178 7 13
		mu 27 4 177 178 7 13
		mu 28 4 177 178 7 13
		mu 29 4 177 178 7 13
		mu 30 4 177 178 7 13
		mu 31 4 194 195 19 25
		mu 32 4 194 195 19 25
		mu 33 4 193 194 19 25
		mu 34 4 193 194 19 25
		f 4 -146 384 386 -386
		mu 0 4 44 45 46 47
		mu 1 4 56 57 58 59
		mu 2 4 56 57 58 59
		mu 3 4 59 60 61 62
		mu 4 4 46 47 48 49
		mu 5 4 44 45 46 47
		mu 6 4 44 45 46 47
		mu 7 4 44 45 46 47
		mu 8 4 52 53 54 55
		mu 9 4 44 45 46 47
		mu 10 4 46 47 48 49
		mu 11 4 54 55 56 57
		mu 12 4 54 55 56 57
		mu 13 4 58 59 60 61
		mu 14 4 54 55 56 57
		mu 15 4 58 59 60 61
		mu 16 4 58 59 60 61
		mu 17 4 42 43 44 45
		mu 18 4 42 43 44 45
		mu 19 4 42 43 44 45
		mu 20 4 42 43 44 45
		mu 21 4 47 48 49 50
		mu 22 4 34 35 36 37
		mu 23 4 34 35 36 37
		mu 24 4 34 35 36 37
		mu 25 4 34 35 36 37
		mu 26 4 34 35 36 37
		mu 27 4 34 35 36 37
		mu 28 4 34 35 36 37
		mu 29 4 34 35 36 37
		mu 30 4 34 35 36 37
		mu 31 4 46 47 48 49
		mu 32 4 46 47 48 49
		mu 33 4 46 47 48 49
		mu 34 4 46 47 48 49
		f 4 146 388 -390 -388
		mu 0 4 48 49 50 51
		mu 1 4 60 61 62 63
		mu 2 4 60 61 62 63
		mu 3 4 63 64 65 66
		mu 4 4 50 51 52 53
		mu 5 4 48 49 50 51
		mu 6 4 48 49 50 51
		mu 7 4 48 49 50 51
		mu 8 4 56 57 58 59
		mu 9 4 48 49 50 51
		mu 10 4 50 51 52 53
		mu 11 4 58 59 60 61
		mu 12 4 58 59 60 61
		mu 13 4 62 63 64 65
		mu 14 4 58 59 60 61
		mu 15 4 62 63 64 65
		mu 16 4 62 63 64 65
		mu 17 4 46 47 48 49
		mu 18 4 46 47 48 49
		mu 19 4 46 47 48 49
		mu 20 4 46 47 48 49
		mu 21 4 51 52 53 54
		mu 22 4 38 39 40 41
		mu 23 4 38 39 40 41
		mu 24 4 38 39 40 41
		mu 25 4 38 39 40 41
		mu 26 4 38 39 40 41
		mu 27 4 38 39 40 41
		mu 28 4 38 39 40 41
		mu 29 4 38 39 40 41
		mu 30 4 38 39 40 41
		mu 31 4 50 51 52 53
		mu 32 4 50 51 52 53
		mu 33 4 50 51 52 53
		mu 34 4 50 51 52 53
		f 4 147 387 -391 -385
		mu 0 4 52 53 54 55
		mu 1 4 64 65 66 67
		mu 2 4 64 65 66 67
		mu 3 4 67 68 69 70
		mu 4 4 54 55 56 57
		mu 5 4 52 53 54 55
		mu 6 4 52 53 54 55
		mu 7 4 52 53 54 55
		mu 8 4 60 61 62 63
		mu 9 4 52 53 54 55
		mu 10 4 54 55 56 57
		mu 11 4 62 63 64 65
		mu 12 4 62 63 64 65
		mu 13 4 66 67 68 69
		mu 14 4 62 63 64 65
		mu 15 4 66 67 68 69
		mu 16 4 66 67 68 69
		mu 17 4 50 51 52 53
		mu 18 4 50 51 52 53
		mu 19 4 50 51 52 53
		mu 20 4 50 51 52 53
		mu 21 4 55 56 57 58
		mu 22 4 42 43 44 45
		mu 23 4 42 43 44 45
		mu 24 4 42 43 44 45
		mu 25 4 42 43 44 45
		mu 26 4 42 43 44 45
		mu 27 4 42 43 44 45
		mu 28 4 42 43 44 45
		mu 29 4 42 43 44 45
		mu 30 4 42 43 44 45
		mu 31 4 54 55 56 57
		mu 32 4 54 55 56 57
		mu 33 4 54 55 56 57
		mu 34 4 54 55 56 57
		f 4 -149 385 391 -389
		mu 0 4 56 57 58 59
		mu 1 4 68 69 70 71
		mu 2 4 68 69 70 71
		mu 3 4 71 72 73 74
		mu 4 4 58 59 60 61
		mu 5 4 56 57 58 59
		mu 6 4 56 57 58 59
		mu 7 4 56 57 58 59
		mu 8 4 64 65 66 67
		mu 9 4 56 57 58 59
		mu 10 4 58 59 60 61
		mu 11 4 66 67 68 69
		mu 12 4 66 67 68 69
		mu 13 4 70 71 72 73
		mu 14 4 66 67 68 69
		mu 15 4 70 71 72 73
		mu 16 4 70 71 72 73
		mu 17 4 54 55 56 57
		mu 18 4 54 55 56 57
		mu 19 4 54 55 56 57
		mu 20 4 54 55 56 57
		mu 21 4 59 60 61 62
		mu 22 4 46 47 48 49
		mu 23 4 46 47 48 49
		mu 24 4 46 47 48 49
		mu 25 4 46 47 48 49
		mu 26 4 46 47 48 49
		mu 27 4 46 47 48 49
		mu 28 4 46 47 48 49
		mu 29 4 46 47 48 49
		mu 30 4 46 47 48 49
		mu 31 4 58 59 60 61
		mu 32 4 58 59 60 61
		mu 33 4 58 59 60 61
		mu 34 4 58 59 60 61
		f 4 -150 392 394 -394
		mu 0 4 60 61 62 63
		mu 1 4 72 73 74 75
		mu 2 4 72 73 74 75
		mu 3 4 75 76 77 78
		mu 4 4 62 63 64 65
		mu 5 4 60 61 62 63
		mu 6 4 60 61 62 63
		mu 7 4 60 61 62 63
		mu 8 4 68 69 70 71
		mu 9 4 60 61 62 63
		mu 10 4 62 63 64 65
		mu 11 4 70 71 72 73
		mu 12 4 70 71 72 73
		mu 13 4 74 75 76 77
		mu 14 4 70 71 72 73
		mu 15 4 74 75 76 77
		mu 16 4 74 75 76 77
		mu 17 4 58 59 60 61
		mu 18 4 58 59 60 61
		mu 19 4 58 59 60 61
		mu 20 4 58 59 60 61
		mu 21 4 63 64 65 66
		mu 22 4 50 51 52 53
		mu 23 4 50 51 52 53
		mu 24 4 50 51 52 53
		mu 25 4 50 51 52 53
		mu 26 4 50 51 52 53
		mu 27 4 50 51 52 53
		mu 28 4 50 51 52 53
		mu 29 4 50 51 52 53
		mu 30 4 50 51 52 53
		mu 31 4 62 63 64 65
		mu 32 4 62 63 64 65
		mu 33 4 62 63 64 65
		mu 34 4 62 63 64 65
		f 4 150 396 -398 -396
		mu 0 4 64 65 66 67
		mu 1 4 76 77 78 79
		mu 2 4 76 77 78 79
		mu 3 4 79 80 81 82
		mu 4 4 66 67 68 69
		mu 5 4 64 65 66 67
		mu 6 4 64 65 66 67
		mu 7 4 64 65 66 67
		mu 8 4 72 73 74 75
		mu 9 4 64 65 66 67
		mu 10 4 66 67 68 69
		mu 11 4 74 75 76 77
		mu 12 4 74 75 76 77
		mu 13 4 78 79 80 81
		mu 14 4 74 75 76 77
		mu 15 4 78 79 80 81
		mu 16 4 78 79 80 81
		mu 17 4 62 63 64 65
		mu 18 4 62 63 64 65
		mu 19 4 62 63 64 65
		mu 20 4 62 63 64 65
		mu 21 4 67 68 69 70
		mu 22 4 54 55 56 57
		mu 23 4 54 55 56 57
		mu 24 4 54 55 56 57
		mu 25 4 54 55 56 57
		mu 26 4 54 55 56 57
		mu 27 4 54 55 56 57
		mu 28 4 54 55 56 57
		mu 29 4 54 55 56 57
		mu 30 4 54 55 56 57
		mu 31 4 66 67 68 69
		mu 32 4 66 67 68 69
		mu 33 4 66 67 68 69
		mu 34 4 66 67 68 69
		f 4 151 395 -399 -393
		mu 0 4 68 69 70 71
		mu 1 4 80 81 82 83
		mu 2 4 80 81 82 83
		mu 3 4 83 84 85 86
		mu 4 4 70 71 72 73
		mu 5 4 68 69 70 71
		mu 6 4 68 69 70 71
		mu 7 4 68 69 70 71
		mu 8 4 76 77 78 79
		mu 9 4 68 69 70 71
		mu 10 4 70 71 72 73
		mu 11 4 78 79 80 81
		mu 12 4 78 79 80 81
		mu 13 4 82 83 84 85
		mu 14 4 78 79 80 81
		mu 15 4 82 83 84 85
		mu 16 4 82 83 84 85
		mu 17 4 66 67 68 69
		mu 18 4 66 67 68 69
		mu 19 4 66 67 68 69
		mu 20 4 66 67 68 69
		mu 21 4 71 72 73 74
		mu 22 4 58 59 60 61
		mu 23 4 58 59 60 61
		mu 24 4 58 59 60 61
		mu 25 4 58 59 60 61
		mu 26 4 58 59 60 61
		mu 27 4 58 59 60 61
		mu 28 4 58 59 60 61
		mu 29 4 58 59 60 61
		mu 30 4 58 59 60 61
		mu 31 4 70 71 72 73
		mu 32 4 70 71 72 73
		mu 33 4 70 71 72 73
		mu 34 4 70 71 72 73
		f 4 -153 393 399 -397
		mu 0 4 72 73 74 75
		mu 1 4 84 85 86 87
		mu 2 4 84 85 86 87
		mu 3 4 87 88 89 90
		mu 4 4 74 75 76 77
		mu 5 4 72 73 74 75
		mu 6 4 72 73 74 75
		mu 7 4 72 73 74 75
		mu 8 4 80 81 82 83
		mu 9 4 72 73 74 75
		mu 10 4 74 75 76 77
		mu 11 4 82 83 84 85
		mu 12 4 82 83 84 85
		mu 13 4 86 87 88 89
		mu 14 4 82 83 84 85
		mu 15 4 86 87 88 89
		mu 16 4 86 87 88 89
		mu 17 4 70 71 72 73
		mu 18 4 70 71 72 73
		mu 19 4 70 71 72 73
		mu 20 4 70 71 72 73
		mu 21 4 75 76 77 78
		mu 22 4 62 63 64 65
		mu 23 4 62 63 64 65
		mu 24 4 62 63 64 65
		mu 25 4 62 63 64 65
		mu 26 4 62 63 64 65
		mu 27 4 62 63 64 65
		mu 28 4 62 63 64 65
		mu 29 4 62 63 64 65
		mu 30 4 62 63 64 65
		mu 31 4 74 75 76 77
		mu 32 4 74 75 76 77
		mu 33 4 74 75 76 77
		mu 34 4 74 75 76 77
		f 4 -154 400 402 -402
		mu 0 4 76 77 78 79
		mu 1 4 88 89 90 91
		mu 2 4 88 89 90 91
		mu 3 4 91 92 93 94
		mu 4 4 78 79 80 81
		mu 5 4 76 77 78 79
		mu 6 4 76 77 78 79
		mu 7 4 76 77 78 79
		mu 8 4 84 85 86 87
		mu 9 4 76 77 78 79
		mu 10 4 78 79 80 81
		mu 11 4 86 87 88 89
		mu 12 4 86 87 88 89
		mu 13 4 90 91 92 93
		mu 14 4 86 87 88 89
		mu 15 4 90 91 92 93
		mu 16 4 90 91 92 93
		mu 17 4 74 75 76 77
		mu 18 4 74 75 76 77
		mu 19 4 74 75 76 77
		mu 20 4 74 75 76 77
		mu 21 4 79 80 81 82
		mu 22 4 66 67 68 69
		mu 23 4 66 67 68 69
		mu 24 4 66 67 68 69
		mu 25 4 66 67 68 69
		mu 26 4 66 67 68 69
		mu 27 4 66 67 68 69
		mu 28 4 66 67 68 69
		mu 29 4 66 67 68 69
		mu 30 4 66 67 68 69
		mu 31 4 78 79 80 81
		mu 32 4 78 79 80 81
		mu 33 4 78 79 80 81
		mu 34 4 78 79 80 81
		f 4 154 404 -406 -404
		mu 0 4 80 81 82 83
		mu 1 4 92 93 94 95
		mu 2 4 92 93 94 95
		mu 3 4 95 96 97 98
		mu 4 4 82 83 84 85
		mu 5 4 80 81 82 83
		mu 6 4 80 81 82 83
		mu 7 4 80 81 82 83
		mu 8 4 88 89 90 91
		mu 9 4 80 81 82 83
		mu 10 4 82 83 84 85
		mu 11 4 90 91 92 93
		mu 12 4 90 91 92 93
		mu 13 4 94 95 96 97
		mu 14 4 90 91 92 93
		mu 15 4 94 95 96 97
		mu 16 4 94 95 96 97
		mu 17 4 78 79 80 81
		mu 18 4 78 79 80 81
		mu 19 4 78 79 80 81
		mu 20 4 78 79 80 81
		mu 21 4 83 84 85 86
		mu 22 4 70 71 72 73
		mu 23 4 70 71 72 73
		mu 24 4 70 71 72 73
		mu 25 4 70 71 72 73
		mu 26 4 70 71 72 73
		mu 27 4 70 71 72 73
		mu 28 4 70 71 72 73
		mu 29 4 70 71 72 73
		mu 30 4 70 71 72 73
		mu 31 4 82 83 84 85
		mu 32 4 82 83 84 85
		mu 33 4 82 83 84 85
		mu 34 4 82 83 84 85
		f 4 155 403 -407 -401
		mu 0 4 84 85 86 87
		mu 1 4 96 97 98 99
		mu 2 4 96 97 98 99
		mu 3 4 99 100 101 102
		mu 4 4 86 87 88 89
		mu 5 4 84 85 86 87
		mu 6 4 84 85 86 87
		mu 7 4 84 85 86 87
		mu 8 4 92 93 94 95
		mu 9 4 84 85 86 87
		mu 10 4 86 87 88 89
		mu 11 4 94 95 96 97
		mu 12 4 94 95 96 97
		mu 13 4 98 99 100 101
		mu 14 4 94 95 96 97
		mu 15 4 98 99 100 101
		mu 16 4 98 99 100 101
		mu 17 4 82 83 84 85
		mu 18 4 82 83 84 85
		mu 19 4 82 83 84 85
		mu 20 4 82 83 84 85
		mu 21 4 87 88 89 90
		mu 22 4 74 75 76 77
		mu 23 4 74 75 76 77
		mu 24 4 74 75 76 77
		mu 25 4 74 75 76 77
		mu 26 4 74 75 76 77
		mu 27 4 74 75 76 77
		mu 28 4 74 75 76 77
		mu 29 4 74 75 76 77
		mu 30 4 74 75 76 77
		mu 31 4 86 87 88 89
		mu 32 4 86 87 88 89
		mu 33 4 86 87 88 89
		mu 34 4 86 87 88 89
		f 4 -157 401 407 -405
		mu 0 4 88 89 90 91
		mu 1 4 100 101 102 103
		mu 2 4 100 101 102 103
		mu 3 4 103 104 105 106
		mu 4 4 90 91 92 93
		mu 5 4 88 89 90 91
		mu 6 4 88 89 90 91
		mu 7 4 88 89 90 91
		mu 8 4 96 97 98 99
		mu 9 4 88 89 90 91
		mu 10 4 90 91 92 93
		mu 11 4 98 99 100 101
		mu 12 4 98 99 100 101
		mu 13 4 102 103 104 105
		mu 14 4 98 99 100 101
		mu 15 4 102 103 104 105
		mu 16 4 102 103 104 105
		mu 17 4 86 87 88 89
		mu 18 4 86 87 88 89
		mu 19 4 86 87 88 89
		mu 20 4 86 87 88 89
		mu 21 4 91 92 93 94
		mu 22 4 78 79 80 81
		mu 23 4 78 79 80 81
		mu 24 4 78 79 80 81
		mu 25 4 78 79 80 81
		mu 26 4 78 79 80 81
		mu 27 4 78 79 80 81
		mu 28 4 78 79 80 81
		mu 29 4 78 79 80 81
		mu 30 4 78 79 80 81
		mu 31 4 90 91 92 93
		mu 32 4 90 91 92 93
		mu 33 4 90 91 92 93
		mu 34 4 90 91 92 93
		f 4 -158 408 410 -410
		mu 0 4 92 93 94 95
		mu 1 4 104 105 106 107
		mu 2 4 104 105 106 107
		mu 3 4 107 108 109 110
		mu 4 4 94 95 96 97
		mu 5 4 92 93 94 95
		mu 6 4 92 93 94 95
		mu 7 4 92 93 94 95
		mu 8 4 100 101 102 103
		mu 9 4 92 93 94 95
		mu 10 4 94 95 96 97
		mu 11 4 102 103 104 105
		mu 12 4 102 103 104 105
		mu 13 4 106 107 108 109
		mu 14 4 102 103 104 105
		mu 15 4 106 107 108 109
		mu 16 4 106 107 108 109
		mu 17 4 90 91 92 93
		mu 18 4 90 91 92 93
		mu 19 4 90 91 92 93
		mu 20 4 90 91 92 93
		mu 21 4 95 96 97 98
		mu 22 4 82 83 84 85
		mu 23 4 82 83 84 85
		mu 24 4 82 83 84 85
		mu 25 4 82 83 84 85
		mu 26 4 82 83 84 85
		mu 27 4 82 83 84 85
		mu 28 4 82 83 84 85
		mu 29 4 82 83 84 85
		mu 30 4 82 83 84 85
		mu 31 4 94 95 96 97
		mu 32 4 94 95 96 97
		mu 33 4 94 95 96 97
		mu 34 4 94 95 96 97
		f 4 158 412 -414 -412
		mu 0 4 96 97 98 99
		mu 1 4 108 109 110 111
		mu 2 4 108 109 110 111
		mu 3 4 111 112 113 114
		mu 4 4 98 99 100 101
		mu 5 4 96 97 98 99
		mu 6 4 96 97 98 99
		mu 7 4 96 97 98 99
		mu 8 4 104 105 106 107
		mu 9 4 96 97 98 99
		mu 10 4 98 99 100 101
		mu 11 4 106 107 108 109
		mu 12 4 106 107 108 109
		mu 13 4 110 111 112 113
		mu 14 4 106 107 108 109
		mu 15 4 110 111 112 113
		mu 16 4 110 111 112 113
		mu 17 4 94 95 96 97
		mu 18 4 94 95 96 97
		mu 19 4 94 95 96 97
		mu 20 4 94 95 96 97
		mu 21 4 99 100 101 102
		mu 22 4 86 87 88 89
		mu 23 4 86 87 88 89
		mu 24 4 86 87 88 89
		mu 25 4 86 87 88 89
		mu 26 4 86 87 88 89
		mu 27 4 86 87 88 89
		mu 28 4 86 87 88 89
		mu 29 4 86 87 88 89
		mu 30 4 86 87 88 89
		mu 31 4 98 99 100 101
		mu 32 4 98 99 100 101
		mu 33 4 98 99 100 101
		mu 34 4 98 99 100 101
		f 4 159 411 -415 -409
		mu 0 4 100 101 102 103
		mu 1 4 112 113 114 115
		mu 2 4 112 113 114 115
		mu 3 4 115 116 117 118
		mu 4 4 102 103 104 105
		mu 5 4 100 101 102 103
		mu 6 4 100 101 102 103
		mu 7 4 100 101 102 103
		mu 8 4 108 109 110 111
		mu 9 4 100 101 102 103
		mu 10 4 102 103 104 105
		mu 11 4 110 111 112 113
		mu 12 4 110 111 112 113
		mu 13 4 114 115 116 117
		mu 14 4 110 111 112 113
		mu 15 4 114 115 116 117
		mu 16 4 114 115 116 117
		mu 17 4 98 99 100 101
		mu 18 4 98 99 100 101
		mu 19 4 98 99 100 101
		mu 20 4 98 99 100 101
		mu 21 4 103 104 105 106
		mu 22 4 90 91 92 93
		mu 23 4 90 91 92 93
		mu 24 4 90 91 92 93
		mu 25 4 90 91 92 93
		mu 26 4 90 91 92 93
		mu 27 4 90 91 92 93
		mu 28 4 90 91 92 93
		mu 29 4 90 91 92 93
		mu 30 4 90 91 92 93
		mu 31 4 102 103 104 105
		mu 32 4 102 103 104 105
		mu 33 4 102 103 104 105
		mu 34 4 102 103 104 105
		f 4 -161 409 415 -413
		mu 0 4 104 105 106 107
		mu 1 4 116 117 118 119
		mu 2 4 116 117 118 119
		mu 3 4 119 120 121 122
		mu 4 4 106 107 108 109
		mu 5 4 104 105 106 107
		mu 6 4 104 105 106 107
		mu 7 4 104 105 106 107
		mu 8 4 112 113 114 115
		mu 9 4 104 105 106 107
		mu 10 4 106 107 108 109
		mu 11 4 114 115 116 117
		mu 12 4 114 115 116 117
		mu 13 4 118 119 120 121
		mu 14 4 114 115 116 117
		mu 15 4 118 119 120 121
		mu 16 4 118 119 120 121
		mu 17 4 102 103 104 105
		mu 18 4 102 103 104 105
		mu 19 4 102 103 104 105
		mu 20 4 102 103 104 105
		mu 21 4 107 108 109 110
		mu 22 4 94 95 96 97
		mu 23 4 94 95 96 97
		mu 24 4 94 95 96 97
		mu 25 4 94 95 96 97
		mu 26 4 94 95 96 97
		mu 27 4 94 95 96 97
		mu 28 4 94 95 96 97
		mu 29 4 94 95 96 97
		mu 30 4 94 95 96 97
		mu 31 4 106 107 108 109
		mu 32 4 106 107 108 109
		mu 33 4 106 107 108 109
		mu 34 4 106 107 108 109
		f 4 244 417 -419 -417
		mu 0 4 108 109 110 111
		mu 1 4 120 121 122 123
		mu 2 4 120 121 122 123
		mu 3 4 123 124 125 126
		mu 4 4 110 111 112 113
		mu 5 4 108 109 110 111
		mu 6 4 108 109 110 111
		mu 7 4 108 109 110 111
		mu 8 4 116 117 118 119
		mu 9 4 108 109 110 111
		mu 10 4 110 111 112 113
		mu 11 4 118 119 120 121
		mu 12 4 118 119 120 121
		mu 13 4 122 123 124 125
		mu 14 4 118 119 120 121
		mu 15 4 122 123 124 125
		mu 16 4 122 123 124 125
		mu 17 4 106 107 108 109
		mu 18 4 106 107 108 109
		mu 19 4 106 107 108 109
		mu 20 4 106 107 108 109
		mu 21 4 111 112 113 114
		mu 22 4 98 99 100 101
		mu 23 4 98 99 100 101
		mu 24 4 98 99 100 101
		mu 25 4 98 99 100 101
		mu 26 4 98 99 100 101
		mu 27 4 98 99 100 101
		mu 28 4 98 99 100 101
		mu 29 4 98 99 100 101
		mu 30 4 98 99 100 101
		mu 31 4 110 111 112 113
		mu 32 4 110 111 112 113
		mu 33 4 110 111 112 113
		mu 34 4 110 111 112 113
		f 4 245 419 -421 -418
		mu 0 4 112 113 114 115
		mu 1 4 124 125 126 127
		mu 2 4 124 125 126 127
		mu 3 4 127 128 129 130
		mu 4 4 114 115 116 117
		mu 5 4 112 113 114 115
		mu 6 4 112 113 114 115
		mu 7 4 112 113 114 115
		mu 8 4 120 121 122 123
		mu 9 4 112 113 114 115
		mu 10 4 114 115 116 117
		mu 11 4 122 123 124 125
		mu 12 4 122 123 124 125
		mu 13 4 126 127 128 129
		mu 14 4 122 123 124 125
		mu 15 4 126 127 128 129
		mu 16 4 126 127 128 129
		mu 17 4 110 111 112 113
		mu 18 4 110 111 112 113
		mu 19 4 110 111 112 113
		mu 20 4 110 111 112 113
		mu 21 4 115 116 117 118
		mu 22 4 102 103 104 105
		mu 23 4 102 103 104 105
		mu 24 4 102 103 104 105
		mu 25 4 102 103 104 105
		mu 26 4 102 103 104 105
		mu 27 4 102 103 104 105
		mu 28 4 102 103 104 105
		mu 29 4 102 103 104 105
		mu 30 4 102 103 104 105
		mu 31 4 114 115 116 117
		mu 32 4 114 115 116 117
		mu 33 4 114 115 116 117
		mu 34 4 114 115 116 117
		f 4 -247 421 422 -420
		mu 0 4 116 117 118 119
		mu 1 4 128 129 130 131
		mu 2 4 128 129 130 131
		mu 3 4 131 132 133 134
		mu 4 4 118 119 120 121
		mu 5 4 116 117 118 119
		mu 6 4 116 117 118 119
		mu 7 4 116 117 118 119
		mu 8 4 124 125 126 127
		mu 9 4 116 117 118 119
		mu 10 4 118 119 120 121
		mu 11 4 126 127 128 129
		mu 12 4 126 127 128 129
		mu 13 4 130 131 132 133
		mu 14 4 126 127 128 129
		mu 15 4 130 131 132 133
		mu 16 4 130 131 132 133
		mu 17 4 114 115 116 117
		mu 18 4 114 115 116 117
		mu 19 4 114 115 116 117
		mu 20 4 114 115 116 117
		mu 21 4 119 120 121 122
		mu 22 4 106 107 108 109
		mu 23 4 106 107 108 109
		mu 24 4 106 107 108 109
		mu 25 4 106 107 108 109
		mu 26 4 106 107 108 109
		mu 27 4 106 107 108 109
		mu 28 4 106 107 108 109
		mu 29 4 106 107 108 109
		mu 30 4 106 107 108 109
		mu 31 4 118 119 120 121
		mu 32 4 118 119 120 121
		mu 33 4 118 119 120 121
		mu 34 4 118 119 120 121
		f 4 -248 416 423 -422
		mu 0 4 120 121 122 123
		mu 1 4 132 133 134 135
		mu 2 4 132 133 134 135
		mu 3 4 135 136 137 138
		mu 4 4 122 123 124 125
		mu 5 4 120 121 122 123
		mu 6 4 120 121 122 123
		mu 7 4 120 121 122 123
		mu 8 4 128 129 130 131
		mu 9 4 120 121 122 123
		mu 10 4 122 123 124 125
		mu 11 4 130 131 132 133
		mu 12 4 130 131 132 133
		mu 13 4 134 135 136 137
		mu 14 4 130 131 132 133
		mu 15 4 134 135 136 137
		mu 16 4 134 135 136 137
		mu 17 4 118 119 120 121
		mu 18 4 118 119 120 121
		mu 19 4 118 119 120 121
		mu 20 4 118 119 120 121
		mu 21 4 123 124 125 126
		mu 22 4 110 111 112 113
		mu 23 4 110 111 112 113
		mu 24 4 110 111 112 113
		mu 25 4 110 111 112 113
		mu 26 4 110 111 112 113
		mu 27 4 110 111 112 113
		mu 28 4 110 111 112 113
		mu 29 4 110 111 112 113
		mu 30 4 110 111 112 113
		mu 31 4 122 123 124 125
		mu 32 4 122 123 124 125
		mu 33 4 122 123 124 125
		mu 34 4 122 123 124 125
		f 4 248 425 -427 -425
		mu 0 4 124 125 126 127
		mu 1 4 136 137 138 139
		mu 2 4 136 137 138 139
		mu 3 4 139 140 141 142
		mu 4 4 126 127 128 129
		mu 5 4 124 125 126 127
		mu 6 4 124 125 126 127
		mu 7 4 124 125 126 127
		mu 8 4 132 133 134 135
		mu 9 4 124 125 126 127
		mu 10 4 126 127 128 129
		mu 11 4 134 135 136 137
		mu 12 4 134 135 136 137
		mu 13 4 138 139 140 141
		mu 14 4 134 135 136 137
		mu 15 4 138 139 140 141
		mu 16 4 138 139 140 141
		mu 17 4 122 123 124 125
		mu 18 4 122 123 124 125
		mu 19 4 122 123 124 125
		mu 20 4 122 123 124 125
		mu 21 4 127 128 129 130
		mu 22 4 114 115 116 117
		mu 23 4 114 115 116 117
		mu 24 4 114 115 116 117
		mu 25 4 114 115 116 117
		mu 26 4 114 115 116 117
		mu 27 4 114 115 116 117
		mu 28 4 114 115 116 117
		mu 29 4 114 115 116 117
		mu 30 4 114 115 116 117
		mu 31 4 126 127 128 129
		mu 32 4 126 127 128 129
		mu 33 4 126 127 128 129
		mu 34 4 126 127 128 129
		f 4 249 427 -429 -426
		mu 0 4 128 129 130 131
		mu 1 4 140 141 142 143
		mu 2 4 140 141 142 143
		mu 3 4 143 144 145 146
		mu 4 4 130 131 132 133
		mu 5 4 128 129 130 131
		mu 6 4 128 129 130 131
		mu 7 4 128 129 130 131
		mu 8 4 136 137 138 139
		mu 9 4 128 129 130 131
		mu 10 4 130 131 132 133
		mu 11 4 138 139 140 141
		mu 12 4 138 139 140 141
		mu 13 4 142 143 144 145
		mu 14 4 138 139 140 141
		mu 15 4 142 143 144 145
		mu 16 4 142 143 144 145
		mu 17 4 126 127 128 129
		mu 18 4 126 127 128 129
		mu 19 4 126 127 128 129
		mu 20 4 126 127 128 129
		mu 21 4 131 132 133 134
		mu 22 4 118 119 120 121
		mu 23 4 118 119 120 121
		mu 24 4 118 119 120 121
		mu 25 4 118 119 120 121
		mu 26 4 118 119 120 121
		mu 27 4 118 119 120 121
		mu 28 4 118 119 120 121
		mu 29 4 118 119 120 121
		mu 30 4 118 119 120 121
		mu 31 4 130 131 132 133
		mu 32 4 130 131 132 133
		mu 33 4 130 131 132 133
		mu 34 4 130 131 132 133
		f 4 -251 429 430 -428
		mu 0 4 132 133 134 135
		mu 1 4 144 145 146 147
		mu 2 4 144 145 146 147
		mu 3 4 147 148 149 150
		mu 4 4 134 135 136 137
		mu 5 4 132 133 134 135
		mu 6 4 132 133 134 135
		mu 7 4 132 133 134 135
		mu 8 4 140 141 142 143
		mu 9 4 132 133 134 135
		mu 10 4 134 135 136 137
		mu 11 4 142 143 144 145
		mu 12 4 142 143 144 145
		mu 13 4 146 147 148 149
		mu 14 4 142 143 144 145
		mu 15 4 146 147 148 149
		mu 16 4 146 147 148 149
		mu 17 4 130 131 132 133
		mu 18 4 130 131 132 133
		mu 19 4 130 131 132 133
		mu 20 4 130 131 132 133
		mu 21 4 135 136 137 138
		mu 22 4 122 123 124 125
		mu 23 4 122 123 124 125
		mu 24 4 122 123 124 125
		mu 25 4 122 123 124 125
		mu 26 4 122 123 124 125
		mu 27 4 122 123 124 125
		mu 28 4 122 123 124 125
		mu 29 4 122 123 124 125
		mu 30 4 122 123 124 125
		mu 31 4 134 135 136 137
		mu 32 4 134 135 136 137
		mu 33 4 134 135 136 137
		mu 34 4 134 135 136 137
		f 4 -252 424 431 -430
		mu 0 4 136 137 138 139
		mu 1 4 148 149 150 151
		mu 2 4 148 149 150 151
		mu 3 4 151 152 153 154
		mu 4 4 138 139 140 141
		mu 5 4 136 137 138 139
		mu 6 4 136 137 138 139
		mu 7 4 136 137 138 139
		mu 8 4 144 145 146 147
		mu 9 4 136 137 138 139
		mu 10 4 138 139 140 141
		mu 11 4 146 147 148 149
		mu 12 4 146 147 148 149
		mu 13 4 150 151 152 153
		mu 14 4 146 147 148 149
		mu 15 4 150 151 152 153
		mu 16 4 150 151 152 153
		mu 17 4 134 135 136 137
		mu 18 4 134 135 136 137
		mu 19 4 134 135 136 137
		mu 20 4 134 135 136 137
		mu 21 4 139 140 141 142
		mu 22 4 126 127 128 129
		mu 23 4 126 127 128 129
		mu 24 4 126 127 128 129
		mu 25 4 126 127 128 129
		mu 26 4 126 127 128 129
		mu 27 4 126 127 128 129
		mu 28 4 126 127 128 129
		mu 29 4 126 127 128 129
		mu 30 4 126 127 128 129
		mu 31 4 138 139 140 141
		mu 32 4 138 139 140 141
		mu 33 4 138 139 140 141
		mu 34 4 138 139 140 141
		f 4 252 433 -435 -433
		mu 0 4 140 141 142 143
		mu 1 4 152 153 154 155
		mu 2 4 152 153 154 155
		mu 3 4 155 156 157 158
		mu 4 4 142 143 144 145
		mu 5 4 140 141 142 143
		mu 6 4 140 141 142 143
		mu 7 4 140 141 142 143
		mu 8 4 148 149 150 151
		mu 9 4 140 141 142 143
		mu 10 4 142 143 144 145
		mu 11 4 150 151 152 153
		mu 12 4 150 151 152 153
		mu 13 4 154 155 156 157
		mu 14 4 150 151 152 153
		mu 15 4 154 155 156 157
		mu 16 4 154 155 156 157
		mu 17 4 138 139 140 141
		mu 18 4 138 139 140 141
		mu 19 4 138 139 140 141
		mu 20 4 138 139 140 141
		mu 21 4 143 144 145 146
		mu 22 4 130 131 132 133
		mu 23 4 130 131 132 133
		mu 24 4 130 131 132 133
		mu 25 4 130 131 132 133
		mu 26 4 130 131 132 133
		mu 27 4 130 131 132 133
		mu 28 4 130 131 132 133
		mu 29 4 130 131 132 133
		mu 30 4 130 131 132 133
		mu 31 4 142 143 144 145
		mu 32 4 142 143 144 145
		mu 33 4 142 143 144 145
		mu 34 4 142 143 144 145
		f 4 253 435 -437 -434
		mu 0 4 144 145 146 147
		mu 1 4 156 157 158 159
		mu 2 4 156 157 158 159
		mu 3 4 159 160 161 162
		mu 4 4 146 147 148 149
		mu 5 4 144 145 146 147
		mu 6 4 144 145 146 147
		mu 7 4 144 145 146 147
		mu 8 4 152 153 154 155
		mu 9 4 144 145 146 147
		mu 10 4 146 147 148 149
		mu 11 4 154 155 156 157
		mu 12 4 154 155 156 157
		mu 13 4 158 159 160 161
		mu 14 4 154 155 156 157
		mu 15 4 158 159 160 161
		mu 16 4 158 159 160 161
		mu 17 4 142 143 144 145
		mu 18 4 142 143 144 145
		mu 19 4 142 143 144 145
		mu 20 4 142 143 144 145
		mu 21 4 147 148 149 150
		mu 22 4 134 135 136 137
		mu 23 4 134 135 136 137
		mu 24 4 134 135 136 137
		mu 25 4 134 135 136 137
		mu 26 4 134 135 136 137
		mu 27 4 134 135 136 137
		mu 28 4 134 135 136 137
		mu 29 4 134 135 136 137
		mu 30 4 134 135 136 137
		mu 31 4 146 147 148 149
		mu 32 4 146 147 148 149
		mu 33 4 146 147 148 149
		mu 34 4 146 147 148 149
		f 4 -255 437 438 -436
		mu 0 4 148 149 150 151
		mu 1 4 160 161 162 163
		mu 2 4 160 161 162 163
		mu 3 4 163 164 165 166
		mu 4 4 150 151 152 153
		mu 5 4 148 149 150 151
		mu 6 4 148 149 150 151
		mu 7 4 148 149 150 151
		mu 8 4 156 157 158 159
		mu 9 4 148 149 150 151
		mu 10 4 150 151 152 153
		mu 11 4 158 159 160 161
		mu 12 4 158 159 160 161
		mu 13 4 162 163 164 165
		mu 14 4 158 159 160 161
		mu 15 4 162 163 164 165
		mu 16 4 162 163 164 165
		mu 17 4 146 147 148 149
		mu 18 4 146 147 148 149
		mu 19 4 146 147 148 149
		mu 20 4 146 147 148 149
		mu 21 4 151 152 153 154
		mu 22 4 138 139 140 141
		mu 23 4 138 139 140 141
		mu 24 4 138 139 140 141
		mu 25 4 138 139 140 141
		mu 26 4 138 139 140 141
		mu 27 4 138 139 140 141
		mu 28 4 138 139 140 141
		mu 29 4 138 139 140 141
		mu 30 4 138 139 140 141
		mu 31 4 150 151 152 153
		mu 32 4 150 151 152 153
		mu 33 4 150 151 152 153
		mu 34 4 150 151 152 153
		f 4 -256 432 439 -438
		mu 0 4 152 153 154 155
		mu 1 4 164 165 166 167
		mu 2 4 164 165 166 167
		mu 3 4 167 168 169 170
		mu 4 4 154 155 156 157
		mu 5 4 152 153 154 155
		mu 6 4 152 153 154 155
		mu 7 4 152 153 154 155
		mu 8 4 160 161 162 163
		mu 9 4 152 153 154 155
		mu 10 4 154 155 156 157
		mu 11 4 162 163 164 165
		mu 12 4 162 163 164 165
		mu 13 4 166 167 168 169
		mu 14 4 162 163 164 165
		mu 15 4 166 167 168 169
		mu 16 4 166 167 168 169
		mu 17 4 150 151 152 153
		mu 18 4 150 151 152 153
		mu 19 4 150 151 152 153
		mu 20 4 150 151 152 153
		mu 21 4 155 156 157 158
		mu 22 4 142 143 144 145
		mu 23 4 142 143 144 145
		mu 24 4 142 143 144 145
		mu 25 4 142 143 144 145
		mu 26 4 142 143 144 145
		mu 27 4 142 143 144 145
		mu 28 4 142 143 144 145
		mu 29 4 142 143 144 145
		mu 30 4 142 143 144 145
		mu 31 4 154 155 156 157
		mu 32 4 154 155 156 157
		mu 33 4 154 155 156 157
		mu 34 4 154 155 156 157
		f 4 256 441 -443 -441
		mu 0 4 156 157 158 159
		mu 1 4 168 169 170 171
		mu 2 4 168 169 170 171
		mu 3 4 171 172 173 174
		mu 4 4 158 159 160 161
		mu 5 4 156 157 158 159
		mu 6 4 156 157 158 159
		mu 7 4 156 157 158 159
		mu 8 4 164 165 166 167
		mu 9 4 156 157 158 159
		mu 10 4 158 159 160 161
		mu 11 4 166 167 168 169
		mu 12 4 166 167 168 169
		mu 13 4 170 171 172 173
		mu 14 4 166 167 168 169
		mu 15 4 170 171 172 173
		mu 16 4 170 171 172 173
		mu 17 4 154 155 156 157
		mu 18 4 154 155 156 157
		mu 19 4 154 155 156 157
		mu 20 4 154 155 156 157
		mu 21 4 159 160 161 162
		mu 22 4 146 147 148 149
		mu 23 4 146 147 148 149
		mu 24 4 146 147 148 149
		mu 25 4 146 147 148 149
		mu 26 4 146 147 148 149
		mu 27 4 146 147 148 149
		mu 28 4 146 147 148 149
		mu 29 4 146 147 148 149
		mu 30 4 146 147 148 149
		mu 31 4 158 159 160 161
		mu 32 4 158 159 160 161
		mu 33 4 158 159 160 161
		mu 34 4 158 159 160 161
		f 4 257 443 -445 -442
		mu 0 4 160 161 162 163
		mu 1 4 172 173 174 175
		mu 2 4 172 173 174 175
		mu 3 4 175 176 177 178
		mu 4 4 162 163 164 165
		mu 5 4 160 161 162 163
		mu 6 4 160 161 162 163
		mu 7 4 160 161 162 163
		mu 8 4 168 169 170 171
		mu 9 4 160 161 162 163
		mu 10 4 162 163 164 165
		mu 11 4 170 171 172 173
		mu 12 4 170 171 172 173
		mu 13 4 174 175 176 177
		mu 14 4 170 171 172 173
		mu 15 4 174 175 176 177
		mu 16 4 174 175 176 177
		mu 17 4 158 159 160 161
		mu 18 4 158 159 160 161
		mu 19 4 158 159 160 161
		mu 20 4 158 159 160 161
		mu 21 4 163 164 165 166
		mu 22 4 150 151 152 153
		mu 23 4 150 151 152 153
		mu 24 4 150 151 152 153
		mu 25 4 150 151 152 153
		mu 26 4 150 151 152 153
		mu 27 4 150 151 152 153
		mu 28 4 150 151 152 153
		mu 29 4 150 151 152 153
		mu 30 4 150 151 152 153
		mu 31 4 162 163 164 165
		mu 32 4 162 163 164 165
		mu 33 4 162 163 164 165
		mu 34 4 162 163 164 165
		f 4 -259 445 446 -444
		mu 0 4 164 165 166 167
		mu 1 4 176 177 178 179
		mu 2 4 176 177 178 179
		mu 3 4 179 180 181 182
		mu 4 4 166 167 168 169
		mu 5 4 164 165 166 167
		mu 6 4 164 165 166 167
		mu 7 4 164 165 166 167
		mu 8 4 172 173 174 175
		mu 9 4 164 165 166 167
		mu 10 4 166 167 168 169
		mu 11 4 174 175 176 177
		mu 12 4 174 175 176 177
		mu 13 4 178 179 180 181
		mu 14 4 174 175 176 177
		mu 15 4 178 179 180 181
		mu 16 4 178 179 180 181
		mu 17 4 162 163 164 165
		mu 18 4 162 163 164 165
		mu 19 4 162 163 164 165
		mu 20 4 162 163 164 165
		mu 21 4 167 168 169 170
		mu 22 4 154 155 156 157
		mu 23 4 154 155 156 157
		mu 24 4 154 155 156 157
		mu 25 4 154 155 156 157
		mu 26 4 154 155 156 157
		mu 27 4 154 155 156 157
		mu 28 4 154 155 156 157
		mu 29 4 154 155 156 157
		mu 30 4 154 155 156 157
		mu 31 4 166 167 168 169
		mu 32 4 166 167 168 169
		mu 33 4 166 167 168 169
		mu 34 4 166 167 168 169
		f 4 -260 440 447 -446
		mu 0 4 168 169 170 171
		mu 1 4 180 181 182 183
		mu 2 4 180 181 182 183
		mu 3 4 183 184 185 186
		mu 4 4 170 171 172 173
		mu 5 4 168 169 170 171
		mu 6 4 168 169 170 171
		mu 7 4 168 169 170 171
		mu 8 4 176 177 178 179
		mu 9 4 168 169 170 171
		mu 10 4 170 171 172 173
		mu 11 4 178 179 180 181
		mu 12 4 178 179 180 181
		mu 13 4 182 183 184 185
		mu 14 4 178 179 180 181
		mu 15 4 182 183 184 185
		mu 16 4 182 183 184 185
		mu 17 4 166 167 168 169
		mu 18 4 166 167 168 169
		mu 19 4 166 167 168 169
		mu 20 4 166 167 168 169
		mu 21 4 171 172 173 174
		mu 22 4 158 159 160 161
		mu 23 4 158 159 160 161
		mu 24 4 158 159 160 161
		mu 25 4 158 159 160 161
		mu 26 4 158 159 160 161
		mu 27 4 158 159 160 161
		mu 28 4 158 159 160 161
		mu 29 4 158 159 160 161
		mu 30 4 158 159 160 161
		mu 31 4 170 171 172 173
		mu 32 4 170 171 172 173
		mu 33 4 170 171 172 173
		mu 34 4 170 171 172 173
		f 4 -387 390 389 -392
		mu 0 4 47 55 51 59
		mu 1 4 59 67 63 71
		mu 2 4 59 67 63 71
		mu 3 4 62 70 66 74
		mu 4 4 49 57 53 61
		mu 5 4 47 55 51 59
		mu 6 4 47 55 51 59
		mu 7 4 47 55 51 59
		mu 8 4 55 63 59 67
		mu 9 4 47 55 51 59
		mu 10 4 49 57 53 61
		mu 11 4 57 65 61 69
		mu 12 4 57 65 61 69
		mu 13 4 61 69 65 73
		mu 14 4 57 65 61 69
		mu 15 4 61 69 65 73
		mu 16 4 61 69 65 73
		mu 17 4 45 53 49 57
		mu 18 4 45 53 49 57
		mu 19 4 45 53 49 57
		mu 20 4 45 53 49 57
		mu 21 4 50 58 54 62
		mu 22 4 37 45 41 49
		mu 23 4 37 45 41 49
		mu 24 4 37 45 41 49
		mu 25 4 37 45 41 49
		mu 26 4 37 45 41 49
		mu 27 4 37 45 41 49
		mu 28 4 37 45 41 49
		mu 29 4 37 45 41 49
		mu 30 4 37 45 41 49
		mu 31 4 49 57 53 61
		mu 32 4 49 57 53 61
		mu 33 4 49 57 53 61
		mu 34 4 49 57 53 61
		f 4 -395 398 397 -400
		mu 0 4 63 71 67 75
		mu 1 4 75 83 79 87
		mu 2 4 75 83 79 87
		mu 3 4 78 86 82 90
		mu 4 4 65 73 69 77
		mu 5 4 63 71 67 75
		mu 6 4 63 71 67 75
		mu 7 4 63 71 67 75
		mu 8 4 71 79 75 83
		mu 9 4 63 71 67 75
		mu 10 4 65 73 69 77
		mu 11 4 73 81 77 85
		mu 12 4 73 81 77 85
		mu 13 4 77 85 81 89
		mu 14 4 73 81 77 85
		mu 15 4 77 85 81 89
		mu 16 4 77 85 81 89
		mu 17 4 61 69 65 73
		mu 18 4 61 69 65 73
		mu 19 4 61 69 65 73
		mu 20 4 61 69 65 73
		mu 21 4 66 74 70 78
		mu 22 4 53 61 57 65
		mu 23 4 53 61 57 65
		mu 24 4 53 61 57 65
		mu 25 4 53 61 57 65
		mu 26 4 53 61 57 65
		mu 27 4 53 61 57 65
		mu 28 4 53 61 57 65
		mu 29 4 53 61 57 65
		mu 30 4 53 61 57 65
		mu 31 4 65 73 69 77
		mu 32 4 65 73 69 77
		mu 33 4 65 73 69 77
		mu 34 4 65 73 69 77
		f 4 -403 406 405 -408
		mu 0 4 79 87 83 91
		mu 1 4 91 99 95 103
		mu 2 4 91 99 95 103
		mu 3 4 94 102 98 106
		mu 4 4 81 89 85 93
		mu 5 4 79 87 83 91
		mu 6 4 79 87 83 91
		mu 7 4 79 87 83 91
		mu 8 4 87 95 91 99
		mu 9 4 79 87 83 91
		mu 10 4 81 89 85 93
		mu 11 4 89 97 93 101
		mu 12 4 89 97 93 101
		mu 13 4 93 101 97 105
		mu 14 4 89 97 93 101
		mu 15 4 93 101 97 105
		mu 16 4 93 101 97 105
		mu 17 4 77 85 81 89
		mu 18 4 77 85 81 89
		mu 19 4 77 85 81 89
		mu 20 4 77 85 81 89
		mu 21 4 82 90 86 94
		mu 22 4 69 77 73 81
		mu 23 4 69 77 73 81
		mu 24 4 69 77 73 81
		mu 25 4 69 77 73 81
		mu 26 4 69 77 73 81
		mu 27 4 69 77 73 81
		mu 28 4 69 77 73 81
		mu 29 4 69 77 73 81
		mu 30 4 69 77 73 81
		mu 31 4 81 89 85 93
		mu 32 4 81 89 85 93
		mu 33 4 81 89 85 93
		mu 34 4 81 89 85 93
		f 4 -411 414 413 -416
		mu 0 4 95 103 99 107
		mu 1 4 107 115 111 119
		mu 2 4 107 115 111 119
		mu 3 4 110 118 114 122
		mu 4 4 97 105 101 109
		mu 5 4 95 103 99 107
		mu 6 4 95 103 99 107
		mu 7 4 95 103 99 107
		mu 8 4 103 111 107 115
		mu 9 4 95 103 99 107
		mu 10 4 97 105 101 109
		mu 11 4 105 113 109 117
		mu 12 4 105 113 109 117
		mu 13 4 109 117 113 121
		mu 14 4 105 113 109 117
		mu 15 4 109 117 113 121
		mu 16 4 109 117 113 121
		mu 17 4 93 101 97 105
		mu 18 4 93 101 97 105
		mu 19 4 93 101 97 105
		mu 20 4 93 101 97 105
		mu 21 4 98 106 102 110
		mu 22 4 85 93 89 97
		mu 23 4 85 93 89 97
		mu 24 4 85 93 89 97
		mu 25 4 85 93 89 97
		mu 26 4 85 93 89 97
		mu 27 4 85 93 89 97
		mu 28 4 85 93 89 97
		mu 29 4 85 93 89 97
		mu 30 4 85 93 89 97
		mu 31 4 97 105 101 109
		mu 32 4 97 105 101 109
		mu 33 4 97 105 101 109
		mu 34 4 97 105 101 109
		f 4 420 -423 -424 418
		mu 0 4 115 119 123 111
		mu 1 4 127 131 135 123
		mu 2 4 127 131 135 123
		mu 3 4 130 134 138 126
		mu 4 4 117 121 125 113
		mu 5 4 115 119 123 111
		mu 6 4 115 119 123 111
		mu 7 4 115 119 123 111
		mu 8 4 123 127 131 119
		mu 9 4 115 119 123 111
		mu 10 4 117 121 125 113
		mu 11 4 125 129 133 121
		mu 12 4 125 129 133 121
		mu 13 4 129 133 137 125
		mu 14 4 125 129 133 121
		mu 15 4 129 133 137 125
		mu 16 4 129 133 137 125
		mu 17 4 113 117 121 109
		mu 18 4 113 117 121 109
		mu 19 4 113 117 121 109
		mu 20 4 113 117 121 109
		mu 21 4 118 122 126 114
		mu 22 4 105 109 113 101
		mu 23 4 105 109 113 101
		mu 24 4 105 109 113 101
		mu 25 4 105 109 113 101
		mu 26 4 105 109 113 101
		mu 27 4 105 109 113 101
		mu 28 4 105 109 113 101
		mu 29 4 105 109 113 101
		mu 30 4 105 109 113 101
		mu 31 4 117 121 125 113
		mu 32 4 117 121 125 113
		mu 33 4 117 121 125 113
		mu 34 4 117 121 125 113
		f 4 428 -431 -432 426
		mu 0 4 131 135 139 127
		mu 1 4 143 147 151 139
		mu 2 4 143 147 151 139
		mu 3 4 146 150 154 142
		mu 4 4 133 137 141 129
		mu 5 4 131 135 139 127
		mu 6 4 131 135 139 127
		mu 7 4 131 135 139 127
		mu 8 4 139 143 147 135
		mu 9 4 131 135 139 127
		mu 10 4 133 137 141 129
		mu 11 4 141 145 149 137
		mu 12 4 141 145 149 137
		mu 13 4 145 149 153 141
		mu 14 4 141 145 149 137
		mu 15 4 145 149 153 141
		mu 16 4 145 149 153 141
		mu 17 4 129 133 137 125
		mu 18 4 129 133 137 125
		mu 19 4 129 133 137 125
		mu 20 4 129 133 137 125
		mu 21 4 134 138 142 130
		mu 22 4 121 125 129 117
		mu 23 4 121 125 129 117
		mu 24 4 121 125 129 117
		mu 25 4 121 125 129 117
		mu 26 4 121 125 129 117
		mu 27 4 121 125 129 117
		mu 28 4 121 125 129 117
		mu 29 4 121 125 129 117
		mu 30 4 121 125 129 117
		mu 31 4 133 137 141 129
		mu 32 4 133 137 141 129
		mu 33 4 133 137 141 129
		mu 34 4 133 137 141 129
		f 4 436 -439 -440 434
		mu 0 4 147 151 155 143
		mu 1 4 159 163 167 155
		mu 2 4 159 163 167 155
		mu 3 4 162 166 170 158
		mu 4 4 149 153 157 145
		mu 5 4 147 151 155 143
		mu 6 4 147 151 155 143
		mu 7 4 147 151 155 143
		mu 8 4 155 159 163 151
		mu 9 4 147 151 155 143
		mu 10 4 149 153 157 145
		mu 11 4 157 161 165 153
		mu 12 4 157 161 165 153
		mu 13 4 161 165 169 157
		mu 14 4 157 161 165 153
		mu 15 4 161 165 169 157
		mu 16 4 161 165 169 157
		mu 17 4 145 149 153 141
		mu 18 4 145 149 153 141
		mu 19 4 145 149 153 141
		mu 20 4 145 149 153 141
		mu 21 4 150 154 158 146
		mu 22 4 137 141 145 133
		mu 23 4 137 141 145 133
		mu 24 4 137 141 145 133
		mu 25 4 137 141 145 133
		mu 26 4 137 141 145 133
		mu 27 4 137 141 145 133
		mu 28 4 137 141 145 133
		mu 29 4 137 141 145 133
		mu 30 4 137 141 145 133
		mu 31 4 149 153 157 145
		mu 32 4 149 153 157 145
		mu 33 4 149 153 157 145
		mu 34 4 149 153 157 145
		f 4 444 -447 -448 442
		mu 0 4 163 167 171 159
		mu 1 4 175 179 183 171
		mu 2 4 175 179 183 171
		mu 3 4 178 182 186 174
		mu 4 4 165 169 173 161
		mu 5 4 163 167 171 159
		mu 6 4 163 167 171 159
		mu 7 4 163 167 171 159
		mu 8 4 171 175 179 167
		mu 9 4 163 167 171 159
		mu 10 4 165 169 173 161
		mu 11 4 173 177 181 169
		mu 12 4 173 177 181 169
		mu 13 4 177 181 185 173
		mu 14 4 173 177 181 169
		mu 15 4 177 181 185 173
		mu 16 4 177 181 185 173
		mu 17 4 161 165 169 157
		mu 18 4 161 165 169 157
		mu 19 4 161 165 169 157
		mu 20 4 161 165 169 157
		mu 21 4 166 170 174 162
		mu 22 4 153 157 161 149
		mu 23 4 153 157 161 149
		mu 24 4 153 157 161 149
		mu 25 4 153 157 161 149
		mu 26 4 153 157 161 149
		mu 27 4 153 157 161 149
		mu 28 4 153 157 161 149
		mu 29 4 153 157 161 149
		mu 30 4 153 157 161 149
		mu 31 4 165 169 173 161
		mu 32 4 165 169 173 161
		mu 33 4 165 169 173 161
		mu 34 4 165 169 173 161
		f 4 475 464 82 -470
		mu 9 4 182 176 1 173
		f 4 181 185 -458 -83
		mu 33 4 0 1 175 174
		f 4 -189 -452 -459 -186
		mu 33 4 1 10 176 175
		f 4 -460 451 -185 -453
		mu 33 4 177 176 6 7
		f 4 471 -461 452 -466
		mu 9 4 178 179 174 6
		f 4 -462 453 192 -455
		mu 34 4 176 175 6 7
		f 4 193 -463 454 196
		mu 34 4 3 177 176 11
		f 4 81 -464 -194 -190
		mu 34 4 0 174 177 3
		f 4 80 -471 -78 -88
		mu 9 4 7 178 177 9
		f 4 -467 -472 -81 -454
		mu 9 4 175 179 178 7
		f 4 79 -473 506 -86
		mu 9 4 5 180 179 175
		f 4 78 -474 -80 -84
		mu 9 4 3 181 180 5
		f 4 502 -475 -79 -449
		mu 9 4 172 182 181 3
		f 4 77 -476 -457 -82
		mu 9 4 0 176 182 172
		f 4 -479 368 375 480
		mu 0 4 172 29 30 174
		mu 1 4 184 41 42 186
		mu 2 4 184 41 42 186
		mu 3 4 187 44 45 189
		mu 4 4 174 31 32 176
		mu 5 4 172 29 30 174
		mu 6 4 172 29 30 174
		mu 7 4 172 29 30 174
		mu 8 4 180 37 38 182
		mu 9 4 183 29 30 185
		mu 10 4 174 31 32 176
		mu 11 4 182 39 40 184
		mu 12 4 182 39 40 184
		mu 13 4 186 43 44 188
		mu 14 4 182 39 40 184
		mu 15 4 186 43 44 188
		mu 16 4 186 43 44 188
		mu 17 4 170 27 28 172
		mu 18 4 170 27 28 172
		mu 19 4 170 27 28 172
		mu 20 4 170 27 28 172
		mu 21 4 175 32 33 177
		mu 22 4 162 19 20 164
		mu 23 4 162 19 20 164
		mu 24 4 162 19 20 164
		mu 25 4 162 19 20 164
		mu 26 4 162 19 20 164
		mu 27 4 162 19 20 164
		mu 28 4 162 19 20 164
		mu 29 4 162 19 20 164
		mu 30 4 162 19 20 164
		mu 31 4 174 31 32 176
		mu 32 4 174 31 32 176
		mu 33 4 178 31 32 180
		mu 34 4 178 31 32 180
		f 4 -495 -498 499 500
		mu 0 4 182 181 184 185
		mu 1 4 194 193 196 197
		mu 2 4 194 193 196 197
		mu 3 4 197 196 199 200
		mu 4 4 184 183 186 187
		mu 5 4 182 181 184 185
		mu 6 4 182 181 184 185
		mu 7 4 182 181 184 185
		mu 8 4 190 189 192 193
		mu 9 4 193 192 195 196
		mu 10 4 184 183 186 187
		mu 11 4 192 191 194 195
		mu 12 4 192 191 194 195
		mu 13 4 196 195 198 199
		mu 14 4 192 191 194 195
		mu 15 4 196 195 198 199
		mu 16 4 196 195 198 199
		mu 17 4 180 179 182 183
		mu 18 4 180 179 182 183
		mu 19 4 180 179 182 183
		mu 20 4 180 179 182 183
		mu 21 4 185 184 187 188
		mu 22 4 172 171 174 175
		mu 23 4 172 171 174 175
		mu 24 4 172 171 174 175
		mu 25 4 172 171 174 175
		mu 26 4 172 171 174 175
		mu 27 4 172 171 174 175
		mu 28 4 172 171 174 175
		mu 29 4 172 171 174 175
		mu 30 4 172 171 174 175
		mu 31 4 184 183 186 187
		mu 32 4 184 183 186 187
		mu 33 4 188 187 190 191
		mu 34 4 188 187 190 191
		f 4 -480 -482 -377 -374
		mu 0 4 32 173 175 35
		mu 1 4 44 185 187 47
		mu 2 4 44 185 187 47
		mu 3 4 47 188 190 50
		mu 4 4 34 175 177 37
		mu 5 4 32 173 175 35
		mu 6 4 32 173 175 35
		mu 7 4 32 173 175 35
		mu 8 4 40 181 183 43
		mu 9 4 32 184 186 35
		mu 10 4 34 175 177 37
		mu 11 4 42 183 185 45
		mu 12 4 42 183 185 45
		mu 13 4 46 187 189 49
		mu 14 4 42 183 185 45
		mu 15 4 46 187 189 49
		mu 16 4 46 187 189 49
		mu 17 4 30 171 173 33
		mu 18 4 30 171 173 33
		mu 19 4 30 171 173 33
		mu 20 4 30 171 173 33
		mu 21 4 35 176 178 38
		mu 22 4 22 163 165 25
		mu 23 4 22 163 165 25
		mu 24 4 22 163 165 25
		mu 25 4 22 163 165 25
		mu 26 4 22 163 165 25
		mu 27 4 22 163 165 25
		mu 28 4 22 163 165 25
		mu 29 4 22 163 165 25
		mu 30 4 22 163 165 25
		mu 31 4 34 175 177 37
		mu 32 4 34 175 177 37
		mu 33 4 34 179 181 37
		mu 34 4 34 179 181 37
		f 4 -483 -376 383 376
		mu 0 4 175 174 30 35
		mu 1 4 187 186 42 47
		mu 2 4 187 186 42 47
		mu 3 4 190 189 45 50
		mu 4 4 177 176 32 37
		mu 5 4 175 174 30 35
		mu 6 4 175 174 30 35
		mu 7 4 175 174 30 35
		mu 8 4 183 182 38 43
		mu 9 4 186 185 30 35
		mu 10 4 177 176 32 37
		mu 11 4 185 184 40 45
		mu 12 4 185 184 40 45
		mu 13 4 189 188 44 49
		mu 14 4 185 184 40 45
		mu 15 4 189 188 44 49
		mu 16 4 189 188 44 49
		mu 17 4 173 172 28 33
		mu 18 4 173 172 28 33
		mu 19 4 173 172 28 33
		mu 20 4 173 172 28 33
		mu 21 4 178 177 33 38
		mu 22 4 165 164 20 25
		mu 23 4 165 164 20 25
		mu 24 4 165 164 20 25
		mu 25 4 165 164 20 25
		mu 26 4 165 164 20 25
		mu 27 4 165 164 20 25
		mu 28 4 165 164 20 25
		mu 29 4 165 164 20 25
		mu 30 4 165 164 20 25
		mu 31 4 177 176 32 37
		mu 32 4 177 176 32 37
		mu 33 4 181 180 32 37
		mu 34 4 181 180 32 37
		f 4 -368 483 485 -485
		mu 0 4 17 16 177 176
		mu 1 4 29 28 189 188
		mu 2 4 29 28 189 188
		mu 3 4 32 31 192 191
		mu 4 4 19 18 179 178
		mu 5 4 17 16 177 176
		mu 6 4 17 16 177 176
		mu 7 4 17 16 177 176
		mu 8 4 25 24 185 184
		mu 9 4 17 16 188 187
		mu 10 4 19 18 179 178
		mu 11 4 27 26 187 186
		mu 12 4 27 26 187 186
		mu 13 4 31 30 191 190
		mu 14 4 27 26 187 186
		mu 15 4 31 30 191 190
		mu 16 4 31 30 191 190
		mu 17 4 15 14 175 174
		mu 18 4 15 14 175 174
		mu 19 4 15 14 175 174
		mu 20 4 15 14 175 174
		mu 21 4 20 19 180 179
		mu 22 4 7 6 167 166
		mu 23 4 7 6 167 166
		mu 24 4 7 6 167 166
		mu 25 4 7 6 167 166
		mu 26 4 7 6 167 166
		mu 27 4 7 6 167 166
		mu 28 4 7 6 167 166
		mu 29 4 7 6 167 166
		mu 30 4 7 6 167 166
		mu 31 4 19 18 179 178
		mu 32 4 19 18 179 178
		mu 33 4 19 18 183 182
		mu 34 4 19 18 183 182
		f 4 372 488 -490 -487
		mu 0 4 24 23 179 178
		mu 1 4 36 35 191 190
		mu 2 4 36 35 191 190
		mu 3 4 39 38 194 193
		mu 4 4 26 25 181 180
		mu 5 4 24 23 179 178
		mu 6 4 24 23 179 178
		mu 7 4 24 23 179 178
		mu 8 4 32 31 187 186
		mu 9 4 24 23 190 189
		mu 10 4 26 25 181 180
		mu 11 4 34 33 189 188
		mu 12 4 34 33 189 188
		mu 13 4 38 37 193 192
		mu 14 4 34 33 189 188
		mu 15 4 38 37 193 192
		mu 16 4 38 37 193 192
		mu 17 4 22 21 177 176
		mu 18 4 22 21 177 176
		mu 19 4 22 21 177 176
		mu 20 4 22 21 177 176
		mu 21 4 27 26 182 181
		mu 22 4 14 13 169 168
		mu 23 4 14 13 169 168
		mu 24 4 14 13 169 168
		mu 25 4 14 13 169 168
		mu 26 4 14 13 169 168
		mu 27 4 14 13 169 168
		mu 28 4 14 13 169 168
		mu 29 4 14 13 169 168
		mu 30 4 14 13 169 168
		mu 31 4 26 25 181 180
		mu 32 4 26 25 181 180
		mu 33 4 26 25 185 184
		mu 34 4 26 25 185 184
		f 4 -383 484 490 -489
		mu 0 4 23 17 176 179
		mu 1 4 35 29 188 191
		mu 2 4 35 29 188 191
		mu 3 4 38 32 191 194
		mu 4 4 25 19 178 181
		mu 5 4 23 17 176 179
		mu 6 4 23 17 176 179
		mu 7 4 23 17 176 179
		mu 8 4 31 25 184 187
		mu 9 4 23 17 187 190
		mu 10 4 25 19 178 181
		mu 11 4 33 27 186 189
		mu 12 4 33 27 186 189
		mu 13 4 37 31 190 193
		mu 14 4 33 27 186 189
		mu 15 4 37 31 190 193
		mu 16 4 37 31 190 193
		mu 17 4 21 15 174 177
		mu 18 4 21 15 174 177
		mu 19 4 21 15 174 177
		mu 20 4 21 15 174 177
		mu 21 4 26 20 179 182
		mu 22 4 13 7 166 169
		mu 23 4 13 7 166 169
		mu 24 4 13 7 166 169
		mu 25 4 13 7 166 169
		mu 26 4 13 7 166 169
		mu 27 4 13 7 166 169
		mu 28 4 13 7 166 169
		mu 29 4 13 7 166 169
		mu 30 4 13 7 166 169
		mu 31 4 25 19 178 181
		mu 32 4 25 19 178 181
		mu 33 4 25 19 182 185
		mu 34 4 25 19 182 185
		f 4 -477 491 492 -484
		mu 0 4 31 172 181 180
		mu 1 4 43 184 193 192
		mu 2 4 43 184 193 192
		mu 3 4 46 187 196 195
		mu 4 4 33 174 183 182
		mu 5 4 31 172 181 180
		mu 6 4 31 172 181 180
		mu 7 4 31 172 181 180
		mu 8 4 39 180 189 188
		mu 9 4 31 183 192 191
		mu 10 4 33 174 183 182
		mu 11 4 41 182 191 190
		mu 12 4 41 182 191 190
		mu 13 4 45 186 195 194
		mu 14 4 41 182 191 190
		mu 15 4 45 186 195 194
		mu 16 4 45 186 195 194
		mu 17 4 29 170 179 178
		mu 18 4 29 170 179 178
		mu 19 4 29 170 179 178
		mu 20 4 29 170 179 178
		mu 21 4 34 175 184 183
		mu 22 4 21 162 171 170
		mu 23 4 21 162 171 170
		mu 24 4 21 162 171 170
		mu 25 4 21 162 171 170
		mu 26 4 21 162 171 170
		mu 27 4 21 162 171 170
		mu 28 4 21 162 171 170
		mu 29 4 21 162 171 170
		mu 30 4 21 162 171 170
		mu 31 4 33 174 183 182
		mu 32 4 33 174 183 182
		mu 33 4 33 178 187 186
		mu 34 4 33 178 187 186
		f 4 477 486 -496 -494
		mu 0 4 173 34 183 182
		mu 1 4 185 46 195 194
		mu 2 4 185 46 195 194
		mu 3 4 188 49 198 197
		mu 4 4 175 36 185 184
		mu 5 4 173 34 183 182
		mu 6 4 173 34 183 182
		mu 7 4 173 34 183 182
		mu 8 4 181 42 191 190
		mu 9 4 184 34 194 193
		mu 10 4 175 36 185 184
		mu 11 4 183 44 193 192
		mu 12 4 183 44 193 192
		mu 13 4 187 48 197 196
		mu 14 4 183 44 193 192
		mu 15 4 187 48 197 196
		mu 16 4 187 48 197 196
		mu 17 4 171 32 181 180
		mu 18 4 171 32 181 180
		mu 19 4 171 32 181 180
		mu 20 4 171 32 181 180
		mu 21 4 176 37 186 185
		mu 22 4 163 24 173 172
		mu 23 4 163 24 173 172
		mu 24 4 163 24 173 172
		mu 25 4 163 24 173 172
		mu 26 4 163 24 173 172
		mu 27 4 163 24 173 172
		mu 28 4 163 24 173 172
		mu 29 4 163 24 173 172
		mu 30 4 163 24 173 172
		mu 31 4 175 36 185 184
		mu 32 4 175 36 185 184
		mu 33 4 179 36 189 188
		mu 34 4 179 36 189 188
		f 4 -481 496 497 -492
		mu 0 4 172 174 184 181
		mu 1 4 184 186 196 193
		mu 2 4 184 186 196 193
		mu 3 4 187 189 199 196
		mu 4 4 174 176 186 183
		mu 5 4 172 174 184 181
		mu 6 4 172 174 184 181
		mu 7 4 172 174 184 181
		mu 8 4 180 182 192 189
		mu 9 4 183 185 195 192
		mu 10 4 174 176 186 183
		mu 11 4 182 184 194 191
		mu 12 4 182 184 194 191
		mu 13 4 186 188 198 195
		mu 14 4 182 184 194 191
		mu 15 4 186 188 198 195
		mu 16 4 186 188 198 195
		mu 17 4 170 172 182 179
		mu 18 4 170 172 182 179
		mu 19 4 170 172 182 179
		mu 20 4 170 172 182 179
		mu 21 4 175 177 187 184
		mu 22 4 162 164 174 171
		mu 23 4 162 164 174 171
		mu 24 4 162 164 174 171
		mu 25 4 162 164 174 171
		mu 26 4 162 164 174 171
		mu 27 4 162 164 174 171
		mu 28 4 162 164 174 171
		mu 29 4 162 164 174 171
		mu 30 4 162 164 174 171
		mu 31 4 174 176 186 183
		mu 32 4 174 176 186 183
		mu 33 4 178 180 190 187
		mu 34 4 178 180 190 187
		f 4 482 498 -500 -497
		mu 0 4 174 175 185 184
		mu 1 4 186 187 197 196
		mu 2 4 186 187 197 196
		mu 3 4 189 190 200 199
		mu 4 4 176 177 187 186
		mu 5 4 174 175 185 184
		mu 6 4 174 175 185 184
		mu 7 4 174 175 185 184
		mu 8 4 182 183 193 192
		mu 9 4 185 186 196 195
		mu 10 4 176 177 187 186
		mu 11 4 184 185 195 194
		mu 12 4 184 185 195 194
		mu 13 4 188 189 199 198
		mu 14 4 184 185 195 194
		mu 15 4 188 189 199 198
		mu 16 4 188 189 199 198
		mu 17 4 172 173 183 182
		mu 18 4 172 173 183 182
		mu 19 4 172 173 183 182
		mu 20 4 172 173 183 182
		mu 21 4 177 178 188 187
		mu 22 4 164 165 175 174
		mu 23 4 164 165 175 174
		mu 24 4 164 165 175 174
		mu 25 4 164 165 175 174
		mu 26 4 164 165 175 174
		mu 27 4 164 165 175 174
		mu 28 4 164 165 175 174
		mu 29 4 164 165 175 174
		mu 30 4 164 165 175 174
		mu 31 4 176 177 187 186
		mu 32 4 176 177 187 186
		mu 33 4 180 181 191 190
		mu 34 4 180 181 191 190
		f 4 481 493 -501 -499
		mu 0 4 175 173 182 185
		mu 1 4 187 185 194 197
		mu 2 4 187 185 194 197
		mu 3 4 190 188 197 200
		mu 4 4 177 175 184 187
		mu 5 4 175 173 182 185
		mu 6 4 175 173 182 185
		mu 7 4 175 173 182 185
		mu 8 4 183 181 190 193
		mu 9 4 186 184 193 196
		mu 10 4 177 175 184 187
		mu 11 4 185 183 192 195
		mu 12 4 185 183 192 195
		mu 13 4 189 187 196 199
		mu 14 4 185 183 192 195
		mu 15 4 189 187 196 199
		mu 16 4 189 187 196 199
		mu 17 4 173 171 180 183
		mu 18 4 173 171 180 183
		mu 19 4 173 171 180 183
		mu 20 4 173 171 180 183
		mu 21 4 178 176 185 188
		mu 22 4 165 163 172 175
		mu 23 4 165 163 172 175
		mu 24 4 165 163 172 175
		mu 25 4 165 163 172 175
		mu 26 4 165 163 172 175
		mu 27 4 165 163 172 175
		mu 28 4 165 163 172 175
		mu 29 4 165 163 172 175
		mu 30 4 165 163 172 175
		mu 31 4 177 175 184 187
		mu 32 4 177 175 184 187
		mu 33 4 181 179 188 191
		mu 34 4 181 179 188 191
		f 4 463 511 461 462
		mu 34 4 177 174 175 176
		f 4 456 512 466 -512
		mu 9 4 172 182 179 175
		f 4 469 513 460 -513
		mu 9 4 182 173 174 179
		f 4 457 458 459 -514
		mu 33 4 174 175 176 177
		f 4 -511 514 -509 -510
		mu 33 4 175 174 177 176
		f 4 -502 515 -508 -515
		mu 9 4 173 182 179 174
		f 4 -503 516 -507 -516
		mu 9 4 182 172 175 179
		f 4 -504 -505 -506 -517
		mu 34 4 174 177 176 175
		f 4 169 166 -522 -166
		mu 31 4 0 1 190 189
		f 4 170 167 -523 -167
		mu 31 4 1 4 191 190
		f 4 171 168 -524 -168
		mu 31 4 4 6 192 191
		f 4 172 165 -525 -169
		mu 31 4 6 8 188 192
		f 4 176 -530 -174 -181
		mu 32 4 7 190 189 9
		f 4 175 -531 -177 -180
		mu 32 4 5 191 190 7
		f 4 174 -532 -176 -179
		mu 32 4 3 192 191 5
		f 4 173 -533 -175 -178
		mu 32 4 0 188 192 3
		f 4 -358 352 -540 -361
		mu 11 4 9 17 198 197
		f 4 -541 -353 377 378
		mu 0 4 187 186 37 38
		mu 1 4 199 198 49 50
		mu 2 4 199 198 49 50
		mu 3 4 202 201 52 53
		mu 4 4 189 188 39 40
		mu 5 4 187 186 37 38
		mu 6 4 187 186 37 38
		mu 7 4 187 186 37 38
		mu 8 4 195 194 45 46
		mu 9 4 198 197 37 38
		mu 10 4 189 188 39 40
		mu 11 4 200 199 47 48
		mu 12 4 197 196 47 48
		mu 13 4 201 200 51 52
		mu 14 4 197 196 47 48
		mu 15 4 201 200 51 52
		mu 16 4 201 200 51 52
		mu 17 4 185 184 35 36
		mu 18 4 185 184 35 36
		mu 19 4 185 184 35 36
		mu 20 4 185 184 35 36
		mu 21 4 190 189 40 41
		mu 22 4 177 176 27 28
		mu 23 4 177 176 27 28
		mu 24 4 177 176 27 28
		mu 25 4 177 176 27 28
		mu 26 4 177 176 27 28
		mu 27 4 177 176 27 28
		mu 28 4 177 176 27 28
		mu 29 4 177 176 27 28
		mu 30 4 177 176 27 28
		mu 31 4 194 193 39 40
		mu 32 4 194 193 39 40
		mu 33 4 193 192 39 40
		mu 34 4 193 192 39 40
		f 4 -382 380 -542 -379
		mu 0 4 38 41 188 187
		mu 1 4 50 53 200 199
		mu 2 4 50 53 200 199
		mu 3 4 53 56 203 202
		mu 4 4 40 43 190 189
		mu 5 4 38 41 188 187
		mu 6 4 38 41 188 187
		mu 7 4 38 41 188 187
		mu 8 4 46 49 196 195
		mu 9 4 38 41 199 198
		mu 10 4 40 43 190 189
		mu 11 4 48 51 201 200
		mu 12 4 48 51 198 197
		mu 13 4 52 55 202 201
		mu 14 4 48 51 198 197
		mu 15 4 52 55 202 201
		mu 16 4 52 55 202 201
		mu 17 4 36 39 186 185
		mu 18 4 36 39 186 185
		mu 19 4 36 39 186 185
		mu 20 4 36 39 186 185
		mu 21 4 41 44 191 190
		mu 22 4 28 31 178 177
		mu 23 4 28 31 178 177
		mu 24 4 28 31 178 177
		mu 25 4 28 31 178 177
		mu 26 4 28 31 178 177
		mu 27 4 28 31 178 177
		mu 28 4 28 31 178 177
		mu 29 4 28 31 178 177
		mu 30 4 28 31 178 177
		mu 31 4 40 43 195 194
		mu 32 4 40 43 195 194
		mu 33 4 40 43 194 193
		mu 34 4 40 43 194 193
		f 4 353 -543 -381 -380
		mu 0 4 39 189 188 41
		mu 1 4 51 201 200 53
		mu 2 4 51 201 200 53
		mu 3 4 54 204 203 56
		mu 4 4 41 191 190 43
		mu 5 4 39 189 188 41
		mu 6 4 39 189 188 41
		mu 7 4 39 189 188 41
		mu 8 4 47 197 196 49
		mu 9 4 39 200 199 41
		mu 10 4 41 191 190 43
		mu 11 4 49 202 201 51
		mu 12 4 49 199 198 51
		mu 13 4 53 203 202 55
		mu 14 4 49 199 198 51
		mu 15 4 53 203 202 55
		mu 16 4 53 203 202 55
		mu 17 4 37 187 186 39
		mu 18 4 37 187 186 39
		mu 19 4 37 187 186 39
		mu 20 4 37 187 186 39
		mu 21 4 42 192 191 44
		mu 22 4 29 179 178 31
		mu 23 4 29 179 178 31
		mu 24 4 29 179 178 31
		mu 25 4 29 179 178 31
		mu 26 4 29 179 178 31
		mu 27 4 29 179 178 31
		mu 28 4 29 179 178 31
		mu 29 4 29 179 178 31
		mu 30 4 29 179 178 31
		mu 31 4 41 196 195 43
		mu 32 4 41 196 195 43
		mu 33 4 41 195 194 43
		mu 34 4 41 195 194 43
		f 4 -356 -539 -544 -354
		mu 21 4 9 10 195 193
		f 4 277 -546 -557 -274
		mu 21 4 4 2 197 196
		f 4 -558 545 46 -547
		mu 7 4 191 190 1 2
		f 4 -559 546 48 -548
		mu 7 4 192 191 2 4
		f 4 -560 547 50 -549
		mu 7 4 193 192 4 6
		f 4 -561 548 52 -550
		mu 7 4 194 193 6 8
		f 4 -562 549 42 54
		f 4 57 -563 -55 -54
		mu 6 4 7 191 190 9
		f 4 56 -564 -58 -52
		mu 6 4 5 192 191 7
		f 4 55 -565 -57 -50
		mu 6 4 3 193 192 5
		f 4 276 -566 -56 -48
		mu 6 4 0 194 193 3
		f 4 278 274 -567 -277
		mu 11 4 5 8 204 203
		f 4 -568 544 -162 361
		mu 21 4 194 196 5 8
		f 5 360 -569 -275 275 -359
		mu 11 5 14 196 204 8 16;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 35 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[2]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[3]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[4]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[5]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[6]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[7]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[8]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[9]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[10]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[11]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[12]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[13]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[14]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[15]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[16]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[17]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[18]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[19]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[20]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[21]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[22]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[23]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[24]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[25]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[26]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[27]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[28]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[29]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[30]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[31]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[32]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[33]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[34]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Legs" -p "BasicMechGrouped1";
	rename -uid "49793D68-434D-3303-859B-0A82D42068D6";
	setAttr ".rp" -type "double3" 0 8.2525711059570312 1.4767951965332031 ;
	setAttr ".sp" -type "double3" 0 8.2525711059570312 1.4767951965332031 ;
createNode transform -n "hips" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "F2146221-48BC-34A1-6418-42A6017C8FA0";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 11.000007629394531 0 ;
	setAttr ".sp" -type "double3" 0 11.000007629394531 0 ;
createNode mesh -n "hipsShape" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "FB2CC886-4418-C822-AD21-A9B3C49A12E0";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875312738120556 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "hipsShapeOrig" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "A7A98C44-4F0C-B4EA-B7E2-ED857909E6B2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.42833641 0.5 0.375
		 0.64426696 0.125 0.078117192 0.18749374 -3.7252903e-009 0.32166359 0.25 0.32166359
		 0.10573305 0.29041356 0.066670537 0.42833641 0.93750626 0.375 0.91541356 0.375 0.83458638
		 0.17833641 0.10573304 0.20958623 0.066670544 0.17833641 0.25 0.375 0.30333641 0.625
		 0.30333641 0.625 0.44666359 0.375 0.44666359 0.875 0.25 0.82166356 0.25 0.875 0.078117192
		 0.82166362 0.10573305 0.81250626 -3.7252903e-009 0.57166356 0.25 0.60749179 -3.7252903e-009
		 0.79041356 0.066670537 0.67833638 0.10573304 0.70958626 0.066670544 0.67833638 0.25
		 0.42833635 0.078117192 0.42833641 0.25 0.57166356 0.5 0.57166356 0.67188281 0.42833635
		 0.81249374 0.57166356 0.93750626 0.39250818 -3.7252903e-009 0.57166356 0.078117184
		 0.42833641 0.67188281 0.57166356 0.81249374;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  -5 -18 -2.86654329 -2.86654329 -18 -5 -5 -22.61654282 -2.86654329
		 -2.86654329 -23.50024986 -5 -2.86654329 -18 5 -5 -18 2.86654329 -5 -22.61654282 2.86654329
		 -2.86654329 -23.50024986 5 -5 -23.86654282 1.61654329 -2.86654329 -26 2.5002501 -5 -23.86654282 -1.61654329
		 -2.86654329 -26 -2.5002501 2.86654329 -18 -5 5 -18 -2.86654329 5 -22.61654282 -2.86654329
		 2.86654329 -23.50024986 -5 2.86654329 -18 5 5 -18 2.86654329 5 -22.61654282 2.86654329
		 2.86654329 -23.50024986 5 5 -23.86654282 1.61654329 2.86654329 -26 2.5002501 5 -23.86654282 -1.61654329
		 2.86654329 -26 -2.5002501;
	setAttr -s 40 ".ed[0:39]"  1 12 0 1 0 0 4 16 0 5 0 0 4 5 0 1 3 0 3 2 0
		 2 0 0 3 11 0 11 10 0 10 2 0 5 6 0 6 7 0 7 4 0 6 8 0 8 9 0 9 7 0 8 10 0 11 9 0 13 12 0
		 17 13 0 16 17 0 13 14 0 14 15 0 15 12 0 14 22 0 22 23 0 23 15 0 16 19 0 19 18 0 18 17 0
		 19 21 0 21 20 0 20 18 0 21 23 0 22 20 0 7 19 0 15 3 0 11 23 0 21 9 0;
	setAttr -s 18 -ch 80 ".fc[0:17]" -type "polyFaces" 
		f 8 -5 2 21 20 19 -1 1 -4
		mu 0 8 13 29 22 14 15 30 0 16
		f 4 -2 5 6 7
		mu 0 4 16 0 36 1
		f 4 -7 8 9 10
		mu 0 4 10 2 3 11
		f 4 4 11 12 13
		mu 0 4 29 4 5 28
		f 4 -13 14 15 16
		mu 0 4 28 5 6 34
		f 4 -16 17 -10 18
		mu 0 4 7 8 9 32
		f 6 -11 -18 -15 -12 3 -8
		mu 0 6 10 11 6 5 4 12
		f 4 -20 22 23 24
		mu 0 4 17 18 20 19
		f 4 -24 25 26 27
		mu 0 4 19 20 24 21
		f 4 -22 28 29 30
		mu 0 4 27 22 35 25
		f 4 -30 31 32 33
		mu 0 4 25 35 23 26
		f 4 -33 34 -27 35
		mu 0 4 26 23 21 24
		f 6 -34 -36 -26 -23 -21 -31
		mu 0 6 25 26 24 20 18 27
		f 4 36 -29 -3 -14
		mu 0 4 28 35 22 29
		f 4 -6 0 -25 37
		mu 0 4 36 0 30 31
		f 4 38 -35 39 -19
		mu 0 4 32 37 33 7
		f 4 -17 -40 -32 -37
		mu 0 4 28 34 23 35
		f 4 -9 -38 -28 -39
		mu 0 4 32 36 31 37;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegLeft" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "C7BF1E48-4F68-9874-67E9-07A599B2027E";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 8 10.000007629394531 0 ;
	setAttr ".sp" -type "double3" 8 10.000007629394531 0 ;
createNode mesh -n "upperLegLeftShape" -p "|BasicMechGrouped1|Basic_Legs|upperLegLeft";
	rename -uid "674BACB9-4BDB-E751-94F5-E687FEFB98C1";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000002980232239 0.4999999962747097 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "upperLegLeftShapeOrig" -p "|BasicMechGrouped1|Basic_Legs|upperLegLeft";
	rename -uid "1E95A1A8-418B-1FAC-4F61-23954CDAFB56";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0.31249374
		 0.625 0.31249374 0.375 0.43750626 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.43749374 0.25 0.56250626 -7.4505806e-009 0.18749376
		 0.25 0.18749374 -7.4505806e-009 0.31250626 -7.4505806e-009 0.43749374 0.75 0.56250626
		 0.5 0.68749374 0.25 0.81250626 -7.4505806e-009 0.81250632 0.25 0.43749374 -7.4505806e-009
		 0.31250626 0.25 0.68749374 -7.4505806e-009 0.56250626 0.25 0.43749374 0.5 0.375 0.81249374
		 0.625 0.43750626 0.56250626 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  5 -27 1.50014997 6.4998498 -27 3 5 -19 1.50014997
		 6.4998498 -19 3 11 -27 1.50014997 9.50014973 -27 3 11 -19 1.50014997 9.50014973 -19 3
		 5 -19 -1.50014997 6.4998498 -19 -3 5 -27 -1.50014997 6.4998498 -27 -3 9.50014973 -19 -3
		 11 -19 -1.50014997 11 -27 -1.50014997 9.50014973 -27 -3;
	setAttr -s 24 ".ed[0:23]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0
		 7 6 0 9 12 0 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0
		 6 4 0 8 10 0 11 9 0 12 15 0 14 13 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 4 17 0 18 -4
		mu 0 4 8 18 9 21
		f 4 21 8 22 -12
		mu 0 4 13 22 14 25
		f 4 19 -15 23 -7
		mu 0 4 15 20 16 17
		f 4 20 10 16 2
		mu 0 4 10 11 12 19
		f 8 -5 3 7 6 13 -9 9 -3
		mu 0 8 0 8 21 1 24 14 22 2
		f 8 -13 11 15 14 5 -1 1 -11
		mu 0 8 23 13 25 3 4 5 6 7
		f 4 -2 -18 4 -17
		mu 0 4 12 18 8 19
		f 4 -6 -20 -8 -19
		mu 0 4 9 20 15 21
		f 4 -10 -22 12 -21
		mu 0 4 2 22 13 23
		f 4 -14 -24 -16 -23
		mu 0 4 14 24 3 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegLeft" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "E8FCBD04-4411-1EB3-B1B5-C7B00E0FB20C";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 8 3.0000076293945312 -0.5 ;
	setAttr ".sp" -type "double3" 8 3.0000076293945312 -0.5 ;
createNode mesh -n "lowerLegLeftShape" -p "|BasicMechGrouped1|Basic_Legs|lowerLegLeft";
	rename -uid "9BF0B797-429E-4920-E93F-A9AA77E27EAF";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5000000074505806 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "lowerLegLeftShapeOrig" -p "|BasicMechGrouped1|Basic_Legs|lowerLegLeft";
	rename -uid "C5BE310D-45DF-44C7-8058-38B1646C411B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0.31249374
		 0.625 0.31249374 0.375 0.43750623 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.43749374 0.25 0.56250626 3.7252903e-009 0.18749376
		 0.25 0.18749376 0 0.31250623 0 0.43749374 0.75 0.56250626 0.5 0.68749374 0.25 0.8125062
		 0 0.81250626 0.25 0.43749374 0 0.31250623 0.25 0.68749374 0 0.56250626 0.25 0.43749374
		 0.5 0.375 0.81249374 0.625 0.43750623 0.56250626 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  4.5 -33 1.250175 6.249825 -33 3 4.5 -27 1.250175
		 6.249825 -27 3 11.5 -33 1.250175 9.75017548 -33 3 11.5 -27 1.250175 9.75017548 -27 3
		 4.5 -27 -2.250175 6.249825 -27 -4 4.5 -33 -2.250175 6.249825 -33 -4 9.75017548 -27 -4
		 11.5 -27 -2.250175 11.5 -33 -2.250175 9.75017548 -33 -4;
	setAttr -s 24 ".ed[0:23]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0
		 7 6 0 9 12 0 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0
		 6 4 0 8 10 0 11 9 0 12 15 0 14 13 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 4 17 0 18 -4
		mu 0 4 8 18 9 21
		f 4 21 8 22 -12
		mu 0 4 13 22 14 25
		f 4 19 -15 23 -7
		mu 0 4 15 20 16 17
		f 4 20 10 16 2
		mu 0 4 10 11 12 19
		f 8 -5 3 7 6 13 -9 9 -3
		mu 0 8 0 8 21 1 24 14 22 2
		f 8 -13 11 15 14 5 -1 1 -11
		mu 0 8 23 13 25 3 4 5 6 7
		f 4 -2 -18 4 -17
		mu 0 4 12 18 8 19
		f 4 -6 -20 -8 -19
		mu 0 4 9 20 15 21
		f 4 -10 -22 12 -21
		mu 0 4 2 22 13 23
		f 4 -14 -24 -16 -23
		mu 0 4 14 24 3 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeLeft" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "C8D2C976-4FCD-C2DE-F529-03A59814EA27";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 8 6.0000076293945312 -1.9999999105930328 ;
	setAttr ".sp" -type "double3" 8 6.0000076293945312 -1.9999999105930328 ;
createNode mesh -n "kneeLeftShape" -p "|BasicMechGrouped1|Basic_Legs|kneeLeft";
	rename -uid "4FB9781C-41FC-B052-1E4F-5EA921AB7B23";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875312974058447 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "kneeLeftShapeOrig" -p "|BasicMechGrouped1|Basic_Legs|kneeLeft";
	rename -uid "8FE3AE94-4726-D1A7-49E0-C3A4FA2BE32C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 56 ".uvst[0].uvsp[0:55]" -type "float2" 0.32166362 0.084586345
		 0.29041365 0.05333636 0.31250626 0.25 0.40833524 0.93750626 0.37499997 0.91541362
		 0.375 0.83458638 0.6374926 9.9346753e-010 0.67833644 0.084586397 0.67833644 0.16541357
		 0.70958644 0.19666362 0.68749374 0.24999999 0.625 0.91541362 0.625 0.83458638 0.375
		 0.33458641 0.375 0.41541362 0.18749379 0.25 0.125 0.18750627 0.625 0.33458641 0.625
		 0.41541362 0.81250626 0.25 0.79041356 0.19666354 0.82166356 0.16541363 0.875 0.18750627
		 0.375 0.58458632 0.375 0.66541362 0.12500003 0.062493745 0.18749374 2.2351742e-008
		 0.20958641 0.053336404 0.625 0.58458638 0.625 0.66541362 0.875 0.062493756 0.82166362
		 0.084586434 0.81250626 7.4505806e-009 0.40833521 0.062493715 0.59166479 0.18750626
		 0.40833527 0.31249374 0.59166479 0.43750626 0.40833521 0.56249368 0.59166479 0.68750626
		 0.40833521 0.81249368 0.59166479 0.93750626 0.70958638 0.053336449 0.79041356 0.053336367
		 0.17833641 0.084586434 0.32166359 0.16541359 0.29041353 0.19666353 0.20958635 0.19666357
		 0.17833641 0.16541359 0.36250746 6.4571126e-009 0.59166479 0.062493723 0.40833524
		 0.18750626 0.59166479 0.31249374 0.40833524 0.43750626 0.59166479 0.56249374 0.40833524
		 0.68750626 0.59166479 0.81249374;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".vt[0:31]"  4 -28.58501053 -1.55805826 5.066728115 -29.65173912 -1.11620486
		 4 -27.44194221 -0.41498923 5.066728115 -27.88379478 0.65173882 4 -28.58501053 -2.44194174
		 5.066728115 -29.65173912 -2.88379502 12 -28.58501053 -1.55805826 10.93327236 -29.65173912 -1.11620486
		 12 -27.44194221 -0.41498923 10.93327236 -27.88379478 0.65173882 12 -28.58501053 -2.44194174
		 10.93327236 -29.65173912 -2.88379502 4 -26.55805779 -0.41498923 5.066728115 -26.11620522 0.65173882
		 4 -25.41498947 -1.55805826 5.066728115 -24.34826088 -1.11620486 12 -26.55805779 -0.41498923
		 10.93327236 -26.11620522 0.65173882 12 -25.41498947 -1.55805826 10.93327236 -24.34826088 -1.11620486
		 4 -25.41498947 -2.44194174 5.066728115 -24.34826088 -2.88379502 4 -26.55805779 -3.58501053
		 5.066728115 -26.11620522 -4.65173864 12 -25.41498947 -2.44194174 10.93327236 -24.34826088 -2.88379502
		 12 -26.55805779 -3.58501053 10.93327236 -26.11620522 -4.65173864 4 -27.44194221 -3.58501053
		 5.066728115 -27.88379478 -4.65173864 12 -27.44194221 -3.58501053 10.93327236 -27.88379478 -4.65173864;
	setAttr -s 56 ".ed[0:55]"  0 1 0 1 3 0 3 2 0 2 0 0 0 4 0 4 5 0 5 1 0
		 3 13 0 13 12 0 12 2 0 4 28 0 28 29 0 29 5 0 6 7 0 7 11 0 11 10 0 10 6 0 6 8 0 8 9 0
		 9 7 0 8 16 0 16 17 0 17 9 0 11 31 0 31 30 0 30 10 0 13 15 0 15 14 0 14 12 0 15 21 0
		 21 20 0 20 14 0 16 18 0 18 19 0 19 17 0 18 24 0 24 25 0 25 19 0 21 23 0 23 22 0 22 20 0
		 23 29 0 28 22 0 24 26 0 26 27 0 27 25 0 26 30 0 31 27 0 1 7 0 9 3 0 13 17 0 19 15 0
		 21 25 0 27 23 0 29 31 0 11 5 0;
	setAttr -s 26 -ch 112 ".fc[0:25]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 33 50 44
		f 4 -1 4 5 6
		mu 0 4 33 0 1 48
		f 4 -3 7 8 9
		mu 0 4 44 50 2 45
		f 4 -6 10 11 12
		mu 0 4 3 4 5 39
		f 4 13 14 15 16
		mu 0 4 7 49 6 41
		f 4 -14 17 18 19
		mu 0 4 49 7 8 34
		f 4 -19 20 21 22
		mu 0 4 34 8 9 10
		f 4 -16 23 24 25
		mu 0 4 11 40 55 12
		f 4 -9 26 27 28
		mu 0 4 13 35 52 14
		f 4 -28 29 30 31
		mu 0 4 46 15 16 47
		f 4 -22 32 33 34
		mu 0 4 51 17 18 36
		f 4 -34 35 36 37
		mu 0 4 19 20 21 22
		f 4 -31 38 39 40
		mu 0 4 23 37 54 24
		f 4 -40 41 -12 42
		mu 0 4 43 25 26 27
		f 4 -37 43 44 45
		mu 0 4 53 28 29 38
		f 4 -45 46 -25 47
		mu 0 4 30 31 42 32
		f 4 48 -20 49 -2
		mu 0 4 33 49 34 50
		f 4 50 -35 51 -27
		mu 0 4 35 51 36 52
		f 4 52 -46 53 -39
		mu 0 4 37 53 38 54
		f 4 54 -24 55 -13
		mu 0 4 39 55 40 3
		f 8 -17 -26 -47 -44 -36 -33 -21 -18
		mu 0 8 7 41 42 31 21 20 9 8
		f 8 -43 -11 -5 -4 -10 -29 -32 -41
		mu 0 8 43 27 1 0 44 45 46 47
		f 4 -7 -56 -15 -49
		mu 0 4 33 48 6 49
		f 4 -8 -50 -23 -51
		mu 0 4 35 50 34 51
		f 4 -30 -52 -38 -53
		mu 0 4 37 52 36 53
		f 4 -42 -54 -48 -55
		mu 0 4 39 54 38 55;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegRight" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "B1375159-4271-3413-F6CA-32B63234581A";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -8 10.000007629394531 0 ;
	setAttr ".sp" -type "double3" -8 10.000007629394531 0 ;
createNode mesh -n "upperLegRightShape" -p "|BasicMechGrouped1|Basic_Legs|upperLegRight";
	rename -uid "5261C5C8-470E-9384-6439-DCBB759F671C";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000003725290298 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "upperLegRightShapeOrig" -p "|BasicMechGrouped1|Basic_Legs|upperLegRight";
	rename -uid "0787472A-413D-8582-8121-5FACF07E9FFD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0.31249374
		 0.625 0.43750626 0.625 0.31249374 0.375 0.81249374 0.375 0.93750626 0.43749374 1
		 0.56250626 1 0.625 0.93750626 0.43749374 0 0.56250626 0.25 0.18749376 0 0.18749376
		 0.25 0.31250626 0.25 0.68749374 0 0.81250632 0.25 0.81250632 0 0.43749374 0.5 0.56250626
		 0.75 0.31250626 0 0.43749374 0.25 0.56250626 0 0.68749374 0.25 0.375 0.43750626 0.43749374
		 0.75 0.56250626 0.5 0.625 0.81249374;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  -5 -27 1.50014997 -6.4998498 -27 3 -5 -19 1.50014997
		 -6.4998498 -19 3 -11 -27 1.50014997 -9.50014973 -27 3 -11 -19 1.50014997 -9.50014973 -19 3
		 -6.4998498 -19 -3 -5 -19 -1.50014997 -5 -27 -1.50014997 -6.4998498 -27 -3 -11 -19 -1.50014997
		 -9.50014973 -19 -3 -11 -27 -1.50014997 -9.50014973 -27 -3;
	setAttr -s 24 ".ed[0:23]"  1 5 0 0 1 0 2 9 0 3 7 0 2 3 0 5 4 0 6 12 0
		 6 7 0 8 13 0 9 8 0 10 0 0 11 15 0 10 11 0 13 12 0 14 4 0 14 15 0 1 3 0 2 0 0 4 6 0
		 7 5 0 8 11 0 10 9 0 12 14 0 15 13 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 4 16 3 19 -1
		mu 0 4 8 19 9 20
		f 4 20 11 23 -9
		mu 0 4 16 23 17 24
		f 4 18 6 22 14
		mu 0 4 13 21 14 15
		f 4 21 -3 17 -11
		mu 0 4 10 11 12 18
		f 8 -5 2 9 8 13 -7 7 -4
		mu 0 8 19 0 22 16 24 1 2 9
		f 8 -13 10 1 0 5 -15 15 -12
		mu 0 8 23 3 4 5 6 7 25 17
		f 4 -2 -18 4 -17
		mu 0 4 8 18 12 19
		f 4 -6 -20 -8 -19
		mu 0 4 13 20 9 21
		f 4 -10 -22 12 -21
		mu 0 4 16 22 3 23
		f 4 -14 -24 -16 -23
		mu 0 4 1 24 17 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegRight" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "200CB4C4-4A61-2209-3E7C-2D8D6AF20A0F";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -8 3.0000076293945312 -0.5 ;
	setAttr ".sp" -type "double3" -8 3.0000076293945312 -0.5 ;
createNode mesh -n "lowerLegRightShape" -p "|BasicMechGrouped1|Basic_Legs|lowerLegRight";
	rename -uid "36AD71C2-41C9-C5B7-2FE8-CA8157B7CF6A";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000003725290298 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "lowerLegRightShapeOrig" -p "|BasicMechGrouped1|Basic_Legs|lowerLegRight";
	rename -uid "8EDC5DA5-47F0-13F1-7684-008A69CB90D3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0.31249374
		 0.625 0.43750623 0.625 0.31249374 0.375 0.81249374 0.375 0.9375062 0.43749374 1 0.56250626
		 1 0.625 0.93750626 0.43749377 0 0.56250626 0.25 0.18749377 0 0.18749376 0.25 0.31250623
		 0.25 0.6874938 0 0.81250626 0.25 0.81250632 0 0.43749377 0.5 0.56250626 0.75 0.31250626
		 0 0.43749374 0.25 0.56250626 0 0.68749374 0.25 0.375 0.4375062 0.43749374 0.75 0.56250626
		 0.5 0.625 0.81249374;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  -4.5 -33 1.250175 -6.249825 -33 3 -4.5 -27 1.250175
		 -6.249825 -27 3 -11.5 -33 1.250175 -9.75017548 -33 3 -11.5 -27 1.250175 -9.75017548 -27 3
		 -6.249825 -27 -4 -4.5 -27 -2.250175 -4.5 -33 -2.250175 -6.249825 -33 -4 -11.5 -27 -2.250175
		 -9.75017548 -27 -4 -11.5 -33 -2.250175 -9.75017548 -33 -4;
	setAttr -s 24 ".ed[0:23]"  1 5 0 0 1 0 2 9 0 3 7 0 2 3 0 5 4 0 6 12 0
		 6 7 0 8 13 0 9 8 0 10 0 0 11 15 0 10 11 0 13 12 0 14 4 0 14 15 0 1 3 0 2 0 0 4 6 0
		 7 5 0 8 11 0 10 9 0 12 14 0 15 13 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 4 16 3 19 -1
		mu 0 4 8 19 9 20
		f 4 20 11 23 -9
		mu 0 4 16 23 17 24
		f 4 18 6 22 14
		mu 0 4 13 21 14 15
		f 4 21 -3 17 -11
		mu 0 4 10 11 12 18
		f 8 -5 2 9 8 13 -7 7 -4
		mu 0 8 19 0 22 16 24 1 2 9
		f 8 -13 10 1 0 5 -15 15 -12
		mu 0 8 23 3 4 5 6 7 25 17
		f 4 -2 -18 4 -17
		mu 0 4 8 18 12 19
		f 4 -6 -20 -8 -19
		mu 0 4 13 20 9 21
		f 4 -10 -22 12 -21
		mu 0 4 16 22 3 23
		f 4 -14 -24 -16 -23
		mu 0 4 1 24 17 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeRight" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "F539CEA7-4251-9CFB-449D-B0B027D37CD8";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -8 6.0000076293945312 -1.9999999105930328 ;
	setAttr ".sp" -type "double3" -8 6.0000076293945312 -1.9999999105930328 ;
createNode mesh -n "kneeRightShape" -p "|BasicMechGrouped1|Basic_Legs|kneeRight";
	rename -uid "A7A5AFD3-49B6-1F46-B745-8FB22975AE62";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.46875312924385071 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "kneeRightShapeOrig" -p "|BasicMechGrouped1|Basic_Legs|kneeRight";
	rename -uid "12F61CA7-4271-89F0-9DC7-E1A456637423";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 50 ".uvst[0].uvsp[0:49]" -type "float2" 0.67833644 0.084586382
		 0.7095865 0.053336393 0.68749374 0.24999999 0.59166479 0.93750626 0.625 0.91541362
		 0.625 0.83458638 0.625 0.33458641 0.625 0.41541362 0.81250632 0.25 0.875 0.18750627
		 0.625 0.58458638 0.625 0.66541362 0.875 0.062493756 0.81250632 1.8310548e-008 0.79041368
		 0.053336415 0.67833638 0.16541359 0.70958644 0.19666363 0.79041368 0.19666365 0.82166362
		 0.16541348 0.82166368 0.084586471 0.32166359 0.08458636 0.32166353 0.16541353 0.31250626
		 0.25 0.36250737 0 0.29041362 0.1966636 0.18749379 0.25 0.2095864 0.19666363 0.125
		 0.18750624 0.17833638 0.16541363 0.12500003 0.062493749 0.17833641 0.084586412 0.18749374
		 2.4414062e-008 0.20958644 0.053336408 0.29041362 0.053336404 0.59166479 0.06249373
		 0.40833518 0.18750626 0.59166479 0.31249374 0.40833518 0.43750626 0.59166479 0.56249374
		 0.40833518 0.68750626 0.59166479 0.81249374 0.40833524 0.93750626 0.40833524 0.062493742
		 0.63749254 0 0.40833524 0.31249374 0.59166479 0.18750626 0.40833524 0.56249374 0.59166479
		 0.43750626 0.40833524 0.81249374 0.59166479 0.68750626;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".vt[0:31]"  -12 -28.58501053 -1.55805826 -10.93327236 -29.65173912 -1.11620486
		 -12 -27.44194221 -0.41498923 -10.93327236 -27.88379478 0.65173882 -12 -28.58501053 -2.44194174
		 -10.93327236 -29.65173912 -2.88379502 -12 -26.55805779 -0.41498923 -10.93327236 -26.11620522 0.65173882
		 -12 -25.41498947 -1.55805826 -10.93327236 -24.34826088 -1.11620486 -12 -25.41498947 -2.44194174
		 -10.93327236 -24.34826088 -2.88379502 -12 -26.55805779 -3.58501053 -10.93327236 -26.11620522 -4.65173864
		 -12 -27.44194221 -3.58501053 -10.93327236 -27.88379478 -4.65173864 -4 -28.58501053 -1.55805826
		 -5.066728115 -29.65173912 -1.11620486 -4 -27.44194221 -0.41498923 -5.066728115 -27.88379478 0.65173882
		 -4 -28.58501053 -2.44194174 -5.066728115 -29.65173912 -2.88379502 -4 -26.55805779 -0.41498923
		 -5.066728115 -26.11620522 0.65173882 -4 -25.41498947 -1.55805826 -5.066728115 -24.34826088 -1.11620486
		 -4 -25.41498947 -2.44194174 -5.066728115 -24.34826088 -2.88379502 -4 -26.55805779 -3.58501053
		 -5.066728115 -26.11620522 -4.65173864 -4 -27.44194221 -3.58501053 -5.066728115 -27.88379478 -4.65173864;
	setAttr -s 56 ".ed[0:55]"  0 1 0 1 3 0 3 2 0 2 0 0 0 4 0 4 5 0 5 1 0
		 3 7 0 7 6 0 6 2 0 4 14 0 14 15 0 15 5 0 7 9 0 9 8 0 8 6 0 9 11 0 11 10 0 10 8 0 11 13 0
		 13 12 0 12 10 0 13 15 0 14 12 0 16 17 0 17 21 0 21 20 0 20 16 0 16 18 0 18 19 0 19 17 0
		 18 22 0 22 23 0 23 19 0 21 31 0 31 30 0 30 20 0 22 24 0 24 25 0 25 23 0 24 26 0 26 27 0
		 27 25 0 26 28 0 28 29 0 29 27 0 28 30 0 31 29 0 1 17 0 19 3 0 7 23 0 25 9 0 11 27 0
		 29 13 0 15 31 0 21 5 0;
	setAttr -s 26 -ch 112 ".fc[0:25]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 34 45 15
		f 4 -1 4 5 6
		mu 0 4 34 0 1 43
		f 4 -3 7 8 9
		mu 0 4 15 45 2 16
		f 4 -6 10 11 12
		mu 0 4 3 4 5 40
		f 4 -9 13 14 15
		mu 0 4 6 36 47 7
		f 4 -15 16 17 18
		mu 0 4 17 8 9 18
		f 4 -18 19 20 21
		mu 0 4 10 38 49 11
		f 4 -21 22 -12 23
		mu 0 4 19 12 13 14
		f 8 -5 -4 -10 -16 -19 -22 -24 -11
		mu 0 8 1 0 15 16 17 18 19 14
		f 4 24 25 26 27
		mu 0 4 20 42 23 33
		f 4 -25 28 29 30
		mu 0 4 42 20 21 35
		f 4 -30 31 32 33
		mu 0 4 35 21 24 22
		f 4 -27 34 35 36
		mu 0 4 33 23 31 32
		f 4 -33 37 38 39
		mu 0 4 22 24 26 25
		f 4 -39 40 41 42
		mu 0 4 25 26 28 27
		f 4 -42 43 44 45
		mu 0 4 27 28 30 29
		f 4 -45 46 -36 47
		mu 0 4 29 30 32 31
		f 8 -47 -44 -41 -38 -32 -29 -28 -37
		mu 0 8 32 30 28 26 24 21 20 33
		f 4 48 -31 49 -2
		mu 0 4 34 42 35 45
		f 4 50 -40 51 -14
		mu 0 4 36 44 37 47
		f 4 52 -46 53 -20
		mu 0 4 38 46 39 49
		f 4 54 -35 55 -13
		mu 0 4 40 48 41 3
		f 4 -26 -49 -7 -56
		mu 0 4 23 42 34 43
		f 4 -34 -51 -8 -50
		mu 0 4 35 44 36 45
		f 4 -43 -53 -17 -52
		mu 0 4 37 46 38 47
		f 4 -48 -55 -23 -54
		mu 0 4 39 48 40 49;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft1" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "57727B67-4FF9-F9C0-5661-34AC29CC31E5";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 12 13.000007629394531 -1 ;
	setAttr ".sp" -type "double3" 12 13.000007629394531 -1 ;
createNode mesh -n "panelLeftShape1" -p "|BasicMechGrouped1|Basic_Legs|panelLeft1";
	rename -uid "FC7A0B73-44EA-DE61-0273-9F9CA4F13933";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "panelLeftShape1Orig" -p "|BasicMechGrouped1|Basic_Legs|panelLeft1";
	rename -uid "E141187E-4746-4749-AECE-548B110CC160";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  12.46269703 -23.50512695 -6 12.46269703 -23.50512695 4
		 10.57437515 -16.76463318 -6 10.57437515 -16.76463318 4 11.53730297 -16.49487305 -6
		 11.53730297 -16.49487305 4 13.42562485 -23.23536682 -6 13.42562485 -23.23536682 4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft2" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "AE85FF9F-4B11-5F63-BE9D-F299F8F019EF";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 8.6269063949584961 12.994880676269531 6.0516877174377441 ;
	setAttr ".sp" -type "double3" 8.6269063949584961 12.994880676269531 6.0516877174377441 ;
createNode mesh -n "panelLeftShape2" -p "|BasicMechGrouped1|Basic_Legs|panelLeft2";
	rename -uid "ABA9A7F9-4F06-4742-60F1-CCA5B512945A";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "panelLeftShape2Orig" -p "|BasicMechGrouped1|Basic_Legs|panelLeft2";
	rename -uid "11A38A95-4009-21BF-F41B-7CBC5557360B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  6.20588017 -23.24049377 8.95359039 12.38651276 -23.24049377 5.66728878
		 5.31936646 -16.5 7.28630066 11.5 -16.5 4 4.86730003 -16.76976013 6.43608665 11.047932625 -16.76976013 3.14978504
		 5.75381279 -23.51025391 8.10337543 11.93444633 -23.51025391 4.8170743;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight1" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "F533770A-4025-13D4-5B03-78BAA7EA95F4";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -12 13.000007629394531 -1 ;
	setAttr ".sp" -type "double3" -12 13.000007629394531 -1 ;
createNode mesh -n "panelRightShape1" -p "|BasicMechGrouped1|Basic_Legs|panelRight1";
	rename -uid "A330C60F-4406-14F5-23E7-379004AA358F";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "panelRightShape1Orig" -p "|BasicMechGrouped1|Basic_Legs|panelRight1";
	rename -uid "361AAFEF-440E-1782-5DC3-759F946331AC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.462697 -20.005127 3.5 
		-17.462698 -20.005127 -6.5 -5.5743756 -20.264633 3.5 -15.574375 -20.264633 -6.5 -6.537303 
		-19.994873 4.5 -16.537302 -19.994873 -5.5 -8.4256248 -19.735367 4.5 -18.425625 -19.735367 
		-5.5;
	setAttr -s 8 ".vt[0:7]"  -5 -3.5 0.5 5 -3.5 0.5 -5 3.5 0.5 5 3.5 0.5
		 -5 3.5 -0.5 5 3.5 -0.5 -5 -3.5 -0.5 5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight2" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "335E1BDB-4BDD-37B8-D84F-DEA691203784";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -8.6269068717956543 12.994881629943848 6.0516879558563232 ;
	setAttr ".sp" -type "double3" -8.6269068717956543 12.994881629943848 6.0516879558563232 ;
createNode mesh -n "panelRightShape2" -p "|BasicMechGrouped1|Basic_Legs|panelRight2";
	rename -uid "35D0D6F9-4474-B10C-BCE5-83A5BC816EB1";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "panelRightShape2Orig" -p "|BasicMechGrouped1|Basic_Legs|panelRight2";
	rename -uid "149F51CF-4EB3-51C5-96A5-549AF9C80E81";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8.8865137 -19.740492 5.1672893 
		-9.7058802 -19.740492 8.4535904 -8 -20 3.5 -8.8193665 -20 6.7863011 -7.5479331 -20.26976 
		3.6497855 -8.3673 -20.26976 6.9360862 -8.4344463 -20.010252 5.3170743 -9.2538128 
		-20.010252 8.6033754;
	setAttr -s 8 ".vt[0:7]"  -3.5 -3.5 0.5 3.5 -3.5 0.5 -3.5 3.5 0.5 3.5 3.5 0.5
		 -3.5 3.5 -0.5 3.5 3.5 -0.5 -3.5 -3.5 -0.5 3.5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Right_Arm" -p "BasicMechGrouped1";
	rename -uid "95CA430A-48C9-4855-B955-3293A8CB5802";
	setAttr ".rp" -type "double3" -18 25.348060607910156 3.559107780456543 ;
	setAttr ".sp" -type "double3" -18 25.348060607910156 3.559107780456543 ;
createNode transform -n "upperArmRight" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "B583A5A7-4C28-D4A8-2F97-1DA3210CE6AB";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 22.000007629394531 0 ;
	setAttr ".sp" -type "double3" -18 22.000007629394531 0 ;
createNode mesh -n "upperArmRightShape" -p "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight";
	rename -uid "E7A06FC2-4E9A-DFB8-0656-369F10E688DC";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "upperArmRightShapeOrig" -p "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight";
	rename -uid "CD641D01-4CD5-B1E3-5B46-91901AD1D65B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0.31249374
		 0.625 0.31249374 0.375 0.43750626 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.43749374 0.25 0.5625062 3.7252903e-009 0.18749376
		 0.25 0.18749379 3.7252903e-009 0.3125062 3.7252903e-009 0.43749374 0.75 0.5625062
		 0.5 0.68749374 0.25 0.8125062 3.7252903e-009 0.81250632 0.25 0.43749377 3.7252903e-009
		 0.31250626 0.25 0.6874938 3.7252903e-009 0.56250626 0.25 0.43749377 0.5 0.375 0.81249374
		 0.625 0.43750626 0.56250626 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  -21 -17 1.50014997 -19.50015068 -17 3 -21 -5 1.50014997
		 -19.50015068 -5 3 -15 -17 1.50014997 -16.49984932 -17 3 -15 -5 1.50014997 -16.49984932 -5 3
		 -21 -5 -1.50014997 -19.50015068 -5 -3 -21 -17 -1.50014997 -19.50015068 -17 -3 -16.49984932 -5 -3
		 -15 -5 -1.50014997 -15 -17 -1.50014997 -16.49984932 -17 -3;
	setAttr -s 24 ".ed[0:23]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0
		 7 6 0 9 12 0 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0
		 6 4 0 8 10 0 11 9 0 12 15 0 14 13 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 4 17 0 18 -4
		mu 0 4 8 18 9 21
		f 4 21 8 22 -12
		mu 0 4 13 22 14 25
		f 4 19 -15 23 -7
		mu 0 4 15 20 16 17
		f 4 20 10 16 2
		mu 0 4 10 11 12 19
		f 8 -5 3 7 6 13 -9 9 -3
		mu 0 8 0 8 21 1 24 14 22 2
		f 8 -13 11 15 14 5 -1 1 -11
		mu 0 8 23 13 25 3 4 5 6 7
		f 4 -2 -18 4 -17
		mu 0 4 12 18 8 19
		f 4 -6 -20 -8 -19
		mu 0 4 9 20 15 21
		f 4 -10 -22 12 -21
		mu 0 4 2 22 13 23
		f 4 -14 -24 -16 -23
		mu 0 4 14 24 3 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "shoulderRight" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "88AD5D0C-420E-B769-E900-C48DA383F6BF";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 34.000007629394531 0 ;
	setAttr ".sp" -type "double3" -18 34.000007629394531 0 ;
createNode mesh -n "shoulderRightShape" -p "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight";
	rename -uid "A3A4F20D-43E4-C92D-3E6B-3DBDD2F282F0";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.42707812786102295 0.18056249618530273 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "shoulderRightShapeOrig" -p "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight";
	rename -uid "3114B357-40C1-CD62-C005-6985AC8D7838";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0.375 0.30207813
		 0.375 0.44792187 0.32292187 0.1805625 0.32292187 0 0.42707813 0.1805625 0.625 0.30207813
		 0.57292187 0.1805625 0.57292187 0 0.375 0.5694375 0.42707813 0.75 0.375 0.75 0.42707813
		 0.44792187 0.57292187 0.44792187 0.625 0.5694375 0.625 0.75 0.42707813 7.4505806e-009
		 0.42707813 0.30207813 0.57292187 0.30207813 0.42707813 0.5694375 0.57292187 0.5694375
		 0.57292187 0.75 0.67707813 7.4505806e-009 0.82292187 0 0.82292187 0.1805625 0.67707813
		 0.1805625 0.17707813 7.4505806e-009 0.17707813 0.1805625 0.625 0.44792187 0.40103906
		 0.30207813 0.375 0.24132031 0.42707813 0.24132031 0.57292187 0.24132031 0.59896094
		 0.30207813 0.625 0.24132031 0.59896094 0.44792187 0.57292187 0.50867969 0.42707813
		 0.50867969 0.40103906 0.44792187 0.375 3.7252903e-009 0.375 0.1805625 0.40103906
		 0.24132031 0.625 3.7252903e-009 0.625 0.1805625 0.59896094 0.24132031 0.40103906
		 0.75 0.40103906 0.5694375 0.40103906 0.50867969 0.4140586 0.47830078 0.59896094 0.75
		 0.59896094 0.5694375 0.59896094 0.50867969 0.58594143 0.47830078;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 37 ".uvst[1].uvsp[0:36]" -type "float2" 0.625 0.81249374
		 0.625 0.93750626 0.56250626 1 0.43749374 1 0.375 0.93750626 0.43749374 3.7252903e-009
		 0.43749374 0.25 0.375 0.25 0.68749374 -3.7252903e-009 0.625 0.25 0.375 0.5 0.375
		 0.81249374 0.56250626 0.5 0.625 0.5 0.56250626 0.75 0.56250626 -3.7252903e-009 0.56250626
		 0.25 0.43749374 0.5 0.43749374 0.75 0.68749374 0.25 0.81250626 -3.7252903e-009 0.81250632
		 0.25 0.18749374 0.25 0.18749374 -3.7252903e-009 0.31250626 -3.7252903e-009 0.31250626
		 0.25 0.40624687 0.25 0.5937531 0.25 0.40624687 0.5 0.5937531 0.5 0.5937531 0.7812469
		 0.40624687 0.9687531 0.375 0 0.40624687 0.7812469 0.5 0.875 0.5937531 0.9687531 0.625
		 -3.7252903e-009;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 65 ".vt[0:64]"  -24 4.50024986 3.5002501 -21.50024986 4.50024986 6
		 -21.50024986 7 3.5002501 -14.49975014 7 3.5002501 -14.49975014 4.50024986 6 -12 4.50024986 3.5002501
		 -24 4.50024986 -3.5002501 -21.50024986 7 -3.5002501 -21.50024986 4.50024986 -6 -14.49975014 4.50024986 -6
		 -14.49975014 7 -3.5002501 -12 4.50024986 -3.5002501 -21.50024986 -2 6 -24 -2 3.5002501
		 -12 -2 3.5002501 -14.49975014 -2 6 -24 -2 -3.5002501 -21.50024986 -2 -6 -14.49975014 -2 -6
		 -12 -2 -3.5002501 -23 -5 2.5002501 -20.50024986 -5 5 -23 -2 2.5002501 -20.50024986 -2 5
		 -13 -5 2.5002501 -15.49975014 -5 5 -15.49975014 -2 5 -13 -2 2.5002501 -20.50024986 -2 -5
		 -23 -2 -2.5002501 -23 -5 -2.5002501 -20.50024986 -5 -5 -13 -2 -2.5002501 -15.49975014 -2 -5
		 -13 -5 -2.5002501 -15.49975014 -5 -5 -23.03966713 6.07776022 3.5002501 -21.50024986 6.07776022 5.039667606
		 -14.49975014 6.07776022 5.039667606 -12.96033287 6.07776022 3.5002501 -12.96033287 6.07776022 -3.5002501
		 -14.49975014 6.07776022 -5.03966713 -21.50024986 6.07776022 -5.039667606 -23.03966713 6.077759743 -3.5002501
		 -21.97871399 -2 3.9787128 -23.03966713 -2 5.039667606 -23.03966713 4.50024986 5.039667606
		 -22.58447266 5.66208887 4.58447266 -14.021286964 -2 3.9787128 -12.96033287 -2 5.039667606
		 -12.96033287 4.50024986 5.039667606 -13.4155283 5.66208887 4.58447266 -21.97871399 -2 -3.9787128
		 -23.03966713 -2 -5.039667606 -23.03966713 4.50024986 -5.039667606 -22.58447266 5.66208887 -4.58447266
		 -14.021286964 -2 -3.9787128 -12.96033287 -2 -5.039667606 -12.96033287 4.50024986 -5.039667606
		 -13.4155283 5.66208887 -4.58447266 -14.021286964 -5 -3.9787128 -21.97871399 -5 3.97871256
		 -21.97871399 -5 -3.97871256 -18 -5 -1.5979197e-017 -14.021286964 -5 3.9787128;
	setAttr -s 124 ".ed[0:123]"  21 25 0 21 61 0 24 64 0 30 20 0 31 35 0 31 62 0
		 34 24 0 35 60 0 0 36 0 2 7 0 7 43 0 6 0 0 1 46 0 0 13 0 13 45 0 12 1 0 2 37 0 1 4 0
		 4 38 0 3 2 0 3 39 0 5 11 0 11 40 0 10 3 0 5 50 0 4 15 0 15 49 0 14 5 0 6 54 0 8 17 0
		 17 53 0 16 6 0 8 42 0 7 10 0 10 41 0 9 8 0 9 58 0 11 19 0 19 57 0 18 9 0 13 22 1
		 22 44 0 23 12 1 15 26 1 26 48 0 27 14 1 17 28 1 28 52 0 29 16 1 19 32 1 32 56 0 33 18 1
		 21 23 0 22 20 0 24 27 0 26 25 0 28 31 0 30 29 0 32 34 0 35 33 0 12 15 0 18 17 0 14 19 0
		 16 13 0 26 23 0 28 33 0 32 27 0 22 29 0 36 2 0 37 1 0 38 3 0 39 5 0 40 10 0 41 9 0
		 42 7 0 43 6 0 36 47 1 37 38 1 38 51 1 39 40 1 40 59 1 41 42 1 42 55 1 43 36 1 44 23 0
		 45 12 0 46 0 0 47 37 1 44 45 1 45 46 1 46 47 1 48 27 0 49 14 0 50 4 0 51 39 1 48 49 1
		 49 50 1 50 51 1 52 29 0 53 16 0 54 8 0 55 43 1 52 53 1 53 54 1 54 55 1 56 33 0 57 18 0
		 58 11 0 59 41 1 56 57 1 57 58 1 58 59 1 60 34 0 61 20 0 60 63 1 62 30 0 63 61 1 64 25 0
		 62 63 1 63 64 1 64 60 1 60 62 1 62 61 1 61 64 1;
	setAttr -s 61 -ch 248 ".fc[0:60]" -type "polyFaces" 
		f 4 -116 122 113 -4
		mu 1 4 11 33 31 4
		f 4 83 68 9 10
		mu 0 4 37 28 16 11
		f 4 89 86 13 14
		mu 0 4 38 39 2 3
		f 4 16 77 70 19
		mu 0 4 16 30 31 17
		f 4 20 79 72 23
		mu 0 4 17 32 34 12
		f 4 96 93 25 26
		mu 0 4 41 42 6 7
		f 4 103 100 29 30
		mu 0 4 44 45 18 9
		f 4 81 74 33 34
		mu 0 4 35 36 11 12
		f 4 110 107 37 38
		mu 0 4 48 49 13 14
		f 4 88 -15 40 41
		f 4 95 -27 43 44
		f 4 102 -31 46 47
		f 4 109 -39 49 50
		f 6 -114 -2 52 -85 -42 53
		mu 1 6 24 32 5 6 26 7
		f 6 -118 -3 54 -92 -45 55
		mu 1 6 15 36 8 9 27 16
		f 6 -99 -48 56 5 115 57
		mu 1 6 10 28 17 18 33 11
		f 6 -106 -51 58 -113 -8 59
		mu 1 6 12 29 13 0 30 14
		f 4 60 -26 -18 -16
		mu 0 4 15 7 6 4
		f 4 -20 -24 -34 -10
		mu 0 4 16 17 12 11
		f 4 -36 -40 61 -30
		mu 0 4 18 19 20 9
		f 4 62 -38 -22 -28
		mu 0 4 21 22 23 24
		f 4 63 -14 -12 -32
		mu 0 4 25 3 2 26
		f 4 -53 0 -56 64
		mu 1 4 6 5 15 16
		f 4 65 -60 -5 -57
		mu 1 4 17 12 14 18
		f 4 -55 -7 -59 66
		mu 1 4 19 8 20 21
		f 4 -58 3 -54 67
		mu 1 4 22 23 24 25
		f 4 -61 -43 -65 -44
		f 4 -64 -49 -68 -41
		f 4 -62 -52 -66 -47
		f 4 -63 -46 -67 -50
		f 4 76 87 -17 -69
		mu 0 4 29 40 30 16
		f 4 -71 78 94 -21
		mu 0 4 17 31 43 33
		f 4 82 101 -11 -75
		mu 0 4 36 47 37 11
		f 4 80 108 -35 -73
		mu 0 4 34 51 35 12
		f 4 -87 90 -77 -9
		mu 0 4 2 39 40 29
		f 4 -78 69 17 18
		mu 0 4 31 30 4 6
		f 4 97 -79 -19 -94
		mu 0 4 42 43 31 6
		f 4 -80 71 21 22
		mu 0 4 34 32 5 27
		f 4 111 -81 -23 -108
		mu 0 4 50 51 34 27
		f 4 32 -82 73 35
		mu 0 4 18 36 35 19
		f 4 104 -83 -33 -101
		mu 0 4 46 47 36 18
		f 4 8 -84 75 11
		mu 0 4 0 28 37 1
		f 4 -86 -89 84 42
		f 4 12 -90 85 15
		mu 0 4 4 39 38 15
		f 4 -91 -13 -70 -88
		mu 0 4 40 39 4 30
		f 4 -93 -96 91 45
		f 4 24 -97 92 27
		mu 0 4 24 42 41 21
		f 4 -95 -98 -25 -72
		mu 0 4 33 43 42 24
		f 4 -100 -103 98 48
		f 4 28 -104 99 31
		mu 0 4 8 45 44 10
		f 4 -76 -102 -105 -29
		mu 0 4 1 37 47 46
		f 4 -107 -110 105 51
		f 4 36 -111 106 39
		mu 0 4 19 49 48 20
		f 4 -74 -109 -112 -37
		mu 0 4 19 35 51 50
		f 4 123 117 -1 1
		mu 1 4 31 35 2 3
		f 4 121 -6 4 7
		mu 1 4 30 33 18 14
		f 4 120 112 6 2
		mu 1 4 35 30 0 1
		f 3 -120 -115 -121
		mu 1 3 35 34 30
		f 3 -119 -122 114
		mu 1 3 34 33 30
		f 3 -123 118 116
		mu 1 3 31 33 34
		f 3 -117 119 -124
		mu 1 3 31 34 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "fistRight" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "6489D520-4B3C-F71A-214B-E5BCCAF9F96F";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18.075804710388184 15.685355186462402 12.309107780456543 ;
	setAttr ".sp" -type "double3" -18.075804710388184 15.685355186462402 12.309107780456543 ;
createNode mesh -n "fistRightShape" -p "fistRight";
	rename -uid "5D4D3781-4176-E848-AF18-A8B8C55E91A6";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "fistRightShapeOrig" -p "fistRight";
	rename -uid "B10CE1D2-470E-1F19-8006-6DB1117CC72C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 232 ".uvst[0].uvsp[0:231]" -type "float2" 0.5 0.25 0.5 0.18749997
		 0.625 0.18749997 0.625 0.25 0.59375 0.32499999 0.59375 0.28749999 0.625 0.28749999
		 0.625 0.32499999 0.62669051 0.79823244 0.5625 0.96249998 0.5625 0.92500001 0.63268542
		 0.83147663 0.29999998 0.25 0.29999995 0.18749997 0.33749998 0.18749997 0.33749998
		 0.25 0.61875004 0.26875001 0.69441587 0.2877098 0.60000002 0.37500006 0.54312503
		 0.34437501 0.5 0.0625 0.5 0 0.625 0 0.625 0.0625 0.50562501 0.46937501 0.5625 0.92500001
		 0.51875001 0.92499995 0.33749998 0 0.33749998 0.0625 0.29999998 0.0625 0.29999998
		 0 0.5 0.12499993 0.625 0.12499993 0.66250002 0.12499994 0.66250002 0.0625 0.70000005
		 0.0625 0.70000005 0.12499994 0.5625 0.58750004 0.58125001 0.48125011 0.66250002 0.58750015
		 0.33749998 0.12499993 0.29999995 0.12499993 0.66250002 0.18749997 0.70000005 0.18749997
		 0.5 0.92500001 0.5 0.96249998 0.375 0.96249998 0.375 0.92500001 0.39999998 0.25 0.48124999
		 0.41875002 0.39999998 0.25 0.41874999 0.26874995 0.5 0.37500003 0.48124999 0.41875002
		 0.4375 0.28749996 0.51875001 0.33125001 0.375 0.32499999 0.5 0.32499999 0.51875001
		 0.33125001 0.4375 0.28749996 0.5 0.28749999 0.375 0.28749999 0.5 0.25 0.5 0.28749999
		 0.375 0.25 0.64375001 0.25 0.64375001 0.18749997 0.65296507 0.19737829 0.66218007
		 0.20725662 0.65312505 0.12499994 0.66250002 0.12499994 0.65296388 0.056829706 0.6621778
		 0.051159408 0.64375001 0.0625 0.64375001 0 0.5 0.96249998 0.5 1 0.375 1 0.37452209
		 0.18738748 0.63062501 0.25 0.63062501 0.18749997 0.63062501 0.12499994 0.64375001
		 0.12499994 0.63062501 0.0625 0.63062501 0 0.5625 0.96249998 0.51875001 0.96249998
		 0.50562501 0.46937501 0.52437502 0.40687507 0.58125001 0.48125011 0.5625 0.58750004
		 0.54312503 0.34437501 0.60000002 0.37500006 0.51875001 0.32499999 0.51875001 0.28749999
		 0.5625 0.28749999 0.5625 0.32499999 0.5 0.25 0.375 0.25 0.5 0.28749999 0.5 0.96249998
		 0.5 1 0.375 1 0.51875001 0.96249998 0.51875001 0.28749999 0.5625 0.28749999 0.5625
		 0.32499999 0.5 0.22113958 0.5 0.19227916 0.5 0.18749997 0.625 0.25 0.5 0 0.625 0
		 0.5 0.96249998 0.5 0.28749999 0.5 0.25 0.375 0.25 0.375 0 0.5 0 0.375 0 0.5 0.25
		 0.375 0.25 0.5 0 0.375 0 0.5 0.25 0.375 0.25 0.5 0 0.375 0 0.69999993 0.050078563
		 0.70000005 0.12499994 0.5625 0.28749999 0.5625 0.32499999 0.62666768 0.28135902 0.625
		 0.28749999 0.59375 0.28749999 0.62652498 0.32181895 0.625 0.32499999 0.59375 0.32499999
		 0.59375 0.32499999 0.5625 0.28749999 0.5625 0.32499999 0.59375 0.28749999 0.625 0.28749999
		 0.625 0.32499999 0.59375 0.32499999 0.5625 0.28749999 0.5625 0.32499999 0.37452209
		 0.062612481 0.375 0 0.37452209 0.12499993 0.5 0.18272077 0.5 0.15624996 0.5 0.12977915
		 0.5 0.12499993 0.5 0.12022072 0.5 0.09374997 0.5 0.06727922 0.5 0.0625 0.5 0.05772078
		 0.5 0.02886039 0.5 0.19228107 0.5 0.22114053 0.375 0.22114033 0.375 0.19228064 0.5
		 0.1297795 0.5 0.1562496 0.375 0.15624985 0.375 0.12977952 0.5 0.06728024 0.5 0.093750291
		 0.375 0.09375006 0.375 0.067279749 0.5 0.028859444 0.375 0.028859658 0.375 0.028860366
		 0.375 0.057720732 0.375 0.06727922 0.375 0.09374997 0.375 0.12022072 0.375 0.12977916
		 0.375 0.15624996 0.375 0.18272075 0.375 0.19227916 0.375 0.22113958 0.5 0.19227916
		 0.5 0.22113958 0.375 0.22113958 0.375 0.19227917 0.5 0.12977915 0.5 0.15624994 0.375
		 0.15624994 0.375 0.12977915 0.5 0.06727922 0.5 0.09374997 0.375 0.09374997 0.375
		 0.067279227 0.5 0.02886039 0.375 0.02886039 0.5 0.22113979 0.5 0.19227958 0.375 0.2211397
		 0.375 0.1922794 0.5 0.15624997 0.5 0.1297795 0.375 0.15624996 0.375 0.1297794 0.5
		 0.09374994 0.5 0.067279533 0.375 0.09374997 0.375 0.067279458 0.5 0.02886021 0.375
		 0.028860271 0.5 0.18271969 0.5 0.18272045 0.375 0.18272051 0.37499997 0.1827202 0.5
		 0.12022034 0.5 0.12022036 0.375 0.12022048 0.375 0.12022036 0.49999997 0.057718888
		 0.5 0.057720419 0.375 0.057720542 0.375 0.057719316 0.5 0.05772078 0.375 0.05772078
		 0.5 0.12022071 0.375 0.12022071 0.5 0.18272075 0.375 0.18272075 0.41874999 0.26874995
		 0.65937507 0.25937501;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 203 ".vt";
	setAttr ".vt[0:165]"  -21.96157646 -22 14.73168659 -18 -22 11.70988083 -22.13616371 -14 14.83248425
		 -18 -14 11.70988083 -14 -15.8485508 7 -14 -20.1514492 7 -14 -18.000003814697 7 -18 -22 13.49056625
		 -19.49567032 -20.99104309 7 -19.49567032 -15.0089569092 7 -18 -14 13.49056625 -14 -16.11745453 11
		 -14 -18.000001907349 11 -14 -19.88254547 11 -16 -14.28015041 11 -16 -16 11 -16 -18.000001907349 11
		 -16 -20 11 -16 -22 11 -16 -22 7 -15.0089569092 -19.49567032 7 -15.0089569092 -18.000001907349 7
		 -15.0089569092 -16.50432968 7 -16 -13.90914822 7 -22.67245674 -16 7 -22.15194702 -13.84805298 7
		 -22.67245674 -16 11 -22.21488571 -13.78511524 11 -22.21488571 -22.21488571 11 -22.67245674 -20 11
		 -22.67245674 -20 7 -22.15194702 -22.15194702 7 -22.80763054 -18.000001907349 11 -22.80763054 -18.000001907349 7
		 -19.43179131 -22.30389404 7 -18 -22.30389404 11.70988083 -18 -13.69610596 11.70988083
		 -19.43179131 -13.69610596 7 -22.21357727 -13.84805298 14.87717915 -18 -13.69610596 13.49056625
		 -18 -22.30389404 13.49056625 -22.21357727 -22.15194702 14.87717915 -16 -22.30389404 11
		 -16 -22.30389404 7 -16 -13.69610596 11 -16 -13.69610596 7 -17.38764954 -14 13.49056625
		 -17.38764954 -16 13.49056625 -17.39999962 -16 11.70988083 -17.39999962 -14 11.70988083
		 -17.38764954 -20 13.49056625 -17.38764954 -22 13.49056625 -17.39999962 -22 11.70988083
		 -17.39999962 -20 11.70988083 -17.38764954 -18.000001907349 13.49056625 -17.39999962 -18.000001907349 11.70988083
		 -17.39999962 -22.30389404 11.70988083 -17.39999962 -22.30389404 7 -16.50432968 -20.99104309 7
		 -16.50432968 -15.0089569092 7 -17.39999962 -13.69610596 7 -17.39999962 -13.69610596 11.70988083
		 -17.42310524 -14 13.82323742 -17.3177948 -14 16.8607235 -17.3177948 -22 16.8607235
		 -17.42310524 -22 13.82323742 -17.23379326 -14 13.54300213 -15.077960014 -14 14.41306496
		 -15.18754578 -22 14.44242859 -17.23379326 -22 13.54300213 -17.27010345 -13.981987 12.018017769
		 -16.33641624 -13.9562912 11.85221672 -16.26112366 -21.95451736 12.24091625 -17.19481087 -21.98021317 12.4067173
		 -13.71835136 -18.000001907349 11 -13.71835136 -19.88254547 11 -13.71835136 -20.1514492 7
		 -13.71835136 -18.000003814697 7 -13.71835136 -16.11745453 11 -13.71835136 -15.8485508 7
		 -15.62994576 -12.32540989 9.92006111 -15.90540123 -13.82305622 11.18325233 -13.90795135 -14.23598671 11.15792656
		 -13.63249302 -12.73834038 9.89473534 -15.29007912 -12.7365427 13.3777504 -15.83712196 -13.94207096 11.90740204
		 -13.88194275 -14.30953407 11.45662308 -13.33489799 -13.10400772 12.92696762 -15.20109749 -15.089771271 14.16748238
		 -15.68040085 -15.76082897 13.349226 -13.96732521 -15.96496964 13.098031998 -13.48801899 -15.29391289 13.91628647
		 -22.16117287 -19.84706497 14.84692287 -21.96157646 -20.15293503 14.73168659 -22.67842102 -20 15.12247467
		 -22.34792519 -17.84706688 14.95474434 -22.16117287 -18.15293694 14.84692287 -22.81671143 -18.000003814697 15.1242733
		 -22.13616371 -15.84706593 14.83248425 -22.34792519 -16.15293503 14.95474434 -22.67842102 -16 15.12247467
		 -17.3177948 -15.84706593 16.8607235 -17.3177948 -16.15293503 17.26873589 -18 -16.15293503 13.49056625
		 -17.69385529 -16 13.49056625 -18 -15.84706593 13.49056625 -17.42310524 -16.15293503 13.82323742
		 -17.42310524 -15.84706593 13.82323742 -17.3177948 -17.84706688 17.26873589 -17.3177948 -18.15293694 17.10641098
		 -18 -18.15293694 13.49056625 -17.69385529 -18.000001907349 13.49056625 -18 -17.84706688 13.49056625
		 -17.42310524 -18.15293694 13.82323742 -17.42310524 -17.84706688 13.82323742 -17.3177948 -19.84706497 17.10641098
		 -17.3177948 -20.15293503 16.8607235 -18 -20.15293503 13.49056625 -17.69385529 -20 13.49056625
		 -18 -19.84706497 13.49056625 -17.42310524 -20.15293503 13.82323742 -17.42310524 -19.84706497 13.82323742
		 -15.077960014 -15.8470583 14.41306496 -14.79501534 -16.15294266 14.33725071 -17.23379326 -16.15294456 13.54300213
		 -17.23379326 -15.84705257 13.54300213 -14.79501534 -17.84705925 14.33725071 -14.95831108 -18.15294456 14.38100529
		 -17.23379326 -18.15294838 13.54300213 -17.23379326 -17.84705544 13.54300213 -14.95831108 -19.84705734 14.38100529
		 -15.18754578 -20.15294266 14.44242859 -17.23379326 -20.15294647 13.54300213 -17.23379326 -19.84705544 13.54300213
		 -16.30164909 -15.80249119 11.86960125 -16.298769 -16.10835648 11.5025835 -17.23533821 -15.82817364 12.035404205
		 -17.23245811 -16.13406944 11.66838646 -16.2987709 -17.80245781 11.50258255 -16.2987709 -18.10835075 11.83088589
		 -17.23246002 -17.82815552 11.66838551 -17.23246002 -18.13404846 11.99668884 -16.2987709 -19.80245209 11.83088589
		 -16.29589081 -20.10831833 12.22353268 -17.23245811 -19.82813263 11.99668694 -17.22957802 -20.13402748 12.38933372
		 -18.032951355 -14.92353249 13.46891785 -17.43033791 -14.92353249 13.83822346 -17.23258781 -14.92352676 13.52712727
		 -17.25235748 -14.9050808 11.69549084 -16.2770462 -14.87939072 11.51143074 -14.92634106 -14.92352867 14.49419689
		 -17.32033348 -14.92353249 17.21020126 -22.31890869 -14.92353249 14.91299438 -18.032951355 -17 13.46891785
		 -17.43033791 -17 13.83822346 -17.23258781 -17 13.52712822 -17.23119354 -16.98111343 11.33828259
		 -16.25588226 -16.95540619 11.15422058 -14.64339447 -17 14.41838264 -17.32033348 -17 17.61821556
		 -22.53067017 -17 15.035255432 -18.032951355 -19 13.46891785 -17.43033791 -19 13.83822346
		 -17.23258781 -19.000001907349 13.52712727 -17.23119354 -18.98109055 11.66658401;
	setAttr ".vt[166:202]" -16.25588226 -18.95540237 11.48252392 -14.80668926 -19 14.46213722
		 -17.32033348 -19 17.45588684 -22.34391785 -19 14.92743397 -18.032951355 -21.076467514 13.46891785
		 -17.43033791 -21.076467514 13.83822346 -17.23258781 -21.076473236 13.52712727 -17.21002579 -21.057121277 12.069037437
		 -16.23471451 -21.031417847 11.88497829 -15.035925865 -21.076471329 14.52356052 -17.32033348 -21.076467514 17.21020126
		 -22.14432144 -21.076467514 14.81219673 -15.030960083 -13.82825565 7 -14.62896633 -12.3874588 9.801157
		 -14.28152847 -12.82954788 13.33837032 -14.31648827 -15.41512585 14.30800915 -14.83895969 -16.17894745 13.41605854
		 -14.877841 -14.2017231 11.73559952 -14.92923069 -14.092132568 11.17811584 -20.99104309 -19.49567032 7
		 -20.99104309 -18.000001907349 7 -20.99104309 -16.50432968 7 -20.99104309 -19.49567032 9.51961899
		 -20.99104309 -18.000001907349 9.51961899 -19.49567032 -18.000001907349 9.51961899
		 -19.49567032 -20.99104309 9.51961899 -20.99104309 -16.50432968 9.51961899 -19.49567032 -15.0089569092 9.51961899
		 -16.50432968 -20.99104309 9.51961899 -16.50432968 -18.000001907349 9.51961899 -15.0089569092 -18.000001907349 9.51961899
		 -15.0089569092 -19.49567032 9.51961899 -16.50432968 -15.0089569092 9.51961899 -15.0089569092 -16.50432968 9.51961899
		 -15 -16.7118988 11 -15 -18 11 -15 -19.2881012 11;
	setAttr -s 397 ".ed";
	setAttr ".ed[0:165]"  2 10 0 5 19 0 5 20 1 6 5 1 6 21 1 4 6 1 4 22 1 7 1 0
		 8 185 0 9 187 0 10 3 0 10 146 0 11 4 0 13 5 0 13 18 0 11 12 0 12 13 0 14 11 1 15 200 1
		 16 201 1 17 202 1 18 52 1 20 58 0 22 59 0 23 178 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 20 21 0 21 22 0 23 14 0 24 25 0 24 26 1 26 27 1 27 25 0 28 29 1 30 29 1 31 30 0
		 31 28 0 29 32 1 33 32 1 30 33 0 32 26 1 33 24 0 8 34 1 1 35 0 35 34 1 3 36 0 27 36 1
		 9 37 1 37 36 1 25 37 0 2 38 1 10 39 0 38 39 0 39 36 0 38 27 0 7 40 0 40 35 0 0 41 1
		 41 40 0 28 41 0 29 94 1 41 94 0 32 97 1 26 100 1 18 42 0 19 43 0 42 43 0 42 56 0
		 43 57 0 14 44 0 36 61 0 23 45 0 45 44 0 37 60 0 10 46 0 46 47 0 47 48 1 3 49 0 48 49 0
		 46 49 0 7 51 0 50 51 0 1 52 0 51 52 0 52 53 0 50 53 1 54 50 0 53 55 0 54 55 1 47 54 0
		 55 48 0 49 14 1 48 15 1 55 16 1 53 17 1 56 35 0 57 34 0 58 8 0 59 9 0 60 45 0 61 44 0
		 52 56 1 56 57 1 57 58 1 59 60 1 60 61 1 61 49 1 10 62 0 2 63 0 63 62 1 0 64 0 7 65 0
		 64 65 1 62 66 0 63 67 0 67 66 1 64 68 0 65 69 0 68 69 1 66 70 0 67 71 0 71 70 0 68 72 0
		 69 73 0 72 73 0 12 74 1 13 75 0 74 75 0 5 76 0 75 76 0 6 77 1 77 76 0 74 77 1 11 78 0
		 78 74 0 4 79 0 79 77 0 78 79 0 23 80 0 14 81 0 80 81 1 11 82 0 81 184 0 4 83 0 82 83 1
		 80 179 0 80 84 0 81 85 0 84 85 1 82 86 0 85 183 0 83 87 0 86 87 1 84 180 0 84 88 0
		 85 89 0 88 89 0 86 90 0 89 182 0 87 91 0 90 91 0 88 181 0;
	setAttr ".ed[166:331]" 94 97 0 97 100 0 100 38 0 103 154 0 104 47 1 103 104 0
		 105 104 0 110 162 0 111 54 1 110 111 0 112 111 0 117 170 0 118 50 1 117 118 0 119 118 0
		 93 116 0 115 92 0 92 94 0 94 93 0 96 109 0 108 95 0 95 97 0 97 96 0 99 102 0 101 98 0
		 98 100 0 100 99 0 102 123 0 122 101 0 105 107 0 106 103 0 107 125 0 124 106 0 109 127 0
		 126 108 0 112 114 0 113 110 0 114 129 0 128 113 0 116 131 0 130 115 0 119 121 0 120 117 0
		 121 133 0 132 120 0 123 135 0 134 122 0 125 136 0 137 124 0 127 139 0 138 126 0 129 140 0
		 141 128 0 131 143 0 142 130 0 133 144 0 145 132 0 135 137 0 139 141 0 143 145 0 136 149 0
		 71 150 0 140 157 0 135 158 0 144 165 0 139 166 0 73 173 0 143 174 0 0 177 0 92 169 0
		 95 161 0 98 153 0 62 147 0 101 152 0 106 155 0 108 160 0 113 163 0 115 168 0 120 171 0
		 64 176 0 66 148 0 122 151 0 124 156 0 126 159 0 128 164 0 130 167 0 132 172 0 68 175 0
		 98 99 0 95 96 0 92 93 0 98 105 0 101 107 0 122 125 0 123 124 0 126 129 0 127 128 0
		 130 133 0 131 132 0 116 120 0 115 121 0 109 113 0 108 114 0 102 106 0 99 103 0 95 112 0
		 96 110 0 92 119 0 93 117 0 119 117 0 112 110 0 105 103 0 146 105 0 147 107 0 148 125 0
		 149 70 0 150 134 0 151 67 0 152 63 0 153 2 0 146 147 1 147 148 1 148 149 1 150 151 1
		 151 152 1 152 153 1 154 112 0 155 114 0 156 129 0 157 137 0 158 138 0 159 123 0 160 102 0
		 161 99 0 154 155 1 155 156 1 156 157 1 158 159 1 159 160 1 160 161 1 162 119 0 163 121 0
		 164 133 0 165 141 0 166 142 0 167 127 0 168 109 0 169 96 0 162 163 1 163 164 1 164 165 1
		 166 167 1 167 168 1 168 169 1 170 7 0 171 65 0 172 69 0 173 145 0 174 72 0 175 131 0
		 176 116 0 177 93 0 170 171 1 171 172 1 172 173 1 174 175 1;
	setAttr ".ed[332:396]" 175 176 1 176 177 1 178 4 0 179 83 0 180 87 0 181 91 0
		 182 90 0 183 86 0 184 82 0 178 179 1 179 180 1 180 181 1 181 182 0 182 183 1 183 184 1
		 0 7 0 35 28 1 34 31 0 185 30 1 186 33 1 187 24 1 149 150 0 136 134 0 157 158 0 140 138 0
		 165 166 0 144 142 0 173 174 0 185 186 0 186 187 0 185 188 0 186 189 1 188 189 0 190 189 1
		 8 191 0 191 190 1 191 188 0 187 192 0 189 192 0 9 193 0 193 192 0 190 193 1 58 194 0
		 194 195 1 21 196 1 196 195 1 20 197 0 197 196 0 197 194 0 59 198 0 195 198 1 22 199 0
		 199 198 0 196 199 0 195 190 1 194 191 0 198 193 0 58 19 1 59 23 1 23 4 1 200 11 1
		 201 12 1 202 13 1 200 201 1 201 202 1;
	setAttr -s 196 -ch 794 ".fc[0:195]" -type "polyFaces" 
		f 4 79 80 82 -84
		mu 0 4 0 1 2 3
		f 4 344 338 164 -338
		mu 0 4 4 5 6 7
		f 4 14 29 -2 -14
		mu 0 4 8 9 10 11
		f 4 -34 34 35 36
		mu 0 4 12 13 14 15
		f 4 391 6 23 390
		mu 0 4 16 17 18 19
		f 4 85 87 88 -90
		mu 0 4 20 21 22 23
		f 4 389 69 72 107
		mu 0 4 24 10 25 26
		f 4 37 -39 -40 40
		mu 0 4 27 28 29 30
		f 4 90 89 91 -93
		mu 0 4 31 20 23 32
		f 4 131 133 -136 -137
		mu 0 4 33 34 35 36
		f 4 30 -5 3 2
		mu 0 4 37 38 39 11
		f 4 41 -43 -44 38
		mu 0 4 28 40 41 29
		f 4 93 92 94 -81
		mu 0 4 1 31 32 2
		f 4 138 136 -141 -142
		mu 0 4 42 33 36 43
		f 4 31 -7 5 4
		mu 0 4 38 18 17 39
		f 4 44 -35 -46 42
		mu 0 4 40 14 13 41
		f 4 -49 348 -41 -350
		mu 0 4 44 45 46 47
		f 5 39 -351 -9 46 349
		mu 0 5 30 29 48 49 44
		f 4 364 -366 -368 368
		mu 0 4 50 51 52 53
		f 4 370 -373 -374 365
		mu 0 4 51 54 55 52
		f 5 53 -52 9 352 33
		mu 0 5 56 57 58 59 13
		f 4 -53 -54 -37 50
		mu 0 4 60 57 56 61
		f 4 57 -51 -59 56
		mu 0 4 62 63 61 64
		f 4 25 18 392 -18
		mu 0 4 65 66 67 68
		f 4 395 393 -16 -393
		mu 0 4 67 69 70 68
		f 4 396 394 -17 -394
		mu 0 4 69 71 72 70
		f 4 28 -15 -395 -21
		mu 0 4 73 74 72 71
		f 4 -61 -63 -64 -349
		mu 0 4 75 76 77 46
		f 4 -36 67 168 58
		mu 0 4 15 14 78 64
		f 4 -83 96 -26 -96
		mu 0 4 79 80 66 65
		f 4 -95 97 -27 -97
		mu 0 4 80 81 82 66
		f 4 -92 98 -28 -98
		mu 0 4 81 83 73 82
		f 4 -89 -22 -29 -99
		mu 0 4 83 84 74 73
		f 4 -71 71 106 -73
		mu 0 4 25 85 86 26
		f 4 375 -378 -380 380
		mu 0 4 87 88 89 90
		f 4 382 -385 -386 377
		mu 0 4 88 91 92 89
		f 4 109 104 -77 -104
		mu 0 4 93 94 95 96
		f 4 55 -57 -55 0
		mu 0 4 97 62 64 98
		f 4 10 49 -58 -56
		mu 0 4 97 99 63 62
		f 4 -8 59 60 -48
		mu 0 4 100 101 76 75
		f 4 -60 -348 61 62
		mu 0 4 76 101 102 77
		f 4 -30 68 70 -70
		mu 0 4 10 9 85 25
		f 4 21 105 -72 -69
		mu 0 4 9 103 86 85
		f 4 110 95 73 -105
		mu 0 4 94 104 105 95
		f 4 -33 75 76 -74
		mu 0 4 105 106 96 95
		f 6 11 278 172 170 -80 -79
		mu 0 6 97 107 108 109 1 0
		f 4 -11 78 83 -82
		mu 0 4 110 97 0 3
		f 4 7 86 -88 -85
		mu 0 4 111 112 22 21
		f 4 -106 -87 47 -100
		mu 0 4 86 103 113 75
		f 4 -107 99 48 -101
		mu 0 4 26 86 75 44
		f 4 -102 -108 100 -47
		mu 0 4 49 24 26 44
		f 4 367 -387 -376 387
		mu 0 4 53 52 88 87
		f 4 373 -389 -383 386
		mu 0 4 52 55 91 88
		f 4 102 51 77 -109
		mu 0 4 19 58 57 93
		f 4 52 74 -110 -78
		mu 0 4 57 63 94 93
		f 4 81 -111 -75 -50
		mu 0 4 114 104 94 63
		f 4 -112 -1 112 113
		mu 0 4 115 97 98 116
		f 4 347 115 -117 -115
		mu 0 4 117 111 118 119
		f 4 -118 -114 118 119
		mu 0 4 120 115 116 121
		f 4 121 -123 -121 116
		mu 0 4 118 122 123 119
		f 4 -124 -120 124 125
		mu 0 4 124 120 121 125
		f 4 127 -129 -127 122
		mu 0 4 122 126 127 123
		f 4 16 130 -132 -130
		mu 0 4 70 72 34 33
		f 4 13 132 -134 -131
		mu 0 4 72 128 35 34
		f 4 -4 134 135 -133
		mu 0 4 128 129 36 35
		f 4 15 129 -139 -138
		mu 0 4 68 70 33 42
		f 4 -6 139 140 -135
		mu 0 4 129 17 43 36
		f 4 -13 137 141 -140
		mu 0 4 17 68 42 43
		f 4 32 143 -145 -143
		mu 0 4 106 105 130 131
		f 5 17 145 -341 -147 -144
		mu 0 5 105 132 133 134 130
		f 4 12 147 -149 -146
		mu 0 4 132 135 136 133
		f 4 -335 341 335 -148
		mu 0 4 135 137 138 136
		f 4 144 151 -153 -151
		mu 0 4 131 130 139 140
		f 4 346 340 153 -340
		mu 0 4 141 134 133 142
		f 4 148 155 -157 -154
		mu 0 4 133 136 143 142
		f 4 -336 342 336 -156
		mu 0 4 136 138 144 143
		f 4 152 159 -161 -159
		mu 0 4 140 139 145 146
		f 4 345 339 161 -339
		mu 0 4 5 141 142 6
		f 4 156 163 -165 -162
		mu 0 4 142 143 7 6
		f 4 -337 343 337 -164
		mu 0 4 143 144 4 7
		f 4 -65 -38 63 65
		mu 0 4 147 28 27 148
		f 4 -67 -42 64 166
		mu 0 4 149 40 28 147
		f 4 -68 -45 66 167
		mu 0 4 78 14 40 149
		f 7 -172 169 292 176 174 -94 -171
		mu 0 7 109 150 151 152 153 31 1
		f 7 -176 173 306 180 178 -91 -175
		mu 0 7 153 154 155 156 157 20 31
		f 6 -180 177 320 84 -86 -179
		mu 0 6 157 158 159 111 21 20
		f 4 226 353 282 -355
		mu 0 4 160 161 162 163
		f 4 228 355 296 -357
		mu 0 4 164 165 166 167
		f 4 230 357 310 -359
		mu 0 4 168 169 170 171
		f 4 232 359 324 128
		mu 0 4 126 172 173 127
		f 5 234 327 -185 -66 -62
		mu 0 5 117 174 175 147 148
		f 5 235 313 -189 -167 -184
		mu 0 5 176 177 178 149 147
		f 5 236 299 -193 -168 -188
		mu 0 5 179 180 181 78 149
		f 5 237 285 54 -169 -192
		mu 0 5 182 183 98 64 78
		f 4 -196 -279 286 279
		mu 0 4 184 108 107 185
		f 4 291 -238 -191 239
		mu 0 4 186 183 182 187
		f 4 -202 -293 300 293
		mu 0 4 188 152 151 189
		f 4 305 -237 -187 241
		mu 0 4 190 180 179 191
		f 4 -208 -307 314 307
		mu 0 4 192 156 155 193
		f 4 319 -236 -183 243
		mu 0 4 194 177 176 195
		f 4 -321 328 321 -116
		mu 0 4 111 159 196 118
		f 4 333 -235 114 245
		mu 0 4 197 174 117 119
		f 4 -280 287 280 -198
		mu 0 4 184 185 198 199
		f 4 290 -240 -195 247
		mu 0 4 200 186 187 201
		f 4 -294 301 294 -204
		mu 0 4 188 189 202 203
		f 4 304 -242 -201 249
		mu 0 4 204 190 191 205
		f 4 -308 315 308 -210
		mu 0 4 192 193 206 207
		f 4 318 -244 -207 251
		mu 0 4 208 194 195 209
		f 4 -322 329 322 -122
		mu 0 4 118 196 210 122
		f 4 332 -246 120 253
		mu 0 4 211 197 119 123
		f 4 -281 288 -227 -214
		mu 0 4 199 198 161 160
		f 4 289 -248 -213 -283
		mu 0 4 162 200 201 163
		f 4 -295 302 -229 -218
		mu 0 4 203 202 165 164
		f 4 303 -250 -217 -297
		mu 0 4 166 204 205 167
		f 4 -309 316 -231 -222
		mu 0 4 207 206 169 168
		f 4 317 -252 -221 -311
		mu 0 4 170 208 209 171
		f 4 -323 330 -233 -128
		mu 0 4 122 210 172 126
		f 4 331 -254 126 -325
		mu 0 4 173 211 123 127
		f 3 191 192 -255
		mu 0 3 182 78 181
		f 3 187 188 -256
		mu 0 3 179 149 178
		f 3 183 184 -257
		mu 0 3 176 147 175
		f 4 195 -259 190 257
		mu 0 4 108 184 187 182
		f 4 197 -260 194 258
		mu 0 4 184 199 201 187
		f 4 213 354 212 259
		mu 0 4 199 160 163 201
		f 4 214 -261 211 223
		mu 0 4 212 213 214 215
		f 4 217 356 216 261
		mu 0 4 203 164 167 205
		f 4 218 -263 215 224
		mu 0 4 216 217 218 219
		f 4 221 358 220 263
		mu 0 4 207 168 171 209
		f 4 222 -265 219 225
		mu 0 4 220 221 222 223
		f 4 210 -266 205 264
		mu 0 4 221 224 225 222
		f 4 209 -264 206 266
		mu 0 4 192 207 209 195
		f 4 204 -268 199 262
		mu 0 4 217 226 227 218
		f 4 203 -262 200 268
		mu 0 4 188 203 205 191
		f 4 198 -270 193 260
		mu 0 4 213 228 229 214
		f 4 196 -271 189 269
		mu 0 4 228 150 181 229
		f 4 201 -269 186 271
		mu 0 4 152 188 191 179
		f 4 202 -273 185 267
		mu 0 4 226 154 178 227
		f 4 207 -267 182 273
		mu 0 4 156 192 195 176
		f 4 208 -275 181 265
		mu 0 4 224 158 175 225
		f 4 -276 -274 256 274
		mu 0 4 158 156 176 175
		f 4 -277 -272 255 272
		mu 0 4 154 152 179 178
		f 4 -278 -258 254 270
		mu 0 4 150 108 182 181
		f 3 -173 277 171
		mu 0 3 109 108 150
		f 3 -177 276 175
		mu 0 3 153 152 154
		f 3 -181 275 179
		mu 0 3 157 156 158
		f 4 -287 -12 111 238
		mu 0 4 185 107 97 115
		f 4 -288 -239 117 246
		mu 0 4 198 185 115 120
		f 4 -289 -247 123 -282
		mu 0 4 161 198 120 124
		f 4 281 -126 227 -354
		mu 0 4 161 124 125 162
		f 4 -284 -290 -228 -125
		mu 0 4 121 200 162 125
		f 4 -285 -291 283 -119
		mu 0 4 116 186 200 121
		f 4 -286 -292 284 -113
		mu 0 4 98 183 186 116
		f 4 -301 -170 -197 240
		mu 0 4 189 151 150 228
		f 4 -302 -241 -199 248
		mu 0 4 202 189 228 213
		f 4 -303 -249 -215 -296
		mu 0 4 165 202 213 212
		f 4 295 -224 229 -356
		mu 0 4 165 212 215 166
		f 4 -298 -304 -230 -212
		mu 0 4 214 204 166 215
		f 4 -299 -305 297 -194
		mu 0 4 229 190 204 214
		f 4 -300 -306 298 -190
		mu 0 4 181 180 190 229
		f 4 -315 -174 -203 242
		mu 0 4 193 155 154 226
		f 4 -316 -243 -205 250
		mu 0 4 206 193 226 217
		f 4 -317 -251 -219 -310
		mu 0 4 169 206 217 216
		f 4 309 -225 231 -358
		mu 0 4 169 216 219 170
		f 4 -312 -318 -232 -216
		mu 0 4 218 208 170 219
		f 4 -313 -319 311 -200
		mu 0 4 227 194 208 218
		f 4 -314 -320 312 -186
		mu 0 4 178 177 194 227
		f 4 -329 -178 -209 244
		mu 0 4 196 159 158 224
		f 4 -330 -245 -211 252
		mu 0 4 210 196 224 221
		f 4 -331 -253 -223 -324
		mu 0 4 172 210 221 220
		f 4 323 -226 233 -360
		mu 0 4 172 220 223 173
		f 4 -326 -332 -234 -220
		mu 0 4 222 211 173 223
		f 4 -327 -333 325 -206
		mu 0 4 225 197 211 222
		f 4 -328 -334 326 -182
		mu 0 4 175 174 197 225
		f 4 -342 -25 142 149
		mu 0 4 138 137 106 131
		f 4 -343 -150 150 157
		mu 0 4 144 138 131 140
		f 4 -344 -158 158 165
		mu 0 4 4 144 140 146
		f 4 160 162 -345 -166
		mu 0 4 146 145 5 4
		f 4 154 -346 -163 -160
		mu 0 4 139 141 5 145
		f 4 146 -347 -155 -152
		mu 0 4 130 134 141 139
		f 4 -352 -361 350 43
		mu 0 4 41 230 48 29
		f 4 -353 -362 351 45
		mu 0 4 13 59 230 41
		f 4 360 363 -365 -363
		mu 0 4 48 230 51 50
		f 4 8 362 -369 -367
		mu 0 4 49 48 50 53
		f 4 361 369 -371 -364
		mu 0 4 230 59 54 51
		f 4 -10 371 372 -370
		mu 0 4 59 58 55 54
		f 4 -31 378 379 -377
		mu 0 4 38 37 90 89
		f 4 22 374 -381 -379
		mu 0 4 37 24 87 90
		f 4 -24 383 384 -382
		mu 0 4 19 18 92 91
		f 4 -32 376 385 -384
		mu 0 4 18 38 89 92
		f 4 101 366 -388 -375
		mu 0 4 24 49 53 87
		f 4 -103 381 388 -372
		mu 0 4 58 19 91 55
		f 4 -3 1 -390 -23
		mu 0 4 37 11 10 24
		f 4 -391 108 103 -76
		mu 0 4 16 19 93 96
		f 3 24 334 -392
		mu 0 3 16 231 17
		f 4 26 19 -396 -19
		mu 0 4 66 82 69 67
		f 4 27 20 -397 -20
		mu 0 4 82 73 71 69;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRight" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "02769034-4B08-836C-BA4B-60A7C3E10A81";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 15.000007629394531 -1.25 ;
	setAttr ".sp" -type "double3" -18 15.000007629394531 -1.25 ;
createNode mesh -n "lowerArmRightShape" -p "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight";
	rename -uid "EA3EEFFD-4163-C8A8-B562-07AFBFB5E38A";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "lowerArmRightShapeOrig" -p "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight";
	rename -uid "95DB66BE-421E-CCD0-FCF0-E9897E2CD9B2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 99 ".uvst[0].uvsp[0:98]" -type "float2" 0.375 0.5467304 0.4217304
		 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75
		 0.375 0.7032696 0.6437481 0.24375807 0.54375809 0.30625185 0.38124189 0.26874813
		 0.31874812 0.41873318 0.35625187 0.75626677 0.45624191 0.69377303 0.61875814 0.231227
		 0.68125188 0.081241898 0.375 0.7032696 0.4217304 0.75 0.4217304 0.92500001 0.375
		 0.92500001 0.36098087 0.79882789 0.375 0.92500001 0.45624194 0.69377297 0.35625184
		 0.75626677 0.57826966 0.75 0.625 0.7032696 0.625 0.92500001 0.5782696 0.92500001
		 0.63925052 0.17290242 0.70000005 0 0.68125188 0.081241921 0.61875814 0.231227 0.4217304
		 0.5 0.375 0.5467304 0.375 0.32499999 0.4217304 0.32499999 0.36074951 0.26401913 0.29999998
		 0.25 0.31874812 0.41873324 0.38124189 0.26874813 0.625 0.5467304 0.5782696 0.5 0.57826966
		 0.32499999 0.625 0.32499999 0.56425053 0.31098089 0.54375809 0.30625185 0.64374816
		 0.24375805 0.70000005 0.2032696 0.70000005 0.046730354 0.875 0.046730399 0.875 0.20326962
		 0.125 0.046730399 0.29999998 0.046730399 0.29999998 0.20326963 0.125 0.20326963 0.31401908
		 0.37617201 0.43574953 0.75209755 0.68598092 0.060749516 0.63901913 0.26425049 0.4217304
		 0.5 0.375 0.5467304 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75
		 0.4217304 0.75 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375
		 0.5467304 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696
		 0.625 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375
		 0.7032696 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375 0.5467304
		 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696 0.625
		 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375 0.7032696
		 0.375 0.7032696;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 80 ".vt[0:79]"  -22 -20.50462723 -7 -20.50462723 -22 -7 -22 -20.50462723 6
		 -20.50462723 -22 6 -15.49537182 -22 -7 -14 -20.50462723 -7 -15.49537182 -22 6 -14 -20.50462723 6
		 -20.50462723 -14 -7 -22 -15.49537182 -7 -20.50462723 -14 6 -22 -15.49537182 6 -14 -15.49537182 -7
		 -15.49537182 -14 -7 -14 -15.49537182 6 -15.49537182 -14 6 -19.49567032 -15.0089569092 6
		 -20.99104309 -16.50432968 6 -15.0089569092 -16.50432968 6 -16.50432968 -15.0089569092 6
		 -20.99104309 -19.49567032 6 -19.49567032 -20.99104309 6 -16.50432968 -20.99104309 6
		 -15.0089569092 -19.49567032 6 -20.99104309 -16.50432968 8 -19.49567032 -15.0089569092 8
		 -15.0089569092 -16.50432968 8 -16.50432968 -15.0089569092 8 -19.49567032 -20.99104309 8
		 -20.99104309 -19.49567032 8 -15.0089569092 -19.49567032 8 -16.50432968 -20.99104309 8
		 -20.060417175 -14.70942116 -7 -21.29058075 -15.93958187 -7 -15.93958187 -14.70942116 -7
		 -14.70942116 -15.93958187 -7 -14.70942116 -20.060417175 -7 -15.93958187 -21.29058075 -7
		 -20.060417175 -21.29058075 -7 -21.29058075 -20.060417175 -7 -19.1934967 -16.093933105 -10.5
		 -19.90607071 -16.8065033 -10.5 -16.8065033 -16.093933105 -10.5 -16.093931198 -16.8065033 -10.5
		 -16.093931198 -19.1934967 -10.5 -16.8065033 -19.90607071 -10.5 -19.1934967 -19.90607071 -10.5
		 -19.90607071 -19.1934967 -10.5 -20.060417175 -14.70942116 -7.49999905 -21.29058075 -15.93958187 -7.49999905
		 -20.060417175 -14.70942116 -8.96627808 -21.29058075 -15.93958187 -8.96627808 -15.93958187 -14.70942116 -7.49999905
		 -15.93958187 -14.70942116 -8.96627808 -14.70942116 -15.93958187 -7.49999905 -14.70942116 -15.93958187 -8.96627808
		 -14.70942116 -20.060417175 -7.49999905 -14.70942116 -20.060417175 -8.96627808 -15.93958187 -21.29058075 -7.49999905
		 -15.93958187 -21.29058075 -8.96627808 -20.060417175 -21.29058075 -7.49999905 -20.060417175 -21.29058075 -8.96627808
		 -21.29058075 -20.060417175 -7.49999905 -21.29058075 -20.060417175 -8.96627808 -19.73308945 -15.49966335 -7
		 -20.50033951 -16.26691055 -7 -19.73308945 -15.49966335 -7.49999905 -20.50033951 -16.26691055 -7.49999905
		 -16.26691055 -15.4996624 -7 -16.26691055 -15.4996624 -7.49999905 -15.4996624 -16.26691055 -7
		 -15.4996624 -16.26691055 -7.49999905 -15.49966335 -19.73308945 -7 -15.49966335 -19.73308945 -7.49999905
		 -16.26691055 -20.50033951 -7 -16.26691055 -20.50033951 -7.49999905 -19.73308754 -20.5003376 -7
		 -19.73308754 -20.5003376 -7.49999905 -20.5003376 -19.73308754 -7 -20.5003376 -19.73308754 -7.49999905;
	setAttr -s 152 ".ed[0:151]"  1 4 0 1 0 0 5 4 0 8 13 0 9 0 0 8 9 0 12 5 0
		 13 12 0 25 27 0 25 24 0 26 30 0 27 26 0 29 24 0 29 28 0 31 28 0 31 30 0 1 3 0 3 2 0
		 2 0 0 3 21 1 21 20 0 20 2 1 5 7 0 7 6 0 6 4 0 7 23 1 23 22 0 22 6 1 9 11 0 11 10 0
		 10 8 0 11 17 1 17 16 0 16 10 1 13 15 0 15 14 0 14 12 0 15 19 1 19 18 0 18 14 1 17 24 0
		 25 16 0 19 27 0 26 18 0 21 28 0 29 20 0 23 30 0 31 22 0 10 15 0 6 3 0 14 7 0 2 11 0
		 16 19 0 20 17 0 22 21 0 18 23 0 8 32 1 9 33 1 32 33 1 13 34 1 32 34 1 12 35 1 34 35 1
		 5 36 1 35 36 1 4 37 1 36 37 1 1 38 1 38 37 1 0 39 1 38 39 1 33 39 1 40 41 0 40 42 0
		 42 43 0 43 44 0 44 45 0 46 45 0 46 47 0 41 47 0 48 49 0 48 50 0 50 51 0 49 51 0 48 52 0
		 52 53 0 50 53 0 52 54 0 54 55 0 53 55 0 54 56 0 56 57 0 55 57 0 56 58 0 58 59 0 57 59 0
		 60 58 0 60 61 0 61 59 0 60 62 0 62 63 0 61 63 0 49 62 0 51 63 0 50 40 0 51 41 0 53 42 0
		 55 43 0 57 44 0 59 45 0 61 46 0 63 47 0 32 64 1 33 65 1 64 65 0 48 66 1 64 66 0 49 67 1
		 66 67 0 65 67 0 34 68 1 64 68 0 52 69 1 68 69 0 66 69 0 35 70 1 68 70 0 54 71 1 70 71 0
		 69 71 0 36 72 1 70 72 0 56 73 1 72 73 0 71 73 0 37 74 1 72 74 0 58 75 1 74 75 0 73 75 0
		 38 76 1 76 74 0 60 77 1 76 77 0 77 75 0 39 78 1 76 78 0 62 79 1 78 79 0 77 79 0 65 78 0
		 67 79 0;
	setAttr -s 74 -ch 304 ".fc[0:73]" -type "polyFaces" 
		f 8 -73 73 74 75 76 -78 78 -80
		mu 0 8 0 1 2 3 4 5 6 7
		f 8 -12 -9 9 -13 13 -15 15 -11
		mu 0 8 8 9 10 11 12 13 14 15
		f 4 -2 16 17 18
		mu 0 4 16 17 18 19
		f 4 -18 19 20 21
		mu 0 4 20 21 22 23
		f 4 -3 22 23 24
		mu 0 4 24 25 26 27
		f 4 -24 25 26 27
		mu 0 4 28 29 30 31
		f 4 5 28 29 30
		mu 0 4 32 33 34 35
		f 4 -30 31 32 33
		mu 0 4 36 37 38 39
		f 4 -8 34 35 36
		mu 0 4 40 41 42 43
		f 4 -36 37 38 39
		mu 0 4 43 44 45 46
		f 4 -33 40 -10 41
		mu 0 4 39 38 11 10
		f 4 -39 42 11 43
		mu 0 4 46 45 9 8
		f 4 -21 44 -14 45
		mu 0 4 23 22 13 12
		f 4 -27 46 -16 47
		mu 0 4 31 30 15 14
		f 4 48 -35 -4 -31
		mu 0 4 35 42 41 32
		f 4 -17 0 -25 49
		mu 0 4 18 17 24 27
		f 4 50 -23 -7 -37
		mu 0 4 47 48 49 50
		f 4 -19 51 -29 4
		mu 0 4 51 52 53 54
		f 4 -49 -34 52 -38
		mu 0 4 44 36 39 45
		f 4 -52 -22 53 -32
		mu 0 4 55 20 23 38
		f 4 -50 -28 54 -20
		mu 0 4 56 28 31 22
		f 4 -51 -40 55 -26
		mu 0 4 57 58 46 30
		f 4 -53 -42 8 -43
		mu 0 4 45 39 10 9
		f 4 -54 -46 12 -41
		mu 0 4 38 23 12 11
		f 4 -55 -48 14 -45
		mu 0 4 22 31 14 13
		f 4 -56 -44 10 -47
		mu 0 4 30 46 8 15
		f 4 -6 56 58 -58
		mu 0 4 33 32 59 60
		f 4 3 59 -61 -57
		mu 0 4 32 41 61 59
		f 4 7 61 -63 -60
		mu 0 4 41 40 62 61
		f 4 6 63 -65 -62
		mu 0 4 40 25 63 62
		f 4 2 65 -67 -64
		mu 0 4 25 24 64 63
		f 4 -1 67 68 -66
		mu 0 4 24 17 65 64
		f 4 1 69 -71 -68
		mu 0 4 17 16 66 65
		f 4 -5 57 71 -70
		mu 0 4 16 33 60 66
		f 4 -81 81 82 -84
		mu 0 4 67 68 69 70
		f 4 84 85 -87 -82
		mu 0 4 68 71 72 69
		f 4 87 88 -90 -86
		mu 0 4 71 73 74 72
		f 4 90 91 -93 -89
		mu 0 4 73 75 76 74
		f 4 93 94 -96 -92
		mu 0 4 75 77 78 76
		f 4 -97 97 98 -95
		mu 0 4 77 79 80 78
		f 4 99 100 -102 -98
		mu 0 4 79 81 82 80
		f 4 -103 83 103 -101
		mu 0 4 81 67 70 82
		f 4 -83 104 72 -106
		mu 0 4 70 69 1 0
		f 4 86 106 -74 -105
		mu 0 4 69 72 2 1
		f 4 89 107 -75 -107
		mu 0 4 72 74 3 2
		f 4 92 108 -76 -108
		mu 0 4 74 76 4 3
		f 4 95 109 -77 -109
		mu 0 4 76 78 5 4
		f 4 -99 110 77 -110
		mu 0 4 78 80 6 5
		f 4 101 111 -79 -111
		mu 0 4 80 82 7 6
		f 4 -104 105 79 -112
		mu 0 4 82 70 0 7
		f 4 -115 116 118 -120
		mu 0 4 83 84 85 86
		f 4 121 123 -125 -117
		mu 0 4 84 87 88 85
		f 4 126 128 -130 -124
		mu 0 4 87 89 90 88
		f 4 131 133 -135 -129
		mu 0 4 89 91 92 90
		f 4 136 138 -140 -134
		mu 0 4 91 93 94 92
		f 4 -142 143 144 -139
		mu 0 4 93 95 96 94
		f 4 146 148 -150 -144
		mu 0 4 95 97 98 96
		f 4 -151 119 151 -149
		mu 0 4 97 83 86 98
		f 4 -59 112 114 -114
		mu 0 4 60 59 84 83
		f 4 80 117 -119 -116
		mu 0 4 68 67 86 85
		f 4 60 120 -122 -113
		mu 0 4 59 61 87 84
		f 4 -85 115 124 -123
		mu 0 4 71 68 85 88
		f 4 62 125 -127 -121
		mu 0 4 61 62 89 87
		f 4 -88 122 129 -128
		mu 0 4 73 71 88 90
		f 4 64 130 -132 -126
		mu 0 4 62 63 91 89
		f 4 -91 127 134 -133
		mu 0 4 75 73 90 92
		f 4 66 135 -137 -131
		mu 0 4 63 64 93 91
		f 4 -94 132 139 -138
		mu 0 4 77 75 92 94
		f 4 -69 140 141 -136
		mu 0 4 64 65 95 93
		f 4 96 137 -145 -143
		mu 0 4 79 77 94 96
		f 4 70 145 -147 -141
		mu 0 4 65 66 97 95
		f 4 -100 142 149 -148
		mu 0 4 81 79 96 98
		f 4 -72 113 150 -146
		mu 0 4 66 60 83 97
		f 4 102 147 -152 -118
		mu 0 4 67 81 98 86;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Back" -p "BasicMechGrouped1";
	rename -uid "79F770D3-490E-18A1-E665-80B4B7378138";
	setAttr ".rp" -type "double3" 0 27.174622535705566 -16.000000953674316 ;
	setAttr ".sp" -type "double3" 0 27.174622535705566 -16.000000953674316 ;
createNode transform -n "BackPack" -p "Basic_Back";
	rename -uid "E468920E-4B54-81C6-9DA4-7F976E6941C3";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 27.174622535705566 -16.000000953674316 ;
	setAttr ".sp" -type "double3" 0 27.174622535705566 -16.000000953674316 ;
createNode mesh -n "BackPackShape" -p "BackPack";
	rename -uid "BD53FCB8-4B13-2697-956B-55B7F1A251B1";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000002235174179 0.54166661947965622 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "BackPackShapeOrig" -p "BackPack";
	rename -uid "26A8329D-4373-0B8B-B267-3896D143B474";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 200 ".uvst[0].uvsp[0:199]" -type "float2" 0.375 0.25 0.69588798
		 0.25 0.375 0.5 0.625 0.42911205 0.625 0.5 0.625 0.5 0.375 0.35805225 0.51694769 0.25
		 0.625 0.39194781 0.48305234 0.5 0.55411196 0.25 0.44588798 0.5 0.80411208 0.25 0.30411199
		 0.25 0.19588797 0.25 0.44588798 0.25 0.625 0.32088798 0.55411202 0.5 0.375 0.42911205
		 0.375 0.32088798 0.51694775 0.75 0.625 0.89194775 0.48305228 1 0.375 0.85805219 0.48305231
		 0.25 0.625 0.35805225 0.51694769 0.5 0.375 0.39194781 0.48305231 0.75 0.51694769
		 0.75 0.625 0.85805219 0.625 0.89194775 0.51694769 1 0.48305243 1 0.375 0.89194769
		 0.375 0.85805219 0.48305231 0.75 0.55213338 0.75 0.625 0.85805213 0.625 0.92713344
		 0.51694775 1 0.44786662 1 0.375 0.89194775 0.375 0.82286656 0.44786662 0.75 0.625
		 0.82286656 0.55213338 1 0.375 0.92713344 0.44588798 0.083333239 0.55411202 0.083333246
		 0.69588792 0.083333239 0.625 0.66666675 0.80411208 0.083333246 0.55411196 0.66666675
		 0.44588798 0.66666675 0.375 0.66666675 0.19588797 0.083333239 0.30411202 0.083333246
		 0.44588798 0.16666663 0.55411196 0.16666663 0.69588792 0.16666663 0.625 0.58333337
		 0.80411208 0.16666663 0.55411196 0.58333337 0.44588798 0.58333337 0.375 0.58333337
		 0.19588797 0.16666663 0.30411202 0.16666663 0.30411202 0.083333246 0.44588798 0.083333239
		 0.44588798 0.16666663 0.30411202 0.16666663 0.44588798 0.083333239 0.44588798 0.16666663
		 0.44588798 0.16666663 0.44588798 0.083333239 0.44786659 0.75 0.55213338 0.75 0.375
		 0.92713344 0.375 0.82286656 0.625 0.82286656 0.625 0.92713344 0.55213338 1 0.44786662
		 1 0.40624684 0.84375316 0.5312469 0.7812469 0.59375316 0.90624678 0.4687531 0.9687531
		 0.46875313 0.7812469 0.46875313 0.7812469 0.40624687 0.9062469 0.4062469 0.9062469
		 0.59375316 0.84375316 0.59375316 0.8437531 0.53124684 0.9687531 0.53124684 0.9687531
		 0.5312469 0.7812469 0.40624684 0.84375316 0.59375316 0.90624684 0.46875307 0.9687531
		 0.375 0.35805225 0.48305231 0.25 0.51694769 0.25 0.625 0.35805225 0.625 0.39194781
		 0.51694769 0.5 0.48305234 0.5 0.375 0.39194781 0.375 0.89194769 0.48305228 1 0.44588798
		 0.083333239 0.30411202 0.083333246 0.30411202 0.16666663 0.44588798 0.16666663 0.44588798
		 0.25 0.30411199 0.25 0.375 0.25 0.51694769 1 0.625 0.89194775 0.69588792 0.083333239
		 0.55411202 0.083333246 0.55411196 0.16666663 0.69588792 0.16666663 0.69588798 0.25
		 0.55411196 0.25 0.44588798 0.5 0.375 0.5 0.44588798 0.58333337 0.375 0.58333337 0.625
		 0.42911205 0.625 0.5 0.55411202 0.5 0.625 0.5 0.625 0.58333337 0.55411196 0.58333337
		 0.375 0.85805219 0.48305231 0.75 0.48305231 0.75 0.375 0.85805219 0.51694775 0.75
		 0.625 0.85805219 0.625 0.85805213 0.51694769 0.75 0.51694775 1 0.625 0.89194775 0.375
		 0.89194775 0.48305243 1 0.44786662 0.75 0.375 0.82286656 0.625 0.82286656 0.55213338
		 0.75 0.55213338 1 0.625 0.92713344 0.375 0.92713344 0.44786662 1 0.44786659 0.75
		 0.375 0.82286656 0.625 0.82286656 0.55213338 0.75 0.55213338 1 0.625 0.92713344 0.375
		 0.92713344 0.44786662 1 0.46875313 0.7812469 0.40624684 0.84375316 0.59375316 0.84375316
		 0.5312469 0.7812469 0.53124684 0.9687531 0.59375316 0.90624684 0.40624687 0.9062469
		 0.46875307 0.9687531 0.40624684 0.84375316 0.46875313 0.7812469 0.5312469 0.7812469
		 0.59375316 0.8437531 0.59375316 0.90624678 0.53124684 0.9687531 0.4687531 0.9687531
		 0.4062469 0.9062469 0.80411208 0.16666663 0.80411208 0.25 0.19588797 0.16666663 0.19588797
		 0.25 0.625 0.32088798 0.375 0.42911205 0.375 0.32088798 0.80411208 0.083333246 0.55411196
		 0.66666675 0.625 0.66666675 0.44588798 0.66666675 0.375 0.66666675 0.19588797 0.083333239
		 0.44588798 0.083333239 0.30411202 0.083333246 0.44588798 0.083333239 0.44588798 0.16666663
		 0.44588798 0.16666663 0.44588798 0.083333239 0.30411202 0.16666663 0.44588798 0.16666663;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 170 ".pt";
	setAttr ".pt[0:165]" -type "float3"  5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 
		5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16;
	setAttr ".pt[166:169]" 5 -5 -16 5 -5 -16 5 -5 -16 5 -5 -16;
	setAttr -s 170 ".vt";
	setAttr ".vt[0:165]"  -3.039631367 3.16566277 -1.20300865 -3.039631605 3.16566277 1.20300865
		 -1.20300937 3.16566277 3.03963089 1.20300913 3.16566277 3.03963089 1.20300913 3.16566277 -3.03963089
		 -1.20300865 3.16566277 -3.039632797 3.039631844 3.16566277 1.20300865 3.03963089 3.16566277 -1.20300865
		 -1.20300794 -4 -1.58038712 1.20301008 -4 -1.58038712 1.58038664 -4 -1.20301056 1.58038759 -4 1.2030077
		 1.20300865 -4 1.58038712 -1.20301008 -4 1.58038712 -1.58038664 -4 1.20300961 -1.58038688 -4 -1.20300865
		 -1.58038688 5.10040569 -1.20300865 -1.58038664 5.10040569 1.20300961 1.20300865 5.10040569 1.58038712
		 -1.20301008 5.10040569 1.58038712 1.58038664 5.10040569 -1.20301056 1.58038759 5.10040569 1.2030077
		 -1.20300794 5.10040569 -1.58038712 1.20301008 5.10040569 -1.58038712 -1.20300794 -5.10040665 -1.58038712
		 1.20301008 -5.10040665 -1.58038712 1.58038664 -5.10040665 -1.20301056 1.58038759 -5.10040665 1.2030077
		 1.20300865 -5.10040665 1.58038712 -1.20301008 -5.10040665 1.58038712 -1.58038664 -5.10040665 1.20300961
		 -1.58038688 -5.10040665 -1.20300865 -1.20300698 -5.78086662 -2.92443085 1.20301056 -5.78086662 -2.92442894
		 2.92442894 -5.78086662 -1.20301056 2.92442989 -5.78086662 1.20300674 1.20300865 -5.78086662 2.92442703
		 -1.20301008 -5.78086662 2.92442703 -2.92442799 -5.78086662 1.20300961 -2.92442799 -5.78086662 -1.20300865
		 -1.20300674 -6.75117683 -2.92443085 1.20301056 -6.75117683 -2.92442894 2.92442894 -6.75117683 -1.20301056
		 2.92442989 -6.75117683 1.20300674 1.20300865 -6.75117683 2.92442703 -1.20301008 -6.75117683 2.92442703
		 -2.92442799 -6.75117683 1.20300961 -2.92442799 -6.75117683 -1.20300865 -3.039631367 -2.39748955 1.20300865
		 -1.20300937 -2.39748955 3.03963089 1.20300913 -2.39748955 3.03963089 3.039631844 -2.39748955 1.20300865
		 3.03963089 -2.39748955 -1.20300865 1.20300913 -2.39748955 -3.03963089 -1.20300865 -2.39748955 -3.039632797
		 -3.039631367 -2.39748955 -1.20300865 -3.039631367 2.39748573 1.20300865 -1.20300937 2.39748573 3.03963089
		 1.20300913 2.39748573 3.03963089 3.039631844 2.39748573 1.20300865 3.03963089 2.39748573 -1.20300865
		 1.20300913 2.39748573 -3.03963089 -1.20300865 2.39748573 -3.039632797 -3.039631367 2.39748573 -1.20300865
		 -5 -2.39748955 -1.20300865 -5 -2.39748955 1.20300865 -5 2.39748573 1.20300865 -5 2.39748573 -1.20300865
		 -3.039631367 -2.39748955 3 -3.039631367 2.39748573 3 -5 2.39748573 3 -5 -2.39748955 3
		 0.6016264 -6.75117683 -1.20313072 1.2031312 -6.75117683 -0.60162544 1.2031312 -4.38454437 -0.60162544
		 0.6016264 -4.38454437 -1.20313072 -1.20312905 -6.75117683 -0.60162544 -0.60162449 -6.75117683 -1.20313072
		 -0.60162449 -4.38454437 -1.20313072 -1.20312905 -4.38454437 -0.60162544 1.20313025 -6.75117683 0.60162449
		 0.60162592 -6.75117683 1.20312881 0.60162592 -4.38454437 1.20312881 1.20313025 -4.38454437 0.60162449
		 -0.60162544 -6.75117683 1.20312977 -1.20313001 -6.75117683 0.60162449 -1.20313001 -4.38454437 0.60162449
		 -0.60162544 -4.38454437 1.20312977 -6.96036863 3.16566277 -1.20300865 -6.96036816 3.16566277 1.20300865
		 -8.79699039 3.16566277 3.03963089 -11.20300865 3.16566277 3.03963089 -11.20300865 3.16566277 -3.03963089
		 -8.79699135 3.16566277 -3.039632797 -13.039631844 3.16566277 1.20300865 -13.03963089 3.16566277 -1.20300865
		 -8.7969923 -4 -1.58038712 -11.20301056 -4 -1.58038712 -11.58038712 -4 -1.20301056
		 -11.58038712 -4 1.2030077 -11.20300865 -4 1.58038712 -8.79698944 -4 1.58038712 -8.41961288 -4 1.20300961
		 -8.41961288 -4 -1.20300865 -8.41961288 5.10040569 -1.20300865 -8.41961288 5.10040569 1.20300961
		 -11.20300865 5.10040569 1.58038712 -8.79698944 5.10040569 1.58038712 -11.58038712 5.10040569 -1.20301056
		 -11.58038712 5.10040569 1.2030077 -8.7969923 5.10040569 -1.58038712 -11.20301056 5.10040569 -1.58038712
		 -8.7969923 -5.10040665 -1.58038712 -11.20301056 -5.10040665 -1.58038712 -11.58038712 -5.10040665 -1.20301056
		 -11.58038712 -5.10040665 1.2030077 -11.20300865 -5.10040665 1.58038712 -8.79698944 -5.10040665 1.58038712
		 -8.41961288 -5.10040665 1.20300961 -8.41961288 -5.10040665 -1.20300865 -8.79699326 -5.78086662 -2.92443085
		 -11.20301056 -5.78086662 -2.92442894 -12.92442894 -5.78086662 -1.20301056 -12.92442989 -5.78086662 1.20300674
		 -11.20300865 -5.78086662 2.92442703 -8.79698944 -5.78086662 2.92442703 -7.075572014 -5.78086662 1.20300961
		 -7.075572014 -5.78086662 -1.20300865 -8.79699326 -6.75117683 -2.92443085 -11.20301056 -6.75117683 -2.92442894
		 -12.92442894 -6.75117683 -1.20301056 -12.92442989 -6.75117683 1.20300674 -11.20300865 -6.75117683 2.92442703
		 -8.79698944 -6.75117683 2.92442703 -7.075572014 -6.75117683 1.20300961 -7.075572014 -6.75117683 -1.20300865
		 -6.96036863 -2.39748955 1.20300865 -8.79699039 -2.39748955 3.03963089 -11.20300865 -2.39748955 3.03963089
		 -13.039631844 -2.39748955 1.20300865 -13.03963089 -2.39748955 -1.20300865 -11.20300865 -2.39748955 -3.03963089
		 -8.79699135 -2.39748955 -3.039632797 -6.96036863 -2.39748955 -1.20300865 -6.96036863 2.39748573 1.20300865
		 -8.79699039 2.39748573 3.03963089 -11.20300865 2.39748573 3.03963089 -13.039631844 2.39748573 1.20300865
		 -13.03963089 2.39748573 -1.20300865 -11.20300865 2.39748573 -3.03963089 -8.79699135 2.39748573 -3.039632797
		 -6.96036863 2.39748573 -1.20300865 -6.96036863 -2.39748955 3 -6.96036863 2.39748573 3
		 -10.6016264 -6.75117683 -1.20313072 -11.20313072 -6.75117683 -0.60162544 -11.20313072 -4.38454437 -0.60162544
		 -10.6016264 -4.38454437 -1.20313072 -8.79687119 -6.75117683 -0.60162544 -9.39837551 -6.75117683 -1.20313072
		 -9.39837551 -4.38454437 -1.20313072 -8.79687119 -4.38454437 -0.60162544 -11.20313072 -6.75117683 0.60162449
		 -10.60162544 -6.75117683 1.20312881 -10.60162544 -4.38454437 1.20312881 -11.20313072 -4.38454437 0.60162449;
	setAttr ".vt[166:169]" -9.39837456 -6.75117683 1.20312977 -8.79687023 -6.75117683 0.60162449
		 -8.79687023 -4.38454437 0.60162449 -9.39837456 -4.38454437 1.20312977;
	setAttr -s 328 ".ed";
	setAttr ".ed[0:165]"  16 22 0 17 19 0 17 16 0 18 21 0 19 18 0 21 20 0 23 20 0
		 23 22 0 15 14 0 14 48 0 1 0 0 0 63 0 1 17 0 13 12 0 12 50 0 3 2 0 2 57 0 3 18 0 4 5 0
		 5 22 0 4 61 0 9 8 0 8 54 0 6 7 0 7 20 0 6 59 0 11 10 0 10 52 0 16 0 0 19 2 0 21 6 0
		 23 4 0 9 25 0 25 24 0 24 8 0 11 27 0 27 26 0 26 10 0 13 29 0 29 28 0 28 12 0 15 31 0
		 31 30 0 30 14 0 25 33 1 33 32 0 32 24 1 27 35 1 35 34 0 34 26 1 29 37 1 37 36 0 36 28 1
		 31 39 1 39 38 0 38 30 1 33 41 0 41 40 0 40 32 0 35 43 0 43 42 0 42 34 0 37 45 0 45 44 0
		 44 36 0 39 47 0 47 46 0 46 38 0 41 72 1 43 80 1 45 84 1 47 76 1 2 1 0 4 7 0 6 3 0
		 0 5 0 10 9 0 12 11 0 14 13 0 8 15 0 26 25 0 28 27 0 30 29 0 24 31 0 34 33 0 36 35 0
		 38 37 0 32 39 0 42 41 0 44 43 0 46 45 0 40 47 0 48 56 0 49 13 0 50 58 0 51 11 0 52 60 0
		 53 9 0 54 62 0 55 15 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 48 0
		 56 1 0 57 49 0 58 3 0 59 51 0 60 7 0 61 53 0 62 5 0 63 55 0 56 57 1 57 58 1 58 59 1
		 59 60 1 60 61 1 61 62 1 62 63 1 63 56 0 55 64 0 48 65 1 64 65 1 56 66 1 63 67 0 67 66 1
		 67 64 1 48 68 0 56 69 0 68 69 0 66 70 1 69 70 0 65 71 1 71 70 1 68 71 0 72 77 0 73 42 1
		 74 83 0 75 78 0 75 74 0 76 85 0 77 40 1 78 79 0 80 73 0 81 44 1 82 87 0 83 82 0 84 81 0
		 85 46 1 86 79 0 87 86 0 72 75 0 74 73 0 76 79 0 78 77 0 80 83 0 82 81 0 84 87 0 86 85 0
		 72 73 0 76 77 0 80 81 0;
	setAttr ".ed[166:327]" 84 85 0 105 104 0 105 107 0 107 106 0 106 109 0 109 108 0
		 111 108 0 111 110 0 104 110 0 103 102 0 102 136 0 143 136 0 143 103 0 151 144 0 144 89 0
		 89 88 0 88 151 0 89 105 0 104 88 0 101 100 0 100 138 0 137 138 0 137 101 0 145 146 1
		 146 91 0 91 90 0 90 145 0 91 106 0 107 90 0 92 93 0 93 110 0 111 92 0 92 149 0 149 150 1
		 150 93 0 94 95 0 95 108 0 109 94 0 94 147 0 147 148 1 148 95 0 97 96 0 97 113 0 113 112 0
		 112 96 0 99 98 0 99 115 0 115 114 0 114 98 0 101 117 0 117 116 0 116 100 0 103 119 0
		 119 118 0 118 102 0 113 121 1 121 120 0 120 112 1 115 123 1 123 122 0 122 114 1 117 125 1
		 125 124 0 124 116 1 119 127 1 127 126 0 126 118 1 121 129 0 129 128 0 128 120 0 123 131 0
		 131 130 0 130 122 0 125 133 0 133 132 0 132 124 0 127 135 0 135 134 0 134 126 0 129 154 1
		 154 159 0 159 128 1 131 162 1 162 155 0 155 130 1 133 166 1 166 163 0 163 132 1 135 158 1
		 158 167 0 167 134 1 160 159 0 154 157 0 157 160 0 156 155 0 162 165 0 156 165 0 164 163 0
		 166 169 0 164 169 0 168 167 0 158 161 0 168 161 0 144 145 1 90 89 0 92 95 0 148 149 1
		 146 147 1 94 91 0 150 151 1 88 93 0 98 97 0 114 113 0 100 99 0 116 115 0 102 101 0
		 118 117 0 96 103 0 112 119 0 122 121 0 124 123 0 126 125 0 120 127 0 130 129 0 132 131 0
		 134 133 0 128 135 0 154 155 0 162 163 0 166 167 0 158 159 0 136 137 0 139 99 0 138 139 0
		 139 140 0 98 140 0 141 97 0 140 141 0 141 142 0 96 142 0 142 143 0 145 137 0 136 144 0
		 138 146 0 147 139 0 140 148 0 149 141 0 142 150 0 151 143 0 136 65 1 143 64 0 152 153 0
		 153 70 0 152 71 0 151 67 0 144 66 1 144 153 0 136 152 0 160 161 0 157 156 0 165 164 0
		 169 168 0;
	setAttr -s 160 -ch 656 ".fc[0:159]" -type "polyFaces" 
		f 8 -3 1 4 3 5 -7 7 -1
		mu 0 8 6 24 7 25 8 26 9 27
		f 4 8 9 -108 99
		mu 0 4 34 22 48 57
		f 4 123 108 10 11
		mu 0 4 67 58 15 13
		f 4 -11 12 2 28
		mu 0 4 0 15 24 6
		f 4 13 14 -102 93
		mu 0 4 32 21 50 49
		f 4 117 110 15 16
		mu 0 4 59 60 1 10
		f 4 -16 17 -5 29
		mu 0 4 10 1 25 7
		f 4 18 19 -8 31
		mu 0 4 11 2 27 9
		f 4 -19 20 121 114
		mu 0 4 2 11 64 65
		f 4 23 24 -6 30
		mu 0 4 3 4 26 8
		f 4 -24 25 119 112
		mu 0 4 17 5 61 63
		f 4 -22 32 33 34
		mu 0 4 23 28 36 35
		f 4 -27 35 36 37
		mu 0 4 20 30 38 29
		f 4 -14 38 39 40
		mu 0 4 21 32 40 31
		f 4 -9 41 42 43
		mu 0 4 22 34 42 33
		f 4 -34 44 45 46
		mu 0 4 35 36 44 43
		f 4 -37 47 48 49
		mu 0 4 29 38 45 37
		f 4 -40 50 51 52
		mu 0 4 31 40 46 39
		f 4 -43 53 54 55
		mu 0 4 33 42 47 41
		f 4 -46 56 57 58
		mu 0 4 43 44 76 79
		f 4 -49 59 60 61
		mu 0 4 37 45 80 77
		f 4 -52 62 63 64
		mu 0 4 39 46 82 81
		f 4 -55 65 66 67
		mu 0 4 41 47 78 83
		f 4 -58 68 139 145
		mu 0 4 79 76 88 97
		f 4 -61 69 147 140
		mu 0 4 77 80 92 96
		f 4 -64 70 151 148
		mu 0 4 81 82 94 98
		f 4 -67 71 144 152
		mu 0 4 83 78 90 99
		f 4 158 -140 155 142
		mu 0 4 84 97 88 89
		f 4 156 -148 159 -142
		mu 0 4 85 96 92 93
		f 4 160 -152 161 -150
		mu 0 4 86 98 94 95
		f 4 162 -145 157 -154
		mu 0 4 87 99 90 91
		f 4 116 -17 72 -109
		mu 0 4 58 59 10 15
		f 4 73 -113 120 -21
		mu 0 4 11 17 63 64
		f 4 118 -26 74 -111
		mu 0 4 60 62 12 1
		f 4 122 -12 75 -115
		mu 0 4 66 67 13 14
		f 4 -13 -73 -30 -2
		mu 0 4 24 15 10 7
		f 4 -18 -75 -31 -4
		mu 0 4 25 16 3 8
		f 4 -25 -74 -32 6
		mu 0 4 26 17 11 9
		f 4 -20 -76 -29 0
		mu 0 4 27 18 19 6
		f 4 -77 -38 80 -33
		mu 0 4 28 20 29 36
		f 4 -78 -41 81 -36
		mu 0 4 30 21 31 38
		f 4 -79 -44 82 -39
		mu 0 4 32 22 33 40
		f 4 -80 -35 83 -42
		mu 0 4 34 23 35 42
		f 4 -81 -50 84 -45
		mu 0 4 36 29 37 44
		f 4 -82 -53 85 -48
		mu 0 4 38 31 39 45
		f 4 -83 -56 86 -51
		mu 0 4 40 33 41 46
		f 4 -84 -47 87 -54
		mu 0 4 42 35 43 47
		f 4 -85 -62 88 -57
		mu 0 4 44 37 77 76
		f 4 -86 -65 89 -60
		mu 0 4 45 39 81 80
		f 4 -87 -68 90 -63
		mu 0 4 46 41 83 82
		f 4 -88 -59 91 -66
		mu 0 4 47 43 79 78
		f 4 -69 -89 -141 -164
		mu 0 4 88 76 77 96
		f 4 -70 -90 -149 -166
		mu 0 4 92 80 81 98
		f 4 -71 -91 -153 -167
		mu 0 4 94 82 83 99
		f 4 -72 -92 -146 -165
		mu 0 4 90 78 79 97
		f 4 -94 -101 -10 78
		mu 0 4 32 49 48 22
		f 4 -96 -103 -15 77
		mu 0 4 30 52 50 21
		f 4 -104 95 26 27
		mu 0 4 53 51 30 20
		f 4 -98 -105 -28 76
		mu 0 4 28 54 53 20
		f 4 -106 97 21 22
		mu 0 4 55 54 28 23
		f 4 -100 -107 -23 79
		mu 0 4 34 57 56 23
		f 4 100 -110 -117 -93
		mu 0 4 48 49 59 58
		f 4 101 94 -118 109
		mu 0 4 49 50 60 59
		f 4 102 -112 -119 -95
		mu 0 4 50 52 62 60
		f 4 -120 111 103 96
		mu 0 4 63 61 51 53
		f 4 -121 -97 104 -114
		mu 0 4 64 63 53 54
		f 4 -122 113 105 98
		mu 0 4 65 64 54 55
		f 4 106 -116 -123 -99
		mu 0 4 56 57 67 66
		f 4 107 125 -127 -125
		mu 0 4 57 48 69 68
		f 4 133 135 -138 -139
		mu 0 4 72 73 74 75
		f 4 -124 128 129 -128
		mu 0 4 58 67 71 70
		f 4 115 124 -131 -129
		mu 0 4 67 57 68 71
		f 4 92 132 -134 -132
		mu 0 4 48 58 73 72
		f 4 127 134 -136 -133
		mu 0 4 58 70 74 73
		f 4 -126 131 138 -137
		mu 0 4 69 48 72 75
		f 8 -147 -143 143 141 150 149 154 153
		mu 0 8 91 84 89 85 93 86 95 87
		f 4 163 -157 -144 -156
		mu 0 4 88 96 85 89
		f 4 164 -159 146 -158
		mu 0 4 90 97 84 91
		f 4 165 -161 -151 -160
		mu 0 4 92 98 86 93
		f 4 166 -163 -155 -162
		mu 0 4 94 99 87 95
		f 8 174 -174 172 -172 -171 -170 -169 167
		mu 0 8 100 107 106 105 104 103 102 101
		f 4 -179 177 -177 -176
		mu 0 4 108 111 110 109
		f 4 -183 -182 -181 -180
		mu 0 4 112 115 114 113
		f 4 -185 -168 -184 181
		mu 0 4 116 100 101 114
		f 4 -189 187 -187 -186
		mu 0 4 117 120 119 118
		f 4 -193 -192 -191 -190
		mu 0 4 121 124 123 122
		f 4 -195 169 -194 191
		mu 0 4 124 102 103 123
		f 4 -198 173 -197 -196
		mu 0 4 125 106 107 126
		f 4 -201 -200 -199 195
		mu 0 4 126 128 127 125
		f 4 -204 171 -203 -202
		mu 0 4 129 104 105 130
		f 4 -207 -206 -205 201
		mu 0 4 131 134 133 132
		f 4 -211 -210 -209 207
		mu 0 4 135 138 137 136
		f 4 -215 -214 -213 211
		mu 0 4 139 142 141 140
		f 4 -218 -217 -216 185
		mu 0 4 118 144 143 117
		f 4 -221 -220 -219 175
		mu 0 4 109 146 145 108
		f 4 -224 -223 -222 209
		mu 0 4 138 148 147 137
		f 4 -227 -226 -225 213
		mu 0 4 142 150 149 141
		f 4 -230 -229 -228 216
		mu 0 4 144 152 151 143
		f 4 -233 -232 -231 219
		mu 0 4 146 154 153 145
		f 4 -236 -235 -234 222
		mu 0 4 148 156 155 147
		f 4 -239 -238 -237 225
		mu 0 4 150 158 157 149
		f 4 -242 -241 -240 228
		mu 0 4 152 160 159 151
		f 4 -245 -244 -243 231
		mu 0 4 154 162 161 153
		f 4 -248 -247 -246 234
		mu 0 4 156 164 163 155
		f 4 -251 -250 -249 237
		mu 0 4 158 166 165 157
		f 4 -254 -253 -252 240
		mu 0 4 160 168 167 159
		f 4 -257 -256 -255 243
		mu 0 4 162 170 169 161
		f 4 -260 -259 246 -258
		mu 0 4 171 172 163 164
		f 4 262 -262 249 -261
		mu 0 4 173 174 165 166
		f 4 265 -265 252 -264
		mu 0 4 175 176 167 168
		f 4 268 -268 255 -267
		mu 0 4 177 178 169 170
		f 4 180 -271 192 -270
		mu 0 4 113 114 124 121
		f 4 198 -273 206 -272
		mu 0 4 125 127 134 131
		f 4 190 -275 204 -274
		mu 0 4 122 123 180 179
		f 4 200 -277 182 -276
		mu 0 4 181 182 115 112
		f 4 168 194 270 183
		mu 0 4 101 102 124 114
		f 4 170 203 274 193
		mu 0 4 103 104 129 183
		f 4 -173 197 271 202
		mu 0 4 105 106 125 131
		f 4 -175 184 276 196
		mu 0 4 107 100 185 184
		f 4 208 -279 214 277
		mu 0 4 136 137 142 139
		f 4 212 -281 217 279
		mu 0 4 140 141 144 118
		f 4 215 -283 220 281
		mu 0 4 117 143 146 109
		f 4 218 -285 210 283
		mu 0 4 108 145 138 135
		f 4 221 -286 226 278
		mu 0 4 137 147 150 142
		f 4 224 -287 229 280
		mu 0 4 141 149 152 144
		f 4 227 -288 232 282
		mu 0 4 143 151 154 146
		f 4 230 -289 223 284
		mu 0 4 145 153 148 138
		f 4 233 -290 238 285
		mu 0 4 147 155 158 150
		f 4 236 -291 241 286
		mu 0 4 149 157 160 152
		f 4 239 -292 244 287
		mu 0 4 151 159 162 154
		f 4 242 -293 235 288
		mu 0 4 153 161 156 148
		f 4 293 250 289 245
		mu 0 4 163 166 158 155
		f 4 294 253 290 248
		mu 0 4 165 168 160 157
		f 4 295 256 291 251
		mu 0 4 167 170 162 159
		f 4 296 247 292 254
		mu 0 4 169 164 156 161
		f 4 -282 176 297 188
		mu 0 4 117 109 110 120
		f 4 -280 186 299 298
		mu 0 4 140 118 119 186
		f 4 -302 -212 -299 300
		mu 0 4 187 139 140 188
		f 4 -278 301 303 302
		mu 0 4 136 139 187 189
		f 4 -306 -208 -303 304
		mu 0 4 190 135 136 189
		f 4 -284 305 306 178
		mu 0 4 108 135 191 111
		f 4 308 269 307 -298
		mu 0 4 110 113 121 120
		f 4 -308 189 -310 -188
		mu 0 4 120 121 122 119
		f 4 309 273 310 -300
		mu 0 4 119 122 179 186
		f 4 -312 -301 -311 205
		mu 0 4 134 187 188 133
		f 4 312 -304 311 272
		mu 0 4 127 189 187 134
		f 4 -314 -305 -313 199
		mu 0 4 128 190 189 127
		f 4 313 275 314 -307
		mu 0 4 191 181 112 111
		f 4 316 126 -316 -178
		mu 0 4 111 193 192 110
		f 4 319 137 -319 -318
		mu 0 4 194 197 196 195
		f 4 321 -130 -321 179
		mu 0 4 113 199 198 112
		f 4 320 130 -317 -315
		mu 0 4 112 198 193 111
		f 4 323 317 -323 -309
		mu 0 4 110 194 195 113
		f 4 322 318 -135 -322
		mu 0 4 113 195 196 199
		f 4 136 -320 -324 315
		mu 0 4 192 197 194 110
		f 8 -269 -328 -266 -327 -263 -326 259 324
		mu 0 8 178 177 176 175 174 173 172 171
		f 4 258 325 260 -294
		mu 0 4 163 172 173 166
		f 4 267 -325 257 -297
		mu 0 4 169 178 171 164
		f 4 261 326 263 -295
		mu 0 4 165 174 175 168
		f 4 264 327 266 -296
		mu 0 4 167 176 177 170;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Left_Arm" -p "BasicMechGrouped1";
	rename -uid "757FA97C-4F29-C667-8CA3-2E857511BCF8";
	setAttr ".t" -type "double3" 36 0 0 ;
	setAttr ".rp" -type "double3" -18 25.348060607910156 3.559107780456543 ;
	setAttr ".sp" -type "double3" -18 25.348060607910156 3.559107780456543 ;
createNode transform -n "upperArmLeft" -p "|BasicMechGrouped1|Basic_Left_Arm";
	rename -uid "36F0C00D-473B-CCD1-EDD4-4693566B172B";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 22.000007629394531 0 ;
	setAttr ".sp" -type "double3" -18 22.000007629394531 0 ;
createNode mesh -n "upperArmLeftShape" -p "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft";
	rename -uid "77FD71A8-42A2-E828-A2CB-E7953CC88684";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "upperArmLeftShapeOrig" -p "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft";
	rename -uid "5DF893E8-44F8-F381-84E5-95A491405BFB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0.31249374
		 0.625 0.31249374 0.375 0.43750626 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.43749374 0.25 0.5625062 3.7252903e-009 0.18749376
		 0.25 0.18749379 3.7252903e-009 0.3125062 3.7252903e-009 0.43749374 0.75 0.5625062
		 0.5 0.68749374 0.25 0.8125062 3.7252903e-009 0.81250632 0.25 0.43749377 3.7252903e-009
		 0.31250626 0.25 0.6874938 3.7252903e-009 0.56250626 0.25 0.43749377 0.5 0.375 0.81249374
		 0.625 0.43750626 0.56250626 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  6 0 0 3.0003014 0 0 6 0 0 
		3.0003014 0 0 -6 0 0 -3.0003014 0 0 -6 0 0 -3.0003014 0 0 6 0 0 3.0003014 0 0 6 0 
		0 3.0003014 0 0 -3.0003014 0 0 -6 0 0 -6 0 0 -3.0003014 0 0;
	setAttr -s 16 ".vt[0:15]"  -21 -17 1.50014997 -19.50015068 -17 3 -21 -5 1.50014997
		 -19.50015068 -5 3 -15 -17 1.50014997 -16.49984932 -17 3 -15 -5 1.50014997 -16.49984932 -5 3
		 -21 -5 -1.50014997 -19.50015068 -5 -3 -21 -17 -1.50014997 -19.50015068 -17 -3 -16.49984932 -5 -3
		 -15 -5 -1.50014997 -15 -17 -1.50014997 -16.49984932 -17 -3;
	setAttr -s 24 ".ed[0:23]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0
		 7 6 0 9 12 0 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0
		 6 4 0 8 10 0 11 9 0 12 15 0 14 13 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 4 3 -19 -1 -18
		mu 0 4 8 21 9 18
		f 4 11 -23 -9 -22
		mu 0 4 13 25 14 22
		f 4 6 -24 14 -20
		mu 0 4 15 17 16 20
		f 4 -3 -17 -11 -21
		mu 0 4 10 19 12 11
		f 8 2 -10 8 -14 -7 -8 -4 4
		mu 0 8 0 2 22 14 24 1 21 8
		f 8 10 -2 0 -6 -15 -16 -12 12
		mu 0 8 23 7 6 5 4 3 25 13
		f 4 16 -5 17 1
		mu 0 4 12 19 8 18
		f 4 18 7 19 5
		mu 0 4 9 21 15 20
		f 4 20 -13 21 9
		mu 0 4 2 23 13 22
		f 4 22 15 23 13
		mu 0 4 14 25 3 24;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "ShoulderLeft" -p "|BasicMechGrouped1|Basic_Left_Arm";
	rename -uid "54104A7B-4E41-A92D-9D00-C7AEE96AB8FC";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 34.000007629394531 0 ;
	setAttr ".sp" -type "double3" -18 34.000007629394531 0 ;
createNode mesh -n "ShoulderLeftShape" -p "ShoulderLeft";
	rename -uid "DEEEB512-41C2-4359-04FD-F98EEED2EEDA";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.57292187213897705 0.375 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "ShoulderLeftShapeOrig" -p "ShoulderLeft";
	rename -uid "D28F494F-4AF2-8995-1F32-9DA8760211EA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0.375 0.30207813
		 0.375 0.44792187 0.32292187 0.1805625 0.32292187 0 0.42707813 0.1805625 0.625 0.30207813
		 0.57292187 0.1805625 0.57292187 0 0.375 0.5694375 0.42707813 0.75 0.375 0.75 0.42707813
		 0.44792187 0.57292187 0.44792187 0.625 0.5694375 0.625 0.75 0.42707813 7.4505806e-009
		 0.42707813 0.30207813 0.57292187 0.30207813 0.42707813 0.5694375 0.57292187 0.5694375
		 0.57292187 0.75 0.67707813 7.4505806e-009 0.82292187 0 0.82292187 0.1805625 0.67707813
		 0.1805625 0.17707813 7.4505806e-009 0.17707813 0.1805625 0.625 0.44792187 0.40103906
		 0.30207813 0.375 0.24132031 0.42707813 0.24132031 0.57292187 0.24132031 0.59896094
		 0.30207813 0.625 0.24132031 0.59896094 0.44792187 0.57292187 0.50867969 0.42707813
		 0.50867969 0.40103906 0.44792187 0.375 3.7252903e-009 0.375 0.1805625 0.40103906
		 0.24132031 0.625 3.7252903e-009 0.625 0.1805625 0.59896094 0.24132031 0.40103906
		 0.75 0.40103906 0.5694375 0.40103906 0.50867969 0.4140586 0.47830078 0.59896094 0.75
		 0.59896094 0.5694375 0.59896094 0.50867969 0.58594143 0.47830078;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 37 ".uvst[1].uvsp[0:36]" -type "float2" 0.625 0.81249374
		 0.625 0.93750626 0.56250626 1 0.43749374 1 0.375 0.93750626 0.43749374 3.7252903e-009
		 0.43749374 0.25 0.375 0.25 0.68749374 -3.7252903e-009 0.625 0.25 0.375 0.5 0.375
		 0.81249374 0.56250626 0.5 0.625 0.5 0.56250626 0.75 0.56250626 -3.7252903e-009 0.56250626
		 0.25 0.43749374 0.5 0.43749374 0.75 0.68749374 0.25 0.81250626 -3.7252903e-009 0.81250632
		 0.25 0.18749374 0.25 0.18749374 -3.7252903e-009 0.31250626 -3.7252903e-009 0.31250626
		 0.25 0.40624687 0.25 0.5937531 0.25 0.40624687 0.5 0.5937531 0.5 0.5937531 0.7812469
		 0.40624687 0.9687531 0.375 0 0.40624687 0.7812469 0.5 0.875 0.5937531 0.9687531 0.625
		 -3.7252903e-009;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 65 ".pt[0:64]" -type "float3"  12 0 0 7.0004997 0 0 7.0004997 
		0 0 -7.0004997 0 0 -7.0004997 0 0 -12 0 0 12 0 0 7.0004997 0 0 7.0004997 0 0 -7.0004997 
		0 0 -7.0004997 0 0 -12 0 0 7.0004997 0 0 12 0 0 -12 0 0 -7.0004997 0 0 12 0 0 7.0004997 
		0 0 -7.0004997 0 0 -12 0 0 10 0 0 5.0004997 0 0 10 0 0 5.0004997 0 0 -10 0 0 -5.0004997 
		0 0 -5.0004997 0 0 -10 0 0 5.0004997 0 0 10 0 0 10 0 0 5.0004997 0 0 -10 0 0 -5.0004997 
		0 0 -10 0 0 -5.0004997 0 0 10.079334 0 0 7.0004997 0 0 -7.0004997 0 0 -10.079334 
		0 0 -10.079334 0 0 -7.0004997 0 0 7.0004997 0 0 10.079334 0 0 7.957428 0 0 10.079334 
		0 0 10.079334 0 0 9.1689453 0 0 -7.9574261 0 0 -10.079334 0 0 -10.079334 0 0 -9.1689434 
		0 0 7.957428 0 0 10.079334 0 0 10.079334 0 0 9.1689453 0 0 -7.9574261 0 0 -10.079334 
		0 0 -10.079334 0 0 -9.1689434 0 0 -7.9574261 0 0 7.957428 0 0 7.957428 0 0 0 0 0 
		-7.9574261 0 0;
	setAttr -s 65 ".vt[0:64]"  -24 4.50024986 3.5002501 -21.50024986 4.50024986 6
		 -21.50024986 7 3.5002501 -14.49975014 7 3.5002501 -14.49975014 4.50024986 6 -12 4.50024986 3.5002501
		 -24 4.50024986 -3.5002501 -21.50024986 7 -3.5002501 -21.50024986 4.50024986 -6 -14.49975014 4.50024986 -6
		 -14.49975014 7 -3.5002501 -12 4.50024986 -3.5002501 -21.50024986 -2 6 -24 -2 3.5002501
		 -12 -2 3.5002501 -14.49975014 -2 6 -24 -2 -3.5002501 -21.50024986 -2 -6 -14.49975014 -2 -6
		 -12 -2 -3.5002501 -23 -5 2.5002501 -20.50024986 -5 5 -23 -2 2.5002501 -20.50024986 -2 5
		 -13 -5 2.5002501 -15.49975014 -5 5 -15.49975014 -2 5 -13 -2 2.5002501 -20.50024986 -2 -5
		 -23 -2 -2.5002501 -23 -5 -2.5002501 -20.50024986 -5 -5 -13 -2 -2.5002501 -15.49975014 -2 -5
		 -13 -5 -2.5002501 -15.49975014 -5 -5 -23.03966713 6.07776022 3.5002501 -21.50024986 6.07776022 5.039667606
		 -14.49975014 6.07776022 5.039667606 -12.96033287 6.07776022 3.5002501 -12.96033287 6.07776022 -3.5002501
		 -14.49975014 6.07776022 -5.03966713 -21.50024986 6.07776022 -5.039667606 -23.03966713 6.077759743 -3.5002501
		 -21.97871399 -2 3.9787128 -23.03966713 -2 5.039667606 -23.03966713 4.50024986 5.039667606
		 -22.58447266 5.66208887 4.58447266 -14.021286964 -2 3.9787128 -12.96033287 -2 5.039667606
		 -12.96033287 4.50024986 5.039667606 -13.4155283 5.66208887 4.58447266 -21.97871399 -2 -3.9787128
		 -23.03966713 -2 -5.039667606 -23.03966713 4.50024986 -5.039667606 -22.58447266 5.66208887 -4.58447266
		 -14.021286964 -2 -3.9787128 -12.96033287 -2 -5.039667606 -12.96033287 4.50024986 -5.039667606
		 -13.4155283 5.66208887 -4.58447266 -14.021286964 -5 -3.9787128 -21.97871399 -5 3.97871256
		 -21.97871399 -5 -3.97871256 -18 -5 0 -14.021286964 -5 3.9787128;
	setAttr -s 124 ".ed[0:123]"  21 25 0 21 61 0 24 64 0 30 20 0 31 35 0 31 62 0
		 34 24 0 35 60 0 0 36 0 2 7 0 7 43 0 6 0 0 1 46 0 0 13 0 13 45 0 12 1 0 2 37 0 1 4 0
		 4 38 0 3 2 0 3 39 0 5 11 0 11 40 0 10 3 0 5 50 0 4 15 0 15 49 0 14 5 0 6 54 0 8 17 0
		 17 53 0 16 6 0 8 42 0 7 10 0 10 41 0 9 8 0 9 58 0 11 19 0 19 57 0 18 9 0 13 22 1
		 22 44 0 23 12 1 15 26 1 26 48 0 27 14 1 17 28 1 28 52 0 29 16 1 19 32 1 32 56 0 33 18 1
		 21 23 0 22 20 0 24 27 0 26 25 0 28 31 0 30 29 0 32 34 0 35 33 0 12 15 0 18 17 0 14 19 0
		 16 13 0 26 23 0 28 33 0 32 27 0 22 29 0 36 2 0 37 1 0 38 3 0 39 5 0 40 10 0 41 9 0
		 42 7 0 43 6 0 36 47 1 37 38 1 38 51 1 39 40 1 40 59 1 41 42 1 42 55 1 43 36 1 44 23 0
		 45 12 0 46 0 0 47 37 1 44 45 1 45 46 1 46 47 1 48 27 0 49 14 0 50 4 0 51 39 1 48 49 1
		 49 50 1 50 51 1 52 29 0 53 16 0 54 8 0 55 43 1 52 53 1 53 54 1 54 55 1 56 33 0 57 18 0
		 58 11 0 59 41 1 56 57 1 57 58 1 58 59 1 60 34 0 61 20 0 60 63 1 62 30 0 63 61 1 64 25 0
		 62 63 1 63 64 1 64 60 1 60 62 1 62 61 1 61 64 1;
	setAttr -s 61 -ch 248 ".fc[0:60]" -type "polyFaces" 
		f 4 3 -114 -123 115
		mu 1 4 11 4 31 33
		f 4 -11 -10 -69 -84
		mu 0 4 37 11 16 28
		f 4 -15 -14 -87 -90
		mu 0 4 38 3 2 39
		f 4 -20 -71 -78 -17
		mu 0 4 16 17 31 30
		f 4 -24 -73 -80 -21
		mu 0 4 17 12 34 32
		f 4 -27 -26 -94 -97
		mu 0 4 41 7 6 42
		f 4 -31 -30 -101 -104
		mu 0 4 44 9 18 45
		f 4 -35 -34 -75 -82
		mu 0 4 35 12 11 36
		f 4 -39 -38 -108 -111
		mu 0 4 48 14 13 49
		f 4 -42 -41 14 -89
		f 4 -45 -44 26 -96
		f 4 -48 -47 30 -103
		f 4 -51 -50 38 -110
		f 6 -54 41 84 -53 1 113
		mu 1 6 24 7 26 6 5 32
		f 6 -56 44 91 -55 2 117
		mu 1 6 15 16 27 9 8 36
		f 6 -58 -116 -6 -57 47 98
		mu 1 6 10 11 33 18 17 28
		f 6 -60 7 112 -59 50 105
		mu 1 6 12 14 30 0 13 29
		f 4 15 17 25 -61
		mu 0 4 15 4 6 7
		f 4 9 33 23 19
		mu 0 4 16 11 12 17
		f 4 29 -62 39 35
		mu 0 4 18 9 20 19
		f 4 27 21 37 -63
		mu 0 4 21 24 23 22
		f 4 31 11 13 -64
		mu 0 4 25 26 2 3
		f 4 -65 55 -1 52
		mu 1 4 6 16 15 5
		f 4 56 4 59 -66
		mu 1 4 17 18 14 12
		f 4 -67 58 6 54
		mu 1 4 19 21 20 8
		f 4 -68 53 -4 57
		mu 1 4 22 25 24 23
		f 4 43 64 42 60
		f 4 40 67 48 63
		f 4 46 65 51 61
		f 4 49 66 45 62
		f 4 68 16 -88 -77
		mu 0 4 29 16 30 40
		f 4 20 -95 -79 70
		mu 0 4 17 33 43 31
		f 4 74 10 -102 -83
		mu 0 4 36 11 37 47
		f 4 72 34 -109 -81
		mu 0 4 34 12 35 51
		f 4 8 76 -91 86
		mu 0 4 2 29 40 39
		f 4 -19 -18 -70 77
		mu 0 4 31 6 4 30
		f 4 93 18 78 -98
		mu 0 4 42 6 31 43
		f 4 -23 -22 -72 79
		mu 0 4 34 27 5 32
		f 4 107 22 80 -112
		mu 0 4 50 27 34 51
		f 4 -36 -74 81 -33
		mu 0 4 18 19 35 36
		f 4 100 32 82 -105
		mu 0 4 46 18 36 47
		f 4 -12 -76 83 -9
		mu 0 4 0 1 37 28
		f 4 -43 -85 88 85
		f 4 -16 -86 89 -13
		mu 0 4 4 15 38 39
		f 4 87 69 12 90
		mu 0 4 40 30 4 39
		f 4 -46 -92 95 92
		f 4 -28 -93 96 -25
		mu 0 4 24 21 41 42
		f 4 71 24 97 94
		mu 0 4 33 24 42 43
		f 4 -49 -99 102 99
		f 4 -32 -100 103 -29
		mu 0 4 8 10 44 45
		f 4 28 104 101 75
		mu 0 4 1 46 47 37
		f 4 -52 -106 109 106
		f 4 -40 -107 110 -37
		mu 0 4 19 20 48 49
		f 4 36 111 108 73
		mu 0 4 19 50 51 35
		f 4 -2 0 -118 -124
		mu 1 4 31 3 2 35
		f 4 -8 -5 5 -122
		mu 1 4 30 14 18 33
		f 4 -3 -7 -113 -121
		mu 1 4 35 1 0 30
		f 3 120 114 119
		mu 1 3 35 30 34
		f 3 -115 121 118
		mu 1 3 34 30 33
		f 3 -117 -119 122
		mu 1 3 31 34 33
		f 3 123 -120 116
		mu 1 3 31 35 34;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "fistLeft" -p "|BasicMechGrouped1|Basic_Left_Arm";
	rename -uid "EBCA205A-4EF0-AB82-5F2B-98A989724684";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -17.924195289611816 15.685355186462402 12.309107780456543 ;
	setAttr ".sp" -type "double3" -17.924195289611816 15.685355186462402 12.309107780456543 ;
createNode mesh -n "fistLeftShape" -p "fistLeft";
	rename -uid "B7F070D0-4AC8-01C5-89A7-D3AC620AA618";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "fistLeftShapeOrig" -p "fistLeft";
	rename -uid "1673AB45-47FE-604B-1D22-BE81C979AD7F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 232 ".uvst[0].uvsp[0:231]" -type "float2" 0.5 0.25 0.5 0.18749997
		 0.625 0.18749997 0.625 0.25 0.59375 0.32499999 0.59375 0.28749999 0.625 0.28749999
		 0.625 0.32499999 0.62669051 0.79823244 0.5625 0.96249998 0.5625 0.92500001 0.63268542
		 0.83147663 0.29999998 0.25 0.29999995 0.18749997 0.33749998 0.18749997 0.33749998
		 0.25 0.61875004 0.26875001 0.69441587 0.2877098 0.60000002 0.37500006 0.54312503
		 0.34437501 0.5 0.0625 0.5 0 0.625 0 0.625 0.0625 0.50562501 0.46937501 0.5625 0.92500001
		 0.51875001 0.92499995 0.33749998 0 0.33749998 0.0625 0.29999998 0.0625 0.29999998
		 0 0.5 0.12499993 0.625 0.12499993 0.66250002 0.12499994 0.66250002 0.0625 0.70000005
		 0.0625 0.70000005 0.12499994 0.5625 0.58750004 0.58125001 0.48125011 0.66250002 0.58750015
		 0.33749998 0.12499993 0.29999995 0.12499993 0.66250002 0.18749997 0.70000005 0.18749997
		 0.5 0.92500001 0.5 0.96249998 0.375 0.96249998 0.375 0.92500001 0.39999998 0.25 0.48124999
		 0.41875002 0.39999998 0.25 0.41874999 0.26874995 0.5 0.37500003 0.48124999 0.41875002
		 0.4375 0.28749996 0.51875001 0.33125001 0.375 0.32499999 0.5 0.32499999 0.51875001
		 0.33125001 0.4375 0.28749996 0.5 0.28749999 0.375 0.28749999 0.5 0.25 0.5 0.28749999
		 0.375 0.25 0.64375001 0.25 0.64375001 0.18749997 0.65296507 0.19737829 0.66218007
		 0.20725662 0.65312505 0.12499994 0.66250002 0.12499994 0.65296388 0.056829706 0.6621778
		 0.051159408 0.64375001 0.0625 0.64375001 0 0.5 0.96249998 0.5 1 0.375 1 0.37452209
		 0.18738748 0.63062501 0.25 0.63062501 0.18749997 0.63062501 0.12499994 0.64375001
		 0.12499994 0.63062501 0.0625 0.63062501 0 0.5625 0.96249998 0.51875001 0.96249998
		 0.50562501 0.46937501 0.52437502 0.40687507 0.58125001 0.48125011 0.5625 0.58750004
		 0.54312503 0.34437501 0.60000002 0.37500006 0.51875001 0.32499999 0.51875001 0.28749999
		 0.5625 0.28749999 0.5625 0.32499999 0.5 0.25 0.375 0.25 0.5 0.28749999 0.5 0.96249998
		 0.5 1 0.375 1 0.51875001 0.96249998 0.51875001 0.28749999 0.5625 0.28749999 0.5625
		 0.32499999 0.5 0.22113958 0.5 0.19227916 0.5 0.18749997 0.625 0.25 0.5 0 0.625 0
		 0.5 0.96249998 0.5 0.28749999 0.5 0.25 0.375 0.25 0.375 0 0.5 0 0.375 0 0.5 0.25
		 0.375 0.25 0.5 0 0.375 0 0.5 0.25 0.375 0.25 0.5 0 0.375 0 0.69999993 0.050078563
		 0.70000005 0.12499994 0.5625 0.28749999 0.5625 0.32499999 0.62666768 0.28135902 0.625
		 0.28749999 0.59375 0.28749999 0.62652498 0.32181895 0.625 0.32499999 0.59375 0.32499999
		 0.59375 0.32499999 0.5625 0.28749999 0.5625 0.32499999 0.59375 0.28749999 0.625 0.28749999
		 0.625 0.32499999 0.59375 0.32499999 0.5625 0.28749999 0.5625 0.32499999 0.37452209
		 0.062612481 0.375 0 0.37452209 0.12499993 0.5 0.18272077 0.5 0.15624996 0.5 0.12977915
		 0.5 0.12499993 0.5 0.12022072 0.5 0.09374997 0.5 0.06727922 0.5 0.0625 0.5 0.05772078
		 0.5 0.02886039 0.5 0.19228107 0.5 0.22114053 0.375 0.22114033 0.375 0.19228064 0.5
		 0.1297795 0.5 0.1562496 0.375 0.15624985 0.375 0.12977952 0.5 0.06728024 0.5 0.093750291
		 0.375 0.09375006 0.375 0.067279749 0.5 0.028859444 0.375 0.028859658 0.375 0.028860366
		 0.375 0.057720732 0.375 0.06727922 0.375 0.09374997 0.375 0.12022072 0.375 0.12977916
		 0.375 0.15624996 0.375 0.18272075 0.375 0.19227916 0.375 0.22113958 0.5 0.19227916
		 0.5 0.22113958 0.375 0.22113958 0.375 0.19227917 0.5 0.12977915 0.5 0.15624994 0.375
		 0.15624994 0.375 0.12977915 0.5 0.06727922 0.5 0.09374997 0.375 0.09374997 0.375
		 0.067279227 0.5 0.02886039 0.375 0.02886039 0.5 0.22113979 0.5 0.19227958 0.375 0.2211397
		 0.375 0.1922794 0.5 0.15624997 0.5 0.1297795 0.375 0.15624996 0.375 0.1297794 0.5
		 0.09374994 0.5 0.067279533 0.375 0.09374997 0.375 0.067279458 0.5 0.02886021 0.375
		 0.028860271 0.5 0.18271969 0.5 0.18272045 0.375 0.18272051 0.37499997 0.1827202 0.5
		 0.12022034 0.5 0.12022036 0.375 0.12022048 0.375 0.12022036 0.49999997 0.057718888
		 0.5 0.057720419 0.375 0.057720542 0.375 0.057719316 0.5 0.05772078 0.375 0.05772078
		 0.5 0.12022071 0.375 0.12022071 0.5 0.18272075 0.375 0.18272075 0.41874999 0.26874995
		 0.65937507 0.25937501;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 203 ".pt";
	setAttr ".pt[0:165]" -type "float3"  7.9231529 0 0 0 0 0 8.2723274 0 0 0 
		0 0 -8 0 0 -8 0 0 -8 0 0 0 0 0 2.9913406 0 0 2.9913406 0 0 0 0 0 -8 0 0 -8 0 0 -8 
		0 0 -4 0 0 -4 0 0 -4 0 0 -4 0 0 -4 0 0 -4 0 0 -5.9820862 0 0 -5.9820862 0 0 -5.9820862 
		0 0 -4 0 0 9.3449135 0 0 8.303894 0 0 9.3449135 0 0 8.4297714 0 0 8.4297714 0 0 9.3449135 
		0 0 9.3449135 0 0 8.303894 0 0 9.6152611 0 0 9.6152611 0 0 2.8635826 0 0 0 0 0 0 
		0 0 2.8635826 0 0 8.4271545 0 0 0 0 0 0 0 0 8.4271545 0 0 -4 0 0 -4 0 0 -4 0 0 -4 
		0 0 -1.2247009 0 0 -1.2247009 0 0 -1.2000008 0 0 -1.2000008 0 0 -1.2247009 0 0 -1.2247009 
		0 0 -1.2000008 0 0 -1.2000008 0 0 -1.2247009 0 0 -1.2000008 0 0 -1.2000008 0 0 -1.2000008 
		0 0 -2.9913406 0 0 -2.9913406 0 0 -1.2000008 0 0 -1.2000008 0 0 -1.1537895 0 0 -1.3644104 
		0 0 -1.3644104 0 0 -1.1537895 0 0 -1.5324135 0 0 -5.84408 0 0 -5.6249084 0 0 -1.5324135 
		0 0 -1.4597931 0 0 -3.3271675 0 0 -3.4777527 0 0 -1.6103783 0 0 -8.5632973 0 0 -8.5632973 
		0 0 -8.5632973 0 0 -8.5632973 0 0 -8.5632973 0 0 -8.5632973 0 0 -4.7401085 0 0 -4.1891975 
		0 0 -8.1840973 0 0 -8.735014 0 0 -5.4198418 0 0 -4.3257561 0 0 -8.2361145 0 0 -9.330204 
		0 0 -5.597805 0 0 -4.6391983 0 0 -8.0653496 0 0 -9.023962 0 0 8.3223457 0 0 7.9231529 
		0 0 9.356842 0 0 8.6958504 0 0 8.3223457 0 0 9.6334229 0 0 8.2723274 0 0 8.6958504 
		0 0 9.356842 0 0 -1.3644104 0 0 -1.3644104 0 0 0 0 0 -0.61228943 0 0 0 0 0 -1.1537895 
		0 0 -1.1537895 0 0 -1.3644104 0 0 -1.3644104 0 0 0 0 0 -0.61228943 0 0 0 0 0 -1.1537895 
		0 0 -1.1537895 0 0 -1.3644104 0 0 -1.3644104 0 0 0 0 0 -0.61228943 0 0 0 0 0 -1.1537895 
		0 0 -1.1537895 0 0 -5.84408 0 0 -6.4099693 0 0 -1.5324135 0 0 -1.5324135 0 0 -6.4099693 
		0 0 -6.0833778 0 0 -1.5324135 0 0 -1.5324135 0 0 -6.0833778 0 0 -5.6249084 0 0 -1.5324135 
		0 0 -1.5324135 0 0 -3.3967018 0 0 -3.402462 0 0 -1.5293236 0 0 -1.5350838 0 0 -3.4024582 
		0 0 -3.4024582 0 0 -1.53508 0 0 -1.53508 0 0 -3.4024582 0 0 -3.4082184 0 0 -1.5350838 
		0 0 -1.540844 0 0 0.06590271 0 0 -1.1393242 0 0 -1.5348244 0 0 -1.495285 0 0 -3.4459076 
		0 0 -6.1473179 0 0 -1.359333 0 0 8.6378174 0 0 0.06590271 0 0 -1.1393242 0 0 -1.5348244 
		0 0 -1.5376129 0 0 -3.4882355 0 0 -6.7132111 0 0 -1.359333 0 0 9.0613403 0 0 0.06590271 
		0 0 -1.1393242 0 0 -1.5348244 0 0 -1.5376129 0 0;
	setAttr ".pt[166:202]" -3.4882355 0 0 -6.3866215 0 0 -1.359333 0 0 8.6878357 
		0 0 0.06590271 0 0 -1.1393242 0 0 -1.5348244 0 0 -1.5799484 0 0 -3.530571 0 0 -5.9281483 
		0 0 -1.359333 0 0 8.2886429 0 0 -5.9380798 0 0 -6.7420673 0 0 -7.4369431 0 0 -7.3670235 
		0 0 -6.3220806 0 0 -6.244318 0 0 -6.1415386 0 0 5.9820862 0 0 5.9820862 0 0 5.9820862 
		0 0 5.9820862 0 0 5.9820862 0 0 2.9913406 0 0 2.9913406 0 0 5.9820862 0 0 2.9913406 
		0 0 -2.9913406 0 0 -2.9913406 0 0 -5.9820862 0 0 -5.9820862 0 0 -2.9913406 0 0 -5.9820862 
		0 0 -6 0 0 -6 0 0 -6 0 0;
	setAttr -s 203 ".vt";
	setAttr ".vt[0:165]"  -21.96157646 -22 14.73168659 -18 -22 11.70988083 -22.13616371 -14 14.83248425
		 -18 -14 11.70988083 -14 -15.8485508 7 -14 -20.1514492 7 -14 -18.000003814697 7 -18 -22 13.49056625
		 -19.49567032 -20.99104309 7 -19.49567032 -15.0089569092 7 -18 -14 13.49056625 -14 -16.11745453 11
		 -14 -18.000001907349 11 -14 -19.88254547 11 -16 -14.28015041 11 -16 -16 11 -16 -18.000001907349 11
		 -16 -20 11 -16 -22 11 -16 -22 7 -15.0089569092 -19.49567032 7 -15.0089569092 -18.000001907349 7
		 -15.0089569092 -16.50432968 7 -16 -13.90914822 7 -22.67245674 -16 7 -22.15194702 -13.84805298 7
		 -22.67245674 -16 11 -22.21488571 -13.78511524 11 -22.21488571 -22.21488571 11 -22.67245674 -20 11
		 -22.67245674 -20 7 -22.15194702 -22.15194702 7 -22.80763054 -18.000001907349 11 -22.80763054 -18.000001907349 7
		 -19.43179131 -22.30389404 7 -18 -22.30389404 11.70988083 -18 -13.69610596 11.70988083
		 -19.43179131 -13.69610596 7 -22.21357727 -13.84805298 14.87717915 -18 -13.69610596 13.49056625
		 -18 -22.30389404 13.49056625 -22.21357727 -22.15194702 14.87717915 -16 -22.30389404 11
		 -16 -22.30389404 7 -16 -13.69610596 11 -16 -13.69610596 7 -17.38764954 -14 13.49056625
		 -17.38764954 -16 13.49056625 -17.39999962 -16 11.70988083 -17.39999962 -14 11.70988083
		 -17.38764954 -20 13.49056625 -17.38764954 -22 13.49056625 -17.39999962 -22 11.70988083
		 -17.39999962 -20 11.70988083 -17.38764954 -18.000001907349 13.49056625 -17.39999962 -18.000001907349 11.70988083
		 -17.39999962 -22.30389404 11.70988083 -17.39999962 -22.30389404 7 -16.50432968 -20.99104309 7
		 -16.50432968 -15.0089569092 7 -17.39999962 -13.69610596 7 -17.39999962 -13.69610596 11.70988083
		 -17.42310524 -14 13.82323742 -17.3177948 -14 16.8607235 -17.3177948 -22 16.8607235
		 -17.42310524 -22 13.82323742 -17.23379326 -14 13.54300213 -15.077960014 -14 14.41306496
		 -15.18754578 -22 14.44242859 -17.23379326 -22 13.54300213 -17.27010345 -13.981987 12.018017769
		 -16.33641624 -13.9562912 11.85221672 -16.26112366 -21.95451736 12.24091625 -17.19481087 -21.98021317 12.4067173
		 -13.71835136 -18.000001907349 11 -13.71835136 -19.88254547 11 -13.71835136 -20.1514492 7
		 -13.71835136 -18.000003814697 7 -13.71835136 -16.11745453 11 -13.71835136 -15.8485508 7
		 -15.62994576 -12.32540989 9.92006111 -15.90540123 -13.82305622 11.18325233 -13.90795135 -14.23598671 11.15792656
		 -13.63249302 -12.73834038 9.89473534 -15.29007912 -12.7365427 13.3777504 -15.83712196 -13.94207096 11.90740204
		 -13.88194275 -14.30953407 11.45662308 -13.33489799 -13.10400772 12.92696762 -15.20109749 -15.089771271 14.16748238
		 -15.68040085 -15.76082897 13.349226 -13.96732521 -15.96496964 13.098031998 -13.48801899 -15.29391289 13.91628647
		 -22.16117287 -19.84706497 14.84692287 -21.96157646 -20.15293503 14.73168659 -22.67842102 -20 15.12247467
		 -22.34792519 -17.84706688 14.95474434 -22.16117287 -18.15293694 14.84692287 -22.81671143 -18.000003814697 15.1242733
		 -22.13616371 -15.84706593 14.83248425 -22.34792519 -16.15293503 14.95474434 -22.67842102 -16 15.12247467
		 -17.3177948 -15.84706593 16.8607235 -17.3177948 -16.15293503 17.26873589 -18 -16.15293503 13.49056625
		 -17.69385529 -16 13.49056625 -18 -15.84706593 13.49056625 -17.42310524 -16.15293503 13.82323742
		 -17.42310524 -15.84706593 13.82323742 -17.3177948 -17.84706688 17.26873589 -17.3177948 -18.15293694 17.10641098
		 -18 -18.15293694 13.49056625 -17.69385529 -18.000001907349 13.49056625 -18 -17.84706688 13.49056625
		 -17.42310524 -18.15293694 13.82323742 -17.42310524 -17.84706688 13.82323742 -17.3177948 -19.84706497 17.10641098
		 -17.3177948 -20.15293503 16.8607235 -18 -20.15293503 13.49056625 -17.69385529 -20 13.49056625
		 -18 -19.84706497 13.49056625 -17.42310524 -20.15293503 13.82323742 -17.42310524 -19.84706497 13.82323742
		 -15.077960014 -15.8470583 14.41306496 -14.79501534 -16.15294266 14.33725071 -17.23379326 -16.15294456 13.54300213
		 -17.23379326 -15.84705257 13.54300213 -14.79501534 -17.84705925 14.33725071 -14.95831108 -18.15294456 14.38100529
		 -17.23379326 -18.15294838 13.54300213 -17.23379326 -17.84705544 13.54300213 -14.95831108 -19.84705734 14.38100529
		 -15.18754578 -20.15294266 14.44242859 -17.23379326 -20.15294647 13.54300213 -17.23379326 -19.84705544 13.54300213
		 -16.30164909 -15.80249119 11.86960125 -16.298769 -16.10835648 11.5025835 -17.23533821 -15.82817364 12.035404205
		 -17.23245811 -16.13406944 11.66838646 -16.2987709 -17.80245781 11.50258255 -16.2987709 -18.10835075 11.83088589
		 -17.23246002 -17.82815552 11.66838551 -17.23246002 -18.13404846 11.99668884 -16.2987709 -19.80245209 11.83088589
		 -16.29589081 -20.10831833 12.22353268 -17.23245811 -19.82813263 11.99668694 -17.22957802 -20.13402748 12.38933372
		 -18.032951355 -14.92353249 13.46891785 -17.43033791 -14.92353249 13.83822346 -17.23258781 -14.92352676 13.52712727
		 -17.25235748 -14.9050808 11.69549084 -16.2770462 -14.87939072 11.51143074 -14.92634106 -14.92352867 14.49419689
		 -17.32033348 -14.92353249 17.21020126 -22.31890869 -14.92353249 14.91299438 -18.032951355 -17 13.46891785
		 -17.43033791 -17 13.83822346 -17.23258781 -17 13.52712822 -17.23119354 -16.98111343 11.33828259
		 -16.25588226 -16.95540619 11.15422058 -14.64339447 -17 14.41838264 -17.32033348 -17 17.61821556
		 -22.53067017 -17 15.035255432 -18.032951355 -19 13.46891785 -17.43033791 -19 13.83822346
		 -17.23258781 -19.000001907349 13.52712727 -17.23119354 -18.98109055 11.66658401;
	setAttr ".vt[166:202]" -16.25588226 -18.95540237 11.48252392 -14.80668926 -19 14.46213722
		 -17.32033348 -19 17.45588684 -22.34391785 -19 14.92743397 -18.032951355 -21.076467514 13.46891785
		 -17.43033791 -21.076467514 13.83822346 -17.23258781 -21.076473236 13.52712727 -17.21002579 -21.057121277 12.069037437
		 -16.23471451 -21.031417847 11.88497829 -15.035925865 -21.076471329 14.52356052 -17.32033348 -21.076467514 17.21020126
		 -22.14432144 -21.076467514 14.81219673 -15.030960083 -13.82825565 7 -14.62896633 -12.3874588 9.801157
		 -14.28152847 -12.82954788 13.33837032 -14.31648827 -15.41512585 14.30800915 -14.83895969 -16.17894745 13.41605854
		 -14.877841 -14.2017231 11.73559952 -14.92923069 -14.092132568 11.17811584 -20.99104309 -19.49567032 7
		 -20.99104309 -18.000001907349 7 -20.99104309 -16.50432968 7 -20.99104309 -19.49567032 9.51961899
		 -20.99104309 -18.000001907349 9.51961899 -19.49567032 -18.000001907349 9.51961899
		 -19.49567032 -20.99104309 9.51961899 -20.99104309 -16.50432968 9.51961899 -19.49567032 -15.0089569092 9.51961899
		 -16.50432968 -20.99104309 9.51961899 -16.50432968 -18.000001907349 9.51961899 -15.0089569092 -18.000001907349 9.51961899
		 -15.0089569092 -19.49567032 9.51961899 -16.50432968 -15.0089569092 9.51961899 -15.0089569092 -16.50432968 9.51961899
		 -15 -16.7118988 11 -15 -18 11 -15 -19.2881012 11;
	setAttr -s 397 ".ed";
	setAttr ".ed[0:165]"  2 10 0 5 19 0 5 20 1 6 5 1 6 21 1 4 6 1 4 22 1 7 1 0
		 8 185 0 9 187 0 10 3 0 10 146 0 11 4 0 13 5 0 13 18 0 11 12 0 12 13 0 14 11 1 15 200 1
		 16 201 1 17 202 1 18 52 1 20 58 0 22 59 0 23 178 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 20 21 0 21 22 0 23 14 0 24 25 0 24 26 1 26 27 1 27 25 0 28 29 1 30 29 1 31 30 0
		 31 28 0 29 32 1 33 32 1 30 33 0 32 26 1 33 24 0 8 34 1 1 35 0 35 34 1 3 36 0 27 36 1
		 9 37 1 37 36 1 25 37 0 2 38 1 10 39 0 38 39 0 39 36 0 38 27 0 7 40 0 40 35 0 0 41 1
		 41 40 0 28 41 0 29 94 1 41 94 0 32 97 1 26 100 1 18 42 0 19 43 0 42 43 0 42 56 0
		 43 57 0 14 44 0 36 61 0 23 45 0 45 44 0 37 60 0 10 46 0 46 47 0 47 48 1 3 49 0 48 49 0
		 46 49 0 7 51 0 50 51 0 1 52 0 51 52 0 52 53 0 50 53 1 54 50 0 53 55 0 54 55 1 47 54 0
		 55 48 0 49 14 1 48 15 1 55 16 1 53 17 1 56 35 0 57 34 0 58 8 0 59 9 0 60 45 0 61 44 0
		 52 56 1 56 57 1 57 58 1 59 60 1 60 61 1 61 49 1 10 62 0 2 63 0 63 62 1 0 64 0 7 65 0
		 64 65 1 62 66 0 63 67 0 67 66 1 64 68 0 65 69 0 68 69 1 66 70 0 67 71 0 71 70 0 68 72 0
		 69 73 0 72 73 0 12 74 1 13 75 0 74 75 0 5 76 0 75 76 0 6 77 1 77 76 0 74 77 1 11 78 0
		 78 74 0 4 79 0 79 77 0 78 79 0 23 80 0 14 81 0 80 81 1 11 82 0 81 184 0 4 83 0 82 83 1
		 80 179 0 80 84 0 81 85 0 84 85 1 82 86 0 85 183 0 83 87 0 86 87 1 84 180 0 84 88 0
		 85 89 0 88 89 0 86 90 0 89 182 0 87 91 0 90 91 0 88 181 0;
	setAttr ".ed[166:331]" 94 97 0 97 100 0 100 38 0 103 154 0 104 47 1 103 104 0
		 105 104 0 110 162 0 111 54 1 110 111 0 112 111 0 117 170 0 118 50 1 117 118 0 119 118 0
		 93 116 0 115 92 0 92 94 0 94 93 0 96 109 0 108 95 0 95 97 0 97 96 0 99 102 0 101 98 0
		 98 100 0 100 99 0 102 123 0 122 101 0 105 107 0 106 103 0 107 125 0 124 106 0 109 127 0
		 126 108 0 112 114 0 113 110 0 114 129 0 128 113 0 116 131 0 130 115 0 119 121 0 120 117 0
		 121 133 0 132 120 0 123 135 0 134 122 0 125 136 0 137 124 0 127 139 0 138 126 0 129 140 0
		 141 128 0 131 143 0 142 130 0 133 144 0 145 132 0 135 137 0 139 141 0 143 145 0 136 149 0
		 71 150 0 140 157 0 135 158 0 144 165 0 139 166 0 73 173 0 143 174 0 0 177 0 92 169 0
		 95 161 0 98 153 0 62 147 0 101 152 0 106 155 0 108 160 0 113 163 0 115 168 0 120 171 0
		 64 176 0 66 148 0 122 151 0 124 156 0 126 159 0 128 164 0 130 167 0 132 172 0 68 175 0
		 98 99 0 95 96 0 92 93 0 98 105 0 101 107 0 122 125 0 123 124 0 126 129 0 127 128 0
		 130 133 0 131 132 0 116 120 0 115 121 0 109 113 0 108 114 0 102 106 0 99 103 0 95 112 0
		 96 110 0 92 119 0 93 117 0 119 117 0 112 110 0 105 103 0 146 105 0 147 107 0 148 125 0
		 149 70 0 150 134 0 151 67 0 152 63 0 153 2 0 146 147 1 147 148 1 148 149 1 150 151 1
		 151 152 1 152 153 1 154 112 0 155 114 0 156 129 0 157 137 0 158 138 0 159 123 0 160 102 0
		 161 99 0 154 155 1 155 156 1 156 157 1 158 159 1 159 160 1 160 161 1 162 119 0 163 121 0
		 164 133 0 165 141 0 166 142 0 167 127 0 168 109 0 169 96 0 162 163 1 163 164 1 164 165 1
		 166 167 1 167 168 1 168 169 1 170 7 0 171 65 0 172 69 0 173 145 0 174 72 0 175 131 0
		 176 116 0 177 93 0 170 171 1 171 172 1 172 173 1 174 175 1;
	setAttr ".ed[332:396]" 175 176 1 176 177 1 178 4 0 179 83 0 180 87 0 181 91 0
		 182 90 0 183 86 0 184 82 0 178 179 1 179 180 1 180 181 1 181 182 0 182 183 1 183 184 1
		 0 7 0 35 28 1 34 31 0 185 30 1 186 33 1 187 24 1 149 150 0 136 134 0 157 158 0 140 138 0
		 165 166 0 144 142 0 173 174 0 185 186 0 186 187 0 185 188 0 186 189 1 188 189 0 190 189 1
		 8 191 0 191 190 1 191 188 0 187 192 0 189 192 0 9 193 0 193 192 0 190 193 1 58 194 0
		 194 195 1 21 196 1 196 195 1 20 197 0 197 196 0 197 194 0 59 198 0 195 198 1 22 199 0
		 199 198 0 196 199 0 195 190 1 194 191 0 198 193 0 58 19 1 59 23 1 23 4 1 200 11 1
		 201 12 1 202 13 1 200 201 1 201 202 1;
	setAttr -s 196 -ch 794 ".fc[0:195]" -type "polyFaces" 
		f 4 83 -83 -81 -80
		mu 0 4 0 3 2 1
		f 4 337 -165 -339 -345
		mu 0 4 4 7 6 5
		f 4 13 1 -30 -15
		mu 0 4 8 11 10 9
		f 4 -37 -36 -35 33
		mu 0 4 12 15 14 13
		f 4 -391 -24 -7 -392
		mu 0 4 16 19 18 17
		f 4 89 -89 -88 -86
		mu 0 4 20 23 22 21
		f 4 -108 -73 -70 -390
		mu 0 4 24 26 25 10
		f 4 -41 39 38 -38
		mu 0 4 27 30 29 28
		f 4 92 -92 -90 -91
		mu 0 4 31 32 23 20
		f 4 136 135 -134 -132
		mu 0 4 33 36 35 34
		f 4 -3 -4 4 -31
		mu 0 4 37 11 39 38
		f 4 -39 43 42 -42
		mu 0 4 28 29 41 40
		f 4 80 -95 -93 -94
		mu 0 4 1 2 32 31
		f 4 141 140 -137 -139
		mu 0 4 42 43 36 33
		f 4 -5 -6 6 -32
		mu 0 4 38 39 17 18
		f 4 -43 45 34 -45
		mu 0 4 40 41 13 14
		f 4 349 40 -349 48
		mu 0 4 44 47 46 45
		f 5 -350 -47 8 350 -40
		mu 0 5 30 44 49 48 29
		f 4 -369 367 365 -365
		mu 0 4 50 53 52 51
		f 4 -366 373 372 -371
		mu 0 4 51 52 55 54
		f 5 -34 -353 -10 51 -54
		mu 0 5 56 13 59 58 57
		f 4 -51 36 53 52
		mu 0 4 60 61 56 57
		f 4 -57 58 50 -58
		mu 0 4 62 64 61 63
		f 4 17 -393 -19 -26
		mu 0 4 65 68 67 66
		f 4 392 15 -394 -396
		mu 0 4 67 68 70 69
		f 4 393 16 -395 -397
		mu 0 4 69 70 72 71
		f 4 20 394 14 -29
		mu 0 4 73 71 72 74
		f 4 348 63 62 60
		mu 0 4 75 46 77 76
		f 4 -59 -169 -68 35
		mu 0 4 15 64 78 14
		f 4 95 25 -97 82
		mu 0 4 79 65 66 80
		f 4 96 26 -98 94
		mu 0 4 80 66 82 81
		f 4 97 27 -99 91
		mu 0 4 81 82 73 83
		f 4 98 28 21 88
		mu 0 4 83 73 74 84
		f 4 72 -107 -72 70
		mu 0 4 25 26 86 85
		f 4 -381 379 377 -376
		mu 0 4 87 90 89 88
		f 4 -378 385 384 -383
		mu 0 4 88 89 92 91
		f 4 103 76 -105 -110
		mu 0 4 93 96 95 94
		f 4 -1 54 56 -56
		mu 0 4 97 98 64 62
		f 4 55 57 -50 -11
		mu 0 4 97 62 63 99
		f 4 47 -61 -60 7
		mu 0 4 100 75 76 101
		f 4 -63 -62 347 59
		mu 0 4 76 77 102 101
		f 4 69 -71 -69 29
		mu 0 4 10 25 85 9
		f 4 68 71 -106 -22
		mu 0 4 9 85 86 103
		f 4 104 -74 -96 -111
		mu 0 4 94 95 105 104
		f 4 73 -77 -76 32
		mu 0 4 105 95 96 106
		f 6 78 79 -171 -173 -279 -12
		mu 0 6 97 0 1 109 108 107
		f 4 81 -84 -79 10
		mu 0 4 110 3 0 97
		f 4 84 87 -87 -8
		mu 0 4 111 21 22 112
		f 4 99 -48 86 105
		mu 0 4 86 75 113 103
		f 4 100 -49 -100 106
		mu 0 4 26 44 75 86
		f 4 46 -101 107 101
		mu 0 4 49 44 26 24
		f 4 -388 375 386 -368
		mu 0 4 53 87 88 52
		f 4 -387 382 388 -374
		mu 0 4 52 88 91 55
		f 4 108 -78 -52 -103
		mu 0 4 19 93 57 58
		f 4 77 109 -75 -53
		mu 0 4 57 93 94 63
		f 4 49 74 110 -82
		mu 0 4 114 63 94 104
		f 4 -114 -113 0 111
		mu 0 4 115 116 98 97
		f 4 114 116 -116 -348
		mu 0 4 117 119 118 111
		f 4 -120 -119 113 117
		mu 0 4 120 121 116 115
		f 4 -117 120 122 -122
		mu 0 4 118 119 123 122
		f 4 -126 -125 119 123
		mu 0 4 124 125 121 120
		f 4 -123 126 128 -128
		mu 0 4 122 123 127 126
		f 4 129 131 -131 -17
		mu 0 4 70 33 34 72
		f 4 130 133 -133 -14
		mu 0 4 72 34 35 128
		f 4 132 -136 -135 3
		mu 0 4 128 35 36 129
		f 4 137 138 -130 -16
		mu 0 4 68 42 33 70
		f 4 134 -141 -140 5
		mu 0 4 129 36 43 17
		f 4 139 -142 -138 12
		mu 0 4 17 43 42 68
		f 4 142 144 -144 -33
		mu 0 4 106 131 130 105
		f 5 143 146 340 -146 -18
		mu 0 5 105 130 134 133 132
		f 4 145 148 -148 -13
		mu 0 4 132 133 136 135
		f 4 147 -336 -342 334
		mu 0 4 135 136 138 137
		f 4 150 152 -152 -145
		mu 0 4 131 140 139 130
		f 4 339 -154 -341 -347
		mu 0 4 141 142 133 134
		f 4 153 156 -156 -149
		mu 0 4 133 142 143 136
		f 4 155 -337 -343 335
		mu 0 4 136 143 144 138
		f 4 158 160 -160 -153
		mu 0 4 140 146 145 139
		f 4 338 -162 -340 -346
		mu 0 4 5 6 142 141
		f 4 161 164 -164 -157
		mu 0 4 142 6 7 143
		f 4 163 -338 -344 336
		mu 0 4 143 7 4 144
		f 4 -66 -64 37 64
		mu 0 4 147 148 27 28
		f 4 -167 -65 41 66
		mu 0 4 149 147 28 40
		f 4 -168 -67 44 67
		mu 0 4 78 149 40 14
		f 7 170 93 -175 -177 -293 -170 171
		mu 0 7 109 1 31 153 152 151 150
		f 7 174 90 -179 -181 -307 -174 175
		mu 0 7 153 31 20 157 156 155 154
		f 6 178 85 -85 -321 -178 179
		mu 0 6 157 20 21 111 159 158
		f 4 354 -283 -354 -227
		mu 0 4 160 163 162 161
		f 4 356 -297 -356 -229
		mu 0 4 164 167 166 165
		f 4 358 -311 -358 -231
		mu 0 4 168 171 170 169
		f 4 -129 -325 -360 -233
		mu 0 4 126 127 173 172
		f 5 61 65 184 -328 -235
		mu 0 5 117 148 147 175 174
		f 5 183 166 188 -314 -236
		mu 0 5 176 147 149 178 177
		f 5 187 167 192 -300 -237
		mu 0 5 179 149 78 181 180
		f 5 191 168 -55 -286 -238
		mu 0 5 182 78 64 98 183
		f 4 -280 -287 278 195
		mu 0 4 184 185 107 108
		f 4 -240 190 237 -292
		mu 0 4 186 187 182 183
		f 4 -294 -301 292 201
		mu 0 4 188 189 151 152
		f 4 -242 186 236 -306
		mu 0 4 190 191 179 180
		f 4 -308 -315 306 207
		mu 0 4 192 193 155 156
		f 4 -244 182 235 -320
		mu 0 4 194 195 176 177
		f 4 115 -322 -329 320
		mu 0 4 111 118 196 159
		f 4 -246 -115 234 -334
		mu 0 4 197 119 117 174
		f 4 197 -281 -288 279
		mu 0 4 184 199 198 185
		f 4 -248 194 239 -291
		mu 0 4 200 201 187 186
		f 4 203 -295 -302 293
		mu 0 4 188 203 202 189
		f 4 -250 200 241 -305
		mu 0 4 204 205 191 190
		f 4 209 -309 -316 307
		mu 0 4 192 207 206 193
		f 4 -252 206 243 -319
		mu 0 4 208 209 195 194
		f 4 121 -323 -330 321
		mu 0 4 118 122 210 196
		f 4 -254 -121 245 -333
		mu 0 4 211 123 119 197
		f 4 213 226 -289 280
		mu 0 4 199 160 161 198
		f 4 282 212 247 -290
		mu 0 4 162 163 201 200
		f 4 217 228 -303 294
		mu 0 4 203 164 165 202
		f 4 296 216 249 -304
		mu 0 4 166 167 205 204
		f 4 221 230 -317 308
		mu 0 4 207 168 169 206
		f 4 310 220 251 -318
		mu 0 4 170 171 209 208
		f 4 127 232 -331 322
		mu 0 4 122 126 172 210
		f 4 324 -127 253 -332
		mu 0 4 173 127 123 211
		f 3 254 -193 -192
		mu 0 3 182 181 78
		f 3 255 -189 -188
		mu 0 3 179 178 149
		f 3 256 -185 -184
		mu 0 3 176 175 147
		f 4 -258 -191 258 -196
		mu 0 4 108 182 187 184
		f 4 -259 -195 259 -198
		mu 0 4 184 187 201 199
		f 4 -260 -213 -355 -214
		mu 0 4 199 201 163 160
		f 4 -224 -212 260 -215
		mu 0 4 212 215 214 213
		f 4 -262 -217 -357 -218
		mu 0 4 203 205 167 164
		f 4 -225 -216 262 -219
		mu 0 4 216 219 218 217
		f 4 -264 -221 -359 -222
		mu 0 4 207 209 171 168
		f 4 -226 -220 264 -223
		mu 0 4 220 223 222 221
		f 4 -265 -206 265 -211
		mu 0 4 221 222 225 224
		f 4 -267 -207 263 -210
		mu 0 4 192 195 209 207
		f 4 -263 -200 267 -205
		mu 0 4 217 218 227 226
		f 4 -269 -201 261 -204
		mu 0 4 188 191 205 203
		f 4 -261 -194 269 -199
		mu 0 4 213 214 229 228
		f 4 -270 -190 270 -197
		mu 0 4 228 229 181 150
		f 4 -272 -187 268 -202
		mu 0 4 152 179 191 188
		f 4 -268 -186 272 -203
		mu 0 4 226 227 178 154
		f 4 -274 -183 266 -208
		mu 0 4 156 176 195 192
		f 4 -266 -182 274 -209
		mu 0 4 224 225 175 158
		f 4 -275 -257 273 275
		mu 0 4 158 175 176 156
		f 4 -273 -256 271 276
		mu 0 4 154 178 179 152
		f 4 -271 -255 257 277
		mu 0 4 150 181 182 108
		f 3 -172 -278 172
		mu 0 3 109 150 108
		f 3 -176 -277 176
		mu 0 3 153 154 152
		f 3 -180 -276 180
		mu 0 3 157 158 156
		f 4 -239 -112 11 286
		mu 0 4 185 115 97 107
		f 4 -247 -118 238 287
		mu 0 4 198 120 115 185
		f 4 281 -124 246 288
		mu 0 4 161 124 120 198
		f 4 353 -228 125 -282
		mu 0 4 161 162 125 124
		f 4 124 227 289 283
		mu 0 4 121 125 162 200
		f 4 118 -284 290 284
		mu 0 4 116 121 200 186
		f 4 112 -285 291 285
		mu 0 4 98 116 186 183
		f 4 -241 196 169 300
		mu 0 4 189 228 150 151
		f 4 -249 198 240 301
		mu 0 4 202 213 228 189
		f 4 295 214 248 302
		mu 0 4 165 212 213 202
		f 4 355 -230 223 -296
		mu 0 4 165 166 215 212
		f 4 211 229 303 297
		mu 0 4 214 215 166 204
		f 4 193 -298 304 298
		mu 0 4 229 214 204 190
		f 4 189 -299 305 299
		mu 0 4 181 229 190 180
		f 4 -243 202 173 314
		mu 0 4 193 226 154 155
		f 4 -251 204 242 315
		mu 0 4 206 217 226 193
		f 4 309 218 250 316
		mu 0 4 169 216 217 206
		f 4 357 -232 224 -310
		mu 0 4 169 170 219 216
		f 4 215 231 317 311
		mu 0 4 218 219 170 208
		f 4 199 -312 318 312
		mu 0 4 227 218 208 194
		f 4 185 -313 319 313
		mu 0 4 178 227 194 177
		f 4 -245 208 177 328
		mu 0 4 196 224 158 159
		f 4 -253 210 244 329
		mu 0 4 210 221 224 196
		f 4 323 222 252 330
		mu 0 4 172 220 221 210
		f 4 359 -234 225 -324
		mu 0 4 172 173 223 220
		f 4 219 233 331 325
		mu 0 4 222 223 173 211
		f 4 205 -326 332 326
		mu 0 4 225 222 211 197
		f 4 181 -327 333 327
		mu 0 4 175 225 197 174
		f 4 -150 -143 24 341
		mu 0 4 138 131 106 137
		f 4 -158 -151 149 342
		mu 0 4 144 140 131 138
		f 4 -166 -159 157 343
		mu 0 4 4 146 140 144
		f 4 165 344 -163 -161
		mu 0 4 146 4 5 145
		f 4 159 162 345 -155
		mu 0 4 139 145 5 141
		f 4 151 154 346 -147
		mu 0 4 130 139 141 134
		f 4 -44 -351 360 351
		mu 0 4 41 29 48 230
		f 4 -46 -352 361 352
		mu 0 4 13 41 230 59
		f 4 362 364 -364 -361
		mu 0 4 48 50 51 230
		f 4 366 368 -363 -9
		mu 0 4 49 53 50 48
		f 4 363 370 -370 -362
		mu 0 4 230 51 54 59
		f 4 369 -373 -372 9
		mu 0 4 59 54 55 58
		f 4 376 -380 -379 30
		mu 0 4 38 89 90 37
		f 4 378 380 -375 -23
		mu 0 4 37 90 87 24
		f 4 381 -385 -384 23
		mu 0 4 19 91 92 18
		f 4 383 -386 -377 31
		mu 0 4 18 92 89 38
		f 4 374 387 -367 -102
		mu 0 4 24 87 53 49
		f 4 371 -389 -382 102
		mu 0 4 58 55 91 19
		f 4 22 389 -2 2
		mu 0 4 37 24 10 11
		f 4 75 -104 -109 390
		mu 0 4 16 96 93 19
		f 3 391 -335 -25
		mu 0 3 16 17 231
		f 4 18 395 -20 -27
		mu 0 4 66 67 69 82
		f 4 19 396 -21 -28
		mu 0 4 82 69 71 73;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmLeft" -p "|BasicMechGrouped1|Basic_Left_Arm";
	rename -uid "E322D503-43BD-E5A5-5808-C69698BE0BA3";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 15.000007629394531 -1.25 ;
	setAttr ".sp" -type "double3" -18 15.000007629394531 -1.25 ;
createNode mesh -n "lowerArmLeftShape" -p "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft";
	rename -uid "69AF948D-4E7A-9B23-DE26-908509F9B0F0";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "lowerArmLeftShapeOrig" -p "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft";
	rename -uid "C8C609DA-427F-6436-EEF7-2FA0E8D74D77";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 99 ".uvst[0].uvsp[0:98]" -type "float2" 0.375 0.5467304 0.4217304
		 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75
		 0.375 0.7032696 0.6437481 0.24375807 0.54375809 0.30625185 0.38124189 0.26874813
		 0.31874812 0.41873318 0.35625187 0.75626677 0.45624191 0.69377303 0.61875814 0.231227
		 0.68125188 0.081241898 0.375 0.7032696 0.4217304 0.75 0.4217304 0.92500001 0.375
		 0.92500001 0.36098087 0.79882789 0.375 0.92500001 0.45624194 0.69377297 0.35625184
		 0.75626677 0.57826966 0.75 0.625 0.7032696 0.625 0.92500001 0.5782696 0.92500001
		 0.63925052 0.17290242 0.70000005 0 0.68125188 0.081241921 0.61875814 0.231227 0.4217304
		 0.5 0.375 0.5467304 0.375 0.32499999 0.4217304 0.32499999 0.36074951 0.26401913 0.29999998
		 0.25 0.31874812 0.41873324 0.38124189 0.26874813 0.625 0.5467304 0.5782696 0.5 0.57826966
		 0.32499999 0.625 0.32499999 0.56425053 0.31098089 0.54375809 0.30625185 0.64374816
		 0.24375805 0.70000005 0.2032696 0.70000005 0.046730354 0.875 0.046730399 0.875 0.20326962
		 0.125 0.046730399 0.29999998 0.046730399 0.29999998 0.20326963 0.125 0.20326963 0.31401908
		 0.37617201 0.43574953 0.75209755 0.68598092 0.060749516 0.63901913 0.26425049 0.4217304
		 0.5 0.375 0.5467304 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75
		 0.4217304 0.75 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375
		 0.5467304 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696
		 0.625 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375
		 0.7032696 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375 0.5467304
		 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696 0.625
		 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375 0.7032696
		 0.375 0.7032696;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 80 ".pt[0:79]" -type "float3"  8 0 0 5.0092545 0 0 8 0 0 
		5.0092545 0 0 -5.0092564 0 0 -8 0 0 -5.0092564 0 0 -8 0 0 5.0092545 0 0 8 0 0 5.0092545 
		0 0 8 0 0 -8 0 0 -5.0092564 0 0 -8 0 0 -5.0092564 0 0 2.9913406 0 0 5.9820862 0 0 
		-5.9820862 0 0 -2.9913406 0 0 5.9820862 0 0 2.9913406 0 0 -2.9913406 0 0 -5.9820862 
		0 0 5.9820862 0 0 2.9913406 0 0 -5.9820862 0 0 -2.9913406 0 0 2.9913406 0 0 5.9820862 
		0 0 -5.9820862 0 0 -2.9913406 0 0 4.1208344 0 0 6.5811615 0 0 -4.1208363 0 0 -6.5811577 
		0 0 -6.5811577 0 0 -4.1208363 0 0 4.1208344 0 0 6.5811615 0 0 2.3869934 0 0 3.8121414 
		0 0 -2.3869934 0 0 -3.8121376 0 0 -3.8121376 0 0 -2.3869934 0 0 2.3869934 0 0 3.8121414 
		0 0 4.1208344 0 0 6.5811615 0 0 4.1208344 0 0 6.5811615 0 0 -4.1208363 0 0 -4.1208363 
		0 0 -6.5811577 0 0 -6.5811577 0 0 -6.5811577 0 0 -6.5811577 0 0 -4.1208363 0 0 -4.1208363 
		0 0 4.1208344 0 0 4.1208344 0 0 6.5811615 0 0 6.5811615 0 0 3.4661789 0 0 5.000679 
		0 0 3.4661789 0 0 5.000679 0 0 -3.4661789 0 0 -3.4661789 0 0 -5.0006752 0 0 -5.0006752 
		0 0 -5.0006733 0 0 -5.0006733 0 0 -3.4661789 0 0 -3.4661789 0 0 3.4661751 0 0 3.4661751 
		0 0 5.0006752 0 0 5.0006752 0 0;
	setAttr -s 80 ".vt[0:79]"  -22 -20.50462723 -7 -20.50462723 -22 -7 -22 -20.50462723 6
		 -20.50462723 -22 6 -15.49537182 -22 -7 -14 -20.50462723 -7 -15.49537182 -22 6 -14 -20.50462723 6
		 -20.50462723 -14 -7 -22 -15.49537182 -7 -20.50462723 -14 6 -22 -15.49537182 6 -14 -15.49537182 -7
		 -15.49537182 -14 -7 -14 -15.49537182 6 -15.49537182 -14 6 -19.49567032 -15.0089569092 6
		 -20.99104309 -16.50432968 6 -15.0089569092 -16.50432968 6 -16.50432968 -15.0089569092 6
		 -20.99104309 -19.49567032 6 -19.49567032 -20.99104309 6 -16.50432968 -20.99104309 6
		 -15.0089569092 -19.49567032 6 -20.99104309 -16.50432968 8 -19.49567032 -15.0089569092 8
		 -15.0089569092 -16.50432968 8 -16.50432968 -15.0089569092 8 -19.49567032 -20.99104309 8
		 -20.99104309 -19.49567032 8 -15.0089569092 -19.49567032 8 -16.50432968 -20.99104309 8
		 -20.060417175 -14.70942116 -7 -21.29058075 -15.93958187 -7 -15.93958187 -14.70942116 -7
		 -14.70942116 -15.93958187 -7 -14.70942116 -20.060417175 -7 -15.93958187 -21.29058075 -7
		 -20.060417175 -21.29058075 -7 -21.29058075 -20.060417175 -7 -19.1934967 -16.093933105 -10.5
		 -19.90607071 -16.8065033 -10.5 -16.8065033 -16.093933105 -10.5 -16.093931198 -16.8065033 -10.5
		 -16.093931198 -19.1934967 -10.5 -16.8065033 -19.90607071 -10.5 -19.1934967 -19.90607071 -10.5
		 -19.90607071 -19.1934967 -10.5 -20.060417175 -14.70942116 -7.49999905 -21.29058075 -15.93958187 -7.49999905
		 -20.060417175 -14.70942116 -8.96627808 -21.29058075 -15.93958187 -8.96627808 -15.93958187 -14.70942116 -7.49999905
		 -15.93958187 -14.70942116 -8.96627808 -14.70942116 -15.93958187 -7.49999905 -14.70942116 -15.93958187 -8.96627808
		 -14.70942116 -20.060417175 -7.49999905 -14.70942116 -20.060417175 -8.96627808 -15.93958187 -21.29058075 -7.49999905
		 -15.93958187 -21.29058075 -8.96627808 -20.060417175 -21.29058075 -7.49999905 -20.060417175 -21.29058075 -8.96627808
		 -21.29058075 -20.060417175 -7.49999905 -21.29058075 -20.060417175 -8.96627808 -19.73308945 -15.49966335 -7
		 -20.50033951 -16.26691055 -7 -19.73308945 -15.49966335 -7.49999905 -20.50033951 -16.26691055 -7.49999905
		 -16.26691055 -15.4996624 -7 -16.26691055 -15.4996624 -7.49999905 -15.4996624 -16.26691055 -7
		 -15.4996624 -16.26691055 -7.49999905 -15.49966335 -19.73308945 -7 -15.49966335 -19.73308945 -7.49999905
		 -16.26691055 -20.50033951 -7 -16.26691055 -20.50033951 -7.49999905 -19.73308754 -20.5003376 -7
		 -19.73308754 -20.5003376 -7.49999905 -20.5003376 -19.73308754 -7 -20.5003376 -19.73308754 -7.49999905;
	setAttr -s 152 ".ed[0:151]"  1 4 0 1 0 0 5 4 0 8 13 0 9 0 0 8 9 0 12 5 0
		 13 12 0 25 27 0 25 24 0 26 30 0 27 26 0 29 24 0 29 28 0 31 28 0 31 30 0 1 3 0 3 2 0
		 2 0 0 3 21 1 21 20 0 20 2 1 5 7 0 7 6 0 6 4 0 7 23 1 23 22 0 22 6 1 9 11 0 11 10 0
		 10 8 0 11 17 1 17 16 0 16 10 1 13 15 0 15 14 0 14 12 0 15 19 1 19 18 0 18 14 1 17 24 0
		 25 16 0 19 27 0 26 18 0 21 28 0 29 20 0 23 30 0 31 22 0 10 15 0 6 3 0 14 7 0 2 11 0
		 16 19 0 20 17 0 22 21 0 18 23 0 8 32 1 9 33 1 32 33 1 13 34 1 32 34 1 12 35 1 34 35 1
		 5 36 1 35 36 1 4 37 1 36 37 1 1 38 1 38 37 1 0 39 1 38 39 1 33 39 1 40 41 0 40 42 0
		 42 43 0 43 44 0 44 45 0 46 45 0 46 47 0 41 47 0 48 49 0 48 50 0 50 51 0 49 51 0 48 52 0
		 52 53 0 50 53 0 52 54 0 54 55 0 53 55 0 54 56 0 56 57 0 55 57 0 56 58 0 58 59 0 57 59 0
		 60 58 0 60 61 0 61 59 0 60 62 0 62 63 0 61 63 0 49 62 0 51 63 0 50 40 0 51 41 0 53 42 0
		 55 43 0 57 44 0 59 45 0 61 46 0 63 47 0 32 64 1 33 65 1 64 65 0 48 66 1 64 66 0 49 67 1
		 66 67 0 65 67 0 34 68 1 64 68 0 52 69 1 68 69 0 66 69 0 35 70 1 68 70 0 54 71 1 70 71 0
		 69 71 0 36 72 1 70 72 0 56 73 1 72 73 0 71 73 0 37 74 1 72 74 0 58 75 1 74 75 0 73 75 0
		 38 76 1 76 74 0 60 77 1 76 77 0 77 75 0 39 78 1 76 78 0 62 79 1 78 79 0 77 79 0 65 78 0
		 67 79 0;
	setAttr -s 74 -ch 304 ".fc[0:73]" -type "polyFaces" 
		f 8 79 -79 77 -77 -76 -75 -74 72
		mu 0 8 0 7 6 5 4 3 2 1
		f 8 10 -16 14 -14 12 -10 8 11
		mu 0 8 8 15 14 13 12 11 10 9
		f 4 -19 -18 -17 1
		mu 0 4 16 19 18 17
		f 4 -22 -21 -20 17
		mu 0 4 20 23 22 21
		f 4 -25 -24 -23 2
		mu 0 4 24 27 26 25
		f 4 -28 -27 -26 23
		mu 0 4 28 31 30 29
		f 4 -31 -30 -29 -6
		mu 0 4 32 35 34 33
		f 4 -34 -33 -32 29
		mu 0 4 36 39 38 37
		f 4 -37 -36 -35 7
		mu 0 4 40 43 42 41
		f 4 -40 -39 -38 35
		mu 0 4 43 46 45 44
		f 4 -42 9 -41 32
		mu 0 4 39 10 11 38
		f 4 -44 -12 -43 38
		mu 0 4 46 8 9 45
		f 4 -46 13 -45 20
		mu 0 4 23 12 13 22
		f 4 -48 15 -47 26
		mu 0 4 31 14 15 30
		f 4 30 3 34 -49
		mu 0 4 35 32 41 42
		f 4 -50 24 -1 16
		mu 0 4 18 27 24 17
		f 4 36 6 22 -51
		mu 0 4 47 50 49 48
		f 4 -5 28 -52 18
		mu 0 4 51 54 53 52
		f 4 37 -53 33 48
		mu 0 4 44 45 39 36
		f 4 31 -54 21 51
		mu 0 4 55 38 23 20
		f 4 19 -55 27 49
		mu 0 4 56 22 31 28
		f 4 25 -56 39 50
		mu 0 4 57 30 46 58
		f 4 42 -9 41 52
		mu 0 4 45 9 10 39
		f 4 40 -13 45 53
		mu 0 4 38 11 12 23
		f 4 44 -15 47 54
		mu 0 4 22 13 14 31
		f 4 46 -11 43 55
		mu 0 4 30 15 8 46
		f 4 57 -59 -57 5
		mu 0 4 33 60 59 32
		f 4 56 60 -60 -4
		mu 0 4 32 59 61 41
		f 4 59 62 -62 -8
		mu 0 4 41 61 62 40
		f 4 61 64 -64 -7
		mu 0 4 40 62 63 25
		f 4 63 66 -66 -3
		mu 0 4 25 63 64 24
		f 4 65 -69 -68 0
		mu 0 4 24 64 65 17
		f 4 67 70 -70 -2
		mu 0 4 17 65 66 16
		f 4 69 -72 -58 4
		mu 0 4 16 66 60 33
		f 4 83 -83 -82 80
		mu 0 4 67 70 69 68
		f 4 81 86 -86 -85
		mu 0 4 68 69 72 71
		f 4 85 89 -89 -88
		mu 0 4 71 72 74 73
		f 4 88 92 -92 -91
		mu 0 4 73 74 76 75
		f 4 91 95 -95 -94
		mu 0 4 75 76 78 77
		f 4 94 -99 -98 96
		mu 0 4 77 78 80 79
		f 4 97 101 -101 -100
		mu 0 4 79 80 82 81
		f 4 100 -104 -84 102
		mu 0 4 81 82 70 67
		f 4 105 -73 -105 82
		mu 0 4 70 0 1 69
		f 4 104 73 -107 -87
		mu 0 4 69 1 2 72
		f 4 106 74 -108 -90
		mu 0 4 72 2 3 74
		f 4 107 75 -109 -93
		mu 0 4 74 3 4 76
		f 4 108 76 -110 -96
		mu 0 4 76 4 5 78
		f 4 109 -78 -111 98
		mu 0 4 78 5 6 80
		f 4 110 78 -112 -102
		mu 0 4 80 6 7 82
		f 4 111 -80 -106 103
		mu 0 4 82 7 0 70
		f 4 119 -119 -117 114
		mu 0 4 83 86 85 84
		f 4 116 124 -124 -122
		mu 0 4 84 85 88 87
		f 4 123 129 -129 -127
		mu 0 4 87 88 90 89
		f 4 128 134 -134 -132
		mu 0 4 89 90 92 91
		f 4 133 139 -139 -137
		mu 0 4 91 92 94 93
		f 4 138 -145 -144 141
		mu 0 4 93 94 96 95
		f 4 143 149 -149 -147
		mu 0 4 95 96 98 97
		f 4 148 -152 -120 150
		mu 0 4 97 98 86 83
		f 4 113 -115 -113 58
		mu 0 4 60 83 84 59
		f 4 115 118 -118 -81
		mu 0 4 68 85 86 67
		f 4 112 121 -121 -61
		mu 0 4 59 84 87 61
		f 4 122 -125 -116 84
		mu 0 4 71 88 85 68
		f 4 120 126 -126 -63
		mu 0 4 61 87 89 62
		f 4 127 -130 -123 87
		mu 0 4 73 90 88 71
		f 4 125 131 -131 -65
		mu 0 4 62 89 91 63
		f 4 132 -135 -128 90
		mu 0 4 75 92 90 73
		f 4 130 136 -136 -67
		mu 0 4 63 91 93 64
		f 4 137 -140 -133 93
		mu 0 4 77 94 92 75
		f 4 135 -142 -141 68
		mu 0 4 64 93 95 65
		f 4 142 144 -138 -97
		mu 0 4 79 96 94 77
		f 4 140 146 -146 -71
		mu 0 4 65 95 97 66
		f 4 147 -150 -143 99
		mu 0 4 81 98 96 79
		f 4 145 -151 -114 71
		mu 0 4 66 97 83 60
		f 4 117 151 -148 -103
		mu 0 4 67 86 98 81;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane1";
	rename -uid "DDE00D9F-407E-915D-2128-CA839588B754";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 -33 0 ;
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	rename -uid "1CD8CF06-4B3E-EA7E-F44A-9DBAE20B8F31";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Drill_Right_Arm";
	rename -uid "98E805E7-469D-5CB6-B0FE-E1B7EBDF6D17";
	setAttr ".t" -type "double3" -3.5527136788005009e-015 33 0 ;
	setAttr ".rp" -type "double3" -55 -8 3.5457096099853516 ;
	setAttr ".sp" -type "double3" -55 -8 3.5457096099853516 ;
createNode transform -n "upperArmRight" -p "Drill_Right_Arm";
	rename -uid "DF93389E-4AC6-8296-B8EE-2490EA9B6F35";
	setAttr ".rp" -type "double3" -55 -11 0 ;
	setAttr ".sp" -type "double3" -55 -11 0 ;
createNode mesh -n "upperArmRightShape" -p "|Drill_Right_Arm|upperArmRight";
	rename -uid "25F251A1-4585-096F-3393-0ABF4EDC755B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0.31249374
		 0.625 0.31249374 0.375 0.43750626 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.43749374 0.25 0.5625062 3.7252903e-009 0.18749376
		 0.25 0.18749379 3.7252903e-009 0.3125062 3.7252903e-009 0.43749374 0.75 0.5625062
		 0.5 0.68749374 0.25 0.8125062 3.7252903e-009 0.81250632 0.25 0.43749377 3.7252903e-009
		 0.31250626 0.25 0.6874938 3.7252903e-009 0.56250626 0.25 0.43749377 0.5 0.375 0.81249374
		 0.625 0.43750626 0.56250626 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  -58 -17 1.50014997 -56.50015259 -17 3 -58 -5 1.50014997
		 -56.50015259 -5 3 -52 -17 1.50014997 -53.49984741 -17 3 -52 -5 1.50014997 -53.49984741 -5 3
		 -58 -5 -1.50014997 -56.50015259 -5 -3 -58 -17 -1.50014997 -56.50015259 -17 -3 -53.49984741 -5 -3
		 -52 -5 -1.50014997 -52 -17 -1.50014997 -53.49984741 -17 -3;
	setAttr -s 24 ".ed[0:23]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0
		 7 6 0 9 12 0 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0
		 6 4 0 8 10 0 11 9 0 12 15 0 14 13 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 4 17 0 18 -4
		mu 0 4 8 18 9 21
		f 4 21 8 22 -12
		mu 0 4 13 22 14 25
		f 4 19 -15 23 -7
		mu 0 4 15 20 16 17
		f 4 20 10 16 2
		mu 0 4 10 11 12 19
		f 8 -5 3 7 6 13 -9 9 -3
		mu 0 8 0 8 21 1 24 14 22 2
		f 8 -13 11 15 14 5 -1 1 -11
		mu 0 8 23 13 25 3 4 5 6 7
		f 4 -2 -18 4 -17
		mu 0 4 12 18 8 19
		f 4 -6 -20 -8 -19
		mu 0 4 9 20 15 21
		f 4 -10 -22 12 -21
		mu 0 4 2 22 13 23
		f 4 -14 -24 -16 -23
		mu 0 4 14 24 3 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape4" -p "|Drill_Right_Arm|upperArmRight";
	rename -uid "F4788BF8-49AD-765B-D00A-E1A636C45901";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "shoulderRight" -p "Drill_Right_Arm";
	rename -uid "84BD19F9-4B22-F6C8-3D95-BC8321E820BB";
	setAttr ".rp" -type "double3" -55 1 0 ;
	setAttr ".sp" -type "double3" -55 1 0 ;
createNode mesh -n "shoulderRightShape" -p "|Drill_Right_Arm|shoulderRight";
	rename -uid "05B97081-415E-4A97-2734-ECA3BD8C7F29";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:60]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.57292187213897705 0.375 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0.375 0.30207813
		 0.375 0.44792187 0.32292187 0.1805625 0.32292187 0 0.42707813 0.1805625 0.625 0.30207813
		 0.57292187 0.1805625 0.57292187 0 0.375 0.5694375 0.42707813 0.75 0.375 0.75 0.42707813
		 0.44792187 0.57292187 0.44792187 0.625 0.5694375 0.625 0.75 0.42707813 7.4505806e-009
		 0.42707813 0.30207813 0.57292187 0.30207813 0.42707813 0.5694375 0.57292187 0.5694375
		 0.57292187 0.75 0.67707813 7.4505806e-009 0.82292187 0 0.82292187 0.1805625 0.67707813
		 0.1805625 0.17707813 7.4505806e-009 0.17707813 0.1805625 0.625 0.44792187 0.40103906
		 0.30207813 0.375 0.24132031 0.42707813 0.24132031 0.57292187 0.24132031 0.59896094
		 0.30207813 0.625 0.24132031 0.59896094 0.44792187 0.57292187 0.50867969 0.42707813
		 0.50867969 0.40103906 0.44792187 0.375 3.7252903e-009 0.375 0.1805625 0.40103906
		 0.24132031 0.625 3.7252903e-009 0.625 0.1805625 0.59896094 0.24132031 0.40103906
		 0.75 0.40103906 0.5694375 0.40103906 0.50867969 0.4140586 0.47830078 0.59896094 0.75
		 0.59896094 0.5694375 0.59896094 0.50867969 0.58594143 0.47830078;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 37 ".uvst[1].uvsp[0:36]" -type "float2" 0.625 0.81249374
		 0.625 0.93750626 0.56250626 1 0.43749374 1 0.375 0.93750626 0.43749374 3.7252903e-009
		 0.43749374 0.25 0.375 0.25 0.68749374 -3.7252903e-009 0.625 0.25 0.375 0.5 0.375
		 0.81249374 0.56250626 0.5 0.625 0.5 0.56250626 0.75 0.56250626 -3.7252903e-009 0.56250626
		 0.25 0.43749374 0.5 0.43749374 0.75 0.68749374 0.25 0.81250626 -3.7252903e-009 0.81250632
		 0.25 0.18749374 0.25 0.18749374 -3.7252903e-009 0.31250626 -3.7252903e-009 0.31250626
		 0.25 0.40624687 0.25 0.5937531 0.25 0.40624687 0.5 0.5937531 0.5 0.5937531 0.7812469
		 0.40624687 0.9687531 0.375 0 0.40624687 0.7812469 0.5 0.875 0.5937531 0.9687531 0.625
		 -3.7252903e-009;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 65 ".vt[0:64]"  -61 4.50024986 3.5002501 -58.50025177 4.50024986 6
		 -58.50025177 7 3.5002501 -51.49974823 7 3.5002501 -51.49974823 4.50024986 6 -49 4.50024986 3.5002501
		 -61 4.50024986 -3.5002501 -58.50025177 7 -3.5002501 -58.50025177 4.50024986 -6 -51.49974823 4.50024986 -6
		 -51.49974823 7 -3.5002501 -49 4.50024986 -3.5002501 -58.50025177 -2 6 -61 -2 3.5002501
		 -49 -2 3.5002501 -51.49974823 -2 6 -61 -2 -3.5002501 -58.50025177 -2 -6 -51.49974823 -2 -6
		 -49 -2 -3.5002501 -60 -5 2.5002501 -57.50025177 -5 5 -60 -2 2.5002501 -57.50025177 -2 5
		 -50 -5 2.5002501 -52.49974823 -5 5 -52.49974823 -2 5 -50 -2 2.5002501 -57.50025177 -2 -5
		 -60 -2 -2.5002501 -60 -5 -2.5002501 -57.50025177 -5 -5 -50 -2 -2.5002501 -52.49974823 -2 -5
		 -50 -5 -2.5002501 -52.49974823 -5 -5 -60.039665222 6.07776022 3.5002501 -58.50025177 6.07776022 5.039667606
		 -51.49974823 6.07776022 5.039667606 -49.96033478 6.07776022 3.5002501 -49.96033478 6.07776022 -3.5002501
		 -51.49974823 6.07776022 -5.03966713 -58.50025177 6.07776022 -5.039667606 -60.039665222 6.077759743 -3.5002501
		 -58.97871399 -2 3.9787128 -60.039665222 -2 5.039667606 -60.039665222 4.50024986 5.039667606
		 -59.58447266 5.66208887 4.58447266 -51.021286011 -2 3.9787128 -49.96033478 -2 5.039667606
		 -49.96033478 4.50024986 5.039667606 -50.41552734 5.66208887 4.58447266 -58.97871399 -2 -3.9787128
		 -60.039665222 -2 -5.039667606 -60.039665222 4.50024986 -5.039667606 -59.58447266 5.66208887 -4.58447266
		 -51.021286011 -2 -3.9787128 -49.96033478 -2 -5.039667606 -49.96033478 4.50024986 -5.039667606
		 -50.41552734 5.66208887 -4.58447266 -51.021286011 -5 -3.9787128 -58.97871399 -5 3.97871256
		 -58.97871399 -5 -3.97871256 -55 -5 0 -51.021286011 -5 3.9787128;
	setAttr -s 124 ".ed[0:123]"  21 25 0 21 61 0 24 64 0 30 20 0 31 35 0 31 62 0
		 34 24 0 35 60 0 0 36 0 2 7 0 7 43 0 6 0 0 1 46 0 0 13 0 13 45 0 12 1 0 2 37 0 1 4 0
		 4 38 0 3 2 0 3 39 0 5 11 0 11 40 0 10 3 0 5 50 0 4 15 0 15 49 0 14 5 0 6 54 0 8 17 0
		 17 53 0 16 6 0 8 42 0 7 10 0 10 41 0 9 8 0 9 58 0 11 19 0 19 57 0 18 9 0 13 22 1
		 22 44 0 23 12 1 15 26 1 26 48 0 27 14 1 17 28 1 28 52 0 29 16 1 19 32 1 32 56 0 33 18 1
		 21 23 0 22 20 0 24 27 0 26 25 0 28 31 0 30 29 0 32 34 0 35 33 0 12 15 0 18 17 0 14 19 0
		 16 13 0 26 23 0 28 33 0 32 27 0 22 29 0 36 2 0 37 1 0 38 3 0 39 5 0 40 10 0 41 9 0
		 42 7 0 43 6 0 36 47 1 37 38 1 38 51 1 39 40 1 40 59 1 41 42 1 42 55 1 43 36 1 44 23 0
		 45 12 0 46 0 0 47 37 1 44 45 1 45 46 1 46 47 1 48 27 0 49 14 0 50 4 0 51 39 1 48 49 1
		 49 50 1 50 51 1 52 29 0 53 16 0 54 8 0 55 43 1 52 53 1 53 54 1 54 55 1 56 33 0 57 18 0
		 58 11 0 59 41 1 56 57 1 57 58 1 58 59 1 60 34 0 61 20 0 60 63 1 62 30 0 63 61 1 64 25 0
		 62 63 1 63 64 1 64 60 1 60 62 1 62 61 1 61 64 1;
	setAttr -s 61 -ch 248 ".fc[0:60]" -type "polyFaces" 
		f 4 -116 122 113 -4
		mu 1 4 11 33 31 4
		f 4 83 68 9 10
		mu 0 4 37 28 16 11
		f 4 89 86 13 14
		mu 0 4 38 39 2 3
		f 4 16 77 70 19
		mu 0 4 16 30 31 17
		f 4 20 79 72 23
		mu 0 4 17 32 34 12
		f 4 96 93 25 26
		mu 0 4 41 42 6 7
		f 4 103 100 29 30
		mu 0 4 44 45 18 9
		f 4 81 74 33 34
		mu 0 4 35 36 11 12
		f 4 110 107 37 38
		mu 0 4 48 49 13 14
		f 4 88 -15 40 41
		f 4 95 -27 43 44
		f 4 102 -31 46 47
		f 4 109 -39 49 50
		f 6 -114 -2 52 -85 -42 53
		mu 1 6 24 32 5 6 26 7
		f 6 -118 -3 54 -92 -45 55
		mu 1 6 15 36 8 9 27 16
		f 6 -99 -48 56 5 115 57
		mu 1 6 10 28 17 18 33 11
		f 6 -106 -51 58 -113 -8 59
		mu 1 6 12 29 13 0 30 14
		f 4 60 -26 -18 -16
		mu 0 4 15 7 6 4
		f 4 -20 -24 -34 -10
		mu 0 4 16 17 12 11
		f 4 -36 -40 61 -30
		mu 0 4 18 19 20 9
		f 4 62 -38 -22 -28
		mu 0 4 21 22 23 24
		f 4 63 -14 -12 -32
		mu 0 4 25 3 2 26
		f 4 -53 0 -56 64
		mu 1 4 6 5 15 16
		f 4 65 -60 -5 -57
		mu 1 4 17 12 14 18
		f 4 -55 -7 -59 66
		mu 1 4 19 8 20 21
		f 4 -58 3 -54 67
		mu 1 4 22 23 24 25
		f 4 -61 -43 -65 -44
		f 4 -64 -49 -68 -41
		f 4 -62 -52 -66 -47
		f 4 -63 -46 -67 -50
		f 4 76 87 -17 -69
		mu 0 4 29 40 30 16
		f 4 -71 78 94 -21
		mu 0 4 17 31 43 33
		f 4 82 101 -11 -75
		mu 0 4 36 47 37 11
		f 4 80 108 -35 -73
		mu 0 4 34 51 35 12
		f 4 -87 90 -77 -9
		mu 0 4 2 39 40 29
		f 4 -78 69 17 18
		mu 0 4 31 30 4 6
		f 4 97 -79 -19 -94
		mu 0 4 42 43 31 6
		f 4 -80 71 21 22
		mu 0 4 34 32 5 27
		f 4 111 -81 -23 -108
		mu 0 4 50 51 34 27
		f 4 32 -82 73 35
		mu 0 4 18 36 35 19
		f 4 104 -83 -33 -101
		mu 0 4 46 47 36 18
		f 4 8 -84 75 11
		mu 0 4 0 28 37 1
		f 4 -86 -89 84 42
		f 4 12 -90 85 15
		mu 0 4 4 39 38 15
		f 4 -91 -13 -70 -88
		mu 0 4 40 39 4 30
		f 4 -93 -96 91 45
		f 4 24 -97 92 27
		mu 0 4 24 42 41 21
		f 4 -95 -98 -25 -72
		mu 0 4 33 43 42 24
		f 4 -100 -103 98 48
		f 4 28 -104 99 31
		mu 0 4 8 45 44 10
		f 4 -76 -102 -105 -29
		mu 0 4 1 37 47 46
		f 4 -107 -110 105 51
		f 4 36 -111 106 39
		mu 0 4 19 49 48 20
		f 4 -74 -109 -112 -37
		mu 0 4 19 35 51 50
		f 4 123 117 -1 1
		mu 1 4 31 35 2 3
		f 4 121 -6 4 7
		mu 1 4 30 33 18 14
		f 4 120 112 6 2
		mu 1 4 35 30 0 1
		f 3 -120 -115 -121
		mu 1 3 35 34 30
		f 3 -119 -122 114
		mu 1 3 34 33 30
		f 3 -123 118 116
		mu 1 3 31 33 34
		f 3 -117 119 -124
		mu 1 3 31 34 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape2" -p "|Drill_Right_Arm|shoulderRight";
	rename -uid "412AB0F2-400B-08DD-AFDD-ADA542447A44";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 14 ".uvst[1].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6 -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5 -13 -2 -5
		 -23 -5 -5 -13 -5 -5;
	setAttr -s 24 ".ed[0:23]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0
		 11 13 0 12 14 0 13 15 0 14 8 0 15 9 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 -12 -10 -8 -6
		mu 0 4 1 8 9 3
		f 4 10 4 6 8
		mu 0 4 10 0 2 11
		f 4 12 17 -14 -17
		mu 1 4 0 1 2 3
		f 4 14 21 -16 -21
		mu 1 4 5 4 6 7
		f 4 15 23 -13 -23
		mu 1 4 7 6 8 9
		f 4 -24 -22 -20 -18
		mu 1 4 1 10 11 2
		f 4 22 16 18 20
		mu 1 4 12 0 3 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRight" -p "Drill_Right_Arm";
	rename -uid "620A93DF-479B-5D45-B88F-D785987E5970";
	setAttr ".rp" -type "double3" -55 -18 -1.25 ;
	setAttr ".sp" -type "double3" -55 -18 -1.25 ;
createNode mesh -n "lowerArmRightShape" -p "|Drill_Right_Arm|lowerArmRight";
	rename -uid "CFC804A0-41A4-5281-0622-0F8BF2EA0CA9";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 74 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]" "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]" "f[22]" "f[23]" "f[24]" "f[25]" "f[26]" "f[27]" "f[28]" "f[29]" "f[30]" "f[31]" "f[32]" "f[33]" "f[34]" "f[35]" "f[36]" "f[37]" "f[38]" "f[39]" "f[40]" "f[41]" "f[42]" "f[43]" "f[44]" "f[45]" "f[46]" "f[47]" "f[48]" "f[49]" "f[50]" "f[51]" "f[52]" "f[53]" "f[54]" "f[55]" "f[56]" "f[57]" "f[58]" "f[59]" "f[60]" "f[61]" "f[62]" "f[63]" "f[64]" "f[65]" "f[66]" "f[67]" "f[68]" "f[69]" "f[70]" "f[71]" "f[72]" "f[73]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 99 ".uvst[0].uvsp[0:98]" -type "float2" 0.375 0.5467304 0.4217304
		 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75
		 0.375 0.7032696 0.6437481 0.24375807 0.54375809 0.30625185 0.38124189 0.26874813
		 0.31874812 0.41873318 0.35625187 0.75626677 0.45624191 0.69377303 0.61875814 0.231227
		 0.68125188 0.081241898 0.375 0.7032696 0.4217304 0.75 0.4217304 0.92500001 0.375
		 0.92500001 0.36098087 0.79882789 0.375 0.92500001 0.45624194 0.69377297 0.35625184
		 0.75626677 0.57826966 0.75 0.625 0.7032696 0.625 0.92500001 0.5782696 0.92500001
		 0.63925052 0.17290242 0.70000005 0 0.68125188 0.081241921 0.61875814 0.231227 0.4217304
		 0.5 0.375 0.5467304 0.375 0.32499999 0.4217304 0.32499999 0.36074951 0.26401913 0.29999998
		 0.25 0.31874812 0.41873324 0.38124189 0.26874813 0.625 0.5467304 0.5782696 0.5 0.57826966
		 0.32499999 0.625 0.32499999 0.56425053 0.31098089 0.54375809 0.30625185 0.64374816
		 0.24375805 0.70000005 0.2032696 0.70000005 0.046730354 0.875 0.046730399 0.875 0.20326962
		 0.125 0.046730399 0.29999998 0.046730399 0.29999998 0.20326963 0.125 0.20326963 0.31401908
		 0.37617201 0.43574953 0.75209755 0.68598092 0.060749516 0.63901913 0.26425049 0.4217304
		 0.5 0.375 0.5467304 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75
		 0.4217304 0.75 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375
		 0.5467304 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696
		 0.625 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375
		 0.7032696 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375 0.5467304
		 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696 0.625
		 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375 0.7032696
		 0.375 0.7032696;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 80 ".vt[0:79]"  -59 -20.50462723 -7 -57.50462723 -22 -7 -59 -20.50462723 6
		 -57.50462723 -22 6 -52.49537277 -22 -7 -51 -20.50462723 -7 -52.49537277 -22 6 -51 -20.50462723 6
		 -57.50462723 -14 -7 -59 -15.49537182 -7 -57.50462723 -14 6 -59 -15.49537182 6 -51 -15.49537182 -7
		 -52.49537277 -14 -7 -51 -15.49537182 6 -52.49537277 -14 6 -56.49567032 -15.0089569092 6
		 -57.99104309 -16.50432968 6 -52.0089569092 -16.50432968 6 -53.50432968 -15.0089569092 6
		 -57.99104309 -19.49567032 6 -56.49567032 -20.99104309 6 -53.50432968 -20.99104309 6
		 -52.0089569092 -19.49567032 6 -57.99104309 -16.50432968 8 -56.49567032 -15.0089569092 8
		 -52.0089569092 -16.50432968 8 -53.50432968 -15.0089569092 8 -56.49567032 -20.99104309 8
		 -57.99104309 -19.49567032 8 -52.0089569092 -19.49567032 8 -53.50432968 -20.99104309 8
		 -57.060417175 -14.70942116 -7 -58.29058075 -15.93958187 -7 -52.93958282 -14.70942116 -7
		 -51.70941925 -15.93958187 -7 -51.70941925 -20.060417175 -7 -52.93958282 -21.29058075 -7
		 -57.060417175 -21.29058075 -7 -58.29058075 -20.060417175 -7 -56.1934967 -16.093933105 -10.5
		 -56.90607071 -16.8065033 -10.5 -53.8065033 -16.093933105 -10.5 -53.093933105 -16.8065033 -10.5
		 -53.093933105 -19.1934967 -10.5 -53.8065033 -19.90607071 -10.5 -56.1934967 -19.90607071 -10.5
		 -56.90607071 -19.1934967 -10.5 -57.060417175 -14.70942116 -7.49999905 -58.29058075 -15.93958187 -7.49999905
		 -57.060417175 -14.70942116 -8.96627808 -58.29058075 -15.93958187 -8.96627808 -52.93958282 -14.70942116 -7.49999905
		 -52.93958282 -14.70942116 -8.96627808 -51.70941925 -15.93958187 -7.49999905 -51.70941925 -15.93958187 -8.96627808
		 -51.70941925 -20.060417175 -7.49999905 -51.70941925 -20.060417175 -8.96627808 -52.93958282 -21.29058075 -7.49999905
		 -52.93958282 -21.29058075 -8.96627808 -57.060417175 -21.29058075 -7.49999905 -57.060417175 -21.29058075 -8.96627808
		 -58.29058075 -20.060417175 -7.49999905 -58.29058075 -20.060417175 -8.96627808 -56.73308945 -15.49966335 -7
		 -57.50033951 -16.26691055 -7 -56.73308945 -15.49966335 -7.49999905 -57.50033951 -16.26691055 -7.49999905
		 -53.26691055 -15.4996624 -7 -53.26691055 -15.4996624 -7.49999905 -52.49966431 -16.26691055 -7
		 -52.49966431 -16.26691055 -7.49999905 -52.49966431 -19.73308945 -7 -52.49966431 -19.73308945 -7.49999905
		 -53.26691055 -20.50033951 -7 -53.26691055 -20.50033951 -7.49999905 -56.73308563 -20.5003376 -7
		 -56.73308563 -20.5003376 -7.49999905 -57.50033569 -19.73308754 -7 -57.50033569 -19.73308754 -7.49999905;
	setAttr -s 152 ".ed[0:151]"  1 4 0 1 0 0 5 4 0 8 13 0 9 0 0 8 9 0 12 5 0
		 13 12 0 25 27 0 25 24 0 26 30 0 27 26 0 29 24 0 29 28 0 31 28 0 31 30 0 1 3 0 3 2 0
		 2 0 0 3 21 1 21 20 0 20 2 1 5 7 0 7 6 0 6 4 0 7 23 1 23 22 0 22 6 1 9 11 0 11 10 0
		 10 8 0 11 17 1 17 16 0 16 10 1 13 15 0 15 14 0 14 12 0 15 19 1 19 18 0 18 14 1 17 24 0
		 25 16 0 19 27 0 26 18 0 21 28 0 29 20 0 23 30 0 31 22 0 10 15 0 6 3 0 14 7 0 2 11 0
		 16 19 0 20 17 0 22 21 0 18 23 0 8 32 1 9 33 1 32 33 1 13 34 1 32 34 1 12 35 1 34 35 1
		 5 36 1 35 36 1 4 37 1 36 37 1 1 38 1 38 37 1 0 39 1 38 39 1 33 39 1 40 41 0 40 42 0
		 42 43 0 43 44 0 44 45 0 46 45 0 46 47 0 41 47 0 48 49 0 48 50 0 50 51 0 49 51 0 48 52 0
		 52 53 0 50 53 0 52 54 0 54 55 0 53 55 0 54 56 0 56 57 0 55 57 0 56 58 0 58 59 0 57 59 0
		 60 58 0 60 61 0 61 59 0 60 62 0 62 63 0 61 63 0 49 62 0 51 63 0 50 40 0 51 41 0 53 42 0
		 55 43 0 57 44 0 59 45 0 61 46 0 63 47 0 32 64 1 33 65 1 64 65 0 48 66 1 64 66 0 49 67 1
		 66 67 0 65 67 0 34 68 1 64 68 0 52 69 1 68 69 0 66 69 0 35 70 1 68 70 0 54 71 1 70 71 0
		 69 71 0 36 72 1 70 72 0 56 73 1 72 73 0 71 73 0 37 74 1 72 74 0 58 75 1 74 75 0 73 75 0
		 38 76 1 76 74 0 60 77 1 76 77 0 77 75 0 39 78 1 76 78 0 62 79 1 78 79 0 77 79 0 65 78 0
		 67 79 0;
	setAttr -s 74 -ch 304 ".fc[0:73]" -type "polyFaces" 
		f 8 -73 73 74 75 76 -78 78 -80
		mu 0 8 0 1 2 3 4 5 6 7
		f 8 -12 -9 9 -13 13 -15 15 -11
		mu 0 8 8 9 10 11 12 13 14 15
		f 4 -2 16 17 18
		mu 0 4 16 17 18 19
		f 4 -18 19 20 21
		mu 0 4 20 21 22 23
		f 4 -3 22 23 24
		mu 0 4 24 25 26 27
		f 4 -24 25 26 27
		mu 0 4 28 29 30 31
		f 4 5 28 29 30
		mu 0 4 32 33 34 35
		f 4 -30 31 32 33
		mu 0 4 36 37 38 39
		f 4 -8 34 35 36
		mu 0 4 40 41 42 43
		f 4 -36 37 38 39
		mu 0 4 43 44 45 46
		f 4 -33 40 -10 41
		mu 0 4 39 38 11 10
		f 4 -39 42 11 43
		mu 0 4 46 45 9 8
		f 4 -21 44 -14 45
		mu 0 4 23 22 13 12
		f 4 -27 46 -16 47
		mu 0 4 31 30 15 14
		f 4 48 -35 -4 -31
		mu 0 4 35 42 41 32
		f 4 -17 0 -25 49
		mu 0 4 18 17 24 27
		f 4 50 -23 -7 -37
		mu 0 4 47 48 49 50
		f 4 -19 51 -29 4
		mu 0 4 51 52 53 54
		f 4 -49 -34 52 -38
		mu 0 4 44 36 39 45
		f 4 -52 -22 53 -32
		mu 0 4 55 20 23 38
		f 4 -50 -28 54 -20
		mu 0 4 56 28 31 22
		f 4 -51 -40 55 -26
		mu 0 4 57 58 46 30
		f 4 -53 -42 8 -43
		mu 0 4 45 39 10 9
		f 4 -54 -46 12 -41
		mu 0 4 38 23 12 11
		f 4 -55 -48 14 -45
		mu 0 4 22 31 14 13
		f 4 -56 -44 10 -47
		mu 0 4 30 46 8 15
		f 4 -6 56 58 -58
		mu 0 4 33 32 59 60
		f 4 3 59 -61 -57
		mu 0 4 32 41 61 59
		f 4 7 61 -63 -60
		mu 0 4 41 40 62 61
		f 4 6 63 -65 -62
		mu 0 4 40 25 63 62
		f 4 2 65 -67 -64
		mu 0 4 25 24 64 63
		f 4 -1 67 68 -66
		mu 0 4 24 17 65 64
		f 4 1 69 -71 -68
		mu 0 4 17 16 66 65
		f 4 -5 57 71 -70
		mu 0 4 16 33 60 66
		f 4 -81 81 82 -84
		mu 0 4 67 68 69 70
		f 4 84 85 -87 -82
		mu 0 4 68 71 72 69
		f 4 87 88 -90 -86
		mu 0 4 71 73 74 72
		f 4 90 91 -93 -89
		mu 0 4 73 75 76 74
		f 4 93 94 -96 -92
		mu 0 4 75 77 78 76
		f 4 -97 97 98 -95
		mu 0 4 77 79 80 78
		f 4 99 100 -102 -98
		mu 0 4 79 81 82 80
		f 4 -103 83 103 -101
		mu 0 4 81 67 70 82
		f 4 -83 104 72 -106
		mu 0 4 70 69 1 0
		f 4 86 106 -74 -105
		mu 0 4 69 72 2 1
		f 4 89 107 -75 -107
		mu 0 4 72 74 3 2
		f 4 92 108 -76 -108
		mu 0 4 74 76 4 3
		f 4 95 109 -77 -109
		mu 0 4 76 78 5 4
		f 4 -99 110 77 -110
		mu 0 4 78 80 6 5
		f 4 101 111 -79 -111
		mu 0 4 80 82 7 6
		f 4 -104 105 79 -112
		mu 0 4 82 70 0 7
		f 4 -115 116 118 -120
		mu 0 4 83 84 85 86
		f 4 121 123 -125 -117
		mu 0 4 84 87 88 85
		f 4 126 128 -130 -124
		mu 0 4 87 89 90 88
		f 4 131 133 -135 -129
		mu 0 4 89 91 92 90
		f 4 136 138 -140 -134
		mu 0 4 91 93 94 92
		f 4 -142 143 144 -139
		mu 0 4 93 95 96 94
		f 4 146 148 -150 -144
		mu 0 4 95 97 98 96
		f 4 -151 119 151 -149
		mu 0 4 97 83 86 98
		f 4 -59 112 114 -114
		mu 0 4 60 59 84 83
		f 4 80 117 -119 -116
		mu 0 4 68 67 86 85
		f 4 60 120 -122 -113
		mu 0 4 59 61 87 84
		f 4 -85 115 124 -123
		mu 0 4 71 68 85 88
		f 4 62 125 -127 -121
		mu 0 4 61 62 89 87
		f 4 -88 122 129 -128
		mu 0 4 73 71 88 90
		f 4 64 130 -132 -126
		mu 0 4 62 63 91 89
		f 4 -91 127 134 -133
		mu 0 4 75 73 90 92
		f 4 66 135 -137 -131
		mu 0 4 63 64 93 91
		f 4 -94 132 139 -138
		mu 0 4 77 75 92 94
		f 4 -69 140 141 -136
		mu 0 4 64 65 95 93
		f 4 96 137 -145 -143
		mu 0 4 79 77 94 96
		f 4 70 145 -147 -141
		mu 0 4 65 66 97 95
		f 4 -100 142 149 -148
		mu 0 4 81 79 96 98
		f 4 -72 113 150 -146
		mu 0 4 66 60 83 97
		f 4 102 147 -152 -118
		mu 0 4 67 81 98 86;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "drillRight" -p "Drill_Right_Arm";
	rename -uid "6C3AAA58-4D87-194C-8495-49A315E13A34";
	setAttr ".rp" -type "double3" -55 -17.999999523162842 11.795709848403931 ;
	setAttr ".sp" -type "double3" -55 -17.999999523162842 11.795709848403931 ;
createNode mesh -n "drillRightShape" -p "drillRight";
	rename -uid "71B4A07C-4B6E-D093-52A4-14A4F08373F9";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 109 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]" "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]" "f[22]" "f[23]" "f[24]" "f[25]" "f[26]" "f[27]" "f[28]" "f[29]" "f[30]" "f[31]" "f[32]" "f[33]" "f[34]" "f[35]" "f[36]" "f[37]" "f[38]" "f[39]" "f[40]" "f[41]" "f[42]" "f[43]" "f[44]" "f[45]" "f[46]" "f[47]" "f[48]" "f[49]" "f[50]" "f[51]" "f[52]" "f[53]" "f[54]" "f[55]" "f[56]" "f[57]" "f[58]" "f[59]" "f[60]" "f[61]" "f[62]" "f[63]" "f[64]" "f[65]" "f[66]" "f[67]" "f[68]" "f[69]" "f[70]" "f[71]" "f[72]" "f[73]" "f[74]" "f[75]" "f[76]" "f[77]" "f[78]" "f[79]" "f[80]" "f[81]" "f[82]" "f[83]" "f[84]" "f[85]" "f[86]" "f[87]" "f[88]" "f[89]" "f[90]" "f[91]" "f[92]" "f[93]" "f[94]" "f[95]" "f[96]" "f[97]" "f[98]" "f[99]" "f[100]" "f[101]" "f[102]" "f[103]" "f[104]" "f[105]" "f[106]" "f[107]" "f[108]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 310 ".uvst[1].uvsp";
	setAttr ".uvst[1].uvsp[0:249]" -type "float2" 0.58838832 0.16161166 0.5 0.12500001
		 0.5 2.9802322e-008 0.6742484 0.075751543 0.41161168 0.16161166 0.32575154 0.075751543
		 0.375 0.25 0.25000003 0.25 0.41161168 0.33838832 0.32575154 0.42424846 0.5 0.375
		 0.5 0.5 0.58838832 0.33838835 0.67424846 0.42424849 0.625 0.25 0.75 0.25 0.24724899
		 0.49960732 0.3125 0.5 0.34375 0.58333331 0.3125 0.5 0.37500015 0.50000107 0.39583334
		 0.58333331 0.36639407 0.49632704 0.4375 0.5 0.44791669 0.58333331 0.4375 0.5 0.49999988
		 0.50000107 0.5 0.58333331 0.49231228 0.49632704 0.5625 0.5 0.55208331 0.58333331
		 0.5625 0.5 0.62499958 0.50000107 0.60416663 0.58333331 0.61823052 0.49632704 0.6875
		 0.5 0.65624994 0.58333331 0.6875 0.5 0.74999857 0.50000226 0.70833325 0.58333331
		 0.34375 0.58333331 0.37499997 0.66666663 0.29166666 0.58333331 0.39583334 0.58333331
		 0.41666663 0.66666663 0.44791669 0.58333331 0.4569672 0.66377592 0.5 0.58333331 0.49998885
		 0.66663706 0.55208331 0.58333331 0.54166663 0.66666663 0.60416663 0.58333331 0.58333331
		 0.66666663 0.65624994 0.58333331 0.625 0.66666663 0.70833325 0.58333331 0.66666669
		 0.66666663 0.37499997 0.66666663 0.40624997 0.74999994 0.33333331 0.66666663 0.29166666
		 0.58333331 0.41666663 0.66666663 0.43749997 0.74999994 0.4569672 0.66377592 0.47111931
		 0.75379086 0.39583334 0.58333331 0.49998885 0.66663706 0.49999997 0.74999994 0.54166663
		 0.66666663 0.53124994 0.74999994 0.5 0.58333331 0.58333331 0.66666663 0.56249994
		 0.74999994 0.625 0.66666663 0.59374994 0.74999994 0.60416663 0.58333331 0.66666669
		 0.66666663 0.62499994 0.74999994 0.65624994 0.58333331 0.40624997 0.74999994 0.43749994
		 0.83333325 0.37499997 0.74999994 0.33333331 0.66666663 0.43749997 0.74999994 0.45833325
		 0.83333325 0.37499997 0.66666663 0.47111931 0.75379086 0.47916657 0.83333325 0.49999997
		 0.74999994 0.49999988 0.83333325 0.4569672 0.66377592 0.53124994 0.74999994 0.52083319
		 0.83333325 0.49998885 0.66663706 0.56249994 0.74999994 0.54166651 0.83333325 0.54166663
		 0.66666663 0.59374994 0.74999994 0.56249982 0.83333325 0.62499994 0.74999994 0.58333313
		 0.83333325 0.625 0.66666663 0.43749994 0.83333325 0.46874994 0.91666657 0.41666663
		 0.83333325 0.37499997 0.74999994 0.45833325 0.83333325 0.4791666 0.91666657 0.47916657
		 0.83333325 0.48957956 0.916637 0.43749997 0.74999994 0.49999988 0.83333325 0.49999991
		 0.91287559 0.47111931 0.75379086 0.52083319 0.83333325 0.51041657 0.91666657 0.49999997
		 0.74999994 0.54166651 0.83333325 0.52083325 0.91666657 0.56249982 0.83333325 0.53124994
		 0.91666657 0.56249994 0.74999994 0.58333313 0.83333325 0.54166663 0.91666657 0.59374994
		 0.74999994 0.46874994 0.91666657 0.5 1 0.45833328 0.91666657 0.41666663 0.83333325
		 0.4791666 0.91666657 0.5 1 0.43749994 0.83333325 0.48957956 0.916637 0.49999991 0.91287559
		 0.47916657 0.83333325 0.51041657 0.91666657 0.52083325 0.91666657 0.52083319 0.83333325
		 0.53124994 0.91666657 0.54166663 0.91666657 0.56249982 0.83333325 0.37500015 0.50000107
		 0.4375 0.5 0.4375 0.5 0.36639407 0.49632704 0.44791669 0.58333331 0.37500015 0.50000107
		 0.36639407 0.49632704 0.44791669 0.58333331 0.49999988 0.50000107 0.5625 0.5 0.5625
		 0.5 0.49231228 0.49632704 0.55208331 0.58333331 0.49999988 0.50000107 0.49231228
		 0.49632704 0.55208331 0.58333331 0.62499958 0.50000107 0.6875 0.5 0.61823052 0.49632704
		 0.65624994 0.58333331 0.62499958 0.50000107 0.61823052 0.49632704 0.41666663 0.66666663
		 0.34375 0.58333331 0.34375 0.58333331 0.41666663 0.66666663 0.49998885 0.66663706
		 0.44791669 0.58333331 0.44791669 0.58333331 0.49998885 0.66663706 0.58333331 0.66666663
		 0.55208331 0.58333331 0.55208331 0.58333331 0.58333331 0.66666663 0.66666669 0.66666663
		 0.65624994 0.58333331 0.65624994 0.58333331 0.66666669 0.66666663 0.40624997 0.74999994
		 0.33333331 0.66666663 0.33333331 0.66666663 0.40624997 0.74999994 0.47111931 0.75379086
		 0.41666663 0.66666663 0.41666663 0.66666663 0.53124994 0.74999994 0.49998885 0.66663706
		 0.53124994 0.74999994 0.59374994 0.74999994 0.58333331 0.66666663 0.58333331 0.66666663
		 0.59374994 0.74999994 0.45833325 0.83333325 0.40624997 0.74999994 0.40624997 0.74999994
		 0.45833325 0.83333325 0.49999988 0.83333325 0.47111931 0.75379086 0.47111931 0.75379086
		 0.49999988 0.83333325 0.54166651 0.83333325 0.53124994 0.74999994 0.53124994 0.74999994
		 0.54166651 0.83333325 0.58333313 0.83333325 0.59374994 0.74999994 0.58333313 0.83333325
		 0.46874994 0.91666657 0.41666663 0.83333325 0.41666663 0.83333325 0.48957956 0.916637
		 0.45833325 0.83333325 0.45833325 0.83333325 0.48957956 0.916637 0.51041657 0.91666657
		 0.49999988 0.83333325 0.49999988 0.83333325 0.51041657 0.91666657 0.53124994 0.91666657
		 0.54166651 0.83333325 0.54166651 0.83333325 0.53124994 0.91666657 0.4791666 0.91666657
		 0.5 1 0.5 1 0.4791666 0.91666657 0.5 1 0.46874994 0.91666657 0.46874994 0.91666657
		 0.5 1 0.49999991 0.91287559 0.5 1 0.5 1 0.5 1 0.48957956 0.916637 0.5 1 0.52083325
		 0.91666657 0.5 1 0.5 1 0.52083325 0.91666657 0.5 1 0.51041657 0.91666657 0.51041657
		 0.91666657 0.5 1 0.54166663 0.91666657 0.5 1 0.5 1 0.54166663 0.91666657 0.5 1;
	setAttr ".uvst[1].uvsp[250:309]" 0.53124994 0.91666657 0.53124994 0.91666657
		 0.5 1 0.43749994 0.83333325 0.4791666 0.91666657 0.43749994 0.83333325 0.37499997
		 0.74999994 0.43749994 0.83333325 0.43749994 0.83333325 0.625 0.66666663 0.62499994
		 0.74999994 0.62499994 0.74999994 0.625 0.66666663 0.60416663 0.58333331 0.625 0.66666663
		 0.625 0.66666663 0.60416663 0.58333331 0.5625 0.5 0.60416663 0.58333331 0.60416663
		 0.58333331 0.5625 0.5 0.52083319 0.83333325 0.52083325 0.91666657 0.52083325 0.91666657
		 0.52083319 0.83333325 0.49999997 0.74999994 0.52083319 0.83333325 0.52083319 0.83333325
		 0.49999997 0.74999994 0.4569672 0.66377592 0.49999997 0.74999994 0.49999997 0.74999994
		 0.39583334 0.58333331 0.4569672 0.66377592 0.39583334 0.58333331 0.3125 0.5 0.39583334
		 0.58333331 0.39583334 0.58333331 0.3125 0.5 0.56249982 0.83333325 0.54166663 0.91666657
		 0.54166663 0.91666657 0.56249982 0.83333325 0.56249994 0.74999994 0.56249982 0.83333325
		 0.56249982 0.83333325 0.56249994 0.74999994 0.54166663 0.66666663 0.56249994 0.74999994
		 0.56249994 0.74999994 0.54166663 0.66666663 0.5 0.58333331 0.54166663 0.66666663
		 0.5 0.58333331 0.4375 0.5 0.5 0.58333331 0.5 0.58333331 0.49999991 0.91287559 0.6875
		 0.5 0.70833325 0.58333331;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 101 ".vt[0:100]"  -52.5 -15.5 6.000000476837 -55 -14.46446609 6.000000476837
		 -57.5 -15.5 6.000000476837 -58.53553391 -18 6.000000476837 -57.5 -20.5 6.000000476837
		 -55 -21.53553391 6.000000476837 -52.5 -20.5 6.000000476837 -51.46446609 -18 6.000000476837
		 -51.46446609 -14.46446514 7.59142017 -55 -12.99999905 7.59142017 -58.53553391 -14.46446514 7.59142017
		 -60 -18 7.59142017 -58.53553391 -21.53553391 7.59142017 -55 -23 7.59142017 -51.46446609 -21.53553391 7.59142017
		 -50 -18 7.59142017 -52.053722382 -15.053721428 9.25808716 -55 -13.83333302 9.25808716
		 -57.94627762 -15.053721428 9.25808716 -59.16666412 -18 9.25808716 -57.94627762 -20.94627762 9.25808716
		 -55 -22.16666412 9.25808716 -52.053722382 -20.94627762 9.25808716 -50.83333588 -18 9.25808716
		 -52.64297867 -15.64297676 10.92475319 -55 -14.66666603 10.92475319 -57.35702133 -15.64297676 10.92475319
		 -58.33333206 -18 10.92475319 -57.35702133 -20.35702133 10.92475319 -55 -21.33333206 10.92475319
		 -52.64297867 -20.35702133 10.92475319 -51.66666794 -18 10.92475319 -53.23223114 -16.23223114 12.59142017
		 -55 -15.49999905 12.59142017 -56.76776886 -16.23223114 12.59142017 -57.5 -18 12.59142017
		 -56.76776886 -19.76776505 12.59142017 -55 -20.5 12.59142017 -53.23223114 -19.76776505 12.59142017
		 -52.5 -18 12.59142017 -53.82148743 -16.82148743 14.2580862 -55 -16.33333206 14.2580862
		 -56.17851257 -16.82148743 14.2580862 -56.66666412 -18 14.2580862 -56.17851257 -19.17850876 14.2580862
		 -55 -19.66666412 14.2580862 -53.82148743 -19.17850876 14.2580862 -53.33333588 -18 14.2580862
		 -54.19506073 -17.78431702 15.92475319 -54.58333206 -17.27830887 15.92475319 -55.21568298 -17.19506073 15.92475319
		 -55.72168732 -17.58333206 15.92475319 -55.80493927 -18.21568298 15.92475319 -55.41666794 -18.72168732 15.92475319
		 -54.78431702 -18.80493927 15.92475319 -54.27831268 -18.41666603 15.92475319 -55 -18 17.59141922
		 -54.97576523 -13.32453823 7.59142494 -55.01763916 -14.068276405 9.19750214 -57.67356873 -15.18416309 9.31134892
		 -57.21544266 -15.8030014 10.86787605 -58.073120117 -17.90404701 10.97277641 -57.30789566 -18.0063400269 12.53992939
		 -56.66497421 -19.53885841 12.63183117 -56.059772491 -19.054317474 14.21477032 -55.078849792 -19.45911407 14.2869482
		 -55.3572998 -18.57945251 15.89412975 -54.84793091 -18.63646317 15.9242363 -59.67546082 -17.97576523 7.59142494
		 -58.93172455 -18.017637253 9.19750214 -57.81583405 -20.67357063 9.31134892 -57.1969986 -20.21544075 10.86787605
		 -55.095954895 -21.073120117 10.97277737 -54.99365997 -20.30789566 12.53992939 -53.46113968 -19.66497421 12.63183212
		 -53.94568253 -19.059772491 14.21477032 -53.54088593 -18.0788517 14.2869482 -54.42054749 -18.35729599 15.89412975
		 -54.36354065 -17.84792709 15.9242363 -55.024238586 -22.67546272 7.59142494 -54.98236084 -21.93172455 9.19750214
		 -52.32643127 -20.81583786 9.31134892 -52.78455734 -20.1969986 10.86787605 -51.92687988 -18.095952988 10.97277737
		 -52.69210434 -17.99365997 12.53992939 -53.33502579 -16.46114159 12.63183117 -53.94022751 -16.94568253 14.21477032
		 -54.92115021 -16.54088593 14.2869482 -54.64270401 -17.42054749 15.8941288 -55.15207672 -17.36353874 15.92423439
		 -50.32453918 -18.024236679 7.59142494 -51.068275452 -17.98236275 9.19750214 -52.18416214 -15.32642937 9.31134892
		 -52.8030014 -15.7845583 10.86787605 -54.9040451 -14.92687893 10.97277737 -55.0063400269 -15.69210243 12.53992939
		 -56.53885651 -16.33502579 12.63183117 -56.054317474 -16.94022751 14.21477032 -56.45911407 -17.9211483 14.28694916
		 -55.57945251 -17.6427021 15.89412975 -55.63646317 -18.15207291 15.9242363;
	setAttr -s 208 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 0 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0
		 5 13 0 6 14 0 7 15 0 8 16 1 10 18 1 12 20 1 17 25 1 19 27 1 21 29 1 26 34 1 28 36 1
		 30 38 1 35 43 1 37 45 1 39 47 1 40 48 1 44 52 1 46 54 1 48 56 0 49 56 0 50 56 0 51 56 0
		 52 56 0 53 56 0 54 56 0 55 56 0 51 42 0 42 33 0 33 24 0 24 23 0 23 14 0 50 41 0 41 32 0
		 32 31 0 31 22 0 22 13 0 14 22 1 23 31 1 24 32 1 33 41 1 42 50 1 55 46 0 46 37 0 37 28 0
		 28 19 0 19 10 0 54 45 0 45 36 0 36 27 0 27 18 0 18 9 0 49 40 0 40 39 0 39 30 0 30 21 0
		 21 12 0 48 47 0 47 38 0 38 29 0 29 20 0 20 11 0 53 44 0 44 35 0 35 26 0 26 17 0 17 8 0
		 52 43 0 43 34 0 34 25 0 25 16 0 16 15 0 9 57 1 8 57 0 17 58 1 57 58 1 58 8 0 18 59 1
		 26 60 1 59 60 1 60 58 0 27 61 1 35 62 1 61 62 1 62 60 0 36 63 1 44 64 1 63 64 1 64 62 0
		 45 65 1 53 66 1 65 66 1 66 64 0 54 67 1 67 56 0 66 56 0 67 65 0 65 63 0 63 61 0 61 59 0
		 59 57 0 11 68 1 10 68 0 19 69 1 68 69 1 69 10 0 20 70 1 28 71 1 70 71 1 71 69 0 29 72 1
		 37 73 1 72 73 1 73 71 0 38 74 1 46 75 1 74 75 1 75 73 0 47 76 1 55 77 1 76 77 1 77 75 0
		 48 78 1 78 56 0 77 56 0 78 76 0 76 74 0 74 72 0 72 70 0 70 68 0 13 79 1 12 79 0 21 80 1
		 79 80 1 80 12 0 22 81 1 30 82 1 81 82 1 82 80 0 31 83 1 39 84 1 83 84 1 84 82 0 32 85 1
		 40 86 1 85 86 1;
	setAttr ".ed[166:207]" 86 84 0 41 87 1 49 88 1 87 88 1 88 86 0 50 89 1 89 56 0
		 88 56 0 89 87 0 87 85 0 85 83 0 83 81 0 81 79 0 15 90 1 14 90 0 23 91 1 90 91 1 91 14 0
		 16 92 1 24 93 1 92 93 1 93 91 0 25 94 1 33 95 1 94 95 1 95 93 0 34 96 1 42 97 1 96 97 1
		 97 95 0 43 98 1 51 99 1 98 99 1 99 97 0 52 100 1 100 56 0 99 56 0 100 98 0 98 96 0
		 96 94 0 94 92 0 92 90 0;
	setAttr -s 109 -ch 416 ".fc[0:108]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 1 4 0 1 2 3
		f 4 1 18 -10 -18
		mu 1 4 1 4 5 2
		f 4 2 19 -11 -19
		mu 1 4 4 6 7 5
		f 4 3 20 -12 -20
		mu 1 4 6 8 9 7
		f 4 4 21 -13 -21
		mu 1 4 8 10 11 9
		f 4 5 22 -14 -22
		mu 1 4 10 12 13 11
		f 4 6 23 -15 -23
		mu 1 4 12 14 15 13
		f 4 7 16 -16 -24
		mu 1 4 14 0 3 15
		f 3 93 95 96
		mu 1 3 16 17 18
		f 3 9 25 71
		mu 1 3 19 20 21
		f 3 122 124 125
		mu 1 3 22 23 24
		f 3 11 26 81
		mu 1 3 25 26 27
		f 3 151 153 154
		mu 1 3 28 29 30
		f 3 13 57 56
		mu 1 3 31 32 33
		f 3 180 182 183
		mu 1 3 34 35 36
		f 3 15 24 91
		mu 1 3 37 38 39
		f 4 27 90 -25 -87
		mu 1 4 40 41 42 16
		f 4 99 100 -96 -121
		mu 1 4 43 44 18 17
		f 4 28 70 -26 -67
		mu 1 4 45 46 21 20
		f 4 128 129 -125 -150
		mu 1 4 47 48 24 23
		f 4 29 80 -27 -77
		mu 1 4 49 50 27 26
		f 4 157 158 -154 -179
		mu 1 4 51 52 30 29
		f 4 58 55 -58 -52
		mu 1 4 53 54 33 32
		f 4 186 187 -183 -208
		mu 1 4 55 56 36 35
		f 4 190 191 -187 -207
		mu 1 4 57 58 59 60
		f 4 30 89 -28 -86
		mu 1 4 61 62 41 40
		f 4 103 104 -100 -120
		mu 1 4 63 64 44 65
		f 4 31 69 -29 -66
		mu 1 4 66 67 46 45
		f 4 132 133 -129 -149
		mu 1 4 68 69 48 70
		f 4 32 79 -30 -76
		mu 1 4 71 72 50 49
		f 4 161 162 -158 -178
		mu 1 4 73 74 52 75
		f 4 59 54 -59 -51
		mu 1 4 76 77 54 78
		f 4 60 53 -60 -50
		mu 1 4 79 80 81 82
		f 4 194 195 -191 -206
		mu 1 4 83 84 58 85
		f 4 33 88 -31 -85
		mu 1 4 86 87 62 61
		f 4 107 108 -104 -119
		mu 1 4 88 89 64 90
		f 4 34 68 -32 -65
		mu 1 4 91 92 67 93
		f 4 136 137 -133 -148
		mu 1 4 94 95 69 96
		f 4 35 78 -33 -75
		mu 1 4 97 98 72 71
		f 4 165 166 -162 -177
		mu 1 4 99 100 74 101
		f 4 169 170 -166 -176
		mu 1 4 102 103 104 105
		f 4 61 52 -61 -49
		mu 1 4 106 107 80 79
		f 4 198 199 -195 -205
		mu 1 4 108 109 84 110
		f 4 37 87 -34 -84
		mu 1 4 111 112 87 113
		f 4 111 112 -108 -118
		mu 1 4 114 115 89 116
		f 4 38 67 -35 -64
		mu 1 4 117 118 92 91
		f 4 140 141 -137 -147
		mu 1 4 119 120 95 121
		f 4 36 77 -36 -74
		mu 1 4 122 123 98 124
		f 8 -6 -5 -4 -3 -2 -1 -8 -7
		mu 1 8 12 10 8 6 4 1 0 14
		f 4 40 -40 -37 -73
		mu 1 4 125 126 127 128
		f 4 172 -174 -170 -175
		mu 1 4 129 130 103 131
		f 4 42 -42 -62 -48
		mu 1 4 132 126 107 106
		f 4 201 -203 -199 -204
		mu 1 4 133 130 109 134
		f 4 44 -44 -38 -83
		mu 1 4 135 126 112 111
		f 4 114 -116 -112 -117
		mu 1 4 136 130 115 137
		f 4 46 -46 -39 -63
		mu 1 4 138 126 118 117
		f 4 143 -145 -141 -146
		mu 1 4 139 130 120 140
		f 3 8 92 -94
		mu 1 3 16 19 17
		f 3 86 -97 -95
		mu 1 3 40 16 18
		f 4 85 94 -101 -99
		mu 1 4 141 142 143 144
		f 4 84 98 -105 -103
		mu 1 4 145 146 147 148
		f 4 83 102 -109 -107
		mu 1 4 149 150 151 152
		f 4 82 106 -113 -111
		mu 1 4 153 154 155 156
		f 3 45 -115 -114
		mu 1 3 157 158 159
		f 3 -45 110 115
		mu 1 3 160 161 162
		f 4 -68 113 116 -110
		mu 1 4 163 164 165 166
		f 4 -69 109 117 -106
		mu 1 4 167 168 169 170
		f 4 -70 105 118 -102
		mu 1 4 171 172 173 174
		f 4 -71 101 119 -98
		mu 1 4 175 176 177 178
		f 4 -72 97 120 -93
		mu 1 4 179 180 181 182
		f 3 10 121 -123
		mu 1 3 183 184 185
		f 3 66 -126 -124
		mu 1 3 186 187 188
		f 4 65 123 -130 -128
		mu 1 4 189 190 191 192
		f 4 64 127 -134 -132
		mu 1 4 193 194 195 196
		f 4 63 131 -138 -136
		mu 1 4 197 198 199 200
		f 4 62 135 -142 -140
		mu 1 4 201 202 203 204
		f 3 39 -144 -143
		mu 1 3 205 206 207
		f 3 -47 139 144
		mu 1 3 208 209 210
		f 4 -78 142 145 -139
		mu 1 4 211 212 213 214
		f 4 -79 138 146 -135
		mu 1 4 215 216 217 218
		f 4 -80 134 147 -131
		mu 1 4 219 220 221 222
		f 4 -81 130 148 -127
		mu 1 4 223 224 225 226
		f 4 -82 126 149 -122
		mu 1 4 227 228 229 230
		f 3 12 150 -152
		mu 1 3 231 232 233
		f 3 76 -155 -153
		mu 1 3 234 235 236
		f 4 75 152 -159 -157
		mu 1 4 237 238 239 240
		f 4 74 156 -163 -161
		mu 1 4 241 242 243 244
		f 4 73 160 -167 -165
		mu 1 4 245 246 247 248
		f 4 72 164 -171 -169
		mu 1 4 249 250 251 252
		f 3 41 -173 -172
		mu 1 3 253 254 255
		f 3 -41 168 173
		mu 1 3 256 257 258
		f 4 -53 171 174 -168
		mu 1 4 259 260 261 262
		f 4 -54 167 175 -164
		mu 1 4 263 264 265 266
		f 4 -55 163 176 -160
		mu 1 4 267 268 269 270
		f 4 -56 159 177 -156
		mu 1 4 271 272 273 274
		f 4 -57 155 178 -151
		mu 1 4 275 276 277 278
		f 3 14 179 -181
		mu 1 3 279 280 281
		f 3 51 -184 -182
		mu 1 3 282 283 284
		f 4 50 181 -188 -186
		mu 1 4 285 286 287 288
		f 4 49 185 -192 -190
		mu 1 4 289 290 291 292
		f 4 48 189 -196 -194
		mu 1 4 293 294 295 296
		f 4 47 193 -200 -198
		mu 1 4 297 298 299 300
		f 3 43 -202 -201
		mu 1 3 301 302 303
		f 3 -43 197 202
		mu 1 3 304 305 306
		f 4 -88 200 203 -197
		mu 1 4 87 112 307 134
		f 4 -89 196 204 -193
		mu 1 4 62 87 134 110
		f 4 -90 192 205 -189
		mu 1 4 41 62 110 85
		f 4 -91 188 206 -185
		mu 1 4 42 41 85 60
		f 4 -92 184 207 -180
		mu 1 4 308 39 309 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Gun_Right_Arm";
	rename -uid "B4C89028-4408-11E3-1625-84A81698F616";
	setAttr ".t" -type "double3" -3.5527136788005009e-015 33 0 ;
	setAttr ".rp" -type "double3" -37 -10.123457908630371 4.790226936340332 ;
	setAttr ".sp" -type "double3" -37 -10.123457908630371 4.790226936340332 ;
createNode transform -n "upperArmRight" -p "Gun_Right_Arm";
	rename -uid "2F6904DD-4709-DCCA-DB5F-9DB28184C95F";
	setAttr ".rp" -type "double3" -37 -11 0 ;
	setAttr ".sp" -type "double3" -37 -11 0 ;
createNode mesh -n "upperArmRightShape" -p "|Gun_Right_Arm|upperArmRight";
	rename -uid "439D4C48-4DFB-23CA-8047-B1A001353EFB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0.31249374
		 0.625 0.31249374 0.375 0.43750626 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.43749374 0.25 0.5625062 3.7252903e-009 0.18749376
		 0.25 0.18749379 3.7252903e-009 0.3125062 3.7252903e-009 0.43749374 0.75 0.5625062
		 0.5 0.68749374 0.25 0.8125062 3.7252903e-009 0.81250632 0.25 0.43749377 3.7252903e-009
		 0.31250626 0.25 0.6874938 3.7252903e-009 0.56250626 0.25 0.43749377 0.5 0.375 0.81249374
		 0.625 0.43750626 0.56250626 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  -40 -17 1.50014997 -38.50015259 -17 3 -40 -5 1.50014997
		 -38.50015259 -5 3 -34 -17 1.50014997 -35.49984741 -17 3 -34 -5 1.50014997 -35.49984741 -5 3
		 -40 -5 -1.50014997 -38.50015259 -5 -3 -40 -17 -1.50014997 -38.50015259 -17 -3 -35.49984741 -5 -3
		 -34 -5 -1.50014997 -34 -17 -1.50014997 -35.49984741 -17 -3;
	setAttr -s 24 ".ed[0:23]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0
		 7 6 0 9 12 0 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0
		 6 4 0 8 10 0 11 9 0 12 15 0 14 13 0;
	setAttr -s 10 -ch 48 ".fc[0:9]" -type "polyFaces" 
		f 4 17 0 18 -4
		mu 0 4 8 18 9 21
		f 4 21 8 22 -12
		mu 0 4 13 22 14 25
		f 4 19 -15 23 -7
		mu 0 4 15 20 16 17
		f 4 20 10 16 2
		mu 0 4 10 11 12 19
		f 8 -5 3 7 6 13 -9 9 -3
		mu 0 8 0 8 21 1 24 14 22 2
		f 8 -13 11 15 14 5 -1 1 -11
		mu 0 8 23 13 25 3 4 5 6 7
		f 4 -2 -18 4 -17
		mu 0 4 12 18 8 19
		f 4 -6 -20 -8 -19
		mu 0 4 9 20 15 21
		f 4 -10 -22 12 -21
		mu 0 4 2 22 13 23
		f 4 -14 -24 -16 -23
		mu 0 4 14 24 3 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape4" -p "|Gun_Right_Arm|upperArmRight";
	rename -uid "1603E79C-4BD6-810D-5F4F-4BA3EF3733FF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "shoulderRight" -p "Gun_Right_Arm";
	rename -uid "68DB8342-42DD-8B81-0490-829C177A16CB";
	setAttr ".rp" -type "double3" -37 1 0 ;
	setAttr ".sp" -type "double3" -37 1 0 ;
createNode mesh -n "shoulderRightShape" -p "|Gun_Right_Arm|shoulderRight";
	rename -uid "8EBE9C33-4CCD-419C-AF62-82AE80465F9B";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:60]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.57292187213897705 0.375 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0.375 0.30207813
		 0.375 0.44792187 0.32292187 0.1805625 0.32292187 0 0.42707813 0.1805625 0.625 0.30207813
		 0.57292187 0.1805625 0.57292187 0 0.375 0.5694375 0.42707813 0.75 0.375 0.75 0.42707813
		 0.44792187 0.57292187 0.44792187 0.625 0.5694375 0.625 0.75 0.42707813 7.4505806e-009
		 0.42707813 0.30207813 0.57292187 0.30207813 0.42707813 0.5694375 0.57292187 0.5694375
		 0.57292187 0.75 0.67707813 7.4505806e-009 0.82292187 0 0.82292187 0.1805625 0.67707813
		 0.1805625 0.17707813 7.4505806e-009 0.17707813 0.1805625 0.625 0.44792187 0.40103906
		 0.30207813 0.375 0.24132031 0.42707813 0.24132031 0.57292187 0.24132031 0.59896094
		 0.30207813 0.625 0.24132031 0.59896094 0.44792187 0.57292187 0.50867969 0.42707813
		 0.50867969 0.40103906 0.44792187 0.375 3.7252903e-009 0.375 0.1805625 0.40103906
		 0.24132031 0.625 3.7252903e-009 0.625 0.1805625 0.59896094 0.24132031 0.40103906
		 0.75 0.40103906 0.5694375 0.40103906 0.50867969 0.4140586 0.47830078 0.59896094 0.75
		 0.59896094 0.5694375 0.59896094 0.50867969 0.58594143 0.47830078;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 37 ".uvst[1].uvsp[0:36]" -type "float2" 0.625 0.81249374
		 0.625 0.93750626 0.56250626 1 0.43749374 1 0.375 0.93750626 0.43749374 3.7252903e-009
		 0.43749374 0.25 0.375 0.25 0.68749374 -3.7252903e-009 0.625 0.25 0.375 0.5 0.375
		 0.81249374 0.56250626 0.5 0.625 0.5 0.56250626 0.75 0.56250626 -3.7252903e-009 0.56250626
		 0.25 0.43749374 0.5 0.43749374 0.75 0.68749374 0.25 0.81250626 -3.7252903e-009 0.81250632
		 0.25 0.18749374 0.25 0.18749374 -3.7252903e-009 0.31250626 -3.7252903e-009 0.31250626
		 0.25 0.40624687 0.25 0.5937531 0.25 0.40624687 0.5 0.5937531 0.5 0.5937531 0.7812469
		 0.40624687 0.9687531 0.375 0 0.40624687 0.7812469 0.5 0.875 0.5937531 0.9687531 0.625
		 -3.7252903e-009;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 65 ".vt[0:64]"  -43 4.50024986 3.5002501 -40.50025177 4.50024986 6
		 -40.50025177 7 3.5002501 -33.49974823 7 3.5002501 -33.49974823 4.50024986 6 -31 4.50024986 3.5002501
		 -43 4.50024986 -3.5002501 -40.50025177 7 -3.5002501 -40.50025177 4.50024986 -6 -33.49974823 4.50024986 -6
		 -33.49974823 7 -3.5002501 -31 4.50024986 -3.5002501 -40.50025177 -2 6 -43 -2 3.5002501
		 -31 -2 3.5002501 -33.49974823 -2 6 -43 -2 -3.5002501 -40.50025177 -2 -6 -33.49974823 -2 -6
		 -31 -2 -3.5002501 -42 -5 2.5002501 -39.50025177 -5 5 -42 -2 2.5002501 -39.50025177 -2 5
		 -32 -5 2.5002501 -34.49974823 -5 5 -34.49974823 -2 5 -32 -2 2.5002501 -39.50025177 -2 -5
		 -42 -2 -2.5002501 -42 -5 -2.5002501 -39.50025177 -5 -5 -32 -2 -2.5002501 -34.49974823 -2 -5
		 -32 -5 -2.5002501 -34.49974823 -5 -5 -42.039665222 6.07776022 3.5002501 -40.50025177 6.07776022 5.039667606
		 -33.49974823 6.07776022 5.039667606 -31.96033287 6.07776022 3.5002501 -31.96033287 6.07776022 -3.5002501
		 -33.49974823 6.07776022 -5.03966713 -40.50025177 6.07776022 -5.039667606 -42.039665222 6.077759743 -3.5002501
		 -40.97871399 -2 3.9787128 -42.039665222 -2 5.039667606 -42.039665222 4.50024986 5.039667606
		 -41.58447266 5.66208887 4.58447266 -33.021286011 -2 3.9787128 -31.96033287 -2 5.039667606
		 -31.96033287 4.50024986 5.039667606 -32.41552734 5.66208887 4.58447266 -40.97871399 -2 -3.9787128
		 -42.039665222 -2 -5.039667606 -42.039665222 4.50024986 -5.039667606 -41.58447266 5.66208887 -4.58447266
		 -33.021286011 -2 -3.9787128 -31.96033287 -2 -5.039667606 -31.96033287 4.50024986 -5.039667606
		 -32.41552734 5.66208887 -4.58447266 -33.021286011 -5 -3.9787128 -40.97871399 -5 3.97871256
		 -40.97871399 -5 -3.97871256 -37 -5 0 -33.021286011 -5 3.9787128;
	setAttr -s 124 ".ed[0:123]"  21 25 0 21 61 0 24 64 0 30 20 0 31 35 0 31 62 0
		 34 24 0 35 60 0 0 36 0 2 7 0 7 43 0 6 0 0 1 46 0 0 13 0 13 45 0 12 1 0 2 37 0 1 4 0
		 4 38 0 3 2 0 3 39 0 5 11 0 11 40 0 10 3 0 5 50 0 4 15 0 15 49 0 14 5 0 6 54 0 8 17 0
		 17 53 0 16 6 0 8 42 0 7 10 0 10 41 0 9 8 0 9 58 0 11 19 0 19 57 0 18 9 0 13 22 1
		 22 44 0 23 12 1 15 26 1 26 48 0 27 14 1 17 28 1 28 52 0 29 16 1 19 32 1 32 56 0 33 18 1
		 21 23 0 22 20 0 24 27 0 26 25 0 28 31 0 30 29 0 32 34 0 35 33 0 12 15 0 18 17 0 14 19 0
		 16 13 0 26 23 0 28 33 0 32 27 0 22 29 0 36 2 0 37 1 0 38 3 0 39 5 0 40 10 0 41 9 0
		 42 7 0 43 6 0 36 47 1 37 38 1 38 51 1 39 40 1 40 59 1 41 42 1 42 55 1 43 36 1 44 23 0
		 45 12 0 46 0 0 47 37 1 44 45 1 45 46 1 46 47 1 48 27 0 49 14 0 50 4 0 51 39 1 48 49 1
		 49 50 1 50 51 1 52 29 0 53 16 0 54 8 0 55 43 1 52 53 1 53 54 1 54 55 1 56 33 0 57 18 0
		 58 11 0 59 41 1 56 57 1 57 58 1 58 59 1 60 34 0 61 20 0 60 63 1 62 30 0 63 61 1 64 25 0
		 62 63 1 63 64 1 64 60 1 60 62 1 62 61 1 61 64 1;
	setAttr -s 61 -ch 248 ".fc[0:60]" -type "polyFaces" 
		f 4 -116 122 113 -4
		mu 1 4 11 33 31 4
		f 4 83 68 9 10
		mu 0 4 37 28 16 11
		f 4 89 86 13 14
		mu 0 4 38 39 2 3
		f 4 16 77 70 19
		mu 0 4 16 30 31 17
		f 4 20 79 72 23
		mu 0 4 17 32 34 12
		f 4 96 93 25 26
		mu 0 4 41 42 6 7
		f 4 103 100 29 30
		mu 0 4 44 45 18 9
		f 4 81 74 33 34
		mu 0 4 35 36 11 12
		f 4 110 107 37 38
		mu 0 4 48 49 13 14
		f 4 88 -15 40 41
		f 4 95 -27 43 44
		f 4 102 -31 46 47
		f 4 109 -39 49 50
		f 6 -114 -2 52 -85 -42 53
		mu 1 6 24 32 5 6 26 7
		f 6 -118 -3 54 -92 -45 55
		mu 1 6 15 36 8 9 27 16
		f 6 -99 -48 56 5 115 57
		mu 1 6 10 28 17 18 33 11
		f 6 -106 -51 58 -113 -8 59
		mu 1 6 12 29 13 0 30 14
		f 4 60 -26 -18 -16
		mu 0 4 15 7 6 4
		f 4 -20 -24 -34 -10
		mu 0 4 16 17 12 11
		f 4 -36 -40 61 -30
		mu 0 4 18 19 20 9
		f 4 62 -38 -22 -28
		mu 0 4 21 22 23 24
		f 4 63 -14 -12 -32
		mu 0 4 25 3 2 26
		f 4 -53 0 -56 64
		mu 1 4 6 5 15 16
		f 4 65 -60 -5 -57
		mu 1 4 17 12 14 18
		f 4 -55 -7 -59 66
		mu 1 4 19 8 20 21
		f 4 -58 3 -54 67
		mu 1 4 22 23 24 25
		f 4 -61 -43 -65 -44
		f 4 -64 -49 -68 -41
		f 4 -62 -52 -66 -47
		f 4 -63 -46 -67 -50
		f 4 76 87 -17 -69
		mu 0 4 29 40 30 16
		f 4 -71 78 94 -21
		mu 0 4 17 31 43 33
		f 4 82 101 -11 -75
		mu 0 4 36 47 37 11
		f 4 80 108 -35 -73
		mu 0 4 34 51 35 12
		f 4 -87 90 -77 -9
		mu 0 4 2 39 40 29
		f 4 -78 69 17 18
		mu 0 4 31 30 4 6
		f 4 97 -79 -19 -94
		mu 0 4 42 43 31 6
		f 4 -80 71 21 22
		mu 0 4 34 32 5 27
		f 4 111 -81 -23 -108
		mu 0 4 50 51 34 27
		f 4 32 -82 73 35
		mu 0 4 18 36 35 19
		f 4 104 -83 -33 -101
		mu 0 4 46 47 36 18
		f 4 8 -84 75 11
		mu 0 4 0 28 37 1
		f 4 -86 -89 84 42
		f 4 12 -90 85 15
		mu 0 4 4 39 38 15
		f 4 -91 -13 -70 -88
		mu 0 4 40 39 4 30
		f 4 -93 -96 91 45
		f 4 24 -97 92 27
		mu 0 4 24 42 41 21
		f 4 -95 -98 -25 -72
		mu 0 4 33 43 42 24
		f 4 -100 -103 98 48
		f 4 28 -104 99 31
		mu 0 4 8 45 44 10
		f 4 -76 -102 -105 -29
		mu 0 4 1 37 47 46
		f 4 -107 -110 105 51
		f 4 36 -111 106 39
		mu 0 4 19 49 48 20
		f 4 -74 -109 -112 -37
		mu 0 4 19 35 51 50
		f 4 123 117 -1 1
		mu 1 4 31 35 2 3
		f 4 121 -6 4 7
		mu 1 4 30 33 18 14
		f 4 120 112 6 2
		mu 1 4 35 30 0 1
		f 3 -120 -115 -121
		mu 1 3 35 34 30
		f 3 -119 -122 114
		mu 1 3 34 33 30
		f 3 -123 118 116
		mu 1 3 31 33 34
		f 3 -117 119 -124
		mu 1 3 31 34 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape2" -p "|Gun_Right_Arm|shoulderRight";
	rename -uid "E29326C6-46E9-142F-DE25-1DB359FC21E7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 14 ".uvst[1].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6 -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5 -13 -2 -5
		 -23 -5 -5 -13 -5 -5;
	setAttr -s 24 ".ed[0:23]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0
		 11 13 0 12 14 0 13 15 0 14 8 0 15 9 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 -12 -10 -8 -6
		mu 0 4 1 8 9 3
		f 4 10 4 6 8
		mu 0 4 10 0 2 11
		f 4 12 17 -14 -17
		mu 1 4 0 1 2 3
		f 4 14 21 -16 -21
		mu 1 4 5 4 6 7
		f 4 15 23 -13 -23
		mu 1 4 7 6 8 9
		f 4 -24 -22 -20 -18
		mu 1 4 1 10 11 2
		f 4 22 16 18 20
		mu 1 4 12 0 3 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRightGun" -p "Gun_Right_Arm";
	rename -uid "608D9AF6-402D-65D1-1C95-B9AEC5AD2C6F";
	setAttr ".rp" -type "double3" -37 -20.623457908630371 4.790226936340332 ;
	setAttr ".sp" -type "double3" -37 -20.623457908630371 4.790226936340332 ;
createNode mesh -n "lowerArmRightGunShape" -p "lowerArmRightGun";
	rename -uid "4D86FA98-4D0D-679E-2957-8FA24C909E6A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.47885606810450554 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 158 ".uvst[0].uvsp[0:157]" -type "float2" 0.625 0.5467304 0.375
		 0.7032696 0.4217304 0.75 0.4217304 0.92500001 0.375 0.92500001 0.625 0.7032696 0.625
		 0.92500001 0.375 0.5467304 0.375 0.32499999 0.5782696 0.5 0.57826966 0.32499999 0.625
		 0.32499999 0.4217304 0.32499999 0.4217304 0.5 0.57826966 0.75 0.5782696 0.92500001
		 0.70000005 0.2032696 0.70000005 0.046730354 0.875 0.046730399 0.875 0.20326962 0.125
		 0.046730399 0.29999998 0.046730399 0.29999998 0.20326963 0.125 0.20326963 0.375 0.5467304
		 0.4217304 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75 0.4217304
		 0.75 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.5782696 0.5 0.625 0.5467304
		 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.5467304 0.4217304
		 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75
		 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.5782696 0.5 0.625 0.5467304 0.625
		 0.7032696 0.57826966 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.5467304 0.4217304
		 0.5 0.4217304 0.5 0.375 0.5467304 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625
		 0.5467304 0.625 0.7032696 0.625 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304
		 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.7032696 0.6437481 0.24375807 0.54375809
		 0.30625185 0.38124189 0.26874813 0.31874812 0.41873318 0.35625187 0.75626677 0.45624191
		 0.69377303 0.61875814 0.231227 0.68125188 0.081241898 0.6437481 0.24375807 0.31874812
		 0.41873318 0.38124189 0.26874813 0.54375809 0.30625185 0.70125204 0.032712124 0.34285599
		 0.81839383 0.31874812 0.41873318 0.38231337 0.25929666 0.54042125 0.31069157 0.6437481
		 0.24375807 0.6123848 0.26102608 0.32842079 0.41435975 0.59295321 0.26463816 0.33587527
		 0.57287741 0.33985224 0.37760419 0.425953 0.27906609 0.62232637 0.21951714 0.66087526
		 0.16954048 0.66087526 0.16954048 0.44455057 0.57287043 0.44455051 0.57287049 0.49904698
		 0.2959339 0.49904695 0.2959339 0.59295321 0.26463822 0.33587524 0.57287729 0.42595297
		 0.27906609 0.45624191 0.69377303 0.35625187 0.75626677 0.61875814 0.231227 0.45624191
		 0.69377303 0.45624194 0.69377303 0.68125188 0.081241898 0.61875814 0.231227 0.35625187
		 0.75626677 0.35625187 0.75626677 0.68125188 0.081241898 0.61875814 0.231227 0.68125182
		 0.081241898 0.4062469 0.72501993 0.4062469 0.72501993 0.4062469 0.72501993 0.4062469
		 0.72501993 0.65000498 0.15623444 0.65000498 0.15623444 0.65000498 0.15623444 0.65000498
		 0.15623444 0.62232637 0.21951714 0.66087526 0.16954048 0.59295321 0.26463816 0.44455051
		 0.57287049 0.33587527 0.57287741 0.32842079 0.41435975 0.425953 0.27906609 0.49904695
		 0.2959339 0.65000498 0.15623444 0.61875814 0.231227 0.45624191 0.69377303 0.4062469
		 0.72501993 0.35625187 0.75626677 0.68125188 0.081241898 0.62232637 0.21951714 0.66087526
		 0.16954048 0.59295321 0.26463816 0.44455051 0.57287049 0.33587527 0.57287741 0.32842079
		 0.41435975 0.425953 0.27906609 0.49904695 0.2959339 0.65000498 0.15623444 0.61875814
		 0.231227 0.45624191 0.69377303 0.4062469 0.72501993 0.35625187 0.75626677 0.68125188
		 0.081241898 0.61875814 0.231227 0.45624191 0.69377303 0.45624191 0.69377303 0.61875814
		 0.231227;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 142 ".vt[0:141]"  -41.05241394 -19.59164619 -7 -39.87148285 -21.47247696 -7
		 -41.05241394 -19.59164619 0.61562443 -39.87148285 -21.47247696 0.61562443 -34.12851715 -21.47247696 -7
		 -32.94758606 -19.59164619 -7 -34.12851715 -21.47247696 0.61562443 -32.94758606 -19.59164619 0.61562443
		 -39.5293045 -14 -7 -41.05241394 -16.30416298 -7 -39.5293045 -14 0.61562443 -41.05241394 -16.30416298 0.61562443
		 -32.94758606 -16.30416298 -7 -34.4706955 -14 -7 -32.94758606 -16.30416298 0.61562443
		 -34.4706955 -14 0.61562443 -39.0060157776 -14.70942116 -7 -40.21670532 -16.74837303 -7
		 -34.99398422 -14.70942116 -7 -33.78330231 -16.74837303 -7 -33.78330231 -19.14743614 -7
		 -34.65180206 -20.76305771 -7 -39.34819794 -20.76305771 -7 -40.21670532 -19.14743614 -7
		 -37.9847641 -16.093933105 -10.5 -38.58572006 -17.61529541 -10.5 -36.015235901 -16.093933105 -10.5
		 -35.41428375 -17.61529541 -10.5 -35.41428375 -18.28051567 -10.5 -35.67305756 -19.37854767 -10.5
		 -38.32694626 -19.37854767 -10.5 -38.58572006 -18.28051567 -10.5 -39.0060157776 -14.70942116 -7.49999905
		 -40.21670532 -16.74837303 -7.49999905 -39.0060157776 -14.70942116 -8.96627808 -40.21670532 -16.74837303 -8.96627808
		 -34.99398422 -14.70942116 -7.49999905 -34.99398422 -14.70942116 -8.96627808 -33.78330231 -16.74837303 -7.49999905
		 -33.78330231 -16.74837303 -8.96627808 -33.78330231 -19.14743614 -7.49999905 -33.78330231 -19.14743614 -8.96627808
		 -34.65180206 -20.76305771 -7.49999905 -34.65180206 -20.76305771 -8.96627808 -39.34819794 -20.76305771 -7.49999905
		 -39.34819794 -20.76305771 -8.96627808 -40.21670532 -19.14743614 -7.49999905 -40.21670532 -19.14743614 -8.96627808
		 -38.62041473 -15.49966335 -7 -39.28578186 -17.075702667 -7 -38.62041473 -15.49966335 -7.49999905
		 -39.28578186 -17.075702667 -7.49999905 -35.37958527 -15.4996624 -7 -35.37958527 -15.4996624 -7.49999905
		 -34.71422195 -17.075702667 -7 -34.71422195 -17.075702667 -7.49999905 -34.71422195 -18.82010841 -7
		 -34.71422195 -18.82010841 -7.49999905 -35.037406921 -19.97281647 -7 -35.037406921 -19.97281647 -7.49999905
		 -38.96259308 -19.97281456 -7 -38.96259308 -19.97281456 -7.49999905 -39.28578186 -18.82010651 -7
		 -39.28578186 -18.82010651 -7.49999905 -35.054801941 -23.35330772 0.61562395 -38.94519424 -23.35330772 0.6156249
		 -41.05241394 -16.30416298 8.77538395 -41.05241394 -19.59164619 8.77538395 -32.94758606 -19.59164619 8.77538395
		 -32.94758606 -16.30416298 8.77538395 -34.4706955 -14 8.77538395 -39.5293045 -14 8.7753849
		 -38.94519424 -23.35330772 8.7753849 -35.054801941 -23.35330772 8.77538395 -41.05241394 -16.30416298 15.9171381
		 -41.05241394 -19.59164619 15.9171381 -32.94758606 -19.59164619 15.9171381 -32.94758606 -16.30416298 15.9171381
		 -34.4706955 -14 15.9171381 -39.5293045 -14 15.9171381 -39.22785187 -15.91492462 15.9171381
		 -34.77215195 -15.91492558 15.9171381 -39.22785187 -15.91492462 20.080453873 -34.77215195 -15.91492558 20.080453873
		 -39.25120544 -17.5380764 15.9171381 -38.027954102 -18.76132774 15.9171381 -38.027954102 -18.76132774 20.080453873
		 -39.25120544 -17.5380764 20.080453873 -35.9720459 -18.76132774 15.9171381 -34.74879456 -17.5380764 15.9171381
		 -34.74879456 -17.5380764 20.080453873 -35.9720459 -18.76132774 20.080453873 -36.019161224 -14.71590328 15.9171381
		 -36.019161224 -14.71590328 20.080453873 -37.98083878 -14.71590328 15.9171381 -37.98083878 -14.71590328 20.080453873
		 -36.065746307 -20.13676834 8.77538395 -36.065746307 -20.13676643 14.9582243 -37.9050293 -22.79725456 8.77538586
		 -37.9050293 -22.79725456 14.9582243 -36.094970703 -22.79725456 8.77538395 -36.094970703 -22.79725456 14.9582243
		 -37.93425369 -20.13676834 8.77538395 -37.93425369 -20.13676643 14.9582243 -34.12851715 -21.47247696 8.77538395
		 -35.15098572 -21.4670105 8.77538395 -35.15098572 -21.4670105 14.9582243 -38.84901428 -21.4670105 14.9582243
		 -38.84901428 -21.4670105 8.7753849 -39.87148285 -21.47247696 8.77538395 -38.77819061 -16.08272934 20.080453873
		 -38.79683304 -17.37827492 20.080453873 -37.82047653 -18.35463142 20.080453873 -36.17952728 -18.35463142 20.080453873
		 -35.20316696 -17.37827492 20.080453873 -35.22180939 -16.082731247 20.080453873 -36.21712875 -15.12571239 20.080453873
		 -37.78287125 -15.12571239 20.080453873 -35.52418137 -21.46652412 14.9582243 -38.47582245 -21.46652412 14.9582243
		 -37.72236633 -22.6341114 14.9582243 -36.27763748 -22.6341114 14.9582243 -37.74568939 -20.29893684 14.9582243
		 -36.25431061 -20.29893684 14.9582243 -38.77819061 -16.08272934 14.83047676 -38.79683304 -17.37827492 14.83047676
		 -37.82047653 -18.35463142 14.83047676 -36.17952728 -18.35463142 14.83047676 -35.20316696 -17.37827492 14.83047676
		 -35.22180939 -16.082731247 14.83047676 -36.21712875 -15.12571239 14.83047676 -37.78287125 -15.12571239 14.83047676
		 -35.52418137 -21.46652412 9.70824623 -38.47582245 -21.46652412 9.70824623 -37.72236633 -22.6341114 9.70824623
		 -36.27763748 -22.6341114 9.70824623 -37.74568939 -20.29893684 9.70824623 -36.25431061 -20.29893684 9.70824623
		 -38.94519424 -27.24691582 0.6156249 -35.054801941 -27.24691582 0.61562395 -35.054801941 -27.24691582 8.77538395
		 -38.94519424 -27.24691582 8.7753849;
	setAttr -s 277 ".ed";
	setAttr ".ed[0:165]"  1 4 0 1 0 0 5 4 0 8 13 0 9 0 0 8 9 0 12 5 0 13 12 0
		 1 3 0 3 2 1 2 0 0 5 7 0 7 6 1 6 4 0 9 11 0 11 10 1 10 8 0 13 15 0 15 14 1 14 12 0
		 10 15 1 6 3 0 14 7 1 2 11 1 8 16 1 9 17 1 16 17 1 13 18 1 16 18 1 12 19 1 18 19 1
		 5 20 1 19 20 1 4 21 1 20 21 1 1 22 1 22 21 1 0 23 0 22 23 1 17 23 1 24 25 0 24 26 0
		 26 27 0 27 28 0 28 29 0 30 29 0 30 31 0 25 31 0 32 33 0 32 34 0 34 35 0 33 35 0 32 36 0
		 36 37 0 34 37 0 36 38 0 38 39 0 37 39 0 38 40 0 40 41 0 39 41 0 40 42 0 42 43 0 41 43 0
		 44 42 0 44 45 0 45 43 0 44 46 0 46 47 0 45 47 0 33 46 0 35 47 0 34 24 0 35 25 0 37 26 0
		 39 27 0 41 28 0 43 29 0 45 30 0 47 31 0 16 48 1 17 49 1 48 49 0 32 50 1 48 50 0 33 51 1
		 50 51 0 49 51 0 18 52 1 48 52 0 36 53 1 52 53 0 50 53 0 19 54 1 52 54 0 38 55 1 54 55 0
		 53 55 0 20 56 1 54 56 0 40 57 1 56 57 0 55 57 0 21 58 1 56 58 0 42 59 1 58 59 0 57 59 0
		 22 60 1 60 58 0 44 61 1 60 61 0 61 59 0 23 62 0 60 62 0 46 63 0 62 63 0 61 63 0 49 62 0
		 51 63 0 65 64 0 65 3 0 11 66 0 2 67 0 66 67 1 7 68 0 67 68 0 14 69 0 68 69 1 15 70 0
		 70 69 1 10 71 0 70 71 1 71 66 1 65 72 0 72 109 0 64 73 0 72 73 1 68 104 0 66 74 0
		 67 75 0 74 75 0 68 76 0 75 76 0 69 77 0 76 77 0 70 78 0 78 77 0 71 79 0 78 79 0 79 74 0
		 74 80 1 80 84 0 77 81 1 67 102 1 68 96 1 72 98 1 73 100 1 80 82 0 82 87 0 81 83 0
		 85 88 0 75 84 1 85 75 1 86 91 0 87 86 0;
	setAttr ".ed[166:276]" 89 81 0 76 88 1 89 76 1 90 83 0 91 90 0 92 94 0 92 78 1
		 93 95 0 83 93 0 79 94 1 95 82 0 84 87 0 86 85 0 88 91 0 90 89 0 93 92 0 94 95 0 84 85 0
		 88 89 0 81 92 0 94 80 0 96 105 0 97 106 0 98 108 0 98 100 0 99 101 0 99 107 0 96 97 0
		 99 98 0 101 100 0 102 96 0 103 97 0 103 102 0 6 64 0 104 73 0 105 100 0 106 101 0
		 107 103 0 108 102 0 109 67 0 6 104 0 104 105 0 105 106 0 107 108 0 108 109 0 109 3 0
		 82 110 1 87 111 1 110 111 0 86 112 1 111 112 0 91 113 1 112 113 0 90 114 1 113 114 0
		 83 115 1 114 115 0 93 116 1 115 116 0 95 117 1 116 117 0 117 110 0 106 118 0 107 119 0
		 99 120 1 120 119 0 101 121 1 120 121 0 118 121 0 103 122 1 97 123 1 122 123 0 119 122 0
		 123 118 0 110 124 0 111 125 0 124 125 0 112 126 0 125 126 0 113 127 0 126 127 0 114 128 0
		 127 128 0 115 129 0 128 129 0 116 130 0 129 130 0 117 131 0 130 131 0 131 124 0 118 132 0
		 119 133 0 132 133 0 120 134 0 134 133 0 121 135 0 134 135 0 132 135 0 122 136 0 123 137 0
		 136 137 0 133 136 0 137 132 0 65 138 0 64 139 0 138 139 0 73 140 0 139 140 0 72 141 0
		 141 140 0 138 141 0;
	setAttr -s 137 -ch 554 ".fc[0:136]" -type "polyFaces" 
		f 8 -41 41 42 43 44 -46 46 -48
		mu 0 8 40 41 42 43 44 45 46 47
		f 4 -2 8 9 10
		mu 0 4 1 2 3 4
		f 4 -3 11 12 13
		mu 0 4 14 5 6 15
		f 4 5 14 15 16
		mu 0 4 13 7 8 12
		f 4 -8 17 18 19
		mu 0 4 0 9 10 11
		f 4 20 -18 -4 -17
		mu 0 4 12 10 9 13
		f 4 -9 0 -14 21
		mu 0 4 3 2 14 15
		f 4 22 -12 -7 -20
		mu 0 4 16 17 18 19
		f 4 -11 23 -15 4
		mu 0 4 20 21 22 23
		f 4 -6 24 26 -26
		mu 0 4 7 13 25 24
		f 4 3 27 -29 -25
		mu 0 4 13 9 26 25
		f 4 7 29 -31 -28
		mu 0 4 9 0 27 26
		f 4 6 31 -33 -30
		mu 0 4 0 5 28 27
		f 4 2 33 -35 -32
		mu 0 4 5 14 29 28
		f 4 -1 35 36 -34
		mu 0 4 14 2 30 29
		f 4 1 37 -39 -36
		mu 0 4 2 1 31 30
		f 4 -5 25 39 -38
		mu 0 4 1 7 24 31
		f 4 -49 49 50 -52
		mu 0 4 48 49 33 32
		f 4 52 53 -55 -50
		mu 0 4 49 50 34 33
		f 4 55 56 -58 -54
		mu 0 4 50 51 35 34
		f 4 58 59 -61 -57
		mu 0 4 51 52 36 35
		f 4 61 62 -64 -60
		mu 0 4 52 53 37 36
		f 4 -65 65 66 -63
		mu 0 4 53 54 38 37
		f 4 67 68 -70 -66
		mu 0 4 54 55 39 38
		f 4 -71 51 71 -69
		mu 0 4 55 48 32 39
		f 4 -51 72 40 -74
		mu 0 4 32 33 41 40
		f 4 54 74 -42 -73
		mu 0 4 33 34 42 41
		f 4 57 75 -43 -75
		mu 0 4 34 35 43 42
		f 4 60 76 -44 -76
		mu 0 4 35 36 44 43
		f 4 63 77 -45 -77
		mu 0 4 36 37 45 44
		f 4 -67 78 45 -78
		mu 0 4 37 38 46 45
		f 4 69 79 -47 -79
		mu 0 4 38 39 47 46
		f 4 -72 73 47 -80
		mu 0 4 39 32 40 47
		f 4 -83 84 86 -88
		mu 0 4 56 57 58 59
		f 4 89 91 -93 -85
		mu 0 4 57 60 61 58
		f 4 94 96 -98 -92
		mu 0 4 60 62 63 61
		f 4 99 101 -103 -97
		mu 0 4 62 64 65 63
		f 4 104 106 -108 -102
		mu 0 4 64 66 67 65
		f 4 -110 111 112 -107
		mu 0 4 66 68 69 67
		f 4 114 116 -118 -112
		mu 0 4 68 70 71 69
		f 4 -119 87 119 -117
		mu 0 4 70 56 59 71
		f 4 -27 80 82 -82
		mu 0 4 24 25 57 56
		f 4 48 85 -87 -84
		mu 0 4 49 48 59 58
		f 4 28 88 -90 -81
		mu 0 4 25 26 60 57
		f 4 -53 83 92 -91
		mu 0 4 50 49 58 61
		f 4 30 93 -95 -89
		mu 0 4 26 27 62 60
		f 4 -56 90 97 -96
		mu 0 4 51 50 61 63
		f 4 32 98 -100 -94
		mu 0 4 27 28 64 62
		f 4 -59 95 102 -101
		mu 0 4 52 51 63 65
		f 4 34 103 -105 -99
		mu 0 4 28 29 66 64
		f 4 -62 100 107 -106
		mu 0 4 53 52 65 67
		f 4 -37 108 109 -104
		mu 0 4 29 30 68 66
		f 4 64 105 -113 -111
		mu 0 4 54 53 67 69
		f 4 38 113 -115 -109
		mu 0 4 30 31 70 68
		f 4 -68 110 117 -116
		mu 0 4 55 54 69 71
		f 4 -40 81 118 -114
		mu 0 4 31 24 56 70
		f 4 70 115 -120 -86
		mu 0 4 48 55 71 59
		f 8 242 244 246 248 250 252 254 255
		mu 0 8 140 141 142 143 144 145 146 147
		f 4 -24 123 -125 -123
		mu 0 4 72 79 111 80
		f 4 -23 127 -129 -126
		mu 0 4 76 75 81 107
		f 4 -19 129 130 -128
		mu 0 4 75 74 82 81
		f 4 -21 131 -133 -130
		mu 0 4 74 73 83 82
		f 4 -16 122 -134 -132
		mu 0 4 73 72 80 83
		f 4 211 -122 134 135
		mu 0 4 124 125 78 112
		f 4 271 273 -276 -277
		mu 0 4 154 155 156 157
		f 4 -200 206 200 -137
		mu 0 4 77 118 119 106
		f 4 124 140 -142 -140
		mu 0 4 80 111 84 89
		f 4 126 142 -144 -141
		mu 0 4 111 107 85 84
		f 4 128 144 -146 -143
		mu 0 4 107 81 86 85
		f 4 -131 146 147 -145
		mu 0 4 81 82 87 86
		f 4 132 148 -150 -147
		mu 0 4 82 83 88 87
		f 4 133 139 -151 -149
		mu 0 4 83 80 89 88
		f 4 141 162 -153 -152
		mu 0 4 89 84 97 90
		f 4 143 167 -162 163
		mu 0 4 84 85 99 103
		f 4 145 153 -167 168
		mu 0 4 85 86 94 104
		f 4 -154 -148 -173 -186
		mu 0 4 94 86 87 105
		f 4 149 175 -172 172
		mu 0 4 87 88 101 105
		f 4 150 151 -187 -176
		mu 0 4 88 89 90 101
		f 4 -127 154 196 -156
		mu 0 4 107 111 117 114
		f 4 210 -136 156 189
		mu 0 4 123 124 112 116
		f 4 137 157 -191 -157
		mu 0 4 112 106 110 116
		f 4 -201 207 201 -158
		mu 0 4 106 119 120 110
		f 4 152 177 -160 -159
		mu 0 4 90 97 98 96
		f 4 178 161 179 -165
		mu 0 4 92 103 99 100
		f 4 180 166 160 -170
		mu 0 4 93 104 94 91
		f 4 181 171 182 -174
		mu 0 4 95 105 101 102
		f 4 -194 -197 -199 197
		mu 0 4 113 114 117 115
		f 4 209 -190 -195 192
		mu 0 4 122 123 116 108
		f 4 194 190 -196 -192
		mu 0 4 108 116 110 109
		f 4 195 -202 208 202
		mu 0 4 109 110 120 121
		f 4 183 -179 -166 -178
		mu 0 4 97 103 92 98
		f 4 184 -181 -171 -180
		mu 0 4 99 104 93 100
		f 4 185 -182 -175 -161
		mu 0 4 94 105 95 91
		f 4 186 158 -177 -183
		mu 0 4 101 90 96 102
		f 3 -163 -164 -184
		mu 0 3 97 84 103
		f 3 -168 -169 -185
		mu 0 3 99 85 104
		f 4 258 -261 262 -264
		mu 0 4 151 148 149 150
		f 4 -207 -13 125 138
		mu 0 4 119 118 76 107
		f 4 -208 -139 155 187
		mu 0 4 120 119 107 114
		f 4 -209 -188 193 188
		mu 0 4 121 120 114 113
		f 4 -267 -268 -259 -269
		mu 0 4 152 153 148 151
		f 4 198 -205 -210 203
		mu 0 4 115 117 123 122
		f 4 -206 -211 204 -155
		mu 0 4 111 124 123 117
		f 4 -10 -212 205 -124
		mu 0 4 79 125 124 111
		f 4 159 213 -215 -213
		mu 0 4 96 98 127 126
		f 4 165 215 -217 -214
		mu 0 4 98 92 128 127
		f 4 164 217 -219 -216
		mu 0 4 92 100 129 128
		f 4 170 219 -221 -218
		mu 0 4 100 93 130 129
		f 4 169 221 -223 -220
		mu 0 4 93 91 131 130
		f 4 174 223 -225 -222
		mu 0 4 91 95 132 131
		f 4 173 225 -227 -224
		mu 0 4 95 102 133 132
		f 4 176 212 -228 -226
		mu 0 4 102 96 126 133
		f 4 -193 230 231 -230
		mu 0 4 122 108 135 134
		f 4 191 232 -234 -231
		mu 0 4 108 109 136 135
		f 4 -203 228 234 -233
		mu 0 4 109 121 137 136
		f 4 -198 235 237 -237
		mu 0 4 113 115 139 138
		f 4 -204 229 238 -236
		mu 0 4 115 122 134 139
		f 4 -189 236 239 -229
		mu 0 4 121 113 138 137
		f 4 214 241 -243 -241
		mu 0 4 126 127 141 140
		f 4 216 243 -245 -242
		mu 0 4 127 128 142 141
		f 4 218 245 -247 -244
		mu 0 4 128 129 143 142
		f 4 220 247 -249 -246
		mu 0 4 129 130 144 143
		f 4 222 249 -251 -248
		mu 0 4 130 131 145 144
		f 4 224 251 -253 -250
		mu 0 4 131 132 146 145
		f 4 226 253 -255 -252
		mu 0 4 132 133 147 146
		f 4 227 240 -256 -254
		mu 0 4 133 126 140 147
		f 4 -232 259 260 -258
		mu 0 4 134 135 149 148
		f 4 233 261 -263 -260
		mu 0 4 135 136 150 149
		f 4 -235 256 263 -262
		mu 0 4 136 137 151 150
		f 4 -238 264 266 -266
		mu 0 4 138 139 153 152
		f 4 -239 257 267 -265
		mu 0 4 139 134 148 153
		f 4 -240 265 268 -257
		mu 0 4 137 138 152 151
		f 4 -22 199 -121 121
		mu 0 4 3 118 77 78
		f 4 120 270 -272 -270
		mu 0 4 78 77 155 154
		f 4 136 272 -274 -271
		mu 0 4 77 106 156 155
		f 4 -138 274 275 -273
		mu 0 4 106 112 157 156
		f 4 -135 269 276 -275
		mu 0 4 112 78 154 157;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape3" -p "lowerArmRightGun";
	rename -uid "7E80A09F-4EF1-A453-9EE7-449D71F05453";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cubic_Left_Arm";
	rename -uid "92041E3B-4226-31AA-9733-E49D916D956C";
	setAttr ".t" -type "double3" 16.999999999999996 33 0 ;
	setAttr ".rp" -type "double3" 18 -7.5 4 ;
	setAttr ".sp" -type "double3" 18 -7.5 4 ;
createNode transform -n "armLeft1" -p "Cubic_Left_Arm";
	rename -uid "5B9DCD14-44F5-1C38-B310-848478A8BC51";
	setAttr ".rp" -type "double3" 18 2.5 0 ;
	setAttr ".sp" -type "double3" 18 2.5 0 ;
createNode mesh -n "armLeftShape1" -p "|Cubic_Left_Arm|armLeft1";
	rename -uid "81D8B622-4F8B-CA6E-83C7-A8B8D3C6292C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  12 -2 6 24 -2 6 12 7 6 24 7 6 12 7 -6 24 7 -6
		 12 -2 -6 24 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmLeft" -p "Cubic_Left_Arm";
	rename -uid "C6D45555-4ECD-AE23-5DFF-57999D3DE5C9";
	setAttr ".rp" -type "double3" 18 -11 0 ;
	setAttr ".sp" -type "double3" 18 -11 0 ;
createNode mesh -n "upperArmLeftShape" -p "|Cubic_Left_Arm|upperArmLeft";
	rename -uid "71DFA954-4703-85CD-B8BB-4197C1882155";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  36 -7.1054274e-015 2.220446e-015 
		36 -7.1054274e-015 2.220446e-015 36 -7.1054274e-015 2.220446e-015 36 -7.1054274e-015 
		2.220446e-015 36 -7.1054274e-015 2.220446e-015 36 -7.1054274e-015 2.220446e-015 36 
		-7.1054274e-015 2.220446e-015 36 -7.1054274e-015 2.220446e-015;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmLeft" -p "Cubic_Left_Arm";
	rename -uid "04357830-4F2C-6711-B217-C79CEBAE082A";
	setAttr ".rp" -type "double3" 18 -18 4 ;
	setAttr ".sp" -type "double3" 18 -18 4 ;
createNode mesh -n "lowerArmLeftShape" -p "|Cubic_Left_Arm|lowerArmLeft";
	rename -uid "852181E5-44AA-1FAB-EC0F-EAA6DC33CFFB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  36 3.5527137e-015 1.7763568e-015 
		36 3.5527137e-015 1.7763568e-015 36 3.5527137e-015 1.7763568e-015 36 3.5527137e-015 
		1.7763568e-015 36 3.5527137e-015 1.7763568e-015 36 3.5527137e-015 1.7763568e-015 
		36 3.5527137e-015 1.7763568e-015 36 3.5527137e-015 1.7763568e-015;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armLeft2" -p "Cubic_Left_Arm";
	rename -uid "AF3BF46A-42C5-3C23-11AA-FFA31517B656";
	setAttr ".rp" -type "double3" 18 -3.5 0 ;
	setAttr ".sp" -type "double3" 18 -3.5 0 ;
createNode mesh -n "armLeftShape2" -p "|Cubic_Left_Arm|armLeft2";
	rename -uid "AC56E6E0-4F2C-247D-F832-5ABE3754A6C1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  13 -5 5 23 -5 5 13 -2 5 23 -2 5 13 -2 -5
		 23 -2 -5 13 -5 -5 23 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "330CDD03-4486-8BA3-8E28-2F9A7D7108A5";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "EB5F7D34-40CB-5C40-BC7F-0FB1330D385B";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8BF7B8F2-41AA-7ADC-287A-7E85EA77E1C1";
createNode displayLayerManager -n "layerManager";
	rename -uid "DE9F92FD-4CF9-2117-59E8-819A5F68D05A";
	setAttr -s 3 ".dli";
	setAttr ".dli[3]" 1;
	setAttr ".dli[4]" 2;
createNode displayLayer -n "defaultLayer";
	rename -uid "4085AE31-4CEA-0975-1FB7-E181F8E20DAE";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "86F60DDA-4484-BC9F-89F3-01A3546BC0D0";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "1D0F1E37-4A8A-A123-D6A9-679337D5783E";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "5CB01404-49E4-D27F-80CC-5590DD35CDFF";
	setAttr ".w" 10;
	setAttr ".h" 8;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube2";
	rename -uid "80215462-4720-5811-BDBF-3CABF39A8D0D";
	setAttr ".w" 6;
	setAttr ".h" 8;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube3";
	rename -uid "485774BA-41D5-6B47-1A0B-2D9BCAEA4729";
	setAttr ".w" 7;
	setAttr ".h" 6;
	setAttr ".d" 7;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "54C76ABA-4E87-6A38-5E51-09801C8F20AE";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1 0 1;
createNode polyCube -n "polyCube4";
	rename -uid "8D8EA23B-4BD4-C7A9-6418-8BB72A958F8A";
	setAttr ".w" 8;
	setAttr ".h" 5;
	setAttr ".d" 5;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry2";
	rename -uid "2667C77D-4AE1-F49D-EB9A-B89717893460";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 0 0 1;
createNode transformGeometry -n "transformGeometry3";
	rename -uid "CBBB4418-47D3-B1D5-C7F1-B88061D1E0DC";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 -8 -0.5 1;
createNode transformGeometry -n "transformGeometry4";
	rename -uid "1581101F-4397-622B-69AB-43A374D0BF73";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.70710678118654746 0.70710678118654768 0
		 0 -0.70710678118654768 0.70710678118654746 0 8 -5 -2 1;
createNode polyCube -n "polyCube5";
	rename -uid "254C7E88-43BD-5F96-FE35-E5BABE340230";
	setAttr ".w" 10;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube6";
	rename -uid "F9C7DB55-488F-B8D7-CF89-0D81FA44309D";
	setAttr ".w" 7;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "3E1FE795-4A57-0096-2DC6-70B3AE2BE52A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 453\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 452\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 453\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1112\n            -height 710\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n"
		+ "            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n"
		+ "                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n"
		+ "\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1112\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1112\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D359CC2C-4330-3F25-4DC2-51AEBFAC6971";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 120 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube7";
	rename -uid "559D9D51-47BD-434F-417F-9FAC6505AF5B";
	setAttr ".w" 20;
	setAttr ".h" 4;
	setAttr ".d" 18;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry5";
	rename -uid "E9E077C0-4E29-FA31-7748-D19AC1391C4C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry6";
	rename -uid "EC9EFFAE-46F3-4480-4BB4-D68D307AACDF";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry7";
	rename -uid "E2F4CE49-4274-17CB-7130-C7AE84E9341C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry8";
	rename -uid "6631E2D1-4860-AEF4-DB1C-7098D054CA4A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry9";
	rename -uid "1163723F-4DD1-B842-E152-5CAC6D624D8C";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 -2.7755575615628914e-017 1 0
		 -0.26976023601264199 0.96292752326766717 5.5511151231257827e-017 0 -0.96292752326766728 -0.26976023601264193 1.1102230246251565e-016 0
		 12 -20 0 1;
createNode transformGeometry -n "transformGeometry10";
	rename -uid "36814056-4C2E-DE67-205C-B98EC7CE1DB7";
	setAttr ".txf" -type "matrix" 0.88294759285892688 -1.3877787807814457e-016 -0.46947156278589086 0
		 -0.1266447595783457 0.96292752326766717 -0.23818415103641816 0 0.45206708919801941 0.26976023601264182 0.85021453876679487 0
		 8.6269065389189556 -20.005126449443157 7.0516877289946809 1;
createNode polyCube -n "polyCube8";
	rename -uid "6738B210-4080-16FE-2F51-0AA44FB0D2B0";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube9";
	rename -uid "C40BE38B-4975-7B08-D713-59897C1E67F0";
	setAttr ".w" 13;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube10";
	rename -uid "398A8CF3-423D-51BB-DFD7-16A4761E016E";
	setAttr ".w" 14;
	setAttr ".h" 10;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak1";
	rename -uid "C313D164-4FA5-DC5A-DFFE-60BC6EF213E9";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  7 -5 -1 7 -5 -1 7 -5 -1 7
		 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1;
createNode transformGeometry -n "transformGeometry11";
	rename -uid "1431F1F0-4C93-7C38-38B9-92BD65356955";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 0.080198924328858931 0
		 0 -0.080198924328858931 0.99677887845624713 0 -1 -3.0499999999999949 0.79999999999998117 1;
createNode transformGeometry -n "transformGeometry12";
	rename -uid "60349C85-4613-42DE-5B69-56B668AF13BA";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018336 0 0 1 0 0
		 0.38461756040018336 0 0.92307601649691418 0 0.99999999999999778 0 13 1;
createNode polyCube -n "polyCube11";
	rename -uid "ED30530E-4D51-D1A7-9F47-92AD1D4F4E58";
	setAttr ".w" 7;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube12";
	rename -uid "9A96DE03-43F3-3B18-9C72-418ED6F3B382";
	setAttr ".w" 14;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak2";
	rename -uid "33334318-4531-A1A4-0802-7CACC4E28DA7";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093;
createNode transformGeometry -n "transformGeometry13";
	rename -uid "B0B2AE7A-4047-F186-D1F6-F599466439B3";
	setAttr ".txf" -type "matrix" 0.85206933223181147 -0.41082766406220717 0.32434315702851696 0
		 0.38461756040018319 0.91171141897700703 0.14440090283216203 0 -0.35503124552896226 0.0017084929389307166 0.93485287385236782 0
		 -1.3 0.015643446504023089 -0.098768834059513783 1;
createNode polyCube -n "polyCube13";
	rename -uid "9563A64B-43D0-45BF-39B9-E6A5C9C06430";
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube14";
	rename -uid "EF19D4A1-4F7E-B8CD-8282-388E4678AAE7";
	setAttr ".w" 13;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak3";
	rename -uid "6D576173-45BA-8565-1798-84B6B017B978";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -3.5 -0.5 6.5 -3.5 -0.5
		 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5;
createNode transformGeometry -n "transformGeometry14";
	rename -uid "73F384E1-440B-A631-EE57-2D97854B1320";
	setAttr ".txf" -type "matrix" 0.96126169593831889 0 -0.27563735581699905 0 0 1 0 0
		 0.27563735581699905 0 0.96126169593831889 0 0.5 -12.499999999999998 11.5 1;
createNode polyCube -n "polyCube15";
	rename -uid "1888269C-410C-44B7-4915-4ABB8EDA545D";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry15";
	rename -uid "87CD74CE-4C81-39F2-7622-37ABB979F889";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 0 -1 0 0 1 0 0 1 0 2.2204460492503131e-016 0
		 11.499999999999979 -7.5 -0.99999999999999012 1;
createNode polyCube -n "polyCube16";
	rename -uid "6EE74D4C-4E8E-AA70-C5C7-5C8BC768DB57";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube17";
	rename -uid "8878502D-46DA-DDA8-1039-B7A7CD249DD4";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry16";
	rename -uid "4C0842CC-4A13-91C5-95FB-49AA6B865820";
	setAttr ".txf" -type "matrix" 0.94865541582805746 -0.31631140039539413 0 0 0.31631140039539413 0.94865541582805746 0 0
		 0 0 1 0 12.525672292085968 -14.841844299802286 -4 1;
createNode polyCube -n "polyCube18";
	rename -uid "EECFD6F9-4BD1-ACB9-F770-6C938581FF5A";
	setAttr ".w" 8;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube19";
	rename -uid "832ECCF9-41FE-590A-C000-199E07CA5835";
	setAttr ".h" 2;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube20";
	rename -uid "E381CF92-4DBB-5E80-EEE7-75B9B372EC9E";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube21";
	rename -uid "F962A912-4E24-4930-03AF-A68648612EE2";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube22";
	rename -uid "B892D7B7-4364-18F0-F668-B680F093092C";
	setAttr ".w" 5;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube23";
	rename -uid "9CFE9CB5-48DE-C5EF-AAC1-0C8AE95CE1EB";
	setAttr ".w" 6;
	setAttr ".h" 0.01;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry17";
	rename -uid "BB4F6B63-4859-0B98-652A-678E093BA62F";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4 -0.5 -1 1;
createNode transformGeometry -n "transformGeometry18";
	rename -uid "0EB95C3C-49D2-7B4E-B5B5-C28661B0243D";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 -2 -1 1;
createNode transformGeometry -n "transformGeometry19";
	rename -uid "5746B87C-4BF9-75CB-795B-EAB11D3E005E";
	setAttr ".txf" -type "matrix" 0.79999892814850848 -0.60000142913266252 0 0 0.60000142913266252 0.79999892814850848 0 0
		 0 0 1 0 2.2999980349376026 -4.1000041087574015 -1 1;
createNode transformGeometry -n "transformGeometry20";
	rename -uid "D1E40504-4CF2-BF08-21AB-188785E56B1B";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 1 0 5.999999999999992 -5.4999999999999991 -1 1;
createNode transformGeometry -n "transformGeometry21";
	rename -uid "BD7A6EFF-4F93-AF13-B36D-CE840CFC740E";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 7.5 -3 -1 1;
createNode transformGeometry -n "transformGeometry22";
	rename -uid "606417B4-49C7-E882-F47A-6ABF96B731AD";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.86602540378443871 0.49999999999999994 0
		 0 -0.49999999999999994 0.86602540378443871 0 4 -1.25 -1 1;
createNode transformGeometry -n "transformGeometry23";
	rename -uid "29400EF9-4901-32F6-5A09-CF8C6B014F7D";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -17 -1 1;
createNode polyTweak -n "polyTweak4";
	rename -uid "29CDD8A8-4D14-BEF2-2931-BCA13CBF62C2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[0:3]" -type "float3"  -1 0 0 1 0 0 -1 0 0 1 0 0;
createNode transformGeometry -n "transformGeometry24";
	rename -uid "D72E8F73-4A5F-98E7-E544-84B95DC0C4D9";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -7.5 -11.5 1;
createNode transformGeometry -n "transformGeometry25";
	rename -uid "828F6D3A-4037-5063-5122-58BFD6251C9C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1.5 11.5 1;
createNode polyTweak -n "polyTweak5";
	rename -uid "8B3D4378-48B5-81F5-78E3-438A83A3390F";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -1.5 -1.5 6.5 -1.5 -1.5
		 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5;
createNode transformGeometry -n "transformGeometry26";
	rename -uid "5596D9A9-47D5-B5B8-AF0E-47B82DB0EB97";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018324 0 0 1 0 0
		 0.38461756040018324 0 0.92307601649691418 0 0.99999999999999978 0 13 1;
createNode transformGeometry -n "transformGeometry27";
	rename -uid "2BDC3B3E-486C-863E-3B6D-4BACA1607B15";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 0 1 0 0 -1.1102230246251565e-016 0 1 0
		 -2.2204460492503131e-016 0 0 1;
createNode polyTweak -n "polyTweak6";
	rename -uid "CCB74E53-47F8-F2CD-9D28-1A9C9595D025";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -2.5 -1.5 0 -2.5 -1.5 0
		 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5;
createNode transformGeometry -n "transformGeometry28";
	rename -uid "B6511720-4F37-D088-9FB2-898C38D27F54";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.98768834059513777 0.15643446504023087 0
		 0 -0.15643446504023087 0.98768834059513777 0 0 -18.699999999999999 12.699999999999999 1;
createNode transformGeometry -n "transformGeometry29";
	rename -uid "392882F0-46CD-F60F-33D8-1091A26B9F95";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -18.699999999999999 12.699999999999999 1;
createNode polyTweak -n "polyTweak7";
	rename -uid "E614DEAC-42E3-53CE-3F64-A9A09A54DF8D";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -3.5 0 0 -3.5 0 0 -3.5 0
		 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0;
createNode transformGeometry -n "transformGeometry30";
	rename -uid "CFD69E13-4F33-EC74-B5CA-828583EBD4D5";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -12.499999999999998 11 1;
createNode transformGeometry -n "transformGeometry31";
	rename -uid "9D765ED2-472F-4D44-5389-D992D179610E";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 0.38461756040018324 0 0 1 0 0
		 -0.38461756040018324 0 0.92307601649691418 0 5.4615702909401849 0 0.93847259198938104 1;
createNode transformGeometry -n "transformGeometry32";
	rename -uid "30D32989-491A-A765-150A-D59ABBA844EA";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry33";
	rename -uid "A44105B6-4D94-3481-B4E6-DA9E268AC258";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry34";
	rename -uid "86ECE2E6-42DD-4A14-B968-5DB94CB5DAAA";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry35";
	rename -uid "CA99D8E4-4C1F-C762-EB58-C0A971D8BDD0";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry36";
	rename -uid "D9DBB557-4A6A-D709-1CAD-BFA4E9268A76";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry37";
	rename -uid "40BBD014-4BD5-6672-ED4E-6EA1F89B2E28";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry38";
	rename -uid "D49FA4B9-4AEE-8A36-7EF0-C1934BE46969";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry39";
	rename -uid "46E3DB6E-4F9A-DF0B-27A1-C6B62C6C6F59";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry40";
	rename -uid "39E81AFD-4D23-D118-9C41-84AA1FFC680C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry41";
	rename -uid "1E72E26A-4F3B-B7A2-DE2B-799468144F8A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry42";
	rename -uid "ED701055-4E8B-F5DA-13FE-AF8555FB8F1A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry43";
	rename -uid "F02C6DA8-43F2-9347-5C73-B6AC1CC237E8";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry44";
	rename -uid "D7D4F962-4421-8E33-31E6-26A6E0CC6737";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry45";
	rename -uid "D0660701-4415-4B05-4EA3-CF881D9BAEC0";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.030845914622084619 0.99677887845624713 0.074029703596820554 0
		 0.38337866049027253 -0.080198924328858931 0.92010267645365451 0 -5.2908204681950295 1.1228312710267943 1.5021176062717214 1;
createNode transformGeometry -n "transformGeometry46";
	rename -uid "7AAA0379-4F05-C997-78D0-8685D3BE0528";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry47";
	rename -uid "88D074D8-4F7C-47D8-A486-63B28662117D";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry48";
	rename -uid "3372E96A-40CB-8480-842B-DFAD7DD3B53B";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry49";
	rename -uid "0ED7BCFF-4418-1AAC-754A-5EBD6EE84A24";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry50";
	rename -uid "C436AC16-4450-C1B8-AFDF-36B65C240D06";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry51";
	rename -uid "BBEAF9E9-48AC-B1EA-F009-FFA53D405F35";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode polyCube -n "polyCube24";
	rename -uid "A240EF2B-40E3-C65B-678C-08B4636D5C21";
	setAttr ".w" 12;
	setAttr ".h" 9;
	setAttr ".d" 12;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube25";
	rename -uid "70CE1749-418E-FEB1-3E7C-9ABD21534D0A";
	setAttr ".w" 10;
	setAttr ".h" 3;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube26";
	rename -uid "E508D78D-41CD-B69F-13C2-B7B42A8CF99A";
	setAttr ".w" 6;
	setAttr ".h" 12;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube27";
	rename -uid "81205D2E-47AE-F32A-A780-748093631382";
	setAttr ".w" 8;
	setAttr ".h" 8;
	setAttr ".d" 22;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak8";
	rename -uid "3F94CF60-4EE7-A771-A1EA-39823B5FC1FB";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  8.8817842e-016 1.5 0 8.8817842e-016
		 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016
		 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0;
createNode transformGeometry -n "transformGeometry52";
	rename -uid "E2491441-426A-2AC1-9FB0-ACA329B64218";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 1 0 1;
createNode transformGeometry -n "transformGeometry53";
	rename -uid "6AC1074D-4D5E-FAE8-9CD2-F5893AE64E3A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -11 0 1;
createNode transformGeometry -n "transformGeometry54";
	rename -uid "82FC2D08-4756-0F3A-2D90-5FB6B8AAF1FD";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -18 4 1;
createNode transformGeometry -n "transformGeometry55";
	rename -uid "E06C9A47-4190-ABEB-0F3C-59854B67D17A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -3.5 0 1;
createNode transformGeometry -n "transformGeometry56";
	rename -uid "4F66D127-4A26-B67E-C222-C78A3D8D1396";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -1 1;
createNode transformGeometry -n "transformGeometry57";
	rename -uid "16CB2CEE-4C7E-B65F-8641-95A6FEC5765A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -1 1;
createNode displayLayer -n "layer1";
	rename -uid "C9D462A5-4D4A-DC2B-E7F9-CBB0766BF300";
	setAttr ".v" no;
	setAttr ".do" 1;
createNode displayLayer -n "layer2";
	rename -uid "F09E0FF1-4746-6336-25B0-05B9D4DB9C8A";
	setAttr ".do" 2;
createNode polyPlane -n "polyPlane1";
	rename -uid "3877D582-420C-5279-E319-D4AB843EA620";
	setAttr ".w" 100;
	setAttr ".h" 100;
	setAttr ".cuv" 2;
createNode groupId -n "groupId62";
	rename -uid "1BB08FC0-4F94-EF99-B1F3-96A24F0CD473";
	setAttr ".ihi" 0;
createNode groupId -n "groupId63";
	rename -uid "70CD433E-4238-9B3A-C512-46BE0C8F9AA7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId66";
	rename -uid "43C9886F-44EE-C9C2-6109-4C9AAA0438FF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId67";
	rename -uid "85F2AA2E-45BB-F18C-1DD7-46B16D980F03";
	setAttr ".ihi" 0;
createNode skinCluster -n "skinCluster1";
	rename -uid "7D16C33E-4ED7-807E-A447-15AE5C290250";
	setAttr ".skm" 2;
	setAttr -s 16 ".wl";
	setAttr ".wl[0].w[1]"  1;
	setAttr ".wl[1].w[1]"  1;
	setAttr ".wl[2].w[1]"  1;
	setAttr ".wl[3].w[1]"  1;
	setAttr ".wl[4].w[1]"  1;
	setAttr ".wl[5].w[1]"  1;
	setAttr ".wl[6].w[1]"  1;
	setAttr ".wl[7].w[1]"  1;
	setAttr ".wl[8].w[1]"  1;
	setAttr ".wl[9].w[1]"  1;
	setAttr ".wl[10].w[1]"  1;
	setAttr ".wl[11].w[1]"  1;
	setAttr ".wl[12].w[1]"  1;
	setAttr ".wl[13].w[1]"  1;
	setAttr ".wl[14].w[1]"  1;
	setAttr ".wl[15].w[1]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 3.0114813836638388e-015 2.2204460492503175e-016 -1 0
		 -1 1.110223024625156e-016 -2.7755575615629569e-016 0 -5.5929053192526625e-031 1 3.3306690738754696e-016 0
		 0.99999999999993427 -6.9607119365214783e-015 18.000000000000025 1;
	setAttr ".pm[1]" -type "matrix" 1.8364233684265028e-016 1.8665171476922902e-016 -1 0
		 -0.97618706018395285 0.21693045781865569 7.2164496600634623e-016 0 0.21693045781865558 0.97618706018395285 3.3306690738754588e-016 0
		 -4.8809353009197816 1.0846522890932735 18.000000000000028 1;
	setAttr ".pm[2]" -type "matrix" 3.9662527105997003e-016 5.9600907888582843e-017 -1.000000000000002 0
		 -0.6246950475544214 0.78086880944303083 2.72004641033163e-015 0 0.78086880944303283 0.6246950475544234 3.3306690738754598e-016 0
		 -10.30746828464796 9.6827732370935777 18.000000000000092 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4307351417057087e-016 1.192308159657884e-016 0
		 -5.5511151231257768e-016 1 2.8310687127941452e-015 0 -1.1102230246251496e-016 -2.7200464103316331e-015 0.99999999999999978 0
		 -18.000000000000011 17.999999999999993 -6.9999999999999591 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 36 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak1";
	rename -uid "A2021613-4AC4-EF03-EFCD-98AF01514946";
createNode objectSet -n "skinCluster1Set";
	rename -uid "1D724014-4BF5-2640-EA7B-2687FCE7A6C9";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	rename -uid "BB4D600E-4824-1ABB-E67A-0B9A11021FFA";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	rename -uid "22ED9012-4C19-103A-30DE-66BEA9EBF592";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	rename -uid "020B15C3-4D6E-1ADA-2EE1-9E967079B05E";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId69";
	rename -uid "5B9EF0DD-4998-B039-7313-758500108D7C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "12D65F7F-4DCD-B33F-CA78-229F673D0BC9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose1";
	rename -uid "3A4AE2D7-43D9-83DA-ABF0-4B89A9197063";
	setAttr -s 4 ".wm";
	setAttr -s 4 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 18.000000000000025 0.99999999999998823
		 2.8528867454083944e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.49999999999999989 -0.5 -0.50000000000000011 0.50000000000000011 1
		 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.9999999999999982 -1.3305329060742126e-015
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.10911677189150884 0.99402893825681782 1
		 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 9.2195444572928729 1.9984014443252818e-015
		 -3.5527136788005009e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.33225483419176022 0.94318965492429252 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 6.4031242374328396 -1.7763568394002505e-015
		 -7.1054273576010019e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.23405725290886095 0.6672459833979949 0.23405725290886084 0.66724598339799523 1
		 1 1 yes;
	setAttr -s 4 ".m";
	setAttr -s 4 ".p";
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster2";
	rename -uid "69B2E11E-4828-6B97-B674-91808A871167";
	setAttr ".skm" 2;
	setAttr -s 65 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr ".wl[8].w[0]"  1;
	setAttr ".wl[9].w[0]"  1;
	setAttr ".wl[10].w[0]"  1;
	setAttr ".wl[11].w[0]"  1;
	setAttr ".wl[12].w[0]"  1;
	setAttr ".wl[13].w[0]"  1;
	setAttr ".wl[14].w[0]"  1;
	setAttr ".wl[15].w[0]"  1;
	setAttr ".wl[16].w[0]"  1;
	setAttr ".wl[17].w[0]"  1;
	setAttr ".wl[18].w[0]"  1;
	setAttr ".wl[19].w[0]"  1;
	setAttr ".wl[20].w[0]"  1;
	setAttr ".wl[21].w[0]"  1;
	setAttr ".wl[22].w[0]"  1;
	setAttr ".wl[23].w[0]"  1;
	setAttr ".wl[24].w[0]"  1;
	setAttr ".wl[25].w[0]"  1;
	setAttr ".wl[26].w[0]"  1;
	setAttr ".wl[27].w[0]"  1;
	setAttr ".wl[28].w[0]"  1;
	setAttr ".wl[29].w[0]"  1;
	setAttr ".wl[30].w[0]"  1;
	setAttr ".wl[31].w[0]"  1;
	setAttr ".wl[32].w[0]"  1;
	setAttr ".wl[33].w[0]"  1;
	setAttr ".wl[34].w[0]"  1;
	setAttr ".wl[35].w[0]"  1;
	setAttr ".wl[36].w[0]"  1;
	setAttr ".wl[37].w[0]"  1;
	setAttr ".wl[38].w[0]"  1;
	setAttr ".wl[39].w[0]"  1;
	setAttr ".wl[40].w[0]"  1;
	setAttr ".wl[41].w[0]"  1;
	setAttr ".wl[42].w[0]"  1;
	setAttr ".wl[43].w[0]"  1;
	setAttr ".wl[44].w[0]"  1;
	setAttr ".wl[45].w[0]"  1;
	setAttr ".wl[46].w[0]"  1;
	setAttr ".wl[47].w[0]"  1;
	setAttr ".wl[48].w[0]"  1;
	setAttr ".wl[49].w[0]"  1;
	setAttr ".wl[50].w[0]"  1;
	setAttr ".wl[51].w[0]"  1;
	setAttr ".wl[52].w[0]"  1;
	setAttr ".wl[53].w[0]"  1;
	setAttr ".wl[54].w[0]"  1;
	setAttr ".wl[55].w[0]"  1;
	setAttr ".wl[56].w[0]"  1;
	setAttr ".wl[57].w[0]"  1;
	setAttr ".wl[58].w[0]"  1;
	setAttr ".wl[59].w[0]"  1;
	setAttr ".wl[60].w[0]"  1;
	setAttr ".wl[61].w[0]"  1;
	setAttr ".wl[62].w[0]"  1;
	setAttr ".wl[63].w[0]"  1;
	setAttr ".wl[64].w[0]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 3.0114813836638388e-015 2.2204460492503175e-016 -1 0
		 -1 1.110223024625156e-016 -2.7755575615629569e-016 0 -5.5929053192526625e-031 1 3.3306690738754696e-016 0
		 0.99999999999993427 -6.9607119365214783e-015 18.000000000000025 1;
	setAttr ".pm[1]" -type "matrix" 1.8364233684265028e-016 1.8665171476922902e-016 -1 0
		 -0.97618706018395285 0.21693045781865569 7.2164496600634623e-016 0 0.21693045781865558 0.97618706018395285 3.3306690738754588e-016 0
		 -4.8809353009197816 1.0846522890932735 18.000000000000028 1;
	setAttr ".pm[2]" -type "matrix" 3.9662527105997003e-016 5.9600907888582843e-017 -1.000000000000002 0
		 -0.6246950475544214 0.78086880944303083 2.72004641033163e-015 0 0.78086880944303283 0.6246950475544234 3.3306690738754598e-016 0
		 -10.30746828464796 9.6827732370935777 18.000000000000092 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 3.1908755001178373e-016 -3.8036954511554082e-017 -0
		 -5.5511151231257768e-016 1 2.8310687127941488e-015 0 -1.1102230246251474e-016 -2.7200464103316331e-015 0.99999999999999978 -0
		 -18.000000000000043 17.999999999999993 -6.9999999999999512 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 36 0 0 1;
	setAttr -s 3 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 3 ".lw";
	setAttr -s 3 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 3 ".ifcl";
	setAttr -s 3 ".ifcl";
createNode groupId -n "groupId70";
	rename -uid "FF202541-46AE-E4DA-F6A1-C4B33C4B1C1D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "9BB1EEC5-46AF-D383-563D-219A94BC6173";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:60]";
createNode tweak -n "tweak2";
	rename -uid "C03B7658-4BF0-126A-690D-ADBB4F40804A";
createNode objectSet -n "skinCluster2Set";
	rename -uid "0AB31DD0-4B54-7972-C36C-2FA6FCE489B8";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster2GroupId";
	rename -uid "E89F6A92-47B5-B98C-D1F3-7488BA9449E2";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster2GroupParts";
	rename -uid "CA493880-4AE2-D4E0-A389-269B49C8853F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet2";
	rename -uid "1BDA690C-44E5-DFD4-AC3A-98AD3BFDF68A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId72";
	rename -uid "CD72D178-4D2E-0E86-97F5-84BFFB667DCA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "E815A6AD-4445-C510-39EC-F683619A98DB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster3";
	rename -uid "AA0BB7AA-43EA-D76D-C246-7AA8412CE155";
	setAttr ".skm" 2;
	setAttr -s 203 ".wl";
	setAttr ".wl[0].w[3]"  1;
	setAttr ".wl[1].w[3]"  1;
	setAttr ".wl[2].w[3]"  1;
	setAttr ".wl[3].w[3]"  1;
	setAttr ".wl[4].w[3]"  1;
	setAttr ".wl[5].w[3]"  1;
	setAttr ".wl[6].w[3]"  1;
	setAttr ".wl[7].w[3]"  1;
	setAttr ".wl[8].w[3]"  1;
	setAttr ".wl[9].w[3]"  1;
	setAttr ".wl[10].w[3]"  1;
	setAttr ".wl[11].w[3]"  1;
	setAttr ".wl[12].w[3]"  1;
	setAttr ".wl[13].w[3]"  1;
	setAttr ".wl[14].w[3]"  1;
	setAttr ".wl[15].w[3]"  1;
	setAttr ".wl[16].w[3]"  1;
	setAttr ".wl[17].w[3]"  1;
	setAttr ".wl[18].w[3]"  1;
	setAttr ".wl[19].w[3]"  1;
	setAttr ".wl[20].w[3]"  1;
	setAttr ".wl[21].w[3]"  1;
	setAttr ".wl[22].w[3]"  1;
	setAttr ".wl[23].w[3]"  1;
	setAttr ".wl[24].w[3]"  1;
	setAttr ".wl[25].w[3]"  1;
	setAttr ".wl[26].w[3]"  1;
	setAttr ".wl[27].w[3]"  1;
	setAttr ".wl[28].w[3]"  1;
	setAttr ".wl[29].w[3]"  1;
	setAttr ".wl[30].w[3]"  1;
	setAttr ".wl[31].w[3]"  1;
	setAttr ".wl[32].w[3]"  1;
	setAttr ".wl[33].w[3]"  1;
	setAttr ".wl[34].w[3]"  1;
	setAttr ".wl[35].w[3]"  1;
	setAttr ".wl[36].w[3]"  1;
	setAttr ".wl[37].w[3]"  1;
	setAttr ".wl[38].w[3]"  1;
	setAttr ".wl[39].w[3]"  1;
	setAttr ".wl[40].w[3]"  1;
	setAttr ".wl[41].w[3]"  1;
	setAttr ".wl[42].w[3]"  1;
	setAttr ".wl[43].w[3]"  1;
	setAttr ".wl[44].w[3]"  1;
	setAttr ".wl[45].w[3]"  1;
	setAttr ".wl[46].w[3]"  1;
	setAttr ".wl[47].w[3]"  1;
	setAttr ".wl[48].w[3]"  1;
	setAttr ".wl[49].w[3]"  1;
	setAttr ".wl[50].w[3]"  1;
	setAttr ".wl[51].w[3]"  1;
	setAttr ".wl[52].w[3]"  1;
	setAttr ".wl[53].w[3]"  1;
	setAttr ".wl[54].w[3]"  1;
	setAttr ".wl[55].w[3]"  1;
	setAttr ".wl[56].w[3]"  1;
	setAttr ".wl[57].w[3]"  1;
	setAttr ".wl[58].w[3]"  1;
	setAttr ".wl[59].w[3]"  1;
	setAttr ".wl[60].w[3]"  1;
	setAttr ".wl[61].w[3]"  1;
	setAttr ".wl[62].w[3]"  1;
	setAttr ".wl[63].w[3]"  1;
	setAttr ".wl[64].w[3]"  1;
	setAttr ".wl[65].w[3]"  1;
	setAttr ".wl[66].w[3]"  1;
	setAttr ".wl[67].w[3]"  1;
	setAttr ".wl[68].w[3]"  1;
	setAttr ".wl[69].w[3]"  1;
	setAttr ".wl[70].w[3]"  1;
	setAttr ".wl[71].w[3]"  1;
	setAttr ".wl[72].w[3]"  1;
	setAttr ".wl[73].w[3]"  1;
	setAttr ".wl[74].w[3]"  1;
	setAttr ".wl[75].w[3]"  1;
	setAttr ".wl[76].w[3]"  1;
	setAttr ".wl[77].w[3]"  1;
	setAttr ".wl[78].w[3]"  1;
	setAttr ".wl[79].w[3]"  1;
	setAttr ".wl[80].w[3]"  1;
	setAttr ".wl[81].w[3]"  1;
	setAttr ".wl[82].w[3]"  1;
	setAttr ".wl[83].w[3]"  1;
	setAttr ".wl[84].w[3]"  1;
	setAttr ".wl[85].w[3]"  1;
	setAttr ".wl[86].w[3]"  1;
	setAttr ".wl[87].w[3]"  1;
	setAttr ".wl[88].w[3]"  1;
	setAttr ".wl[89].w[3]"  1;
	setAttr ".wl[90].w[3]"  1;
	setAttr ".wl[91].w[3]"  1;
	setAttr ".wl[92].w[3]"  1;
	setAttr ".wl[93].w[3]"  1;
	setAttr ".wl[94].w[3]"  1;
	setAttr ".wl[95].w[3]"  1;
	setAttr ".wl[96].w[3]"  1;
	setAttr ".wl[97].w[3]"  1;
	setAttr ".wl[98].w[3]"  1;
	setAttr ".wl[99].w[3]"  1;
	setAttr ".wl[100].w[3]"  1;
	setAttr ".wl[101].w[3]"  1;
	setAttr ".wl[102].w[3]"  1;
	setAttr ".wl[103].w[3]"  1;
	setAttr ".wl[104].w[3]"  1;
	setAttr ".wl[105].w[3]"  1;
	setAttr ".wl[106].w[3]"  1;
	setAttr ".wl[107].w[3]"  1;
	setAttr ".wl[108].w[3]"  1;
	setAttr ".wl[109].w[3]"  1;
	setAttr ".wl[110].w[3]"  1;
	setAttr ".wl[111].w[3]"  1;
	setAttr ".wl[112].w[3]"  1;
	setAttr ".wl[113].w[3]"  1;
	setAttr ".wl[114].w[3]"  1;
	setAttr ".wl[115].w[3]"  1;
	setAttr ".wl[116].w[3]"  1;
	setAttr ".wl[117].w[3]"  1;
	setAttr ".wl[118].w[3]"  1;
	setAttr ".wl[119].w[3]"  1;
	setAttr ".wl[120].w[3]"  1;
	setAttr ".wl[121].w[3]"  1;
	setAttr ".wl[122].w[3]"  1;
	setAttr ".wl[123].w[3]"  1;
	setAttr ".wl[124].w[3]"  1;
	setAttr ".wl[125].w[3]"  1;
	setAttr ".wl[126].w[3]"  1;
	setAttr ".wl[127].w[3]"  1;
	setAttr ".wl[128].w[3]"  1;
	setAttr ".wl[129].w[3]"  1;
	setAttr ".wl[130].w[3]"  1;
	setAttr ".wl[131].w[3]"  1;
	setAttr ".wl[132].w[3]"  1;
	setAttr ".wl[133].w[3]"  1;
	setAttr ".wl[134].w[3]"  1;
	setAttr ".wl[135].w[3]"  1;
	setAttr ".wl[136].w[3]"  1;
	setAttr ".wl[137].w[3]"  1;
	setAttr ".wl[138].w[3]"  1;
	setAttr ".wl[139].w[3]"  1;
	setAttr ".wl[140].w[3]"  1;
	setAttr ".wl[141].w[3]"  1;
	setAttr ".wl[142].w[3]"  1;
	setAttr ".wl[143].w[3]"  1;
	setAttr ".wl[144].w[3]"  1;
	setAttr ".wl[145].w[3]"  1;
	setAttr ".wl[146].w[3]"  1;
	setAttr ".wl[147].w[3]"  1;
	setAttr ".wl[148].w[3]"  1;
	setAttr ".wl[149].w[3]"  1;
	setAttr ".wl[150].w[3]"  1;
	setAttr ".wl[151].w[3]"  1;
	setAttr ".wl[152].w[3]"  1;
	setAttr ".wl[153].w[3]"  1;
	setAttr ".wl[154].w[3]"  1;
	setAttr ".wl[155].w[3]"  1;
	setAttr ".wl[156].w[3]"  1;
	setAttr ".wl[157].w[3]"  1;
	setAttr ".wl[158].w[3]"  1;
	setAttr ".wl[159].w[3]"  1;
	setAttr ".wl[160].w[3]"  1;
	setAttr ".wl[161].w[3]"  1;
	setAttr ".wl[162].w[3]"  1;
	setAttr ".wl[163].w[3]"  1;
	setAttr ".wl[164].w[3]"  1;
	setAttr ".wl[165].w[3]"  1;
	setAttr ".wl[166].w[3]"  1;
	setAttr ".wl[167].w[3]"  1;
	setAttr ".wl[168].w[3]"  1;
	setAttr ".wl[169].w[3]"  1;
	setAttr ".wl[170].w[3]"  1;
	setAttr ".wl[171].w[3]"  1;
	setAttr ".wl[172].w[3]"  1;
	setAttr ".wl[173].w[3]"  1;
	setAttr ".wl[174].w[3]"  1;
	setAttr ".wl[175].w[3]"  1;
	setAttr ".wl[176].w[3]"  1;
	setAttr ".wl[177].w[3]"  1;
	setAttr ".wl[178].w[3]"  1;
	setAttr ".wl[179].w[3]"  1;
	setAttr ".wl[180].w[3]"  1;
	setAttr ".wl[181].w[3]"  1;
	setAttr ".wl[182].w[3]"  1;
	setAttr ".wl[183].w[3]"  1;
	setAttr ".wl[184].w[3]"  1;
	setAttr ".wl[185].w[3]"  1;
	setAttr ".wl[186].w[3]"  1;
	setAttr ".wl[187].w[3]"  1;
	setAttr ".wl[188].w[3]"  1;
	setAttr ".wl[189].w[3]"  1;
	setAttr ".wl[190].w[3]"  1;
	setAttr ".wl[191].w[3]"  1;
	setAttr ".wl[192].w[3]"  1;
	setAttr ".wl[193].w[3]"  1;
	setAttr ".wl[194].w[3]"  1;
	setAttr ".wl[195].w[3]"  1;
	setAttr ".wl[196].w[3]"  1;
	setAttr ".wl[197].w[3]"  1;
	setAttr ".wl[198].w[3]"  1;
	setAttr ".wl[199].w[3]"  1;
	setAttr ".wl[200].w[3]"  1;
	setAttr ".wl[201].w[3]"  1;
	setAttr ".wl[202].w[3]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" -1.1102230246251565e-016 2.2204460492503131e-016 -1 0
		 -1 1.110223024625156e-016 2.2204460492503136e-016 0 3.6977854932234928e-032 1 3.3306690738754696e-016 -0
		 0.99999999999999023 -6.9607119365214783e-015 18.000000000000025 1;
	setAttr ".pm[1]" -type "matrix" 1.8364233684265028e-016 1.8665171476922902e-016 -1 0
		 -0.97618706018395285 0.21693045781865569 7.2164496600634623e-016 0 0.21693045781865558 0.97618706018395285 3.3306690738754588e-016 0
		 -4.8809353009197816 1.0846522890932735 18.000000000000028 1;
	setAttr ".pm[2]" -type "matrix" 3.9662527105997003e-016 5.9600907888582843e-017 -1.000000000000002 0
		 -0.6246950475544214 0.78086880944303083 2.72004641033163e-015 0 0.78086880944303283 0.6246950475544234 3.3306690738754598e-016 0
		 -10.30746828464796 9.6827732370935777 18.000000000000092 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4307351417057087e-016 1.192308159657884e-016 0
		 -5.5511151231257768e-016 1 2.8310687127941452e-015 0 -1.1102230246251496e-016 -2.7200464103316331e-015 0.99999999999999978 0
		 -18.000000000000011 17.999999999999993 -6.9999999999999591 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 36 0 0 1;
	setAttr -s 3 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 3 ".lw";
	setAttr -s 3 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 3 ".ifcl";
	setAttr -s 3 ".ifcl";
createNode tweak -n "tweak3";
	rename -uid "E977FAB7-4C31-4A71-0EC8-05AD045003A6";
createNode objectSet -n "skinCluster3Set";
	rename -uid "49459FE0-47B0-E04C-B005-3C83E7D0FDDE";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster3GroupId";
	rename -uid "79A83926-4E90-1CEF-CF91-4BAB27FD3A88";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster3GroupParts";
	rename -uid "B342A0DF-4AEE-2792-DC6E-8D8E6CE8F82A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet3";
	rename -uid "F898E3C3-4351-7306-E8EB-B69EAA0E0D15";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId74";
	rename -uid "8984A523-4631-FFEE-9841-ECB922427105";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "F8B306D7-44FB-F6FB-858A-D983399D51DA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster4";
	rename -uid "E4F5B0B2-40D7-C590-8E28-47BB49B240B6";
	setAttr ".skm" 2;
	setAttr -s 80 ".wl";
	setAttr ".wl[0].w[2]"  1;
	setAttr ".wl[1].w[2]"  1;
	setAttr ".wl[2].w[2]"  1;
	setAttr ".wl[3].w[2]"  1;
	setAttr ".wl[4].w[2]"  1;
	setAttr ".wl[5].w[2]"  1;
	setAttr ".wl[6].w[2]"  1;
	setAttr ".wl[7].w[2]"  1;
	setAttr ".wl[8].w[2]"  1;
	setAttr ".wl[9].w[2]"  1;
	setAttr ".wl[10].w[2]"  1;
	setAttr ".wl[11].w[2]"  1;
	setAttr ".wl[12].w[2]"  1;
	setAttr ".wl[13].w[2]"  1;
	setAttr ".wl[14].w[2]"  1;
	setAttr ".wl[15].w[2]"  1;
	setAttr ".wl[16].w[2]"  1;
	setAttr ".wl[17].w[2]"  1;
	setAttr ".wl[18].w[2]"  1;
	setAttr ".wl[19].w[2]"  1;
	setAttr ".wl[20].w[2]"  1;
	setAttr ".wl[21].w[2]"  1;
	setAttr ".wl[22].w[2]"  1;
	setAttr ".wl[23].w[2]"  1;
	setAttr ".wl[24].w[2]"  1;
	setAttr ".wl[25].w[2]"  1;
	setAttr ".wl[26].w[2]"  1;
	setAttr ".wl[27].w[2]"  1;
	setAttr ".wl[28].w[2]"  1;
	setAttr ".wl[29].w[2]"  1;
	setAttr ".wl[30].w[2]"  1;
	setAttr ".wl[31].w[2]"  1;
	setAttr ".wl[32].w[2]"  1;
	setAttr ".wl[33].w[2]"  1;
	setAttr ".wl[34].w[2]"  1;
	setAttr ".wl[35].w[2]"  1;
	setAttr ".wl[36].w[2]"  1;
	setAttr ".wl[37].w[2]"  1;
	setAttr ".wl[38].w[2]"  1;
	setAttr ".wl[39].w[2]"  1;
	setAttr ".wl[40].w[2]"  1;
	setAttr ".wl[41].w[2]"  1;
	setAttr ".wl[42].w[2]"  1;
	setAttr ".wl[43].w[2]"  1;
	setAttr ".wl[44].w[2]"  1;
	setAttr ".wl[45].w[2]"  1;
	setAttr ".wl[46].w[2]"  1;
	setAttr ".wl[47].w[2]"  1;
	setAttr ".wl[48].w[2]"  1;
	setAttr ".wl[49].w[2]"  1;
	setAttr ".wl[50].w[2]"  1;
	setAttr ".wl[51].w[2]"  1;
	setAttr ".wl[52].w[2]"  1;
	setAttr ".wl[53].w[2]"  1;
	setAttr ".wl[54].w[2]"  1;
	setAttr ".wl[55].w[2]"  1;
	setAttr ".wl[56].w[2]"  1;
	setAttr ".wl[57].w[2]"  1;
	setAttr ".wl[58].w[2]"  1;
	setAttr ".wl[59].w[2]"  1;
	setAttr ".wl[60].w[2]"  1;
	setAttr ".wl[61].w[2]"  1;
	setAttr ".wl[62].w[2]"  1;
	setAttr ".wl[63].w[2]"  1;
	setAttr ".wl[64].w[2]"  1;
	setAttr ".wl[65].w[2]"  1;
	setAttr ".wl[66].w[2]"  1;
	setAttr ".wl[67].w[2]"  1;
	setAttr ".wl[68].w[2]"  1;
	setAttr ".wl[69].w[2]"  1;
	setAttr ".wl[70].w[2]"  1;
	setAttr ".wl[71].w[2]"  1;
	setAttr ".wl[72].w[2]"  1;
	setAttr ".wl[73].w[2]"  1;
	setAttr ".wl[74].w[2]"  1;
	setAttr ".wl[75].w[2]"  1;
	setAttr ".wl[76].w[2]"  1;
	setAttr ".wl[77].w[2]"  1;
	setAttr ".wl[78].w[2]"  1;
	setAttr ".wl[79].w[2]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 3.0114813836638388e-015 2.2204460492503175e-016 -1 0
		 -1 1.110223024625156e-016 -2.7755575615629569e-016 0 -5.5929053192526625e-031 1 3.3306690738754696e-016 0
		 0.99999999999993427 -6.9607119365214783e-015 18.000000000000025 1;
	setAttr ".pm[1]" -type "matrix" 1.8364233684265028e-016 1.8665171476922902e-016 -1 0
		 -0.97618706018395285 0.21693045781865569 7.2164496600634623e-016 0 0.21693045781865558 0.97618706018395285 3.3306690738754588e-016 0
		 -4.8809353009197816 1.0846522890932735 18.000000000000028 1;
	setAttr ".pm[2]" -type "matrix" 3.9662527105997003e-016 5.9600907888582843e-017 -1.000000000000002 0
		 -0.6246950475544214 0.78086880944303083 2.72004641033163e-015 0 0.78086880944303283 0.6246950475544234 3.3306690738754598e-016 0
		 -10.30746828464796 9.6827732370935777 18.000000000000092 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4307351417057087e-016 1.192308159657884e-016 0
		 -5.5511151231257768e-016 1 2.8310687127941452e-015 0 -1.1102230246251496e-016 -2.7200464103316331e-015 0.99999999999999978 0
		 -18.000000000000011 17.999999999999993 -6.9999999999999591 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 36 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak4";
	rename -uid "1844AC8B-474D-EA61-3266-EB89CF3C3AB9";
createNode objectSet -n "skinCluster4Set";
	rename -uid "06CBB9A7-447A-E606-9E80-A69A34906628";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster4GroupId";
	rename -uid "558D513D-4C6F-98C0-5980-3B9285BE9A68";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster4GroupParts";
	rename -uid "470C6798-4AC4-4881-CC56-58B540E1AB62";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet4";
	rename -uid "2F4D9291-48D0-3586-9313-A3B139C8798C";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId76";
	rename -uid "42810670-4B2D-E172-5FB7-A69C268B682D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "FBB8BC2C-4758-B3B1-F5C6-8293508B40A2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster5";
	rename -uid "623FB0F4-44CF-0B06-D1A6-4CB52E505DBF";
	setAttr ".skm" 2;
	setAttr -s 65 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr ".wl[8].w[0]"  1;
	setAttr ".wl[9].w[0]"  1;
	setAttr ".wl[10].w[0]"  1;
	setAttr ".wl[11].w[0]"  1;
	setAttr ".wl[12].w[0]"  1;
	setAttr ".wl[13].w[0]"  1;
	setAttr ".wl[14].w[0]"  1;
	setAttr ".wl[15].w[0]"  1;
	setAttr ".wl[16].w[0]"  1;
	setAttr ".wl[17].w[0]"  1;
	setAttr ".wl[18].w[0]"  1;
	setAttr ".wl[19].w[0]"  1;
	setAttr ".wl[20].w[0]"  1;
	setAttr ".wl[21].w[0]"  1;
	setAttr ".wl[22].w[0]"  1;
	setAttr ".wl[23].w[0]"  1;
	setAttr ".wl[24].w[0]"  1;
	setAttr ".wl[25].w[0]"  1;
	setAttr ".wl[26].w[0]"  1;
	setAttr ".wl[27].w[0]"  1;
	setAttr ".wl[28].w[0]"  1;
	setAttr ".wl[29].w[0]"  1;
	setAttr ".wl[30].w[0]"  1;
	setAttr ".wl[31].w[0]"  1;
	setAttr ".wl[32].w[0]"  1;
	setAttr ".wl[33].w[0]"  1;
	setAttr ".wl[34].w[0]"  1;
	setAttr ".wl[35].w[0]"  1;
	setAttr ".wl[36].w[0]"  1;
	setAttr ".wl[37].w[0]"  1;
	setAttr ".wl[38].w[0]"  1;
	setAttr ".wl[39].w[0]"  1;
	setAttr ".wl[40].w[0]"  1;
	setAttr ".wl[41].w[0]"  1;
	setAttr ".wl[42].w[0]"  1;
	setAttr ".wl[43].w[0]"  1;
	setAttr ".wl[44].w[0]"  1;
	setAttr ".wl[45].w[0]"  1;
	setAttr ".wl[46].w[0]"  1;
	setAttr ".wl[47].w[0]"  1;
	setAttr ".wl[48].w[0]"  1;
	setAttr ".wl[49].w[0]"  1;
	setAttr ".wl[50].w[0]"  1;
	setAttr ".wl[51].w[0]"  1;
	setAttr ".wl[52].w[0]"  1;
	setAttr ".wl[53].w[0]"  1;
	setAttr ".wl[54].w[0]"  1;
	setAttr ".wl[55].w[0]"  1;
	setAttr ".wl[56].w[0]"  1;
	setAttr ".wl[57].w[0]"  1;
	setAttr ".wl[58].w[0]"  1;
	setAttr ".wl[59].w[0]"  1;
	setAttr ".wl[60].w[0]"  1;
	setAttr ".wl[61].w[0]"  1;
	setAttr ".wl[62].w[0]"  1;
	setAttr ".wl[63].w[0]"  1;
	setAttr ".wl[64].w[0]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 1.4918631422521206e-015 3.3306690738754839e-016 -1 0
		 -1 1.110223024625157e-016 6.9388939039071929e-016 0 2.2204460492503037e-016 1 2.7755575615628899e-016 0
		 1.000000000000052 -4.9623104921961989e-015 -18 1;
	setAttr ".pm[1]" -type "matrix" 8.3244655735354775e-016 1.6587031404865265e-016 -1.0000000000000002 0
		 -0.97618706018395096 0.21693045781865497 -1.3045120539345625e-015 0 0.21693045781865508 0.97618706018395296 7.771561172376079e-016 0
		 -4.8809353009197149 1.0846522890932555 -17.999999999999996 1;
	setAttr ".pm[2]" -type "matrix" -2.8618187619441876e-017 6.546897429831028e-016 -1.0000000000000002 0
		 -0.62654852027652819 0.779382416878448 -3.053113317719218e-016 0 0.77938241687844922 0.6265485202765263 2.7755575615628904e-016 0
		 -10.330444117628252 9.6582567957451761 -17.999999999999996 1;
	setAttr ".pm[3]" -type "matrix" 1.0000000000000002 1.0118840052997088e-015 -3.9634938013954415e-016 -0
		 -9.4527080230400152e-016 0.999997177638006 -0.0023758611116653778 0 3.8738919747877616e-016 0.0023758611116653223 0.99999717763800611 -0
		 17.999999999999783 17.995208764708689 -7.0332564108393409 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 3 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 3 ".lw";
	setAttr -s 3 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 3 ".ifcl";
	setAttr -s 3 ".ifcl";
createNode groupId -n "groupId77";
	rename -uid "837A447C-44EC-F704-34B1-09BDE115DCD1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "D5924322-430A-E0EB-ED44-1CA32213BE14";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:60]";
createNode tweak -n "tweak5";
	rename -uid "452E428B-45E0-357F-40E7-3380EB4C4ED1";
createNode objectSet -n "skinCluster5Set";
	rename -uid "7F085475-4269-4B95-D48E-708582F3890F";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster5GroupId";
	rename -uid "A47BB121-4018-8BF6-067A-08BC2CD18DA6";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster5GroupParts";
	rename -uid "CDEAB67A-4ECF-3CD8-28CC-E8A40EEA7E4C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet5";
	rename -uid "791D9226-4490-4FFE-93E9-3B82581E583A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId79";
	rename -uid "69C51E57-42CA-79D7-2517-4285158B2492";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "F8A52D62-49CC-9EE5-0FA9-E59113FA6347";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose3";
	rename -uid "6FB1923A-4C60-9F44-48AF-93A53050E3F0";
	setAttr -s 4 ".wm";
	setAttr -s 5 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 -18 1.0000000000000251
		 1.0846492522709522e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.49999999999999983 -0.50000000000000011 -0.50000000000000022 0.5 1
		 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.9999999999999982 -1.3305329060742134e-015
		 -3.5527136788005009e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.1091167718915086 0.99402893825681782 1
		 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 9.2195444572928658 4.2188474935755949e-015
		 -7.1054273576010019e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.33113415515421973 0.94358368536675175 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 6.4031242374328272 -1.4209120415035843e-015
		 -2.0250467969162855e-013 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.23405725290885909 0.66724598339799546 0.23405725290885893 0.66724598339799601 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 6.4031242374328254 -1.7763568394002505e-015
		 -2.0250467969162855e-013 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.23405725290885909 0.66724598339799546 0.23405725290885893 0.66724598339799601 1
		 1 1 yes;
	setAttr -s 4 ".m";
	setAttr -s 4 ".p";
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster6";
	rename -uid "D4E1047A-480C-5E41-65C9-8F8EC1546E4F";
	setAttr ".skm" 2;
	setAttr -s 16 ".wl";
	setAttr ".wl[0].w[1]"  1;
	setAttr ".wl[1].w[1]"  1;
	setAttr ".wl[2].w[1]"  1;
	setAttr ".wl[3].w[1]"  1;
	setAttr ".wl[4].w[1]"  1;
	setAttr ".wl[5].w[1]"  1;
	setAttr ".wl[6].w[1]"  1;
	setAttr ".wl[7].w[1]"  1;
	setAttr ".wl[8].w[1]"  1;
	setAttr ".wl[9].w[1]"  1;
	setAttr ".wl[10].w[1]"  1;
	setAttr ".wl[11].w[1]"  1;
	setAttr ".wl[12].w[1]"  1;
	setAttr ".wl[13].w[1]"  1;
	setAttr ".wl[14].w[1]"  1;
	setAttr ".wl[15].w[1]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 1.4918631422521206e-015 3.3306690738754839e-016 -1 0
		 -1 1.110223024625157e-016 6.9388939039071929e-016 0 2.2204460492503037e-016 1 2.7755575615628899e-016 0
		 1.000000000000052 -4.9623104921961989e-015 -18 1;
	setAttr ".pm[1]" -type "matrix" 8.3244655735354775e-016 1.6587031404865265e-016 -1.0000000000000002 0
		 -0.97618706018395096 0.21693045781865497 -1.3045120539345625e-015 0 0.21693045781865508 0.97618706018395296 7.771561172376079e-016 0
		 -4.8809353009197149 1.0846522890932555 -17.999999999999996 1;
	setAttr ".pm[2]" -type "matrix" -2.8618187619441876e-017 6.546897429831028e-016 -1.0000000000000002 0
		 -0.62654852027652819 0.779382416878448 -3.053113317719218e-016 0 0.77938241687844922 0.6265485202765263 2.7755575615628904e-016 0
		 -10.330444117628252 9.6582567957451761 -17.999999999999996 1;
	setAttr ".pm[3]" -type "matrix" 1.0000000000000002 -2.3738444712824201e-016 -9.7318059438380286e-016 0
		 -1.0023631849078758e-015 0.999997177638006 -0.002375861111665377 0 3.8706769807673593e-016 0.0023758611116653219 0.99999717763800611 0
		 17.99999999999978 17.995208764708654 -7.033256410839356 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak6";
	rename -uid "4E7EF221-4F15-0A22-B1F6-A4838DAEE676";
createNode objectSet -n "skinCluster6Set";
	rename -uid "6EB8D3A4-4A23-6C75-7DB6-12BF17B01846";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster6GroupId";
	rename -uid "FE7B29B7-4F6E-0AEA-3B48-B9BAE2F31E92";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster6GroupParts";
	rename -uid "21ED815A-4CE3-A19F-BA3D-268E4C63A580";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet6";
	rename -uid "9358D4C4-42EC-1463-8805-1A91D6361999";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId81";
	rename -uid "5B47E12F-46D8-F5A0-2CF0-1492D6B44953";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts14";
	rename -uid "16DD7EA0-43DE-0699-240C-66962966DCA1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster7";
	rename -uid "A21D718C-4D5D-EF24-3F7E-78B050D668C6";
	setAttr ".skm" 2;
	setAttr -s 80 ".wl";
	setAttr ".wl[0].w[2]"  1;
	setAttr ".wl[1].w[2]"  1;
	setAttr ".wl[2].w[2]"  1;
	setAttr ".wl[3].w[2]"  1;
	setAttr ".wl[4].w[2]"  1;
	setAttr ".wl[5].w[2]"  1;
	setAttr ".wl[6].w[2]"  1;
	setAttr ".wl[7].w[2]"  1;
	setAttr ".wl[8].w[2]"  1;
	setAttr ".wl[9].w[2]"  1;
	setAttr ".wl[10].w[2]"  1;
	setAttr ".wl[11].w[2]"  1;
	setAttr ".wl[12].w[2]"  1;
	setAttr ".wl[13].w[2]"  1;
	setAttr ".wl[14].w[2]"  1;
	setAttr ".wl[15].w[2]"  1;
	setAttr ".wl[16].w[2]"  1;
	setAttr ".wl[17].w[2]"  1;
	setAttr ".wl[18].w[2]"  1;
	setAttr ".wl[19].w[2]"  1;
	setAttr ".wl[20].w[2]"  1;
	setAttr ".wl[21].w[2]"  1;
	setAttr ".wl[22].w[2]"  1;
	setAttr ".wl[23].w[2]"  1;
	setAttr ".wl[24].w[2]"  1;
	setAttr ".wl[25].w[2]"  1;
	setAttr ".wl[26].w[2]"  1;
	setAttr ".wl[27].w[2]"  1;
	setAttr ".wl[28].w[2]"  1;
	setAttr ".wl[29].w[2]"  1;
	setAttr ".wl[30].w[2]"  1;
	setAttr ".wl[31].w[2]"  1;
	setAttr ".wl[32].w[2]"  1;
	setAttr ".wl[33].w[2]"  1;
	setAttr ".wl[34].w[2]"  1;
	setAttr ".wl[35].w[2]"  1;
	setAttr ".wl[36].w[2]"  1;
	setAttr ".wl[37].w[2]"  1;
	setAttr ".wl[38].w[2]"  1;
	setAttr ".wl[39].w[2]"  1;
	setAttr ".wl[40].w[2]"  1;
	setAttr ".wl[41].w[2]"  1;
	setAttr ".wl[42].w[2]"  1;
	setAttr ".wl[43].w[2]"  1;
	setAttr ".wl[44].w[2]"  1;
	setAttr ".wl[45].w[2]"  1;
	setAttr ".wl[46].w[2]"  1;
	setAttr ".wl[47].w[2]"  1;
	setAttr ".wl[48].w[2]"  1;
	setAttr ".wl[49].w[2]"  1;
	setAttr ".wl[50].w[2]"  1;
	setAttr ".wl[51].w[2]"  1;
	setAttr ".wl[52].w[2]"  1;
	setAttr ".wl[53].w[2]"  1;
	setAttr ".wl[54].w[2]"  1;
	setAttr ".wl[55].w[2]"  1;
	setAttr ".wl[56].w[2]"  1;
	setAttr ".wl[57].w[2]"  1;
	setAttr ".wl[58].w[2]"  1;
	setAttr ".wl[59].w[2]"  1;
	setAttr ".wl[60].w[2]"  1;
	setAttr ".wl[61].w[2]"  1;
	setAttr ".wl[62].w[2]"  1;
	setAttr ".wl[63].w[2]"  1;
	setAttr ".wl[64].w[2]"  1;
	setAttr ".wl[65].w[2]"  1;
	setAttr ".wl[66].w[2]"  1;
	setAttr ".wl[67].w[2]"  1;
	setAttr ".wl[68].w[2]"  1;
	setAttr ".wl[69].w[2]"  1;
	setAttr ".wl[70].w[2]"  1;
	setAttr ".wl[71].w[2]"  1;
	setAttr ".wl[72].w[2]"  1;
	setAttr ".wl[73].w[2]"  1;
	setAttr ".wl[74].w[2]"  1;
	setAttr ".wl[75].w[2]"  1;
	setAttr ".wl[76].w[2]"  1;
	setAttr ".wl[77].w[2]"  1;
	setAttr ".wl[78].w[2]"  1;
	setAttr ".wl[79].w[2]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 1.4918631422521206e-015 3.3306690738754839e-016 -1 0
		 -1 1.110223024625157e-016 6.9388939039071929e-016 0 2.2204460492503037e-016 1 2.7755575615628899e-016 0
		 1.000000000000052 -4.9623104921961989e-015 -18 1;
	setAttr ".pm[1]" -type "matrix" 8.3244655735354775e-016 1.6587031404865265e-016 -1.0000000000000002 0
		 -0.97618706018395096 0.21693045781865497 -1.3045120539345625e-015 0 0.21693045781865508 0.97618706018395296 7.771561172376079e-016 0
		 -4.8809353009197149 1.0846522890932555 -17.999999999999996 1;
	setAttr ".pm[2]" -type "matrix" -2.8618187619441876e-017 6.546897429831028e-016 -1.0000000000000002 0
		 -0.62654852027652819 0.779382416878448 -3.053113317719218e-016 0 0.77938241687844922 0.6265485202765263 2.7755575615628904e-016 0
		 -10.330444117628252 9.6582567957451761 -17.999999999999996 1;
	setAttr ".pm[3]" -type "matrix" 1.0000000000000002 -2.3738444712824201e-016 -9.7318059438380286e-016 0
		 -1.0023631849078758e-015 0.999997177638006 -0.002375861111665377 0 3.8706769807673593e-016 0.0023758611116653219 0.99999717763800611 0
		 17.99999999999978 17.995208764708654 -7.033256410839356 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak7";
	rename -uid "8F2AD4E5-48AE-CF88-DA01-81BAB3D6C995";
createNode objectSet -n "skinCluster7Set";
	rename -uid "7E223E0E-4FA2-E982-478E-1CBDB0300E06";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster7GroupId";
	rename -uid "BB4C16F8-4450-7A03-5257-6782B947D0F7";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster7GroupParts";
	rename -uid "EFC804B3-434B-2D8F-D660-37B02FE01B0F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet7";
	rename -uid "2F892695-496A-04AC-1761-23A507D96068";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId83";
	rename -uid "0E036B28-41BE-BC3E-CE55-0095D6247162";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts16";
	rename -uid "3DA76BFF-46BC-E25C-670B-1AA641439426";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster8";
	rename -uid "072D6D32-4EDF-81B0-43AA-86AB2753A7F7";
	setAttr ".skm" 2;
	setAttr -s 203 ".wl";
	setAttr ".wl[0].w[3]"  1;
	setAttr ".wl[1].w[3]"  1;
	setAttr ".wl[2].w[3]"  1;
	setAttr ".wl[3].w[3]"  1;
	setAttr ".wl[4].w[3]"  1;
	setAttr ".wl[5].w[3]"  1;
	setAttr ".wl[6].w[3]"  1;
	setAttr ".wl[7].w[3]"  1;
	setAttr ".wl[8].w[3]"  1;
	setAttr ".wl[9].w[3]"  1;
	setAttr ".wl[10].w[3]"  1;
	setAttr ".wl[11].w[3]"  1;
	setAttr ".wl[12].w[3]"  1;
	setAttr ".wl[13].w[3]"  1;
	setAttr ".wl[14].w[3]"  1;
	setAttr ".wl[15].w[3]"  1;
	setAttr ".wl[16].w[3]"  1;
	setAttr ".wl[17].w[3]"  1;
	setAttr ".wl[18].w[3]"  1;
	setAttr ".wl[19].w[3]"  1;
	setAttr ".wl[20].w[3]"  1;
	setAttr ".wl[21].w[3]"  1;
	setAttr ".wl[22].w[3]"  1;
	setAttr ".wl[23].w[3]"  1;
	setAttr ".wl[24].w[3]"  1;
	setAttr ".wl[25].w[3]"  1;
	setAttr ".wl[26].w[3]"  1;
	setAttr ".wl[27].w[3]"  1;
	setAttr ".wl[28].w[3]"  1;
	setAttr ".wl[29].w[3]"  1;
	setAttr ".wl[30].w[3]"  1;
	setAttr ".wl[31].w[3]"  1;
	setAttr ".wl[32].w[3]"  1;
	setAttr ".wl[33].w[3]"  1;
	setAttr ".wl[34].w[3]"  1;
	setAttr ".wl[35].w[3]"  1;
	setAttr ".wl[36].w[3]"  1;
	setAttr ".wl[37].w[3]"  1;
	setAttr ".wl[38].w[3]"  1;
	setAttr ".wl[39].w[3]"  1;
	setAttr ".wl[40].w[3]"  1;
	setAttr ".wl[41].w[3]"  1;
	setAttr ".wl[42].w[3]"  1;
	setAttr ".wl[43].w[3]"  1;
	setAttr ".wl[44].w[3]"  1;
	setAttr ".wl[45].w[3]"  1;
	setAttr ".wl[46].w[3]"  1;
	setAttr ".wl[47].w[3]"  1;
	setAttr ".wl[48].w[3]"  1;
	setAttr ".wl[49].w[3]"  1;
	setAttr ".wl[50].w[3]"  1;
	setAttr ".wl[51].w[3]"  1;
	setAttr ".wl[52].w[3]"  1;
	setAttr ".wl[53].w[3]"  1;
	setAttr ".wl[54].w[3]"  1;
	setAttr ".wl[55].w[3]"  1;
	setAttr ".wl[56].w[3]"  1;
	setAttr ".wl[57].w[3]"  1;
	setAttr ".wl[58].w[3]"  1;
	setAttr ".wl[59].w[3]"  1;
	setAttr ".wl[60].w[3]"  1;
	setAttr ".wl[61].w[3]"  1;
	setAttr ".wl[62].w[3]"  1;
	setAttr ".wl[63].w[3]"  1;
	setAttr ".wl[64].w[3]"  1;
	setAttr ".wl[65].w[3]"  1;
	setAttr ".wl[66].w[3]"  1;
	setAttr ".wl[67].w[3]"  1;
	setAttr ".wl[68].w[3]"  1;
	setAttr ".wl[69].w[3]"  1;
	setAttr ".wl[70].w[3]"  1;
	setAttr ".wl[71].w[3]"  1;
	setAttr ".wl[72].w[3]"  1;
	setAttr ".wl[73].w[3]"  1;
	setAttr ".wl[74].w[3]"  1;
	setAttr ".wl[75].w[3]"  1;
	setAttr ".wl[76].w[3]"  1;
	setAttr ".wl[77].w[3]"  1;
	setAttr ".wl[78].w[3]"  1;
	setAttr ".wl[79].w[3]"  1;
	setAttr ".wl[80].w[3]"  1;
	setAttr ".wl[81].w[3]"  1;
	setAttr ".wl[82].w[3]"  1;
	setAttr ".wl[83].w[3]"  1;
	setAttr ".wl[84].w[3]"  1;
	setAttr ".wl[85].w[3]"  1;
	setAttr ".wl[86].w[3]"  1;
	setAttr ".wl[87].w[3]"  1;
	setAttr ".wl[88].w[3]"  1;
	setAttr ".wl[89].w[3]"  1;
	setAttr ".wl[90].w[3]"  1;
	setAttr ".wl[91].w[3]"  1;
	setAttr ".wl[92].w[3]"  1;
	setAttr ".wl[93].w[3]"  1;
	setAttr ".wl[94].w[3]"  1;
	setAttr ".wl[95].w[3]"  1;
	setAttr ".wl[96].w[3]"  1;
	setAttr ".wl[97].w[3]"  1;
	setAttr ".wl[98].w[3]"  1;
	setAttr ".wl[99].w[3]"  1;
	setAttr ".wl[100].w[3]"  1;
	setAttr ".wl[101].w[3]"  1;
	setAttr ".wl[102].w[3]"  1;
	setAttr ".wl[103].w[3]"  1;
	setAttr ".wl[104].w[3]"  1;
	setAttr ".wl[105].w[3]"  1;
	setAttr ".wl[106].w[3]"  1;
	setAttr ".wl[107].w[3]"  1;
	setAttr ".wl[108].w[3]"  1;
	setAttr ".wl[109].w[3]"  1;
	setAttr ".wl[110].w[3]"  1;
	setAttr ".wl[111].w[3]"  1;
	setAttr ".wl[112].w[3]"  1;
	setAttr ".wl[113].w[3]"  1;
	setAttr ".wl[114].w[3]"  1;
	setAttr ".wl[115].w[3]"  1;
	setAttr ".wl[116].w[3]"  1;
	setAttr ".wl[117].w[3]"  1;
	setAttr ".wl[118].w[3]"  1;
	setAttr ".wl[119].w[3]"  1;
	setAttr ".wl[120].w[3]"  1;
	setAttr ".wl[121].w[3]"  1;
	setAttr ".wl[122].w[3]"  1;
	setAttr ".wl[123].w[3]"  1;
	setAttr ".wl[124].w[3]"  1;
	setAttr ".wl[125].w[3]"  1;
	setAttr ".wl[126].w[3]"  1;
	setAttr ".wl[127].w[3]"  1;
	setAttr ".wl[128].w[3]"  1;
	setAttr ".wl[129].w[3]"  1;
	setAttr ".wl[130].w[3]"  1;
	setAttr ".wl[131].w[3]"  1;
	setAttr ".wl[132].w[3]"  1;
	setAttr ".wl[133].w[3]"  1;
	setAttr ".wl[134].w[3]"  1;
	setAttr ".wl[135].w[3]"  1;
	setAttr ".wl[136].w[3]"  1;
	setAttr ".wl[137].w[3]"  1;
	setAttr ".wl[138].w[3]"  1;
	setAttr ".wl[139].w[3]"  1;
	setAttr ".wl[140].w[3]"  1;
	setAttr ".wl[141].w[3]"  1;
	setAttr ".wl[142].w[3]"  1;
	setAttr ".wl[143].w[3]"  1;
	setAttr ".wl[144].w[3]"  1;
	setAttr ".wl[145].w[3]"  1;
	setAttr ".wl[146].w[3]"  1;
	setAttr ".wl[147].w[3]"  1;
	setAttr ".wl[148].w[3]"  1;
	setAttr ".wl[149].w[3]"  1;
	setAttr ".wl[150].w[3]"  1;
	setAttr ".wl[151].w[3]"  1;
	setAttr ".wl[152].w[3]"  1;
	setAttr ".wl[153].w[3]"  1;
	setAttr ".wl[154].w[3]"  1;
	setAttr ".wl[155].w[3]"  1;
	setAttr ".wl[156].w[3]"  1;
	setAttr ".wl[157].w[3]"  1;
	setAttr ".wl[158].w[3]"  1;
	setAttr ".wl[159].w[3]"  1;
	setAttr ".wl[160].w[3]"  1;
	setAttr ".wl[161].w[3]"  1;
	setAttr ".wl[162].w[3]"  1;
	setAttr ".wl[163].w[3]"  1;
	setAttr ".wl[164].w[3]"  1;
	setAttr ".wl[165].w[3]"  1;
	setAttr ".wl[166].w[3]"  1;
	setAttr ".wl[167].w[3]"  1;
	setAttr ".wl[168].w[3]"  1;
	setAttr ".wl[169].w[3]"  1;
	setAttr ".wl[170].w[3]"  1;
	setAttr ".wl[171].w[3]"  1;
	setAttr ".wl[172].w[3]"  1;
	setAttr ".wl[173].w[3]"  1;
	setAttr ".wl[174].w[3]"  1;
	setAttr ".wl[175].w[3]"  1;
	setAttr ".wl[176].w[3]"  1;
	setAttr ".wl[177].w[3]"  1;
	setAttr ".wl[178].w[3]"  1;
	setAttr ".wl[179].w[3]"  1;
	setAttr ".wl[180].w[3]"  1;
	setAttr ".wl[181].w[3]"  1;
	setAttr ".wl[182].w[3]"  1;
	setAttr ".wl[183].w[3]"  1;
	setAttr ".wl[184].w[3]"  1;
	setAttr ".wl[185].w[3]"  1;
	setAttr ".wl[186].w[3]"  1;
	setAttr ".wl[187].w[3]"  1;
	setAttr ".wl[188].w[3]"  1;
	setAttr ".wl[189].w[3]"  1;
	setAttr ".wl[190].w[3]"  1;
	setAttr ".wl[191].w[3]"  1;
	setAttr ".wl[192].w[3]"  1;
	setAttr ".wl[193].w[3]"  1;
	setAttr ".wl[194].w[3]"  1;
	setAttr ".wl[195].w[3]"  1;
	setAttr ".wl[196].w[3]"  1;
	setAttr ".wl[197].w[3]"  1;
	setAttr ".wl[198].w[3]"  1;
	setAttr ".wl[199].w[3]"  1;
	setAttr ".wl[200].w[3]"  1;
	setAttr ".wl[201].w[3]"  1;
	setAttr ".wl[202].w[3]"  1;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" -4.9960036108132034e-016 3.3306690738754711e-016 -1.0000000000000002 0
		 -1 1.1102230246251548e-016 4.4408920985006271e-016 0 2.2204460492503146e-016 1 2.7755575615628904e-016 -0
		 1.0000000000000162 -4.9623104921961918e-015 -18.000000000000004 1;
	setAttr ".pm[1]" -type "matrix" 8.038083985504763e-016 1.6256759586795396e-016 -1.0000000000000002 0
		 -0.97618706018395096 0.21693045781865497 -1.3045120539345625e-015 0 0.21693045781865508 0.97618706018395296 7.771561172376079e-016 0
		 -4.8809353009197167 1.0846522890932555 -18 1;
	setAttr ".pm[2]" -type "matrix" 2.4819188083741644e-017 5.0071663796351376e-016 -1.0000000000000002 0
		 -0.6265485202765283 0.779382416878448 -3.0531133177192189e-016 0 0.77938241687844911 0.6265485202765263 2.7755575615628909e-016 0
		 -10.330444117628254 9.6582567957451726 -18 1;
	setAttr ".pm[3]" -type "matrix" 1.0000000000000002 -1.2533496606970108e-016 -9.6054739998967053e-016 0
		 -9.4527080230399975e-016 0.999997177638006 -0.0023758611116653778 0 3.8738919747877616e-016 0.0023758611116653223 0.99999717763800611 0
		 17.999999999999783 17.995208764708657 -7.0332564108393569 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 3 ".ma";
	setAttr -s 4 ".dpf[0:3]"  4 4 4 4;
	setAttr -s 3 ".lw";
	setAttr -s 3 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 3 ".ifcl";
	setAttr -s 3 ".ifcl";
createNode tweak -n "tweak8";
	rename -uid "B86DBA1E-4C7E-F41D-C2E6-6E869A2C8780";
createNode objectSet -n "skinCluster8Set";
	rename -uid "5A816B02-49C3-8F26-8224-E8B11D7A2A65";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster8GroupId";
	rename -uid "0A962C3D-409B-59C3-784B-2482DB56DB14";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster8GroupParts";
	rename -uid "4C5381DF-4B0A-6B31-7E8A-6CADC5A6CE3F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet8";
	rename -uid "07E7CAF4-408A-F0B0-8D98-8DA11C9C33F6";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId85";
	rename -uid "56EF9C9C-4857-9462-184F-EF9573030F22";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "01BF1345-4122-7056-9654-25B8D8A6703C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster9";
	rename -uid "7E6C8C4C-43E8-0BAD-8B18-88920303588C";
	setAttr ".skm" 2;
	setAttr -s 314 ".wl";
	setAttr ".wl[0].w[1]"  1;
	setAttr ".wl[1].w[1]"  1;
	setAttr ".wl[2].w[1]"  1;
	setAttr ".wl[3].w[1]"  1;
	setAttr ".wl[4].w[1]"  1;
	setAttr ".wl[5].w[1]"  1;
	setAttr ".wl[6].w[1]"  1;
	setAttr ".wl[7].w[1]"  1;
	setAttr ".wl[8].w[1]"  1;
	setAttr ".wl[9].w[1]"  1;
	setAttr ".wl[10].w[1]"  1;
	setAttr ".wl[11].w[1]"  1;
	setAttr ".wl[12].w[1]"  1;
	setAttr ".wl[13].w[1]"  1;
	setAttr ".wl[14].w[1]"  1;
	setAttr ".wl[15].w[1]"  1;
	setAttr ".wl[16].w[1]"  1;
	setAttr ".wl[17].w[1]"  1;
	setAttr ".wl[18].w[1]"  1;
	setAttr ".wl[19].w[1]"  1;
	setAttr ".wl[20].w[1]"  1;
	setAttr ".wl[21].w[1]"  1;
	setAttr ".wl[22].w[1]"  1;
	setAttr ".wl[23].w[1]"  1;
	setAttr ".wl[24].w[1]"  1;
	setAttr ".wl[25].w[1]"  1;
	setAttr ".wl[26].w[1]"  1;
	setAttr ".wl[27].w[1]"  1;
	setAttr ".wl[28].w[1]"  1;
	setAttr ".wl[29].w[1]"  1;
	setAttr ".wl[30].w[1]"  1;
	setAttr ".wl[31].w[1]"  1;
	setAttr ".wl[32].w[1]"  1;
	setAttr ".wl[33].w[1]"  1;
	setAttr ".wl[34].w[1]"  1;
	setAttr ".wl[35].w[1]"  1;
	setAttr ".wl[36].w[1]"  1;
	setAttr ".wl[37].w[1]"  1;
	setAttr ".wl[38].w[1]"  1;
	setAttr ".wl[39].w[1]"  1;
	setAttr ".wl[40].w[1]"  1;
	setAttr ".wl[41].w[1]"  1;
	setAttr ".wl[42].w[1]"  1;
	setAttr ".wl[43].w[1]"  1;
	setAttr ".wl[44].w[1]"  1;
	setAttr ".wl[45].w[1]"  1;
	setAttr ".wl[46].w[1]"  1;
	setAttr ".wl[47].w[1]"  1;
	setAttr ".wl[48].w[1]"  1;
	setAttr ".wl[49].w[1]"  1;
	setAttr ".wl[50].w[1]"  1;
	setAttr ".wl[51].w[1]"  1;
	setAttr ".wl[52].w[1]"  1;
	setAttr ".wl[53].w[1]"  1;
	setAttr ".wl[54].w[1]"  1;
	setAttr ".wl[55].w[1]"  1;
	setAttr ".wl[56].w[1]"  1;
	setAttr ".wl[57].w[1]"  1;
	setAttr ".wl[58].w[1]"  1;
	setAttr ".wl[59].w[1]"  1;
	setAttr ".wl[60].w[1]"  1;
	setAttr ".wl[61].w[1]"  1;
	setAttr ".wl[62].w[1]"  1;
	setAttr ".wl[63].w[1]"  1;
	setAttr ".wl[64].w[1]"  1;
	setAttr ".wl[65].w[1]"  1;
	setAttr ".wl[66].w[1]"  1;
	setAttr ".wl[67].w[1]"  1;
	setAttr ".wl[68].w[1]"  1;
	setAttr ".wl[69].w[1]"  1;
	setAttr ".wl[70].w[1]"  1;
	setAttr ".wl[71].w[1]"  1;
	setAttr ".wl[72].w[1]"  1;
	setAttr ".wl[73].w[1]"  1;
	setAttr ".wl[74].w[1]"  1;
	setAttr ".wl[75].w[1]"  1;
	setAttr ".wl[76].w[1]"  1;
	setAttr ".wl[77].w[1]"  1;
	setAttr ".wl[78].w[1]"  1;
	setAttr ".wl[79].w[1]"  1;
	setAttr ".wl[80].w[1]"  1;
	setAttr ".wl[81].w[1]"  1;
	setAttr ".wl[82].w[1]"  1;
	setAttr ".wl[83].w[1]"  1;
	setAttr ".wl[84].w[1]"  1;
	setAttr ".wl[85].w[1]"  1;
	setAttr ".wl[86].w[1]"  1;
	setAttr ".wl[87].w[1]"  1;
	setAttr ".wl[88].w[1]"  1;
	setAttr ".wl[89].w[1]"  1;
	setAttr ".wl[90].w[1]"  1;
	setAttr ".wl[91].w[1]"  1;
	setAttr ".wl[92].w[1]"  1;
	setAttr ".wl[93].w[1]"  1;
	setAttr ".wl[94].w[1]"  1;
	setAttr ".wl[95].w[1]"  1;
	setAttr ".wl[96].w[1]"  1;
	setAttr ".wl[97].w[1]"  1;
	setAttr ".wl[98].w[1]"  1;
	setAttr ".wl[99].w[1]"  1;
	setAttr ".wl[100].w[1]"  1;
	setAttr ".wl[101].w[1]"  1;
	setAttr ".wl[102].w[1]"  1;
	setAttr ".wl[103].w[1]"  1;
	setAttr ".wl[104].w[1]"  1;
	setAttr ".wl[105].w[1]"  1;
	setAttr ".wl[106].w[1]"  1;
	setAttr ".wl[107].w[1]"  1;
	setAttr ".wl[108].w[1]"  1;
	setAttr ".wl[109].w[1]"  1;
	setAttr ".wl[110].w[1]"  1;
	setAttr ".wl[111].w[1]"  1;
	setAttr ".wl[112].w[1]"  1;
	setAttr ".wl[113].w[1]"  1;
	setAttr ".wl[114].w[1]"  1;
	setAttr ".wl[115].w[1]"  1;
	setAttr ".wl[116].w[1]"  1;
	setAttr ".wl[117].w[1]"  1;
	setAttr ".wl[118].w[1]"  1;
	setAttr ".wl[119].w[1]"  1;
	setAttr ".wl[120].w[1]"  1;
	setAttr ".wl[121].w[1]"  1;
	setAttr ".wl[122].w[1]"  1;
	setAttr ".wl[123].w[1]"  1;
	setAttr ".wl[124].w[1]"  1;
	setAttr ".wl[125].w[1]"  1;
	setAttr ".wl[126].w[1]"  1;
	setAttr ".wl[127].w[1]"  1;
	setAttr ".wl[128].w[1]"  1;
	setAttr ".wl[129].w[1]"  1;
	setAttr ".wl[130].w[1]"  1;
	setAttr ".wl[131].w[1]"  1;
	setAttr ".wl[132].w[1]"  1;
	setAttr ".wl[133].w[1]"  1;
	setAttr ".wl[134].w[1]"  1;
	setAttr ".wl[135].w[1]"  1;
	setAttr ".wl[136].w[1]"  1;
	setAttr ".wl[137].w[1]"  1;
	setAttr ".wl[138].w[1]"  1;
	setAttr ".wl[139].w[1]"  1;
	setAttr ".wl[140].w[1]"  1;
	setAttr ".wl[141].w[1]"  1;
	setAttr ".wl[142].w[1]"  1;
	setAttr ".wl[143].w[1]"  1;
	setAttr ".wl[144].w[1]"  1;
	setAttr ".wl[145].w[1]"  1;
	setAttr ".wl[146].w[1]"  1;
	setAttr ".wl[147].w[1]"  1;
	setAttr ".wl[148].w[1]"  1;
	setAttr ".wl[149].w[1]"  1;
	setAttr ".wl[150].w[1]"  1;
	setAttr ".wl[151].w[1]"  1;
	setAttr ".wl[152].w[1]"  1;
	setAttr ".wl[153].w[1]"  1;
	setAttr ".wl[154].w[1]"  1;
	setAttr ".wl[155].w[1]"  1;
	setAttr ".wl[156].w[1]"  1;
	setAttr ".wl[157].w[1]"  1;
	setAttr ".wl[158].w[1]"  1;
	setAttr ".wl[159].w[1]"  1;
	setAttr ".wl[160].w[1]"  1;
	setAttr ".wl[161].w[1]"  1;
	setAttr ".wl[162].w[1]"  1;
	setAttr ".wl[163].w[1]"  1;
	setAttr ".wl[164].w[1]"  1;
	setAttr ".wl[165].w[1]"  1;
	setAttr ".wl[166].w[1]"  1;
	setAttr ".wl[167].w[1]"  1;
	setAttr ".wl[168].w[1]"  1;
	setAttr ".wl[169].w[1]"  1;
	setAttr ".wl[170].w[1]"  1;
	setAttr ".wl[171].w[1]"  1;
	setAttr ".wl[172].w[1]"  1;
	setAttr ".wl[173].w[1]"  1;
	setAttr ".wl[174].w[1]"  1;
	setAttr ".wl[175].w[1]"  1;
	setAttr ".wl[176].w[1]"  1;
	setAttr ".wl[177].w[1]"  1;
	setAttr ".wl[178].w[1]"  1;
	setAttr ".wl[179].w[1]"  1;
	setAttr ".wl[180].w[1]"  1;
	setAttr ".wl[181].w[1]"  1;
	setAttr ".wl[182].w[1]"  1;
	setAttr ".wl[183].w[1]"  1;
	setAttr ".wl[184].w[1]"  1;
	setAttr ".wl[185].w[1]"  1;
	setAttr ".wl[186].w[1]"  1;
	setAttr ".wl[187].w[1]"  1;
	setAttr ".wl[188].w[1]"  1;
	setAttr ".wl[189].w[1]"  1;
	setAttr ".wl[190].w[1]"  1;
	setAttr ".wl[191].w[1]"  1;
	setAttr ".wl[192].w[1]"  1;
	setAttr ".wl[193].w[1]"  1;
	setAttr ".wl[194].w[1]"  1;
	setAttr ".wl[195].w[1]"  1;
	setAttr ".wl[196].w[1]"  1;
	setAttr ".wl[197].w[1]"  1;
	setAttr ".wl[198].w[1]"  1;
	setAttr ".wl[199].w[1]"  1;
	setAttr ".wl[200].w[1]"  1;
	setAttr ".wl[201].w[1]"  1;
	setAttr ".wl[202].w[1]"  1;
	setAttr ".wl[203].w[1]"  1;
	setAttr ".wl[204].w[1]"  1;
	setAttr ".wl[205].w[1]"  1;
	setAttr ".wl[206].w[1]"  1;
	setAttr ".wl[207].w[1]"  1;
	setAttr ".wl[208].w[1]"  1;
	setAttr ".wl[209].w[1]"  1;
	setAttr ".wl[210].w[1]"  1;
	setAttr ".wl[211].w[1]"  1;
	setAttr ".wl[212].w[1]"  1;
	setAttr ".wl[213].w[1]"  1;
	setAttr ".wl[214].w[1]"  1;
	setAttr ".wl[215].w[1]"  1;
	setAttr ".wl[216].w[1]"  1;
	setAttr ".wl[217].w[1]"  1;
	setAttr ".wl[218].w[1]"  1;
	setAttr ".wl[219].w[1]"  1;
	setAttr ".wl[220].w[1]"  1;
	setAttr ".wl[221].w[1]"  1;
	setAttr ".wl[222].w[1]"  1;
	setAttr ".wl[223].w[1]"  1;
	setAttr ".wl[224].w[1]"  1;
	setAttr ".wl[225].w[1]"  1;
	setAttr ".wl[226].w[1]"  1;
	setAttr ".wl[227].w[1]"  1;
	setAttr ".wl[228].w[1]"  1;
	setAttr ".wl[229].w[1]"  1;
	setAttr ".wl[230].w[1]"  1;
	setAttr ".wl[231].w[1]"  1;
	setAttr ".wl[232].w[1]"  1;
	setAttr ".wl[233].w[1]"  1;
	setAttr ".wl[234].w[1]"  1;
	setAttr ".wl[235].w[1]"  1;
	setAttr ".wl[236].w[1]"  1;
	setAttr ".wl[237].w[1]"  1;
	setAttr ".wl[238].w[1]"  1;
	setAttr ".wl[239].w[1]"  1;
	setAttr ".wl[240].w[1]"  1;
	setAttr ".wl[241].w[1]"  1;
	setAttr ".wl[242].w[1]"  1;
	setAttr ".wl[243].w[1]"  1;
	setAttr ".wl[244].w[1]"  1;
	setAttr ".wl[245].w[1]"  1;
	setAttr ".wl[246].w[1]"  1;
	setAttr ".wl[247].w[1]"  1;
	setAttr ".wl[248].w[1]"  1;
	setAttr ".wl[249].w[1]"  1;
	setAttr ".wl[250].w[1]"  1;
	setAttr ".wl[251].w[1]"  1;
	setAttr ".wl[252].w[1]"  1;
	setAttr ".wl[253].w[1]"  1;
	setAttr ".wl[254].w[1]"  1;
	setAttr ".wl[255].w[1]"  1;
	setAttr ".wl[256].w[1]"  1;
	setAttr ".wl[257].w[1]"  1;
	setAttr ".wl[258].w[1]"  1;
	setAttr ".wl[259].w[1]"  1;
	setAttr ".wl[260].w[1]"  1;
	setAttr ".wl[261].w[1]"  1;
	setAttr ".wl[262].w[1]"  1;
	setAttr ".wl[263].w[1]"  1;
	setAttr ".wl[264].w[1]"  1;
	setAttr ".wl[265].w[1]"  1;
	setAttr ".wl[266].w[1]"  1;
	setAttr ".wl[267].w[1]"  1;
	setAttr ".wl[268].w[1]"  1;
	setAttr ".wl[269].w[1]"  1;
	setAttr ".wl[270].w[1]"  1;
	setAttr ".wl[271].w[1]"  1;
	setAttr ".wl[272].w[1]"  1;
	setAttr ".wl[273].w[1]"  1;
	setAttr ".wl[274].w[1]"  1;
	setAttr ".wl[275].w[1]"  1;
	setAttr ".wl[276].w[1]"  1;
	setAttr ".wl[277].w[1]"  1;
	setAttr ".wl[278].w[1]"  1;
	setAttr ".wl[279].w[1]"  1;
	setAttr ".wl[280].w[1]"  1;
	setAttr ".wl[281].w[1]"  1;
	setAttr ".wl[282].w[1]"  1;
	setAttr ".wl[283].w[1]"  1;
	setAttr ".wl[284].w[1]"  1;
	setAttr ".wl[285].w[1]"  1;
	setAttr ".wl[286].w[1]"  1;
	setAttr ".wl[287].w[1]"  1;
	setAttr ".wl[288].w[1]"  1;
	setAttr ".wl[289].w[1]"  1;
	setAttr ".wl[290].w[1]"  1;
	setAttr ".wl[291].w[1]"  1;
	setAttr ".wl[292].w[1]"  1;
	setAttr ".wl[293].w[1]"  1;
	setAttr ".wl[294].w[1]"  1;
	setAttr ".wl[295].w[1]"  1;
	setAttr ".wl[296].w[1]"  1;
	setAttr ".wl[297].w[1]"  1;
	setAttr ".wl[298].w[1]"  1;
	setAttr ".wl[299].w[1]"  1;
	setAttr ".wl[300].w[1]"  1;
	setAttr ".wl[301].w[1]"  1;
	setAttr ".wl[302].w[1]"  1;
	setAttr ".wl[303].w[1]"  1;
	setAttr ".wl[304].w[1]"  1;
	setAttr ".wl[305].w[1]"  1;
	setAttr ".wl[306].w[1]"  1;
	setAttr ".wl[307].w[1]"  1;
	setAttr ".wl[308].w[1]"  1;
	setAttr ".wl[309].w[1]"  1;
	setAttr ".wl[310].w[1]"  1;
	setAttr ".wl[311].w[1]"  1;
	setAttr ".wl[312].w[1]"  1;
	setAttr ".wl[313].w[1]"  1;
	setAttr -s 7 ".pm";
	setAttr ".pm[0]" -type "matrix" 7.2164496600635106e-016 -4.0059342843254369e-032 1 0
		 1.0000000000000002 -5.5511151231257617e-017 -7.7715611723760978e-016 0 -5.2385294487332815e-031 1 -1.1102230246251565e-016 0
		 8.0000000000000302 -4.4501546691732532e-015 -9.1038288019262947e-015 1;
	setAttr ".pm[1]" -type "matrix" 7.2164496600635195e-016 -4.0059342843254369e-032 1 0
		 1.0000000000000002 -5.5511151231257617e-017 -7.7715611723760978e-016 0 -8.0118685686509033e-032 1 -1.1102230246251565e-016 0
		 -2.664535259100382e-015 -6.0725335168213404e-015 -7.4814499542781829e-015 1;
	setAttr ".pm[2]" -type "matrix" -0.99999999999999978 -4.0059342843254079e-032 9.4368957093138483e-016 0
		 9.9920072216264089e-016 -5.5511151231257396e-017 1 0 1.1102230246251563e-016 1 -1.0477058897466583e-031 0
		 7.4814499542782097e-015 -6.0725335168213397e-015 -2.6645352591003816e-015 1;
	setAttr ".pm[3]" -type "matrix" -0.98639392383214131 1.1985407815323346e-030 0.1643989873053619 0
		 0.16439898730535774 -5.5511151231258505e-017 0.98639392383214453 0 1.0951172455888006e-016 1 -1.8251954093145163e-017 0
		 -11.836727085985727 -6.0725335168213199e-015 1.9727878476643288 1;
	setAttr ".pm[4]" -type "matrix" 0.99999999999999978 -4.0059342843254265e-032 -4.9960036108132054e-016 0
		 -5.5511151231257837e-016 -5.5511151231257396e-017 -1 0 -1.1102230246251563e-016 1 5.5466782398352404e-032 0
		 -7.4814499542781687e-015 -6.0725335168213397e-015 2.6645352591003781e-015 1;
	setAttr ".pm[5]" -type "matrix" 0.98639392383214175 5.0706162847928988e-031 0.16439898730535801 0
		 0.16439898730535499 -5.5511151231258394e-017 -0.98639392383214397 0 -1.0951172455888011e-016 1 -1.8251954093144066e-017 0
		 -11.836727085985757 -6.0725335168213688e-015 -1.9727878476642755 1;
	setAttr ".pm[6]" -type "matrix" 1 2.7755575615628923e-016 -1.6331885928403767e-031 -0
		 -3.3306690738754696e-016 1.0000000000000002 -4.9960036108132054e-016 0 -1.1102230246251547e-016 4.4408920985006262e-016 1 -0
		 -7.4814499542781798e-015 -3.0000000000000031 -6.0725335168213444e-015 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 7 ".dpf[0:6]"  4 4 4 4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
createNode groupId -n "groupId86";
	rename -uid "5A1B7A03-4167-0917-DCAA-3ABD8DC2E4F7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts19";
	rename -uid "36A89176-4323-1D5B-EA2A-80B619F2BFA5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:280]";
createNode tweak -n "tweak9";
	rename -uid "426E1C71-4825-DAD2-61CA-489E01D00398";
createNode objectSet -n "skinCluster9Set";
	rename -uid "09ACF6BE-4800-FD38-90FA-B3A4982ED46F";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster9GroupId";
	rename -uid "7670557B-4CD3-EE16-DE70-C7A78858AA88";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster9GroupParts";
	rename -uid "3A212719-4E83-A344-1B6D-89AF65828E0E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet9";
	rename -uid "49317B4C-40C3-3534-5E38-1F9BB5776487";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId88";
	rename -uid "E51167D3-4B53-CF33-1745-FDB2FD2EB903";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts21";
	rename -uid "8120FBF3-4DAA-5DB0-250C-DC8CC0FA0001";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose4";
	rename -uid "F7BE7969-493F-E15D-BD8F-F495D2788CE0";
	setAttr -s 4 ".wm";
	setAttr -s 12 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.8865798640254062e-015
		 -7.9999999999999964 4.0060654593231834e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.50000000000000022 0.49999999999999983 0.49999999999999978 0.50000000000000011 1
		 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.4814499542781845e-015
		 2.6645352591003757e-015 5.1843550971212207e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0.50000000000000011 0.49999999999999978 0.49999999999999989 0.50000000000000033 1
		 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 -7.8886090522101181e-031
		 0 1.5777218104420236e-030 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.70710678118654746 0 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 11.999999999999995 -4.7331654313260708e-030
		 -1.4432899320127e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.082480531544892891 0 0.99659267602971668 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 -7.8886090522101181e-031
		 0 1.5777218104420236e-030 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.70710678118654746 0 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 12.000000000000007 7.8886090522101181e-030
		 5.3290705182007435e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.082480531544892891 0 0.99659267602971668 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 3 1.3322676295501878e-015
		 -1.3322676295501914e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.5 -0.5 -0.49999999999999978 0.50000000000000022 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 3 1.3322676295501878e-015
		 -1.3322676295501914e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.5 -0.5 -0.49999999999999978 0.50000000000000022 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 3 1.3322676295501878e-015
		 -1.3322676295501914e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.5 -0.5 -0.49999999999999978 0.50000000000000022 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 3 1.3322676295501878e-015
		 -1.3322676295501914e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.5 -0.5 -0.49999999999999978 0.50000000000000022 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 3 1.3322676295501878e-015
		 -1.3322676295501914e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.5 -0.5 -0.49999999999999978 0.50000000000000022 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 3 1.3322676295501878e-015
		 -1.3322676295501914e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.5 -0.5 -0.49999999999999978 0.50000000000000022 1
		 1 1 yes;
	setAttr -s 6 ".m";
	setAttr -s 6 ".p";
	setAttr -s 12 ".g[2:11]" yes no yes no no no no no no no;
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster10";
	rename -uid "33D9ED4F-4316-8681-B124-80BF1B44DF6B";
	setAttr ".skm" 2;
	setAttr -s 170 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr ".wl[8].w[0]"  1;
	setAttr ".wl[9].w[0]"  1;
	setAttr ".wl[10].w[0]"  1;
	setAttr ".wl[11].w[0]"  1;
	setAttr ".wl[12].w[0]"  1;
	setAttr ".wl[13].w[0]"  1;
	setAttr ".wl[14].w[0]"  1;
	setAttr ".wl[15].w[0]"  1;
	setAttr ".wl[16].w[0]"  1;
	setAttr ".wl[17].w[0]"  1;
	setAttr ".wl[18].w[0]"  1;
	setAttr ".wl[19].w[0]"  1;
	setAttr ".wl[20].w[0]"  1;
	setAttr ".wl[21].w[0]"  1;
	setAttr ".wl[22].w[0]"  1;
	setAttr ".wl[23].w[0]"  1;
	setAttr ".wl[24].w[0]"  1;
	setAttr ".wl[25].w[0]"  1;
	setAttr ".wl[26].w[0]"  1;
	setAttr ".wl[27].w[0]"  1;
	setAttr ".wl[28].w[0]"  1;
	setAttr ".wl[29].w[0]"  1;
	setAttr ".wl[30].w[0]"  1;
	setAttr ".wl[31].w[0]"  1;
	setAttr ".wl[32].w[0]"  1;
	setAttr ".wl[33].w[0]"  1;
	setAttr ".wl[34].w[0]"  1;
	setAttr ".wl[35].w[0]"  1;
	setAttr ".wl[36].w[0]"  1;
	setAttr ".wl[37].w[0]"  1;
	setAttr ".wl[38].w[0]"  1;
	setAttr ".wl[39].w[0]"  1;
	setAttr ".wl[40].w[0]"  1;
	setAttr ".wl[41].w[0]"  1;
	setAttr ".wl[42].w[0]"  1;
	setAttr ".wl[43].w[0]"  1;
	setAttr ".wl[44].w[0]"  1;
	setAttr ".wl[45].w[0]"  1;
	setAttr ".wl[46].w[0]"  1;
	setAttr ".wl[47].w[0]"  1;
	setAttr ".wl[48].w[0]"  1;
	setAttr ".wl[49].w[0]"  1;
	setAttr ".wl[50].w[0]"  1;
	setAttr ".wl[51].w[0]"  1;
	setAttr ".wl[52].w[0]"  1;
	setAttr ".wl[53].w[0]"  1;
	setAttr ".wl[54].w[0]"  1;
	setAttr ".wl[55].w[0]"  1;
	setAttr ".wl[56].w[0]"  1;
	setAttr ".wl[57].w[0]"  1;
	setAttr ".wl[58].w[0]"  1;
	setAttr ".wl[59].w[0]"  1;
	setAttr ".wl[60].w[0]"  1;
	setAttr ".wl[61].w[0]"  1;
	setAttr ".wl[62].w[0]"  1;
	setAttr ".wl[63].w[0]"  1;
	setAttr ".wl[64].w[0]"  1;
	setAttr ".wl[65].w[0]"  1;
	setAttr ".wl[66].w[0]"  1;
	setAttr ".wl[67].w[0]"  1;
	setAttr ".wl[68].w[0]"  1;
	setAttr ".wl[69].w[0]"  1;
	setAttr ".wl[70].w[0]"  1;
	setAttr ".wl[71].w[0]"  1;
	setAttr ".wl[72].w[0]"  1;
	setAttr ".wl[73].w[0]"  1;
	setAttr ".wl[74].w[0]"  1;
	setAttr ".wl[75].w[0]"  1;
	setAttr ".wl[76].w[0]"  1;
	setAttr ".wl[77].w[0]"  1;
	setAttr ".wl[78].w[0]"  1;
	setAttr ".wl[79].w[0]"  1;
	setAttr ".wl[80].w[0]"  1;
	setAttr ".wl[81].w[0]"  1;
	setAttr ".wl[82].w[0]"  1;
	setAttr ".wl[83].w[0]"  1;
	setAttr ".wl[84].w[0]"  1;
	setAttr ".wl[85].w[0]"  1;
	setAttr ".wl[86].w[0]"  1;
	setAttr ".wl[87].w[0]"  1;
	setAttr ".wl[88].w[0]"  1;
	setAttr ".wl[89].w[0]"  1;
	setAttr ".wl[90].w[0]"  1;
	setAttr ".wl[91].w[0]"  1;
	setAttr ".wl[92].w[0]"  1;
	setAttr ".wl[93].w[0]"  1;
	setAttr ".wl[94].w[0]"  1;
	setAttr ".wl[95].w[0]"  1;
	setAttr ".wl[96].w[0]"  1;
	setAttr ".wl[97].w[0]"  1;
	setAttr ".wl[98].w[0]"  1;
	setAttr ".wl[99].w[0]"  1;
	setAttr ".wl[100].w[0]"  1;
	setAttr ".wl[101].w[0]"  1;
	setAttr ".wl[102].w[0]"  1;
	setAttr ".wl[103].w[0]"  1;
	setAttr ".wl[104].w[0]"  1;
	setAttr ".wl[105].w[0]"  1;
	setAttr ".wl[106].w[0]"  1;
	setAttr ".wl[107].w[0]"  1;
	setAttr ".wl[108].w[0]"  1;
	setAttr ".wl[109].w[0]"  1;
	setAttr ".wl[110].w[0]"  1;
	setAttr ".wl[111].w[0]"  1;
	setAttr ".wl[112].w[0]"  1;
	setAttr ".wl[113].w[0]"  1;
	setAttr ".wl[114].w[0]"  1;
	setAttr ".wl[115].w[0]"  1;
	setAttr ".wl[116].w[0]"  1;
	setAttr ".wl[117].w[0]"  1;
	setAttr ".wl[118].w[0]"  1;
	setAttr ".wl[119].w[0]"  1;
	setAttr ".wl[120].w[0]"  1;
	setAttr ".wl[121].w[0]"  1;
	setAttr ".wl[122].w[0]"  1;
	setAttr ".wl[123].w[0]"  1;
	setAttr ".wl[124].w[0]"  1;
	setAttr ".wl[125].w[0]"  1;
	setAttr ".wl[126].w[0]"  1;
	setAttr ".wl[127].w[0]"  1;
	setAttr ".wl[128].w[0]"  1;
	setAttr ".wl[129].w[0]"  1;
	setAttr ".wl[130].w[0]"  1;
	setAttr ".wl[131].w[0]"  1;
	setAttr ".wl[132].w[0]"  1;
	setAttr ".wl[133].w[0]"  1;
	setAttr ".wl[134].w[0]"  1;
	setAttr ".wl[135].w[0]"  1;
	setAttr ".wl[136].w[0]"  1;
	setAttr ".wl[137].w[0]"  1;
	setAttr ".wl[138].w[0]"  1;
	setAttr ".wl[139].w[0]"  1;
	setAttr ".wl[140].w[0]"  1;
	setAttr ".wl[141].w[0]"  1;
	setAttr ".wl[142].w[0]"  1;
	setAttr ".wl[143].w[0]"  1;
	setAttr ".wl[144].w[0]"  1;
	setAttr ".wl[145].w[0]"  1;
	setAttr ".wl[146].w[0]"  1;
	setAttr ".wl[147].w[0]"  1;
	setAttr ".wl[148].w[0]"  1;
	setAttr ".wl[149].w[0]"  1;
	setAttr ".wl[150].w[0]"  1;
	setAttr ".wl[151].w[0]"  1;
	setAttr ".wl[152].w[0]"  1;
	setAttr ".wl[153].w[0]"  1;
	setAttr ".wl[154].w[0]"  1;
	setAttr ".wl[155].w[0]"  1;
	setAttr ".wl[156].w[0]"  1;
	setAttr ".wl[157].w[0]"  1;
	setAttr ".wl[158].w[0]"  1;
	setAttr ".wl[159].w[0]"  1;
	setAttr ".wl[160].w[0]"  1;
	setAttr ".wl[161].w[0]"  1;
	setAttr ".wl[162].w[0]"  1;
	setAttr ".wl[163].w[0]"  1;
	setAttr ".wl[164].w[0]"  1;
	setAttr ".wl[165].w[0]"  1;
	setAttr ".wl[166].w[0]"  1;
	setAttr ".wl[167].w[0]"  1;
	setAttr ".wl[168].w[0]"  1;
	setAttr ".wl[169].w[0]"  1;
	setAttr -s 7 ".pm";
	setAttr ".pm[0]" -type "matrix" 7.2164496600635106e-016 -4.0059342843254369e-032 1 0
		 1.0000000000000002 -5.5511151231257617e-017 -7.7715611723760978e-016 0 -5.2385294487332815e-031 1 -1.1102230246251565e-016 0
		 8.0000000000000302 -4.4501546691732532e-015 -9.1038288019262947e-015 1;
	setAttr ".pm[1]" -type "matrix" 7.2164496600635195e-016 -4.0059342843254369e-032 1 0
		 1.0000000000000002 -5.5511151231257617e-017 -7.7715611723760978e-016 0 -8.0118685686509033e-032 1 -1.1102230246251565e-016 0
		 -2.664535259100382e-015 -6.0725335168213404e-015 -7.4814499542781829e-015 1;
	setAttr ".pm[2]" -type "matrix" -0.99999999999999978 -4.0059342843254079e-032 9.4368957093138483e-016 0
		 9.9920072216264089e-016 -5.5511151231257396e-017 1 0 1.1102230246251563e-016 1 -1.0477058897466583e-031 0
		 7.4814499542782097e-015 -6.0725335168213397e-015 -2.6645352591003816e-015 1;
	setAttr ".pm[3]" -type "matrix" -0.98639392383214131 1.1985407815323346e-030 0.1643989873053619 0
		 0.16439898730535774 -5.5511151231258505e-017 0.98639392383214453 0 1.0951172455888006e-016 1 -1.8251954093145163e-017 0
		 -11.836727085985727 -6.0725335168213199e-015 1.9727878476643288 1;
	setAttr ".pm[4]" -type "matrix" 0.99999999999999978 -4.0059342843254265e-032 -4.9960036108132054e-016 0
		 -5.5511151231257837e-016 -5.5511151231257396e-017 -1 0 -1.1102230246251563e-016 1 5.5466782398352404e-032 0
		 -7.4814499542781687e-015 -6.0725335168213397e-015 2.6645352591003781e-015 1;
	setAttr ".pm[5]" -type "matrix" 0.98639392383214175 5.0706162847928988e-031 0.16439898730535801 0
		 0.16439898730535499 -5.5511151231258394e-017 -0.98639392383214397 0 -1.0951172455888011e-016 1 -1.8251954093144066e-017 0
		 -11.836727085985757 -6.0725335168213688e-015 -1.9727878476642755 1;
	setAttr ".pm[6]" -type "matrix" 1 2.7755575615628923e-016 -1.6331885928403767e-031 -0
		 -3.3306690738754696e-016 1.0000000000000002 -4.9960036108132054e-016 0 -1.1102230246251547e-016 4.4408920985006262e-016 1 -0
		 -7.4814499542781798e-015 -3.0000000000000031 -6.0725335168213444e-015 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 7 ".dpf[0:6]"  4 4 4 4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak10";
	rename -uid "487A1401-42CC-56E7-5355-DB8A8741FF80";
createNode objectSet -n "skinCluster10Set";
	rename -uid "ABD3194E-4470-6BE1-7735-2382F9795AB8";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster10GroupId";
	rename -uid "B0914569-458B-8509-297B-188E64D1D0B0";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster10GroupParts";
	rename -uid "155E6539-420A-B73E-BFB1-4FA5C58E7793";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet10";
	rename -uid "7B53FAD5-467E-8B6F-EDC6-69BAFC5CB5B9";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId90";
	rename -uid "6D9BAE08-4C86-14ED-B0AE-4D8DFF0608F7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts23";
	rename -uid "FF9CF4B9-452D-55C3-5A0D-D2982A5B5E03";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster11";
	rename -uid "67F0B652-477B-6FA2-13AD-DBA46644BF65";
	setAttr ".skm" 2;
	setAttr -s 8 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr -s 6 ".pm";
	setAttr ".pm[0]" -type "matrix" 7.7715611723760958e-016 -2.2204460492503121e-016 1 0
		 1 1.1102230246251602e-016 -6.6613381477509392e-016 0 2.588449845256445e-031 1 3.3306690738754696e-016 0
		 -2.6645352591003816e-015 -5.1843550971212121e-015 -7.4814499542781845e-015 1;
	setAttr ".pm[1]" -type "matrix" -0.99999999999999978 -2.2204460492503126e-016 9.9920072216264246e-016 0
		 8.8817841970012504e-016 1.1102230246251625e-016 0.99999999999999978 0 -3.3306690738754691e-016 1 3.3280069439011471e-031 0
		 7.4814499542782129e-015 -5.1843550971212113e-015 -2.6645352591003816e-015 1;
	setAttr ".pm[2]" -type "matrix" -0.98639392383214131 -2.2204460492502993e-016 0.16439898730536195 0
		 0.1643989873053576 1.1102230246251511e-016 0.9863939238321443 0 -3.2853517367663869e-016 1 5.4755862279443496e-017 0
		 -11.836727085985729 -5.1843550971211931e-015 1.9727878476643286 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 -2.2204460492503118e-016 -5.5511151231257817e-016 0
		 -4.4408920985006217e-016 1.1102230246251625e-016 -0.99999999999999978 0 3.3306690738754691e-016 1 -1.8488927466117462e-031 0
		 -7.4814499542781703e-015 -5.1843550971212113e-015 2.6645352591003785e-015 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214375 -2.2204460492503121e-016 0.16439898730535593 -0
		 0.16439898730535604 1.1102230246251583e-016 -0.98639392383214375 0 3.2853517367663967e-016 1 5.4755862279439496e-017 -0
		 -11.836727085985741 -5.1843550971212247e-015 -1.9727878476642833 1;
	setAttr ".pm[5]" -type "matrix" 1 3.3306690738754686e-016 -2.2204460492503136e-016 -0
		 -2.2204460492503123e-016 1 -3.3306690738754681e-016 0 3.3306690738754716e-016 4.4408920985006271e-016 1 -0
		 -7.4814499542781829e-015 -3.0000000000000027 -5.1843550971212184e-015 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 2 ".ma";
	setAttr -s 6 ".dpf[0:5]"  4 4 4 4 4 4;
	setAttr -s 2 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 2 ".ifcl";
createNode tweak -n "tweak11";
	rename -uid "6ED34841-4C00-9B70-1E6E-B487DD0C9CB0";
createNode objectSet -n "skinCluster11Set";
	rename -uid "E82D0A58-4261-D476-D158-F7A12B295BE5";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster11GroupId";
	rename -uid "EBA6ACE1-48F1-1FEF-1270-718A7E50A02D";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster11GroupParts";
	rename -uid "D9908315-468E-3E31-C090-FC9994516621";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet11";
	rename -uid "6D7A4FE3-4E1B-45E0-41AD-C4A1B2B1A0CA";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId92";
	rename -uid "100BA6CE-40C1-6BC5-8A2E-E98D1C35C362";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts25";
	rename -uid "86BBACD5-405B-1F6A-ACBB-9E8FBF838E52";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster12";
	rename -uid "62DF2857-4F20-CC6A-0E2E-59AA0C5382EB";
	setAttr ".skm" 2;
	setAttr -s 8 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr -s 6 ".pm";
	setAttr ".pm[0]" -type "matrix" 7.7715611723760958e-016 -2.2204460492503121e-016 1 0
		 1 1.1102230246251602e-016 -6.6613381477509392e-016 0 2.588449845256445e-031 1 3.3306690738754696e-016 0
		 -2.6645352591003816e-015 -5.1843550971212121e-015 -7.4814499542781845e-015 1;
	setAttr ".pm[1]" -type "matrix" -0.99999999999999978 -2.2204460492503126e-016 9.9920072216264246e-016 0
		 8.8817841970012504e-016 1.1102230246251625e-016 0.99999999999999978 0 -3.3306690738754691e-016 1 3.3280069439011471e-031 0
		 7.4814499542782129e-015 -5.1843550971212113e-015 -2.6645352591003816e-015 1;
	setAttr ".pm[2]" -type "matrix" -0.98639392383214131 -2.2204460492502993e-016 0.16439898730536195 0
		 0.1643989873053576 1.1102230246251511e-016 0.9863939238321443 0 -3.2853517367663869e-016 1 5.4755862279443496e-017 0
		 -11.836727085985729 -5.1843550971211931e-015 1.9727878476643286 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 -2.2204460492503118e-016 -5.5511151231257817e-016 -0
		 -4.4408920985006262e-016 1.110223024625158e-016 -0.99999999999999978 0 3.3306690738754691e-016 1 -1.8488927466117462e-031 -0
		 -7.4814499542781845e-015 -5.1843550971212184e-015 2.6645352591003785e-015 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214375 -2.2204460492503121e-016 0.16439898730535593 -0
		 0.16439898730535604 1.1102230246251583e-016 -0.98639392383214375 0 3.2853517367663967e-016 1 5.4755862279439496e-017 -0
		 -11.836727085985741 -5.1843550971212247e-015 -1.9727878476642833 1;
	setAttr ".pm[5]" -type "matrix" 1 3.3306690738754686e-016 -2.2204460492503136e-016 -0
		 -2.2204460492503123e-016 1 -3.3306690738754681e-016 0 3.3306690738754716e-016 4.4408920985006271e-016 1 -0
		 -7.4814499542781829e-015 -3.0000000000000027 -5.1843550971212184e-015 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 2 ".ma";
	setAttr -s 6 ".dpf[0:5]"  4 4 4 4 4 4;
	setAttr -s 2 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 2 ".ifcl";
createNode tweak -n "tweak12";
	rename -uid "68386077-430B-DD9F-6FA8-43AF6C6EF1B4";
createNode objectSet -n "skinCluster12Set";
	rename -uid "F1E4B7C7-4DE5-5D1C-27A0-EBA27CDCCEDA";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster12GroupId";
	rename -uid "EA7700D5-414F-FF64-6881-338190B27AB6";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster12GroupParts";
	rename -uid "2E6941B0-4B13-15A7-B7EF-F88A941E44E0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet12";
	rename -uid "3E72CD9D-4778-7ADE-1634-2B829E0C78C6";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId94";
	rename -uid "C9AF9701-482E-551D-06EC-648977D9FBEB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts27";
	rename -uid "DF9DE52C-47B2-6B5A-5334-4EBDF6E0F60B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster13";
	rename -uid "2A0F711A-41C2-3A94-EF6A-4191BFB9F6FC";
	setAttr ".skm" 2;
	setAttr -s 8 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr -s 6 ".pm";
	setAttr ".pm[0]" -type "matrix" 7.7715611723760958e-016 -2.2204460492503121e-016 1 0
		 1 1.1102230246251602e-016 -6.6613381477509392e-016 0 2.588449845256445e-031 1 3.3306690738754696e-016 0
		 -2.6645352591003816e-015 -5.1843550971212121e-015 -7.4814499542781845e-015 1;
	setAttr ".pm[1]" -type "matrix" -0.99999999999999978 -2.2204460492503126e-016 9.9920072216264246e-016 0
		 8.8817841970012504e-016 1.1102230246251625e-016 0.99999999999999978 0 -3.3306690738754691e-016 1 3.3280069439011471e-031 0
		 7.4814499542782129e-015 -5.1843550971212113e-015 -2.6645352591003816e-015 1;
	setAttr ".pm[2]" -type "matrix" -0.9863939238321433 -2.2204460492503126e-016 0.16439898730535746 -0
		 0.16439898730535735 1.1102230246251578e-016 0.9863939238321433 -0 -3.2853517367663958e-016 1 5.4755862279440001e-017 -0
		 -11.836727085985713 -5.1843550971212144e-015 1.9727878476642746 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 -2.2204460492503118e-016 -5.5511151231257817e-016 0
		 -4.4408920985006217e-016 1.1102230246251625e-016 -0.99999999999999978 0 3.3306690738754691e-016 1 -1.8488927466117462e-031 0
		 -7.4814499542781703e-015 -5.1843550971212113e-015 2.6645352591003785e-015 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214175 -2.2204460492503077e-016 0.16439898730535793 0
		 0.16439898730535504 1.1102230246251523e-016 -0.98639392383214375 0 3.2853517367663879e-016 1 5.4755862279442824e-017 0
		 -11.836727085985757 -5.1843550971212389e-015 -1.9727878476642753 1;
	setAttr ".pm[5]" -type "matrix" 1 3.3306690738754686e-016 -2.2204460492503136e-016 -0
		 -2.2204460492503123e-016 1 -3.3306690738754681e-016 0 3.3306690738754716e-016 4.4408920985006271e-016 1 -0
		 -7.4814499542781829e-015 -3.0000000000000027 -5.1843550971212184e-015 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 2 ".ma";
	setAttr -s 6 ".dpf[0:5]"  4 4 4 4 4 4;
	setAttr -s 2 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 2 ".ifcl";
createNode tweak -n "tweak13";
	rename -uid "ADAAB486-47B4-933D-06F0-13A877306593";
createNode objectSet -n "skinCluster13Set";
	rename -uid "A2879EDA-4100-65E8-E558-8BBB20F3ABD6";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster13GroupId";
	rename -uid "513FF8AF-4D56-6B6F-B936-63ADB39BCC94";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster13GroupParts";
	rename -uid "B16EA449-4675-5015-0E10-95B35524F78A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet13";
	rename -uid "0CB97F13-4058-9B26-8D16-A197B0BCBB30";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId96";
	rename -uid "FDA26623-4EA9-AA96-81B7-7C80D1F2FB96";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts29";
	rename -uid "C9DCA47B-4389-B0A4-1161-5090A13915DC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster14";
	rename -uid "EC581949-4512-74DD-C340-4F82DC0696A0";
	setAttr ".skm" 2;
	setAttr -s 8 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr -s 6 ".pm";
	setAttr ".pm[0]" -type "matrix" 7.7715611723760958e-016 -2.2204460492503121e-016 1 0
		 1 1.1102230246251602e-016 -6.6613381477509392e-016 0 2.588449845256445e-031 1 3.3306690738754696e-016 0
		 -2.6645352591003816e-015 -5.1843550971212121e-015 -7.4814499542781845e-015 1;
	setAttr ".pm[1]" -type "matrix" -0.99999999999999978 -2.2204460492503126e-016 9.9920072216264069e-016 -0
		 8.8817841970012504e-016 1.110223024625158e-016 0.99999999999999978 -0 -3.3306690738754691e-016 1 3.3280069439011431e-031 -0
		 7.4814499542781845e-015 -5.1843550971212184e-015 -2.6645352591003816e-015 1;
	setAttr ".pm[2]" -type "matrix" -0.9863939238321433 -2.2204460492503126e-016 0.16439898730535746 -0
		 0.16439898730535735 1.1102230246251578e-016 0.9863939238321433 -0 -3.2853517367663958e-016 1 5.4755862279440001e-017 -0
		 -11.836727085985713 -5.1843550971212144e-015 1.9727878476642746 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 -2.2204460492503118e-016 -5.5511151231257817e-016 0
		 -4.4408920985006217e-016 1.1102230246251625e-016 -0.99999999999999978 0 3.3306690738754691e-016 1 -1.8488927466117462e-031 0
		 -7.4814499542781703e-015 -5.1843550971212113e-015 2.6645352591003785e-015 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214175 -2.2204460492503077e-016 0.16439898730535793 0
		 0.16439898730535504 1.1102230246251523e-016 -0.98639392383214375 0 3.2853517367663879e-016 1 5.4755862279442824e-017 0
		 -11.836727085985757 -5.1843550971212389e-015 -1.9727878476642753 1;
	setAttr ".pm[5]" -type "matrix" 1 3.3306690738754686e-016 -2.2204460492503136e-016 -0
		 -2.2204460492503123e-016 1 -3.3306690738754681e-016 0 3.3306690738754716e-016 4.4408920985006271e-016 1 -0
		 -7.4814499542781829e-015 -3.0000000000000027 -5.1843550971212184e-015 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 2 ".ma";
	setAttr -s 6 ".dpf[0:5]"  4 4 4 4 4 4;
	setAttr -s 2 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 2 ".ifcl";
createNode tweak -n "tweak14";
	rename -uid "CFC687B2-4864-85CE-9C67-BF832D981706";
createNode objectSet -n "skinCluster14Set";
	rename -uid "C8572A8C-42AE-9321-265F-63A8B962317F";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster14GroupId";
	rename -uid "FCFFE0A4-46D5-E4B5-3898-B0841EC13410";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster14GroupParts";
	rename -uid "C0A599E8-42DB-BAF3-CD1C-A08894B9A6C4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet14";
	rename -uid "A95C8FD1-46D9-3DC9-C78A-09A2880457A3";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId98";
	rename -uid "FB9B30C7-47F7-8471-2315-5F8D7FBB995D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts31";
	rename -uid "6AE1C07B-417F-235D-8263-20950749818E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster15";
	rename -uid "734B0C44-46CB-6D50-6C14-15BA98299527";
	setAttr ".skm" 2;
	setAttr -s 24 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr ".wl[8].w[0]"  1;
	setAttr ".wl[9].w[0]"  1;
	setAttr ".wl[10].w[0]"  1;
	setAttr ".wl[11].w[0]"  1;
	setAttr ".wl[12].w[0]"  1;
	setAttr ".wl[13].w[0]"  1;
	setAttr ".wl[14].w[0]"  1;
	setAttr ".wl[15].w[0]"  1;
	setAttr ".wl[16].w[0]"  1;
	setAttr ".wl[17].w[0]"  1;
	setAttr ".wl[18].w[0]"  1;
	setAttr ".wl[19].w[0]"  1;
	setAttr ".wl[20].w[0]"  1;
	setAttr ".wl[21].w[0]"  1;
	setAttr ".wl[22].w[0]"  1;
	setAttr ".wl[23].w[0]"  1;
	setAttr -s 9 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 -1.1093356479670487e-031 0 5.5466782398352384e-031 -2.2204460492503131e-016 -1 0
		 1.2316099784104836e-046 1 -2.2204460492503131e-016 0 2.2204460492504674e-016 -5.1070259132757201e-015 -22 1;
	setAttr ".pm[1]" -type "matrix" -5.1520706354812162e-015 3.0768603323386754e-016 -1 0
		 -0.99161147425389917 0.12925433891364804 4.756054730794773e-015 0 0.12925433891364804 0.99161147425390339 -2.6199967993414138e-016 0
		 -21.815452433585754 2.8435954561002572 8.0000000000000959 1;
	setAttr ".pm[2]" -type "matrix" -5.0959989091979708e-015 -2.8179409857626486e-015 -1 0
		 -0.8334831353172294 -0.55254489694662723 5.7713693005414685e-015 0 -0.55254489694662834 0.83348313531723051 -3.884102297773727e-016 0
		 -22.143929693072756 -15.461925534710955 8.0000000000001261 1;
	setAttr ".pm[3]" -type "matrix" -2.4791129611168535e-015 5.2262381342463482e-015 -1 0
		 -0.37139067635410333 0.92847669088525953 5.7713693005414764e-015 0 0.92847669088525975 0.37139067635410344 -3.8841022977737161e-016 -0
		 -9.656157585206671 29.525558770151193 8.0000000000000853 1;
	setAttr ".pm[4]" -type "matrix" -1 5.7061566750476561e-015 2.6804902499878877e-016 -0
		 5.6881025736945888e-015 0.99999999999999989 -2.2759572004815725e-015 0 -2.7738792731485576e-016 -2.3314683517128264e-015 -1.0000000000000002 0
		 8.0000000000001545 32.999999999999936 2.999999999999921 1;
	setAttr ".pm[5]" -type "matrix" -1.0991085665622097e-015 1.5726153137797406e-016 -1 -1.0053301412455735e-031
		 -0.99161147425390361 0.12925433891364749 1.1964681092336514e-015 -1.2490009027033026e-016
		 0.12925433891364702 0.99161147425390361 -5.8874068197368488e-017 1.5612511283791276e-017
		 -21.815452433585889 2.8435954561002483 -7.999999999999976 0.999999999999997;
	setAttr ".pm[6]" -type "matrix" -2.7601821004368921e-016 -4.0887775524536075e-016 -1 0
		 -0.83348313531722773 -0.55254489694663178 1.1188475330860084e-015 0 -0.55254489694663178 0.83348313531722773 -5.8874068197355722e-018 0
		 -22.143929693072781 -15.461925534711124 -7.9999999999999742 1;
	setAttr ".pm[7]" -type "matrix" -3.994412775188315e-016 1.0359702810488311e-015 -1 0
		 -0.37139067635410683 0.92847669088525842 1.1188475330860068e-015 0 0.92847669088525842 0.37139067635410683 -5.8874068197367525e-018 -0
		 -9.6561575852067865 29.525558770151214 -7.9999999999999307 1;
	setAttr ".pm[8]" -type "matrix" 1 1.275153616632931e-015 -3.9844869221162399e-016 -0
		 -1.2853809867797809e-015 1 -2.8310687127941496e-015 0 3.3895431420728332e-016 2.83106871279415e-015 1 -0
		 7.9999999999999698 33 -3.0000000000000977 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 5 ".ma";
	setAttr -s 9 ".dpf[0:8]"  4 4 4 4 4 4 4 4 4;
	setAttr -s 5 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 5 ".ifcl";
createNode tweak -n "tweak15";
	rename -uid "888EEA8D-4964-4B2D-CB1B-4393167A0075";
createNode objectSet -n "skinCluster15Set";
	rename -uid "F68427D5-450F-853C-FAFD-45BBF5E4317C";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster15GroupId";
	rename -uid "E7497A84-4D69-DB4D-4E1F-3BB04D87783E";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster15GroupParts";
	rename -uid "FD380EF8-49EA-F127-DCE9-F5A5B0EFAC22";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet15";
	rename -uid "6739348E-4113-0203-6A1C-E88DEEAFC5C3";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId100";
	rename -uid "67CA5068-40CB-0DBD-31F1-FE89DFF47663";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts33";
	rename -uid "73360971-4D63-FAE5-EB6E-90A3A857256D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose5";
	rename -uid "6823AEF2-4DD8-7EFE-414A-5E93A4C375FD";
	setAttr -s 9 ".wm";
	setAttr -s 18 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 -2.2204460492503121e-016
		 -22 2.2204460492503136e-016 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.70710678118654757 0 0 0.70710678118654746 1
		 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 8 8.8817841970012602e-016
		 3.5527136788005009e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.045794447660438609 -0.70562232714354933 0.045794447660438609 0.70562232714354534 1
		 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.0422974419109181 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.34994812815420445 0.93676907912322149 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.6419710876814353 4.1580967435406215
		 -1.014299755297543e-012 -0 0 -0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.77571811636938592 0.63107955436405305 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.3851648071344869 1.862673582821694e-015
		 -7.2830630415410269e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.13371921058204519 -0.69434801988722816 0.13371921058204517 0.69434801988722827 1
		 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 -8 -8.8817841970012523e-016
		 1.1546319456101628e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.045794447660438165 -0.70562232714354767 0.045794447660438109 0.705622327143547 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.0422974419109465 4.4408920985006262e-016
		 -1.7763568394002505e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.34994812815420634 0.93676907912322083 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.6419710876813847 4.1580967435406375
		 -4.8849813083506888e-014 -0 0 -0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.77571811636938648 0.63107955436405239 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.385164807134518 7.0145348813397137e-016
		 4.9737991503207013e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.1337192105820447 0.69434801988722816 0.13371921058204464 0.6943480198872285 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.3851648071344869 1.862673582821694e-015
		 -7.2830630415410269e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.13371921058204519 -0.69434801988722816 0.13371921058204517 0.69434801988722827 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.385164807134518 7.0145348813397137e-016
		 4.9737991503207013e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.1337192105820447 0.69434801988722816 0.13371921058204464 0.6943480198872285 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.3851648071344869 1.862673582821694e-015
		 -7.2830630415410269e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.13371921058204519 -0.69434801988722816 0.13371921058204517 0.69434801988722827 1
		 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.385164807134518 7.0145348813397137e-016
		 4.9737991503207013e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.1337192105820447 0.69434801988722816 0.13371921058204464 0.6943480198872285 1
		 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.3851648071344869 1.862673582821694e-015
		 -7.2830630415410269e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.13371921058204519 -0.69434801988722816 0.13371921058204517 0.69434801988722827 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.0852946064091489 0.55709309824615538
		 5.0626169922907138e-014 -0 0 -0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.1337192105820447 0.69434801988722816 0.13371921058204464 0.6943480198872285 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.3851648071344869 1.862673582821694e-015
		 -7.2830630415410269e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.13371921058204519 -0.69434801988722816 0.13371921058204517 0.69434801988722827 1
		 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.3851648071344869 1.862673582821694e-015
		 -7.2830630415410269e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.13371921058204519 -0.69434801988722816 0.13371921058204517 0.69434801988722827 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.0852946064091329 0.55709309824616038
		 2.8066438062523957e-013 -0 0 -0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.13371921058204519 -0.69434801988722816 0.13371921058204517 0.69434801988722827 1
		 1 1 yes;
	setAttr -s 9 ".m";
	setAttr -s 9 ".p";
	setAttr -s 18 ".g[3:17]" yes no no no yes no no no no no no no no no 
		no;
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster16";
	rename -uid "7EC36560-42B8-B63D-7CBD-2383A877F588";
	setAttr ".skm" 2;
	setAttr -s 16 ".wl";
	setAttr ".wl[0].w[5]"  1;
	setAttr ".wl[1].w[5]"  1;
	setAttr ".wl[2].w[5]"  1;
	setAttr ".wl[3].w[5]"  1;
	setAttr ".wl[4].w[5]"  1;
	setAttr ".wl[5].w[5]"  1;
	setAttr ".wl[6].w[5]"  1;
	setAttr ".wl[7].w[5]"  1;
	setAttr ".wl[8].w[5]"  1;
	setAttr ".wl[9].w[5]"  1;
	setAttr ".wl[10].w[5]"  1;
	setAttr ".wl[11].w[5]"  1;
	setAttr ".wl[12].w[5]"  1;
	setAttr ".wl[13].w[5]"  1;
	setAttr ".wl[14].w[5]"  1;
	setAttr ".wl[15].w[5]"  1;
	setAttr -s 9 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 -1.2325951644078318e-031 0 6.1629758220391539e-031 -2.2204460492503131e-016 -1 0
		 1.368455531567204e-046 1 -2.2204460492503131e-016 0 2.2204460492504847e-016 -5.1070259132757201e-015 -22 1;
	setAttr ".pm[1]" -type "matrix" -5.7713693005414748e-015 3.884102297773727e-016 -1 0
		 -0.99161147425390117 0.12925433891364804 5.7713693005414748e-015 0 0.12925433891364804 0.99161147425390139 -3.8841022977737142e-016 -0
		 -21.815452433585786 2.8435954561002532 8.0000000000001261 1;
	setAttr ".pm[2]" -type "matrix" -4.6124606491359471e-015 -3.4906694014186895e-015 -1 0
		 -0.83348313531723039 -0.55254489694662723 5.7713693005414756e-015 0 -0.55254489694662734 0.83348313531723051 -3.8841022977737181e-016 -0
		 -22.143929693072788 -15.461925534710955 8.0000000000001261 1;
	setAttr ".pm[3]" -type "matrix" -2.4791129611168535e-015 5.2262381342463482e-015 -1 0
		 -0.37139067635410333 0.92847669088525953 5.7713693005414764e-015 0 0.92847669088525975 0.37139067635410344 -3.8841022977737161e-016 -0
		 -9.656157585206671 29.525558770151193 8.0000000000000853 1;
	setAttr ".pm[4]" -type "matrix" -1 5.7061566750476561e-015 2.6804902499878877e-016 -0
		 5.6881025736945888e-015 0.99999999999999989 -2.2759572004815725e-015 0 -2.7738792731485576e-016 -2.3314683517128264e-015 -1.0000000000000002 0
		 8.0000000000001545 32.999999999999936 2.999999999999921 1;
	setAttr ".pm[5]" -type "matrix" -1.0991077266346226e-015 1.5726142189529074e-016 -1 -1.1170329025256857e-031
		 -0.99161147425390384 0.12925433891364757 1.2050926176945009e-015 -1.3877787807814474e-016
		 0.12925433891364704 0.99161147425390384 -6.4761475017105349e-017 1.7347234759768086e-017
		 -21.815452433585893 2.8435954561002501 -7.999999999999976 0.99999999999999667;
	setAttr ".pm[6]" -type "matrix" -2.0301798134853611e-016 -3.8743293197973857e-016 -1 0
		 -0.83348313531722773 -0.55254489694663178 1.1188475330860086e-015 0 -0.55254489694663178 0.83348313531722773 -5.8874068197354359e-018 0
		 -22.143929693072781 -15.461925534711124 -7.9999999999999742 1;
	setAttr ".pm[7]" -type "matrix" -5.2410208337909436e-016 1.0386889807478823e-015 -1 0
		 -0.37139067635410727 0.92847669088526041 1.1188475330860048e-015 0 0.92847669088525842 0.37139067635410783 -5.8874068197367979e-018 0
		 -10.956027785932164 30.082651868397431 -7.9999999999999378 1;
	setAttr ".pm[8]" -type "matrix" 1 1.275153616632931e-015 -3.9844869221162399e-016 -0
		 -1.2853809867797809e-015 1 -2.8310687127941496e-015 0 3.3895431420728332e-016 2.83106871279415e-015 1 -0
		 7.9999999999999698 33 -3.0000000000000977 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 9 ".dpf[0:8]"  4 4 4 4 4 4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak16";
	rename -uid "7446A218-4E22-539D-0169-6F8F57F2AE18";
createNode objectSet -n "skinCluster16Set";
	rename -uid "3950BD81-4F04-B815-BE81-5D82A6B4E6B5";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster16GroupId";
	rename -uid "7CCDC44F-40EB-C961-1A1C-20BC1E5F404A";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster16GroupParts";
	rename -uid "642DC087-44FB-808B-4DED-A695B31F6928";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet16";
	rename -uid "0900A6CC-4EA1-35EF-B560-689486571449";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId102";
	rename -uid "E2773A2B-4A3A-BAFD-DD46-71B88147D353";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts35";
	rename -uid "1D45890F-4383-8520-D18E-41A05941718B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster17";
	rename -uid "169FC187-41C5-CA49-F04A-9582FFCCE1F5";
	setAttr ".skm" 2;
	setAttr -s 32 ".wl";
	setAttr ".wl[0].w[6]"  1;
	setAttr ".wl[1].w[6]"  1;
	setAttr ".wl[2].w[6]"  1;
	setAttr ".wl[3].w[6]"  1;
	setAttr ".wl[4].w[6]"  1;
	setAttr ".wl[5].w[6]"  1;
	setAttr ".wl[6].w[6]"  1;
	setAttr ".wl[7].w[6]"  1;
	setAttr ".wl[8].w[6]"  1;
	setAttr ".wl[9].w[6]"  1;
	setAttr ".wl[10].w[6]"  1;
	setAttr ".wl[11].w[6]"  1;
	setAttr ".wl[12].w[6]"  1;
	setAttr ".wl[13].w[6]"  1;
	setAttr ".wl[14].w[6]"  1;
	setAttr ".wl[15].w[6]"  1;
	setAttr ".wl[16].w[6]"  1;
	setAttr ".wl[17].w[6]"  1;
	setAttr ".wl[18].w[6]"  1;
	setAttr ".wl[19].w[6]"  1;
	setAttr ".wl[20].w[6]"  1;
	setAttr ".wl[21].w[6]"  1;
	setAttr ".wl[22].w[6]"  1;
	setAttr ".wl[23].w[6]"  1;
	setAttr ".wl[24].w[6]"  1;
	setAttr ".wl[25].w[6]"  1;
	setAttr ".wl[26].w[6]"  1;
	setAttr ".wl[27].w[6]"  1;
	setAttr ".wl[28].w[6]"  1;
	setAttr ".wl[29].w[6]"  1;
	setAttr ".wl[30].w[6]"  1;
	setAttr ".wl[31].w[6]"  1;
	setAttr -s 9 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 -1.2325951644078318e-031 0 6.1629758220391539e-031 -2.2204460492503131e-016 -1 0
		 1.368455531567204e-046 1 -2.2204460492503131e-016 0 2.2204460492504847e-016 -5.1070259132757201e-015 -22 1;
	setAttr ".pm[1]" -type "matrix" -5.7713693005414748e-015 3.884102297773727e-016 -1 0
		 -0.99161147425390117 0.12925433891364804 5.7713693005414748e-015 0 0.12925433891364804 0.99161147425390139 -3.8841022977737142e-016 -0
		 -21.815452433585786 2.8435954561002532 8.0000000000001261 1;
	setAttr ".pm[2]" -type "matrix" -4.6124606491359471e-015 -3.4906694014186895e-015 -1 0
		 -0.83348313531723039 -0.55254489694662723 5.7713693005414756e-015 0 -0.55254489694662734 0.83348313531723051 -3.8841022977737181e-016 -0
		 -22.143929693072788 -15.461925534710955 8.0000000000001261 1;
	setAttr ".pm[3]" -type "matrix" -2.4791129611168535e-015 5.2262381342463482e-015 -1 0
		 -0.37139067635410333 0.92847669088525953 5.7713693005414764e-015 0 0.92847669088525975 0.37139067635410344 -3.8841022977737161e-016 -0
		 -9.656157585206671 29.525558770151193 8.0000000000000853 1;
	setAttr ".pm[4]" -type "matrix" -1 5.7061566750476561e-015 2.6804902499878877e-016 -0
		 5.6881025736945888e-015 0.99999999999999989 -2.2759572004815725e-015 0 -2.7738792731485576e-016 -2.3314683517128264e-015 -1.0000000000000002 0
		 8.0000000000001545 32.999999999999936 2.999999999999921 1;
	setAttr ".pm[5]" -type "matrix" -1.0991077266346226e-015 1.5726142189529074e-016 -1 -1.1170329025256857e-031
		 -0.99161147425390384 0.12925433891364757 1.2050926176945009e-015 -1.3877787807814474e-016
		 0.12925433891364704 0.99161147425390384 -6.4761475017105349e-017 1.7347234759768086e-017
		 -21.815452433585893 2.8435954561002501 -7.999999999999976 0.99999999999999667;
	setAttr ".pm[6]" -type "matrix" -2.0301798134853611e-016 -3.8743293197973857e-016 -1 0
		 -0.83348313531722773 -0.55254489694663178 1.1188475330860086e-015 0 -0.55254489694663178 0.83348313531722773 -5.8874068197354359e-018 0
		 -22.143929693072781 -15.461925534711124 -7.9999999999999742 1;
	setAttr ".pm[7]" -type "matrix" -5.2410208337909436e-016 1.0386889807478823e-015 -1 0
		 -0.37139067635410727 0.92847669088526041 1.1188475330860048e-015 0 0.92847669088525842 0.37139067635410783 -5.8874068197367979e-018 0
		 -10.956027785932164 30.082651868397431 -7.9999999999999378 1;
	setAttr ".pm[8]" -type "matrix" 1 1.275153616632931e-015 -3.9844869221162399e-016 -0
		 -1.2853809867797809e-015 1 -2.8310687127941496e-015 0 3.3895431420728332e-016 2.83106871279415e-015 1 -0
		 7.9999999999999698 33 -3.0000000000000977 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 9 ".dpf[0:8]"  4 4 4 4 4 4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak17";
	rename -uid "7F9F822C-4BFD-2FBF-F950-42AD67E2037B";
createNode objectSet -n "skinCluster17Set";
	rename -uid "83EC70E8-4591-93FF-C9A8-4A97769F758C";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster17GroupId";
	rename -uid "380C2915-41A4-30EA-C195-69B3F7894C55";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster17GroupParts";
	rename -uid "90422437-4A72-600A-A2D9-DFA9BEB47AAA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet17";
	rename -uid "3E501160-455C-B5F2-344C-A4A1CE80AE1A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId104";
	rename -uid "53083271-42D8-B6F4-5FC6-4BB180D3AFC7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts37";
	rename -uid "F9759F20-480C-3505-03C5-6986B3DBE6E2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster18";
	rename -uid "62F08092-4107-F474-CF24-47B9D768AEE4";
	setAttr ".skm" 2;
	setAttr -s 16 ".wl";
	setAttr ".wl[0].w[6]"  1;
	setAttr ".wl[1].w[6]"  1;
	setAttr ".wl[2].w[6]"  1;
	setAttr ".wl[3].w[6]"  1;
	setAttr ".wl[4].w[6]"  1;
	setAttr ".wl[5].w[6]"  1;
	setAttr ".wl[6].w[6]"  1;
	setAttr ".wl[7].w[6]"  1;
	setAttr ".wl[8].w[6]"  1;
	setAttr ".wl[9].w[6]"  1;
	setAttr ".wl[10].w[6]"  1;
	setAttr ".wl[11].w[6]"  1;
	setAttr ".wl[12].w[6]"  1;
	setAttr ".wl[13].w[6]"  1;
	setAttr ".wl[14].w[6]"  1;
	setAttr ".wl[15].w[6]"  1;
	setAttr -s 9 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 -1.2325951644078318e-031 0 6.1629758220391539e-031 -2.2204460492503131e-016 -1 0
		 1.368455531567204e-046 1 -2.2204460492503131e-016 0 2.2204460492504847e-016 -5.1070259132757201e-015 -22 1;
	setAttr ".pm[1]" -type "matrix" -5.7713693005414748e-015 3.884102297773727e-016 -1 0
		 -0.99161147425390117 0.12925433891364804 5.7713693005414748e-015 0 0.12925433891364804 0.99161147425390139 -3.8841022977737142e-016 -0
		 -21.815452433585786 2.8435954561002532 8.0000000000001261 1;
	setAttr ".pm[2]" -type "matrix" -4.6124606491359471e-015 -3.4906694014186895e-015 -1 0
		 -0.83348313531723039 -0.55254489694662723 5.7713693005414756e-015 0 -0.55254489694662734 0.83348313531723051 -3.8841022977737181e-016 -0
		 -22.143929693072788 -15.461925534710955 8.0000000000001261 1;
	setAttr ".pm[3]" -type "matrix" -2.4791129611168535e-015 5.2262381342463482e-015 -1 0
		 -0.37139067635410333 0.92847669088525953 5.7713693005414764e-015 0 0.92847669088525975 0.37139067635410344 -3.8841022977737161e-016 -0
		 -9.656157585206671 29.525558770151193 8.0000000000000853 1;
	setAttr ".pm[4]" -type "matrix" -1 5.7061566750476561e-015 2.6804902499878877e-016 -0
		 5.6881025736945888e-015 0.99999999999999989 -2.2759572004815725e-015 0 -2.7738792731485576e-016 -2.3314683517128264e-015 -1.0000000000000002 0
		 8.0000000000001545 32.999999999999936 2.999999999999921 1;
	setAttr ".pm[5]" -type "matrix" -1.0991077266346226e-015 1.5726142189529074e-016 -1 -1.1170329025256857e-031
		 -0.99161147425390384 0.12925433891364757 1.2050926176945009e-015 -1.3877787807814474e-016
		 0.12925433891364704 0.99161147425390384 -6.4761475017105349e-017 1.7347234759768086e-017
		 -21.815452433585893 2.8435954561002501 -7.999999999999976 0.99999999999999667;
	setAttr ".pm[6]" -type "matrix" -2.0301798134853611e-016 -3.8743293197973857e-016 -1 0
		 -0.83348313531722773 -0.55254489694663178 1.1188475330860086e-015 0 -0.55254489694663178 0.83348313531722773 -5.8874068197354359e-018 0
		 -22.143929693072781 -15.461925534711124 -7.9999999999999742 1;
	setAttr ".pm[7]" -type "matrix" -5.2410208337909436e-016 1.0386889807478823e-015 -1 0
		 -0.37139067635410727 0.92847669088526041 1.1188475330860048e-015 0 0.92847669088525842 0.37139067635410783 -5.8874068197367979e-018 0
		 -10.956027785932164 30.082651868397431 -7.9999999999999378 1;
	setAttr ".pm[8]" -type "matrix" 1 1.2751540395085462e-015 5.7956484172760544e-017 0
		 -1.2853809867797789e-015 1 -2.8310687127941496e-015 0 3.3895431420728332e-016 2.83106871279415e-015 1 0
		 7.9999999999999698 33 -3.0000000000000973 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 5 ".ma";
	setAttr -s 9 ".dpf[0:8]"  4 4 4 4 4 4 4 4 4;
	setAttr -s 5 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 5 ".ifcl";
createNode tweak -n "tweak18";
	rename -uid "7226D133-4591-A721-A04E-5E92A9B83F8F";
createNode objectSet -n "skinCluster18Set";
	rename -uid "9A831967-4C4B-61E8-C9BB-5587F20C48BA";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster18GroupId";
	rename -uid "83FA663E-4578-73E7-D81C-F8BD8F1D9598";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster18GroupParts";
	rename -uid "92FE6756-4881-3912-0EAF-56BF1D0B1ACD";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet18";
	rename -uid "EBF528CB-49DE-E968-D2C4-C8816F02E7D5";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId106";
	rename -uid "FDC20041-42E3-5CF0-6A58-24B3BA26CB1E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts39";
	rename -uid "43E3BBCE-4FFD-5987-CCD1-41BCE8428BF5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster19";
	rename -uid "EAB48DB9-4ADB-3BDB-06F9-9D8CAD6A8A47";
	setAttr ".skm" 2;
	setAttr -s 16 ".wl";
	setAttr ".wl[0].w[1]"  1;
	setAttr ".wl[1].w[1]"  1;
	setAttr ".wl[2].w[1]"  1;
	setAttr ".wl[3].w[1]"  1;
	setAttr ".wl[4].w[1]"  1;
	setAttr ".wl[5].w[1]"  1;
	setAttr ".wl[6].w[1]"  1;
	setAttr ".wl[7].w[1]"  1;
	setAttr ".wl[8].w[1]"  1;
	setAttr ".wl[9].w[1]"  1;
	setAttr ".wl[10].w[1]"  1;
	setAttr ".wl[11].w[1]"  1;
	setAttr ".wl[12].w[1]"  1;
	setAttr ".wl[13].w[1]"  1;
	setAttr ".wl[14].w[1]"  1;
	setAttr ".wl[15].w[1]"  1;
	setAttr -s 9 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 -1.2325951644078318e-031 0 6.1629758220391539e-031 -2.2204460492503131e-016 -1 0
		 1.368455531567204e-046 1 -2.2204460492503131e-016 0 2.2204460492504847e-016 -5.1070259132757201e-015 -22 1;
	setAttr ".pm[1]" -type "matrix" -5.0832596726967436e-015 2.9871667806236697e-016 -1 0
		 -0.99161147425389895 0.12925433891364804 4.6432420008229173e-015 0 0.12925433891364804 0.99161147425390361 -2.479540632848936e-016 0
		 -21.815452433585751 2.8435954561002577 8.0000000000000924 1;
	setAttr ".pm[2]" -type "matrix" -5.1497253825381957e-015 -2.7431933840230886e-015 -1 0
		 -0.83348313531722928 -0.55254489694662723 5.7713693005414677e-015 0 -0.55254489694662845 0.83348313531723051 -3.884102297773728e-016 0
		 -22.143929693072753 -15.461925534710955 8.0000000000001261 1;
	setAttr ".pm[3]" -type "matrix" -2.3617642863106107e-015 4.8460159155099045e-015 -1 0
		 -0.37139067635410489 0.92847669088526175 5.7713693005414764e-015 0 0.92847669088526175 0.37139067635410344 -3.8841022977737068e-016 0
		 -10.956027785932076 30.082651868397424 8.0000000000004388 1;
	setAttr ".pm[4]" -type "matrix" -1 5.7061566750476561e-015 2.6804902499878877e-016 -0
		 5.6881025736945888e-015 0.99999999999999989 -2.2759572004815725e-015 0 -2.7738792731485576e-016 -2.3314683517128264e-015 -1.0000000000000002 0
		 8.0000000000001545 32.999999999999936 2.999999999999921 1;
	setAttr ".pm[5]" -type "matrix" -1.0991161259104941e-015 1.5726251672212389e-016 -1 0
		 -0.99161147425390161 0.12925433891364674 1.1188475330860064e-015 0 0.12925433891364677 0.99161147425390161 -5.8874068197368241e-018 -0
		 -21.815452433585858 2.8435954561002319 -7.999999999999976 1;
	setAttr ".pm[6]" -type "matrix" -9.3302026830006734e-016 -6.0188116463596033e-016 -1 0
		 -0.83348313531722773 -0.55254489694663178 1.1188475330860066e-015 0 -0.55254489694663178 0.83348313531722773 -5.8874068197367995e-018 -0
		 -22.143929693072781 -15.461925534711124 -7.9999999999999742 1;
	setAttr ".pm[7]" -type "matrix" -3.994412775188315e-016 1.0359702810488311e-015 -1 0
		 -0.37139067635410683 0.92847669088525842 1.1188475330860068e-015 0 0.92847669088525842 0.37139067635410683 -5.8874068197367525e-018 -0
		 -9.6561575852067865 29.525558770151214 -7.9999999999999307 1;
	setAttr ".pm[8]" -type "matrix" 1 1.275153616632931e-015 -3.9844869221162399e-016 -0
		 -1.2853809867797809e-015 1 -2.8310687127941496e-015 0 3.3895431420728332e-016 2.83106871279415e-015 1 -0
		 7.9999999999999698 33 -3.0000000000000977 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 9 ".dpf[0:8]"  4 4 4 4 4 4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak19";
	rename -uid "2FE2F863-4DD7-F801-A3F5-EB9FBDF2C295";
createNode objectSet -n "skinCluster19Set";
	rename -uid "5EB699C2-4CBC-960A-0F0E-BE905AC5B5C0";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster19GroupId";
	rename -uid "303AF2C0-494F-FAE7-D13F-728A1B760CBD";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster19GroupParts";
	rename -uid "3745EAEE-4E2A-E695-4C8E-FEA9D8EBC6D1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet19";
	rename -uid "325A92B3-4324-E266-9DF9-3FA7B514CE3D";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId108";
	rename -uid "B0EF1ED9-439C-137C-5CF3-B8933EA56B2F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts41";
	rename -uid "2A032111-4D6B-851A-C132-BDA5CE183680";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster20";
	rename -uid "F05E0556-437B-9A94-66AC-9FA21A4C1D44";
	setAttr ".skm" 2;
	setAttr -s 32 ".wl";
	setAttr ".wl[0].w[2]"  1;
	setAttr ".wl[1].w[2]"  1;
	setAttr ".wl[2].w[2]"  1;
	setAttr ".wl[3].w[2]"  1;
	setAttr ".wl[4].w[2]"  1;
	setAttr ".wl[5].w[2]"  1;
	setAttr ".wl[6].w[2]"  1;
	setAttr ".wl[7].w[2]"  1;
	setAttr ".wl[8].w[2]"  1;
	setAttr ".wl[9].w[2]"  1;
	setAttr ".wl[10].w[2]"  1;
	setAttr ".wl[11].w[2]"  1;
	setAttr ".wl[12].w[2]"  1;
	setAttr ".wl[13].w[2]"  1;
	setAttr ".wl[14].w[2]"  1;
	setAttr ".wl[15].w[2]"  1;
	setAttr ".wl[16].w[2]"  1;
	setAttr ".wl[17].w[2]"  1;
	setAttr ".wl[18].w[2]"  1;
	setAttr ".wl[19].w[2]"  1;
	setAttr ".wl[20].w[2]"  1;
	setAttr ".wl[21].w[2]"  1;
	setAttr ".wl[22].w[2]"  1;
	setAttr ".wl[23].w[2]"  1;
	setAttr ".wl[24].w[2]"  1;
	setAttr ".wl[25].w[2]"  1;
	setAttr ".wl[26].w[2]"  1;
	setAttr ".wl[27].w[2]"  1;
	setAttr ".wl[28].w[2]"  1;
	setAttr ".wl[29].w[2]"  1;
	setAttr ".wl[30].w[2]"  1;
	setAttr ".wl[31].w[2]"  1;
	setAttr -s 9 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 -1.2325951644078318e-031 0 6.1629758220391539e-031 -2.2204460492503131e-016 -1 0
		 1.368455531567204e-046 1 -2.2204460492503131e-016 0 2.2204460492504847e-016 -5.1070259132757201e-015 -22 1;
	setAttr ".pm[1]" -type "matrix" -5.0832596726967436e-015 2.9871667806236697e-016 -1 0
		 -0.99161147425389895 0.12925433891364804 4.6432420008229173e-015 0 0.12925433891364804 0.99161147425390361 -2.479540632848936e-016 0
		 -21.815452433585751 2.8435954561002577 8.0000000000000924 1;
	setAttr ".pm[2]" -type "matrix" -5.1497253825381957e-015 -2.7431933840230886e-015 -1 0
		 -0.83348313531722928 -0.55254489694662723 5.7713693005414677e-015 0 -0.55254489694662845 0.83348313531723051 -3.884102297773728e-016 0
		 -22.143929693072753 -15.461925534710955 8.0000000000001261 1;
	setAttr ".pm[3]" -type "matrix" -2.3617642863106107e-015 4.8460159155099045e-015 -1 0
		 -0.37139067635410489 0.92847669088526175 5.7713693005414764e-015 0 0.92847669088526175 0.37139067635410344 -3.8841022977737068e-016 0
		 -10.956027785932076 30.082651868397424 8.0000000000004388 1;
	setAttr ".pm[4]" -type "matrix" -1 5.7061566750476561e-015 2.6804902499878877e-016 -0
		 5.6881025736945888e-015 0.99999999999999989 -2.2759572004815725e-015 0 -2.7738792731485576e-016 -2.3314683517128264e-015 -1.0000000000000002 0
		 8.0000000000001545 32.999999999999936 2.999999999999921 1;
	setAttr ".pm[5]" -type "matrix" -1.0991161259104941e-015 1.5726251672212389e-016 -1 0
		 -0.99161147425390161 0.12925433891364674 1.1188475330860064e-015 0 0.12925433891364677 0.99161147425390161 -5.8874068197368241e-018 -0
		 -21.815452433585858 2.8435954561002319 -7.999999999999976 1;
	setAttr ".pm[6]" -type "matrix" -9.3302026830006734e-016 -6.0188116463596033e-016 -1 0
		 -0.83348313531722773 -0.55254489694663178 1.1188475330860066e-015 0 -0.55254489694663178 0.83348313531722773 -5.8874068197367995e-018 -0
		 -22.143929693072781 -15.461925534711124 -7.9999999999999742 1;
	setAttr ".pm[7]" -type "matrix" -3.994412775188315e-016 1.0359702810488311e-015 -1 0
		 -0.37139067635410683 0.92847669088525842 1.1188475330860068e-015 0 0.92847669088525842 0.37139067635410683 -5.8874068197367525e-018 -0
		 -9.6561575852067865 29.525558770151214 -7.9999999999999307 1;
	setAttr ".pm[8]" -type "matrix" 1 1.275153616632931e-015 -3.9844869221162399e-016 -0
		 -1.2853809867797809e-015 1 -2.8310687127941496e-015 0 3.3895431420728332e-016 2.83106871279415e-015 1 -0
		 7.9999999999999698 33 -3.0000000000000977 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 9 ".dpf[0:8]"  4 4 4 4 4 4 4 4 4;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode tweak -n "tweak20";
	rename -uid "C5FCDDDE-48EA-DD6F-4019-04B661FC2218";
createNode objectSet -n "skinCluster20Set";
	rename -uid "05A07924-4CAF-58B1-C532-029CF9AB457A";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster20GroupId";
	rename -uid "E0F95CD7-4CAE-B11E-97BE-E4AB4DB24BF3";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster20GroupParts";
	rename -uid "C871F24A-4672-32D7-3B22-1E876A876231";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet20";
	rename -uid "15E58B04-400F-A696-70AF-26863EF449C8";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId110";
	rename -uid "2E43D63A-4B72-3815-144A-D88743EF5C69";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts43";
	rename -uid "AF21B359-4818-475A-6EFC-69B104D19483";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster21";
	rename -uid "A9970E9C-4F93-A766-1AF8-CDB141948CCF";
	setAttr ".skm" 2;
	setAttr -s 16 ".wl";
	setAttr ".wl[0].w[2]"  1;
	setAttr ".wl[1].w[2]"  1;
	setAttr ".wl[2].w[2]"  1;
	setAttr ".wl[3].w[2]"  1;
	setAttr ".wl[4].w[2]"  1;
	setAttr ".wl[5].w[2]"  1;
	setAttr ".wl[6].w[2]"  1;
	setAttr ".wl[7].w[2]"  1;
	setAttr ".wl[8].w[2]"  1;
	setAttr ".wl[9].w[2]"  1;
	setAttr ".wl[10].w[2]"  1;
	setAttr ".wl[11].w[2]"  1;
	setAttr ".wl[12].w[2]"  1;
	setAttr ".wl[13].w[2]"  1;
	setAttr ".wl[14].w[2]"  1;
	setAttr ".wl[15].w[2]"  1;
	setAttr -s 9 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 -1.2325951644078318e-031 0 6.1629758220391539e-031 -2.2204460492503131e-016 -1 0
		 1.368455531567204e-046 1 -2.2204460492503131e-016 0 2.2204460492504847e-016 -5.1070259132757201e-015 -22 1;
	setAttr ".pm[1]" -type "matrix" -5.0832596726967436e-015 2.9871667806236697e-016 -1 0
		 -0.99161147425389895 0.12925433891364804 4.6432420008229173e-015 0 0.12925433891364804 0.99161147425390361 -2.479540632848936e-016 0
		 -21.815452433585751 2.8435954561002577 8.0000000000000924 1;
	setAttr ".pm[2]" -type "matrix" -5.1497253825381957e-015 -2.7431933840230886e-015 -1 0
		 -0.83348313531722928 -0.55254489694662723 5.7713693005414677e-015 0 -0.55254489694662845 0.83348313531723051 -3.884102297773728e-016 0
		 -22.143929693072753 -15.461925534710955 8.0000000000001261 1;
	setAttr ".pm[3]" -type "matrix" -2.3617642863106107e-015 4.8460159155099045e-015 -1 0
		 -0.37139067635410489 0.92847669088526175 5.7713693005414764e-015 0 0.92847669088526175 0.37139067635410344 -3.8841022977737068e-016 0
		 -10.956027785932076 30.082651868397424 8.0000000000004388 1;
	setAttr ".pm[4]" -type "matrix" -1 5.7061567348649985e-015 1.7298171342377721e-016 0
		 5.6881025736945888e-015 0.99999999999999989 -2.2759572004815725e-015 0 -2.7738792731485576e-016 -2.3314683517128303e-015 -1.0000000000000002 0
		 8.0000000000001528 32.999999999999936 2.9999999999999223 1;
	setAttr ".pm[5]" -type "matrix" -1.0991161259104941e-015 1.5726251672212389e-016 -1 0
		 -0.99161147425390161 0.12925433891364674 1.1188475330860064e-015 0 0.12925433891364677 0.99161147425390161 -5.8874068197368241e-018 -0
		 -21.815452433585858 2.8435954561002319 -7.999999999999976 1;
	setAttr ".pm[6]" -type "matrix" -9.3302026830006734e-016 -6.0188116463596033e-016 -1 0
		 -0.83348313531722773 -0.55254489694663178 1.1188475330860066e-015 0 -0.55254489694663178 0.83348313531722773 -5.8874068197367995e-018 -0
		 -22.143929693072781 -15.461925534711124 -7.9999999999999742 1;
	setAttr ".pm[7]" -type "matrix" -3.994412775188315e-016 1.0359702810488311e-015 -1 0
		 -0.37139067635410683 0.92847669088525842 1.1188475330860068e-015 0 0.92847669088525842 0.37139067635410683 -5.8874068197367525e-018 -0
		 -9.6561575852067865 29.525558770151214 -7.9999999999999307 1;
	setAttr ".pm[8]" -type "matrix" 1 1.275153616632931e-015 -3.9844869221162399e-016 -0
		 -1.2853809867797809e-015 1 -2.8310687127941496e-015 0 3.3895431420728332e-016 2.83106871279415e-015 1 -0
		 7.9999999999999698 33 -3.0000000000000977 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 5 ".ma";
	setAttr -s 9 ".dpf[0:8]"  4 4 4 4 4 4 4 4 4;
	setAttr -s 5 ".lw";
	setAttr -s 5 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 5 ".ifcl";
	setAttr -s 5 ".ifcl";
createNode tweak -n "tweak21";
	rename -uid "D5142E17-4523-5456-A2F5-A38402F34B12";
createNode objectSet -n "skinCluster21Set";
	rename -uid "F583C73B-4895-DA50-9E14-33A89278F331";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster21GroupId";
	rename -uid "5F78D3CC-4091-9581-8B58-F1A9ADE7A636";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster21GroupParts";
	rename -uid "7FEDEEB5-4753-459A-B2AE-66B395D13020";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet21";
	rename -uid "669591DA-4EB1-82DD-7004-D4A8725994B1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId112";
	rename -uid "971D753C-47D4-472A-194B-8A98A2243627";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts45";
	rename -uid "05BFA111-42D4-5D97-FC4B-93BBF2C46887";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 87 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 7 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "layer2.di" "root.do";
connectAttr "root.s" "hipsJnt.is";
connectAttr "hipsJnt.s" "leftUpperLeg.is";
connectAttr "leftUpperLeg.s" "leftLowerLeg.is";
connectAttr "leftLowerLeg.s" "leftFoot.is";
connectAttr "leftFoot.s" "leftToe.is";
connectAttr "hipsJnt.s" "rightUpperLeg.is";
connectAttr "rightUpperLeg.s" "rightLowerLeg.is";
connectAttr "rightLowerLeg.s" "rightFoot.is";
connectAttr "rightFoot.s" "rightToe.is";
connectAttr "hipsJnt.s" "spine.is";
connectAttr "spine.s" "chest.is";
connectAttr "chest.s" "head.is";
connectAttr "chest.s" "rightClavacle.is";
connectAttr "rightClavacle.s" "rightShoulder.is";
connectAttr "rightShoulder.s" "rightUpperArm.is";
connectAttr "rightUpperArm.s" "rightElbow.is";
connectAttr "rightElbow.s" "rightWrist.is";
connectAttr "chest.s" "leftClavacle.is";
connectAttr "leftClavacle.s" "leftShoulder.is";
connectAttr "leftShoulder.s" "leftUpperArm.is";
connectAttr "leftUpperArm.s" "leftElbow.is";
connectAttr "leftElbow.s" "leftWrist.is";
connectAttr "layer1.di" "BasicMechGrouped.do";
connectAttr "transformGeometry23.og" "baseShape.i";
connectAttr "transformGeometry24.og" "backSideShape.i";
connectAttr "transformGeometry15.og" "leftSideShape.i";
connectAttr "transformGeometry16.og" "leftNozzleShape.i";
connectAttr "transformGeometry25.og" "frontSideShape.i";
connectAttr "transformGeometry26.og" "frontLeftShape1.i";
connectAttr "transformGeometry45.og" "frontLeftShape2.i";
connectAttr "transformGeometry46.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCubeShape7.i"
		;
connectAttr "transformGeometry47.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8|pCubeShape8.i"
		;
connectAttr "transformGeometry48.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11|pCubeShape11.i"
		;
connectAttr "transformGeometry49.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10|pCubeShape10.i"
		;
connectAttr "transformGeometry50.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9|pCubeShape9.i"
		;
connectAttr "transformGeometry51.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12|pCubeShape12.i"
		;
connectAttr "transformGeometry28.og" "jawMidShape.i";
connectAttr "transformGeometry29.og" "pCubeShape5.i";
connectAttr "transformGeometry30.og" "teethmidShape.i";
connectAttr "transformGeometry14.og" "teethLeftShape.i";
connectAttr "transformGeometry5.og" "|BasicMechGrouped|Basic_Legs|hips|hipsShape.i"
		;
connectAttr "transformGeometry6.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|upperLegLeftShape.i"
		;
connectAttr "transformGeometry7.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft|lowerLegLeftShape.i"
		;
connectAttr "transformGeometry8.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft|kneeLeftShape.i"
		;
connectAttr "transformGeometry56.og" "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeftShape1.i"
		;
connectAttr "transformGeometry57.og" "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2|panelLeftShape2.i"
		;
connectAttr "transformGeometry52.og" "armRightShape1.i";
connectAttr "transformGeometry53.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|upperArmRightShape.i"
		;
connectAttr "transformGeometry54.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight|lowerArmRightShape.i"
		;
connectAttr "transformGeometry55.og" "armRightShape2.i";
connectAttr "groupId86.id" "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[0].gco"
		;
connectAttr "skinCluster9GroupId.id" "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[13].gid"
		;
connectAttr "skinCluster9Set.mwc" "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[13].gco"
		;
connectAttr "groupId88.id" "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[14].gid"
		;
connectAttr "tweakSet9.mwc" "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[14].gco"
		;
connectAttr "skinCluster9.og[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.i";
connectAttr "tweak9.vl[0].vt[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.twl";
connectAttr "skinCluster15.og[0]" "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.i"
		;
connectAttr "skinCluster15GroupId.id" "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog.og[2].gid"
		;
connectAttr "skinCluster15Set.mwc" "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog.og[2].gco"
		;
connectAttr "groupId100.id" "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog.og[3].gid"
		;
connectAttr "tweakSet15.mwc" "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog.og[3].gco"
		;
connectAttr "tweak15.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.twl"
		;
connectAttr "skinCluster19.og[0]" "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.i"
		;
connectAttr "skinCluster19GroupId.id" "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.iog.og[2].gid"
		;
connectAttr "skinCluster19Set.mwc" "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.iog.og[2].gco"
		;
connectAttr "groupId108.id" "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.iog.og[3].gid"
		;
connectAttr "tweakSet19.mwc" "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.iog.og[3].gco"
		;
connectAttr "tweak19.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.twl"
		;
connectAttr "skinCluster21.og[0]" "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.i"
		;
connectAttr "skinCluster21GroupId.id" "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.iog.og[2].gid"
		;
connectAttr "skinCluster21Set.mwc" "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.iog.og[2].gco"
		;
connectAttr "groupId112.id" "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.iog.og[3].gid"
		;
connectAttr "tweakSet21.mwc" "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.iog.og[3].gco"
		;
connectAttr "tweak21.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.twl"
		;
connectAttr "skinCluster20.og[0]" "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.i"
		;
connectAttr "skinCluster20GroupId.id" "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.iog.og[2].gid"
		;
connectAttr "skinCluster20Set.mwc" "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.iog.og[2].gco"
		;
connectAttr "groupId110.id" "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.iog.og[3].gid"
		;
connectAttr "tweakSet20.mwc" "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.iog.og[3].gco"
		;
connectAttr "tweak20.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.twl"
		;
connectAttr "skinCluster16.og[0]" "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.i"
		;
connectAttr "skinCluster16GroupId.id" "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.iog.og[2].gid"
		;
connectAttr "skinCluster16Set.mwc" "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.iog.og[2].gco"
		;
connectAttr "groupId102.id" "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.iog.og[3].gid"
		;
connectAttr "tweakSet16.mwc" "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.iog.og[3].gco"
		;
connectAttr "tweak16.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.twl"
		;
connectAttr "skinCluster18.og[0]" "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.i"
		;
connectAttr "skinCluster18GroupId.id" "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.iog.og[2].gid"
		;
connectAttr "skinCluster18Set.mwc" "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.iog.og[2].gco"
		;
connectAttr "groupId106.id" "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.iog.og[3].gid"
		;
connectAttr "tweakSet18.mwc" "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.iog.og[3].gco"
		;
connectAttr "tweak18.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.twl"
		;
connectAttr "skinCluster17.og[0]" "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.i"
		;
connectAttr "skinCluster17GroupId.id" "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.iog.og[2].gid"
		;
connectAttr "skinCluster17Set.mwc" "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.iog.og[2].gco"
		;
connectAttr "groupId104.id" "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.iog.og[3].gid"
		;
connectAttr "tweakSet17.mwc" "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.iog.og[3].gco"
		;
connectAttr "tweak17.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.twl"
		;
connectAttr "skinCluster14GroupId.id" "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.iog.og[6].gid"
		;
connectAttr "skinCluster14Set.mwc" "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.iog.og[6].gco"
		;
connectAttr "groupId98.id" "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.iog.og[7].gid"
		;
connectAttr "tweakSet14.mwc" "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.iog.og[7].gco"
		;
connectAttr "skinCluster14.og[0]" "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.i"
		;
connectAttr "tweak14.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.twl"
		;
connectAttr "skinCluster13GroupId.id" "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.iog.og[4].gid"
		;
connectAttr "skinCluster13Set.mwc" "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.iog.og[4].gco"
		;
connectAttr "groupId96.id" "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.iog.og[5].gid"
		;
connectAttr "tweakSet13.mwc" "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.iog.og[5].gco"
		;
connectAttr "skinCluster13.og[0]" "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.i"
		;
connectAttr "tweak13.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.twl"
		;
connectAttr "skinCluster12GroupId.id" "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.iog.og[6].gid"
		;
connectAttr "skinCluster12Set.mwc" "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.iog.og[6].gco"
		;
connectAttr "groupId94.id" "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.iog.og[7].gid"
		;
connectAttr "tweakSet12.mwc" "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.iog.og[7].gco"
		;
connectAttr "skinCluster12.og[0]" "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.i"
		;
connectAttr "tweak12.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.twl"
		;
connectAttr "skinCluster11GroupId.id" "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.iog.og[4].gid"
		;
connectAttr "skinCluster11Set.mwc" "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.iog.og[4].gco"
		;
connectAttr "groupId92.id" "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.iog.og[5].gid"
		;
connectAttr "tweakSet11.mwc" "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.iog.og[5].gco"
		;
connectAttr "skinCluster11.og[0]" "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.i"
		;
connectAttr "tweak11.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.twl"
		;
connectAttr "skinCluster6.og[0]" "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.i"
		;
connectAttr "skinCluster6GroupId.id" "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.iog.og[4].gid"
		;
connectAttr "skinCluster6Set.mwc" "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.iog.og[4].gco"
		;
connectAttr "groupId81.id" "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.iog.og[5].gid"
		;
connectAttr "tweakSet6.mwc" "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.iog.og[5].gco"
		;
connectAttr "tweak6.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.twl"
		;
connectAttr "skinCluster5.og[0]" "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.i"
		;
connectAttr "groupId77.id" "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0].gco"
		;
connectAttr "skinCluster5GroupId.id" "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[7].gid"
		;
connectAttr "skinCluster5Set.mwc" "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[7].gco"
		;
connectAttr "groupId79.id" "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[8].gid"
		;
connectAttr "tweakSet5.mwc" "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[8].gco"
		;
connectAttr "tweak5.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.twl"
		;
connectAttr "skinCluster8.og[0]" "fistRightShape.i";
connectAttr "skinCluster8GroupId.id" "fistRightShape.iog.og[3].gid";
connectAttr "skinCluster8Set.mwc" "fistRightShape.iog.og[3].gco";
connectAttr "groupId85.id" "fistRightShape.iog.og[4].gid";
connectAttr "tweakSet8.mwc" "fistRightShape.iog.og[4].gco";
connectAttr "tweak8.vl[0].vt[0]" "fistRightShape.twl";
connectAttr "skinCluster7.og[0]" "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.i"
		;
connectAttr "skinCluster7GroupId.id" "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[5].gid"
		;
connectAttr "skinCluster7Set.mwc" "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[5].gco"
		;
connectAttr "groupId83.id" "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[6].gid"
		;
connectAttr "tweakSet7.mwc" "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[6].gco"
		;
connectAttr "tweak7.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.twl"
		;
connectAttr "skinCluster10.og[0]" "BackPackShape.i";
connectAttr "skinCluster10GroupId.id" "BackPackShape.iog.og[2].gid";
connectAttr "skinCluster10Set.mwc" "BackPackShape.iog.og[2].gco";
connectAttr "groupId90.id" "BackPackShape.iog.og[3].gid";
connectAttr "tweakSet10.mwc" "BackPackShape.iog.og[3].gco";
connectAttr "tweak10.vl[0].vt[0]" "BackPackShape.twl";
connectAttr "skinCluster1.og[0]" "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.i"
		;
connectAttr "skinCluster1GroupId.id" "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.iog.og[2].gid"
		;
connectAttr "skinCluster1Set.mwc" "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.iog.og[2].gco"
		;
connectAttr "groupId69.id" "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.iog.og[3].gid"
		;
connectAttr "tweakSet1.mwc" "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.iog.og[3].gco"
		;
connectAttr "tweak1.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.twl"
		;
connectAttr "skinCluster2.og[0]" "ShoulderLeftShape.i";
connectAttr "groupId70.id" "ShoulderLeftShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "ShoulderLeftShape.iog.og[0].gco";
connectAttr "skinCluster2GroupId.id" "ShoulderLeftShape.iog.og[3].gid";
connectAttr "skinCluster2Set.mwc" "ShoulderLeftShape.iog.og[3].gco";
connectAttr "groupId72.id" "ShoulderLeftShape.iog.og[4].gid";
connectAttr "tweakSet2.mwc" "ShoulderLeftShape.iog.og[4].gco";
connectAttr "tweak2.vl[0].vt[0]" "ShoulderLeftShape.twl";
connectAttr "skinCluster3.og[0]" "fistLeftShape.i";
connectAttr "skinCluster3GroupId.id" "fistLeftShape.iog.og[1].gid";
connectAttr "skinCluster3Set.mwc" "fistLeftShape.iog.og[1].gco";
connectAttr "groupId74.id" "fistLeftShape.iog.og[2].gid";
connectAttr "tweakSet3.mwc" "fistLeftShape.iog.og[2].gco";
connectAttr "tweak3.vl[0].vt[0]" "fistLeftShape.twl";
connectAttr "skinCluster4.og[0]" "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.i"
		;
connectAttr "skinCluster4GroupId.id" "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.iog.og[1].gid"
		;
connectAttr "skinCluster4Set.mwc" "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.iog.og[1].gco"
		;
connectAttr "groupId76.id" "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.iog.og[2].gid"
		;
connectAttr "tweakSet4.mwc" "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.iog.og[2].gco"
		;
connectAttr "tweak4.vl[0].vt[0]" "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.twl"
		;
connectAttr "polyPlane1.out" "pPlaneShape1.i";
connectAttr "groupId62.id" "|Drill_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|Drill_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0].gco"
		;
connectAttr "groupId66.id" "|Drill_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|Drill_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[0].gco"
		;
connectAttr "groupId67.id" "drillRightShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "drillRightShape.iog.og[0].gco";
connectAttr "groupId63.id" "|Gun_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|Gun_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0].gco"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube2.out" "transformGeometry1.ig";
connectAttr "transformGeometry1.og" "transformGeometry2.ig";
connectAttr "polyCube3.out" "transformGeometry3.ig";
connectAttr "polyCube4.out" "transformGeometry4.ig";
connectAttr "polyCube1.out" "transformGeometry5.ig";
connectAttr "transformGeometry2.og" "transformGeometry6.ig";
connectAttr "transformGeometry3.og" "transformGeometry7.ig";
connectAttr "transformGeometry4.og" "transformGeometry8.ig";
connectAttr "polyCube5.out" "transformGeometry9.ig";
connectAttr "polyCube6.out" "transformGeometry10.ig";
connectAttr "polyCube10.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "transformGeometry11.ig";
connectAttr "transformGeometry11.og" "transformGeometry12.ig";
connectAttr "polyCube12.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "transformGeometry13.ig";
connectAttr "polyCube14.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "transformGeometry14.ig";
connectAttr "polyCube15.out" "transformGeometry15.ig";
connectAttr "polyCube17.out" "transformGeometry16.ig";
connectAttr "polyCube18.out" "transformGeometry17.ig";
connectAttr "polyCube19.out" "transformGeometry18.ig";
connectAttr "polyCube22.out" "transformGeometry19.ig";
connectAttr "polyCube21.out" "transformGeometry20.ig";
connectAttr "polyCube20.out" "transformGeometry21.ig";
connectAttr "polyCube23.out" "transformGeometry22.ig";
connectAttr "polyCube7.out" "transformGeometry23.ig";
connectAttr "polyCube16.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "transformGeometry24.ig";
connectAttr "polyCube8.out" "transformGeometry25.ig";
connectAttr "polyCube9.out" "polyTweak5.ip";
connectAttr "polyTweak5.out" "transformGeometry26.ig";
connectAttr "transformGeometry12.og" "transformGeometry27.ig";
connectAttr "polyCube11.out" "polyTweak6.ip";
connectAttr "polyTweak6.out" "transformGeometry28.ig";
connectAttr "transformGeometry13.og" "transformGeometry29.ig";
connectAttr "polyCube13.out" "polyTweak7.ip";
connectAttr "polyTweak7.out" "transformGeometry30.ig";
connectAttr "transformGeometry27.og" "transformGeometry31.ig";
connectAttr "transformGeometry17.og" "transformGeometry32.ig";
connectAttr "transformGeometry18.og" "transformGeometry33.ig";
connectAttr "transformGeometry19.og" "transformGeometry34.ig";
connectAttr "transformGeometry20.og" "transformGeometry35.ig";
connectAttr "transformGeometry21.og" "transformGeometry36.ig";
connectAttr "transformGeometry22.og" "transformGeometry37.ig";
connectAttr "transformGeometry31.og" "transformGeometry38.ig";
connectAttr "transformGeometry32.og" "transformGeometry39.ig";
connectAttr "transformGeometry33.og" "transformGeometry40.ig";
connectAttr "transformGeometry34.og" "transformGeometry41.ig";
connectAttr "transformGeometry35.og" "transformGeometry42.ig";
connectAttr "transformGeometry36.og" "transformGeometry43.ig";
connectAttr "transformGeometry37.og" "transformGeometry44.ig";
connectAttr "transformGeometry38.og" "transformGeometry45.ig";
connectAttr "transformGeometry39.og" "transformGeometry46.ig";
connectAttr "transformGeometry40.og" "transformGeometry47.ig";
connectAttr "transformGeometry41.og" "transformGeometry48.ig";
connectAttr "transformGeometry42.og" "transformGeometry49.ig";
connectAttr "transformGeometry43.og" "transformGeometry50.ig";
connectAttr "transformGeometry44.og" "transformGeometry51.ig";
connectAttr "polyCube24.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "transformGeometry52.ig";
connectAttr "polyCube26.out" "transformGeometry53.ig";
connectAttr "polyCube27.out" "transformGeometry54.ig";
connectAttr "polyCube25.out" "transformGeometry55.ig";
connectAttr "transformGeometry9.og" "transformGeometry56.ig";
connectAttr "transformGeometry10.og" "transformGeometry57.ig";
connectAttr "layerManager.dli[3]" "layer1.id";
connectAttr "layerManager.dli[4]" "layer2.id";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "leftShoulder.wm" "skinCluster1.ma[0]";
connectAttr "leftUpperArm.wm" "skinCluster1.ma[1]";
connectAttr "leftElbow.wm" "skinCluster1.ma[2]";
connectAttr "leftWrist.wm" "skinCluster1.ma[3]";
connectAttr "leftShoulder.liw" "skinCluster1.lw[0]";
connectAttr "leftUpperArm.liw" "skinCluster1.lw[1]";
connectAttr "leftElbow.liw" "skinCluster1.lw[2]";
connectAttr "leftWrist.liw" "skinCluster1.lw[3]";
connectAttr "leftShoulder.obcc" "skinCluster1.ifcl[0]";
connectAttr "leftUpperArm.obcc" "skinCluster1.ifcl[1]";
connectAttr "leftElbow.obcc" "skinCluster1.ifcl[2]";
connectAttr "leftWrist.obcc" "skinCluster1.ifcl[3]";
connectAttr "leftElbow.msg" "skinCluster1.ptt";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId69.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.iog.og[2]" "skinCluster1Set.dsm"
		 -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId69.msg" "tweakSet1.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.iog.og[3]" "tweakSet1.dsm"
		 -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "upperArmLeftShapeOrig.w" "groupParts2.ig";
connectAttr "groupId69.id" "groupParts2.gi";
connectAttr "leftShoulder.msg" "bindPose1.m[0]";
connectAttr "leftUpperArm.msg" "bindPose1.m[1]";
connectAttr "leftElbow.msg" "bindPose1.m[2]";
connectAttr "leftWrist.msg" "bindPose1.m[3]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "leftShoulder.bps" "bindPose1.wm[0]";
connectAttr "leftUpperArm.bps" "bindPose1.wm[1]";
connectAttr "leftElbow.bps" "bindPose1.wm[2]";
connectAttr "leftWrist.bps" "bindPose1.wm[3]";
connectAttr "skinCluster2GroupParts.og" "skinCluster2.ip[0].ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2.ip[0].gi";
connectAttr "leftShoulder.wm" "skinCluster2.ma[0]";
connectAttr "leftUpperArm.wm" "skinCluster2.ma[1]";
connectAttr "leftElbow.wm" "skinCluster2.ma[2]";
connectAttr "leftShoulder.liw" "skinCluster2.lw[0]";
connectAttr "leftUpperArm.liw" "skinCluster2.lw[1]";
connectAttr "leftElbow.liw" "skinCluster2.lw[2]";
connectAttr "leftShoulder.obcc" "skinCluster2.ifcl[0]";
connectAttr "leftUpperArm.obcc" "skinCluster2.ifcl[1]";
connectAttr "leftElbow.obcc" "skinCluster2.ifcl[2]";
connectAttr "bindPose1.msg" "skinCluster2.bp";
connectAttr "leftShoulder.msg" "skinCluster2.ptt";
connectAttr "ShoulderLeftShapeOrig.w" "groupParts3.ig";
connectAttr "groupId70.id" "groupParts3.gi";
connectAttr "groupParts5.og" "tweak2.ip[0].ig";
connectAttr "groupId72.id" "tweak2.ip[0].gi";
connectAttr "skinCluster2GroupId.msg" "skinCluster2Set.gn" -na;
connectAttr "ShoulderLeftShape.iog.og[3]" "skinCluster2Set.dsm" -na;
connectAttr "skinCluster2.msg" "skinCluster2Set.ub[0]";
connectAttr "tweak2.og[0]" "skinCluster2GroupParts.ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2GroupParts.gi";
connectAttr "groupId72.msg" "tweakSet2.gn" -na;
connectAttr "ShoulderLeftShape.iog.og[4]" "tweakSet2.dsm" -na;
connectAttr "tweak2.msg" "tweakSet2.ub[0]";
connectAttr "groupParts3.og" "groupParts5.ig";
connectAttr "groupId72.id" "groupParts5.gi";
connectAttr "skinCluster3GroupParts.og" "skinCluster3.ip[0].ig";
connectAttr "skinCluster3GroupId.id" "skinCluster3.ip[0].gi";
connectAttr "leftUpperArm.wm" "skinCluster3.ma[1]";
connectAttr "leftElbow.wm" "skinCluster3.ma[2]";
connectAttr "leftWrist.wm" "skinCluster3.ma[3]";
connectAttr "leftUpperArm.liw" "skinCluster3.lw[1]";
connectAttr "leftElbow.liw" "skinCluster3.lw[2]";
connectAttr "leftWrist.liw" "skinCluster3.lw[3]";
connectAttr "leftUpperArm.obcc" "skinCluster3.ifcl[1]";
connectAttr "leftElbow.obcc" "skinCluster3.ifcl[2]";
connectAttr "leftWrist.obcc" "skinCluster3.ifcl[3]";
connectAttr "bindPose1.msg" "skinCluster3.bp";
connectAttr "leftWrist.msg" "skinCluster3.ptt";
connectAttr "groupParts7.og" "tweak3.ip[0].ig";
connectAttr "groupId74.id" "tweak3.ip[0].gi";
connectAttr "skinCluster3GroupId.msg" "skinCluster3Set.gn" -na;
connectAttr "fistLeftShape.iog.og[1]" "skinCluster3Set.dsm" -na;
connectAttr "skinCluster3.msg" "skinCluster3Set.ub[0]";
connectAttr "tweak3.og[0]" "skinCluster3GroupParts.ig";
connectAttr "skinCluster3GroupId.id" "skinCluster3GroupParts.gi";
connectAttr "groupId74.msg" "tweakSet3.gn" -na;
connectAttr "fistLeftShape.iog.og[2]" "tweakSet3.dsm" -na;
connectAttr "tweak3.msg" "tweakSet3.ub[0]";
connectAttr "fistLeftShapeOrig.w" "groupParts7.ig";
connectAttr "groupId74.id" "groupParts7.gi";
connectAttr "skinCluster4GroupParts.og" "skinCluster4.ip[0].ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4.ip[0].gi";
connectAttr "leftShoulder.wm" "skinCluster4.ma[0]";
connectAttr "leftUpperArm.wm" "skinCluster4.ma[1]";
connectAttr "leftElbow.wm" "skinCluster4.ma[2]";
connectAttr "leftWrist.wm" "skinCluster4.ma[3]";
connectAttr "leftShoulder.liw" "skinCluster4.lw[0]";
connectAttr "leftUpperArm.liw" "skinCluster4.lw[1]";
connectAttr "leftElbow.liw" "skinCluster4.lw[2]";
connectAttr "leftWrist.liw" "skinCluster4.lw[3]";
connectAttr "leftShoulder.obcc" "skinCluster4.ifcl[0]";
connectAttr "leftUpperArm.obcc" "skinCluster4.ifcl[1]";
connectAttr "leftElbow.obcc" "skinCluster4.ifcl[2]";
connectAttr "leftWrist.obcc" "skinCluster4.ifcl[3]";
connectAttr "bindPose1.msg" "skinCluster4.bp";
connectAttr "leftElbow.msg" "skinCluster4.ptt";
connectAttr "groupParts9.og" "tweak4.ip[0].ig";
connectAttr "groupId76.id" "tweak4.ip[0].gi";
connectAttr "skinCluster4GroupId.msg" "skinCluster4Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.iog.og[1]" "skinCluster4Set.dsm"
		 -na;
connectAttr "skinCluster4.msg" "skinCluster4Set.ub[0]";
connectAttr "tweak4.og[0]" "skinCluster4GroupParts.ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4GroupParts.gi";
connectAttr "groupId76.msg" "tweakSet4.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.iog.og[2]" "tweakSet4.dsm"
		 -na;
connectAttr "tweak4.msg" "tweakSet4.ub[0]";
connectAttr "lowerArmLeftShapeOrig.w" "groupParts9.ig";
connectAttr "groupId76.id" "groupParts9.gi";
connectAttr "skinCluster5GroupParts.og" "skinCluster5.ip[0].ig";
connectAttr "skinCluster5GroupId.id" "skinCluster5.ip[0].gi";
connectAttr "bindPose3.msg" "skinCluster5.bp";
connectAttr "rightShoulder.wm" "skinCluster5.ma[0]";
connectAttr "rightUpperArm.wm" "skinCluster5.ma[1]";
connectAttr "rightElbow.wm" "skinCluster5.ma[2]";
connectAttr "rightShoulder.liw" "skinCluster5.lw[0]";
connectAttr "rightUpperArm.liw" "skinCluster5.lw[1]";
connectAttr "rightElbow.liw" "skinCluster5.lw[2]";
connectAttr "rightShoulder.obcc" "skinCluster5.ifcl[0]";
connectAttr "rightUpperArm.obcc" "skinCluster5.ifcl[1]";
connectAttr "rightElbow.obcc" "skinCluster5.ifcl[2]";
connectAttr "shoulderRightShapeOrig.w" "groupParts10.ig";
connectAttr "groupId77.id" "groupParts10.gi";
connectAttr "groupParts12.og" "tweak5.ip[0].ig";
connectAttr "groupId79.id" "tweak5.ip[0].gi";
connectAttr "skinCluster5GroupId.msg" "skinCluster5Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[7]" "skinCluster5Set.dsm"
		 -na;
connectAttr "skinCluster5.msg" "skinCluster5Set.ub[0]";
connectAttr "tweak5.og[0]" "skinCluster5GroupParts.ig";
connectAttr "skinCluster5GroupId.id" "skinCluster5GroupParts.gi";
connectAttr "groupId79.msg" "tweakSet5.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[8]" "tweakSet5.dsm"
		 -na;
connectAttr "tweak5.msg" "tweakSet5.ub[0]";
connectAttr "groupParts10.og" "groupParts12.ig";
connectAttr "groupId79.id" "groupParts12.gi";
connectAttr "rightShoulder.msg" "bindPose3.m[0]";
connectAttr "rightUpperArm.msg" "bindPose3.m[1]";
connectAttr "rightElbow.msg" "bindPose3.m[2]";
connectAttr "rightWrist.msg" "bindPose3.m[4]";
connectAttr "bindPose3.w" "bindPose3.p[0]";
connectAttr "bindPose3.m[0]" "bindPose3.p[1]";
connectAttr "bindPose3.m[1]" "bindPose3.p[2]";
connectAttr "bindPose3.m[2]" "bindPose3.p[4]";
connectAttr "rightShoulder.bps" "bindPose3.wm[0]";
connectAttr "rightUpperArm.bps" "bindPose3.wm[1]";
connectAttr "rightElbow.bps" "bindPose3.wm[2]";
connectAttr "rightWrist.bps" "bindPose3.wm[4]";
connectAttr "skinCluster6GroupParts.og" "skinCluster6.ip[0].ig";
connectAttr "skinCluster6GroupId.id" "skinCluster6.ip[0].gi";
connectAttr "rightShoulder.wm" "skinCluster6.ma[0]";
connectAttr "rightUpperArm.wm" "skinCluster6.ma[1]";
connectAttr "rightElbow.wm" "skinCluster6.ma[2]";
connectAttr "rightWrist.wm" "skinCluster6.ma[3]";
connectAttr "rightShoulder.liw" "skinCluster6.lw[0]";
connectAttr "rightUpperArm.liw" "skinCluster6.lw[1]";
connectAttr "rightElbow.liw" "skinCluster6.lw[2]";
connectAttr "rightWrist.liw" "skinCluster6.lw[3]";
connectAttr "rightShoulder.obcc" "skinCluster6.ifcl[0]";
connectAttr "rightUpperArm.obcc" "skinCluster6.ifcl[1]";
connectAttr "rightElbow.obcc" "skinCluster6.ifcl[2]";
connectAttr "rightWrist.obcc" "skinCluster6.ifcl[3]";
connectAttr "bindPose3.msg" "skinCluster6.bp";
connectAttr "groupParts14.og" "tweak6.ip[0].ig";
connectAttr "groupId81.id" "tweak6.ip[0].gi";
connectAttr "skinCluster6GroupId.msg" "skinCluster6Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.iog.og[4]" "skinCluster6Set.dsm"
		 -na;
connectAttr "skinCluster6.msg" "skinCluster6Set.ub[0]";
connectAttr "tweak6.og[0]" "skinCluster6GroupParts.ig";
connectAttr "skinCluster6GroupId.id" "skinCluster6GroupParts.gi";
connectAttr "groupId81.msg" "tweakSet6.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.iog.og[5]" "tweakSet6.dsm"
		 -na;
connectAttr "tweak6.msg" "tweakSet6.ub[0]";
connectAttr "upperArmRightShapeOrig.w" "groupParts14.ig";
connectAttr "groupId81.id" "groupParts14.gi";
connectAttr "skinCluster7GroupParts.og" "skinCluster7.ip[0].ig";
connectAttr "skinCluster7GroupId.id" "skinCluster7.ip[0].gi";
connectAttr "rightShoulder.wm" "skinCluster7.ma[0]";
connectAttr "rightUpperArm.wm" "skinCluster7.ma[1]";
connectAttr "rightElbow.wm" "skinCluster7.ma[2]";
connectAttr "rightWrist.wm" "skinCluster7.ma[3]";
connectAttr "rightShoulder.liw" "skinCluster7.lw[0]";
connectAttr "rightUpperArm.liw" "skinCluster7.lw[1]";
connectAttr "rightElbow.liw" "skinCluster7.lw[2]";
connectAttr "rightWrist.liw" "skinCluster7.lw[3]";
connectAttr "rightShoulder.obcc" "skinCluster7.ifcl[0]";
connectAttr "rightUpperArm.obcc" "skinCluster7.ifcl[1]";
connectAttr "rightElbow.obcc" "skinCluster7.ifcl[2]";
connectAttr "rightWrist.obcc" "skinCluster7.ifcl[3]";
connectAttr "bindPose3.msg" "skinCluster7.bp";
connectAttr "groupParts16.og" "tweak7.ip[0].ig";
connectAttr "groupId83.id" "tweak7.ip[0].gi";
connectAttr "skinCluster7GroupId.msg" "skinCluster7Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[5]" "skinCluster7Set.dsm"
		 -na;
connectAttr "skinCluster7.msg" "skinCluster7Set.ub[0]";
connectAttr "tweak7.og[0]" "skinCluster7GroupParts.ig";
connectAttr "skinCluster7GroupId.id" "skinCluster7GroupParts.gi";
connectAttr "groupId83.msg" "tweakSet7.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[6]" "tweakSet7.dsm"
		 -na;
connectAttr "tweak7.msg" "tweakSet7.ub[0]";
connectAttr "lowerArmRightShapeOrig.w" "groupParts16.ig";
connectAttr "groupId83.id" "groupParts16.gi";
connectAttr "skinCluster8GroupParts.og" "skinCluster8.ip[0].ig";
connectAttr "skinCluster8GroupId.id" "skinCluster8.ip[0].gi";
connectAttr "rightUpperArm.wm" "skinCluster8.ma[1]";
connectAttr "rightElbow.wm" "skinCluster8.ma[2]";
connectAttr "rightWrist.wm" "skinCluster8.ma[3]";
connectAttr "rightUpperArm.liw" "skinCluster8.lw[1]";
connectAttr "rightElbow.liw" "skinCluster8.lw[2]";
connectAttr "rightWrist.liw" "skinCluster8.lw[3]";
connectAttr "rightUpperArm.obcc" "skinCluster8.ifcl[1]";
connectAttr "rightElbow.obcc" "skinCluster8.ifcl[2]";
connectAttr "rightWrist.obcc" "skinCluster8.ifcl[3]";
connectAttr "bindPose3.msg" "skinCluster8.bp";
connectAttr "groupParts18.og" "tweak8.ip[0].ig";
connectAttr "groupId85.id" "tweak8.ip[0].gi";
connectAttr "skinCluster8GroupId.msg" "skinCluster8Set.gn" -na;
connectAttr "fistRightShape.iog.og[3]" "skinCluster8Set.dsm" -na;
connectAttr "skinCluster8.msg" "skinCluster8Set.ub[0]";
connectAttr "tweak8.og[0]" "skinCluster8GroupParts.ig";
connectAttr "skinCluster8GroupId.id" "skinCluster8GroupParts.gi";
connectAttr "groupId85.msg" "tweakSet8.gn" -na;
connectAttr "fistRightShape.iog.og[4]" "tweakSet8.dsm" -na;
connectAttr "tweak8.msg" "tweakSet8.ub[0]";
connectAttr "fistRightShapeOrig.w" "groupParts18.ig";
connectAttr "groupId85.id" "groupParts18.gi";
connectAttr "skinCluster9GroupParts.og" "skinCluster9.ip[0].ig";
connectAttr "skinCluster9GroupId.id" "skinCluster9.ip[0].gi";
connectAttr "bindPose4.msg" "skinCluster9.bp";
connectAttr "spine.wm" "skinCluster9.ma[0]";
connectAttr "chest.wm" "skinCluster9.ma[1]";
connectAttr "rightClavacle.wm" "skinCluster9.ma[3]";
connectAttr "leftClavacle.wm" "skinCluster9.ma[5]";
connectAttr "spine.liw" "skinCluster9.lw[0]";
connectAttr "chest.liw" "skinCluster9.lw[1]";
connectAttr "rightClavacle.liw" "skinCluster9.lw[3]";
connectAttr "leftClavacle.liw" "skinCluster9.lw[5]";
connectAttr "spine.obcc" "skinCluster9.ifcl[0]";
connectAttr "chest.obcc" "skinCluster9.ifcl[1]";
connectAttr "rightClavacle.obcc" "skinCluster9.ifcl[3]";
connectAttr "leftClavacle.obcc" "skinCluster9.ifcl[5]";
connectAttr "BasicMechGrouped1_Basic_Torso_base1ShapeOrig.w" "groupParts19.ig";
connectAttr "groupId86.id" "groupParts19.gi";
connectAttr "groupParts21.og" "tweak9.ip[0].ig";
connectAttr "groupId88.id" "tweak9.ip[0].gi";
connectAttr "skinCluster9GroupId.msg" "skinCluster9Set.gn" -na;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[13]" "skinCluster9Set.dsm"
		 -na;
connectAttr "skinCluster9.msg" "skinCluster9Set.ub[0]";
connectAttr "tweak9.og[0]" "skinCluster9GroupParts.ig";
connectAttr "skinCluster9GroupId.id" "skinCluster9GroupParts.gi";
connectAttr "groupId88.msg" "tweakSet9.gn" -na;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[14]" "tweakSet9.dsm"
		 -na;
connectAttr "tweak9.msg" "tweakSet9.ub[0]";
connectAttr "groupParts19.og" "groupParts21.ig";
connectAttr "groupId88.id" "groupParts21.gi";
connectAttr "spine.msg" "bindPose4.m[0]";
connectAttr "chest.msg" "bindPose4.m[1]";
connectAttr "rightClavacle.msg" "bindPose4.m[3]";
connectAttr "leftClavacle.msg" "bindPose4.m[5]";
connectAttr "bindPose4.w" "bindPose4.p[0]";
connectAttr "bindPose4.m[0]" "bindPose4.p[1]";
connectAttr "bindPose4.m[1]" "bindPose4.p[2]";
connectAttr "bindPose4.m[2]" "bindPose4.p[3]";
connectAttr "bindPose4.m[1]" "bindPose4.p[4]";
connectAttr "bindPose4.m[4]" "bindPose4.p[5]";
connectAttr "spine.bps" "bindPose4.wm[0]";
connectAttr "chest.bps" "bindPose4.wm[1]";
connectAttr "rightClavacle.bps" "bindPose4.wm[3]";
connectAttr "leftClavacle.bps" "bindPose4.wm[5]";
connectAttr "skinCluster10GroupParts.og" "skinCluster10.ip[0].ig";
connectAttr "skinCluster10GroupId.id" "skinCluster10.ip[0].gi";
connectAttr "spine.wm" "skinCluster10.ma[0]";
connectAttr "chest.wm" "skinCluster10.ma[1]";
connectAttr "rightClavacle.wm" "skinCluster10.ma[3]";
connectAttr "leftClavacle.wm" "skinCluster10.ma[5]";
connectAttr "spine.liw" "skinCluster10.lw[0]";
connectAttr "chest.liw" "skinCluster10.lw[1]";
connectAttr "rightClavacle.liw" "skinCluster10.lw[3]";
connectAttr "leftClavacle.liw" "skinCluster10.lw[5]";
connectAttr "spine.obcc" "skinCluster10.ifcl[0]";
connectAttr "chest.obcc" "skinCluster10.ifcl[1]";
connectAttr "rightClavacle.obcc" "skinCluster10.ifcl[3]";
connectAttr "leftClavacle.obcc" "skinCluster10.ifcl[5]";
connectAttr "bindPose4.msg" "skinCluster10.bp";
connectAttr "groupParts23.og" "tweak10.ip[0].ig";
connectAttr "groupId90.id" "tweak10.ip[0].gi";
connectAttr "skinCluster10GroupId.msg" "skinCluster10Set.gn" -na;
connectAttr "BackPackShape.iog.og[2]" "skinCluster10Set.dsm" -na;
connectAttr "skinCluster10.msg" "skinCluster10Set.ub[0]";
connectAttr "tweak10.og[0]" "skinCluster10GroupParts.ig";
connectAttr "skinCluster10GroupId.id" "skinCluster10GroupParts.gi";
connectAttr "groupId90.msg" "tweakSet10.gn" -na;
connectAttr "BackPackShape.iog.og[3]" "tweakSet10.dsm" -na;
connectAttr "tweak10.msg" "tweakSet10.ub[0]";
connectAttr "BackPackShapeOrig.w" "groupParts23.ig";
connectAttr "groupId90.id" "groupParts23.gi";
connectAttr "skinCluster11GroupParts.og" "skinCluster11.ip[0].ig";
connectAttr "skinCluster11GroupId.id" "skinCluster11.ip[0].gi";
connectAttr "chest.wm" "skinCluster11.ma[0]";
connectAttr "rightClavacle.wm" "skinCluster11.ma[2]";
connectAttr "chest.liw" "skinCluster11.lw[0]";
connectAttr "rightClavacle.liw" "skinCluster11.lw[2]";
connectAttr "chest.obcc" "skinCluster11.ifcl[0]";
connectAttr "rightClavacle.obcc" "skinCluster11.ifcl[2]";
connectAttr "bindPose4.msg" "skinCluster11.bp";
connectAttr "groupParts25.og" "tweak11.ip[0].ig";
connectAttr "groupId92.id" "tweak11.ip[0].gi";
connectAttr "skinCluster11GroupId.msg" "skinCluster11Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.iog.og[4]" "skinCluster11Set.dsm"
		 -na;
connectAttr "skinCluster11.msg" "skinCluster11Set.ub[0]";
connectAttr "tweak11.og[0]" "skinCluster11GroupParts.ig";
connectAttr "skinCluster11GroupId.id" "skinCluster11GroupParts.gi";
connectAttr "groupId92.msg" "tweakSet11.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.iog.og[5]" "tweakSet11.dsm"
		 -na;
connectAttr "tweak11.msg" "tweakSet11.ub[0]";
connectAttr "panelRightShape2Orig.w" "groupParts25.ig";
connectAttr "groupId92.id" "groupParts25.gi";
connectAttr "skinCluster12GroupParts.og" "skinCluster12.ip[0].ig";
connectAttr "skinCluster12GroupId.id" "skinCluster12.ip[0].gi";
connectAttr "chest.wm" "skinCluster12.ma[0]";
connectAttr "rightClavacle.wm" "skinCluster12.ma[2]";
connectAttr "chest.liw" "skinCluster12.lw[0]";
connectAttr "rightClavacle.liw" "skinCluster12.lw[2]";
connectAttr "chest.obcc" "skinCluster12.ifcl[0]";
connectAttr "rightClavacle.obcc" "skinCluster12.ifcl[2]";
connectAttr "bindPose4.msg" "skinCluster12.bp";
connectAttr "groupParts27.og" "tweak12.ip[0].ig";
connectAttr "groupId94.id" "tweak12.ip[0].gi";
connectAttr "skinCluster12GroupId.msg" "skinCluster12Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.iog.og[6]" "skinCluster12Set.dsm"
		 -na;
connectAttr "skinCluster12.msg" "skinCluster12Set.ub[0]";
connectAttr "tweak12.og[0]" "skinCluster12GroupParts.ig";
connectAttr "skinCluster12GroupId.id" "skinCluster12GroupParts.gi";
connectAttr "groupId94.msg" "tweakSet12.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.iog.og[7]" "tweakSet12.dsm"
		 -na;
connectAttr "tweak12.msg" "tweakSet12.ub[0]";
connectAttr "panelRightShape1Orig.w" "groupParts27.ig";
connectAttr "groupId94.id" "groupParts27.gi";
connectAttr "skinCluster13GroupParts.og" "skinCluster13.ip[0].ig";
connectAttr "skinCluster13GroupId.id" "skinCluster13.ip[0].gi";
connectAttr "chest.wm" "skinCluster13.ma[0]";
connectAttr "leftClavacle.wm" "skinCluster13.ma[4]";
connectAttr "chest.liw" "skinCluster13.lw[0]";
connectAttr "leftClavacle.liw" "skinCluster13.lw[4]";
connectAttr "chest.obcc" "skinCluster13.ifcl[0]";
connectAttr "leftClavacle.obcc" "skinCluster13.ifcl[4]";
connectAttr "bindPose4.msg" "skinCluster13.bp";
connectAttr "groupParts29.og" "tweak13.ip[0].ig";
connectAttr "groupId96.id" "tweak13.ip[0].gi";
connectAttr "skinCluster13GroupId.msg" "skinCluster13Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.iog.og[4]" "skinCluster13Set.dsm"
		 -na;
connectAttr "skinCluster13.msg" "skinCluster13Set.ub[0]";
connectAttr "tweak13.og[0]" "skinCluster13GroupParts.ig";
connectAttr "skinCluster13GroupId.id" "skinCluster13GroupParts.gi";
connectAttr "groupId96.msg" "tweakSet13.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.iog.og[5]" "tweakSet13.dsm"
		 -na;
connectAttr "tweak13.msg" "tweakSet13.ub[0]";
connectAttr "panelLeftShape2Orig.w" "groupParts29.ig";
connectAttr "groupId96.id" "groupParts29.gi";
connectAttr "skinCluster14GroupParts.og" "skinCluster14.ip[0].ig";
connectAttr "skinCluster14GroupId.id" "skinCluster14.ip[0].gi";
connectAttr "chest.wm" "skinCluster14.ma[0]";
connectAttr "leftClavacle.wm" "skinCluster14.ma[4]";
connectAttr "chest.liw" "skinCluster14.lw[0]";
connectAttr "leftClavacle.liw" "skinCluster14.lw[4]";
connectAttr "chest.obcc" "skinCluster14.ifcl[0]";
connectAttr "leftClavacle.obcc" "skinCluster14.ifcl[4]";
connectAttr "bindPose4.msg" "skinCluster14.bp";
connectAttr "groupParts31.og" "tweak14.ip[0].ig";
connectAttr "groupId98.id" "tweak14.ip[0].gi";
connectAttr "skinCluster14GroupId.msg" "skinCluster14Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.iog.og[6]" "skinCluster14Set.dsm"
		 -na;
connectAttr "skinCluster14.msg" "skinCluster14Set.ub[0]";
connectAttr "tweak14.og[0]" "skinCluster14GroupParts.ig";
connectAttr "skinCluster14GroupId.id" "skinCluster14GroupParts.gi";
connectAttr "groupId98.msg" "tweakSet14.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.iog.og[7]" "tweakSet14.dsm"
		 -na;
connectAttr "tweak14.msg" "tweakSet14.ub[0]";
connectAttr "panelLeftShape1Orig.w" "groupParts31.ig";
connectAttr "groupId98.id" "groupParts31.gi";
connectAttr "skinCluster15GroupParts.og" "skinCluster15.ip[0].ig";
connectAttr "skinCluster15GroupId.id" "skinCluster15.ip[0].gi";
connectAttr "bindPose5.msg" "skinCluster15.bp";
connectAttr "hipsJnt.wm" "skinCluster15.ma[0]";
connectAttr "leftUpperLeg.wm" "skinCluster15.ma[1]";
connectAttr "leftLowerLeg.wm" "skinCluster15.ma[2]";
connectAttr "rightUpperLeg.wm" "skinCluster15.ma[5]";
connectAttr "rightLowerLeg.wm" "skinCluster15.ma[6]";
connectAttr "hipsJnt.liw" "skinCluster15.lw[0]";
connectAttr "leftUpperLeg.liw" "skinCluster15.lw[1]";
connectAttr "leftLowerLeg.liw" "skinCluster15.lw[2]";
connectAttr "rightUpperLeg.liw" "skinCluster15.lw[5]";
connectAttr "rightLowerLeg.liw" "skinCluster15.lw[6]";
connectAttr "hipsJnt.obcc" "skinCluster15.ifcl[0]";
connectAttr "leftUpperLeg.obcc" "skinCluster15.ifcl[1]";
connectAttr "leftLowerLeg.obcc" "skinCluster15.ifcl[2]";
connectAttr "rightUpperLeg.obcc" "skinCluster15.ifcl[5]";
connectAttr "rightLowerLeg.obcc" "skinCluster15.ifcl[6]";
connectAttr "groupParts33.og" "tweak15.ip[0].ig";
connectAttr "groupId100.id" "tweak15.ip[0].gi";
connectAttr "skinCluster15GroupId.msg" "skinCluster15Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog.og[2]" "skinCluster15Set.dsm"
		 -na;
connectAttr "skinCluster15.msg" "skinCluster15Set.ub[0]";
connectAttr "tweak15.og[0]" "skinCluster15GroupParts.ig";
connectAttr "skinCluster15GroupId.id" "skinCluster15GroupParts.gi";
connectAttr "groupId100.msg" "tweakSet15.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog.og[3]" "tweakSet15.dsm"
		 -na;
connectAttr "tweak15.msg" "tweakSet15.ub[0]";
connectAttr "hipsShapeOrig.w" "groupParts33.ig";
connectAttr "groupId100.id" "groupParts33.gi";
connectAttr "hipsJnt.msg" "bindPose5.m[0]";
connectAttr "leftUpperLeg.msg" "bindPose5.m[1]";
connectAttr "leftLowerLeg.msg" "bindPose5.m[2]";
connectAttr "leftFoot.msg" "bindPose5.m[3]";
connectAttr "rightUpperLeg.msg" "bindPose5.m[5]";
connectAttr "rightLowerLeg.msg" "bindPose5.m[6]";
connectAttr "rightFoot.msg" "bindPose5.m[7]";
connectAttr "rightToe.msg" "bindPose5.m[14]";
connectAttr "leftToe.msg" "bindPose5.m[17]";
connectAttr "bindPose5.w" "bindPose5.p[0]";
connectAttr "bindPose5.m[0]" "bindPose5.p[1]";
connectAttr "bindPose5.m[1]" "bindPose5.p[2]";
connectAttr "bindPose5.m[2]" "bindPose5.p[3]";
connectAttr "bindPose5.m[0]" "bindPose5.p[5]";
connectAttr "bindPose5.m[5]" "bindPose5.p[6]";
connectAttr "bindPose5.m[6]" "bindPose5.p[7]";
connectAttr "bindPose5.m[7]" "bindPose5.p[14]";
connectAttr "bindPose5.m[3]" "bindPose5.p[17]";
connectAttr "hipsJnt.bps" "bindPose5.wm[0]";
connectAttr "leftUpperLeg.bps" "bindPose5.wm[1]";
connectAttr "leftLowerLeg.bps" "bindPose5.wm[2]";
connectAttr "leftFoot.bps" "bindPose5.wm[3]";
connectAttr "rightUpperLeg.bps" "bindPose5.wm[5]";
connectAttr "rightLowerLeg.bps" "bindPose5.wm[6]";
connectAttr "rightFoot.bps" "bindPose5.wm[7]";
connectAttr "rightToe.bps" "bindPose5.wm[14]";
connectAttr "leftToe.bps" "bindPose5.wm[17]";
connectAttr "skinCluster16GroupParts.og" "skinCluster16.ip[0].ig";
connectAttr "skinCluster16GroupId.id" "skinCluster16.ip[0].gi";
connectAttr "hipsJnt.wm" "skinCluster16.ma[0]";
connectAttr "rightUpperLeg.wm" "skinCluster16.ma[5]";
connectAttr "rightLowerLeg.wm" "skinCluster16.ma[6]";
connectAttr "rightFoot.wm" "skinCluster16.ma[7]";
connectAttr "hipsJnt.liw" "skinCluster16.lw[0]";
connectAttr "rightUpperLeg.liw" "skinCluster16.lw[5]";
connectAttr "rightLowerLeg.liw" "skinCluster16.lw[6]";
connectAttr "rightFoot.liw" "skinCluster16.lw[7]";
connectAttr "hipsJnt.obcc" "skinCluster16.ifcl[0]";
connectAttr "rightUpperLeg.obcc" "skinCluster16.ifcl[5]";
connectAttr "rightLowerLeg.obcc" "skinCluster16.ifcl[6]";
connectAttr "rightFoot.obcc" "skinCluster16.ifcl[7]";
connectAttr "bindPose5.msg" "skinCluster16.bp";
connectAttr "groupParts35.og" "tweak16.ip[0].ig";
connectAttr "groupId102.id" "tweak16.ip[0].gi";
connectAttr "skinCluster16GroupId.msg" "skinCluster16Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.iog.og[2]" "skinCluster16Set.dsm"
		 -na;
connectAttr "skinCluster16.msg" "skinCluster16Set.ub[0]";
connectAttr "tweak16.og[0]" "skinCluster16GroupParts.ig";
connectAttr "skinCluster16GroupId.id" "skinCluster16GroupParts.gi";
connectAttr "groupId102.msg" "tweakSet16.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.iog.og[3]" "tweakSet16.dsm"
		 -na;
connectAttr "tweak16.msg" "tweakSet16.ub[0]";
connectAttr "upperLegRightShapeOrig.w" "groupParts35.ig";
connectAttr "groupId102.id" "groupParts35.gi";
connectAttr "skinCluster17GroupParts.og" "skinCluster17.ip[0].ig";
connectAttr "skinCluster17GroupId.id" "skinCluster17.ip[0].gi";
connectAttr "hipsJnt.wm" "skinCluster17.ma[0]";
connectAttr "rightUpperLeg.wm" "skinCluster17.ma[5]";
connectAttr "rightLowerLeg.wm" "skinCluster17.ma[6]";
connectAttr "rightFoot.wm" "skinCluster17.ma[7]";
connectAttr "hipsJnt.liw" "skinCluster17.lw[0]";
connectAttr "rightUpperLeg.liw" "skinCluster17.lw[5]";
connectAttr "rightLowerLeg.liw" "skinCluster17.lw[6]";
connectAttr "rightFoot.liw" "skinCluster17.lw[7]";
connectAttr "hipsJnt.obcc" "skinCluster17.ifcl[0]";
connectAttr "rightUpperLeg.obcc" "skinCluster17.ifcl[5]";
connectAttr "rightLowerLeg.obcc" "skinCluster17.ifcl[6]";
connectAttr "rightFoot.obcc" "skinCluster17.ifcl[7]";
connectAttr "bindPose5.msg" "skinCluster17.bp";
connectAttr "groupParts37.og" "tweak17.ip[0].ig";
connectAttr "groupId104.id" "tweak17.ip[0].gi";
connectAttr "skinCluster17GroupId.msg" "skinCluster17Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.iog.og[2]" "skinCluster17Set.dsm"
		 -na;
connectAttr "skinCluster17.msg" "skinCluster17Set.ub[0]";
connectAttr "tweak17.og[0]" "skinCluster17GroupParts.ig";
connectAttr "skinCluster17GroupId.id" "skinCluster17GroupParts.gi";
connectAttr "groupId104.msg" "tweakSet17.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.iog.og[3]" "tweakSet17.dsm"
		 -na;
connectAttr "tweak17.msg" "tweakSet17.ub[0]";
connectAttr "kneeRightShapeOrig.w" "groupParts37.ig";
connectAttr "groupId104.id" "groupParts37.gi";
connectAttr "skinCluster18GroupParts.og" "skinCluster18.ip[0].ig";
connectAttr "skinCluster18GroupId.id" "skinCluster18.ip[0].gi";
connectAttr "hipsJnt.wm" "skinCluster18.ma[0]";
connectAttr "rightUpperLeg.wm" "skinCluster18.ma[5]";
connectAttr "rightLowerLeg.wm" "skinCluster18.ma[6]";
connectAttr "rightFoot.wm" "skinCluster18.ma[7]";
connectAttr "rightToe.wm" "skinCluster18.ma[8]";
connectAttr "hipsJnt.liw" "skinCluster18.lw[0]";
connectAttr "rightUpperLeg.liw" "skinCluster18.lw[5]";
connectAttr "rightLowerLeg.liw" "skinCluster18.lw[6]";
connectAttr "rightFoot.liw" "skinCluster18.lw[7]";
connectAttr "rightToe.liw" "skinCluster18.lw[8]";
connectAttr "hipsJnt.obcc" "skinCluster18.ifcl[0]";
connectAttr "rightUpperLeg.obcc" "skinCluster18.ifcl[5]";
connectAttr "rightLowerLeg.obcc" "skinCluster18.ifcl[6]";
connectAttr "rightFoot.obcc" "skinCluster18.ifcl[7]";
connectAttr "rightToe.obcc" "skinCluster18.ifcl[8]";
connectAttr "bindPose5.msg" "skinCluster18.bp";
connectAttr "groupParts39.og" "tweak18.ip[0].ig";
connectAttr "groupId106.id" "tweak18.ip[0].gi";
connectAttr "skinCluster18GroupId.msg" "skinCluster18Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.iog.og[2]" "skinCluster18Set.dsm"
		 -na;
connectAttr "skinCluster18.msg" "skinCluster18Set.ub[0]";
connectAttr "tweak18.og[0]" "skinCluster18GroupParts.ig";
connectAttr "skinCluster18GroupId.id" "skinCluster18GroupParts.gi";
connectAttr "groupId106.msg" "tweakSet18.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.iog.og[3]" "tweakSet18.dsm"
		 -na;
connectAttr "tweak18.msg" "tweakSet18.ub[0]";
connectAttr "lowerLegRightShapeOrig.w" "groupParts39.ig";
connectAttr "groupId106.id" "groupParts39.gi";
connectAttr "skinCluster19GroupParts.og" "skinCluster19.ip[0].ig";
connectAttr "skinCluster19GroupId.id" "skinCluster19.ip[0].gi";
connectAttr "hipsJnt.wm" "skinCluster19.ma[0]";
connectAttr "leftUpperLeg.wm" "skinCluster19.ma[1]";
connectAttr "leftLowerLeg.wm" "skinCluster19.ma[2]";
connectAttr "leftFoot.wm" "skinCluster19.ma[3]";
connectAttr "hipsJnt.liw" "skinCluster19.lw[0]";
connectAttr "leftUpperLeg.liw" "skinCluster19.lw[1]";
connectAttr "leftLowerLeg.liw" "skinCluster19.lw[2]";
connectAttr "leftFoot.liw" "skinCluster19.lw[3]";
connectAttr "hipsJnt.obcc" "skinCluster19.ifcl[0]";
connectAttr "leftUpperLeg.obcc" "skinCluster19.ifcl[1]";
connectAttr "leftLowerLeg.obcc" "skinCluster19.ifcl[2]";
connectAttr "leftFoot.obcc" "skinCluster19.ifcl[3]";
connectAttr "bindPose5.msg" "skinCluster19.bp";
connectAttr "groupParts41.og" "tweak19.ip[0].ig";
connectAttr "groupId108.id" "tweak19.ip[0].gi";
connectAttr "skinCluster19GroupId.msg" "skinCluster19Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.iog.og[2]" "skinCluster19Set.dsm"
		 -na;
connectAttr "skinCluster19.msg" "skinCluster19Set.ub[0]";
connectAttr "tweak19.og[0]" "skinCluster19GroupParts.ig";
connectAttr "skinCluster19GroupId.id" "skinCluster19GroupParts.gi";
connectAttr "groupId108.msg" "tweakSet19.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.iog.og[3]" "tweakSet19.dsm"
		 -na;
connectAttr "tweak19.msg" "tweakSet19.ub[0]";
connectAttr "upperLegLeftShapeOrig.w" "groupParts41.ig";
connectAttr "groupId108.id" "groupParts41.gi";
connectAttr "skinCluster20GroupParts.og" "skinCluster20.ip[0].ig";
connectAttr "skinCluster20GroupId.id" "skinCluster20.ip[0].gi";
connectAttr "hipsJnt.wm" "skinCluster20.ma[0]";
connectAttr "leftUpperLeg.wm" "skinCluster20.ma[1]";
connectAttr "leftLowerLeg.wm" "skinCluster20.ma[2]";
connectAttr "leftFoot.wm" "skinCluster20.ma[3]";
connectAttr "hipsJnt.liw" "skinCluster20.lw[0]";
connectAttr "leftUpperLeg.liw" "skinCluster20.lw[1]";
connectAttr "leftLowerLeg.liw" "skinCluster20.lw[2]";
connectAttr "leftFoot.liw" "skinCluster20.lw[3]";
connectAttr "hipsJnt.obcc" "skinCluster20.ifcl[0]";
connectAttr "leftUpperLeg.obcc" "skinCluster20.ifcl[1]";
connectAttr "leftLowerLeg.obcc" "skinCluster20.ifcl[2]";
connectAttr "leftFoot.obcc" "skinCluster20.ifcl[3]";
connectAttr "bindPose5.msg" "skinCluster20.bp";
connectAttr "groupParts43.og" "tweak20.ip[0].ig";
connectAttr "groupId110.id" "tweak20.ip[0].gi";
connectAttr "skinCluster20GroupId.msg" "skinCluster20Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.iog.og[2]" "skinCluster20Set.dsm"
		 -na;
connectAttr "skinCluster20.msg" "skinCluster20Set.ub[0]";
connectAttr "tweak20.og[0]" "skinCluster20GroupParts.ig";
connectAttr "skinCluster20GroupId.id" "skinCluster20GroupParts.gi";
connectAttr "groupId110.msg" "tweakSet20.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.iog.og[3]" "tweakSet20.dsm"
		 -na;
connectAttr "tweak20.msg" "tweakSet20.ub[0]";
connectAttr "kneeLeftShapeOrig.w" "groupParts43.ig";
connectAttr "groupId110.id" "groupParts43.gi";
connectAttr "skinCluster21GroupParts.og" "skinCluster21.ip[0].ig";
connectAttr "skinCluster21GroupId.id" "skinCluster21.ip[0].gi";
connectAttr "hipsJnt.wm" "skinCluster21.ma[0]";
connectAttr "leftUpperLeg.wm" "skinCluster21.ma[1]";
connectAttr "leftLowerLeg.wm" "skinCluster21.ma[2]";
connectAttr "leftFoot.wm" "skinCluster21.ma[3]";
connectAttr "leftToe.wm" "skinCluster21.ma[4]";
connectAttr "hipsJnt.liw" "skinCluster21.lw[0]";
connectAttr "leftUpperLeg.liw" "skinCluster21.lw[1]";
connectAttr "leftLowerLeg.liw" "skinCluster21.lw[2]";
connectAttr "leftFoot.liw" "skinCluster21.lw[3]";
connectAttr "leftToe.liw" "skinCluster21.lw[4]";
connectAttr "hipsJnt.obcc" "skinCluster21.ifcl[0]";
connectAttr "leftUpperLeg.obcc" "skinCluster21.ifcl[1]";
connectAttr "leftLowerLeg.obcc" "skinCluster21.ifcl[2]";
connectAttr "leftFoot.obcc" "skinCluster21.ifcl[3]";
connectAttr "leftToe.obcc" "skinCluster21.ifcl[4]";
connectAttr "bindPose5.msg" "skinCluster21.bp";
connectAttr "groupParts45.og" "tweak21.ip[0].ig";
connectAttr "groupId112.id" "tweak21.ip[0].gi";
connectAttr "skinCluster21GroupId.msg" "skinCluster21Set.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.iog.og[2]" "skinCluster21Set.dsm"
		 -na;
connectAttr "skinCluster21.msg" "skinCluster21Set.ub[0]";
connectAttr "tweak21.og[0]" "skinCluster21GroupParts.ig";
connectAttr "skinCluster21GroupId.id" "skinCluster21GroupParts.gi";
connectAttr "groupId112.msg" "tweakSet21.gn" -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.iog.og[3]" "tweakSet21.dsm"
		 -na;
connectAttr "tweak21.msg" "tweakSet21.ub[0]";
connectAttr "lowerLegLeftShapeOrig.w" "groupParts45.ig";
connectAttr "groupId112.id" "groupParts45.gi";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|hipsShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|upperLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft|lowerLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft|kneeLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|upperLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|lowerLegRight|lowerLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|kneeRight|kneeRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2|panelLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRight2|panelRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "baseShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontLeftShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontRightShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontLeftShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "jawMidShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethmidShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "leftSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "rightSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "backSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "leftNozzleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "rightNozzleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "frontLeftShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "armRightShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "armRightShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|upperArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight|lowerArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|upperArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft|lowerArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeft2|armLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|upperLegLeft|upperLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|lowerLegLeft|lowerLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|kneeLeft|kneeLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|upperLegRight|upperLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|lowerLegRight|lowerLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|kneeRight|kneeRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelLeft1|panelLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelLeft2|panelLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelRight1|panelRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|panelRight2|panelRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Cubic_Left_Arm|armLeft1|armLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Cubic_Left_Arm|upperArmLeft|upperArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Cubic_Left_Arm|lowerArmLeft|lowerArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Cubic_Left_Arm|armLeft2|armLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "BackPackShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "lowerArmRightGunShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|Gun_Right_Arm|upperArmRight|upperArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Drill_Right_Arm|upperArmRight|upperArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "pPlaneShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "fistRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|upperArmLeft|upperArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "fistLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|lowerArmLeft|lowerArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Drill_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Gun_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|Drill_Right_Arm|lowerArmRight|lowerArmRightShape.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "drillRightShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "ShoulderLeftShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|shoulderRight|shoulderRightShape.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "groupId62.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId63.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId66.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId67.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId70.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId77.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId86.msg" ":initialShadingGroup.gn" -na;
// End of Basic_Mecha.0010.ma
