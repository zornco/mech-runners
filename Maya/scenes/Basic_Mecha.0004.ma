//Maya ASCII 2017ff05 scene
//Name: Basic_Mecha.0004.ma
//Last modified: Sat, Feb 03, 2018 03:07:29 AM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "57F02CE5-4F31-A6EC-5D05-8897BB4CC603";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -48.782310781075402 5.9412432958812955 77.488922899702644 ;
	setAttr ".r" -type "double3" -9.9383523760205357 -390.19999999999987 -9.2000677165232195e-016 ;
	setAttr ".rp" -type "double3" -1.5543122344752192e-014 -7.4829031859735551e-014 
		0 ;
	setAttr ".rpt" -type "double3" 7.1649883269682367e-014 3.4361230584639746e-014 -3.2486321733328545e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "8DA41C8D-4BCD-E8D6-9D20-B899EBCA6DC4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 87.180170724020329;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -5.5870086548659188 -9.105048940286899 3.2719630344411428 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "DEFBBE81-4D59-C1DB-60E7-FC96822C4ACA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BEC4DFEB-4727-263C-1835-5A976D6A6F5E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 25.971735514366536;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "90496FA8-4FA9-6A33-DE77-0AAC0D1B9DAD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "D197ED46-4D10-44A6-2CAA-A896A1A3D1A8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 109.01391833912356;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "EE308D80-4811-9364-4FA8-00B92704BA63";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.2584937751254 -17.685995180364522 2.0615391731264463 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "5518C0C6-48E8-F804-B673-848FBDEA0A7C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.2584937751252;
	setAttr ".ow" 79.644358160618268;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 8.8817841970012523e-016 -17.685995180364522 2.0615391731262243 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "Rootroot";
	rename -uid "36260844-4F53-3ABA-4FAF-13818B63E76D";
	setAttr ".t" -type "double3" 0 -19.000007629394531 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "rootTorso" -p "Rootroot";
	rename -uid "479E03A7-4499-3560-C05C-429ED5C2467F";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "rootHead" -p "Rootroot";
	rename -uid "6A860384-462E-B055-A6AC-20B0C72C74A0";
	setAttr ".t" -type "double3" 0 19.000007629394531 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "rootBack" -p "Rootroot";
	rename -uid "A557B44D-492B-0C6F-C530-2CB7D5EC7B85";
	setAttr ".t" -type "double3" 0 19.000007629394531 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode joint -n "rootArmLeft" -p "Rootroot";
	rename -uid "8A14F95D-43CC-01EA-BBFA-17BDF6BFCF11";
	setAttr ".t" -type "double3" 0 19.000007629394531 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
createNode joint -n "clavacleLeft" -p "rootArmLeft";
	rename -uid "0A945317-4EE3-EB7F-2AB5-03AC8DC39B66";
	setAttr ".t" -type "double3" 12 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 9.4623222080256131 0 ;
	setAttr ".radi" 0.5;
createNode joint -n "shoulderLeft" -p "clavacleLeft";
	rename -uid "E0200340-44B4-E4FC-33E2-6D827B1DC23D";
	setAttr ".t" -type "double3" 6.0827625302982185 2.2204460492503084e-016 -1.2258395704233382e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -84.46232220802564 0 ;
	setAttr ".radi" 0.75862068965517249;
createNode joint -n "upperArmLeft" -p "shoulderLeft";
	rename -uid "22CC745B-4EB7-13A7-11E4-90A2A69841EE";
	setAttr ".t" -type "double3" 6 -1.3322676295501863e-015 7.3955709864469725e-032 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 12.52880770915149 ;
	setAttr ".radi" 0.92514885123928725;
createNode joint -n "elbowLeft" -p "|Rootroot|rootArmLeft|clavacleLeft|shoulderLeft|upperArmLeft";
	rename -uid "C2099CCD-4838-926F-D6B9-91A53F63CD9C";
	setAttr ".t" -type "double3" 9.2195444572928746 2.4086414957743302e-015 -3.4416913763379853e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 50.906141113770467 ;
	setAttr ".radi" 0.91091061603443957;
createNode joint -n "wristLeft" -p "elbowLeft";
	rename -uid "77D0BB3F-4F80-6EA5-945C-FC83CB2AC823";
	setAttr ".t" -type "double3" 8.9442719099991574 -2.6368658817559114e-015 -1.7456710297014655e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -26.565051177078036 89.999999999999957 0 ;
	setAttr ".radi" 0.91091061603443957;
createNode joint -n "rootArmRight" -p "Rootroot";
	rename -uid "49B0CCC9-486E-087B-CC8D-12B7D23365C8";
	setAttr ".t" -type "double3" 0 19.000007629394531 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000014 0 180 ;
createNode joint -n "clavacleRight" -p "rootArmRight";
	rename -uid "25F5D630-4D5A-3AA1-28AD-02BCC6A52E6D";
	setAttr ".t" -type "double3" 12 -3.2631145762725383e-031 -1.4695761589768238e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -9.4623222080256131 0 ;
	setAttr ".radi" 0.5;
createNode joint -n "shoulderRight" -p "clavacleRight";
	rename -uid "7351C74A-4037-5BD7-B967-25A1079A1699";
	setAttr ".t" -type "double3" 6.0827625302982202 2.2204460492503118e-016 2.2012771446046362e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 84.462322208025554 0 ;
	setAttr ".radi" 0.75862068965517249;
createNode joint -n "upperArmRight" -p "shoulderRight";
	rename -uid "767B0B5C-40FC-4E3A-0BBD-CEBF936F3BAB";
	setAttr ".t" -type "double3" 5.9999999999999973 -1.3322676295501857e-015 -7.395570986447013e-032 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 12.52880770915149 ;
	setAttr ".radi" 0.92514885123928725;
createNode joint -n "elbowRight" -p "|Rootroot|rootArmRight|clavacleRight|shoulderRight|upperArmRight";
	rename -uid "8550BA6D-4204-420E-7222-1D882F7D9221";
	setAttr ".t" -type "double3" 9.2195444572928782 1.881954465240176e-016 1.5874924867134391e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 50.906141113770467 ;
	setAttr ".radi" 0.91091061603443957;
createNode joint -n "wristRight" -p "elbowRight";
	rename -uid "83A0B628-42FF-E34E-AE26-4BB8FAB543BB";
	setAttr ".t" -type "double3" 8.9442719099991539 -9.158477970445895e-016 3.9313215599544332e-017 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -26.565051177078008 89.999999999999957 0 ;
	setAttr ".radi" 0.91091061603443957;
createNode joint -n "rootLegsJnt" -p "Rootroot";
	rename -uid "404F0E79-428F-F754-F6EA-0ABC3B751C23";
	setAttr ".t" -type "double3" 0 -2.9999923706054687 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 1.5865543914200564;
createNode joint -n "hipsJnt" -p "rootLegsJnt";
	rename -uid "F222B8CA-46BD-39D1-B6EF-BFB5F8060C79";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 0 2.2204460492503131e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
createNode joint -n "upperLegLeftJnt" -p "hipsJnt";
	rename -uid "292AC345-4BCF-8FC5-27D3-DCBBBE5F9382";
	setAttr ".t" -type "double3" 7.9999999999999982 8.8817841970012523e-016 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000099 -68.198590513648156 -90.000000000000071 ;
createNode joint -n "lowerLegLeftJnt" -p "upperLegLeftJnt";
	rename -uid "0E8B7813-4C3D-3D46-B1FF-44AE78FE8FE1";
	setAttr ".t" -type "double3" 5.385164807134502 -1.1439567460034809e-015 -6.1513674471247417e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 21.801409486352046 ;
createNode joint -n "footLeftJnt" -p "lowerLegLeftJnt";
	rename -uid "0B3D773A-45B5-A820-0FAD-22A43471D445";
	setAttr ".t" -type "double3" 3.9999999999999893 2.2204460492503273e-015 7.1942451995710144e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 68.198590513648014 ;
createNode joint -n "joint1" -p "footLeftJnt";
	rename -uid "E2F77DF6-499F-3965-E3CA-F3A215E21165";
	setAttr ".t" -type "double3" 5.3851648071344913 1.3810678947432673e-016 -7.638334409421077e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 21.801409486351922 -90 0 ;
createNode joint -n "upperLegRightJnt" -p "hipsJnt";
	rename -uid "FDCED3EE-4275-E132-3983-19A04AE62FE4";
	setAttr ".t" -type "double3" -7.9999999999999982 -8.8817841970012523e-016 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 -68.198590513648199 -90 ;
createNode joint -n "lowerLegRightJnt" -p "upperLegRightJnt";
	rename -uid "A0CA8E94-4496-707A-ACDF-8093C37D5E34";
	setAttr ".t" -type "double3" 5.3851648071345046 -2.0752935374143411e-015 -3.3188640504066122e-032 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 21.801409486351847 ;
createNode joint -n "footRightJnt" -p "lowerLegRightJnt";
	rename -uid "A251C16D-4684-41C3-943D-26AE5B13DD4F";
	setAttr ".t" -type "double3" 3.9999999999999787 6.661338147751168e-016 -4.1744385725905886e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 68.198590513648085 ;
createNode joint -n "joint2" -p "footRightJnt";
	rename -uid "6F3D4236-40B6-DEAC-4BF9-6284F0ABB3BB";
	setAttr ".t" -type "double3" 5.3851648071345073 2.1270835834672298e-015 4.9737991503207013e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -21.801409486351837 89.999999999999972 0 ;
createNode transform -n "BasicMechGrouped";
	rename -uid "0177049C-4D00-9488-267F-B69328B58F5D";
	setAttr ".rp" -type "double3" 0 -13 1.0000000000000009 ;
	setAttr ".sp" -type "double3" 0 -13 1.0000000000000009 ;
createNode transform -n "Basic_Torso" -p "BasicMechGrouped";
	rename -uid "38150AC2-4E38-3106-AF69-2AB392F4167C";
	setAttr ".rp" -type "double3" 0 5 -1 ;
	setAttr ".sp" -type "double3" 0 5 -1 ;
createNode transform -n "base" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "5687955E-4776-20EC-B431-EB86A683BF5F";
	setAttr ".rp" -type "double3" 0 -17 -1 ;
	setAttr ".sp" -type "double3" 0 -17 -1 ;
createNode mesh -n "baseShape" -p "base";
	rename -uid "B20F1ADE-4BB5-52FE-2AD2-6FB2C764C8AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  -3 0 0 3 0 0 -3 0 0 3 0 0;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "backSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "D459BBFE-443F-93C5-3C8E-498F2B118BF5";
	setAttr ".rp" -type "double3" 0 -7.5 -11.5 ;
	setAttr ".sp" -type "double3" 0 -7.5 -11.5 ;
createNode mesh -n "backSideShape" -p "backSide";
	rename -uid "8C90774E-404D-A119-5C13-11B5F4956669";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[0:1]" -type "float3"  0 -4 0 0 -4 0;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "FE9902B7-4B05-B77A-9454-A09CCB9ADCF3";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "rightSideShape" -p "rightSide";
	rename -uid "E0DE3407-41EB-E6C2-CEAD-018F87BE1007";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -26 0 0 -26 0 0 -26 0 0 -26 
		0 0 -20 0 0 -20 0 0 -20 0 0 -20 0 0;
	setAttr -s 8 ".vt[0:7]"  13 -15 8 13 -15 -10 13 0 8 13 0 -10 10 0 8
		 10 0 -10 10 -15 8 10 -15 -10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightNozzle" -p "rightSide";
	rename -uid "6E3AAC7B-4D89-5CFF-9166-978B87C5BE70";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "rightNozzleShape" -p "rightNozzle";
	rename -uid "EA8B82A3-45D0-60B5-8430-1C8E0660895D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -22.205099 0 0 -25.999722 
		0 0 -24.102966 0 0 -27.897589 0 0 -24.102966 0 0 -27.897589 0 0 -22.205099 0 0 -25.999722 
		0 0;
	setAttr -s 8 ".vt[0:7]"  11.10254955 -15.94851685 -2.5 12.99986076 -16.58113861 -2.5
		 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5 12.051483154 -13.10255051 -5.5
		 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5 12.99986076 -16.58113861 -5.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "06C37FDC-4EEB-D5E2-F652-A48C6BCF8552";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "leftSideShape" -p "leftSide";
	rename -uid "1290F97F-4AEF-7373-D2B6-EFA594447191";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftNozzle" -p "leftSide";
	rename -uid "9E258720-4205-43CC-E9AA-B6ACF03285C0";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "leftNozzleShape" -p "leftNozzle";
	rename -uid "756ED554-4D39-2719-C8E7-15A011104AA8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "4EF332D3-40DD-D9F6-22A4-E29160E69659";
	setAttr ".rp" -type "double3" 0 -1.5 11.5 ;
	setAttr ".sp" -type "double3" 0 -1.5 11.5 ;
createNode mesh -n "frontSideShape" -p "frontSide";
	rename -uid "240FBBC4-4A64-C047-B4B0-A49AA879F1E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft1" -p "frontSide";
	rename -uid "DA5A97AE-4AB6-D808-737D-48A54CC76B56";
	setAttr ".rp" -type "double3" 0.99999999999999978 0 13 ;
	setAttr ".sp" -type "double3" 0.99999999999999978 0 13 ;
createNode mesh -n "frontLeftShape1" -p "frontLeft1";
	rename -uid "8981AC15-446A-F817-6A7C-2BA9D2BE092E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft2" -p "frontLeft1";
	rename -uid "EC447CBF-4B7A-68CE-5D43-3387389E5D50";
	setAttr ".rp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
createNode mesh -n "frontLeftShape2" -p "frontLeft2";
	rename -uid "C4FBF2AC-4AEF-1CD1-8BE8-D1AA68F5582A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[0]" -type "float3" -0.075828865 0 0 ;
	setAttr ".pt[2]" -type "float3" -0.38835922 8.8817842e-016 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "frontLeft2";
	rename -uid "8F4E599B-471A-D845-EC15-50AA27EF9227";
	setAttr ".rp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "F23315E2-4483-29C2-F6AB-4CA8057855AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "01186FCA-494E-584F-5C95-228E0CF99493";
	setAttr ".rp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8";
	rename -uid "D96C6124-421A-9B22-8816-5CB8F1492E86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "E59C2CDB-44D4-3824-3860-81A55AC3D276";
	setAttr ".rp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11";
	rename -uid "A4C79538-4683-2A6D-69DD-DF9B10E5FD1A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "B5364B96-4B40-02BE-DD0C-38A4F8490A73";
	setAttr ".rp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10";
	rename -uid "49274F99-4C74-D279-E5C0-E5A7D437EE54";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "C5F9ED8E-4ADB-B6FE-0679-38816DDCE302";
	setAttr ".rp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9";
	rename -uid "A6E870DD-4579-55CB-8072-7CB118EF453B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "A2942867-4BB8-B9AF-4995-66AA739642DC";
	setAttr ".rp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube15";
	rename -uid "A54AB347-497D-39BE-BBCC-77960C5B8ED5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  8.7661715 -10.002368 11.40912 
		6.8029666 -10.002368 9.7271099 8.7687492 -10.004949 11.415309 6.8055449 -10.004949 
		9.7332993 8.1979952 -8.6640949 12.045508 6.2347908 -8.6640949 10.3635 8.1954165 -8.6615152 
		12.03932 6.2322121 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "23A77EF9-4783-E752-C980-89A4DCF51C92";
	setAttr ".rp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube14";
	rename -uid "9462CCF8-4905-AC44-3704-0BAC8F8F7885";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.3451328 -9.0245857 12.22831 
		6.8835888 -9.0245857 9.9206047 7.3477111 -9.0271645 12.234498 6.886168 -9.0271654 
		9.9267941 6.7769575 -7.6863122 12.864698 6.3154144 -7.6863122 10.556993 6.7743788 
		-7.6837316 12.85851 6.3128357 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "5764FA3A-4D93-9542-2DBF-BABA02737ED2";
	setAttr ".rp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube13";
	rename -uid "94145998-4709-D2FF-3583-32AEB55EBF4D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.4257555 -8.0468016 12.421804 
		6.9642124 -8.0468016 10.114099 7.4283352 -8.0493822 12.427992 6.9667902 -8.0493822 
		10.120287 6.8575802 -6.708529 13.058191 6.3960366 -6.7085285 10.750487 6.8550024 
		-6.7059488 13.052003 6.3934579 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "5310FC2F-4553-4570-E395-86979156798A";
	setAttr ".rp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12";
	rename -uid "36B47657-41C2-2C16-9608-83A187855E82";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft3" -p "frontLeft1";
	rename -uid "547EA155-44AE-5549-0CE7-2AAF174DF833";
	setAttr ".rp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-016 0 8.8817841970012523e-016 ;
createNode mesh -n "frontLeftShape3" -p "frontLeft3";
	rename -uid "3DD55ED4-46B0-DDAB-C522-869D1C2E88B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.076490089 0 0 -25.998447 
		0 0 -0.38087735 8.8817842e-016 0 -26.615368 0 0 0.76427841 0 0 -25.081852 0 0 1.381197 
		0 0 -24.464935 0 0;
	setAttr -s 8 ".vt[0:7]"  0.076159477 -13.017789841 13.38278198 12.99922371 -13.017789841 7.99813509
		 0.38461828 -3.050000191 14.1230793 13.30768394 -3.050000191 8.73843193 -0.38213921 -2.88960266 12.28287315
		 12.54092598 -2.88960218 6.89822626 -0.69059849 -12.85739136 11.54257584 12.23246765 -12.85739136 6.1579299;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "frontLeft3";
	rename -uid "18C07746-4664-7A3E-40AC-5D91CD75FCEC";
	setAttr ".rp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "6256EB2D-474C-CFDC-4C55-80BEA832B066";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.3908882 0 0 -22.160103 
		0 0 -7.5521345 0 0 -22.321348 0 0 -6.0478439 0 0 -20.817059 0 0 -5.8865986 0 0 -20.655813 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.69544411 -6.36505508 14.26905155 11.080051422 -6.36505508 11.19211197
		 3.77606726 -5.38727188 14.46254539 11.1606741 -5.38727188 11.38560486 3.023921967 -4.96803474 12.65740871
		 10.40852928 -4.96803474 9.58046818 2.94329929 -5.94581842 12.46391582 10.32790661 -5.94581842 9.38697529;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "1668818A-4BB5-549C-6343-30A3FEE4547F";
	setAttr ".rp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube8";
	rename -uid "F7A4CEAE-434C-3AE8-2A92-1BAFBD6E3A42";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -8.9145479 
		0 0 -7.3908882 0 0 -9.2370405 0 0 -5.8865986 0 0 -7.7327509 0 0 -5.564106 0 0 -7.4102583 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 4.45727396 -8.32062149 13.49744701
		 3.69544411 -6.36505508 14.26905155 4.61852026 -6.36505508 13.8844347 2.94329929 -5.94581842 12.46391582
		 3.86637545 -5.94581842 12.079298973 2.78205299 -7.90138435 12.076927185 3.70512915 -7.90138435 11.69231033;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "CC8ACE55-4EB5-FDE7-E18B-689B796CA117";
	setAttr ".rp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube11";
	rename -uid "1385D34A-4516-DBA4-9F76-DAA67AB24F03";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -13.969256 
		0 0 -8.3050871 0 0 -15.205947 0 0 -6.8007975 0 0 -13.701655 0 0 -5.564106 0 0 -12.464966 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 6.9846282 -11.25397968 11.76311302
		 4.15254354 -7.53839588 13.80608845 7.60297346 -10.47175312 11.68713665 3.40039873 -7.11915922 12.00095272064
		 6.85082769 -10.052515984 9.88199997 2.78205299 -7.90138435 12.076927185 6.23248291 -10.83474159 9.95797634;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "34D68EA8-4803-4E79-C904-A684F4BD506D";
	setAttr ".rp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube10";
	rename -uid "396814A8-4D2E-D144-C8D0-2A9FD7C569D4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -21.353872 0 0 -21.51512 
		0 0 -13.969266 0 0 -14.130511 0 0 -12.464975 0 0 -12.626222 0 0 -19.849583 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  10.67693615 -11.2539711 10.2246418 10.75755978 -10.27618885 10.4181366
		 6.98463297 -11.2539711 11.76311302 7.065255642 -10.27618885 11.95660782 6.23248768 -10.83473492 9.95797539
		 6.31311083 -9.85695076 10.15146923 9.92479134 -10.83473492 8.41950512 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "8522D05E-4841-D0FF-366B-F5A8D9869DBB";
	setAttr ".rp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube9";
	rename -uid "D38F2CC7-4397-B5F8-0172-CABCEE855294";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -19.668966 0 0 -21.51512 
		0 0 -20.313951 0 0 -22.160103 0 0 -18.809662 0 0 -20.655813 0 0 -18.164677 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  9.83448315 -10.27618885 10.8027544 10.75755978 -10.27618885 10.4181366
		 10.15697575 -6.36505508 11.57672882 11.080051422 -6.36505508 11.19211197 9.40483093 -5.94581842 9.77159309
		 10.32790661 -5.94581842 9.38697529 9.082338333 -9.85695076 8.99761677 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "19710E1A-469E-9039-EA80-70A346E1E157";
	setAttr ".rp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube15";
	rename -uid "D8143C51-4CEB-F78A-FE7C-67BD22386525";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.7661715 -10.002368 11.40912 
		-12.802967 -10.002368 9.7271099 -2.7687492 -10.004949 11.415309 -12.805545 -10.004949 
		9.7332993 -2.1979952 -8.6640949 12.045508 -12.234791 -8.6640949 10.3635 -2.1954165 
		-8.6615152 12.03932 -12.232212 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "97D92174-410F-884E-8D68-1385374252ED";
	setAttr ".rp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube14";
	rename -uid "6D077C63-4F2E-29B8-D239-BB905E4595F0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.3451328 -9.0245857 12.22831 
		-12.883589 -9.0245857 9.9206047 -1.3477111 -9.0271645 12.234498 -12.886168 -9.0271654 
		9.9267941 -0.77695751 -7.6863122 12.864698 -12.315414 -7.6863122 10.556993 -0.77437878 
		-7.6837316 12.85851 -12.312836 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "35D6485E-4C33-D003-E0E1-919F8F1C20D7";
	setAttr ".rp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube13";
	rename -uid "DDA181E2-4B40-A65D-2F5C-35AEF94596B8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.4257555 -8.0468016 12.421804 
		-12.964212 -8.0468016 10.114099 -1.4283352 -8.0493822 12.427992 -12.96679 -8.0493822 
		10.120287 -0.85758018 -6.708529 13.058191 -12.396036 -6.7085285 10.750487 -0.8550024 
		-6.7059488 13.052003 -12.393457 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "9BB721EF-4A61-C40A-CE46-D291E9E0E748";
	setAttr ".rp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube12";
	rename -uid "2F531F50-4CFC-CA61-6F5E-3486BFB98488";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9.0127592 0 0 -20.08967 
		0 0 -9.0179157 0 0 -20.094828 0 0 -7.8764067 0 0 -18.953318 0 0 -7.8712502 0 0 -18.94816 
		0 0;
	setAttr -s 8 ".vt[0:7]"  4.5063796 -7.074018478 13.61529922 10.044835091 -7.074018478 11.30759335
		 4.50895786 -7.066599846 13.62148666 10.047413826 -7.066599846 11.31378174 3.93820333 -5.72574568 12.2516861
		 9.47665882 -5.72574568 9.94398022 3.93562508 -5.73316574 12.2454977 9.47408009 -5.73316574 9.93779278;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontRight1" -p "frontSide";
	rename -uid "4597C8EE-4951-8DB2-268E-CF8D4274CC89";
	setAttr ".rp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
	setAttr ".sp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
createNode mesh -n "frontRightShape1" -p "frontRight1";
	rename -uid "65BFBE3B-40CA-3AE2-BC95-7FA64C9617C9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -6.4999886 -1.5 6.4999719 
		-7.5 -1.5 11.5 -6.4999886 -1.5 6.4999719 -7.5 -1.5 11.5 -5.3461356 -1.5 6.7307439 
		-6.3461475 -1.5 11.730772 -5.3461356 -1.5 6.7307439 -6.3461475 -1.5 11.730772;
	setAttr -s 8 ".vt[0:7]"  -6.5 -1.5 1.5 6.5 -1.5 1.5 -6.5 1.5 1.5 6.5 1.5 1.5
		 -6.5 1.5 -1.5 6.5 1.5 -1.5 -6.5 -1.5 -1.5 6.5 -1.5 -1.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "jawMid" -p "frontSide";
	rename -uid "89DD58D2-4EBA-FEA5-4620-779AD1CDDDB9";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "jawMidShape" -p "jawMid";
	rename -uid "BBF1F4BB-4B86-E8BB-08F6-36B6004C7BDD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube6" -p "jawMid";
	rename -uid "244AC02B-40FF-1E35-DA16-9E87AD3FC4B0";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "2D86932E-400D-DFAB-892B-19933F24CE01";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  30.304123 -18.700001 12.7 
		6.4461813 -18.700001 12.7 26.457947 -18.700001 12.7 2.6000054 -18.700001 12.7 24.327759 
		-18.700001 12.7 0.46981788 -18.700001 12.7 28.173935 -18.700001 12.7 4.3159933 -18.700001 
		12.7;
	setAttr -s 8 ".vt[0:7]"  -15.15206146 1.20867193 -5.36155796 -3.22309065 -4.54291534 -0.82075399
		 -13.22897339 5.76722908 -4.63955355 -1.30000269 0.01564152 -0.098749459 -12.16387939 5.76210356 -7.4441123
		 -0.23490894 0.01051604 -2.90330839 -14.086967468 1.20354652 -8.16611671 -2.15799665 -4.54804087 -3.62531281;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube5" -p "jawMid";
	rename -uid "5C15DB6A-40CF-A28D-8F27-77A9656A0CDB";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "39E10765-44E7-234D-22B2-F2BA48648DB0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethmid" -p "frontSide";
	rename -uid "298B6792-4E41-C0F5-614B-86BFC82313CE";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11 ;
createNode mesh -n "teethmidShape" -p "teethmid";
	rename -uid "FA1D085F-4E2E-5401-EC68-34BB66F7FDCB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethLeft" -p "teethmid";
	rename -uid "BDFBD9ED-489C-8419-DE9B-FE899DE6FE67";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethLeftShape" -p "teethLeft";
	rename -uid "85E2D2E6-4D90-2E60-D420-6A86E5506DE2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 3 ".pt";
	setAttr ".pt[1]" -type "float3" -1.4901161e-008 2.3436136 0 ;
	setAttr ".pt[3]" -type "float3" -1.4901161e-008 0 0 ;
	setAttr ".pt[7]" -type "float3" 0 2.3436136 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethRight" -p "teethmid";
	rename -uid "0BBCA9C6-4D6B-0074-7A2F-C0A378F8E720";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethRightShape" -p "teethRight";
	rename -uid "2DC87D9D-46ED-BB5C-9CC4-77A69DB4A2E3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1 0 0 -25.992804 2.3436136 
		0 -1 0 0 -25.992804 0 0 -0.44872528 0 0 -25.441528 0 0 -0.44872528 0 0 -25.441528 
		2.3436136 0;
	setAttr -s 8 ".vt[0:7]"  0.5 -19.5 11.5 12.99640179 -19.5 7.91671467
		 0.5 -12.5 11.5 12.99640179 -12.5 7.91671467 0.22436264 -12.5 10.53873825 12.72076416 -12.5 6.95545292
		 0.22436264 -19.5 10.53873825 12.72076416 -19.5 6.95545292;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Legs" -p "BasicMechGrouped";
	rename -uid "0E955BC1-4964-2BAE-1301-8C89317036A7";
	setAttr ".rp" -type "double3" 0 -25.5 -0.26776695251464844 ;
	setAttr ".sp" -type "double3" 0 -25.5 -0.26776695251464844 ;
createNode transform -n "hips" -p "|BasicMechGrouped|Basic_Legs";
	rename -uid "FF221B08-4D2C-B8C4-D3E0-6A9FB22B3148";
	setAttr ".rp" -type "double3" 0 -22 0 ;
	setAttr ".sp" -type "double3" 0 -22 0 ;
createNode mesh -n "hipsShape" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "77109185-401B-E0D8-99D4-9E8E8A364F90";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegLeft" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "42BBF380-45E3-7FA5-B896-15BC05AED86A";
	setAttr ".rp" -type "double3" 8 -22 0 ;
	setAttr ".sp" -type "double3" 8 -22 0 ;
createNode mesh -n "upperLegLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "5EA57348-475E-14C4-5813-288368A89BE5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegLeft" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "228A07EC-48C7-D1B5-C59A-CA93721A23C5";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "lowerLegLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft";
	rename -uid "2F94FD93-495B-848A-C8EC-43804F051E67";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeLeft" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft";
	rename -uid "8C34651A-4401-EFE8-7775-988B897B7665";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "kneeLeftShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft";
	rename -uid "DC8270F5-4118-DFD7-DFA5-80B685231A9C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegRight" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "9B70C2EC-4F64-7511-B019-008BE4895002";
	setAttr ".rp" -type "double3" -8 -22 0 ;
	setAttr ".sp" -type "double3" -8 -22 0 ;
createNode mesh -n "upperLegRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "5F05A66E-43B5-DCB9-ACE4-44B565BA7A15";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10 -22 0 -22 -22 0 -10 -22 
		0 -22 -22 0 -10 -22 0 -22 -22 0 -10 -22 0 -22 -22 0;
	setAttr -s 8 ".vt[0:7]"  5 -5 3 11 -5 3 5 3 3 11 3 3 5 3 -3 11 3 -3
		 5 -5 -3 11 -5 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegRight" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "F8F9085D-4AAD-48B5-138A-9A97137A60CF";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "lowerLegRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|lowerLegRight";
	rename -uid "CFEB7661-434F-3C86-89D4-ABBEC9BFE28F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9 -22 0 -23 -22 0 -9 -22 
		0 -23 -22 0 -9 -22 0 -23 -22 0 -9 -22 0 -23 -22 0;
	setAttr -s 8 ".vt[0:7]"  4.5 -11 3 11.5 -11 3 4.5 -5 3 11.5 -5 3 4.5 -5 -4
		 11.5 -5 -4 4.5 -11 -4 11.5 -11 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeRight" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight";
	rename -uid "8CE62129-4288-F4FE-0934-6FBF0BAFC905";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "kneeRightShape" -p "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|kneeRight";
	rename -uid "D4AE6C18-46FD-A0C5-74F7-55B272B7B015";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8 -22 0 -24 -22 0 -8 -22 
		0 -24 -22 0 -8 -22 0 -24 -22 0 -8 -22 0 -24 -22 0;
	setAttr -s 8 ".vt[0:7]"  4 -8.53553391 -2 12 -8.53553391 -2 4 -5 1.53553391
		 12 -5 1.53553391 4 -1.46446609 -2 12 -1.46446609 -2 4 -5 -5.53553391 12 -5 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft1" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "06472B54-43CE-28A3-B943-D9BDAB2EA2FA";
	setAttr ".rp" -type "double3" 12 -20 -1 ;
	setAttr ".sp" -type "double3" 12 -20 -1 ;
createNode mesh -n "panelLeftShape1" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1";
	rename -uid "DBFD30E4-4851-2760-62DE-C197E02BF610";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft2" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1";
	rename -uid "E32EE436-4527-A180-E176-B59AE16172E1";
	setAttr ".rp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
	setAttr ".sp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
createNode mesh -n "panelLeftShape2" -p "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2";
	rename -uid "8B9BCF4D-47FC-F698-98BB-6DACEA0D097E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight1" -p "|BasicMechGrouped|Basic_Legs|hips";
	rename -uid "F68B51CC-4D47-0E73-FF16-88A6A326CC32";
	setAttr ".rp" -type "double3" -12.000000000000004 -20 -1 ;
	setAttr ".sp" -type "double3" -12.000000000000004 -20 -1 ;
createNode mesh -n "panelRightShape1" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1";
	rename -uid "94705DCE-447C-DE62-7C01-7987E04AA3E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.462697 -20.005127 3.5 
		-17.462698 -20.005127 -6.5 -5.5743756 -20.264633 3.5 -15.574375 -20.264633 -6.5 -6.537303 
		-19.994873 4.5 -16.537302 -19.994873 -5.5 -8.4256248 -19.735367 4.5 -18.425625 -19.735367 
		-5.5;
	setAttr -s 8 ".vt[0:7]"  -5 -3.5 0.5 5 -3.5 0.5 -5 3.5 0.5 5 3.5 0.5
		 -5 3.5 -0.5 5 3.5 -0.5 -5 -3.5 -0.5 5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight2" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1";
	rename -uid "83C52A54-4F1F-904E-B235-1FA276B726FB";
	setAttr ".rp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
createNode mesh -n "panelRightShape2" -p "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRight2";
	rename -uid "3E1D1CC0-4F0D-A0E9-D73E-D4A0EA4779E2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8.8865137 -19.740492 5.1672893 
		-9.7058802 -19.740492 8.4535904 -8 -20 3.5 -8.8193665 -20 6.7863011 -7.5479331 -20.26976 
		3.6497855 -8.3673 -20.26976 6.9360862 -8.4344463 -20.010252 5.3170743 -9.2538128 
		-20.010252 8.6033754;
	setAttr -s 8 ".vt[0:7]"  -3.5 -3.5 0.5 3.5 -3.5 0.5 -3.5 3.5 0.5 3.5 3.5 0.5
		 -3.5 3.5 -0.5 3.5 3.5 -0.5 -3.5 -3.5 -0.5 3.5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Right_Arm" -p "BasicMechGrouped";
	rename -uid "F3B002F6-4980-5CC0-B5B0-FB934E143A8D";
	setAttr ".rp" -type "double3" -12 -7.5 4 ;
	setAttr ".sp" -type "double3" -12 -7.5 4 ;
createNode transform -n "armRight1" -p "|BasicMechGrouped|Basic_Right_Arm";
	rename -uid "BD74FAE6-410F-1FC1-D37E-9DA76F5EBB34";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armRightShape1" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1";
	rename -uid "F9CA20B8-4D87-D5E4-A152-A59C959A712A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmRight" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1";
	rename -uid "55D5A9D1-42CF-366A-9909-E39182F42194";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmRightShape" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight";
	rename -uid "74D767FC-49D7-8FF4-1FA1-1A841375FD7D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRight" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight";
	rename -uid "124826B8-4A44-F82F-FF5C-BF8E08AF2D8B";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmRightShape" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight";
	rename -uid "7F70E54D-4C9C-07D2-9469-0CA2707F78A2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armRight2" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1";
	rename -uid "5FB98D3E-4CD7-A706-BE56-3E8AC5A0CE51";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armRightShape2" -p "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRight2";
	rename -uid "0473C47A-4B4E-11A3-D0CB-0C8D83367D91";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Left_Arm" -p "BasicMechGrouped";
	rename -uid "DB84E1AA-438F-A86B-DDBC-73B4CCC1BA99";
	setAttr ".rp" -type "double3" 18 -7.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" 18 -7.5 4.0000000000000018 ;
createNode transform -n "armLeft1" -p "|BasicMechGrouped|Basic_Left_Arm";
	rename -uid "5368470F-49AB-1828-DF8C-DFAA22B8B83D";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".rpt" -type "double3" 36 0 2.2043642384652358e-015 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armLeftShape1" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "3E95B1B2-4291-169C-A142-8995F4B3B91B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmLeft" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "A85A8278-406C-58A5-0534-F4BB74D2C79C";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmLeftShape" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "DE602BD4-425D-C2F9-FB0D-A283FC4BDE5E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmLeft" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "40E8FABA-44ED-FEEC-C5DD-8B905F34991C";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmLeftShape" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft";
	rename -uid "09F812BB-4C5F-0F18-33FB-4397A9EA1DB5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armLeft2" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1";
	rename -uid "3AC2A0E7-42FD-1EB0-DABF-8C97ADFE498B";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armLeftShape2" -p "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeft2";
	rename -uid "9E99AC44-4840-37B9-5266-8FAC8A536702";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5
		 -13 -2 -5 -23 -5 -5 -13 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "BasicMechGrouped1";
	rename -uid "3976F5F6-4C7B-341A-4D60-AAB8DA883134";
	setAttr ".rp" -type "double3" 0 -13 1.0000000000000009 ;
	setAttr ".sp" -type "double3" 0 -13 1.0000000000000009 ;
createNode transform -n "Basic_Torso" -p "BasicMechGrouped1";
	rename -uid "1CEE4AAC-4FC9-F84D-BE9C-E8A579ACEC8D";
	setAttr ".rp" -type "double3" 0 5 -1 ;
	setAttr ".sp" -type "double3" 0 5 -1 ;
createNode transform -n "BasicMechGrouped1_Basic_Torso_base1" -p "|BasicMechGrouped1|Basic_Torso";
	rename -uid "2209FA7F-40A5-2C7F-0650-14A3774CB749";
	setAttr ".rp" -type "double3" 0 -11.819221496582031 0.73127269744873047 ;
	setAttr ".sp" -type "double3" 0 -11.819221496582031 0.73127269744873047 ;
createNode mesh -n "BasicMechGrouped1_Basic_Torso_base1Shape" -p "BasicMechGrouped1_Basic_Torso_base1";
	rename -uid "4380B87F-463B-D4B4-B353-34B8A6D7E718";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr -av ".iog[0].og[10].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[210]" -type "float3" 0 0.3226589 0 ;
	setAttr ".pt[211]" -type "float3" 0 0.3226589 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "BasicMechGrouped1_Basic_Torso_base1";
	rename -uid "014F91CE-44CD-54E0-A36F-CC932CF00893";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:202]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.875 ;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 10 ".uvst[0].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 10 ".uvst[1].uvsp[0:9]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1;
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr -s 12 ".uvst[2].uvsp[0:11]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.125
		 0 0.125 0.25;
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr -s 12 ".uvst[3].uvsp[0:11]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.125 0
		 0.125 0.25;
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr -s 14 ".uvst[4].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr -s 14 ".uvst[5].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr -s 14 ".uvst[6].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr -s 14 ".uvst[7].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr -s 14 ".uvst[8].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr -s 14 ".uvst[9].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr -s 14 ".uvst[10].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr -s 14 ".uvst[11].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr -s 14 ".uvst[12].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr -s 14 ".uvst[13].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr -s 14 ".uvst[14].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr -s 14 ".uvst[15].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr -s 14 ".uvst[16].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr -s 14 ".uvst[17].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr -s 14 ".uvst[18].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr -s 14 ".uvst[19].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr -s 14 ".uvst[20].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr -s 14 ".uvst[21].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr -s 14 ".uvst[22].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr -s 14 ".uvst[23].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr -s 14 ".uvst[24].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr -s 14 ".uvst[25].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr -s 14 ".uvst[26].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr -s 14 ".uvst[27].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr -s 14 ".uvst[28].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr -s 14 ".uvst[29].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr -s 14 ".uvst[30].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr -s 14 ".uvst[31].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr -s 14 ".uvst[32].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr -s 14 ".uvst[33].uvsp[0:13]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr -s 14 ".uvst[34].uvsp[0:13]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 280 ".vt";
	setAttr ".vt[0:165]"  -10 -19 8 10 -19 8 -9.99999809 -15 8 9.99999809 -15 8
		 -9.99999809 -15 -10 9.99999809 -15 -10 -10 -19 -10 10 -19 -10 -10 -19 -10 10 -19 -10
		 -10 0 -10 10 0 -10 -9 0 -13 9 0 -13 -9 -15 -13 9 -15 -13 -13 -15 8 -13 -15 -10 -13 0 8
		 -13 0 -10 -10 0 8 -10 0 -10 -10 -19 8 -10 -19 -10 13 -15 8 13 -15 -10 13 0 8 13 0 -10
		 10 0 8 10 0 -10 10 -19.000019073486 8 10 -19.000019073486 -10 11.10254955 -15.94851685 -2.5
		 12.99986076 -16.58113861 -2.5 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5
		 12.051483154 -13.10255051 -5.5 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5
		 12.99986076 -16.58113861 -5.5 -1 -3 13 1 -3 13 -1 0 13 1 0 13 -1 0 10 1 0 10 -1 -3 10
		 1 -3 10 1 -3 13 12.99998856 -3 7.99997139 1 0 13 12.99998856 0 7.99997139 -0.1538527 0 10.23077202
		 11.84613609 0 5.23074341 -0.1538527 -3 10.23077202 11.84613609 -3 5.23074341 -12.99998856 -3 7.99997187
		 -1 -3 13 -12.99998856 0 7.99997187 -1 0 13 -11.84613609 0 5.23074389 0.15385246 0 10.23077202
		 -11.84613609 -3 5.23074389 0.15385246 -3 10.23077202 -3.5 -23.63844299 11.91782761
		 3.5 -23.63844299 11.91782761 -3.5 -18.70000076 12.69999981 3.5 -18.70000076 12.69999981
		 -3.5 -18.23069763 9.73693466 3.5 -18.23069763 9.73693466 -3.5 -23.16913986 8.95476246
		 3.5 -23.16913986 8.95476246 -0.5 -19.5 11.5 0.5 -19.5 11.5 -0.5 -12.5 11.5 0.5 -12.5 11.5
		 -0.5 -12.5 10.5 0.5 -12.5 10.5 -0.5 -19.5 10.5 0.5 -19.5 10.5 -11.10254955 -15.94851685 -2.5
		 -12.99986076 -16.58113861 -2.5 -12.051483154 -13.10255051 -2.5 -13.94879436 -13.73517227 -2.5
		 -12.051483154 -13.10255051 -5.5 -13.94879436 -13.73517227 -5.5 -11.10254955 -15.94851685 -5.5
		 -12.99986076 -16.58113861 -5.5 0.00033061206 -13.017789841 13.38278198 12.99922371 -13.017789841 7.99813509
		 -0.0037409365 -3.050000191 14.1230793 13.30768394 -3.050000191 8.73843193 -0.38213921 -2.88960266 12.28287315
		 12.54092598 -2.88960218 6.89822626 -0.69059849 -12.85739136 11.54257584 12.23246765 -12.85739136 6.1579299
		 3.69544411 -6.36505508 14.26905155 11.080051422 -6.36505508 11.19211197 3.77606726 -5.38727188 14.46254539
		 11.1606741 -5.38727188 11.38560486 3.023921967 -4.96803474 12.65740871 10.40852928 -4.96803474 9.58046818
		 2.94329929 -5.94581842 12.46391582 10.32790661 -5.94581842 9.38697529 3.53419828 -8.32062149 13.88206387
		 4.45727396 -8.32062149 13.49744701 3.69544411 -6.36505508 14.26905155 4.61852026 -6.36505508 13.8844347
		 2.94329929 -5.94581842 12.46391582 3.86637545 -5.94581842 12.079298973 2.78205299 -7.90138435 12.076927185
		 3.70512915 -7.90138435 11.69231033 3.53419828 -8.32062149 13.88206387 6.9846282 -11.25397968 11.76311302
		 4.15254354 -7.53839588 13.80608845 7.60297346 -10.47175312 11.68713665 3.40039873 -7.11915922 12.00095272064
		 6.85082769 -10.052515984 9.88199997 2.78205299 -7.90138435 12.076927185 6.23248291 -10.83474159 9.95797634
		 10.67693615 -11.2539711 10.2246418 10.75755978 -10.27618885 10.4181366 6.98463297 -11.2539711 11.76311302
		 7.065255642 -10.27618885 11.95660782 6.23248768 -10.83473492 9.95797539 6.31311083 -9.85695076 10.15146923
		 9.92479134 -10.83473492 8.41950512 10.0054140091 -9.85695076 8.61299896 9.83448315 -10.27618885 10.8027544
		 10.75755978 -10.27618885 10.4181366 10.15697575 -6.36505508 11.57672882 11.080051422 -6.36505508 11.19211197
		 9.40483093 -5.94581842 9.77159309 10.32790661 -5.94581842 9.38697529 9.082338333 -9.85695076 8.99761677
		 10.0054140091 -9.85695076 8.61299896 5.76617146 -10.0073680878 12.40911961 9.80296707 -10.0073680878 10.72710991
		 5.76874924 -9.9999485 12.41530895 9.80554485 -9.9999485 10.73329926 5.19799519 -8.65909481 11.045508385
		 9.2347908 -8.65909481 9.36349964 5.19541645 -8.66651535 11.039319992 9.23221207 -8.66651535 9.3573103
		 4.34513283 -9.029585838 13.22830963 9.88358879 -9.029585838 10.92060471 4.34771109 -9.022164345 13.23449802
		 9.88616753 -9.022165298 10.92679405 3.77695751 -7.68131208 11.86469841 9.31541443 -7.68131208 9.55699253
		 3.77437878 -7.68873167 11.85851002 9.31283569 -7.68873167 9.55080414 4.4257555 -8.051801682 13.42180443
		 9.96421242 -8.051801682 11.11409855 4.42833519 -8.044382095 13.42799187 9.9667902 -8.044382095 11.12028694
		 3.85758018 -6.70352888 12.058191299 9.39603615 -6.7035284 9.75048733 3.8550024 -6.71094894 12.052002907
		 9.39345741 -6.71094894 9.74429798 4.5063796 -7.074018478 13.61529922 10.044835091 -7.074018478 11.30759335
		 4.50895786 -7.066599846 13.62148666 10.047413826 -7.066599846 11.31378174 3.93820333 -5.72574568 12.2516861
		 9.47665882 -5.72574568 9.94398022;
	setAttr ".vt[166:279]" 3.93562508 -5.73316574 12.2454977 9.47408009 -5.73316574 9.93779278
		 -0.00033061206 -13.017789841 13.38278198 -12.99922371 -13.017789841 7.99813509 0.0037409365 -3.050000191 14.1230793
		 -13.30768394 -3.050000191 8.73843193 0.38213921 -2.88960266 12.28287315 -12.54092598 -2.88960218 6.89822626
		 0.69059849 -12.85739136 11.54257584 -12.23246765 -12.85739136 6.1579299 -3.69544411 -6.36505508 14.26905155
		 -11.080051422 -6.36505508 11.19211197 -3.77606726 -5.38727188 14.46254539 -11.1606741 -5.38727188 11.38560486
		 -3.023921967 -4.96803474 12.65740871 -10.40852928 -4.96803474 9.58046818 -2.94329929 -5.94581842 12.46391582
		 -10.32790661 -5.94581842 9.38697529 -3.53419828 -8.32062149 13.88206387 -4.45727396 -8.32062149 13.49744701
		 -3.69544411 -6.36505508 14.26905155 -4.61852026 -6.36505508 13.8844347 -2.94329929 -5.94581842 12.46391582
		 -3.86637545 -5.94581842 12.079298973 -2.78205299 -7.90138435 12.076927185 -3.70512915 -7.90138435 11.69231033
		 -3.53419828 -8.32062149 13.88206387 -6.9846282 -11.25397968 11.76311302 -4.15254354 -7.53839588 13.80608845
		 -7.60297346 -10.47175312 11.68713665 -3.40039873 -7.11915922 12.00095272064 -6.85082769 -10.052515984 9.88199997
		 -2.78205299 -7.90138435 12.076927185 -6.23248291 -10.83474159 9.95797634 -10.67693615 -11.2539711 10.2246418
		 -10.75755978 -10.27618885 10.4181366 -6.98463297 -11.2539711 11.76311302 -7.065255642 -10.27618885 11.95660782
		 -6.23248768 -10.83473492 9.95797539 -6.31311083 -9.85695076 10.15146923 -9.92479134 -10.83473492 8.41950512
		 -10.0054140091 -9.85695076 8.61299896 -9.83448315 -10.27618885 10.8027544 -10.75755978 -10.27618885 10.4181366
		 -10.15697575 -6.36505508 11.57672882 -11.080051422 -6.36505508 11.19211197 -9.40483093 -5.94581842 9.77159309
		 -10.32790661 -5.94581842 9.38697529 -9.082338333 -9.85695076 8.99761677 -10.0054140091 -9.85695076 8.61299896
		 -5.76617146 -10.0073680878 12.40911961 -9.80296707 -10.0073680878 10.72710991 -5.76874924 -9.9999485 12.41530895
		 -9.80554485 -9.9999485 10.73329926 -5.19799519 -8.65909481 11.045508385 -9.2347908 -8.65909481 9.36349964
		 -5.19541645 -8.66651535 11.039319992 -9.23221207 -8.66651535 9.3573103 -4.34513283 -9.029585838 13.22830963
		 -9.88358879 -9.029585838 10.92060471 -4.34771109 -9.022164345 13.23449802 -9.88616753 -9.022165298 10.92679405
		 -3.77695751 -7.68131208 11.86469841 -9.31541443 -7.68131208 9.55699253 -3.77437878 -7.68873167 11.85851002
		 -9.31283569 -7.68873167 9.55080414 -4.4257555 -8.051801682 13.42180443 -9.96421242 -8.051801682 11.11409855
		 -4.42833519 -8.044382095 13.42799187 -9.9667902 -8.044382095 11.12028694 -3.85758018 -6.70352888 12.058191299
		 -9.39603615 -6.7035284 9.75048733 -3.8550024 -6.71094894 12.052002907 -9.39345741 -6.71094894 9.74429798
		 -4.5063796 -7.074018478 13.61529922 -10.044835091 -7.074018478 11.30759335 -4.50895786 -7.066599846 13.62148666
		 -10.047413826 -7.066599846 11.31378174 -3.93820333 -5.72574568 12.2516861 -9.47665882 -5.72574568 9.94398022
		 -3.93562508 -5.73316574 12.2454977 -9.47408009 -5.73316574 9.93779278 15.15206146 -17.49132919 7.33844185
		 3.22309065 -23.24291611 11.87924576 13.22897339 -12.93277168 8.060445786 1.30000269 -18.6843586 12.60125065
		 12.16387939 -12.93789673 5.25588751 0.23490894 -18.68948555 9.79669189 14.086967468 -17.49645424 4.53388309
		 2.15799665 -23.24804115 9.074687004 -15.15206146 -17.49132919 7.33844185 -3.22309065 -23.24291611 11.87924576
		 -13.22897339 -12.93277168 8.060445786 -1.30000269 -18.6843586 12.60125065 -12.16387939 -12.93789673 5.25588751
		 -0.23490894 -18.68948555 9.79669189 -14.086967468 -17.49645424 4.53388309 -2.15799665 -23.24804115 9.074687004
		 0.5 -19.5 11.5 12.99640179 -17.15638733 7.91671467 0.5 -12.5 11.5 12.99640179 -12.5 7.91671467
		 0.22436264 -12.5 10.53873825 12.72076416 -12.5 6.95545292 0.22436264 -19.5 10.53873825
		 12.72076416 -17.15638733 6.95545292 -0.5 -19.5 11.5 -12.99640179 -17.15638733 7.91671467
		 -0.5 -12.5 11.5 -12.99640179 -12.5 7.91671467 -0.22436264 -12.5 10.53873825 -12.72076416 -12.5 6.95545292
		 -0.22436264 -19.5 10.53873825 -12.72076416 -17.15638733 6.95545292;
	setAttr -s 418 ".ed";
	setAttr ".ed[0:165]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0 3 5 0 6 0 0
		 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0 11 13 0 12 14 0 13 15 0
		 14 8 0 15 9 0 16 17 0 18 19 0 20 21 0 22 23 0 16 18 0 17 19 0 18 20 0 19 21 0 20 22 0
		 21 23 0 22 16 0 23 17 0 24 25 0 26 27 0 28 29 0 30 31 0 24 26 0 25 27 0 26 28 0 27 29 0
		 28 30 0 29 31 0 30 24 0 31 25 0 32 33 0 34 35 0 36 37 0 38 39 0 32 34 0 33 35 0 34 36 0
		 35 37 0 36 38 0 37 39 0 38 32 0 39 33 0 40 41 0 42 43 0 44 45 0 46 47 0 40 42 0 41 43 0
		 42 44 0 43 45 0 44 46 0 45 47 0 46 40 0 47 41 0 48 49 0 50 51 0 52 53 0 54 55 0 48 50 0
		 49 51 0 50 52 0 51 53 0 52 54 0 53 55 0 54 48 0 55 49 0 56 57 0 58 59 0 60 61 0 62 63 0
		 56 58 0 57 59 0 58 60 0 59 61 0 60 62 0 61 63 0 62 56 0 63 57 0 64 65 0 66 67 0 68 69 0
		 70 71 0 64 66 0 65 67 0 66 68 0 67 69 0 68 70 0 69 71 0 70 64 0 71 65 0 72 73 0 74 75 0
		 76 77 0 78 79 0 72 74 0 73 75 0 74 76 0 75 77 0 76 78 0 77 79 0 78 72 0 79 73 0 80 81 0
		 82 83 0 84 85 0 86 87 0 80 82 0 81 83 0 82 84 0 83 85 0 84 86 0 85 87 0 86 80 0 87 81 0
		 88 89 0 90 91 0 92 93 0 94 95 0 88 90 0 89 91 0 90 92 0 91 93 0 92 94 0 93 95 0 94 88 0
		 95 89 0 96 97 0 98 99 0 100 101 0 102 103 0 96 98 0 97 99 0 98 100 0 99 101 0 100 102 0
		 101 103 0 102 96 0 103 97 0 104 105 0 106 107 0 108 109 0 110 111 0 104 106 0 105 107 0
		 106 108 0 107 109 0 108 110 0 109 111 0 110 104 0 111 105 0;
	setAttr ".ed[166:331]" 112 113 0 114 115 0 116 117 0 118 119 0 112 114 0 113 115 0
		 114 116 0 115 117 0 116 118 0 117 119 0 118 112 0 119 113 0 120 121 0 122 123 0 124 125 0
		 126 127 0 120 122 0 121 123 0 122 124 0 123 125 0 124 126 0 125 127 0 126 120 0 127 121 0
		 128 129 0 130 131 0 132 133 0 134 135 0 128 130 0 129 131 0 130 132 0 131 133 0 132 134 0
		 133 135 0 134 128 0 135 129 0 136 137 0 138 139 0 140 141 0 142 143 0 136 138 0 137 139 0
		 138 140 0 139 141 0 140 142 0 141 143 0 142 136 0 143 137 0 144 145 0 146 147 0 148 149 0
		 150 151 0 144 146 0 145 147 0 146 148 0 147 149 0 148 150 0 149 151 0 150 144 0 151 145 0
		 152 153 0 154 155 0 156 157 0 158 159 0 152 154 0 153 155 0 154 156 0 155 157 0 156 158 0
		 157 159 0 158 152 0 159 153 0 160 161 0 162 163 0 164 165 0 166 167 0 160 162 0 161 163 0
		 162 164 0 163 165 0 164 166 0 165 167 0 166 160 0 167 161 0 168 169 0 170 171 0 172 173 0
		 174 175 0 168 170 0 169 171 0 170 172 0 171 173 0 172 174 0 173 175 0 174 168 0 175 169 0
		 176 177 0 178 179 0 180 181 0 182 183 0 176 178 0 177 179 0 178 180 0 179 181 0 180 182 0
		 181 183 0 182 176 0 183 177 0 184 185 0 186 187 0 188 189 0 190 191 0 184 186 0 185 187 0
		 186 188 0 187 189 0 188 190 0 189 191 0 190 184 0 191 185 0 192 193 0 194 195 0 196 197 0
		 198 199 0 192 194 0 193 195 0 194 196 0 195 197 0 196 198 0 197 199 0 198 192 0 199 193 0
		 200 201 0 202 203 0 204 205 0 206 207 0 200 202 0 201 203 0 202 204 0 203 205 0 204 206 0
		 205 207 0 206 200 0 207 201 0 208 209 0 210 211 0 212 213 0 214 215 0 208 210 0 209 211 0
		 210 212 0 211 213 0 212 214 0 213 215 0 214 208 0 215 209 0 216 217 0 218 219 0 220 221 0
		 222 223 0 216 218 0 217 219 0 218 220 0 219 221 0 220 222 0 221 223 0;
	setAttr ".ed[332:417]" 222 216 0 223 217 0 224 225 0 226 227 0 228 229 0 230 231 0
		 224 226 0 225 227 0 226 228 0 227 229 0 228 230 0 229 231 0 230 224 0 231 225 0 232 233 0
		 234 235 0 236 237 0 238 239 0 232 234 0 233 235 0 234 236 0 235 237 0 236 238 0 237 239 0
		 238 232 0 239 233 0 240 241 0 242 243 0 244 245 0 246 247 0 240 242 0 241 243 0 242 244 0
		 243 245 0 244 246 0 245 247 0 246 240 0 247 241 0 248 249 0 250 251 0 252 253 0 254 255 0
		 248 250 0 249 251 0 250 252 0 251 253 0 252 254 0 253 255 0 254 248 0 255 249 0 256 257 0
		 258 259 0 260 261 0 262 263 0 256 258 0 257 259 0 258 260 0 259 261 0 260 262 0 261 263 0
		 262 256 0 263 257 0 264 265 0 266 267 0 268 269 0 270 271 0 264 266 0 265 267 0 266 268 0
		 267 269 0 268 270 0 269 271 0 270 264 0 271 265 0 272 273 0 274 275 0 276 277 0 278 279 0
		 272 274 0 273 275 0 274 276 0 275 277 0 276 278 0 277 279 0 278 272 0 279 273 0;
	setAttr -s 203 -ch 812 ".fc[0:202]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 3 9 -1 -9
		mu 0 4 6 7 9 8
		f 4 10 15 -12 -15
		mu 1 4 0 1 2 3
		f 4 11 17 -13 -17
		mu 1 4 3 2 4 5
		f 4 12 19 -14 -19
		mu 1 4 5 4 6 7
		f 4 13 21 -11 -21
		mu 1 4 7 6 8 9
		f 4 26 23 -28 -23
		mu 2 4 0 1 2 3
		f 4 28 24 -30 -24
		mu 2 4 1 4 5 2
		f 4 30 25 -32 -25
		mu 2 4 4 6 7 5
		f 4 32 22 -34 -26
		mu 2 4 6 8 9 7
		f 4 -31 -29 -27 -33
		mu 2 4 10 11 1 0
		f 4 34 39 -36 -39
		mu 3 4 0 1 2 3
		f 4 35 41 -37 -41
		mu 3 4 3 2 4 5
		f 4 36 43 -38 -43
		mu 3 4 5 4 6 7
		f 4 37 45 -35 -45
		mu 3 4 7 6 8 9
		f 4 44 38 40 42
		mu 3 4 10 0 3 11
		f 4 46 51 -48 -51
		mu 4 4 0 1 2 3
		f 4 47 53 -49 -53
		mu 4 4 3 2 4 5
		f 4 48 55 -50 -55
		mu 4 4 5 4 6 7
		f 4 49 57 -47 -57
		mu 4 4 7 6 8 9
		f 4 -58 -56 -54 -52
		mu 4 4 1 10 11 2
		f 4 56 50 52 54
		mu 4 4 12 0 3 13
		f 4 58 63 -60 -63
		mu 5 4 0 1 2 3
		f 4 59 65 -61 -65
		mu 5 4 3 2 4 5
		f 4 60 67 -62 -67
		mu 5 4 5 4 6 7
		f 4 61 69 -59 -69
		mu 5 4 7 6 8 9
		f 4 -70 -68 -66 -64
		mu 5 4 1 10 11 2
		f 4 68 62 64 66
		mu 5 4 12 0 3 13
		f 4 70 75 -72 -75
		mu 6 4 0 1 2 3
		f 4 71 77 -73 -77
		mu 6 4 3 2 4 5
		f 4 72 79 -74 -79
		mu 6 4 5 4 6 7
		f 4 73 81 -71 -81
		mu 6 4 7 6 8 9
		f 4 -82 -80 -78 -76
		mu 6 4 1 10 11 2
		f 4 80 74 76 78
		mu 6 4 12 0 3 13
		f 4 82 87 -84 -87
		mu 7 4 0 1 2 3
		f 4 83 89 -85 -89
		mu 7 4 3 2 4 5
		f 4 84 91 -86 -91
		mu 7 4 5 4 6 7
		f 4 85 93 -83 -93
		mu 7 4 7 6 8 9
		f 4 -94 -92 -90 -88
		mu 7 4 1 10 11 2
		f 4 92 86 88 90
		mu 7 4 12 0 3 13
		f 4 94 99 -96 -99
		mu 8 4 0 1 2 3
		f 4 95 101 -97 -101
		mu 8 4 3 2 4 5
		f 4 96 103 -98 -103
		mu 8 4 5 4 6 7
		f 4 97 105 -95 -105
		mu 8 4 7 6 8 9
		f 4 -106 -104 -102 -100
		mu 8 4 1 10 11 2
		f 4 104 98 100 102
		mu 8 4 12 0 3 13
		f 4 106 111 -108 -111
		mu 9 4 0 1 2 3
		f 4 107 113 -109 -113
		mu 9 4 3 2 4 5
		f 4 108 115 -110 -115
		mu 9 4 5 4 6 7
		f 4 109 117 -107 -117
		mu 9 4 7 6 8 9
		f 4 -118 -116 -114 -112
		mu 9 4 1 10 11 2
		f 4 116 110 112 114
		mu 9 4 12 0 3 13
		f 4 122 119 -124 -119
		mu 10 4 0 1 2 3
		f 4 124 120 -126 -120
		mu 10 4 1 4 5 2
		f 4 126 121 -128 -121
		mu 10 4 4 6 7 5
		f 4 128 118 -130 -122
		mu 10 4 6 8 9 7
		f 4 123 125 127 129
		mu 10 4 3 2 10 11
		f 4 -127 -125 -123 -129
		mu 10 4 12 13 1 0
		f 4 130 135 -132 -135
		mu 11 4 0 1 2 3
		f 4 131 137 -133 -137
		mu 11 4 3 2 4 5
		f 4 132 139 -134 -139
		mu 11 4 5 4 6 7
		f 4 133 141 -131 -141
		mu 11 4 7 6 8 9
		f 4 -142 -140 -138 -136
		mu 11 4 1 10 11 2
		f 4 140 134 136 138
		mu 11 4 12 0 3 13
		f 4 142 147 -144 -147
		mu 12 4 0 1 2 3
		f 4 143 149 -145 -149
		mu 12 4 3 2 4 5
		f 4 144 151 -146 -151
		mu 12 4 5 4 6 7
		f 4 145 153 -143 -153
		mu 12 4 7 6 8 9
		f 4 -154 -152 -150 -148
		mu 12 4 1 10 11 2
		f 4 152 146 148 150
		mu 12 4 12 0 3 13
		f 4 154 159 -156 -159
		mu 13 4 0 1 2 3
		f 4 155 161 -157 -161
		mu 13 4 3 2 4 5
		f 4 156 163 -158 -163
		mu 13 4 5 4 6 7
		f 4 157 165 -155 -165
		mu 13 4 7 6 8 9
		f 4 -166 -164 -162 -160
		mu 13 4 1 10 11 2
		f 4 164 158 160 162
		mu 13 4 12 0 3 13
		f 4 166 171 -168 -171
		mu 14 4 0 1 2 3
		f 4 167 173 -169 -173
		mu 14 4 3 2 4 5
		f 4 168 175 -170 -175
		mu 14 4 5 4 6 7
		f 4 169 177 -167 -177
		mu 14 4 7 6 8 9
		f 4 -178 -176 -174 -172
		mu 14 4 1 10 11 2
		f 4 176 170 172 174
		mu 14 4 12 0 3 13
		f 4 178 183 -180 -183
		mu 15 4 0 1 2 3
		f 4 179 185 -181 -185
		mu 15 4 3 2 4 5
		f 4 180 187 -182 -187
		mu 15 4 5 4 6 7
		f 4 181 189 -179 -189
		mu 15 4 7 6 8 9
		f 4 -190 -188 -186 -184
		mu 15 4 1 10 11 2
		f 4 188 182 184 186
		mu 15 4 12 0 3 13
		f 4 190 195 -192 -195
		mu 16 4 0 1 2 3
		f 4 191 197 -193 -197
		mu 16 4 3 2 4 5
		f 4 192 199 -194 -199
		mu 16 4 5 4 6 7
		f 4 193 201 -191 -201
		mu 16 4 7 6 8 9
		f 4 -202 -200 -198 -196
		mu 16 4 1 10 11 2
		f 4 200 194 196 198
		mu 16 4 12 0 3 13
		f 4 202 207 -204 -207
		mu 17 4 0 1 2 3
		f 4 203 209 -205 -209
		mu 17 4 3 2 4 5
		f 4 204 211 -206 -211
		mu 17 4 5 4 6 7
		f 4 205 213 -203 -213
		mu 17 4 7 6 8 9
		f 4 -214 -212 -210 -208
		mu 17 4 1 10 11 2
		f 4 212 206 208 210
		mu 17 4 12 0 3 13
		f 4 214 219 -216 -219
		mu 18 4 0 1 2 3
		f 4 215 221 -217 -221
		mu 18 4 3 2 4 5
		f 4 216 223 -218 -223
		mu 18 4 5 4 6 7
		f 4 217 225 -215 -225
		mu 18 4 7 6 8 9
		f 4 -226 -224 -222 -220
		mu 18 4 1 10 11 2
		f 4 224 218 220 222
		mu 18 4 12 0 3 13
		f 4 226 231 -228 -231
		mu 19 4 0 1 2 3
		f 4 227 233 -229 -233
		mu 19 4 3 2 4 5
		f 4 228 235 -230 -235
		mu 19 4 5 4 6 7
		f 4 229 237 -227 -237
		mu 19 4 7 6 8 9
		f 4 -238 -236 -234 -232
		mu 19 4 1 10 11 2
		f 4 236 230 232 234
		mu 19 4 12 0 3 13
		f 4 238 243 -240 -243
		mu 20 4 0 1 2 3
		f 4 239 245 -241 -245
		mu 20 4 3 2 4 5
		f 4 240 247 -242 -247
		mu 20 4 5 4 6 7
		f 4 241 249 -239 -249
		mu 20 4 7 6 8 9
		f 4 -250 -248 -246 -244
		mu 20 4 1 10 11 2
		f 4 248 242 244 246
		mu 20 4 12 0 3 13
		f 4 254 251 -256 -251
		mu 21 4 0 1 2 3
		f 4 256 252 -258 -252
		mu 21 4 1 4 5 2
		f 4 258 253 -260 -253
		mu 21 4 4 6 7 5
		f 4 260 250 -262 -254
		mu 21 4 6 8 9 7
		f 4 255 257 259 261
		mu 21 4 3 2 10 11
		f 4 -259 -257 -255 -261
		mu 21 4 12 13 1 0
		f 4 266 263 -268 -263
		mu 22 4 0 1 2 3
		f 4 268 264 -270 -264
		mu 22 4 1 4 5 2
		f 4 270 265 -272 -265
		mu 22 4 4 6 7 5
		f 4 272 262 -274 -266
		mu 22 4 6 8 9 7
		f 4 267 269 271 273
		mu 22 4 3 2 10 11
		f 4 -271 -269 -267 -273
		mu 22 4 12 13 1 0
		f 4 278 275 -280 -275
		mu 23 4 0 1 2 3
		f 4 280 276 -282 -276
		mu 23 4 1 4 5 2
		f 4 282 277 -284 -277
		mu 23 4 4 6 7 5
		f 4 284 274 -286 -278
		mu 23 4 6 8 9 7
		f 4 279 281 283 285
		mu 23 4 3 2 10 11
		f 4 -283 -281 -279 -285
		mu 23 4 12 13 1 0
		f 4 290 287 -292 -287
		mu 24 4 0 1 2 3
		f 4 292 288 -294 -288
		mu 24 4 1 4 5 2
		f 4 294 289 -296 -289
		mu 24 4 4 6 7 5
		f 4 296 286 -298 -290
		mu 24 4 6 8 9 7
		f 4 291 293 295 297
		mu 24 4 3 2 10 11
		f 4 -295 -293 -291 -297
		mu 24 4 12 13 1 0
		f 4 302 299 -304 -299
		mu 25 4 0 1 2 3
		f 4 304 300 -306 -300
		mu 25 4 1 4 5 2
		f 4 306 301 -308 -301
		mu 25 4 4 6 7 5
		f 4 308 298 -310 -302
		mu 25 4 6 8 9 7
		f 4 303 305 307 309
		mu 25 4 3 2 10 11
		f 4 -307 -305 -303 -309
		mu 25 4 12 13 1 0
		f 4 314 311 -316 -311
		mu 26 4 0 1 2 3
		f 4 316 312 -318 -312
		mu 26 4 1 4 5 2
		f 4 318 313 -320 -313
		mu 26 4 4 6 7 5
		f 4 320 310 -322 -314
		mu 26 4 6 8 9 7
		f 4 315 317 319 321
		mu 26 4 3 2 10 11
		f 4 -319 -317 -315 -321
		mu 26 4 12 13 1 0
		f 4 326 323 -328 -323
		mu 27 4 0 1 2 3
		f 4 328 324 -330 -324
		mu 27 4 1 4 5 2
		f 4 330 325 -332 -325
		mu 27 4 4 6 7 5
		f 4 332 322 -334 -326
		mu 27 4 6 8 9 7
		f 4 327 329 331 333
		mu 27 4 3 2 10 11
		f 4 -331 -329 -327 -333
		mu 27 4 12 13 1 0
		f 4 338 335 -340 -335
		mu 28 4 0 1 2 3
		f 4 340 336 -342 -336
		mu 28 4 1 4 5 2
		f 4 342 337 -344 -337
		mu 28 4 4 6 7 5
		f 4 344 334 -346 -338
		mu 28 4 6 8 9 7
		f 4 339 341 343 345
		mu 28 4 3 2 10 11
		f 4 -343 -341 -339 -345
		mu 28 4 12 13 1 0
		f 4 350 347 -352 -347
		mu 29 4 0 1 2 3
		f 4 352 348 -354 -348
		mu 29 4 1 4 5 2
		f 4 354 349 -356 -349
		mu 29 4 4 6 7 5
		f 4 356 346 -358 -350
		mu 29 4 6 8 9 7
		f 4 351 353 355 357
		mu 29 4 3 2 10 11
		f 4 -355 -353 -351 -357
		mu 29 4 12 13 1 0
		f 4 362 359 -364 -359
		mu 30 4 0 1 2 3
		f 4 364 360 -366 -360
		mu 30 4 1 4 5 2
		f 4 366 361 -368 -361
		mu 30 4 4 6 7 5
		f 4 368 358 -370 -362
		mu 30 4 6 8 9 7
		f 4 363 365 367 369
		mu 30 4 3 2 10 11
		f 4 -367 -365 -363 -369
		mu 30 4 12 13 1 0
		f 4 374 371 -376 -371
		mu 31 4 0 1 2 3
		f 4 376 372 -378 -372
		mu 31 4 1 4 5 2
		f 4 378 373 -380 -373
		mu 31 4 4 6 7 5
		f 4 380 370 -382 -374
		mu 31 4 6 8 9 7
		f 4 375 377 379 381
		mu 31 4 3 2 10 11
		f 4 -379 -377 -375 -381
		mu 31 4 12 13 1 0
		f 4 382 387 -384 -387
		mu 32 4 0 1 2 3
		f 4 383 389 -385 -389
		mu 32 4 3 2 4 5
		f 4 384 391 -386 -391
		mu 32 4 5 4 6 7
		f 4 385 393 -383 -393
		mu 32 4 7 6 8 9
		f 4 -394 -392 -390 -388
		mu 32 4 1 10 11 2
		f 4 392 386 388 390
		mu 32 4 12 0 3 13
		f 4 394 399 -396 -399
		mu 33 4 0 1 2 3
		f 4 395 401 -397 -401
		mu 33 4 3 2 4 5
		f 4 396 403 -398 -403
		mu 33 4 5 4 6 7
		f 4 397 405 -395 -405
		mu 33 4 7 6 8 9
		f 4 -406 -404 -402 -400
		mu 33 4 1 10 11 2
		f 4 404 398 400 402
		mu 33 4 12 0 3 13
		f 4 410 407 -412 -407
		mu 34 4 0 1 2 3
		f 4 412 408 -414 -408
		mu 34 4 1 4 5 2
		f 4 414 409 -416 -409
		mu 34 4 4 6 7 5
		f 4 416 406 -418 -410
		mu 34 4 6 8 9 7
		f 4 411 413 415 417
		mu 34 4 3 2 10 11
		f 4 -415 -413 -411 -417
		mu 34 4 12 13 1 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 35 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[2]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[3]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[4]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[5]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[6]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[7]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[8]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[9]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[10]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[11]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[12]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[13]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[14]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[15]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[16]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[17]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[18]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[19]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[20]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[21]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[22]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[23]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[24]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[25]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[26]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[27]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[28]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[29]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[30]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[31]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[32]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[33]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[34]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Legs" -p "BasicMechGrouped1";
	rename -uid "49793D68-434D-3303-859B-0A82D42068D6";
	setAttr ".rp" -type "double3" 0 -25.5 -0.26776695251464844 ;
	setAttr ".sp" -type "double3" 0 -25.5 -0.26776695251464844 ;
createNode transform -n "hips" -p "|BasicMechGrouped1|Basic_Legs";
	rename -uid "F2146221-48BC-34A1-6418-42A6017C8FA0";
	setAttr ".rp" -type "double3" 0 -22 0 ;
	setAttr ".sp" -type "double3" 0 -22 0 ;
createNode mesh -n "hipsShape" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "FB2CC886-4418-C822-AD21-A9B3C49A12E0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -5 -26 5 5 -26 5 -5 -18 5 5 -18 5 -5 -18 -5
		 5 -18 -5 -5 -26 -5 5 -26 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegLeft" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "C7BF1E48-4F68-9874-67E9-07A599B2027E";
	setAttr ".rp" -type "double3" 8 -22 0 ;
	setAttr ".sp" -type "double3" 8 -22 0 ;
createNode mesh -n "upperLegLeftShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft";
	rename -uid "674BACB9-4BDB-E751-94F5-E687FEFB98C1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  5 -27 3 11 -27 3 5 -19 3 11 -19 3 5 -19 -3
		 11 -19 -3 5 -27 -3 11 -27 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegLeft" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft";
	rename -uid "E8FCBD04-4411-1EB3-B1B5-C7B00E0FB20C";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "lowerLegLeftShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|lowerLegLeft";
	rename -uid "9BF0B797-429E-4920-E93F-A9AA77E27EAF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  4.5 -33 3 11.5 -33 3 4.5 -27 3 11.5 -27 3
		 4.5 -27 -4 11.5 -27 -4 4.5 -33 -4 11.5 -33 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeLeft" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft";
	rename -uid "C8D2C976-4FCD-C2DE-F529-03A59814EA27";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "kneeLeftShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|kneeLeft";
	rename -uid "4FB9781C-41FC-B052-1E4F-5EA921AB7B23";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  4 -30.53553391 -2 12 -30.53553391 -2 4 -27 1.53553391
		 12 -27 1.53553391 4 -23.46446609 -2 12 -23.46446609 -2 4 -27 -5.53553391 12 -27 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegRight" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "B1375159-4271-3413-F6CA-32B63234581A";
	setAttr ".rp" -type "double3" -8 -22 0 ;
	setAttr ".sp" -type "double3" -8 -22 0 ;
createNode mesh -n "upperLegRightShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight";
	rename -uid "5261C5C8-470E-9384-6439-DCBB759F671C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10 -22 0 -22 -22 0 -10 -22 
		0 -22 -22 0 -10 -22 0 -22 -22 0 -10 -22 0 -22 -22 0;
	setAttr -s 8 ".vt[0:7]"  5 -5 3 11 -5 3 5 3 3 11 3 3 5 3 -3 11 3 -3
		 5 -5 -3 11 -5 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegRight" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight";
	rename -uid "200CB4C4-4A61-2209-3E7C-2D8D6AF20A0F";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "lowerLegRightShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|lowerLegRight";
	rename -uid "36AD71C2-41C9-C5B7-2FE8-CA8157B7CF6A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9 -22 0 -23 -22 0 -9 -22 
		0 -23 -22 0 -9 -22 0 -23 -22 0 -9 -22 0 -23 -22 0;
	setAttr -s 8 ".vt[0:7]"  4.5 -11 3 11.5 -11 3 4.5 -5 3 11.5 -5 3 4.5 -5 -4
		 11.5 -5 -4 4.5 -11 -4 11.5 -11 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeRight" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight";
	rename -uid "F539CEA7-4251-9CFB-449D-B0B027D37CD8";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "kneeRightShape" -p "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|kneeRight";
	rename -uid "A7A5AFD3-49B6-1F46-B745-8FB22975AE62";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8 -22 0 -24 -22 0 -8 -22 
		0 -24 -22 0 -8 -22 0 -24 -22 0 -8 -22 0 -24 -22 0;
	setAttr -s 8 ".vt[0:7]"  4 -8.53553391 -2 12 -8.53553391 -2 4 -5 1.53553391
		 12 -5 1.53553391 4 -1.46446609 -2 12 -1.46446609 -2 4 -5 -5.53553391 12 -5 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft1" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "57727B67-4FF9-F9C0-5661-34AC29CC31E5";
	setAttr ".rp" -type "double3" 12 -20 -1 ;
	setAttr ".sp" -type "double3" 12 -20 -1 ;
createNode mesh -n "panelLeftShape1" -p "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1";
	rename -uid "FC7A0B73-44EA-DE61-0273-9F9CA4F13933";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  12.46269703 -23.50512695 -6 12.46269703 -23.50512695 4
		 10.57437515 -16.76463318 -6 10.57437515 -16.76463318 4 11.53730297 -16.49487305 -6
		 11.53730297 -16.49487305 4 13.42562485 -23.23536682 -6 13.42562485 -23.23536682 4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft2" -p "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1";
	rename -uid "AE85FF9F-4B11-5F63-BE9D-F299F8F019EF";
	setAttr ".rp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
	setAttr ".sp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
createNode mesh -n "panelLeftShape2" -p "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1|panelLeft2";
	rename -uid "ABA9A7F9-4F06-4742-60F1-CCA5B512945A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  6.20588017 -23.24049377 8.95359039 12.38651276 -23.24049377 5.66728878
		 5.31936646 -16.5 7.28630066 11.5 -16.5 4 4.86730003 -16.76976013 6.43608665 11.047932625 -16.76976013 3.14978504
		 5.75381279 -23.51025391 8.10337543 11.93444633 -23.51025391 4.8170743;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight1" -p "|BasicMechGrouped1|Basic_Legs|hips";
	rename -uid "F533770A-4025-13D4-5B03-78BAA7EA95F4";
	setAttr ".rp" -type "double3" -12.000000000000004 -20 -1 ;
	setAttr ".sp" -type "double3" -12.000000000000004 -20 -1 ;
createNode mesh -n "panelRightShape1" -p "|BasicMechGrouped1|Basic_Legs|hips|panelRight1";
	rename -uid "A330C60F-4406-14F5-23E7-379004AA358F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.462697 -20.005127 3.5 
		-17.462698 -20.005127 -6.5 -5.5743756 -20.264633 3.5 -15.574375 -20.264633 -6.5 -6.537303 
		-19.994873 4.5 -16.537302 -19.994873 -5.5 -8.4256248 -19.735367 4.5 -18.425625 -19.735367 
		-5.5;
	setAttr -s 8 ".vt[0:7]"  -5 -3.5 0.5 5 -3.5 0.5 -5 3.5 0.5 5 3.5 0.5
		 -5 3.5 -0.5 5 3.5 -0.5 -5 -3.5 -0.5 5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight2" -p "|BasicMechGrouped1|Basic_Legs|hips|panelRight1";
	rename -uid "335E1BDB-4BDD-37B8-D84F-DEA691203784";
	setAttr ".rp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
createNode mesh -n "panelRightShape2" -p "|BasicMechGrouped1|Basic_Legs|hips|panelRight1|panelRight2";
	rename -uid "35D0D6F9-4474-B10C-BCE5-83A5BC816EB1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8.8865137 -19.740492 5.1672893 
		-9.7058802 -19.740492 8.4535904 -8 -20 3.5 -8.8193665 -20 6.7863011 -7.5479331 -20.26976 
		3.6497855 -8.3673 -20.26976 6.9360862 -8.4344463 -20.010252 5.3170743 -9.2538128 
		-20.010252 8.6033754;
	setAttr -s 8 ".vt[0:7]"  -3.5 -3.5 0.5 3.5 -3.5 0.5 -3.5 3.5 0.5 3.5 3.5 0.5
		 -3.5 3.5 -0.5 3.5 3.5 -0.5 -3.5 -3.5 -0.5 3.5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Right_Arm" -p "BasicMechGrouped1";
	rename -uid "95CA430A-48C9-4855-B955-3293A8CB5802";
	setAttr ".r" -type "double3" 0 0 -14.999999999999998 ;
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode transform -n "armRight1" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "08ABD1DE-4F7E-9280-708B-35A7D88D0355";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armRightShape1" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight1";
	rename -uid "2206BA41-4B14-DA7D-2608-139BE80ECA8D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmRight" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "B583A5A7-4C28-D4A8-2F97-1DA3210CE6AB";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmRightShape" -p "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight";
	rename -uid "E7A06FC2-4E9A-DFB8-0656-369F10E688DC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRight" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "D0892255-41ED-B671-EF05-F1BFB8E1BDB9";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmRightShape" -p "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight";
	rename -uid "B653893E-4783-7D83-53D3-158BBEB4512F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armRight2" -p "|BasicMechGrouped1|Basic_Right_Arm";
	rename -uid "AFC980FE-4F11-5357-67DC-5D9A73FFD9A7";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armRightShape2" -p "|BasicMechGrouped1|Basic_Right_Arm|armRight2";
	rename -uid "538766D8-4460-752B-3E9E-BA8A0A1907F7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5
		 -13 -2 -5 -23 -5 -5 -13 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Left_Arm" -p "BasicMechGrouped1";
	rename -uid "92041E3B-4226-31AA-9733-E49D916D956C";
	setAttr ".r" -type "double3" 0 0 14.999999999999998 ;
	setAttr ".rp" -type "double3" 18 1 0 ;
	setAttr ".sp" -type "double3" 18 1 0 ;
createNode transform -n "armLeft1" -p "|BasicMechGrouped1|Basic_Left_Arm";
	rename -uid "5B9DCD14-44F5-1C38-B310-848478A8BC51";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".rpt" -type "double3" 36 0 2.2043642384652358e-015 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armLeftShape1" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1";
	rename -uid "81D8B622-4F8B-CA6E-83C7-A8B8D3C6292C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmLeft" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1";
	rename -uid "C6D45555-4ECD-AE23-5DFF-57999D3DE5C9";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmLeftShape" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "71DFA954-4703-85CD-B8BB-4197C1882155";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmLeft" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft";
	rename -uid "04357830-4F2C-6711-B217-C79CEBAE082A";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmLeftShape" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft";
	rename -uid "852181E5-44AA-1FAB-EC0F-EAA6DC33CFFB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armLeft2" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1";
	rename -uid "AF3BF46A-42C5-3C23-11AA-FFA31517B656";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armLeftShape2" -p "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|armLeft2";
	rename -uid "AC56E6E0-4F2C-247D-F832-5ABE3754A6C1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5
		 -13 -2 -5 -23 -5 -5 -13 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "3DF111BC-4842-49BB-5A7C-A98CE9D326F4";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "CD59C0B5-46F3-68C5-894D-B38B5682A31D";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "C693912C-4F29-95BE-5FEC-66B4EDFE3A33";
createNode displayLayerManager -n "layerManager";
	rename -uid "1E464E77-4D3B-BE87-9CEC-3790444C74A4";
	setAttr -s 3 ".dli";
	setAttr ".dli[3]" 1;
	setAttr ".dli[4]" 2;
createNode displayLayer -n "defaultLayer";
	rename -uid "4085AE31-4CEA-0975-1FB7-E181F8E20DAE";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "BBD0B1A7-4241-97CA-A1C6-D29D636F4BDC";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "1D0F1E37-4A8A-A123-D6A9-679337D5783E";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "5CB01404-49E4-D27F-80CC-5590DD35CDFF";
	setAttr ".w" 10;
	setAttr ".h" 8;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube2";
	rename -uid "80215462-4720-5811-BDBF-3CABF39A8D0D";
	setAttr ".w" 6;
	setAttr ".h" 8;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube3";
	rename -uid "485774BA-41D5-6B47-1A0B-2D9BCAEA4729";
	setAttr ".w" 7;
	setAttr ".h" 6;
	setAttr ".d" 7;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "54C76ABA-4E87-6A38-5E51-09801C8F20AE";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1 0 1;
createNode polyCube -n "polyCube4";
	rename -uid "8D8EA23B-4BD4-C7A9-6418-8BB72A958F8A";
	setAttr ".w" 8;
	setAttr ".h" 5;
	setAttr ".d" 5;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry2";
	rename -uid "2667C77D-4AE1-F49D-EB9A-B89717893460";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 0 0 1;
createNode transformGeometry -n "transformGeometry3";
	rename -uid "CBBB4418-47D3-B1D5-C7F1-B88061D1E0DC";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 -8 -0.5 1;
createNode transformGeometry -n "transformGeometry4";
	rename -uid "1581101F-4397-622B-69AB-43A374D0BF73";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.70710678118654746 0.70710678118654768 0
		 0 -0.70710678118654768 0.70710678118654746 0 8 -5 -2 1;
createNode polyCube -n "polyCube5";
	rename -uid "254C7E88-43BD-5F96-FE35-E5BABE340230";
	setAttr ".w" 10;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube6";
	rename -uid "F9C7DB55-488F-B8D7-CF89-0D81FA44309D";
	setAttr ".w" 7;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "3E1FE795-4A57-0096-2DC6-70B3AE2BE52A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 555\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 555\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 555\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 916\n            -height 710\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n"
		+ "            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n"
		+ "                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n"
		+ "\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 916\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 916\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D359CC2C-4330-3F25-4DC2-51AEBFAC6971";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube7";
	rename -uid "559D9D51-47BD-434F-417F-9FAC6505AF5B";
	setAttr ".w" 20;
	setAttr ".h" 4;
	setAttr ".d" 18;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry5";
	rename -uid "E9E077C0-4E29-FA31-7748-D19AC1391C4C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry6";
	rename -uid "EC9EFFAE-46F3-4480-4BB4-D68D307AACDF";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry7";
	rename -uid "E2F4CE49-4274-17CB-7130-C7AE84E9341C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry8";
	rename -uid "6631E2D1-4860-AEF4-DB1C-7098D054CA4A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry9";
	rename -uid "1163723F-4DD1-B842-E152-5CAC6D624D8C";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 -2.7755575615628914e-017 1 0
		 -0.26976023601264199 0.96292752326766717 5.5511151231257827e-017 0 -0.96292752326766728 -0.26976023601264193 1.1102230246251565e-016 0
		 12 -20 0 1;
createNode transformGeometry -n "transformGeometry10";
	rename -uid "36814056-4C2E-DE67-205C-B98EC7CE1DB7";
	setAttr ".txf" -type "matrix" 0.88294759285892688 -1.3877787807814457e-016 -0.46947156278589086 0
		 -0.1266447595783457 0.96292752326766717 -0.23818415103641816 0 0.45206708919801941 0.26976023601264182 0.85021453876679487 0
		 8.6269065389189556 -20.005126449443157 7.0516877289946809 1;
createNode polyCube -n "polyCube8";
	rename -uid "6738B210-4080-16FE-2F51-0AA44FB0D2B0";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube9";
	rename -uid "C40BE38B-4975-7B08-D713-59897C1E67F0";
	setAttr ".w" 13;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube10";
	rename -uid "398A8CF3-423D-51BB-DFD7-16A4761E016E";
	setAttr ".w" 14;
	setAttr ".h" 10;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak1";
	rename -uid "C313D164-4FA5-DC5A-DFFE-60BC6EF213E9";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  7 -5 -1 7 -5 -1 7 -5 -1 7
		 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1;
createNode transformGeometry -n "transformGeometry11";
	rename -uid "1431F1F0-4C93-7C38-38B9-92BD65356955";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 0.080198924328858931 0
		 0 -0.080198924328858931 0.99677887845624713 0 -1 -3.0499999999999949 0.79999999999998117 1;
createNode transformGeometry -n "transformGeometry12";
	rename -uid "60349C85-4613-42DE-5B69-56B668AF13BA";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018336 0 0 1 0 0
		 0.38461756040018336 0 0.92307601649691418 0 0.99999999999999778 0 13 1;
createNode polyCube -n "polyCube11";
	rename -uid "ED30530E-4D51-D1A7-9F47-92AD1D4F4E58";
	setAttr ".w" 7;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube12";
	rename -uid "9A96DE03-43F3-3B18-9C72-418ED6F3B382";
	setAttr ".w" 14;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak2";
	rename -uid "33334318-4531-A1A4-0802-7CACC4E28DA7";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093;
createNode transformGeometry -n "transformGeometry13";
	rename -uid "B0B2AE7A-4047-F186-D1F6-F599466439B3";
	setAttr ".txf" -type "matrix" 0.85206933223181147 -0.41082766406220717 0.32434315702851696 0
		 0.38461756040018319 0.91171141897700703 0.14440090283216203 0 -0.35503124552896226 0.0017084929389307166 0.93485287385236782 0
		 -1.3 0.015643446504023089 -0.098768834059513783 1;
createNode polyCube -n "polyCube13";
	rename -uid "9563A64B-43D0-45BF-39B9-E6A5C9C06430";
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube14";
	rename -uid "EF19D4A1-4F7E-B8CD-8282-388E4678AAE7";
	setAttr ".w" 13;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak3";
	rename -uid "6D576173-45BA-8565-1798-84B6B017B978";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -3.5 -0.5 6.5 -3.5 -0.5
		 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5;
createNode transformGeometry -n "transformGeometry14";
	rename -uid "73F384E1-440B-A631-EE57-2D97854B1320";
	setAttr ".txf" -type "matrix" 0.96126169593831889 0 -0.27563735581699905 0 0 1 0 0
		 0.27563735581699905 0 0.96126169593831889 0 0.5 -12.499999999999998 11.5 1;
createNode polyCube -n "polyCube15";
	rename -uid "1888269C-410C-44B7-4915-4ABB8EDA545D";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry15";
	rename -uid "87CD74CE-4C81-39F2-7622-37ABB979F889";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 0 -1 0 0 1 0 0 1 0 2.2204460492503131e-016 0
		 11.499999999999979 -7.5 -0.99999999999999012 1;
createNode polyCube -n "polyCube16";
	rename -uid "6EE74D4C-4E8E-AA70-C5C7-5C8BC768DB57";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube17";
	rename -uid "8878502D-46DA-DDA8-1039-B7A7CD249DD4";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry16";
	rename -uid "4C0842CC-4A13-91C5-95FB-49AA6B865820";
	setAttr ".txf" -type "matrix" 0.94865541582805746 -0.31631140039539413 0 0 0.31631140039539413 0.94865541582805746 0 0
		 0 0 1 0 12.525672292085968 -14.841844299802286 -4 1;
createNode polyCube -n "polyCube18";
	rename -uid "EECFD6F9-4BD1-ACB9-F770-6C938581FF5A";
	setAttr ".w" 8;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube19";
	rename -uid "832ECCF9-41FE-590A-C000-199E07CA5835";
	setAttr ".h" 2;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube20";
	rename -uid "E381CF92-4DBB-5E80-EEE7-75B9B372EC9E";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube21";
	rename -uid "F962A912-4E24-4930-03AF-A68648612EE2";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube22";
	rename -uid "B892D7B7-4364-18F0-F668-B680F093092C";
	setAttr ".w" 5;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube23";
	rename -uid "9CFE9CB5-48DE-C5EF-AAC1-0C8AE95CE1EB";
	setAttr ".w" 6;
	setAttr ".h" 0.01;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry17";
	rename -uid "BB4F6B63-4859-0B98-652A-678E093BA62F";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4 -0.5 -1 1;
createNode transformGeometry -n "transformGeometry18";
	rename -uid "0EB95C3C-49D2-7B4E-B5B5-C28661B0243D";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 -2 -1 1;
createNode transformGeometry -n "transformGeometry19";
	rename -uid "5746B87C-4BF9-75CB-795B-EAB11D3E005E";
	setAttr ".txf" -type "matrix" 0.79999892814850848 -0.60000142913266252 0 0 0.60000142913266252 0.79999892814850848 0 0
		 0 0 1 0 2.2999980349376026 -4.1000041087574015 -1 1;
createNode transformGeometry -n "transformGeometry20";
	rename -uid "D1E40504-4CF2-BF08-21AB-188785E56B1B";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 1 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 1 0 5.999999999999992 -5.4999999999999991 -1 1;
createNode transformGeometry -n "transformGeometry21";
	rename -uid "BD7A6EFF-4F93-AF13-B36D-CE840CFC740E";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 7.5 -3 -1 1;
createNode transformGeometry -n "transformGeometry22";
	rename -uid "606417B4-49C7-E882-F47A-6ABF96B731AD";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.86602540378443871 0.49999999999999994 0
		 0 -0.49999999999999994 0.86602540378443871 0 4 -1.25 -1 1;
createNode transformGeometry -n "transformGeometry23";
	rename -uid "29400EF9-4901-32F6-5A09-CF8C6B014F7D";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -17 -1 1;
createNode polyTweak -n "polyTweak4";
	rename -uid "29CDD8A8-4D14-BEF2-2931-BCA13CBF62C2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[0:3]" -type "float3"  -1 0 0 1 0 0 -1 0 0 1 0 0;
createNode transformGeometry -n "transformGeometry24";
	rename -uid "D72E8F73-4A5F-98E7-E544-84B95DC0C4D9";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -7.5 -11.5 1;
createNode transformGeometry -n "transformGeometry25";
	rename -uid "828F6D3A-4037-5063-5122-58BFD6251C9C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1.5 11.5 1;
createNode polyTweak -n "polyTweak5";
	rename -uid "8B3D4378-48B5-81F5-78E3-438A83A3390F";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -1.5 -1.5 6.5 -1.5 -1.5
		 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5;
createNode transformGeometry -n "transformGeometry26";
	rename -uid "5596D9A9-47D5-B5B8-AF0E-47B82DB0EB97";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018324 0 0 1 0 0
		 0.38461756040018324 0 0.92307601649691418 0 0.99999999999999978 0 13 1;
createNode transformGeometry -n "transformGeometry27";
	rename -uid "2BDC3B3E-486C-863E-3B6D-4BACA1607B15";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 0 1 0 0 -1.1102230246251565e-016 0 1 0
		 -2.2204460492503131e-016 0 0 1;
createNode polyTweak -n "polyTweak6";
	rename -uid "CCB74E53-47F8-F2CD-9D28-1A9C9595D025";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -2.5 -1.5 0 -2.5 -1.5 0
		 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5;
createNode transformGeometry -n "transformGeometry28";
	rename -uid "B6511720-4F37-D088-9FB2-898C38D27F54";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.98768834059513777 0.15643446504023087 0
		 0 -0.15643446504023087 0.98768834059513777 0 0 -18.699999999999999 12.699999999999999 1;
createNode transformGeometry -n "transformGeometry29";
	rename -uid "392882F0-46CD-F60F-33D8-1091A26B9F95";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -18.699999999999999 12.699999999999999 1;
createNode polyTweak -n "polyTweak7";
	rename -uid "E614DEAC-42E3-53CE-3F64-A9A09A54DF8D";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -3.5 0 0 -3.5 0 0 -3.5 0
		 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0;
createNode transformGeometry -n "transformGeometry30";
	rename -uid "CFD69E13-4F33-EC74-B5CA-828583EBD4D5";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -12.499999999999998 11 1;
createNode transformGeometry -n "transformGeometry31";
	rename -uid "9D765ED2-472F-4D44-5389-D992D179610E";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 0.38461756040018324 0 0 1 0 0
		 -0.38461756040018324 0 0.92307601649691418 0 5.4615702909401849 0 0.93847259198938104 1;
createNode transformGeometry -n "transformGeometry32";
	rename -uid "30D32989-491A-A765-150A-D59ABBA844EA";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry33";
	rename -uid "A44105B6-4D94-3481-B4E6-DA9E268AC258";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry34";
	rename -uid "86ECE2E6-42DD-4A14-B968-5DB94CB5DAAA";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry35";
	rename -uid "CA99D8E4-4C1F-C762-EB58-C0A971D8BDD0";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry36";
	rename -uid "D9DBB557-4A6A-D709-1CAD-BFA4E9268A76";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry37";
	rename -uid "40BBD014-4BD5-6672-ED4E-6EA1F89B2E28";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-016 0 -6.9388939039072284e-018 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-017 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry38";
	rename -uid "D49FA4B9-4AEE-8A36-7EF0-C1934BE46969";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry39";
	rename -uid "46E3DB6E-4F9A-DF0B-27A1-C6B62C6C6F59";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry40";
	rename -uid "39E81AFD-4D23-D118-9C41-84AA1FFC680C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry41";
	rename -uid "1E72E26A-4F3B-B7A2-DE2B-799468144F8A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry42";
	rename -uid "ED701055-4E8B-F5DA-13FE-AF8555FB8F1A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry43";
	rename -uid "F02C6DA8-43F2-9347-5C73-B6AC1CC237E8";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry44";
	rename -uid "D7D4F962-4421-8E33-31E6-26A6E0CC6737";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry45";
	rename -uid "D0660701-4415-4B05-4EA3-CF881D9BAEC0";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.030845914622084619 0.99677887845624713 0.074029703596820554 0
		 0.38337866049027253 -0.080198924328858931 0.92010267645365451 0 -5.2908204681950295 1.1228312710267943 1.5021176062717214 1;
createNode transformGeometry -n "transformGeometry46";
	rename -uid "7AAA0379-4F05-C997-78D0-8685D3BE0528";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry47";
	rename -uid "88D074D8-4F7C-47D8-A486-63B28662117D";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry48";
	rename -uid "3372E96A-40CB-8480-842B-DFAD7DD3B53B";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry49";
	rename -uid "0ED7BCFF-4418-1AAC-754A-5EBD6EE84A24";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry50";
	rename -uid "C436AC16-4450-C1B8-AFDF-36B65C240D06";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry51";
	rename -uid "BBEAF9E9-48AC-B1EA-F009-FFA53D405F35";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode polyCube -n "polyCube24";
	rename -uid "A240EF2B-40E3-C65B-678C-08B4636D5C21";
	setAttr ".w" 12;
	setAttr ".h" 9;
	setAttr ".d" 12;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube25";
	rename -uid "70CE1749-418E-FEB1-3E7C-9ABD21534D0A";
	setAttr ".w" 10;
	setAttr ".h" 3;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube26";
	rename -uid "E508D78D-41CD-B69F-13C2-B7B42A8CF99A";
	setAttr ".w" 6;
	setAttr ".h" 12;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube27";
	rename -uid "81205D2E-47AE-F32A-A780-748093631382";
	setAttr ".w" 8;
	setAttr ".h" 8;
	setAttr ".d" 22;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak8";
	rename -uid "3F94CF60-4EE7-A771-A1EA-39823B5FC1FB";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  8.8817842e-016 1.5 0 8.8817842e-016
		 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016
		 1.5 0 8.8817842e-016 1.5 0 8.8817842e-016 1.5 0;
createNode transformGeometry -n "transformGeometry52";
	rename -uid "E2491441-426A-2AC1-9FB0-ACA329B64218";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 1 0 1;
createNode transformGeometry -n "transformGeometry53";
	rename -uid "6AC1074D-4D5E-FAE8-9CD2-F5893AE64E3A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -11 0 1;
createNode transformGeometry -n "transformGeometry54";
	rename -uid "82FC2D08-4756-0F3A-2D90-5FB6B8AAF1FD";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -18 4 1;
createNode transformGeometry -n "transformGeometry55";
	rename -uid "E06C9A47-4190-ABEB-0F3C-59854B67D17A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -3.5 0 1;
createNode transformGeometry -n "transformGeometry56";
	rename -uid "4F66D127-4A26-B67E-C222-C78A3D8D1396";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -1 1;
createNode transformGeometry -n "transformGeometry57";
	rename -uid "16CB2CEE-4C7E-B65F-8641-95A6FEC5765A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -1 1;
createNode displayLayer -n "layer1";
	rename -uid "C9D462A5-4D4A-DC2B-E7F9-CBB0766BF300";
	setAttr ".v" no;
	setAttr ".do" 1;
createNode displayLayer -n "layer2";
	rename -uid "F09E0FF1-4746-6336-25B0-05B9D4DB9C8A";
	setAttr ".do" 2;
createNode polySplitEdge -n "polySplitEdge1";
	rename -uid "D2ED56E6-48C1-0BF2-6369-21BE630F2B98";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[3]" "e[8:10]" "e[25]" "e[37]";
createNode groupParts -n "groupParts1";
	rename -uid "EDB062CE-47E2-99FB-40A7-6E97C76D391C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:202]";
	setAttr ".gi" 85;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "5D5C4F29-431F-2565-B8B5-9C84B4C7C4EE";
	setAttr ".dc" -type "componentList" 1 "f[16]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "63B2D6D8-45A8-3D4A-B00B-CCBC19666D18";
	setAttr ".dc" -type "componentList" 1 "f[11]";
createNode polySplitVert -n "polySplitVert1";
	rename -uid "0911BF36-4BE5-4339-963F-9785B018A4F5";
	setAttr ".ics" -type "componentList" 3 "vtx[0:1]" "vtx[22]" "vtx[30]";
createNode polyTweak -n "polyTweak9";
	rename -uid "3082FD45-4517-A3A6-81EE-FDA1E59C9160";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[1]" -type "float3" 0 -7.1525574e-007 0 ;
	setAttr ".tk[7]" -type "float3" 0 -7.1525574e-007 0 ;
	setAttr ".tk[22]" -type "float3" 0 3.9999995 0 ;
	setAttr ".tk[23]" -type "float3" 0 -4.7683716e-007 0 ;
	setAttr ".tk[282]" -type "float3" 0 3.9999995 0 ;
	setAttr ".tk[286]" -type "float3" 0 -4.7683716e-007 0 ;
createNode deleteComponent -n "deleteComponent3";
	rename -uid "109354AF-4A23-28C2-D5B1-A9823BF1D963";
	setAttr ".dc" -type "componentList" 1 "f[0]";
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "5A682CA9-4E87-B9F0-4673-EF9BE5F0D138";
	setAttr ".ics" -type "componentList" 4 "vtx[0:8]" "vtx[21:22]" "vtx[29:30]" "vtx[279:285]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyTweak -n "polyTweak10";
	rename -uid "FFC19FD3-49D1-BF59-FD2E-8B8FA162BF88";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[5]" -type "float3" 0 2.3841858e-007 1.9073486e-006 ;
	setAttr ".tk[6]" -type "float3" 0 2.3841858e-007 1.9073486e-006 ;
	setAttr ".tk[7]" -type "float3" 0 -1.9073486e-005 0 ;
	setAttr ".tk[8]" -type "float3" 0 -1.9073486e-005 0 ;
	setAttr ".tk[29]" -type "float3" 0 4.0000196 0 ;
	setAttr ".tk[279]" -type "float3" 0 4 0 ;
	setAttr ".tk[280]" -type "float3" 0 4 0 ;
	setAttr ".tk[282]" -type "float3" 0 4.0000196 0 ;
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "99AAE7BE-4B40-A8DA-EA76-77B3B66CB553";
	setAttr ".ics" -type "componentList" 2 "e[16]" "e[34]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 12;
	setAttr ".sv2" 22;
	setAttr ".d" 1;
createNode polyTweak -n "polyTweak11";
	rename -uid "5E2C7AEB-4B32-AA6E-61A1-30A0857A8949";
	setAttr ".uopa" yes;
	setAttr -s 11 ".tk";
	setAttr ".tk[1]" -type "float3" 0 0 -1.9734911 ;
	setAttr ".tk[2]" -type "float3" 0 0 -1.9734911 ;
	setAttr ".tk[17]" -type "float3" 0 0 -1.9734911 ;
	setAttr ".tk[23]" -type "float3" 0 0 -1.9734911 ;
	setAttr ".tk[46]" -type "float3" -1.8461361 0 0.7957654 ;
	setAttr ".tk[48]" -type "float3" -1.8461361 0 0.7957654 ;
	setAttr ".tk[53]" -type "float3" 1.8461361 0 0.79576492 ;
	setAttr ".tk[55]" -type "float3" 1.8461361 0 0.79576492 ;
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "7C46A213-4A1B-003C-3BA7-04BBDF5CB13E";
	setAttr ".ics" -type "componentList" 2 "e[15]" "e[23]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 9;
	setAttr ".sv2" 14;
	setAttr ".d" 1;
createNode deleteComponent -n "deleteComponent4";
	rename -uid "5CBE8C8F-4055-D828-6E54-91A14ACBC6B7";
	setAttr ".dc" -type "componentList" 3 "f[48:49]" "f[193]" "f[199]";
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "D739DF0E-4FFB-5595-0F36-20BB26137F96";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "01397B29-49CF-1AC5-D604-57B3E2A94C1D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "526FB792-468F-3A9D-950B-9692DC0DEF28";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "9309620D-4B5F-6041-295D-2FB4E903C82E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "29F15865-47B2-D839-99D2-71970F61F688";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "9743E975-4CBA-D3F1-BEB9-EF915C91B606";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "D5EFB407-43F1-67A7-FA1A-FFADD3EE75BA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "B0FDA7DE-435D-9761-EB07-6D8579C42F5A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "18096BC1-4373-8E5C-AD22-1CBC745987CF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "7418B214-4D2F-3B53-CC69-94ADC4354427";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" -6.2496874e-006 -6.1219803e-006 ;
	setAttr ".uvtk[6]" -type "float2" -7.0319611e-006 5.3583326e-006 ;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "33782F40-4EB6-49BE-5E8B-D398A29228B8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "F384C626-4353-4E17-4A1D-BBB135B26664";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "D7E835E3-474F-353A-34C6-45B873ACB1D9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "01E4F633-4F13-CDAD-38C2-A6A198F6E617";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "5AFCA003-4EA1-A045-7417-54AA5A1CA614";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "90819486-4962-A9CD-DFCF-E69CA84E5918";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "D1694C72-4CBA-2B4B-8083-9DB8B7B6F082";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV18";
	rename -uid "B9351145-43BE-FD4B-88D7-7BA25955257A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV19";
	rename -uid "58C24BAA-4DC4-59EE-20A2-59A2045F8219";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV20";
	rename -uid "532638FB-422C-5DD5-393A-129BA2B3DFD6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV21";
	rename -uid "FC32703E-4400-2FC8-A79E-CCAC42916426";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV22";
	rename -uid "FD9EDD5A-4554-A86F-57F6-9DBF471F55A1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV23";
	rename -uid "4DD2F7C1-4E4D-E4EA-00DA-A3A76B53F6C8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV24";
	rename -uid "66B94B1C-4819-2D58-F9A1-C0BDC15A49A4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV25";
	rename -uid "EBF8C0F8-4ACF-EC74-FFCF-EDB234F3AA82";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV26";
	rename -uid "1085DCD1-4D5A-B777-6E4F-EB885EEC1DF5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV27";
	rename -uid "057ACCEE-4CD1-9C73-AF31-10AC16FD2EC1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV28";
	rename -uid "6C6A307E-4748-CB48-F9F5-83904287F213";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV29";
	rename -uid "E30BDC4E-44E6-B311-3571-E995713D966F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV30";
	rename -uid "275B8444-4CC9-E583-6D92-129E1FD93BF5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV31";
	rename -uid "7880480A-48EE-F428-38DA-9180A53BDABA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV32";
	rename -uid "A8816AAC-4ADB-39A8-33B4-AA81F124D861";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV33";
	rename -uid "1D6B8D55-4637-0913-8F1C-A18DBE2C73D7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV34";
	rename -uid "8AC5E0EE-4335-DE36-AD4D-9795352F4F03";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" 0.0053007584 -0.017715519 ;
	setAttr ".uvtk[7]" -type "float2" 0.0053007454 0.017756527 ;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV35";
	rename -uid "3100E743-4B15-318A-99CD-B18A5A0FA32A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "C0C57AAC-41C9-C8EE-823D-5DA6E696C6E3";
	setAttr ".ics" -type "componentList" 4 "vtx[70]" "vtx[72]" "vtx[261]" "vtx[263]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak12";
	rename -uid "BB3F4BF3-48EA-B63D-3023-25A342463BAE";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[261]" -type "float3" 0.27563736 0 -0.038738251 ;
	setAttr ".tk[263]" -type "float3" 0.27563736 0 -0.038738251 ;
createNode polyTweakUV -n "polyTweakUV36";
	rename -uid "230F3CF4-4246-F874-355A-97A58AAC66A1";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV37";
	rename -uid "D921DFE0-412D-FB1C-7E13-F58ADFBD2FD7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV38";
	rename -uid "D89A0E8E-4C9F-FA5F-4F0D-DBAAA88F7420";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV39";
	rename -uid "B0487DB0-4AD8-352F-CD08-46A329C46CAA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV40";
	rename -uid "3B00F161-4838-78FD-27FB-DDAE2BD35232";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV41";
	rename -uid "89315B74-4932-D7F1-90D8-5A8A176F6EB1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV42";
	rename -uid "EDF204FF-48A2-6506-EDCF-F3BCEE6A2F0A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV43";
	rename -uid "20ED646C-4279-3BF1-A619-44BCFC00FDF6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV44";
	rename -uid "457579B1-4705-5778-E1AC-648076004AF1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV45";
	rename -uid "8973BFBD-4808-DF68-1CA3-AD89DFB83112";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" 5.5641899e-006 -8.3198656e-006 ;
	setAttr ".uvtk[7]" -type "float2" 5.9702184e-006 7.1874224e-006 ;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV46";
	rename -uid "8EB5218D-4D8C-F111-A901-93ACB8054496";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV47";
	rename -uid "1C2D5000-4FD7-DE90-ADE4-67BB3374DD94";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV48";
	rename -uid "85352A68-4410-E316-088F-BEBF725F2AA3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV49";
	rename -uid "D81F364D-488B-5CF4-6EFF-548418CDDA17";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV50";
	rename -uid "6DA3AA8E-4CD4-43CC-1EAD-11BC25D4FE00";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV51";
	rename -uid "DAFE7697-4975-FE1A-885B-D78E7BF13DA2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV52";
	rename -uid "062CA157-40EA-60BF-3470-20B94582B728";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV53";
	rename -uid "24479AFE-4615-0352-FA4E-3FA3033D59B2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV54";
	rename -uid "AE0FE810-4F20-D9FF-E25F-C2A2D624C5B5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV55";
	rename -uid "189F3C69-4651-7B95-9086-2FAC3CA2CE57";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV56";
	rename -uid "A970A187-47DE-66ED-DB65-A1A8AD799D4E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV57";
	rename -uid "731155EE-4DDD-4B21-4E1F-06BF259F314E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV58";
	rename -uid "053E2724-44E9-D303-F467-4E92053581D3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV59";
	rename -uid "E12AF1BD-4556-83C1-C046-0DA3BC2C56C2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV60";
	rename -uid "1847D533-431E-A7F7-3F9B-B894E5BD5A3C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV61";
	rename -uid "826A6D91-4E85-BA4F-E84B-4DA6A42EBE2A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV62";
	rename -uid "D72F58F7-4DA5-0D06-63DB-4D8B4694D6F4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV63";
	rename -uid "321C62CD-46B9-9562-C661-5CAD426FE6F1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV64";
	rename -uid "C4C9454E-44EE-6927-8E64-38B949543241";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV65";
	rename -uid "52D47AEA-40BB-830E-5940-29A8A607A32A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV66";
	rename -uid "03862760-41A8-0F58-3C17-779165D3F41E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV67";
	rename -uid "DCC61652-4832-B87C-782D-D88B3C386E03";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV68";
	rename -uid "F047B85A-4BBD-C155-9716-60BB154CEB4C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV69";
	rename -uid "EE4792B6-48F7-B40C-A4E6-D0AA199005AB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV70";
	rename -uid "2D8356E3-45BF-2891-B089-C297A683F871";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" 0.0053007584 -0.017715519 ;
	setAttr ".uvtk[6]" -type "float2" 0.0053007454 0.017756527 ;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert3";
	rename -uid "245076CA-458C-DA52-AF54-EB8FE5177726";
	setAttr ".ics" -type "componentList" 4 "vtx[69]" "vtx[71]" "vtx[267]" "vtx[269]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak13";
	rename -uid "4B0055A2-4A54-8AEC-B459-1FB1EC5F637A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[267]" -type "float3" -0.27563736 0 -0.038738251 ;
	setAttr ".tk[269]" -type "float3" -0.27563736 0 -0.038738251 ;
createNode deleteComponent -n "deleteComponent5";
	rename -uid "A93456A5-4295-6E57-BF04-6E8FF220506D";
	setAttr ".dc" -type "componentList" 2 "f[59]" "f[119]";
createNode polySewEdge -n "polySewEdge1";
	rename -uid "39DD5F6E-45B5-54D7-54E2-76900B931520";
	setAttr ".ics" -type "componentList" 2 "e[129]" "e[249]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
createNode polyTweakUV -n "polyTweakUV71";
	rename -uid "6C959182-4505-74E1-A3A2-3790AD187630";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV72";
	rename -uid "3837BC99-4DA1-BE94-AA7B-CA8E57C30531";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV73";
	rename -uid "98077AC7-463A-1692-243A-D592A60DA498";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV74";
	rename -uid "37C84F6E-4282-CE51-BECD-3E8F79988D87";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV75";
	rename -uid "40B31402-4504-F29F-D510-BBB012FE751E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV76";
	rename -uid "C0B7A2DF-4C90-77B7-37A5-B4B48F05742A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV77";
	rename -uid "200D38B2-442C-40EE-BA2C-0C843FBC1886";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV78";
	rename -uid "793AF5D3-43B6-E816-48D8-10AF53078818";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV79";
	rename -uid "69D40BAC-46B7-1BFA-48D1-4DB5E32EF465";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV80";
	rename -uid "28E1650F-4176-3330-01E0-B18DF1B1081F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV81";
	rename -uid "36256C6A-4965-4937-19C5-DA96C0C41D4F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV82";
	rename -uid "34961943-4E5A-2B64-2961-EABB70B94957";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.00036727072 -1.5854432e-005 ;
	setAttr ".uvtk[3]" -type "float2" -0.012633326 0.035992239 ;
	setAttr ".uvtk[9]" -type "float2" -0.004404394 -0.012814561 ;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV83";
	rename -uid "5E86D081-46FE-89A0-1B0E-D4B01540493C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV84";
	rename -uid "3F63724D-44C6-29B7-8ACA-FA9979BDAA60";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV85";
	rename -uid "21EE8937-4D88-85CB-914B-BCB0A5E0080D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV86";
	rename -uid "3C1E29D0-4BEA-18ED-B476-54A0AE881EAF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV87";
	rename -uid "51A5FFD1-4E11-9448-A542-7FBBF8E4C227";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV88";
	rename -uid "490243BB-42DB-E538-5D56-E4BB0CF245F7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV89";
	rename -uid "E131DD4D-44F9-DA5B-0EED-699E3803545B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV90";
	rename -uid "78A2994A-4890-DFB5-4349-C3A68166A547";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV91";
	rename -uid "73DE4669-44A8-2699-084F-3893103755A7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV92";
	rename -uid "E4B72B31-46A6-242D-D644-068FF65FFAEA";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 3.5932073e-005 1.6772384e-006 ;
	setAttr ".uvtk[1]" -type "float2" -0.0015064966 0.00432433 ;
	setAttr ".uvtk[8]" -type "float2" -0.0003111058 -0.00090770068 ;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV93";
	rename -uid "172C7973-46B5-9226-9BCB-B7AC698107EF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV94";
	rename -uid "18C4C53F-4501-33F2-1193-2B977CD77087";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV95";
	rename -uid "B26FE885-430A-9CC9-96C6-86B95074B2F3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV96";
	rename -uid "2C988A49-4B71-34E5-927B-E89F83B10FDA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV97";
	rename -uid "D009D327-4960-09D5-FEFA-2F85014AFF8E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV98";
	rename -uid "01A4F95D-4F61-89A4-6FAC-17933FFC7439";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV99";
	rename -uid "86C057D4-422B-5630-4607-6A841ECCA96B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV100";
	rename -uid "8C68134C-449F-56CF-1C74-8DBB13D700E2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV101";
	rename -uid "B045A030-4362-2656-80FD-14AAE0A40152";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV102";
	rename -uid "455EA769-4FFB-8EC5-80AA-1586BD8F836B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV103";
	rename -uid "62318768-4F96-FD71-BE35-7AAE97CEA833";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV104";
	rename -uid "7E4F8157-42A5-C6C2-1011-7399EAB95202";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV105";
	rename -uid "D2C907EB-4DF4-54B4-1A1C-2DBEC3B6545C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert4";
	rename -uid "5487A306-42DE-07C8-7157-0BBD4B0DB77B";
	setAttr ".ics" -type "componentList" 4 "vtx[81]" "vtx[83]" "vtx[161]" "vtx[163]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak14";
	rename -uid "A24E9C92-48B1-B487-524A-2A9A6E3A207A";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[81]" -type "float3" -0.00066125393 0 0 ;
	setAttr ".tk[83]" -type "float3" 0.0074818134 0 0 ;
createNode polyTweakUV -n "polyTweakUV106";
	rename -uid "ADE2B740-4113-4459-5F79-63B86629046C";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV107";
	rename -uid "E480520A-4F6D-B029-B289-44B25A0B0963";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV108";
	rename -uid "84336B01-42CC-D081-B55E-ACB8318C2546";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV109";
	rename -uid "047F7659-4728-AD3D-E761-71851196F032";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV110";
	rename -uid "AD269BCA-4EDF-9F1F-6EE1-B4B56A87AA08";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV111";
	rename -uid "53B3824E-4085-51DB-1CD0-A7953548D0FC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV112";
	rename -uid "2DB5FE54-4D32-33CC-4F34-A89552C9D156";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV113";
	rename -uid "7C05C2D7-4588-6173-F4D9-BFA3D3E50E54";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV114";
	rename -uid "823E7C31-41A0-2E39-038E-00A9D921F28B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV115";
	rename -uid "E1C3A5C6-4DEC-ECFA-A492-60809E17A5EA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV116";
	rename -uid "09B29FB8-4412-4C3B-2BB9-E6AC8DD64747";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV117";
	rename -uid "60CD8CC9-4408-5153-1479-DC9A750B65C2";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" 0.0052323746 -0.015680721 ;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV118";
	rename -uid "E6C00B7E-4FA6-F2B2-8409-D5AD14F7D68B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV119";
	rename -uid "92AC0EFA-416D-7DBA-E93C-459A984C0EB4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV120";
	rename -uid "45909633-4D4F-5C2F-2C06-DAB5AD8486AD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV121";
	rename -uid "EE33A8F9-4E94-2B43-036B-428A7F80D4B2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV122";
	rename -uid "E352F7A6-48B5-4FCD-9B5C-9091EF5E6E55";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV123";
	rename -uid "8B1AD553-4CE5-E885-1527-E4BC5D052880";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV124";
	rename -uid "6D541C91-4E76-7D1E-B39A-E6A3B20A0373";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV125";
	rename -uid "51C69097-4084-E057-9FAA-D09389EF51D4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV126";
	rename -uid "BF45CA7A-4520-3C81-E36D-F292D2CA8D22";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV127";
	rename -uid "5903D1A4-4A42-F163-45B5-6094F0A4C8DF";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" 0.0071218377 -0.021060703 ;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV128";
	rename -uid "570A6F79-410D-0B9C-E367-F3964A116277";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV129";
	rename -uid "B96409B4-412D-EB07-AD60-4DA6DBB87460";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV130";
	rename -uid "758309D4-4034-DD2B-C343-95A48CE8F88D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV131";
	rename -uid "6E575D55-41A4-A517-8E0F-D4930C764D92";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV132";
	rename -uid "A919A29C-4670-A7AB-DA4C-4A88C8A65269";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV133";
	rename -uid "83BB6F53-42B9-C428-16C1-619C16C6B9DE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV134";
	rename -uid "7D07EEB1-4C09-63A7-3BB6-D29BB231324F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV135";
	rename -uid "5F8664F1-4AA4-9258-DFD6-F59819B0376E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV136";
	rename -uid "7E463B3D-4F01-090A-EFCF-4D982EA2FFB3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV137";
	rename -uid "67A60283-47B4-0A92-074C-FDA557D77070";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV138";
	rename -uid "02516BB9-4BA2-6A7E-F0CB-B298DFBA3E9A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV139";
	rename -uid "4C625DDA-427B-6C83-3096-908C84D570B2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV140";
	rename -uid "D640B498-4028-B1BD-83B3-FA9650109525";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert5";
	rename -uid "D540AF1D-4E16-B837-64DF-D8BF706954E1";
	setAttr ".ics" -type "componentList" 2 "vtx[85]" "vtx[163]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak15";
	rename -uid "3E8287BE-4649-24BF-592C-119BF1A72502";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[85]" -type "float3" 0.38213921 0 0 ;
	setAttr ".tk[163]" -type "float3" -0.38213921 0 0 ;
createNode polyTweakUV -n "polyTweakUV141";
	rename -uid "C967EA90-4243-D1C5-9918-9187F57126B2";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV142";
	rename -uid "C1292B46-4480-879C-7914-9B912380B499";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV143";
	rename -uid "E607AD4E-426A-BFE1-CB64-0283C68A99E2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV144";
	rename -uid "9EAFDF13-4C84-5A16-0FD9-FAB5CB00ABBD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV145";
	rename -uid "934B9136-4BC6-A8AC-3DC7-ECB52F8FD70B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV146";
	rename -uid "C716A0AF-4EDC-4762-0D92-E7A7EB3DB7EA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV147";
	rename -uid "A5C50D2C-462A-ECD3-98F2-889D4050DA47";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV148";
	rename -uid "63BB2515-4F66-E558-5FE0-85B1DA4CC90E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV149";
	rename -uid "F0672300-4DDE-F5A1-4AFC-C2BD9CFA2E2F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV150";
	rename -uid "659E5BF3-40F1-D56E-02DF-66934FA7C991";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV151";
	rename -uid "B81354A9-45FE-461C-33CD-36B52D1DD841";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV152";
	rename -uid "B106F256-410E-06A0-FEDE-2C977330A8AA";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[7]" -type "float2" 0.010698814 0.027252993 ;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV153";
	rename -uid "A201D325-419E-2E55-D433-2D81A94DF1C6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV154";
	rename -uid "83093482-4724-DC89-2849-00B1E1F8063D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV155";
	rename -uid "4A55DCC3-483E-FCE7-42CE-2CA729FD3C76";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV156";
	rename -uid "573C4284-4FC8-5325-C91B-F1A3020D3D61";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV157";
	rename -uid "F102DC07-4E2D-5BE2-34D2-5D9DD9B5C50A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV158";
	rename -uid "C194C700-46BB-C829-16BE-3DA197876602";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV159";
	rename -uid "11CF1705-4880-5B9B-C663-798CDE121392";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV160";
	rename -uid "2B6E48AA-4435-10BA-695A-8182D28C0737";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV161";
	rename -uid "413E4852-4D15-0490-68B9-76995DF68569";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV162";
	rename -uid "48978177-4513-DE61-E5F5-4A8551741AE7";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[6]" -type "float2" 0.011642481 0.028822256 ;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV163";
	rename -uid "732850B8-47CA-E16E-477A-6693A1BEAB4B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV164";
	rename -uid "3B33C779-41B4-96DA-F551-CD87D5EB16AC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV165";
	rename -uid "EE91D0A8-48D9-CE4F-6622-248FBBB1EDF4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV166";
	rename -uid "C2E8080C-4A49-7DB6-BBD1-AB83996BC665";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV167";
	rename -uid "6E2640A6-4B88-2ADD-0FD6-849694382F23";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV168";
	rename -uid "21459EFA-4C91-5E4F-2E29-399D2C058A74";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV169";
	rename -uid "9649ECBC-4D1D-A185-B1A3-46AF45ED483E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV170";
	rename -uid "6CE2251D-41CC-9336-438C-3C8502D074A5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV171";
	rename -uid "F48B7093-481E-2B8A-3CAE-EDB04C71AA00";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV172";
	rename -uid "D0797363-48CE-DB52-78B7-978A2E05FA39";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV173";
	rename -uid "6C7F849F-439D-09AE-C4C5-A0A3BF752259";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV174";
	rename -uid "B92D8A25-47FE-6926-F7F3-8EAFD0480DF9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV175";
	rename -uid "CEB4573D-4881-0CAB-36FC-95A86203CC16";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert6";
	rename -uid "5A1824E8-4814-FEAB-173A-9BB32BD8BCA2";
	setAttr ".ics" -type "componentList" 2 "vtx[87]" "vtx[164]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak16";
	rename -uid "0DF3EDA2-4104-0B0F-AF72-5FB7848475D4";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[87]" -type "float3" 0.69059849 0 0 ;
	setAttr ".tk[164]" -type "float3" -0.69059849 0 0 ;
createNode polyMergeVert -n "polyMergeVert7";
	rename -uid "6F73C1B0-49E2-E21F-B6B3-E68DCEC235A0";
	setAttr ".ics" -type "componentList" 2 "vtx[8]" "vtx[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert8";
	rename -uid "F51FABC7-45B9-4B6D-7CAB-67928B8F85EA";
	setAttr ".ics" -type "componentList" 2 "vtx[7]" "vtx[18]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polySplit -n "polySplit1";
	rename -uid "75FBC52F-41DD-A244-6C5F-09927B6B8A2A";
	setAttr -s 4 ".e[0:3]"  0.2 0.80000001 0.80000001 0.2;
	setAttr -s 4 ".d[0:3]"  -2147483622 -2147483637 -2147483636 -2147483612;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode deleteComponent -n "deleteComponent6";
	rename -uid "66C8731E-4B47-125A-7B59-DB9BA8E77158";
	setAttr ".dc" -type "componentList" 1 "f[37]";
createNode deleteComponent -n "deleteComponent7";
	rename -uid "81AF6890-49F2-3DBD-DBA6-D79AFCCC3A34";
	setAttr ".dc" -type "componentList" 1 "f[30]";
createNode polySplitEdge -n "polySplitEdge2";
	rename -uid "075137BB-4533-0E0F-80CC-6FAA71CB45F7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[63]";
createNode deleteComponent -n "deleteComponent8";
	rename -uid "3916668B-47A8-6032-A931-5B901DA5C2A9";
	setAttr ".dc" -type "componentList" 2 "f[25]" "f[35]";
createNode deleteComponent -n "deleteComponent9";
	rename -uid "3322F3D3-48F5-9A1D-2439-6FAFB7CE619C";
	setAttr ".dc" -type "componentList" 2 "f[24]" "f[29]";
createNode polyTweakUV -n "polyTweakUV176";
	rename -uid "8720A027-4A06-273E-D6D3-F1BCAF3030C5";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV177";
	rename -uid "D0E9AF43-466C-E1AD-DF2A-559A7A5545A0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV178";
	rename -uid "96A483B1-4DFB-39D9-C825-29B8329A4A8C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV179";
	rename -uid "1E6311FD-42C1-6486-19CD-3FA407E7E2E8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV180";
	rename -uid "F44200C3-425C-9C1C-478C-F0B98A589801";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV181";
	rename -uid "D706578E-4DD9-788B-2BBC-7E9680C6A82B";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" 0.072116442 -0.0096154474 ;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV182";
	rename -uid "2BB72FB3-4BD4-AADC-F8D7-0E9642C81A36";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV183";
	rename -uid "55B618C0-44D9-B8E2-36BC-D7B04C949BCD";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" -0.017790344 -0.0092618382 ;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV184";
	rename -uid "68C27640-4CBE-94A4-1747-90B122771629";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV185";
	rename -uid "FE599CA8-4E2A-BDF1-178E-2F94902B7CA6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV186";
	rename -uid "1A72B659-455F-7F10-8FFD-2D9603310FAD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV187";
	rename -uid "8ED837F8-4555-EA5D-3EB0-9EA2F7D87080";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV188";
	rename -uid "E16CA98D-41FA-FBC0-DB39-0DA69B5A0C84";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV189";
	rename -uid "CA277982-4227-D09A-21AF-809E4EED736C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV190";
	rename -uid "5F452F83-4560-A189-8A59-F1A77347B983";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV191";
	rename -uid "348153F2-4430-17FD-1E70-4090AF835FE0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV192";
	rename -uid "9580D165-465B-9970-89DB-0FB17F8C51A5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV193";
	rename -uid "D11400E8-4AA4-E191-DF2C-A7913F3E4FEC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV194";
	rename -uid "68A0659E-44EF-9AC8-0A3C-C38C1970DE2E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV195";
	rename -uid "463502EE-4093-E4BB-C011-F08E0D26DA72";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV196";
	rename -uid "EF38FE79-49C8-06C4-20AF-38B6A7BDCB8C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV197";
	rename -uid "CE161CC7-4E39-2A25-EEDB-36A644281E07";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV198";
	rename -uid "568B7C45-4153-F2DB-B5D0-2D8C94B925BA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV199";
	rename -uid "4A243352-463D-8EE2-C107-E8B74B98DE75";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV200";
	rename -uid "0BE6951A-49A0-C048-D6C7-76A4E6FFC14C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV201";
	rename -uid "D6BCFF13-4589-018D-9713-9FBB4EE5853A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV202";
	rename -uid "13DE52BD-4AF5-86DB-5B05-94A59FE65854";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV203";
	rename -uid "58AB15A9-4C13-8385-9065-39B7C21C966B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV204";
	rename -uid "4FEAA82A-44EA-AF5B-5A5F-FD9DFA74A329";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV205";
	rename -uid "AAC41DB2-49B7-0B06-5AA8-2BB13C92E11C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV206";
	rename -uid "363004C5-4A67-3D5D-005F-E29C77ADB1F9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV207";
	rename -uid "B10B3A1F-491C-24A8-663B-60BB6EAA8ECE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV208";
	rename -uid "00D25A64-42B9-EE4F-E4E2-7B86349BA95B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV209";
	rename -uid "D9F1EE4E-4926-6030-3798-38AAE8724678";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV210";
	rename -uid "8F3AD764-46D6-BF8B-1D60-F7BBE6048A6A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert9";
	rename -uid "0BF4F1C9-4095-36C1-68ED-4D89DF9CDCF1";
	setAttr ".ics" -type "componentList" 2 "vtx[35]" "vtx[52]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak17";
	rename -uid "F686D1DE-41D3-4A2B-32CE-A5B801F0C0BD";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk";
	setAttr ".tk[35]" -type "float3" 0.57692623 0 0.11538601 ;
	setAttr ".tk[39]" -type "float3" 0 2.8610229e-006 0 ;
	setAttr ".tk[40]" -type "float3" 0 2.8610229e-006 0 ;
	setAttr ".tk[45]" -type "float3" 0 2.8610229e-006 0 ;
	setAttr ".tk[46]" -type "float3" 0 2.8610229e-006 0 ;
	setAttr ".tk[52]" -type "float3" -0.57692623 0 -0.11538601 ;
	setAttr ".tk[81]" -type "float3" 0 -2.3841858e-007 0 ;
	setAttr ".tk[82]" -type "float3" 0 -2.3841858e-007 0 ;
	setAttr ".tk[83]" -type "float3" 0 -2.3841858e-007 0 ;
	setAttr ".tk[84]" -type "float3" 0 -2.3841858e-007 0 ;
createNode polyTweakUV -n "polyTweakUV211";
	rename -uid "AFB4D7BC-47E3-51FD-5820-799CC8EA5340";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV212";
	rename -uid "7C101751-45A4-A15D-8BD1-6A9B26AA44CD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV213";
	rename -uid "567DB809-4C1E-026E-92DB-0CB198CB48E7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV214";
	rename -uid "4215B099-4D3E-515E-E462-29B00B597D7C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV215";
	rename -uid "73954CF3-4049-FC0B-D6DB-04994F511DAE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV216";
	rename -uid "8270D22B-49D8-55AA-F00C-63892092437F";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" -0.072115764 -0.0096154492 ;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV217";
	rename -uid "A7C677D6-446A-25A1-A476-718A129578FD";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" 0.017790345 -0.0092618363 ;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV218";
	rename -uid "AA88A329-4EA7-3E80-3907-CBB1B33C90F7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV219";
	rename -uid "EB7B0598-4D20-B991-0C33-8292296A7B3A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV220";
	rename -uid "951E1F82-411F-E31F-D58C-6B938FA2F4D5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV221";
	rename -uid "3C6006B1-4654-E880-94EC-49A22DEE739F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV222";
	rename -uid "BB4C76BF-4BC8-D75D-5A2B-7892267B0FF3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV223";
	rename -uid "05176B1E-47B0-E8E4-6DA0-0F847CCD4010";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV224";
	rename -uid "5826FF53-4F96-8F90-D8B6-B093124BDF19";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV225";
	rename -uid "58B8500D-4D20-D2BA-F003-18B1A7DA2872";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV226";
	rename -uid "14331092-46CE-1A99-0972-23841943FDC5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV227";
	rename -uid "443A5796-4A4F-C02F-7B2D-3AA3C4CA43A7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV228";
	rename -uid "DEA5E0B1-42B2-7105-F68B-D3A925EF2ADA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV229";
	rename -uid "C26CF450-44FB-CC3D-E2A6-66B1D6A3F890";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV230";
	rename -uid "4E6265C6-443A-8E26-9180-32BBF05A5F97";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV231";
	rename -uid "9A70894F-47AE-F4E9-892F-5393C6FF4F41";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV232";
	rename -uid "1F4399EA-40FC-81AF-1F70-C0A473893D64";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV233";
	rename -uid "ACA492F8-40F9-E40A-7527-18A03570F0EE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV234";
	rename -uid "2AF95DE8-4FD8-B060-E84C-47B5913DAEF4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV235";
	rename -uid "33F401CA-4D88-361B-F8DF-90AB2DF0BCC6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV236";
	rename -uid "9D5CB904-4D1E-E83F-2A8B-9DB33C76C726";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV237";
	rename -uid "C881FB27-4814-038F-CB7D-0CB8CAF7D2CC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV238";
	rename -uid "25B32723-4902-B661-BE09-70B58E660C92";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV239";
	rename -uid "E7CAE27F-498D-BAC1-B267-768CC7FE5DF5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV240";
	rename -uid "C4D1B1C1-43AE-6008-39A4-4C8475DD38AB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV241";
	rename -uid "14032313-467F-3A6C-8E0D-4097F4FF6B4A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV242";
	rename -uid "8F6BE6FA-4D58-D3FA-F8C5-C49E8C257185";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV243";
	rename -uid "B251EC01-4B54-3FF0-9030-0CA15D90750D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV244";
	rename -uid "A1777AF0-4064-BDA7-311C-86BC848CA871";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV245";
	rename -uid "98D5E8F7-4B5D-20E3-F8D1-A1864189E410";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert10";
	rename -uid "8F83E581-4AD5-DA99-33A0-9DAEBB71395D";
	setAttr ".ics" -type "componentList" 2 "vtx[36]" "vtx[43]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak18";
	rename -uid "C85DE57C-419C-7DE3-F51A-53885228E21E";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[36]" -type "float3" -0.57692635 0 0.11538601 ;
	setAttr ".tk[43]" -type "float3" 0.57692635 0 -0.11538601 ;
createNode polyTweakUV -n "polyTweakUV246";
	rename -uid "BABF0CF7-4B54-32B4-75FF-5281E7DA1491";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV247";
	rename -uid "9DD99575-46F6-3F4F-9E75-8A854ABDD701";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV248";
	rename -uid "89AB6985-4BF8-0C90-A7A0-A8B42F28DA7F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV249";
	rename -uid "56C24F6A-4F94-1BC7-D921-51995DF07A1E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV250";
	rename -uid "60D2C07F-485A-70D5-5530-2ABB69CD162D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV251";
	rename -uid "0662E998-4CB8-21E3-2369-75B12771786C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[7]" -type "float2" 0.072116628 0.0096154213 ;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV252";
	rename -uid "1BEE232A-4F96-0B35-A8D3-5BAE197BC6DE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV253";
	rename -uid "EBFBE007-40A5-A9DA-CA9C-0985EF9ACDDC";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[6]" -type "float2" -0.018500302 0.0074368282 ;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV254";
	rename -uid "87130A73-4AB6-D694-12B4-2590DF69BE7A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV255";
	rename -uid "481CCFF2-449A-0065-A1E7-E09806B8B124";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV256";
	rename -uid "83E3A7EE-498C-CBA1-3CF6-A8A32DF7F8BC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV257";
	rename -uid "64BE4BE5-4A4D-2B0F-FD1C-3FBFF0DBA8D4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV258";
	rename -uid "AC0A4A40-48BC-E364-C32E-6A9F724DA8D0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV259";
	rename -uid "43387BA1-4229-D5E7-B3F3-51967F5FB2E0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV260";
	rename -uid "D0CF4458-4436-61DA-361B-1296D0E6434D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV261";
	rename -uid "895898E6-4EC0-1E9B-BB1F-3B8556209EF5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV262";
	rename -uid "8F4EE721-4DB1-8809-908C-BDB88414E43E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV263";
	rename -uid "9E78C2A8-4ADD-D857-C4DE-0C8747C46F57";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV264";
	rename -uid "5BBD2D1D-4A5B-9BB0-BF4B-20A18415AE02";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV265";
	rename -uid "95C25B27-41DA-C6F3-76EC-94816F251A76";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV266";
	rename -uid "19466EF5-4FF8-96FC-5FE7-74B040DC942F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV267";
	rename -uid "59C91CF5-43D8-09A5-A5D5-1BA989C236E3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV268";
	rename -uid "7D753C35-4E04-A5FB-5FE7-35807274C62E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV269";
	rename -uid "BFD3DDD1-4AF8-988D-7653-16BCC7E75D68";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV270";
	rename -uid "5E3FE6B6-472E-59ED-98DB-4BBD07D767BB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV271";
	rename -uid "70FD96D0-4DEE-60E3-E1AC-5FBB559FCEAE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV272";
	rename -uid "353C6705-42E0-85C6-02F9-8D9AFEBA52FB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV273";
	rename -uid "4BC596D0-48C9-68A3-B790-0582032F07D9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV274";
	rename -uid "5D20ED4B-43F2-AE11-9FB1-D88BAE9E716E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV275";
	rename -uid "D281C9E0-44AF-34A2-DFA7-56B7C2D2C7B7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV276";
	rename -uid "9ECC0668-4132-B601-645F-D28D855741BD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV277";
	rename -uid "0A061130-48DE-DC48-1138-69921E6802A5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV278";
	rename -uid "27090404-4742-61CB-D1B4-1F9A1BCFBC5F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV279";
	rename -uid "AEB21CFE-40F1-EF73-4930-C4BA1FDCE62E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV280";
	rename -uid "A6E4A21C-4F36-820C-1405-F2B0F007E988";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert11";
	rename -uid "694CE873-4A6D-C801-492C-77AA3D1DCE5A";
	setAttr ".ics" -type "componentList" 2 "vtx[37]" "vtx[52]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak19";
	rename -uid "F4B92B35-4312-3584-31FC-65A37F732790";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[37]" -type "float3" 0.57692623 0 0.11538601 ;
	setAttr ".tk[52]" -type "float3" -0.57692623 0 -0.11538601 ;
createNode polyTweakUV -n "polyTweakUV281";
	rename -uid "B158C27B-4661-F754-2F0A-EAAE8FDEC3F2";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV282";
	rename -uid "3A536E1E-483B-449B-E28B-ED99804DE9BA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV283";
	rename -uid "7CA1799F-40CD-8EF8-902C-5BB96C4D6CD5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV284";
	rename -uid "4F686896-4E24-3A3C-D26B-808C05612AE4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV285";
	rename -uid "9EDE5BF4-4877-C212-58CD-FFBC6EDE92AD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV286";
	rename -uid "2BEFCA7B-4FBA-3A64-1E51-CA9821C93E6A";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[6]" -type "float2" -0.072115801 0.0096152825 ;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV287";
	rename -uid "9A642F06-4682-1062-ED57-348AF4A4A947";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[7]" -type "float2" 0.018500304 0.0074369498 ;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV288";
	rename -uid "9F60027B-452B-71D5-CB52-1A86484F52F6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV289";
	rename -uid "E1C1BB69-4EDD-4029-24BA-8B9E59277EE2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV290";
	rename -uid "A324EF3B-4661-38EF-3FE1-E1B724ACE8F0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV291";
	rename -uid "655D9244-4C56-FC1C-55D0-8E92A037E133";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV292";
	rename -uid "01B1E723-4866-4DE4-C81C-D69727CB4E5F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV293";
	rename -uid "A7B4258E-4F2D-D5D2-5041-79AA9848415C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV294";
	rename -uid "9C6911DD-4BF0-9E75-05AD-7C8F21E72343";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV295";
	rename -uid "CD91C02D-444B-E9E8-DD01-14B477F35EAD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV296";
	rename -uid "A4525CFC-4BDC-5B11-D537-9B95DE7E8B4C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV297";
	rename -uid "12005FEC-44E7-A3E6-777E-35AF0658590F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV298";
	rename -uid "71BB7721-424C-D5F6-F3A9-14A1F78D76A4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV299";
	rename -uid "0E28E7B5-4E02-D9D0-2722-0082EB4933D8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV300";
	rename -uid "8B4E8D65-499C-5A02-CBBB-73AA5795A0BA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV301";
	rename -uid "D5D723DB-4A61-F5F4-26DB-94B1A8250A13";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV302";
	rename -uid "0BEE32E5-49C8-5FE2-86F4-90B821F5F6B9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV303";
	rename -uid "B6B52627-42AA-B731-3580-23B0FA52AEBD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV304";
	rename -uid "803DFB60-4739-F03B-DDA2-04B7A596A33C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV305";
	rename -uid "7544569C-4E3B-8F78-3390-A586F57C7968";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV306";
	rename -uid "C01C91E7-4216-7C28-60BB-8498C25DD3BA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV307";
	rename -uid "CA895210-4643-FD23-2503-E6B6C9044D22";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV308";
	rename -uid "A71F87FF-4CC2-BE15-1BDE-3388C2111820";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV309";
	rename -uid "7D644272-4916-EF7B-CD38-938207BAF37C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV310";
	rename -uid "D0126F6D-43DB-350D-3FA7-D0AF05852348";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV311";
	rename -uid "E4F28B08-46B1-4203-E56E-3D82355FBF13";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV312";
	rename -uid "A5E93FA0-4D74-73F8-A083-5C9FE8FC935D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV313";
	rename -uid "BA1F8E1B-4797-DA86-05F7-A7BAC7AC88F9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV314";
	rename -uid "8616CDC4-42FA-A8D5-FBAB-0BB75161AEE6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV315";
	rename -uid "9EB41B19-4467-E893-8EF0-82ADC62036EF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert12";
	rename -uid "B2BA3CF0-45D4-54A2-8610-8085CFD23AB9";
	setAttr ".ics" -type "componentList" 2 "vtx[38]" "vtx[44]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak20";
	rename -uid "FE6BABF3-4501-FB7F-6AA5-D0BACBEE7B48";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[38]" -type "float3" -0.57692635 1.4305115e-006 0.11538601 ;
	setAttr ".tk[44]" -type "float3" 0.57692635 -1.4305115e-006 -0.11538601 ;
createNode polyMergeVert -n "polyMergeVert13";
	rename -uid "00E7E9EA-4454-D8C1-52F7-8F861852763C";
	setAttr ".ics" -type "componentList" 5 "vtx[31:34]" "vtx[39]" "vtx[41]" "vtx[46]" "vtx[48]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polySplitEdge -n "polySplitEdge3";
	rename -uid "77BACAAA-4ECD-78B6-C27F-C3ACB032CB97";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[51]" "e[63]" "e[71]" "e[408]";
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_31__pntx";
	rename -uid "8646D71A-4852-5850-C78B-30ACF4AC3EB8";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_31__pnty";
	rename -uid "BC57209D-487B-EE86-E186-FDBF30E23695";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_31__pntz";
	rename -uid "6F4A572A-4BAE-0504-01CA-B989341D9A1E";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_32__pntx";
	rename -uid "4DFCFC27-4758-49F6-D1F5-42B116A7EA77";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_32__pnty";
	rename -uid "146EC3EF-409E-319C-D821-7DA62C8E82FE";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_32__pntz";
	rename -uid "B8421773-432A-1B95-D074-80A6A31C24CD";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_37__pntx";
	rename -uid "0159C804-4FF3-F0E7-0E51-91BA92973940";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_37__pnty";
	rename -uid "21ABD929-4EF2-B0FA-3C4A-D2810342E313";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_37__pntz";
	rename -uid "2335FCDA-47A4-597F-2BD5-2FA85C41944C";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_38__pntx";
	rename -uid "FFE59BDB-45B2-3FCF-4F63-40990228AB7C";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_38__pnty";
	rename -uid "38283D44-4F0C-48AC-8E11-F7A5C1F81B0D";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_38__pntz";
	rename -uid "8D06BE72-49BF-116A-5C70-5E97138B38D3";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_39__pntx";
	rename -uid "482CE284-4E83-92CB-9B28-D8B7A543EEAF";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_39__pnty";
	rename -uid "75D34431-4564-6808-6CB7-3499F55971FA";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_39__pntz";
	rename -uid "DCD18407-46A2-9587-7B54-57BEECCA6822";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_42__pntx";
	rename -uid "67EF49A4-49FC-18FE-D6D2-FC85A9E70EA3";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_42__pnty";
	rename -uid "A9F20FDB-479B-E1B9-B5C2-EEAAD3DD14C5";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_42__pntz";
	rename -uid "8EAED351-4256-14D9-4482-A38D2E40852A";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_43__pntx";
	rename -uid "616B0C55-4863-6391-AD3C-A29C6D057BB1";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_43__pnty";
	rename -uid "C818237E-4E99-1863-FBE8-A89F76BCFB6A";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_43__pntz";
	rename -uid "B54265C1-430F-B2FE-3EFE-D790DDA249C3";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_46__pntx";
	rename -uid "D73DCFDE-4BC3-BA9E-0684-CB9280A00917";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_46__pnty";
	rename -uid "234244B4-4224-AC42-9450-B19A66A4DA09";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_46__pntz";
	rename -uid "6C73CDA8-45A3-A2A6-9F56-9BB44F5CB625";
	setAttr ".tan" 18;
	setAttr ".ktv[0]"  1 0;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "C85FFB2C-4BC8-86BC-D94B-C5BE9929897C";
	setAttr ".ics" -type "componentList" 1 "e[405:407]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak21";
	rename -uid "3963734F-44F9-B9A5-F826-56A856335824";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
createNode polyTweak -n "polyTweak22";
	rename -uid "FB948E75-4299-AFF3-FEF8-0A879E72640E";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[31]" -type "float3" 0.21998736 -12 -6.9734912 ;
	setAttr ".tk[32]" -type "float3" -0.21998736 -12 -6.9734912 ;
	setAttr ".tk[37]" -type "float3" 0 -6.5208826 0 ;
	setAttr ".tk[38]" -type "float3" 0 -6.5208826 0 ;
	setAttr ".tk[39]" -type "float3" -2.9999895 -12 -5.8735471 ;
	setAttr ".tk[42]" -type "float3" 0 -6.5208826 0 ;
	setAttr ".tk[43]" -type "float3" 2.9999897 -12 -5.8735471 ;
	setAttr ".tk[46]" -type "float3" 0 -6.5208826 0 ;
createNode polySplit -n "polySplit2";
	rename -uid "8B193709-4E99-1CD2-C25F-15AA212802A2";
	setAttr -s 4 ".e[0:3]"  0.60000002 0.40000001 0.40000001 0.60000002;
	setAttr -s 4 ".d[0:3]"  -2147483248 -2147483637 -2147483636 -2147483613;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV316";
	rename -uid "2E9E1E30-4841-E53F-B82D-DFA2C5BD76DA";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[1]" -type "float2" -3.6337344e-008 0.060838047;
createNode polyTweakUV -n "polyTweakUV317";
	rename -uid "F647C2E8-4FBD-22C9-260A-CE82262486F8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV318";
	rename -uid "6E508937-4AC4-28D8-FB83-C289AE971AA6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV319";
	rename -uid "961F288B-42EB-6F3C-8E79-A1B360FEF65B";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[7]" -type "float2" 0.060838047 -2.4389706e-007 ;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV320";
	rename -uid "7B711264-4954-7A2C-F1F6-09B99471B0A5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV321";
	rename -uid "4DFB14BC-49A3-11F6-20D6-7E8FA6AF5BD5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV322";
	rename -uid "BE65E983-44FC-DD3C-C5A9-009DE0F43859";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[8]" -type "float2" -0.0023432949 -0.0006732457 ;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV323";
	rename -uid "F3232ED7-4DF7-911A-2D20-81A4BE1FB5C3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV324";
	rename -uid "73B75AE9-45FD-FAA8-46AC-CA8D4868B90F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV325";
	rename -uid "48B5E77F-42F3-6552-CE97-C499FB6A23BD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV326";
	rename -uid "512484E1-4AC1-0A86-28B6-7EA9EC290F8F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV327";
	rename -uid "94CF5266-4FF5-B901-E2FC-32889DB8BBFC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV328";
	rename -uid "BDB57C17-4180-8FB6-BC48-9FB4CBB2C661";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV329";
	rename -uid "EABD177A-4973-8698-5518-7EBB747AF30B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV330";
	rename -uid "273D526B-4272-F9A6-1A39-DCB14BEDC884";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV331";
	rename -uid "119A4D69-4A4B-33D9-A305-C9959FE5A767";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV332";
	rename -uid "3AA3ACFB-4E69-1A54-E6DD-64B4E1D4991C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV333";
	rename -uid "CAE23557-457E-00A6-F6B4-ED87E938C6D4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV334";
	rename -uid "6A99FAF5-4CE5-28F0-31C4-93BB870CDD69";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV335";
	rename -uid "A908AF01-45EB-D933-12DA-66875F3A202B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV336";
	rename -uid "AEC8537B-455C-7441-1E6C-07ABDC9E003D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV337";
	rename -uid "D29BC5F6-445E-C12C-63BF-4BA17A7E8000";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV338";
	rename -uid "6A584478-4717-F737-8EBD-1786C4FB9210";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV339";
	rename -uid "BE141C05-4ECC-09B1-3B36-1C801EDEF925";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV340";
	rename -uid "0E9F7539-4DCE-40D3-0840-E783AF37D5D6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV341";
	rename -uid "94E81833-4D0F-CD86-2502-A2BE1CF8EC6B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV342";
	rename -uid "77240DBA-4406-7C01-0F0E-14B845A6608F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV343";
	rename -uid "00275356-461C-7DBB-0D2B-87AC97675472";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV344";
	rename -uid "4BCCD306-4306-9D66-24B7-938C700636BC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV345";
	rename -uid "64A4AF7F-4E7B-7376-A876-06B6CB051F37";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV346";
	rename -uid "4CADB282-4A1A-B257-6CD0-8E8CA9D57422";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV347";
	rename -uid "F4895889-487C-38A5-D7DE-56993FD12F25";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV348";
	rename -uid "677AA728-48DE-720D-15AE-FC9BA6FD0797";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV349";
	rename -uid "B9A1A457-4C22-2A39-557D-D689303F74BD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV350";
	rename -uid "DCB7B50F-4A5D-B4EA-3963-9289E008E024";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert14";
	rename -uid "3A073191-418A-95D0-4F51-3CBFCF976AFE";
	setAttr ".ics" -type "componentList" 2 "vtx[2]" "vtx[39]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak23";
	rename -uid "EECA136C-4F31-026C-FF14-4F85C30520C8";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[2]" -type "float3" 0 3.8146973e-006 -3.9000845 ;
	setAttr ".tk[39]" -type "float3" 0 9.5367432e-007 0 ;
	setAttr ".tk[260]" -type "float3" 0 -0.52088261 0 ;
	setAttr ".tk[261]" -type "float3" 0 -0.52088261 0 ;
	setAttr ".tk[262]" -type "float3" 0 -0.52088261 0 ;
	setAttr ".tk[263]" -type "float3" 0 -0.52088261 0 ;
createNode polyTweakUV -n "polyTweakUV351";
	rename -uid "C7EA4FA9-4E8A-B740-48A7-28B61BF7D613";
	setAttr ".uopa" yes;
	setAttr ".uvtk[0]" -type "float2" 2.6813776e-008 0.060838051;
createNode polyTweakUV -n "polyTweakUV352";
	rename -uid "1D8B3993-4043-04CF-5BC9-09AE05381898";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV353";
	rename -uid "EE150D2F-4591-D5C4-181E-02B3E7C53BB9";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[6]" -type "float2" 0.06083804 -1.7427483e-007 ;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV354";
	rename -uid "FA6E4248-44F8-453D-D91B-BEB8784BD6EB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV355";
	rename -uid "DEEC44E4-4C3A-AF41-012F-949F3CEEE340";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV356";
	rename -uid "6ED9C7BB-4975-3703-6C9A-38BE468D7A36";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV357";
	rename -uid "20DC6045-4383-A6DC-123E-838FF7BFFA02";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV358";
	rename -uid "9CF3AE85-43D9-F63C-6461-FE9AC7D4A7D0";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[9]" -type "float2" 0.0023432472 -0.00067322177 ;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV359";
	rename -uid "5E89ED55-4082-3919-8839-28B448C23C3C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV360";
	rename -uid "8837EF60-41BE-07BC-8BA8-ABA6EBC12F4E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV361";
	rename -uid "55F679B3-40DE-6203-4CF9-2CBD017D9703";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV362";
	rename -uid "159A1FD4-4929-F59B-3CC0-49A9B9C5992C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV363";
	rename -uid "8A94355B-4D7D-4C0F-3966-49B7B37115FB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV364";
	rename -uid "64797B61-4F2A-F823-252F-1683BAE0DE7D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV365";
	rename -uid "2677775C-42CA-A18D-D7B6-D9A7E116BCD4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV366";
	rename -uid "CFCC4931-4349-1DB1-75CC-D78E3E99E02E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV367";
	rename -uid "FC0CAE6F-4B30-73D7-43CA-09B4F102E88B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV368";
	rename -uid "2EEEB01F-4D95-AC4E-B603-29B8690DB49A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV369";
	rename -uid "F75C88B6-4725-E6DE-04AC-9EA9FA8D5DFF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV370";
	rename -uid "711FBF61-474E-A3B1-F1E8-5D9F3038CD1C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV371";
	rename -uid "61750442-45B1-0193-6742-CA8B8E892801";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV372";
	rename -uid "844E953E-4935-F244-D3E1-1683725AB0B3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV373";
	rename -uid "EFA12768-438A-AD36-B7BB-8CBE10CF78F0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV374";
	rename -uid "52517F4C-4318-7590-B5D4-7E8A294BD506";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV375";
	rename -uid "4408A46C-471D-53FE-0AAD-169EDE20C879";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV376";
	rename -uid "D3E6320A-46A1-CEA3-ED4F-4AA38226E39B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV377";
	rename -uid "0D4FBBA7-42A6-587B-8097-48B8CB662AC1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV378";
	rename -uid "27E4AF1B-4EC7-5A8B-B377-2589D75DC642";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV379";
	rename -uid "8C5D2765-4A2C-4110-015B-5396090B7FB4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV380";
	rename -uid "309682A4-49D5-E1A1-C4DA-559720837F3B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV381";
	rename -uid "D2FA9E3C-4DB7-8301-30F7-BA8E1326FD8B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV382";
	rename -uid "691F5FA3-4EB6-E490-DCBD-038F4C6AC090";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV383";
	rename -uid "BF3E21D5-4DE1-0398-C253-B09BE5B21852";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV384";
	rename -uid "B9A2D5B4-493F-DC7F-6751-12AF357200D5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV385";
	rename -uid "6608D5E9-44F7-39AE-9740-6099A81BB60A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert15";
	rename -uid "7EC03429-4D67-BAB9-0E47-73AFBFEE2953";
	setAttr ".ics" -type "componentList" 2 "vtx[1]" "vtx[42]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak24";
	rename -uid "C91F773B-479D-D918-1F20-CA97B51A417C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[1]" -type "float3" 0 0 -3.900084 ;
createNode polyMergeVert -n "polyMergeVert16";
	rename -uid "D079587D-41BE-7F61-1C88-3EA9EA03BE61";
	setAttr ".ics" -type "componentList" 4 "vtx[41]" "vtx[44]" "vtx[258]" "vtx[261]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyBridgeEdge -n "polyBridgeEdge3";
	rename -uid "D691C69E-483F-EA44-650F-90895210371F";
	setAttr ".ics" -type "componentList" 2 "e[1]" "e[50]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 2;
	setAttr ".sv2" 31;
	setAttr ".d" 1;
createNode polyMergeVert -n "polyMergeVert17";
	rename -uid "607DC110-402D-39D0-4877-9C9CF7942A41";
	setAttr ".ics" -type "componentList" 5 "vtx[53:56]" "vtx[241]" "vtx[243]" "vtx[247]" "vtx[249]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyTweak -n "polyTweak25";
	rename -uid "9055FEB3-44E1-0C8D-1145-03882202839B";
	setAttr ".uopa" yes;
	setAttr -s 13 ".tk";
	setAttr ".tk[86]" -type "float3" 0.077127807 0.62562466 0.11316252 ;
	setAttr ".tk[92]" -type "float3" 0.077127807 0.62562466 0.11316252 ;
	setAttr ".tk[95]" -type "float3" 0.34182554 -0.29701704 -0.2114094 ;
	setAttr ".tk[97]" -type "float3" 0.34182554 -0.29701704 -0.2114094 ;
createNode deleteComponent -n "deleteComponent10";
	rename -uid "82B12571-463D-778C-3C60-18853E0209B8";
	setAttr ".dc" -type "componentList" 1 "f[62]";
createNode deleteComponent -n "deleteComponent11";
	rename -uid "FA7C2A67-40DE-2714-6718-BBB873A97534";
	setAttr ".dc" -type "componentList" 1 "f[69]";
createNode polyTweakUV -n "polyTweakUV386";
	rename -uid "A4D20C5B-4C6C-F734-0AAE-C8ADE142CD8C";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV387";
	rename -uid "2D8FCC19-452D-C0CE-9597-07B4FC201B35";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV388";
	rename -uid "1D75803D-43C3-1132-6E65-E4A01513CE13";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV389";
	rename -uid "2AF0EC33-4B83-167A-F48E-5FA2D3EAE53D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV390";
	rename -uid "B6866B7C-4509-095F-5651-D8A234B7FBA7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV391";
	rename -uid "BBAF7B30-49D8-F6C6-BF29-D1B256DD5A9C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV392";
	rename -uid "1E7704D7-416C-6DAD-2110-3D9E48C30A9D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV393";
	rename -uid "56EE0E48-44E2-5A8B-7BCF-19A2164E1F6D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV394";
	rename -uid "C18D0FA9-4708-B778-457E-3390C52005DE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV395";
	rename -uid "EF1849C9-4678-419A-D56E-AA8AA43C7777";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV396";
	rename -uid "62FF55D6-465E-14D9-83D4-ADB3C4F8C4FB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV397";
	rename -uid "7F9D51F2-4867-7C93-657B-20B77F2CCFAE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV398";
	rename -uid "36D0BB40-445C-3B4D-2839-0CB94B94F6FB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV399";
	rename -uid "2A9BC56F-4C40-9794-AFB2-7E9DD3D5248A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" -0.0056568882 -0.0060015284 ;
	setAttr ".uvtk[6]" -type "float2" -0.0055350186 -0.0073207235 ;
	setAttr ".uvtk[8]" -type "float2" -3.1798809e-006 -0.026260916 ;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV400";
	rename -uid "81102F8F-4325-8406-D73A-FF98C5F6235D";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" 0.0040987455 -0.00016321796 ;
	setAttr ".uvtk[5]" -type "float2" 0.0036665255 0.00018492131 ;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV401";
	rename -uid "B05D83C1-4AA6-56AB-244E-C084281BB013";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV402";
	rename -uid "756A08DC-40DF-3201-E0E8-CAB42515F639";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV403";
	rename -uid "EE8346EE-43E0-7395-9CDE-70857609C876";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV404";
	rename -uid "1AE42EFF-497E-F63C-75AA-47AF20715870";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV405";
	rename -uid "3F51F780-4E01-5EA7-41A1-5E9291E8CA73";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV406";
	rename -uid "C408B4CB-4CA4-4D00-AE75-CC8F24A7E118";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV407";
	rename -uid "DA012E82-48B9-CA7D-BDF2-0E9EE78E73F0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV408";
	rename -uid "982D2694-4A1F-A59C-CFB3-A8BC613D8CF6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV409";
	rename -uid "4085715E-4B2C-8F9C-196A-57A59BBBE8BC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV410";
	rename -uid "671BAFB6-4881-4460-56DC-239064089B99";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV411";
	rename -uid "545D9310-4D78-DC53-0579-BCA99C3AE048";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV412";
	rename -uid "7FB03DA6-4909-4C54-1D0C-7FBF59F08947";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV413";
	rename -uid "337D1311-43A2-2969-7CB9-F8BDD7A58665";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV414";
	rename -uid "1D221D19-4FD1-1E8A-3672-5086B1200938";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV415";
	rename -uid "DE1B15CB-44D5-5D60-A776-48A9B3014952";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV416";
	rename -uid "9638B75C-43BF-CC73-0421-CCBBCB83E464";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV417";
	rename -uid "FEA41A1C-42F8-7466-44E8-85B79F5716B4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV418";
	rename -uid "AAF21FE3-4C31-BE04-4540-DDA1EE85C882";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV419";
	rename -uid "00B70BA5-4915-DE35-8BCB-0FBF4E2CFDDC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV420";
	rename -uid "E2B08005-4EAD-5AAF-7640-B083F7AF7831";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert18";
	rename -uid "1FDB1D83-4856-BE5F-639C-898EA88154CC";
	setAttr ".ics" -type "componentList" 4 "vtx[86]" "vtx[92]" "vtx[95]" "vtx[97]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak26";
	rename -uid "1B95126E-4C09-3F6B-921A-BDA7212986E4";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[77]" -type "float3" 0.92307615 -2.3841858e-007 -0.38461733 ;
	setAttr ".tk[83]" -type "float3" 0.92307615 -2.3841858e-007 -0.38461733 ;
	setAttr ".tk[86]" -type "float3" -0.040032864 -0.14041615 -0.015931129 ;
	setAttr ".tk[87]" -type "float3" 0 0 2.9802322e-008 ;
	setAttr ".tk[89]" -type "float3" 0 0 2.9802322e-008 ;
	setAttr ".tk[92]" -type "float3" -0.040032625 -0.14041662 -0.015930176 ;
createNode deleteComponent -n "deleteComponent12";
	rename -uid "9780674D-4EF3-1AAC-FE1E-FDB7C21D0060";
	setAttr ".dc" -type "componentList" 1 "f[60]";
createNode deleteComponent -n "deleteComponent13";
	rename -uid "8EC3397D-4E47-AEE6-7B40-94A4067E7D73";
	setAttr ".dc" -type "componentList" 1 "f[58]";
createNode polyTweakUV -n "polyTweakUV421";
	rename -uid "E4C5496B-4AD0-C979-80E4-458C6F5EEF3E";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV422";
	rename -uid "48A0542C-4576-A9ED-2352-D3866AC8A208";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV423";
	rename -uid "CE48DEC4-4339-CC64-6251-7B8161375DF1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV424";
	rename -uid "CE09D58F-4E68-73B2-5A3E-928A25924238";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV425";
	rename -uid "3A242380-4A3E-E355-A508-B8A61D1A0517";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV426";
	rename -uid "1B82FF38-488F-D9C3-F1CD-659F2207F7D4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV427";
	rename -uid "D414BEE7-4014-B552-8AC3-98A902D6ECCA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV428";
	rename -uid "2CC08309-452B-9EA2-BA40-ED89022CFE36";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV429";
	rename -uid "F80D1122-4622-89C3-96B7-0EA6B1BD8B03";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV430";
	rename -uid "19C587D0-4684-2C8E-EF88-E4833D20D5F5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV431";
	rename -uid "6F1842E1-4968-3147-7C89-FC94867EDFE7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV432";
	rename -uid "6DA03211-44EC-464E-506F-2A85F090227A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV433";
	rename -uid "4335BE5D-4ACA-5922-992A-ECB6F4BA4E7E";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" -0.0036309701 -4.2833371e-006 ;
	setAttr ".uvtk[5]" -type "float2" -0.004113107 3.6938366e-006 ;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV434";
	rename -uid "BF09B0DF-4B7A-7BC1-488B-A782CB831499";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" 0.0010325894 0.13933682 ;
	setAttr ".uvtk[5]" -type "float2" 0.0022968638 -0.15127757 ;
	setAttr ".uvtk[11]" -type "float2" 0.00048055683 0.13161317 ;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV435";
	rename -uid "39B51FBF-4CF5-6188-CD54-5AA8C74C7347";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV436";
	rename -uid "26F21D91-4443-10B2-1D5A-69B61DC067EE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV437";
	rename -uid "3B075613-414F-9EB6-D7FE-A6B0CA0D4313";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV438";
	rename -uid "6EC22C77-4DDE-D7E9-EFBD-638552CD2F08";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV439";
	rename -uid "270A8E30-47AB-7B72-CA79-E1BBFC32742F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV440";
	rename -uid "D85D53CD-4076-F4AC-DD71-24A34430863A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV441";
	rename -uid "A0D029B4-43A1-B5A7-0D98-FEAC5E863C98";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV442";
	rename -uid "470E7E26-40DC-C141-C5DC-5F9DD4AAB161";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV443";
	rename -uid "3489FA52-459A-CCFE-09D7-EF852E2047B4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV444";
	rename -uid "D2B7424D-4307-346B-7C49-B59B931C5551";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV445";
	rename -uid "209C4E29-4BFC-D67A-BB66-CE83300EF803";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV446";
	rename -uid "4DBB40F5-456A-62AB-86F8-87AB637D0CB4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV447";
	rename -uid "B10B2B56-4D38-8D6B-E387-4C97BF86A686";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV448";
	rename -uid "9A6441DC-425D-E432-E515-F19A4FAB7464";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV449";
	rename -uid "1845B53B-4F26-7970-851A-1085EC7D3D8F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV450";
	rename -uid "A4FC4035-4C79-CBF6-4654-20B07B7A03B6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV451";
	rename -uid "A62ECD15-4CD8-7201-4A97-85BE37245B50";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV452";
	rename -uid "3FBBEE32-4984-9A86-3A48-57A020647BDB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV453";
	rename -uid "10584E2C-46C2-97BF-61D4-E09B928C548B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV454";
	rename -uid "0B5711C7-4962-E05A-55D4-139BCD60E0B5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV455";
	rename -uid "047FCB78-40EA-B14C-2ADD-EBAA62DFC11D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert19";
	rename -uid "89558C19-4BA3-2F8B-CB48-01A3C75E1805";
	setAttr ".ics" -type "componentList" 4 "vtx[79]" "vtx[81]" "vtx[87]" "vtx[89]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak27";
	rename -uid "94C097D4-4C38-A1AB-F717-1CAFE70782CA";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[87]" -type "float3" 0.08062315 0.9777832 0.19349384 ;
	setAttr ".tk[89]" -type "float3" 0.080622673 0.97778368 0.19349289 ;
createNode polyTweak -n "polyTweak28";
	rename -uid "E5896036-4EDC-15C7-475D-38AC056C74B1";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[93]" -type "float3" -0.23419012 0.19861147 0.14370602 ;
	setAttr ".tk[94]" -type "float3" -0.23419012 0.19861147 0.14370602 ;
	setAttr ".tk[100]" -type "float3" 0.60349756 3.8743019e-007 -0.25145951 ;
	setAttr ".tk[102]" -type "float3" 0.60349756 3.8743019e-007 -0.25145951 ;
createNode deleteComponent -n "deleteComponent14";
	rename -uid "AA350A25-40B6-279F-DF13-C791653045E3";
	setAttr ".dc" -type "componentList" 1 "f[68]";
createNode deleteComponent -n "deleteComponent15";
	rename -uid "8BA61936-4DCD-1EC7-09E5-D98CA938D718";
	setAttr ".dc" -type "componentList" 1 "f[66]";
createNode polyTweakUV -n "polyTweakUV456";
	rename -uid "6E0F626F-42BF-F0EB-EABC-21898AE81C76";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV457";
	rename -uid "ACEC69F2-4A1D-D386-C707-37B05B897066";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV458";
	rename -uid "08DA18D5-4D6F-1B02-6848-64A9500A73FD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV459";
	rename -uid "50D994D1-4571-C7D2-56FB-EEAFD240F93A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV460";
	rename -uid "77204FE3-45FA-9E65-2BF6-B8BDED73E6B3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV461";
	rename -uid "9FAAFAA6-459A-9434-165D-A59D28E493C1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV462";
	rename -uid "AD05D5EF-440F-740E-232C-84ACC1E0CF48";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV463";
	rename -uid "B35EEFB4-4102-B008-327D-869CE30B59F7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV464";
	rename -uid "1877C0D8-49C1-8993-5627-BF8A3B8EA0B9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV465";
	rename -uid "4007DDEB-430A-37E0-D6CB-7FAEB3F75B1A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV466";
	rename -uid "FD78479A-4413-F993-B504-018F410B5FDA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV467";
	rename -uid "79F507D2-4FAE-737A-7259-91A92696BD9D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV468";
	rename -uid "0387FB5E-4BB3-C2D6-59E1-7FAF57BA46E1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV469";
	rename -uid "6BBA7339-4CAA-C49A-775B-F7BEDA083870";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV470";
	rename -uid "54010C15-415B-ECA7-BE90-E797BCF5647B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" 0.0092784241 -0.00022588279 ;
	setAttr ".uvtk[6]" -type "float2" 0.0042956197 0.00010155429 ;
	setAttr ".uvtk[8]" -type "float2" -4.928541e-007 -3.1187042e-006 ;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV471";
	rename -uid "152A0008-4E71-155F-BDFA-64969192E07D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" 2.156245e-006 0.018971581 ;
	setAttr ".uvtk[5]" -type "float2" 9.94949e-006 -0.023101538 ;
	setAttr ".uvtk[13]" -type "float2" 3.8790708e-006 0.016312707 ;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV472";
	rename -uid "84646A2E-4A25-1B26-295E-68B81917695E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV473";
	rename -uid "5508CDB5-4EA0-F37E-42FE-858E6413BF78";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV474";
	rename -uid "45D1558B-4F00-F9BB-EA09-DD80C3697AE9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV475";
	rename -uid "74358AA4-49F3-ED29-A940-B59EC616B85C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV476";
	rename -uid "B05671F3-4385-6D43-D605-0C81940C9579";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV477";
	rename -uid "D8D3E255-409F-7C1D-7BB2-C5B187C2CD4E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV478";
	rename -uid "02B4EFDF-4437-E905-A9F5-3B9C78B0EC2C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV479";
	rename -uid "D2CAE27D-4A85-ADBC-EABB-108658196D60";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV480";
	rename -uid "3319DEA5-4BE6-20C9-CD22-538AA29F854E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV481";
	rename -uid "AAB77AD4-4CBC-1919-E731-91BA4BA01E08";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV482";
	rename -uid "F1EC1FE4-4FE8-D460-BB27-3F8AF7DFAFE6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV483";
	rename -uid "1098ED38-4600-8965-5E4B-B5B55615F512";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV484";
	rename -uid "5F407575-4F5E-1501-79FE-29A2883A2646";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV485";
	rename -uid "C7F0BD3A-4DDC-A26F-1C14-88B6E398A845";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV486";
	rename -uid "0C0671A9-4C1E-0EF1-BFCC-A2AA11B19826";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV487";
	rename -uid "7331998D-410A-DD01-0354-28856B8BAB0A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV488";
	rename -uid "499A3C40-46B2-03DC-C2FB-A69B826765B9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV489";
	rename -uid "B07FE137-40A0-F5AE-18D2-5EAA2DDFA8C6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV490";
	rename -uid "071490E8-4628-8185-B655-9582A31CD042";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert20";
	rename -uid "505FE689-47C6-6727-235E-5B979ADA8C40";
	setAttr ".ics" -type "componentList" 4 "vtx[92]" "vtx[96]" "vtx[99]" "vtx[101]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweakUV -n "polyTweakUV491";
	rename -uid "E479F51D-43E2-E09F-9258-B28C1456246E";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV492";
	rename -uid "FDA2FD01-417F-9D02-44E2-E3B2F613FA89";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV493";
	rename -uid "461D9339-4E5D-8537-6671-87BC80677FFE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV494";
	rename -uid "E71DD6AF-4193-7011-F1BB-F7B97D7D0C30";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV495";
	rename -uid "AA9E45C6-4C27-93B6-1D84-798A6563B2A0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV496";
	rename -uid "3CF9E088-4CF5-ADC0-1C57-37ABBA540FDA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV497";
	rename -uid "B693E223-41BC-310A-1B19-6099114AFB71";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV498";
	rename -uid "C8C8A405-44DC-20BE-0313-1FA71CE4D356";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV499";
	rename -uid "0C545F25-4B93-6844-3055-F4B65301DEB3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV500";
	rename -uid "23477F39-4F99-1D24-F26C-3F85AB3B7501";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV501";
	rename -uid "3BC89546-4A49-F6ED-45DF-0C8606B34F3F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV502";
	rename -uid "E1733FB1-47DB-405A-C8B5-42B753089D9E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV503";
	rename -uid "3CF322D8-47E3-7102-A800-CDA41E076199";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV504";
	rename -uid "0B39451B-4FB8-F41A-778D-5489E5918452";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV505";
	rename -uid "03F142B1-4BCB-D503-8158-739ACADF01AA";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[2]" -type "float2" -0.0048327991 8.3453306e-005 ;
	setAttr ".uvtk[4]" -type "float2" -0.0062146038 -8.7127541e-005 ;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV506";
	rename -uid "7BEFC5E0-4ACE-0C8C-9EDA-CBB6107C332A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[2]" -type "float2" 0.00077445584 0.019526953 ;
	setAttr ".uvtk[4]" -type "float2" 0.00076844386 -0.016767373 ;
	setAttr ".uvtk[11]" -type "float2" 0.00023844549 0.022806913 ;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV507";
	rename -uid "3BB36DD1-4F85-D792-DF0B-ECB0CD27A89F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV508";
	rename -uid "83D06397-41F9-D979-323F-C0ADF02DB98A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV509";
	rename -uid "A3C70605-466E-4189-DE8F-73B8040D8A36";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV510";
	rename -uid "3DD96095-48D3-4C74-9BA8-559CBB251392";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV511";
	rename -uid "676722D0-4E05-3350-B715-CCAE760E74D7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV512";
	rename -uid "6B88605F-4A8C-451A-D381-E1B039A88F5A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV513";
	rename -uid "AF9DF9D2-4259-692B-9A9D-5E829AEE9188";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV514";
	rename -uid "665B7BB2-48E8-213A-7585-CB95EC564DF1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV515";
	rename -uid "FAF21B59-4EAB-C629-77A5-1DAC423FDE84";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV516";
	rename -uid "6B93646C-4D4C-1F50-DD44-AD8355A8659A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV517";
	rename -uid "B1C4BA05-42ED-682A-12D7-C0BC305A4798";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV518";
	rename -uid "CC8A3F01-4950-424C-2355-D9BBE810B247";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV519";
	rename -uid "23D0B080-44ED-7FA7-7483-FBB290002FCE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV520";
	rename -uid "3C01649D-451D-5A7D-E1F0-9CBE15F233B5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV521";
	rename -uid "BD606970-4C50-1099-87CC-1390E327A8BF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV522";
	rename -uid "5F497AF1-4FED-E642-63AC-A386FB35025B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV523";
	rename -uid "9B4180AD-427A-6A30-5F33-DD898D19949A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV524";
	rename -uid "761B0BE1-4145-AF04-2CAD-7294F9546709";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV525";
	rename -uid "31260CA2-470A-8E6C-8C41-AB90C9A2D1BA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert21";
	rename -uid "CAC3F53A-45C1-4281-7A56-B4B045D426EF";
	setAttr ".ics" -type "componentList" 3 "vtx[93:94]" "vtx[100]" "vtx[102]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak29";
	rename -uid "0CCCD43B-4E4D-6C50-0F50-E5876644DF46";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk";
	setAttr ".tk[99]" -type "float3" -4.6640635e-006 -8.6073687e-006 3.5762787e-007 ;
	setAttr ".tk[100]" -type "float3" -0.29996967 0.0030469894 0.12569427 ;
	setAttr ".tk[101]" -type "float3" -4.6640635e-006 -6.7000205e-006 1.3113022e-006 ;
	setAttr ".tk[102]" -type "float3" -0.29997063 0.0030460358 0.12569618 ;
createNode polyTweak -n "polyTweak30";
	rename -uid "5F335042-493C-7FA7-BC61-9D843410BC2E";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[98]" -type "float3" -1.1295928 0.0010155346 0.47090259 ;
	setAttr ".tk[102]" -type "float3" -1.1295928 0.0010155346 0.47090259 ;
createNode deleteComponent -n "deleteComponent16";
	rename -uid "5B4ED0CC-4A5F-2735-5B3E-DCA79C17256D";
	setAttr ".dc" -type "componentList" 1 "f[68]";
createNode deleteComponent -n "deleteComponent17";
	rename -uid "422048DE-436E-D5C4-32A1-94AD605F54AF";
	setAttr ".dc" -type "componentList" 1 "f[73]";
createNode polyTweakUV -n "polyTweakUV526";
	rename -uid "E57F500F-4CA8-DFCE-8F04-FBBD92BA1D60";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV527";
	rename -uid "92CEB099-43D1-5D7F-A55C-0B8A29C9A95B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV528";
	rename -uid "FC6224AB-4A6F-DFEF-9BB9-A99C5E6E220F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV529";
	rename -uid "06891D0D-46BD-5E62-AC94-489E203B2ACC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV530";
	rename -uid "61D07945-4C40-47AE-BFA8-A281C9F5E075";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV531";
	rename -uid "6AFAA65E-4542-AD1B-5581-3DAFC150F6E3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV532";
	rename -uid "7E361BFF-40FB-6B79-B9F3-B4BBC1D8A322";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV533";
	rename -uid "2A7426A3-441F-DEBB-6CF7-FEBD0B98DB9E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV534";
	rename -uid "3AF0257B-4657-56AB-14D5-54AC6B7646AA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV535";
	rename -uid "2DBD3AEC-4101-DD03-11B2-C5B04C9E4659";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV536";
	rename -uid "D047065E-41AF-36A4-6187-C2A09DFFDD39";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV537";
	rename -uid "68FE522F-4317-E02D-B6AA-83A76D334EB8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV538";
	rename -uid "A6FF3384-426F-133C-F3ED-49AA93E2E0A1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV539";
	rename -uid "A4BF18B7-4E4B-775D-FDD9-6496EB5452CA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV540";
	rename -uid "BDCE6A69-4F92-034F-AD72-A0ACE5729BF2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV541";
	rename -uid "B66D5FA7-4F12-6C62-22A0-5AABDEA35D38";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" -0.00015043448 0.0034516717 ;
	setAttr ".uvtk[6]" -type "float2" -0.00019176491 -0.018601781 ;
	setAttr ".uvtk[8]" -type "float2" -0.0001834758 -0.023973627 ;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV542";
	rename -uid "4E152E1D-4942-B905-AA1E-84B3F7DDB327";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 5.0464464e-006 4.8307976e-007 ;
	setAttr ".uvtk[7]" -type "float2" 1.2499081e-005 -7.8130705e-007 ;
	setAttr ".uvtk[10]" -type "float2" 3.0616143e-006 7.9726237e-007 ;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV543";
	rename -uid "F3CF101B-440A-7F66-E157-7DBBE9808A63";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV544";
	rename -uid "5CD9AB14-4951-6F4E-C189-A1BC909B726A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV545";
	rename -uid "B8E12B38-456D-8C30-FC29-889FD3D43428";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV546";
	rename -uid "F4BD7CAF-4C6A-BAF9-492E-52B13395D81E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV547";
	rename -uid "CF414D0B-415C-7557-8B53-ECB9A99BE5DF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV548";
	rename -uid "8C19F8F2-4FA0-82E9-D2CA-DAA623839ABD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV549";
	rename -uid "EEA329DC-42E8-2DF1-E1AB-B5B1BF226084";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV550";
	rename -uid "7732CE57-4F04-852F-30BE-8587C2082316";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV551";
	rename -uid "A97FF682-4170-F764-8DE6-61A6DCDF56AF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV552";
	rename -uid "04776758-43AC-5DA3-F85B-759B1B582D80";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV553";
	rename -uid "563B2B0D-4D45-A2B4-DC4E-24A651F84602";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV554";
	rename -uid "19B24F32-4687-F72C-6FAF-7E98B53D8290";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV555";
	rename -uid "947EC606-4350-D087-5B54-FBAA2376E404";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV556";
	rename -uid "39F03999-4776-0EC9-8770-05B6D872C4D2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV557";
	rename -uid "33C31336-4772-C349-66DE-8D8C71C8504C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV558";
	rename -uid "68F4A9DA-4210-09BD-0EB0-97A371132669";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV559";
	rename -uid "0747EBC6-44FF-C8D5-A649-50909C9A5934";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV560";
	rename -uid "E5AC2F0C-48F7-695B-206F-C994F8071861";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert22";
	rename -uid "44216C58-41D2-EC8A-1BFC-D08EBB9717F4";
	setAttr ".ics" -type "componentList" 3 "vtx[98]" "vtx[102:103]" "vtx[109]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak31";
	rename -uid "20EEDA5B-417C-012D-4472-6897286DF6BB";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[98]" -type "float3" 0.20651627 -0.0010156631 -0.086284637 ;
	setAttr ".tk[102]" -type "float3" 0.20651722 -0.0010156631 -0.086284637 ;
createNode polyTweakUV -n "polyTweakUV561";
	rename -uid "55B1A8C4-4EA2-4338-220F-C2A3BDAFDD1C";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV562";
	rename -uid "FCE477C6-4D18-616C-589B-74AC62A1348E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV563";
	rename -uid "C4447375-4C1C-7581-E383-8FAA59FAA19F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV564";
	rename -uid "D38353C9-4724-3FAE-0F06-F18F5C6D7A9B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV565";
	rename -uid "33C09BC1-416A-8FA0-F1FD-8E82A2C8AEC7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV566";
	rename -uid "678665CE-415B-8CB0-71C1-679C8BEBCBD0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV567";
	rename -uid "2EC53A37-48E5-9B0F-7B23-1A9CE1162AC5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV568";
	rename -uid "59F39089-4058-94A1-830E-308DFBDD0BB7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV569";
	rename -uid "A1BE45B8-4B75-D927-7D89-13AF4EC8EAB4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV570";
	rename -uid "621C4E9A-4721-BDF0-F0B2-86B4CBC27E10";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV571";
	rename -uid "FFEB0D77-4871-3C85-9586-B2B2A2B95E84";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV572";
	rename -uid "6BB9453B-401F-58F2-1BB5-26BD2583A736";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV573";
	rename -uid "DF9C85A2-4197-19A0-936C-19AAA832FA2C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV574";
	rename -uid "6FE882BB-4965-9488-FB7F-2FBBE85A1401";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV575";
	rename -uid "9A19B7A4-428B-1432-1ECD-589B75E5EF7D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV576";
	rename -uid "8511559F-4B5C-A662-D825-10B4063068EE";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -9.558883e-006 -0.0081199696 ;
	setAttr ".uvtk[7]" -type "float2" -2.9243556e-005 0.015233216 ;
	setAttr ".uvtk[10]" -type "float2" 3.5749724e-006 -0.00066387275 ;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV577";
	rename -uid "13C231D6-402A-F28A-A726-F1A29DA71C71";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" -4.1084777e-006 -0.062499121 ;
	setAttr ".uvtk[6]" -type "float2" -7.7283403e-006 0.062498599 ;
	setAttr ".uvtk[8]" -type "float2" -3.1305344e-006 -0.06249886 ;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV578";
	rename -uid "15A47EF4-422A-89AB-3776-6A86C4D13E5A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV579";
	rename -uid "5DBF2E81-49B5-0FFB-8DA7-BABCCBF8416F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV580";
	rename -uid "59FCDD4E-46E8-528C-3F1F-44863483B15A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV581";
	rename -uid "7C07C0DD-40DB-2062-88D1-05824B5CB5DB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV582";
	rename -uid "C40D0F07-49B1-2F99-8C3E-F797F2EF805D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV583";
	rename -uid "541D1E1E-4E2B-A033-C17C-C193DC47A2FC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV584";
	rename -uid "08255E67-4DC6-5E51-07D7-D1B3F5946914";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV585";
	rename -uid "AC6EB291-4B6E-5377-D347-EA9E3B14E8AC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV586";
	rename -uid "2FD446A3-405C-54BB-7BDE-329FA5B36294";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV587";
	rename -uid "52EBDA5C-45DA-ACE5-5679-BA84819FE81F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV588";
	rename -uid "AE8EA1CD-4DD4-87C4-0277-9E846B6C9579";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV589";
	rename -uid "D0003D3C-4D62-FB87-9718-038593D9FDF0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV590";
	rename -uid "37121427-48FF-F693-CD09-B99EB3457452";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV591";
	rename -uid "A502BE44-4833-CDA9-C84B-5D8FB9E242E1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV592";
	rename -uid "DBDB343C-48FB-A9DB-3F8E-61B6DAD86816";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV593";
	rename -uid "2BA49BDF-4CA0-9A49-F6E1-BCA750F601CE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV594";
	rename -uid "B7F671CB-418B-7A3F-B735-D9AD3803F7E3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV595";
	rename -uid "7B93C667-4FBC-19AA-3FEE-2FBC655DFEC4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert23";
	rename -uid "78ADBD63-48B3-8F4C-5A45-9FBEBABC9E32";
	setAttr ".ics" -type "componentList" 4 "vtx[97]" "vtx[101]" "vtx[103]" "vtx[108]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak32";
	rename -uid "5447EF49-455B-99BF-B3D0-E7A9BB2F54B1";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[103]" -type "float3" -0.080623627 -0.97778225 -0.1934948 ;
	setAttr ".tk[108]" -type "float3" -0.080622673 -0.97778416 -0.19349384 ;
createNode polyTweak -n "polyTweak33";
	rename -uid "267C2610-4CBF-AF4E-849A-B580277B98CF";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[78]" -type "float3" -1.0283754 2.9802322e-008 0.42849258 ;
	setAttr ".tk[84]" -type "float3" -1.0283754 2.9802322e-008 0.42849258 ;
createNode deleteComponent -n "deleteComponent18";
	rename -uid "65E0569B-43EA-6F0E-7647-E0A4A5E4F8F9";
	setAttr ".dc" -type "componentList" 1 "f[71]";
createNode deleteComponent -n "deleteComponent19";
	rename -uid "F4C28853-4423-4D01-D066-9AB2577130EC";
	setAttr ".dc" -type "componentList" 1 "f[57]";
createNode polyTweakUV -n "polyTweakUV596";
	rename -uid "9837C7C2-4588-B79A-17F0-9498533571ED";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV597";
	rename -uid "6EB83B2A-47A1-69E7-A5FE-99B097014EAA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV598";
	rename -uid "7692B1B2-45D7-17CF-B648-43BF40B7AF3E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV599";
	rename -uid "8488E234-4E3F-7495-18C6-B4A80E190F43";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV600";
	rename -uid "9532871C-4AE0-9FEA-186C-F795258A4CB5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV601";
	rename -uid "88199AFD-428E-F827-4A21-47A991CC0459";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV602";
	rename -uid "A6D05DB4-48B3-465D-EFE3-7FA730EA49C2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV603";
	rename -uid "AA70BA87-4685-9987-CFBA-15A895BFC69B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV604";
	rename -uid "BBB40E60-443E-FD53-E4D9-0FBE136B6F77";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV605";
	rename -uid "7BC234CB-4012-CDB8-91C8-8186117075C9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV606";
	rename -uid "C5542B72-46F4-C99F-F8ED-E181EC1D8F6C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV607";
	rename -uid "8C63C93D-4D58-C8C3-08DA-509BBC6969F2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV608";
	rename -uid "4031047A-4D5C-F119-69A4-09926AD85622";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" -0.016517011 1.3643329e-005 ;
	setAttr ".uvtk[6]" -type "float2" -0.0076328297 -5.286724e-006 ;
	setAttr ".uvtk[8]" -type "float2" 0.0016049696 -4.5660317e-006 ;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV609";
	rename -uid "5B6262EF-46D2-0399-3463-009297247D8A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV610";
	rename -uid "3F3D7D44-4FE7-7DA6-E94F-08A275B05C2C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV611";
	rename -uid "FFE55BEB-42F5-9F90-002E-9DBCC4E737B7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV612";
	rename -uid "EE8AAA51-4DA0-5C97-7BEE-AA9C1B3F89E6";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" 5.9420245e-006 -4.7918934e-007 ;
	setAttr ".uvtk[5]" -type "float2" 1.6997528e-005 7.3754802e-007 ;
	setAttr ".uvtk[11]" -type "float2" 2.6925225e-006 -7.1889389e-007 ;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV613";
	rename -uid "C7A817F5-4616-AE94-1F97-7F8FAD70C5DD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV614";
	rename -uid "DD9EE518-451F-4BC2-7604-4FB7D2237B82";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV615";
	rename -uid "7751424C-4784-2844-BE1F-C6BFEC9716B3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV616";
	rename -uid "EAA69082-4FE2-5DC7-0996-8D9BBE3C1B70";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV617";
	rename -uid "398599EB-42D0-4D2C-E2D2-DEA57FEFB929";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV618";
	rename -uid "B4C9567C-41A4-9D15-8B4D-53A9089E196D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV619";
	rename -uid "C5FFF157-47AE-A600-A33E-A1BF41F525CE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV620";
	rename -uid "C168EC57-472D-58B4-8DD9-2480CBD1B9A7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV621";
	rename -uid "9E43EFD6-4CCB-2EC9-FC11-69A66887439E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV622";
	rename -uid "9B6BA86A-4FC6-9CD2-2E7E-9F9ED540FD13";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV623";
	rename -uid "6B3FEE15-4FB0-FFAF-1ECE-3F9214F4951C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV624";
	rename -uid "5317EB67-4AB8-84B3-2BF4-2A9A7F303F7F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV625";
	rename -uid "F75CE52B-4211-E59F-8EEF-A7949384B9F8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV626";
	rename -uid "2EC164C5-4040-D14B-6068-FFB9548931A8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV627";
	rename -uid "691DAA3F-467A-0E8B-9FEB-9AB63F41D2E0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV628";
	rename -uid "3039E3D2-4B86-E8F3-EE20-B8A0B57F1001";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV629";
	rename -uid "E9012D31-4447-4132-D3C7-CAB0DF06425F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV630";
	rename -uid "A7E55289-45F3-7A97-FBDC-4084BB1E1AAD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert24";
	rename -uid "AF932C76-4DC0-93EF-28D0-E58338A6ADEC";
	setAttr ".ics" -type "componentList" 4 "vtx[78]" "vtx[84]" "vtx[103]" "vtx[105]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak34";
	rename -uid "EB5A46CB-4797-EB9A-52FB-3195C61D1B13";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[78]" -type "float3" 0.10529995 0 -0.043875694 ;
	setAttr ".tk[84]" -type "float3" 0.10529995 0 -0.043874741 ;
createNode polyTweakUV -n "polyTweakUV631";
	rename -uid "99E967AB-49FF-A249-8D48-1DB7EA98B76D";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV632";
	rename -uid "3AEDE899-4222-F1F0-2960-A2AB7AB607B1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV633";
	rename -uid "C6BA69E2-4899-4293-C045-5A8F6447BA3B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV634";
	rename -uid "54D199E8-48D0-09BC-CA1E-1EA4C14DF114";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV635";
	rename -uid "1914BC15-480B-53D6-D65B-A39883DE3EB3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV636";
	rename -uid "B95E6A25-4D7E-90F6-EC18-D3AF0E95EC52";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV637";
	rename -uid "BAA89D56-4F9E-1F7B-89AE-709D1775F1C3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV638";
	rename -uid "771DD10E-40FF-EFD5-6EB0-F5B24E7657A7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV639";
	rename -uid "A366BC3F-4894-B716-B6DA-3A934A6AFC25";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV640";
	rename -uid "3EBE192B-447C-F574-7786-609B5A1FB045";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV641";
	rename -uid "A330230E-42E0-BBE2-96FB-88AABAC60FD7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV642";
	rename -uid "9B8B5A4E-4118-40FC-5F76-8DAB573923B2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV643";
	rename -uid "32460724-4068-FD0D-37FD-9ABC480D0D9D";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[2]" -type "float2" 0.0046210526 -4.701154e-006 ;
	setAttr ".uvtk[4]" -type "float2" 0.0060315263 4.8949078e-006 ;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV644";
	rename -uid "09DEBEB0-4BA1-B3B6-AE90-8AA88AF1FFAF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV645";
	rename -uid "E327644A-4869-034F-F757-859FCFB31719";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV646";
	rename -uid "0B2CFA90-461B-3CA8-E226-A4843D7D8BC9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV647";
	rename -uid "639F5A40-4444-52EF-FF96-18AD06CC0C1F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[2]" -type "float2" -5.2809714e-006 0.062499225 ;
	setAttr ".uvtk[4]" -type "float2" -1.3663686e-005 -0.062498555 ;
	setAttr ".uvtk[9]" -type "float2" -4.4550302e-006 0.062498983 ;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV648";
	rename -uid "5914270B-4B3A-5316-FED8-91B7A5DB8F2E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV649";
	rename -uid "64085D99-497E-C09C-C728-6BABDFCF6469";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV650";
	rename -uid "0BD10CA2-4311-19FF-BF53-FAA747C26C22";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV651";
	rename -uid "02298157-4711-5AA6-A724-D692ECB1C3B6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV652";
	rename -uid "25352DD8-4987-73ED-7E5A-51AAC3639FB8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV653";
	rename -uid "FC61CBC4-46C4-A38E-2DD7-B682445668E1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV654";
	rename -uid "4CABAA05-494E-20F5-E4FD-ADB8245855E1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV655";
	rename -uid "DC2CDA19-407F-6DDA-E0F4-E5916847620A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV656";
	rename -uid "9BDC3D03-4C88-1EFB-9AFA-2F89E44B5194";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV657";
	rename -uid "8798BBC0-4D74-B14B-27B9-058BFD644601";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV658";
	rename -uid "093E1319-40F0-64AC-FDE6-2095B34252CF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV659";
	rename -uid "F433798F-4586-18FD-BBAE-799E121FBA2E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV660";
	rename -uid "C141ADFB-49B2-01C1-B1BC-AA8340AFB53F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV661";
	rename -uid "994D1513-4AA2-6388-3C6F-22B892246C57";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV662";
	rename -uid "0BB785CF-4839-FE09-7A5E-3D8AE80C5674";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV663";
	rename -uid "597E3DE6-46A7-615D-C87C-F9B09216672D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV664";
	rename -uid "41B1824E-4A98-AF8F-AD43-FDAD2FCD4DAF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV665";
	rename -uid "517B5EC4-4891-5B9A-0738-E5AF1E9794E0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert25";
	rename -uid "4E8F5EF7-467A-26B6-9B48-F4B6173F9570";
	setAttr ".ics" -type "componentList" 3 "vtx[80]" "vtx[82]" "vtx[103:104]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak35";
	rename -uid "2D66C379-49E7-DF66-83A9-8E82FC62B104";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[103]" -type "float3" 0.080622673 0.9777832 0.19349289 ;
	setAttr ".tk[104]" -type "float3" 0.080622673 0.97778368 0.19349289 ;
createNode polyTweak -n "polyTweak36";
	rename -uid "F5BD4EB7-47A7-AEC4-957C-51BEBEDDF8D9";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[103]" -type "float3" 0.99094182 3.2782555e-007 -0.41289592 ;
	setAttr ".tk[105]" -type "float3" 0.99094206 2.0861626e-007 -0.41289401 ;
	setAttr ".tk[111]" -type "float3" 1.3595655 -1.4901161e-007 -0.56648886 ;
	setAttr ".tk[113]" -type "float3" 1.359566 2.0861626e-007 -0.56648886 ;
createNode deleteComponent -n "deleteComponent20";
	rename -uid "7080B4E4-48BF-1A17-84F5-2EA0D0666B27";
	setAttr ".dc" -type "componentList" 1 "f[108:113]";
createNode deleteComponent -n "deleteComponent21";
	rename -uid "9685CBCF-44FE-A040-5B08-60BB4EEE3D60";
	setAttr ".dc" -type "componentList" 1 "f[102:107]";
createNode deleteComponent -n "deleteComponent22";
	rename -uid "0118A7F4-4A41-51FA-15B4-76BEC98E0CB3";
	setAttr ".dc" -type "componentList" 1 "f[138:143]";
createNode deleteComponent -n "deleteComponent23";
	rename -uid "CB1B5B0A-4532-1781-3B11-90A0EF550E4F";
	setAttr ".dc" -type "componentList" 1 "f[114:119]";
createNode deleteComponent -n "deleteComponent24";
	rename -uid "DEB870B7-470B-319F-ECC8-06A1D34D02A5";
	setAttr ".dc" -type "componentList" 1 "f[126:131]";
createNode deleteComponent -n "deleteComponent25";
	rename -uid "68FA6FB2-4EDC-706B-ED42-30B1964222E0";
	setAttr ".dc" -type "componentList" 1 "f[120:125]";
createNode deleteComponent -n "deleteComponent26";
	rename -uid "1C95B7BE-46E5-4C1E-3774-08AFCED3BFD8";
	setAttr ".dc" -type "componentList" 1 "f[102:107]";
createNode deleteComponent -n "deleteComponent27";
	rename -uid "70293D34-46F0-1278-8292-0782E8B768A5";
	setAttr ".dc" -type "componentList" 1 "f[108:113]";
createNode deleteComponent -n "deleteComponent28";
	rename -uid "9E5B2810-4582-6035-9911-4E879E5190BB";
	setAttr ".dc" -type "componentList" 1 "f[102:107]";
createNode polyMirror -n "polyMirror1";
	rename -uid "56D68BE4-4369-BF32-AF0D-F79EE888E381";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[53:96]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".ad" 0;
	setAttr ".mm" 0;
	setAttr ".fnf" 130;
	setAttr ".lnf" 173;
createNode polyTweak -n "polyTweak37";
	rename -uid "5F489EDB-4213-BE36-BC71-CD85F1538C13";
	setAttr ".uopa" yes;
	setAttr -s 80 ".tk";
	setAttr ".tk[47]" -type "float3" 2.1999974 0 0 ;
	setAttr ".tk[48]" -type "float3" -2.1999974 0 0 ;
	setAttr ".tk[49]" -type "float3" 2.1999974 0 0 ;
	setAttr ".tk[50]" -type "float3" -2.1999974 0 0 ;
createNode deleteComponent -n "deleteComponent29";
	rename -uid "C042150B-468D-E424-977B-B1990AD1103F";
	setAttr ".dc" -type "componentList" 2 "f[36]" "f[106]";
createNode deleteComponent -n "deleteComponent30";
	rename -uid "BF55D9D5-49D9-07C6-BCB0-8B8F05FAE0C4";
	setAttr ".dc" -type "componentList" 2 "f[36]" "f[110]";
createNode polyTweakUV -n "polyTweakUV666";
	rename -uid "CF1DDACE-4BA2-FAE7-5B12-FB831BB15594";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV667";
	rename -uid "751A30C7-43EE-9371-6327-1EBDBD133288";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV668";
	rename -uid "64BDB6F3-4E25-7FAA-DF38-BBB19811F52C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV669";
	rename -uid "2A2A858A-43F8-C84D-E17D-279EB5CC21C9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV670";
	rename -uid "A5A4380F-46A0-6A5D-42E8-E6B240D8909A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV671";
	rename -uid "B998E179-440D-4128-E747-4D9F58E01F6C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV672";
	rename -uid "9160C086-4545-19D4-323F-F1866A0B3A90";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV673";
	rename -uid "A281BD7F-4E4A-A35E-EC4A-80844DF06109";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV674";
	rename -uid "B3EBEF6F-4172-B887-9E61-C9BE3FFA61D2";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" -0.055571664 -4.4443942e-007 ;
	setAttr ".uvtk[6]" -type "float2" 0.0060111037 4.1798225e-007 ;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV675";
	rename -uid "275B3BE6-4BC9-29B5-62A0-A9960CF472A4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV676";
	rename -uid "809A6134-4B0F-0989-DD57-28B14A08E9E6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV677";
	rename -uid "B6924EA8-49D1-354C-6D90-70B75302ABC6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV678";
	rename -uid "8A1ECCAB-4AF7-B4C6-81DF-3D94AFB504D1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV679";
	rename -uid "B13DC37B-4972-68DC-F96A-CE968A3ADFEB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV680";
	rename -uid "CCEEA1D4-4B9C-62C0-EA76-239F2CE59F90";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV681";
	rename -uid "A5FC3DE9-44A0-E2CF-AB8C-F2A3E941A9EF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV682";
	rename -uid "2F21836D-461C-CAFE-9A5E-2FB4048F4038";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV683";
	rename -uid "F1D509DD-4025-F517-1A47-C1A56C426E47";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV684";
	rename -uid "986E5D97-4DC2-A05E-F0BF-2D9429BF5DE1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV685";
	rename -uid "9C06D5D3-4D50-089D-221B-D1A0E62F7EC0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV686";
	rename -uid "07677ACC-451A-6E59-620A-2491C14D0D7B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV687";
	rename -uid "A297D0FE-42DE-6CA8-DFC7-06855EA2130D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV688";
	rename -uid "C77B2C06-47F5-7CB9-9589-00B624BF6011";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV689";
	rename -uid "F526FADD-4F46-E1E6-9FB4-02814BEE4AFF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV690";
	rename -uid "D3761E33-408D-D396-4EC1-2E8C7FE0E149";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV691";
	rename -uid "52CCA8B1-4C6F-7CE9-63F7-6A99AFAC826F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV692";
	rename -uid "15A292E1-4EE1-0CE4-42E7-94B73ECFBCBA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV693";
	rename -uid "C9E49FF3-48C6-E29D-5EDC-E78B7F9CD46E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV694";
	rename -uid "902CD8F5-4BB2-3A9A-8868-7FA3433E0F62";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV695";
	rename -uid "914DF9C0-46C6-1DFD-FCCC-A2BA768C3214";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV696";
	rename -uid "7518DB2C-4B38-2F54-D2F3-FDBF1447A7C1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV697";
	rename -uid "97FDA929-4DAF-6F14-2E1F-9CAA3B01AA1C";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" -0.019917849 -0.026921958 ;
	setAttr ".uvtk[7]" -type "float2" -0.021692734 0.05107921 ;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV698";
	rename -uid "69088BB4-4BCA-D617-CD2E-5E881A85C1C2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV699";
	rename -uid "53726AA3-4DC7-0BAC-96BB-7BAF8A358894";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV700";
	rename -uid "36FFAF90-4DBD-5616-225E-218F736AB58B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert26";
	rename -uid "74E07036-4D00-321A-90AD-D8A01C7D785E";
	setAttr ".ics" -type "componentList" 4 "vtx[50]" "vtx[52]" "vtx[144]" "vtx[146]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak38";
	rename -uid "41408281-497F-85F0-0C6D-78ACD85C6F37";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[144]" -type "float3" 1.0650936 0.45878792 -0.059757233 ;
	setAttr ".tk[146]" -type "float3" 1.3420033 0.078901291 -0.11992455 ;
createNode polyTweakUV -n "polyTweakUV701";
	rename -uid "D0F14B5D-48E0-23BC-68BA-0EAB46282C20";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV702";
	rename -uid "C67F5D65-462B-1CF2-84EB-9FB4A7CDBF88";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV703";
	rename -uid "F9BF5651-4091-43F5-12BB-88AD3B142973";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV704";
	rename -uid "351AB388-462D-A8D0-D0FF-F6A03FD6B1A8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV705";
	rename -uid "5CEAA33F-48B9-F31D-F729-C4B2EEF95D69";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV706";
	rename -uid "67DAB309-4010-35D1-8019-A5A9D7E05905";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV707";
	rename -uid "5164EC9B-40EA-C0F4-A772-F88CAC0B8FB7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV708";
	rename -uid "A524D07F-4307-3A9B-601B-6EAE0BB8EEA9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV709";
	rename -uid "6D7C362B-4A1B-8F36-D5AD-3487CEFF0D71";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" 0.032418776 -6.7914016e-007 ;
	setAttr ".uvtk[7]" -type "float2" -0.0030025577 5.0598112e-007 ;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV710";
	rename -uid "238819E9-4DF0-4D37-9FF5-D5A6371E4235";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV711";
	rename -uid "311BCC73-4A43-57AA-89A0-398BCF0F4E11";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV712";
	rename -uid "B8567CAD-48A5-4D41-6AC5-61A12C49AA5C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV713";
	rename -uid "DF348993-46B0-A66C-3A20-A09FE00DC5FC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV714";
	rename -uid "B36B9403-4F4F-EFE7-F26B-F3A34BB2BB69";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV715";
	rename -uid "C3F53AC1-42A5-1533-53F6-E48855344B73";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV716";
	rename -uid "24D8CCC1-44AA-C28F-3EA3-27A3B4E716B6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV717";
	rename -uid "DB6DF205-4D9D-3E70-CDA0-5B89DBA2CD9A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV718";
	rename -uid "9A2815B5-4719-15FA-E619-8AAADF4C68B3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV719";
	rename -uid "BC8D508E-45A6-682F-8ED3-D1BDB57A0BE6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV720";
	rename -uid "1AC3CAE6-48B6-C79E-061A-BFA4889E5194";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV721";
	rename -uid "54202F76-4C89-2A00-07C4-7CB3F4C52DDC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV722";
	rename -uid "708130EE-4585-29E5-E5CC-2EBB88AFC736";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV723";
	rename -uid "3DF305A9-49F9-9F75-BABB-D39D9B1C50BC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV724";
	rename -uid "19B83EEA-4E95-79A3-B554-4981484CD296";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV725";
	rename -uid "60B8213B-4D00-2174-1AF7-27858B301955";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV726";
	rename -uid "5533E710-43E1-BBDF-6F6D-67BAB2327E54";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV727";
	rename -uid "21256826-40AD-1AE6-0AE1-8082C439F3A6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV728";
	rename -uid "067BAD5E-4892-4481-3E8F-B7AC4FD143A2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV729";
	rename -uid "156D9A22-47D7-C4E4-C1B6-57B1D6143391";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV730";
	rename -uid "D2EB249A-4536-B7FF-4937-21A885681C59";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV731";
	rename -uid "F16BEEEF-439F-D6AE-324A-55BA78B18DBA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV732";
	rename -uid "4A887523-49EB-1596-5A11-3D81EC85FB30";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV733";
	rename -uid "02DA2A63-4720-2344-4518-E89CAA7DE954";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" -0.019917849 -0.026921958 ;
	setAttr ".uvtk[6]" -type "float2" -0.021692734 0.05107921 ;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV734";
	rename -uid "F9BFCFB6-4DB4-1D9C-3E58-4A94CDA2E5D6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV735";
	rename -uid "244F67D5-46D0-07F3-EC88-0D8FAD085A68";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert27";
	rename -uid "86262812-4639-CF8C-0BC4-56B3E0FF0F54";
	setAttr ".ics" -type "componentList" 4 "vtx[49]" "vtx[51]" "vtx[150]" "vtx[152]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak39";
	rename -uid "45E48979-4646-03FA-C08E-0B945CFF399B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[150]" -type "float3" -1.0650936 0.45878792 -0.059757233 ;
	setAttr ".tk[152]" -type "float3" -1.3420033 0.078901291 -0.11992455 ;
createNode polyTweakUV -n "polyTweakUV736";
	rename -uid "DEE22F87-42FD-10E9-34A0-27A1B7AB0D53";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV737";
	rename -uid "C2287E39-4C5B-5589-F74D-3595C9A0422C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV738";
	rename -uid "279C2682-45BB-B104-DDC5-F0829BA8F787";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV739";
	rename -uid "DA2C5C5D-4FA8-02E2-46AF-13BEFC8303AB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV740";
	rename -uid "BBBD4AAB-401C-0422-94DD-3AAD33BDF61E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV741";
	rename -uid "A7A47E5E-4727-567A-1918-F58BD9E35596";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV742";
	rename -uid "85D3C01B-47E3-BBE3-024B-A2B32F91F3E8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV743";
	rename -uid "7DADF0B2-45B6-0BCE-D4E6-138DA78338FA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV744";
	rename -uid "168EEF4F-4AA4-E235-3A80-C0AB71B70B24";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" 0.025645092 4.9997476e-007 ;
	setAttr ".uvtk[2]" -type "float2" -0.05649602 5.2174204e-007 ;
	setAttr ".uvtk[8]" -type "float2" 0.0022531697 -1.4020511e-006 ;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV745";
	rename -uid "D69246EE-4D66-FD98-534D-6DAFFB5CDEA4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV746";
	rename -uid "08980FBD-4696-A531-AFB3-058B772E14EF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV747";
	rename -uid "8DFC372B-4D61-53E1-07F0-3081FB560C20";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV748";
	rename -uid "841F36A9-4AA1-0867-361D-999C71943F8C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV749";
	rename -uid "5EABDBC8-46AF-1AD2-0B25-E8948DF4B455";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV750";
	rename -uid "8019B49A-426D-3C0C-F827-5AAD6E53DDCA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV751";
	rename -uid "71F54A19-48C3-DCBC-773F-009BD37AE26A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV752";
	rename -uid "0ED6DD1B-431B-D565-5CD2-85AA2F544A54";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV753";
	rename -uid "73FB94CB-461B-9CC7-BAE9-1989161C187A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV754";
	rename -uid "C0551803-4098-204A-E75C-538CB8EE070C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV755";
	rename -uid "6BDEBDF6-436B-6857-33B1-6C96FE01FE6F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV756";
	rename -uid "5691E8AA-476F-5F93-7CA8-2DB7CD7D2A35";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV757";
	rename -uid "97731A74-463E-403F-0368-53AE0D77E3D8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV758";
	rename -uid "F4C7B1C7-4316-8F62-3020-2F8D50555F75";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV759";
	rename -uid "BE5CC043-4A33-F281-AECA-67A07EB85326";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV760";
	rename -uid "9CE730F7-48CD-0D79-7B35-0DA8F87A2B59";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV761";
	rename -uid "95B43B97-499A-C0AF-7862-598453FAD19C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV762";
	rename -uid "4BF6B14F-4C9A-B5C9-EBD0-74A1295A02DF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV763";
	rename -uid "FE2212DB-4363-EC6A-4D79-5993BCAB1D88";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV764";
	rename -uid "E56A310A-4D6C-ECC3-6D13-11952DEDCF72";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV765";
	rename -uid "0A93F514-4A83-2995-CAA3-D38FF1940909";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV766";
	rename -uid "A6B78B62-43DF-8D1F-C04D-45889458E320";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV767";
	rename -uid "B7049DFB-4E71-76C6-499F-A382165FD3B6";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[2]" -type "float2" 0.00068665162 -0.0063852058 ;
	setAttr ".uvtk[3]" -type "float2" -0.0010882574 -0.023076372 ;
	setAttr ".uvtk[9]" -type "float2" -0.0010881452 0.031819984 ;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV768";
	rename -uid "129CDBAA-4FB9-0744-7B1C-BEBC87CB832B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV769";
	rename -uid "6A2D6311-4867-D721-A55C-1EB2BA1582DF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV770";
	rename -uid "C1853902-4A58-7D8F-6F23-4889F441ECB0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert28";
	rename -uid "45048DDA-46FC-0EFC-DF58-89B85479AF72";
	setAttr ".ics" -type "componentList" 4 "vtx[46]" "vtx[48]" "vtx[140]" "vtx[142]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak40";
	rename -uid "54A33AF8-410D-289A-6AEA-6C80E5BA56AF";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[140]" -type "float3" 0.27690935 -0.39552689 0.038581848 ;
	setAttr ".tk[142]" -type "float3" -1.1920929e-007 -0.015642166 0.098749161 ;
createNode polyTweakUV -n "polyTweakUV771";
	rename -uid "801A8F1B-414A-B072-F7C7-20AEE4326409";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV772";
	rename -uid "B3E83478-448B-970D-EE26-3699C22A7B11";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV773";
	rename -uid "DB296112-4221-A8D5-B2AC-7B8B91B85164";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV774";
	rename -uid "F72366A8-4D66-4E12-1B5B-EEA732D4A6CB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV775";
	rename -uid "D1AFF3A5-4053-560B-E749-BFB217E70C39";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV776";
	rename -uid "A27FDE89-4194-E43B-9808-5F90FD2ABA30";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV777";
	rename -uid "152714CC-458F-603A-83CE-5C9204B5B9D7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV778";
	rename -uid "A8EB65F5-4F08-CB4C-490F-4AAD5BBCFF26";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV779";
	rename -uid "C8CDFC7C-4FDD-E288-C541-B5B23104936F";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.014870503 4.4266707e-007 ;
	setAttr ".uvtk[3]" -type "float2" 0.031807419 6.4115562e-007 ;
	setAttr ".uvtk[9]" -type "float2" -0.0016898633 -1.7290133e-006 ;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV780";
	rename -uid "3D89FDB6-46E9-1A0D-AE62-16BA5319C4F1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV781";
	rename -uid "948CD3DC-48C8-B95A-A741-33AA3625F0B9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV782";
	rename -uid "FF767556-4281-348B-FC98-A380368DB751";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV783";
	rename -uid "35FD882C-4294-36C4-E54E-D38BF20C8A5F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV784";
	rename -uid "F515961C-4672-3D08-0BDE-E99521268FCB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV785";
	rename -uid "D461B8E0-484D-5D36-0DA3-018CAE891A87";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV786";
	rename -uid "108924DD-431D-2ECF-87B9-BDB969DAF1AD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV787";
	rename -uid "0E53B395-41FE-BCAC-9181-9D8FCD5165F7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV788";
	rename -uid "1BA88AB3-414D-A992-C416-E5A087BCC464";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV789";
	rename -uid "A09F4874-402D-971D-F236-849899D63854";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV790";
	rename -uid "D49E976C-472B-4A56-C0CD-CC88C8BE0C29";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV791";
	rename -uid "B77CDDAE-4263-F063-E364-15A61224B3BB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV792";
	rename -uid "8C2456F2-4FD8-2D20-C477-34AC615EA7D7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV793";
	rename -uid "CD4AEBB3-453C-C82C-86F6-8CB6DA8BAC29";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV794";
	rename -uid "52D8BF98-46CD-5184-5B51-B5966E733CBE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV795";
	rename -uid "49B20B44-4B8C-27EC-6048-B6863CD68B23";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV796";
	rename -uid "9324216B-47A8-134A-0EA0-4C92CC90BBF6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV797";
	rename -uid "F13EB08E-4AD3-10CE-E106-70843CBA7A50";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV798";
	rename -uid "999BB2C1-4DC3-7241-849A-0DB8D4B0EEA8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV799";
	rename -uid "49A8944B-4F34-A0E2-E808-5698E5918097";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV800";
	rename -uid "E5EC8E3E-448B-A4B8-8922-E5A28BB0F344";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV801";
	rename -uid "1AB78F88-4BE8-7531-4B99-32A25934FE7D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV802";
	rename -uid "46539EEB-48EA-4CD8-F1FB-C8A49B8AB7D2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV803";
	rename -uid "BC9A5F32-4491-3B95-CF28-F0B2AEFD8E24";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" -0.0010882574 -0.023076372 ;
	setAttr ".uvtk[2]" -type "float2" 0.00068665162 -0.0063852058 ;
	setAttr ".uvtk[8]" -type "float2" -0.0010881452 0.031819984 ;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV804";
	rename -uid "CA0EA40A-4B7D-A163-357B-B8A21A61208C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV805";
	rename -uid "B3B3936F-44EF-AF1A-F995-FFB96852BA1E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert29";
	rename -uid "E74CBF13-42A8-B431-5941-4C9F93E9ED8B";
	setAttr ".ics" -type "componentList" 4 "vtx[45]" "vtx[47]" "vtx[144]" "vtx[146]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak41";
	rename -uid "DC076D8A-4D61-BEE6-92C7-278E73726E31";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[144]" -type "float3" -0.27690935 -0.39552689 0.038581848 ;
	setAttr ".tk[146]" -type "float3" 1.1920929e-007 -0.015642166 0.098749161 ;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "F1C44546-403C-2C30-5AA9-55BA1ABB2570";
	setAttr ".ics" -type "componentList" 2 "f[32]" "f[35]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -21.169222 10.827381 ;
	setAttr ".rs" 55482;
	setAttr ".lt" -type "double3" 0 5.7037707890117417e-015 0.34612517404185883 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.5 -23.638442993164062 8.9547624588012695 ;
	setAttr ".cbx" -type "double3" 3.5 -18.700000762939453 12.699999809265137 ;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "E7F60793-4BCA-F6BD-3CF9-99903FED47F9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[119]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.099999999999999978;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode deleteComponent -n "deleteComponent31";
	rename -uid "30C32D43-401C-B1C4-EFCF-A6A2332AF587";
	setAttr ".dc" -type "componentList" 2 "f[174:175]" "f[178]";
createNode polyTweakUV -n "polyTweakUV806";
	rename -uid "66093A87-49BD-06C2-3841-16BFB104B968";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV807";
	rename -uid "0F70F796-4590-28C9-1C9A-9CB97504F8BF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV808";
	rename -uid "A68866FD-4B75-DAF9-491D-8D9FB4AD3CBE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV809";
	rename -uid "619CADD1-4356-01E7-3706-4AA4926F0880";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV810";
	rename -uid "FEC650F9-4525-3F5A-61F5-EEB095D0A450";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV811";
	rename -uid "F3339C23-417C-09A7-E333-ECB2B18A0A25";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV812";
	rename -uid "619EA606-4A5D-04F6-66D3-E29535A7E9BD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV813";
	rename -uid "AA9993C4-42F0-0CE0-3FD7-C48622BE02B4";
	setAttr ".uopa" yes;
	setAttr ".uvtk[0]" -type "float2" 7.3932355e-008 1.3887484e-006;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV814";
	rename -uid "FFC6690A-4C37-C8D5-8C17-1FB6909087C7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV815";
	rename -uid "DB7CE3A1-401E-C5B1-152B-1E950FA8165A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV816";
	rename -uid "0486FC06-46EA-FD45-39CD-A49D45F31EAC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV817";
	rename -uid "58EC6FDC-4EDD-6EA7-865D-1FB488B03421";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV818";
	rename -uid "83A5D1CD-4708-C749-344C-17A482FEE2A8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV819";
	rename -uid "8E64D364-458B-B33F-DF0B-DFAA546140E9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV820";
	rename -uid "AFC75A1C-449F-CDDD-3BDD-08B8095FDEF3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV821";
	rename -uid "B9DFA687-4322-B13B-0975-38AB544A7F90";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV822";
	rename -uid "A275872D-46F5-8442-B88D-B79ADD82C98B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV823";
	rename -uid "BE43F712-4E14-70A0-07D6-7B8D4D9816A9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV824";
	rename -uid "F8C8860E-4F37-7F5B-ADC8-43ABD1476F1A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV825";
	rename -uid "2C32FADB-43AA-11FE-6737-5BAFEF22BF9C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV826";
	rename -uid "79AE5C08-4DBF-05A4-183E-2BAE131DD994";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV827";
	rename -uid "36670048-4474-991C-A0A5-05BB524D7574";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" -0.1498203 -0.00035812493 ;
	setAttr ".uvtk[10]" -type "float2" 0.0014321894 -0.15387532 ;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV828";
	rename -uid "85F96CE7-4954-A5B2-D7C5-429FF59E271F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV829";
	rename -uid "B525AAFF-4B03-2F46-8A42-E8927E31AB6E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV830";
	rename -uid "19A35DDF-46A2-9DFF-F1EE-3E8EE2B2BD43";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV831";
	rename -uid "60969098-4B14-CB76-24EB-8D9031EBF3B2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV832";
	rename -uid "10A551CB-4D1D-726B-9668-E688A09B5399";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV833";
	rename -uid "4506F10B-4D5B-841B-ACF4-6FA4D5EA91E8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV834";
	rename -uid "A34604C9-426F-6D84-AA81-20BCF48B3FD3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV835";
	rename -uid "ABCF7F5C-4AFF-5B8A-BA02-C588F7E0FDA0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV836";
	rename -uid "6F3A144C-4C7F-7DAD-B937-46ADB7E0C654";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV837";
	rename -uid "F9E9CDED-4BF3-BE88-C9D5-89B6253D35AB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV838";
	rename -uid "5E7640A8-4D1C-1F71-6E65-DDBB8D1A6279";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV839";
	rename -uid "63CC68F2-426B-8D5E-3FCA-7CBD8FE83F0C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV840";
	rename -uid "7C61289A-4A43-C3EB-E965-67B52B6015DD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert30";
	rename -uid "BC084BB5-487B-B332-9474-958DF9534732";
	setAttr ".ics" -type "componentList" 2 "vtx[135]" "vtx[157]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak42";
	rename -uid "DC677C05-4030-9008-21BE-8A810B77A6DD";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[135]" -type "float3" -0.45906258 -0.11039782 1.1017456 ;
createNode polyTweakUV -n "polyTweakUV841";
	rename -uid "B1062A5D-46D5-B81F-6F79-44B1E090E408";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV842";
	rename -uid "5AE8C5DB-46FE-D116-7B9C-D081E84A68C9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV843";
	rename -uid "69919CE7-4E19-432F-99AB-0AB0D406CD5C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV844";
	rename -uid "D08FE744-4225-816A-B56D-E9A0465F37AB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV845";
	rename -uid "066240E5-4B80-116A-9349-5F9A3E07F983";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV846";
	rename -uid "EF988306-41B4-36C9-30EC-3C9BE8CF2964";
	setAttr ".uopa" yes;
	setAttr ".uvtk[0]" -type "float2" 3.1249203e-006 1.3590889e-006;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV847";
	rename -uid "6D258FBF-4729-B5B9-6512-899F5B51801D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV848";
	rename -uid "EF28BA92-4E45-8ECC-3F5B-659CECAF7737";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[1]" -type "float2" -5.8833724e-008 1.7361281e-006;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV849";
	rename -uid "2F5D0E9A-472A-FB3F-2367-38A0EBD934D7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV850";
	rename -uid "54809346-44AD-25C6-3BAC-1BBB4AC41D0F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV851";
	rename -uid "5CF4BA63-460B-A8B4-028E-1094C90620E1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV852";
	rename -uid "8C7819A6-4EC1-7FDE-7CB3-C794824687DC";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[15]" -type "float2" -0.0069644055 -0.077458814 ;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV853";
	rename -uid "63D194F1-440D-7739-EF73-F099E826F825";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV854";
	rename -uid "4D91F064-4CD5-FA50-8203-A29C734D74E9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV855";
	rename -uid "1AB2749D-4809-A5AB-0B99-F5B03E7D581B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV856";
	rename -uid "0F40D06A-45D5-8D0A-1CE3-088BC90287F7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV857";
	rename -uid "B26B92CF-4BDA-2E44-1F0C-91B0E59F9169";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV858";
	rename -uid "67346EE6-42BB-FB74-65C6-409B54E5F8BD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV859";
	rename -uid "0E91EC00-4C3E-396F-E93D-2485C9BBA7B4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV860";
	rename -uid "CB48CC60-47A9-6B1B-3B4A-759B378C9F89";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV861";
	rename -uid "595D9EC4-4CDC-41B2-C9BA-1AB61BD266DF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV862";
	rename -uid "9E8E1B14-4916-2D89-A5B6-EC9A94989979";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[8]" -type "float2" -0.0045397342 -0.14242867 ;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV863";
	rename -uid "A2CDA0C0-40A7-3485-BC2D-85A0A9DDD605";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV864";
	rename -uid "752FB344-4640-DFC7-ED94-6690193802A9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV865";
	rename -uid "26D7D0BE-4094-899F-1DAF-4C861B4162F7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV866";
	rename -uid "BBAE7890-44EC-BA31-629E-35800D77CE2D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV867";
	rename -uid "6FAA0B3C-4051-AFE8-15D0-A3AF4C3FEC08";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV868";
	rename -uid "41D136BF-4F24-8D4B-C617-91B4685F1E2D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV869";
	rename -uid "104C0F6B-4844-8D17-555D-31A0AD254E63";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV870";
	rename -uid "D2F0B33D-4CA8-E098-AE81-489779E08679";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV871";
	rename -uid "C72F0D2C-423C-3B8E-C311-06B599321A4D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV872";
	rename -uid "0E6E5343-4417-8FC5-37EA-A2A6EDC0BC9E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV873";
	rename -uid "2C4BC57A-4183-CEAD-7208-45A02A39037D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV874";
	rename -uid "D9037E47-49F4-493E-B470-7287344DB973";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV875";
	rename -uid "3D6B02E6-4EE2-DAB0-89CD-03B181EB2CFF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert31";
	rename -uid "26CE80F3-4254-8777-4E2A-0DA04E756D4A";
	setAttr ".ics" -type "componentList" 2 "vtx[155]" "vtx[226]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak43";
	rename -uid "7CCB2B00-43C0-08D9-B6D4-E78424C8A311";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[226]" -type "float3" 0.00020945072 -0.1103971 1.1465826 ;
createNode polyTweakUV -n "polyTweakUV876";
	rename -uid "42E6E0F0-4D29-F34A-23A8-DCB49C23A48C";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV877";
	rename -uid "68D467D6-4452-CBB5-0779-43A6823DFFE7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV878";
	rename -uid "BBF8854C-4117-EFD0-28F2-7F9E140E3145";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV879";
	rename -uid "ADEA489F-45AF-D738-79A4-90A39EB4E502";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV880";
	rename -uid "2A76A4FF-456A-CFA7-F0A3-6EA13C19C699";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV881";
	rename -uid "2962C41B-4AFC-891E-92F8-A7A58DF2ECB0";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[1]" -type "float2" -2.3426101e-006 1.7585622e-006;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV882";
	rename -uid "1D1F77E4-4E8B-3106-B843-03BDC4B4E716";
	setAttr ".uopa" yes;
	setAttr ".uvtk[0]" -type "float2" 7.4229355e-008 1.4187806e-006;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV883";
	rename -uid "EBB1A616-4B77-F9E7-23DA-F58EBEAF828B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV884";
	rename -uid "98280AB3-4562-0875-EFCA-98A31C3FC423";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV885";
	rename -uid "389C0FA6-4D1E-AFDC-B6F5-40A500B058F7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV886";
	rename -uid "66FF49FF-43BD-0D27-CD99-6F97215D201D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV887";
	rename -uid "B790DBA5-46AF-8055-EDE2-9B9DFA34571C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[11]" -type "float2" -0.0074302284 -0.12908658 ;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV888";
	rename -uid "133855D5-4506-64D8-058D-63BEB3738D4B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV889";
	rename -uid "06293274-4E22-10A5-1233-92B8F5A6FAE8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV890";
	rename -uid "8380EA40-406D-FABC-7471-D9A770ED909D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV891";
	rename -uid "01768375-41F8-5FFF-D26D-368AE1BC6335";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV892";
	rename -uid "AE768B55-47CB-5406-E36D-EFB5F4BD6609";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV893";
	rename -uid "005063C5-4D1B-054B-9DA3-9C81E8A60FE7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV894";
	rename -uid "2CF6CD1B-4AE6-8E56-22B4-A49B078845B1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV895";
	rename -uid "9B8811DB-4549-5576-1FDD-1BAFD5A63E4F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV896";
	rename -uid "9B3DD45F-44C7-34E5-AACB-5DABBF9DAA0F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV897";
	rename -uid "15E73209-4210-8C4B-383A-39894830951F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV898";
	rename -uid "306C75B5-4854-E21E-CC7D-25B1E877FF2F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV899";
	rename -uid "ED62E41D-41F3-A00E-F95A-CAAD4C800AF6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV900";
	rename -uid "59C6B55E-4B9A-13FA-C42A-9C9B568FE41C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV901";
	rename -uid "B215B6B7-4D38-186A-5DD2-AFAACCD49AE9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV902";
	rename -uid "2004ACB0-4F82-E6DC-F79C-2BADD18ED8EC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV903";
	rename -uid "DBA4D527-4C80-6091-E92A-5084D48A995C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV904";
	rename -uid "522628F3-45D5-E333-645D-3AB52FDDB93B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV905";
	rename -uid "08B863A3-4B2B-DEDD-A2EF-4E92F0A71917";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV906";
	rename -uid "771BE0BB-4573-4151-7D2B-5A8B25D6A795";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV907";
	rename -uid "1B7A8D6E-4192-3890-5CC1-2FA240EFDF20";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV908";
	rename -uid "A2CF6B92-42E9-D05B-D326-E1AC0911DECC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV909";
	rename -uid "5C997EEF-46AB-C5A8-D4C5-D692A673FDAF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV910";
	rename -uid "40541381-42A7-8D8F-C2EF-ED861B3DC3CF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert32";
	rename -uid "0811020C-422B-5F0D-31AF-3C86F27CB26B";
	setAttr ".ics" -type "componentList" 2 "vtx[156]" "vtx[226]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak44";
	rename -uid "47AB0410-4DA8-2E33-32E9-82BF36862AEE";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[226]" -type "float3" 0.0015195608 -0.11039567 1.1458406 ;
createNode polyTweakUV -n "polyTweakUV911";
	rename -uid "2719CB9A-481E-062E-683A-FB8F0E70E425";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV912";
	rename -uid "9C313046-4251-2093-E3FE-18B8FB613871";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV913";
	rename -uid "6D53CA84-401D-149F-5BB2-ED8B64D94C44";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV914";
	rename -uid "6FBA704B-4EBC-C231-5FE0-C69935DADFF2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV915";
	rename -uid "26E71E12-4A3A-137B-8DF1-4FB62C6A94C2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV916";
	rename -uid "3DD4C251-4974-8415-2F61-578A8CBDED6B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV917";
	rename -uid "28AB1E84-4E5F-41F1-1F58-54A4D0530B47";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[1]" -type "float2" -5.858956e-008 1.7139804e-006;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV918";
	rename -uid "13E586C0-4324-6E8B-C715-C39C8E0DB41C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV919";
	rename -uid "7D98F38D-425C-BF07-E656-73984E168969";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV920";
	rename -uid "67CE5061-46B8-EC6D-14BD-6293F1C60A4D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV921";
	rename -uid "8CF1EE4C-47DB-304B-C723-8FABDC1678C9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV922";
	rename -uid "65454B6F-4563-9124-62DE-FC8AA15EB7AA";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" -0.14982024 -0.00035811978 ;
	setAttr ".uvtk[9]" -type "float2" 0.0010633882 -0.15182079 ;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV923";
	rename -uid "C63EA03B-4756-8CBF-4C0E-40A831E9655E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV924";
	rename -uid "5CAB5B33-4E3F-2C18-461B-5191F340C1DC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV925";
	rename -uid "850B1D93-40A7-E740-16A7-03AA8D517C19";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV926";
	rename -uid "E9FDF31E-4BB9-4961-0522-4BA09B8119BB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV927";
	rename -uid "77FDD6E8-4E2F-B4ED-F01E-6E8A48D7E2DC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV928";
	rename -uid "E80F04E9-4DC0-3C75-307C-4E8471092D12";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV929";
	rename -uid "F84DBCAA-4FB9-821D-A0D2-7BA62DB3D734";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV930";
	rename -uid "150AF663-4D8B-6F4E-5C08-9884B8AFE165";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV931";
	rename -uid "38069D4A-4FE3-E9C9-FA3F-079AE5862389";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV932";
	rename -uid "66EAC061-4E54-DCF2-AA21-C5A99B5E2795";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV933";
	rename -uid "C32DB7EB-4E82-20DA-132E-E388637C999F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV934";
	rename -uid "5C6C8B6C-4BE3-01C6-1F6E-508FE1691F02";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV935";
	rename -uid "D6F653E2-4C68-9C06-C767-34B0F3A385FB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV936";
	rename -uid "DB0A4DA1-445E-6096-EA23-BABEB4CB6BB5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV937";
	rename -uid "0A7EC528-451B-5D15-3B0C-16AA683C7ADF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV938";
	rename -uid "C797CF0C-4B70-45B4-5DF4-F3A8BA8DE90F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV939";
	rename -uid "1EEDBF93-4B26-5997-95B9-C28B57D11CBB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV940";
	rename -uid "3D66E53B-4C34-2273-2DDE-F09392E1C5B0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV941";
	rename -uid "FF7D116E-412D-0DBC-E7BB-228188C8BC1D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV942";
	rename -uid "EFE0478E-4A1B-FCF8-82A3-268801B4BBE9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV943";
	rename -uid "3BA57A54-4941-A4C7-3ED6-F8AA79D3FB2B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV944";
	rename -uid "4FE40ECE-40B8-3EBC-241E-B084D11A7DE0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV945";
	rename -uid "7A016B72-4D93-F889-85C5-3498FC16BA50";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert33";
	rename -uid "40A6DF35-448D-3AB4-73FA-27B2084B0B2D";
	setAttr ".ics" -type "componentList" 2 "vtx[72]" "vtx[154]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak45";
	rename -uid "70C1D272-4418-EEED-6AF7-8FA2E7523B16";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[72]" -type "float3" 0.45906258 -0.11039758 1.1017451 ;
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "84A7E9BC-4C11-424F-48C5-F19E2CDFE01E";
	setAttr ".ics" -type "componentList" 3 "e[13]" "e[25]" "e[259]";
createNode groupParts -n "groupParts2";
	rename -uid "5C7906EC-4731-8D92-948B-BF8C34111EB5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:176]";
	setAttr ".gi" 88;
createNode polyCloseBorder -n "polyCloseBorder2";
	rename -uid "A63CC78D-403B-70C3-5F53-358E2BE19B0C";
	setAttr ".ics" -type "componentList" 3 "e[14]" "e[34]" "e[256]";
createNode groupParts -n "groupParts3";
	rename -uid "37F70702-41A2-34DD-C051-C59A06669F5F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:177]";
	setAttr ".gi" 89;
createNode polyCloseBorder -n "polyCloseBorder3";
	rename -uid "5544777D-40C0-A95F-3F8A-538B1AE5DB3E";
	setAttr ".ics" -type "componentList" 3 "e[18]" "e[37]" "e[257]";
createNode groupParts -n "groupParts4";
	rename -uid "3081234C-48D2-9A8C-E24F-8396EE3F81CB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:178]";
	setAttr ".gi" 90;
createNode polyCloseBorder -n "polyCloseBorder4";
	rename -uid "073CBD2F-4B57-9CD7-B830-B6BC80680925";
	setAttr ".ics" -type "componentList" 3 "e[17]" "e[27]" "e[258]";
createNode groupParts -n "groupParts5";
	rename -uid "FF430DCE-4BD5-B1F9-0D8B-069EA758A722";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:179]";
	setAttr ".gi" 91;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "58C7008A-49BE-00AC-EBB1-5CB204E8D43F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[11]" "e[263]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak46";
	rename -uid "C6E702FF-45AB-4663-B17F-70BD47660FB6";
	setAttr ".uopa" yes;
	setAttr -s 54 ".tk";
	setAttr ".tk[45]" -type "float3" 0 1.9073486e-006 4.7683716e-007 ;
	setAttr ".tk[46]" -type "float3" 0 1.9073486e-006 4.7683716e-007 ;
	setAttr ".tk[47]" -type "float3" 0 1.9073486e-006 1.1920929e-007 ;
	setAttr ".tk[48]" -type "float3" 0 1.9073486e-006 1.1920929e-007 ;
	setAttr ".tk[49]" -type "float3" 0 2.8610229e-006 -3.5762787e-007 ;
	setAttr ".tk[50]" -type "float3" 0 2.8610229e-006 -3.5762787e-007 ;
	setAttr ".tk[51]" -type "float3" 0 1.9073486e-006 -7.1525574e-007 ;
	setAttr ".tk[52]" -type "float3" 0 1.9073486e-006 -7.1525574e-007 ;
	setAttr ".tk[53]" -type "float3" -2.4738256e-010 0 0 ;
	setAttr ".tk[54]" -type "float3" 2.4738256e-010 0 0 ;
	setAttr ".tk[55]" -type "float3" -2.4738256e-010 0 0 ;
	setAttr ".tk[56]" -type "float3" 2.4738256e-010 0 0 ;
	setAttr ".tk[57]" -type "float3" -2.4738256e-010 0 0 ;
	setAttr ".tk[58]" -type "float3" 2.4738256e-010 0 0 ;
	setAttr ".tk[59]" -type "float3" -2.4738256e-010 0 0 ;
	setAttr ".tk[60]" -type "float3" 2.4738256e-010 0 0 ;
	setAttr ".tk[69]" -type "float3" 0 0 0.19325992 ;
	setAttr ".tk[70]" -type "float3" 0 0 0.19325992 ;
	setAttr ".tk[73]" -type "float3" 0 0 0.19325992 ;
	setAttr ".tk[74]" -type "float3" 0 0 0.19325992 ;
	setAttr ".tk[133]" -type "float3" 0 0 0.19325992 ;
	setAttr ".tk[136]" -type "float3" 0 0 0.19325992 ;
	setAttr ".tk[137]" -type "float3" 0 1.4901161e-007 -2.3841858e-007 ;
	setAttr ".tk[139]" -type "float3" 0 1.1920929e-007 1.6391277e-007 ;
	setAttr ".tk[140]" -type "float3" 0 0 3.5762787e-007 ;
	setAttr ".tk[141]" -type "float3" 0 1.4901161e-007 -2.3841858e-007 ;
	setAttr ".tk[143]" -type "float3" 0 1.1920929e-007 1.6391277e-007 ;
	setAttr ".tk[144]" -type "float3" 0 0 3.5762787e-007 ;
	setAttr ".tk[145]" -type "float3" 5.7276338e-008 0 0 ;
	setAttr ".tk[146]" -type "float3" 5.7276338e-008 0 0 ;
	setAttr ".tk[147]" -type "float3" -1.071021e-008 0 0 ;
	setAttr ".tk[148]" -type "float3" -1.071021e-008 0 0 ;
	setAttr ".tk[149]" -type "float3" -5.7276338e-008 0 0 ;
	setAttr ".tk[150]" -type "float3" -5.7276338e-008 0 0 ;
	setAttr ".tk[151]" -type "float3" 1.071021e-008 0 0 ;
	setAttr ".tk[152]" -type "float3" 1.071021e-008 0 0 ;
	setAttr ".tk[216]" -type "float3" 0 1.9073486e-006 0 ;
	setAttr ".tk[217]" -type "float3" 0 1.9073486e-006 0 ;
	setAttr ".tk[218]" -type "float3" 0 1.9073486e-006 1.1920929e-007 ;
	setAttr ".tk[219]" -type "float3" 0 1.9073486e-006 1.1920929e-007 ;
	setAttr ".tk[220]" -type "float3" 0 1.9073486e-006 2.3841858e-007 ;
	setAttr ".tk[221]" -type "float3" 0 1.9073486e-006 2.3841858e-007 ;
	setAttr ".tk[222]" -type "float3" 0 -2.3841858e-007 2.3841858e-007 ;
	setAttr ".tk[223]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[224]" -type "float3" 0 -2.3841858e-007 2.3841858e-007 ;
createNode polyBevel3 -n "polyBevel3";
	rename -uid "3FB6C556-405D-3D31-8BE6-5BA780050004";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[9]" "e[259]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweakUV -n "polyTweakUV946";
	rename -uid "2ACD9734-4D76-6DDA-B9E5-37BC4804097A";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV947";
	rename -uid "5E4A3CA0-4227-FDE2-1F1B-EAADD166F8E2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[8]" -type "float2" 0.0087885866 0.10699124 ;
	setAttr ".uvtk[9]" -type "float2" -0.016115146 0.030885449 ;
	setAttr ".uvtk[12]" -type "float2" 0.0036211105 -7.449305e-007 ;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV948";
	rename -uid "97744C13-46DF-BFC9-DC52-3AA308704BC4";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" 0.0036237487 7.3565496e-007 ;
	setAttr ".uvtk[9]" -type "float2" 0.0065160873 -0.12501107 ;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV949";
	rename -uid "9D7D770D-453B-3BAC-F572-D8A26660F6B1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV950";
	rename -uid "A1ED6B18-4E98-95D5-19A9-02BA06CB7321";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV951";
	rename -uid "FCBC027B-4AA2-C13B-6AAE-468A773808CA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV952";
	rename -uid "6A6E5ADE-43A4-E5C8-E130-61BDC4A02476";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV953";
	rename -uid "C1BF2FE0-4884-04AA-998A-B0AD89BBC98C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV954";
	rename -uid "851B4807-44E5-A6B1-31B5-2D9B790C71D3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV955";
	rename -uid "36B9E875-46F4-4600-FDCC-F9A6F01DCAD3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV956";
	rename -uid "F8AE0D9A-4385-1A9A-FC33-D2B10DCEF95A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV957";
	rename -uid "1EDA449C-4C6E-AFED-F296-F1A652F50001";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV958";
	rename -uid "C6D179C4-4D86-7F5F-65BA-13AEA67C8A2F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV959";
	rename -uid "8F9E5AE4-406A-BA34-AC28-ED80B5C754DD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV960";
	rename -uid "85EB6F12-49E0-59BE-ED86-D39F65D48B95";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV961";
	rename -uid "10A77937-475A-F3D3-CF9A-82AA384BD701";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV962";
	rename -uid "E104505D-44A8-8165-C5C4-38986A4FEED5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV963";
	rename -uid "D634A59E-4EAF-762F-BC5F-5880F5150FE7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV964";
	rename -uid "F4E36EE1-4C0B-C4BD-387F-FE9D72661CC4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV965";
	rename -uid "5CCB0C24-44CC-AA22-85F3-4A9950E45A7C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV966";
	rename -uid "BDF60A18-4076-9EA9-5642-4AA91A975A8B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV967";
	rename -uid "60EEDC85-46DA-62ED-04EC-CCA6EA8233D1";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV968";
	rename -uid "E4F7ACE4-46D2-3747-AFCC-0C979EEB1D66";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV969";
	rename -uid "5382C0E8-4513-766D-2B1D-9FA01F48358C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV970";
	rename -uid "00DBC5AB-458E-5FA1-7B76-298F1F47017B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV971";
	rename -uid "D7AA5530-49BC-2D9B-6E46-D78871917634";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV972";
	rename -uid "83E1CEE8-4704-2312-48B2-89AA4C6992E3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV973";
	rename -uid "9EA8DCDE-4BF8-994A-934D-899C211E3E0C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV974";
	rename -uid "6BD708D4-4D3E-9E9E-2740-8583F3A0B5F3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV975";
	rename -uid "AB8DD113-45D5-7EB0-32A4-4FB766079BAE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV976";
	rename -uid "E90D6471-41E7-3466-57C0-978A6201E1C9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV977";
	rename -uid "D3826998-4E0B-2BBC-B973-B395C0C86C01";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV978";
	rename -uid "7DAB5EDB-4C69-6867-45C9-1B946C3F0B39";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV979";
	rename -uid "940FBEDE-4961-ABC5-B99E-F58152848845";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV980";
	rename -uid "B40B8F50-444B-C401-21E1-FF80AD198EF3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert34";
	rename -uid "413DEBFB-4A5D-CEDC-75A6-65A37FDE43E2";
	setAttr ".ics" -type "componentList" 3 "vtx[5]" "vtx[12]" "vtx[224:225]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak47";
	rename -uid "632D2838-4489-F4D4-2872-5088393CBC5F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[224]" -type "float3" 0.52570534 0 -1.577117 ;
	setAttr ".tk[225]" -type "float3" -1.5001497 0 0 ;
createNode polyTweakUV -n "polyTweakUV981";
	rename -uid "1BEA9B1E-47C0-6BAC-FF39-47AF13F22D78";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV982";
	rename -uid "80219CA0-4882-FBE1-2B59-04871950BE6A";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.0019967852 -0.0029981693 ;
	setAttr ".uvtk[11]" -type "float2" -0.0045579099 0.12846197 ;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV983";
	rename -uid "4C48A622-48E5-8842-CECC-E5B3453E797B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV984";
	rename -uid "A2DB66BA-42EA-65D6-3AFA-CA86F514D914";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[7]" -type "float2" 0.003623751 7.3565496e-007 ;
	setAttr ".uvtk[17]" -type "float2" 0.004375793 -0.10379154 ;
	setAttr ".uvtk[18]" -type "float2" 0.022052452 -0.026626877 ;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV985";
	rename -uid "A32B34B1-439B-F4A2-45EA-41BB73C6A19B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV986";
	rename -uid "B4AD7517-4EE9-AD38-CF89-53B261A9EE65";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV987";
	rename -uid "3B5DCC24-45BD-4D58-5915-73A2C94807D7";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV988";
	rename -uid "F160F598-4953-3876-CDC4-468A37C52F4D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV989";
	rename -uid "125F20AB-4AF8-3204-52E8-24994DBDE2B2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV990";
	rename -uid "F6CB9BA2-41A2-6594-45A6-52A758E37D7D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV991";
	rename -uid "F08974E2-4CEC-863C-A6A1-75A4A9C0168B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV992";
	rename -uid "1FAAD946-4A36-81B6-5DF7-A09017FDB131";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV993";
	rename -uid "FDC3D236-492C-CC44-B998-EBBB01C2F505";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV994";
	rename -uid "666FB4E1-42E4-E0B5-1341-329DD5FABEB6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV995";
	rename -uid "CBF09DF6-4FF6-5CDA-42D2-938ACA682208";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV996";
	rename -uid "D31B87C1-476B-872E-DCA4-8C953A6F7D4E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV997";
	rename -uid "F1E3FC0A-4920-A7B5-8106-7BACBE290044";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV998";
	rename -uid "B92753D3-4009-E65C-720B-78A84E69CB1B";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV999";
	rename -uid "18C06285-4789-B4F0-BF84-2885D452D87C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV1000";
	rename -uid "D3EFDFAB-40E3-506C-3CD0-3E8018B09B0D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV1001";
	rename -uid "BBE3CAAF-4999-4B10-B246-45AD4DA27ADA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV1002";
	rename -uid "E69CC099-4FAD-816D-00E7-BA92C6F206FF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV1003";
	rename -uid "4470AE22-4E2D-59D6-EA18-AD9BA1D690A9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV1004";
	rename -uid "D27B42B6-438E-5372-DD26-86B1D0810EB2";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV1005";
	rename -uid "C180E128-44C6-415F-E46C-FAA388576185";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV1006";
	rename -uid "854D6E76-4FFB-7E96-45C0-7ABDDE1DD66F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV1007";
	rename -uid "1825E5BB-4C42-D820-363E-0DB907F70542";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV1008";
	rename -uid "8693629A-4B86-6512-578F-56AAE4D6EADE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV1009";
	rename -uid "68DA0DD2-4711-F4AA-53FB-F9B4B7EB1D76";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV1010";
	rename -uid "6276A669-4179-5B6F-702D-DFAFD6DFB47E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV1011";
	rename -uid "6A536D1A-42A6-463A-DE19-109647730045";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV1012";
	rename -uid "19F7308F-4685-75F2-40A7-E885D630D6C0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV1013";
	rename -uid "465B7123-41BF-0236-ECEB-8AB3BDDCE7AB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV1014";
	rename -uid "118C4A91-45FE-FE3C-7679-9E94928344CE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV1015";
	rename -uid "ABDF6E1E-4100-034F-A839-F98AC75391F9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert35";
	rename -uid "8F52D981-43A9-4F58-46BF-BEB8404ED20E";
	setAttr ".ics" -type "componentList" 3 "vtx[6]" "vtx[17]" "vtx[231:232]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak48";
	rename -uid "F8DFC26A-45B8-5FBE-B840-D491AE4FCB44";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[231]" -type "float3" 1.5001497 0 0 ;
	setAttr ".tk[232]" -type "float3" -0.52570534 0 -1.577117 ;
createNode polySplit -n "polySplit3";
	rename -uid "EC8C4475-4C49-3094-2BCF-2C88551FF875";
	setAttr -s 6 ".e[0:5]"  0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 6 ".d[0:5]"  -2147483629 -2147483628 -2147483637 -2147483636 -2147483619 -2147483620;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "7578BA65-4598-5D79-368A-748891DA0DD7";
	setAttr -s 3 ".e[0:2]"  0.5 0.5 0.5;
	setAttr -s 3 ".d[0:2]"  -2147483400 -2147483399 -2147483243;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "1A87B26B-4817-7D1E-D636-8290150A70FA";
	setAttr -s 3 ".e[0:2]"  0.5 0.5 0.5;
	setAttr -s 3 ".d[0:2]"  -2147483397 -2147483398 -2147483245;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyMergeVert -n "polyMergeVert36";
	rename -uid "35A92067-4ABE-FF82-A695-D58B0CAAE615";
	setAttr ".ics" -type "componentList" 4 "vtx[11]" "vtx[16]" "vtx[35]" "vtx[38]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyTweak -n "polyTweak49";
	rename -uid "7DBBB318-4990-E764-4A15-F88CB8FDE47C";
	setAttr ".uopa" yes;
	setAttr -s 17 ".tk";
	setAttr ".tk[231]" -type "float3" -0.4316192 -0.31902277 0.1354294 ;
	setAttr ".tk[232]" -type "float3" -0.4316192 -0.31902289 -0.079131261 ;
	setAttr ".tk[233]" -type "float3" 0.074984752 -0.31902289 -0.36593801 ;
	setAttr ".tk[234]" -type "float3" -0.074984752 -0.31902301 -0.36593801 ;
	setAttr ".tk[235]" -type "float3" 0.4316192 -0.31902301 -0.079131261 ;
	setAttr ".tk[236]" -type "float3" 0.4316192 -0.31902289 0.1354294 ;
	setAttr ".tk[237]" -type "float3" 0.41285312 -0.027551472 -0.36593801 ;
	setAttr ".tk[238]" -type "float3" 0.41285312 0.10261595 -0.36593801 ;
	setAttr ".tk[239]" -type "float3" 0.5383904 0.3545965 -0.55424345 ;
	setAttr ".tk[240]" -type "float3" -0.41285312 -0.027551472 -0.36593801 ;
	setAttr ".tk[241]" -type "float3" -0.41285312 0.10261595 -0.36593801 ;
	setAttr ".tk[242]" -type "float3" -0.5383904 0.35459471 -0.55424345 ;
createNode polySplit -n "polySplit6";
	rename -uid "16CAE78F-4834-5979-63D7-669FB6980F70";
	setAttr -s 8 ".e[0:7]"  0.80000001 0.80000001 0.2 0.2 0.2 0.2 0.80000001
		 0.80000001;
	setAttr -s 8 ".d[0:7]"  -2147483632 -2147483631 -2147483233 -2147483639 -2147483638 -2147483238 
		-2147483623 -2147483624;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyMergeVert -n "polyMergeVert37";
	rename -uid "A5AFC76C-4102-2FA0-185E-CC8C9ED59624";
	setAttr ".ics" -type "componentList" 4 "vtx[66]" "vtx[129]" "vtx[241]" "vtx[248]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert38";
	rename -uid "05086BFD-4CD6-29D9-09B7-1493C74EED20";
	setAttr ".ics" -type "componentList" 4 "vtx[13]" "vtx[18]" "vtx[35]" "vtx[37]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polySplit -n "polySplit7";
	rename -uid "28909EBC-49B4-9F79-941E-1DBA22866932";
	setAttr -s 8 ".e[0:7]"  0.180694 0.180694 0.81930602 0.81930602 0.81930602
		 0.81930602 0.180694 0.180694;
	setAttr -s 8 ".d[0:7]"  -2147483632 -2147483631 -2147483234 -2147483233 -2147483232 -2147483231 
		-2147483623 -2147483624;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV1016";
	rename -uid "21C1CCFD-4216-BEB8-95B1-EAB9510E0F77";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV1017";
	rename -uid "6BDE4859-424B-6528-A145-57A65B8C0046";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV1018";
	rename -uid "E9387402-4B66-DACE-96EF-379AA5B6EA75";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV1019";
	rename -uid "D0F6BAE6-449A-CAAB-00BD-5593E9975045";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[24]" -type "float2" -0.00025544947 -0.00067759684 ;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV1020";
	rename -uid "9ADF22D8-49A1-1F6C-1188-969B38B212DA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV1021";
	rename -uid "F1ECEE1F-4729-6CE1-558F-E7AFA563D7D6";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV1022";
	rename -uid "3C54E5CD-4D46-5C94-E664-BE900C6C14E9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV1023";
	rename -uid "1E810E7B-4935-DE81-3D15-EEA8FE8E37B9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV1024";
	rename -uid "B772D91A-49B3-4B96-A6DC-CFB1778FCDDC";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV1025";
	rename -uid "12E18526-4720-CAF6-9AFA-34BB43815BCA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV1026";
	rename -uid "D5681B18-4BDE-8B80-97AA-56BFD8FA953F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV1027";
	rename -uid "170C559D-4EE0-08CF-4BE2-069114F1D803";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" 0.011697611 0.23896617 ;
	setAttr ".uvtk[4]" -type "float2" -0.22653338 0.0043042195 ;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV1028";
	rename -uid "D0E8B155-412E-5776-2391-1FA46B9C2C61";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV1029";
	rename -uid "20E79CD7-4349-32C0-1098-7DB37D9CDCD8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV1030";
	rename -uid "99F91A12-44EC-D266-A6CF-3AB314D28667";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV1031";
	rename -uid "39F60A23-49C5-9FE2-EFA8-B399B617B6F3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV1032";
	rename -uid "F3AD05C7-4ED2-7B2C-0D33-A4A00AB80987";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV1033";
	rename -uid "CAED65AA-4445-5289-BC40-4180D29309FB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV1034";
	rename -uid "F06CF374-40B4-289B-15E6-51856BC8BF07";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV1035";
	rename -uid "264C3882-42BF-086B-FB28-2F8BF94D50DD";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV1036";
	rename -uid "959CEFD3-4604-9E17-AF24-1E80EF394D06";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV1037";
	rename -uid "40B45D31-4815-D02C-2461-85BAF8AEF7F9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV1038";
	rename -uid "A8AB3947-47B0-8EAA-F458-CDB5BA627D5A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV1039";
	rename -uid "72674E08-4B73-F50E-CBFA-2BB3C7C4A682";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV1040";
	rename -uid "F585A34D-43C8-C271-20B5-D2900AC910C5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV1041";
	rename -uid "27B72B40-4D86-0F63-2AA2-30B8755F13E0";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV1042";
	rename -uid "9C299990-40AD-76CA-4E11-49AF5A93ED03";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV1043";
	rename -uid "EE7B19CF-4342-159B-6ED5-37BF1E7F8731";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV1044";
	rename -uid "99922E52-40D6-A4E9-939A-4BBE41884E57";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV1045";
	rename -uid "597B3E7D-4C62-9610-410E-F4BDBA20408A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV1046";
	rename -uid "796A3CCD-4B93-FA8C-583C-CB9FD7742AB4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV1047";
	rename -uid "131DC329-4BBA-483E-E2B6-07B56A9D36EF";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV1048";
	rename -uid "71A3C1D3-4A2E-7ACD-5840-1C890DD21C9A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV1049";
	rename -uid "1368996D-49C4-027D-FA4A-6E8CE9C5CB7D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV1050";
	rename -uid "18F75B35-43D3-A1A1-5CF0-0BA873B38246";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert39";
	rename -uid "AA040212-4052-7D72-C274-ACADC23257FD";
	setAttr ".ics" -type "componentList" 2 "vtx[66]" "vtx[252]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak50";
	rename -uid "3E1A4736-4BEB-D044-7FB6-76A7C8179416";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[66]" -type "float3" 0.76753139 0.025719643 1.648807 ;
createNode polyMergeVert -n "polyMergeVert40";
	rename -uid "33787FE2-4331-69EA-40B3-2C98578369C9";
	setAttr ".ics" -type "componentList" 8 "vtx[67]" "vtx[69]" "vtx[71]" "vtx[73]" "vtx[75:81]" "vtx[85]" "vtx[223]" "vtx[250]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert41";
	rename -uid "E77CCA8B-4A9E-16BD-82F7-0090E6AC2D80";
	setAttr ".ics" -type "componentList" 2 "vtx[79:82]" "vtx[85:86]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert42";
	rename -uid "883FCB7C-423F-4E53-FBA9-0594C5C37ABF";
	setAttr ".ics" -type "componentList" 2 "vtx[62]" "vtx[83:86]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert43";
	rename -uid "F37B7FBB-4345-4AE1-1A6D-0395A00CD873";
	setAttr ".ics" -type "componentList" 14 "vtx[8]" "vtx[15]" "vtx[23]" "vtx[33]" "vtx[36]" "vtx[119:122]" "vtx[140]" "vtx[142:199]" "vtx[206]" "vtx[210]" "vtx[215:218]" "vtx[228]" "vtx[236:238]" "vtx[243:245]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyTweakUV -n "polyTweakUV1051";
	rename -uid "FFDD73D8-4D7C-3271-CD6A-2790EFA2590A";
	setAttr ".uopa" yes;
createNode polyTweakUV -n "polyTweakUV1052";
	rename -uid "177A4AF4-4182-4666-AC75-77B53D8D4549";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map11";
createNode polyTweakUV -n "polyTweakUV1053";
	rename -uid "34E19E79-4345-F8FB-AA5E-D9BB70E6FD1B";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[20]" -type "float2" -0.0003237866 -4.7574943e-008 ;
	setAttr ".uvs" -type "string" "map12";
createNode polyTweakUV -n "polyTweakUV1054";
	rename -uid "68C940EE-4502-1E49-31DE-5790C032D17A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map13";
createNode polyTweakUV -n "polyTweakUV1055";
	rename -uid "036201B9-439B-AEE2-A87C-0BBBDBD7EF0F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map14";
createNode polyTweakUV -n "polyTweakUV1056";
	rename -uid "1F327DD4-4127-EAB2-6183-58B0B97E0A46";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map15";
createNode polyTweakUV -n "polyTweakUV1057";
	rename -uid "E2987B88-422B-5E16-5277-218F83E56A43";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map16";
createNode polyTweakUV -n "polyTweakUV1058";
	rename -uid "00D398D5-4D8C-DAA4-BC95-0BB3A45F8A2F";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map17";
createNode polyTweakUV -n "polyTweakUV1059";
	rename -uid "4E8CBBE1-4800-9FA5-3FE4-A19632672D98";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map18";
createNode polyTweakUV -n "polyTweakUV1060";
	rename -uid "2F2FE00D-46AE-9BB3-4E33-17A14A9C69F9";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map19";
createNode polyTweakUV -n "polyTweakUV1061";
	rename -uid "865A1335-4820-4DEF-2DC8-56B69C08AE61";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map110";
createNode polyTweakUV -n "polyTweakUV1062";
	rename -uid "6CF170E3-43A2-4991-6E63-62A64F0A1470";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map111";
createNode polyTweakUV -n "polyTweakUV1063";
	rename -uid "E1C4164C-46C1-5900-D9CF-E7BFEB72E14E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map112";
createNode polyTweakUV -n "polyTweakUV1064";
	rename -uid "81313CE0-44BD-2DB2-5353-B792AAA5A79E";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map113";
createNode polyTweakUV -n "polyTweakUV1065";
	rename -uid "B8FDE202-41CC-AE0E-7964-E8902A74DDD5";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map114";
createNode polyTweakUV -n "polyTweakUV1066";
	rename -uid "9D0532A9-4BEC-EDBE-2631-CEA30F307E97";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map115";
createNode polyTweakUV -n "polyTweakUV1067";
	rename -uid "2941A0BC-46CF-B3E3-0ADD-D5844BA82F39";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map116";
createNode polyTweakUV -n "polyTweakUV1068";
	rename -uid "AC1F9B7E-4538-D1CA-C5B1-4BAC2C81F35C";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map117";
createNode polyTweakUV -n "polyTweakUV1069";
	rename -uid "972B91F7-4E7E-37C1-5258-EF9A31DD316A";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map118";
createNode polyTweakUV -n "polyTweakUV1070";
	rename -uid "8A08E57F-45AB-2015-D612-CB843E74964D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map119";
createNode polyTweakUV -n "polyTweakUV1071";
	rename -uid "77E3F5FD-4004-6C3A-2A5E-8F8D094A6239";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map120";
createNode polyTweakUV -n "polyTweakUV1072";
	rename -uid "27D89382-45A2-90F7-E500-E5BAC81483EA";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" -0.22653338 0.004304219 ;
	setAttr ".uvtk[9]" -type "float2" -0.0015258807 0.19520625 ;
	setAttr ".uvs" -type "string" "map121";
createNode polyTweakUV -n "polyTweakUV1073";
	rename -uid "AB59F1FC-41E2-BA25-35AD-D5B8B1525F75";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map122";
createNode polyTweakUV -n "polyTweakUV1074";
	rename -uid "6257B66C-4BC7-BE09-AF36-AF9BE63F2FF4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map123";
createNode polyTweakUV -n "polyTweakUV1075";
	rename -uid "B132E8E3-430F-3B7A-F985-09BEE011C434";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map124";
createNode polyTweakUV -n "polyTweakUV1076";
	rename -uid "921C5AD3-4F2C-BBB9-9F0C-CAA45FE6C7D3";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map125";
createNode polyTweakUV -n "polyTweakUV1077";
	rename -uid "6B06853D-46F3-747D-2C92-C09E722EFF87";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map126";
createNode polyTweakUV -n "polyTweakUV1078";
	rename -uid "6AA6022E-48D0-4399-41EE-6EAA40A7BD59";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map127";
createNode polyTweakUV -n "polyTweakUV1079";
	rename -uid "01A5C5FB-4EFE-5F08-E4F0-61845B596E75";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map128";
createNode polyTweakUV -n "polyTweakUV1080";
	rename -uid "56CB60C0-4A41-760E-4BFB-1FB7C3C031BE";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map129";
createNode polyTweakUV -n "polyTweakUV1081";
	rename -uid "EAFC0D02-49F7-8F37-B4D8-5BB4CD636AC8";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map130";
createNode polyTweakUV -n "polyTweakUV1082";
	rename -uid "966DE561-4C22-9650-9502-F6A75FD38DD4";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map131";
createNode polyTweakUV -n "polyTweakUV1083";
	rename -uid "2E5DB356-4792-6661-7734-8B840C62CACA";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map132";
createNode polyTweakUV -n "polyTweakUV1084";
	rename -uid "7B396A5F-4F1A-42B1-CB0C-6DA55B1DC86D";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map133";
createNode polyTweakUV -n "polyTweakUV1085";
	rename -uid "9F4A185D-4520-606B-4E5F-FB8C40F91DDB";
	setAttr ".uopa" yes;
	setAttr ".uvs" -type "string" "map134";
createNode polyMergeVert -n "polyMergeVert44";
	rename -uid "B48EE42B-48DB-12E5-9D96-EAB81898FC3C";
	setAttr ".ics" -type "componentList" 2 "vtx[122]" "vtx[225]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak51";
	rename -uid "EC1F9BBB-446E-8230-1493-6D814BDFC337";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[122]" -type "float3" -0.76753139 0.025719643 1.648807 ;
createNode polyBevel3 -n "polyBevel4";
	rename -uid "B68B26C5-4845-DBD7-8760-BABB4781131B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[103]" "e[107]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	rename -uid "EE27D61D-4882-530E-CB37-00B7C042FBE9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[0]" "e[16]" "e[19]" "e[24]" "e[28]" "e[354]" "e[359]" "e[403:404]" "e[407]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" 0 0 -1.5760782847198183 ;
	setAttr ".s" -type "double3" 0.78349701406307581 0.81086209125113151 0.16461892798683447 ;
	setAttr ".pvt" -type "float3" 0 -15.915841 8.220109 ;
	setAttr ".rs" 56178;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -13 -19.000009536743164 7.9999966621398926 ;
	setAttr ".cbx" -type "double3" 13 -12.831671714782715 11.592377662658691 ;
createNode polyMergeVert -n "polyMergeVert45";
	rename -uid "0801E8F1-47BA-F881-B8BA-5BA47DE78167";
	setAttr ".ics" -type "componentList" 6 "vtx[0:18]" "vtx[27:36]" "vtx[61:64]" "vtx[117:120]" "vtx[137:139]" "vtx[190:243]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".am" yes;
createNode polyBridgeEdge -n "polyBridgeEdge4";
	rename -uid "1A279A92-4054-A5F9-326A-1189836CF47F";
	setAttr ".ics" -type "componentList" 4 "e[418]" "e[420]" "e[423]" "e[425:427]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 237;
	setAttr ".sv2" 235;
	setAttr ".d" 1;
createNode polyTweak -n "polyTweak52";
	rename -uid "695621C6-4EB8-CB87-0AF3-BDAC2FB84E3F";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[61]" -type "float3" 0 0.18611813 0 ;
	setAttr ".tk[117]" -type "float3" 0 0.18611813 0 ;
	setAttr ".tk[228]" -type "float3" 0 0.18611813 0 ;
	setAttr ".tk[229]" -type "float3" 0 0.18611813 0 ;
	setAttr ".tk[232]" -type "float3" 0 0.024731636 0 ;
	setAttr ".tk[233]" -type "float3" 0 0.024731636 0 ;
	setAttr ".tk[237]" -type "float3" 0 0.58333302 0 ;
	setAttr ".tk[240]" -type "float3" 0 0.58333302 0 ;
	setAttr ".tk[242]" -type "float3" 0 0.60338688 0 ;
	setAttr ".tk[243]" -type "float3" 0 0.60338688 0 ;
createNode polyBridgeEdge -n "polyBridgeEdge5";
	rename -uid "15F576C0-4390-17C3-D605-B8900DC1D6E0";
	setAttr ".ics" -type "componentList" 1 "e[432:433]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 242;
	setAttr ".sv2" 237;
	setAttr ".d" 1;
	setAttr ".td" 1;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "F5F7CC44-471D-6498-D107-48B4E4E193DB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[409:410]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak53";
	rename -uid "8ABCC72F-454D-7889-69DC-6A9C7603BD04";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[242]" -type "float3" 0 0 1.5113081 ;
	setAttr ".tk[243]" -type "float3" 0 0 1.5113081 ;
createNode deleteComponent -n "deleteComponent32";
	rename -uid "FC7795E9-4A39-A9E1-43A7-B1B692E69B54";
	setAttr ".dc" -type "componentList" 3 "f[141]" "f[146]" "f[150]";
createNode deleteComponent -n "deleteComponent33";
	rename -uid "98FCE239-420A-CED0-CEDD-4BB6C6AA8A65";
	setAttr ".dc" -type "componentList" 1 "f[136]";
createNode deleteComponent -n "deleteComponent34";
	rename -uid "07DDE058-497B-C0E1-5B1B-95B489970504";
	setAttr ".dc" -type "componentList" 4 "f[67]" "f[73]" "f[79]" "f[85]";
createNode deleteComponent -n "deleteComponent35";
	rename -uid "BE84D4B8-4E58-4E1D-9590-79BBBF2003D1";
	setAttr ".dc" -type "componentList" 2 "f[79]" "f[81:83]";
createNode deleteComponent -n "deleteComponent36";
	rename -uid "F930B073-4C73-D624-69F6-298CB49C800B";
	setAttr ".dc" -type "componentList" 2 "f[74]" "f[76:78]";
createNode deleteComponent -n "deleteComponent37";
	rename -uid "7CCE03A0-4676-2F2D-E9C1-379E9B2396D1";
	setAttr ".dc" -type "componentList" 2 "f[69]" "f[71:73]";
createNode deleteComponent -n "deleteComponent38";
	rename -uid "564B01D5-4EC8-CBEE-AACD-B0B62606C901";
	setAttr ".dc" -type "componentList" 2 "f[64]" "f[66:68]";
createNode deleteComponent -n "deleteComponent39";
	rename -uid "3D5E242F-4428-E0FF-A01B-BDBB4014D4EB";
	setAttr ".dc" -type "componentList" 2 "f[113]" "f[115:117]";
createNode deleteComponent -n "deleteComponent40";
	rename -uid "2CD5066E-4F7B-4D90-FFA2-5C9B39E384E9";
	setAttr ".dc" -type "componentList" 2 "f[114]" "f[116:117]";
createNode deleteComponent -n "deleteComponent41";
	rename -uid "367773C5-4546-4A32-B69E-90B88CB2AF21";
	setAttr ".dc" -type "componentList" 1 "f[116:117]";
createNode deleteComponent -n "deleteComponent42";
	rename -uid "5E9B7900-4134-8DFA-4A8A-C69B25CE366B";
	setAttr ".dc" -type "componentList" 1 "f[117:118]";
createNode polyExtrudeEdge -n "polyExtrudeEdge2";
	rename -uid "B42B9FC1-4449-269D-CE3F-42ADC50DDB4B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[145:160]" "e[244:259]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.00064468384 -7.8628473 11.492493 ;
	setAttr ".rs" 48939;
	setAttr ".lt" -type "double3" 1.4432899320127035e-015 4.2049697057677804e-015 -0.16669495484800176 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -10.046124458312988 -9.9999485015869141 9.363499641418457 ;
	setAttr ".cbx" -type "double3" 10.04741382598877 -5.725745677947998 13.621486663818359 ;
createNode polyCloseBorder -n "polyCloseBorder5";
	rename -uid "03916AE0-46C2-3C93-193E-EE963F01DC4F";
	setAttr ".ics" -type "componentList" 20 "e[387]" "e[390:392]" "e[395]" "e[398:400]" "e[403]" "e[406:408]" "e[411]" "e[414:416]" "e[419]" "e[421]" "e[423:424]" "e[427]" "e[429]" "e[431:432]" "e[435]" "e[437]" "e[439:440]" "e[443]" "e[445]" "e[447:448]";
createNode groupId -n "groupId1";
	rename -uid "E411AC4B-4A1A-49F9-EB09-2295381DA32C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "D9FF0BBD-4E4D-EA3E-F1E7-E0BD2F9AD8C2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:222]";
createNode polyTweak -n "polyTweak54";
	rename -uid "80F90727-4A63-CB00-3F18-95975FB91004";
	setAttr ".uopa" yes;
	setAttr -s 46 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 -1.4371728 ;
	setAttr ".tk[37]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[38]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[39]" -type "float3" 0 0 -3.5762787e-007 ;
	setAttr ".tk[40]" -type "float3" 0 0 -3.5762787e-007 ;
	setAttr ".tk[41]" -type "float3" 0 9.5367432e-007 1.1920929e-007 ;
	setAttr ".tk[42]" -type "float3" 0 9.5367432e-007 1.1920929e-007 ;
	setAttr ".tk[43]" -type "float3" 0 9.5367432e-007 -4.7683716e-007 ;
	setAttr ".tk[44]" -type "float3" 0 9.5367432e-007 -4.7683716e-007 ;
	setAttr ".tk[105]" -type "float3" 0 2.3841858e-007 0 ;
	setAttr ".tk[107]" -type "float3" 0 0 2.9802322e-008 ;
	setAttr ".tk[108]" -type "float3" 0 1.1920929e-007 2.3841858e-007 ;
	setAttr ".tk[109]" -type "float3" 0 2.3841858e-007 0 ;
	setAttr ".tk[111]" -type "float3" 0 0 2.9802322e-008 ;
	setAttr ".tk[112]" -type "float3" 0 1.1920929e-007 2.3841858e-007 ;
	setAttr ".tk[121]" -type "float3" 0 0 -1.4371728 ;
	setAttr ".tk[156]" -type "float3" 0 -1.1920929e-007 0 ;
	setAttr ".tk[157]" -type "float3" 0 -1.1920929e-007 0 ;
	setAttr ".tk[158]" -type "float3" 0 -1.1920929e-007 0 ;
	setAttr ".tk[159]" -type "float3" 0 -1.1920929e-007 0 ;
	setAttr ".tk[160]" -type "float3" 0 9.5367432e-007 0 ;
	setAttr ".tk[161]" -type "float3" 0 9.5367432e-007 0 ;
	setAttr ".tk[162]" -type "float3" 0 9.5367432e-007 -1.1920929e-007 ;
	setAttr ".tk[163]" -type "float3" 0 9.5367432e-007 -1.1920929e-007 ;
	setAttr ".tk[164]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[165]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[210]" -type "float3" 0 0 -0.52121049 ;
	setAttr ".tk[211]" -type "float3" 0 0 -0.52121049 ;
createNode polySplit -n "polySplit8";
	rename -uid "65986A94-4AF6-C81B-DEF5-D79163BFF711";
	setAttr -s 9 ".e[0:8]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 9 ".d[0:8]"  -2147483567 -2147483566 -2147483463 -2147483461 -2147483562 -2147483563 
		-2147483453 -2147483455 -2147483567;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode deleteComponent -n "deleteComponent43";
	rename -uid "54B5955E-4AF0-7242-8A26-5B81F37E51F9";
	setAttr ".dc" -type "componentList" 1 "f[15]";
createNode deleteComponent -n "deleteComponent44";
	rename -uid "548A1F63-49C8-C312-FD17-D9815D588566";
	setAttr ".dc" -type "componentList" 1 "f[40]";
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "85011463-4D6B-029D-FDDA-169C18AB5DC6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[0]" "e[16]" "e[19]" "e[24]" "e[28]" "e[303]" "e[308]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak55";
	rename -uid "8CF40682-46D4-0CB2-188F-79B91CC768DD";
	setAttr ".uopa" yes;
	setAttr -s 52 ".tk";
	setAttr ".tk[9]" -type "float3" 0 0 2.9802322e-008 ;
	setAttr ".tk[14]" -type "float3" 0 0 2.9802322e-008 ;
	setAttr ".tk[37]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[38]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[39]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[40]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[41]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[42]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[43]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[44]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[45]" -type "float3" 0 0 -0.65919751 ;
	setAttr ".tk[46]" -type "float3" 0 0 -0.65919751 ;
	setAttr ".tk[51]" -type "float3" 0 0 -0.65919751 ;
	setAttr ".tk[52]" -type "float3" 0 0 -0.65919751 ;
	setAttr ".tk[105]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[106]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[107]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[108]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[109]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[110]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[111]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[112]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[113]" -type "float3" -1.3829113 0 -0.62943345 ;
	setAttr ".tk[114]" -type "float3" -0.14264859 0 0 ;
	setAttr ".tk[115]" -type "float3" -0.1396234 0 0 ;
	setAttr ".tk[116]" -type "float3" -1.3535818 0 -0.62943345 ;
	setAttr ".tk[117]" -type "float3" 1.3829113 0 -0.62943345 ;
	setAttr ".tk[118]" -type "float3" 0.14264859 0 0 ;
	setAttr ".tk[119]" -type "float3" 0.1396234 0 0 ;
	setAttr ".tk[120]" -type "float3" 1.3535818 0 -0.62943345 ;
	setAttr ".tk[160]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[161]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[162]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[163]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[164]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[165]" -type "float3" 0 -9.5367432e-007 0 ;
	setAttr ".tk[180]" -type "float3" 0 0 -0.81561393 ;
	setAttr ".tk[185]" -type "float3" 0 0 -0.81561393 ;
	setAttr ".tk[254]" -type "float3" -0.7707572 0 0 ;
	setAttr ".tk[255]" -type "float3" -0.75441056 0 0 ;
	setAttr ".tk[258]" -type "float3" 0.75441056 0 0 ;
	setAttr ".tk[259]" -type "float3" 0.7707572 0 0 ;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "59978194-486B-CB79-E3AF-ACBA537FF564";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[364]" "e[367]" "e[369]" "e[372]" "e[374:376]" "e[378]" "e[380:381]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "30C697DE-48A2-5950-9EE4-2C8FEB284FEE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[457:464]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge5";
	rename -uid "0995132E-49AB-D40E-0FDC-EB8E12E2100C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[8]" "e[13]" "e[21]" "e[198:199]" "e[315]" "e[320]" "e[365]" "e[370]" "e[383]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge6";
	rename -uid "7A69C3E6-4414-3755-69D0-D2AD3E768B83";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[309:313]" "e[316]" "e[321]" "e[368]" "e[373]" "e[384]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge7";
	rename -uid "DC69BBD9-45C2-2739-DBCC-FC9CC57B56C3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[8]" "e[13]" "e[21]" "e[198:199]" "e[315]" "e[320]" "e[365]" "e[370]" "e[383]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge8";
	rename -uid "4C285B15-43BC-FF67-CA27-C79AE91A6836";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[8]" "e[13]" "e[21]" "e[198:199]" "e[315]" "e[320]" "e[365]" "e[370]" "e[383]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge9";
	rename -uid "44591FBB-4690-685D-E8CA-7F99A4A1DAF2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[351]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge10";
	rename -uid "E07F4816-4938-1507-0D0C-D598AE85907F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[81:88]" "e[449:450]" "e[453:454]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak56";
	rename -uid "655DE09B-4BE7-AFFD-2B2E-B6AAD4F1F7B6";
	setAttr ".uopa" yes;
	setAttr -s 21 ".tk";
	setAttr ".tk[4]" -type "float3" 0 -1.1920929e-007 0 ;
	setAttr ".tk[45]" -type "float3" -2.3008325 1.1920929e-007 -1.4305115e-006 ;
	setAttr ".tk[46]" -type "float3" 2.3008325 1.1920929e-007 -2.3841858e-007 ;
	setAttr ".tk[47]" -type "float3" -2.3008325 9.5367432e-007 0 ;
	setAttr ".tk[48]" -type "float3" 2.3008325 9.5367432e-007 -2.3841858e-007 ;
	setAttr ".tk[49]" -type "float3" -2.3008325 -1.7881393e-007 1.1920929e-007 ;
	setAttr ".tk[50]" -type "float3" 2.3008325 -1.7881393e-007 0 ;
	setAttr ".tk[51]" -type "float3" -2.3008325 0 1.4305115e-006 ;
	setAttr ".tk[52]" -type "float3" 2.3008325 -4.7683716e-007 1.4305115e-006 ;
	setAttr ".tk[121]" -type "float3" 0 -2.3841858e-007 0 ;
	setAttr ".tk[252]" -type "float3" -2.3008325 -5.9604645e-008 0 ;
	setAttr ".tk[253]" -type "float3" 2.3008325 -5.9604645e-008 0 ;
	setAttr ".tk[256]" -type "float3" 2.3008325 -5.9604645e-008 -1.1920929e-007 ;
	setAttr ".tk[257]" -type "float3" -2.3008325 -5.9604645e-008 0 ;
createNode polySplit -n "polySplit9";
	rename -uid "2D29B809-4EBC-937A-7299-2FAC67DEE398";
	setAttr -s 7 ".e[0:6]"  0.5 0.5 0.5 0.5 0.5 0.5 0.5;
	setAttr -s 7 ".d[0:6]"  -2147483571 -2147483568 -2147483187 -2147483569 -2147483570 -2147483191 
		-2147483571;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySoftEdge -n "polySoftEdge11";
	rename -uid "955C719E-4BC5-7D62-8B50-119040E3C005";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[81:88]" "e[449:450]" "e[453:454]" "e[471:476]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak57";
	rename -uid "AD2B364A-476C-2742-0B16-4F85B3FCC440";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[45]" -type "float3" -2.3937564 0.63658905 -0.96523285 ;
	setAttr ".tk[46]" -type "float3" 2.3937564 0.63658905 -0.96523285 ;
	setAttr ".tk[47]" -type "float3" -2.7306457 0 -0.97331715 ;
	setAttr ".tk[48]" -type "float3" 2.7306457 0 -0.97331715 ;
	setAttr ".tk[49]" -type "float3" -2.6565967 0 -0.96279526 ;
	setAttr ".tk[50]" -type "float3" 2.6565967 0 -0.96279526 ;
	setAttr ".tk[51]" -type "float3" -2.3268528 0.63658905 -0.95471096 ;
	setAttr ".tk[52]" -type "float3" 2.3268528 0.63658905 -0.95471096 ;
	setAttr ".tk[252]" -type "float3" -2.5600343 0.31829453 -0.97331715 ;
	setAttr ".tk[253]" -type "float3" 2.5600343 0.31829453 -0.97331715 ;
	setAttr ".tk[256]" -type "float3" 2.489604 0.31829453 -0.96279526 ;
	setAttr ".tk[257]" -type "float3" -2.489604 0.31829453 -0.96279526 ;
	setAttr ".tk[260]" -type "float3" 0 0 0.60812801 ;
	setAttr ".tk[261]" -type "float3" 0 0 0.60812801 ;
	setAttr ".tk[262]" -type "float3" 0 0 0.60812801 ;
	setAttr ".tk[263]" -type "float3" 0 0 0.60812801 ;
	setAttr ".tk[264]" -type "float3" 0 0 0.60812801 ;
	setAttr ".tk[265]" -type "float3" 0 0 0.60812801 ;
createNode polySoftEdge -n "polySoftEdge12";
	rename -uid "0A248BF0-4EAD-7A6A-C206-6EBAEA80D3D3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[81:88]" "e[449:450]" "e[453:454]" "e[471:476]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak58";
	rename -uid "93553467-427E-4B3B-2409-79B131886C00";
	setAttr ".uopa" yes;
	setAttr -s 19 ".tk";
	setAttr ".tk[45]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[46]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[47]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[48]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[49]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[50]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[51]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[52]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[252]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[253]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[256]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[257]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[260]" -type "float3" 0 0 -0.47939405 ;
	setAttr ".tk[261]" -type "float3" 0 0 -0.47939405 ;
	setAttr ".tk[262]" -type "float3" 0 0 -0.47939405 ;
	setAttr ".tk[263]" -type "float3" 0 0 -0.47939405 ;
	setAttr ".tk[264]" -type "float3" 0 0 -0.47939405 ;
	setAttr ".tk[265]" -type "float3" 0 0 -0.47939405 ;
createNode polySoftEdge -n "polySoftEdge13";
	rename -uid "A38B6F45-4AFC-731C-4469-DDB328EC5E0C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak59";
	rename -uid "306B1E90-49FA-7F5F-9626-E4B2ADD2C345";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[210]" -type "float3" 2.4785094 -3.9524705 2.1640222 ;
	setAttr ".tk[211]" -type "float3" -2.4785094 -3.9524705 2.1640222 ;
	setAttr ".tk[214]" -type "float3" 2.9572625 -3.9524705 2.1640222 ;
	setAttr ".tk[217]" -type "float3" -2.9572625 -3.9524705 2.1640222 ;
createNode polySoftEdge -n "polySoftEdge14";
	rename -uid "F071E199-4393-44BB-BB32-359164491681";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[362:364]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polySplit -n "polySplit10";
	rename -uid "9F2567F6-41DF-0939-D862-06B6300A90B5";
	setAttr -s 4 ".e[0:3]"  0 0.5 0.5 0;
	setAttr -s 4 ".d[0:3]"  -2147483345 -2147483273 -2147483272 -2147483340;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySoftEdge -n "polySoftEdge15";
	rename -uid "63FC64A4-4240-A5FA-469E-D3994B1C4500";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[368]" "e[373]" "e[384]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak60";
	rename -uid "4D36E44F-4CCD-9F58-2468-03AB06D44EBF";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[212]" -type "float3" 0 0.17322063 0.07557869 ;
	setAttr ".tk[215]" -type "float3" 0 0.17322063 0.07557869 ;
	setAttr ".tk[266]" -type "float3" 0 0.77063322 -1.6977654 ;
	setAttr ".tk[267]" -type "float3" 0 0.77063322 -1.6977654 ;
createNode polySoftEdge -n "polySoftEdge16";
	rename -uid "20AEBE64-46ED-E58E-2E86-AF99B51150F9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[369]" "e[374]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge17";
	rename -uid "BAD0B54C-42F9-8B6D-A9DC-69AD155964D0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[365]" "e[370]" "e[383]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak61";
	rename -uid "D488F02E-4D88-FDC7-89CA-CEB371E90DF4";
	setAttr ".uopa" yes;
	setAttr -s 13 ".tk";
	setAttr ".tk[9]" -type "float3" 0 0 -0.29909107 ;
	setAttr ".tk[14]" -type "float3" 0 0 -0.29909107 ;
	setAttr ".tk[212]" -type "float3" -9.5367432e-007 0 -0.29909107 ;
	setAttr ".tk[213]" -type "float3" -4.7683716e-007 0 0 ;
	setAttr ".tk[215]" -type "float3" 9.5367432e-007 0 -0.29909107 ;
	setAttr ".tk[216]" -type "float3" 4.7683716e-007 0 0 ;
	setAttr ".tk[266]" -type "float3" -1.1897812 0 0 ;
	setAttr ".tk[267]" -type "float3" 1.1897812 0 0 ;
createNode polySplit -n "polySplit11";
	rename -uid "30F1C9C9-464C-3EA3-5FCC-668ECFBF0487";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483273 -2147483272;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySoftEdge -n "polySoftEdge18";
	rename -uid "6CDB3FB0-4FED-AD31-CBAF-14A6E25288CF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[479:481]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak62";
	rename -uid "3E39927C-44C0-ADD5-A6FD-7795A18CA1BC";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[268]" -type "float3" 1.4463269 0 0 ;
	setAttr ".tk[269]" -type "float3" -1.4463269 0 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "56B9E986-4682-55EA-2C29-459C58FEF23B";
	setAttr ".ics" -type "componentList" 2 "f[177:178]" "f[236]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -15.997118 8.0175467 ;
	setAttr ".rs" 52248;
	setAttr ".lt" -type "double3" 0 1.457167719820518e-015 -1.040500613473011 ;
	setAttr ".ls" -type "double3" 0.9764131556240635 -0.20324667395723772 -1.6131689895571895 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -10.18546199798584 -19.162563323974609 7.3198156356811523 ;
	setAttr ".cbx" -type "double3" 10.18546199798584 -12.831671714782715 8.7152776718139648 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 74 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "layer2.di" "Rootroot.do";
connectAttr "Rootroot.s" "rootTorso.is";
connectAttr "Rootroot.s" "rootHead.is";
connectAttr "Rootroot.s" "rootBack.is";
connectAttr "Rootroot.s" "rootArmLeft.is";
connectAttr "rootArmLeft.s" "clavacleLeft.is";
connectAttr "clavacleLeft.s" "shoulderLeft.is";
connectAttr "shoulderLeft.s" "|Rootroot|rootArmLeft|clavacleLeft|shoulderLeft|upperArmLeft.is"
		;
connectAttr "|Rootroot|rootArmLeft|clavacleLeft|shoulderLeft|upperArmLeft.s" "elbowLeft.is"
		;
connectAttr "elbowLeft.s" "wristLeft.is";
connectAttr "Rootroot.s" "rootArmRight.is";
connectAttr "rootArmRight.s" "clavacleRight.is";
connectAttr "clavacleRight.s" "shoulderRight.is";
connectAttr "shoulderRight.s" "|Rootroot|rootArmRight|clavacleRight|shoulderRight|upperArmRight.is"
		;
connectAttr "|Rootroot|rootArmRight|clavacleRight|shoulderRight|upperArmRight.s" "elbowRight.is"
		;
connectAttr "elbowRight.s" "wristRight.is";
connectAttr "Rootroot.s" "rootLegsJnt.is";
connectAttr "rootLegsJnt.s" "hipsJnt.is";
connectAttr "hipsJnt.s" "upperLegLeftJnt.is";
connectAttr "upperLegLeftJnt.s" "lowerLegLeftJnt.is";
connectAttr "lowerLegLeftJnt.s" "footLeftJnt.is";
connectAttr "footLeftJnt.s" "joint1.is";
connectAttr "hipsJnt.s" "upperLegRightJnt.is";
connectAttr "upperLegRightJnt.s" "lowerLegRightJnt.is";
connectAttr "lowerLegRightJnt.s" "footRightJnt.is";
connectAttr "footRightJnt.s" "joint2.is";
connectAttr "layer1.di" "BasicMechGrouped.do";
connectAttr "transformGeometry23.og" "baseShape.i";
connectAttr "transformGeometry24.og" "backSideShape.i";
connectAttr "transformGeometry15.og" "leftSideShape.i";
connectAttr "transformGeometry16.og" "leftNozzleShape.i";
connectAttr "transformGeometry25.og" "frontSideShape.i";
connectAttr "transformGeometry26.og" "frontLeftShape1.i";
connectAttr "transformGeometry45.og" "frontLeftShape2.i";
connectAttr "transformGeometry46.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCubeShape7.i"
		;
connectAttr "transformGeometry47.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8|pCubeShape8.i"
		;
connectAttr "transformGeometry48.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11|pCubeShape11.i"
		;
connectAttr "transformGeometry49.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10|pCubeShape10.i"
		;
connectAttr "transformGeometry50.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9|pCubeShape9.i"
		;
connectAttr "transformGeometry51.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12|pCubeShape12.i"
		;
connectAttr "transformGeometry28.og" "jawMidShape.i";
connectAttr "transformGeometry29.og" "pCubeShape5.i";
connectAttr "transformGeometry30.og" "teethmidShape.i";
connectAttr "transformGeometry14.og" "teethLeftShape.i";
connectAttr "transformGeometry5.og" "|BasicMechGrouped|Basic_Legs|hips|hipsShape.i"
		;
connectAttr "transformGeometry6.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|upperLegLeftShape.i"
		;
connectAttr "transformGeometry7.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft|lowerLegLeftShape.i"
		;
connectAttr "transformGeometry8.og" "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft|kneeLeftShape.i"
		;
connectAttr "transformGeometry56.og" "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeftShape1.i"
		;
connectAttr "transformGeometry57.og" "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2|panelLeftShape2.i"
		;
connectAttr "transformGeometry52.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRightShape1.i"
		;
connectAttr "transformGeometry53.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|upperArmRightShape.i"
		;
connectAttr "transformGeometry54.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight|lowerArmRightShape.i"
		;
connectAttr "transformGeometry55.og" "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRight2|armRightShape2.i"
		;
connectAttr "polyExtrudeFace2.out" "BasicMechGrouped1_Basic_Torso_base1Shape.i";
connectAttr "groupId1.id" "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[0].gco"
		;
connectAttr "polyTweakUV1051.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[0].uvtw"
		;
connectAttr "polyTweakUV1052.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[1].uvtw"
		;
connectAttr "polyTweakUV1053.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[2].uvtw"
		;
connectAttr "polyTweakUV1054.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[3].uvtw"
		;
connectAttr "polyTweakUV1055.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[4].uvtw"
		;
connectAttr "polyTweakUV1056.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[5].uvtw"
		;
connectAttr "polyTweakUV1057.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[6].uvtw"
		;
connectAttr "polyTweakUV1058.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[7].uvtw"
		;
connectAttr "polyTweakUV1059.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[8].uvtw"
		;
connectAttr "polyTweakUV1060.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[9].uvtw"
		;
connectAttr "polyTweakUV1061.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[10].uvtw"
		;
connectAttr "polyTweakUV1062.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[11].uvtw"
		;
connectAttr "polyTweakUV1063.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[12].uvtw"
		;
connectAttr "polyTweakUV1064.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[13].uvtw"
		;
connectAttr "polyTweakUV1065.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[14].uvtw"
		;
connectAttr "polyTweakUV1066.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[15].uvtw"
		;
connectAttr "polyTweakUV1067.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[16].uvtw"
		;
connectAttr "polyTweakUV1068.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[17].uvtw"
		;
connectAttr "polyTweakUV1069.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[18].uvtw"
		;
connectAttr "polyTweakUV1070.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[19].uvtw"
		;
connectAttr "polyTweakUV1071.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[20].uvtw"
		;
connectAttr "polyTweakUV1072.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[21].uvtw"
		;
connectAttr "polyTweakUV1073.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[22].uvtw"
		;
connectAttr "polyTweakUV1074.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[23].uvtw"
		;
connectAttr "polyTweakUV1075.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[24].uvtw"
		;
connectAttr "polyTweakUV1076.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[25].uvtw"
		;
connectAttr "polyTweakUV1077.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[26].uvtw"
		;
connectAttr "polyTweakUV1078.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[27].uvtw"
		;
connectAttr "polyTweakUV1079.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[28].uvtw"
		;
connectAttr "polyTweakUV1080.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[29].uvtw"
		;
connectAttr "polyTweakUV1081.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[30].uvtw"
		;
connectAttr "polyTweakUV1082.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[31].uvtw"
		;
connectAttr "polyTweakUV1083.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[32].uvtw"
		;
connectAttr "polyTweakUV1084.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[33].uvtw"
		;
connectAttr "polyTweakUV1085.uvtk[0]" "BasicMechGrouped1_Basic_Torso_base1Shape.uvst[34].uvtw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube2.out" "transformGeometry1.ig";
connectAttr "transformGeometry1.og" "transformGeometry2.ig";
connectAttr "polyCube3.out" "transformGeometry3.ig";
connectAttr "polyCube4.out" "transformGeometry4.ig";
connectAttr "polyCube1.out" "transformGeometry5.ig";
connectAttr "transformGeometry2.og" "transformGeometry6.ig";
connectAttr "transformGeometry3.og" "transformGeometry7.ig";
connectAttr "transformGeometry4.og" "transformGeometry8.ig";
connectAttr "polyCube5.out" "transformGeometry9.ig";
connectAttr "polyCube6.out" "transformGeometry10.ig";
connectAttr "polyCube10.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "transformGeometry11.ig";
connectAttr "transformGeometry11.og" "transformGeometry12.ig";
connectAttr "polyCube12.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "transformGeometry13.ig";
connectAttr "polyCube14.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "transformGeometry14.ig";
connectAttr "polyCube15.out" "transformGeometry15.ig";
connectAttr "polyCube17.out" "transformGeometry16.ig";
connectAttr "polyCube18.out" "transformGeometry17.ig";
connectAttr "polyCube19.out" "transformGeometry18.ig";
connectAttr "polyCube22.out" "transformGeometry19.ig";
connectAttr "polyCube21.out" "transformGeometry20.ig";
connectAttr "polyCube20.out" "transformGeometry21.ig";
connectAttr "polyCube23.out" "transformGeometry22.ig";
connectAttr "polyCube7.out" "transformGeometry23.ig";
connectAttr "polyCube16.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "transformGeometry24.ig";
connectAttr "polyCube8.out" "transformGeometry25.ig";
connectAttr "polyCube9.out" "polyTweak5.ip";
connectAttr "polyTweak5.out" "transformGeometry26.ig";
connectAttr "transformGeometry12.og" "transformGeometry27.ig";
connectAttr "polyCube11.out" "polyTweak6.ip";
connectAttr "polyTweak6.out" "transformGeometry28.ig";
connectAttr "transformGeometry13.og" "transformGeometry29.ig";
connectAttr "polyCube13.out" "polyTweak7.ip";
connectAttr "polyTweak7.out" "transformGeometry30.ig";
connectAttr "transformGeometry27.og" "transformGeometry31.ig";
connectAttr "transformGeometry17.og" "transformGeometry32.ig";
connectAttr "transformGeometry18.og" "transformGeometry33.ig";
connectAttr "transformGeometry19.og" "transformGeometry34.ig";
connectAttr "transformGeometry20.og" "transformGeometry35.ig";
connectAttr "transformGeometry21.og" "transformGeometry36.ig";
connectAttr "transformGeometry22.og" "transformGeometry37.ig";
connectAttr "transformGeometry31.og" "transformGeometry38.ig";
connectAttr "transformGeometry32.og" "transformGeometry39.ig";
connectAttr "transformGeometry33.og" "transformGeometry40.ig";
connectAttr "transformGeometry34.og" "transformGeometry41.ig";
connectAttr "transformGeometry35.og" "transformGeometry42.ig";
connectAttr "transformGeometry36.og" "transformGeometry43.ig";
connectAttr "transformGeometry37.og" "transformGeometry44.ig";
connectAttr "transformGeometry38.og" "transformGeometry45.ig";
connectAttr "transformGeometry39.og" "transformGeometry46.ig";
connectAttr "transformGeometry40.og" "transformGeometry47.ig";
connectAttr "transformGeometry41.og" "transformGeometry48.ig";
connectAttr "transformGeometry42.og" "transformGeometry49.ig";
connectAttr "transformGeometry43.og" "transformGeometry50.ig";
connectAttr "transformGeometry44.og" "transformGeometry51.ig";
connectAttr "polyCube24.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "transformGeometry52.ig";
connectAttr "polyCube26.out" "transformGeometry53.ig";
connectAttr "polyCube27.out" "transformGeometry54.ig";
connectAttr "polyCube25.out" "transformGeometry55.ig";
connectAttr "transformGeometry9.og" "transformGeometry56.ig";
connectAttr "transformGeometry10.og" "transformGeometry57.ig";
connectAttr "layerManager.dli[3]" "layer1.id";
connectAttr "layerManager.dli[4]" "layer2.id";
connectAttr "groupParts1.og" "polySplitEdge1.ip";
connectAttr "polySurfaceShape1.o" "groupParts1.ig";
connectAttr "polySplitEdge1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polySplitVert1.ip";
connectAttr "polySplitVert1.out" "polyTweak9.ip";
connectAttr "polyTweak9.out" "deleteComponent3.ig";
connectAttr "polyTweak10.out" "polyMergeVert1.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert1.mp";
connectAttr "deleteComponent3.og" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polyBridgeEdge1.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBridgeEdge1.mp";
connectAttr "polyMergeVert1.out" "polyTweak11.ip";
connectAttr "polyBridgeEdge1.out" "polyBridgeEdge2.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBridgeEdge2.mp";
connectAttr "polyBridgeEdge2.out" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyTweakUV10.ip";
connectAttr "polyTweakUV10.out" "polyTweakUV11.ip";
connectAttr "polyTweakUV11.out" "polyTweakUV12.ip";
connectAttr "polyTweakUV12.out" "polyTweakUV13.ip";
connectAttr "polyTweakUV13.out" "polyTweakUV14.ip";
connectAttr "polyTweakUV14.out" "polyTweakUV15.ip";
connectAttr "polyTweakUV15.out" "polyTweakUV16.ip";
connectAttr "polyTweakUV16.out" "polyTweakUV17.ip";
connectAttr "polyTweakUV17.out" "polyTweakUV18.ip";
connectAttr "polyTweakUV18.out" "polyTweakUV19.ip";
connectAttr "polyTweakUV19.out" "polyTweakUV20.ip";
connectAttr "polyTweakUV20.out" "polyTweakUV21.ip";
connectAttr "polyTweakUV21.out" "polyTweakUV22.ip";
connectAttr "polyTweakUV22.out" "polyTweakUV23.ip";
connectAttr "polyTweakUV23.out" "polyTweakUV24.ip";
connectAttr "polyTweakUV24.out" "polyTweakUV25.ip";
connectAttr "polyTweakUV25.out" "polyTweakUV26.ip";
connectAttr "polyTweakUV26.out" "polyTweakUV27.ip";
connectAttr "polyTweakUV27.out" "polyTweakUV28.ip";
connectAttr "polyTweakUV28.out" "polyTweakUV29.ip";
connectAttr "polyTweakUV29.out" "polyTweakUV30.ip";
connectAttr "polyTweakUV30.out" "polyTweakUV31.ip";
connectAttr "polyTweakUV31.out" "polyTweakUV32.ip";
connectAttr "polyTweakUV32.out" "polyTweakUV33.ip";
connectAttr "polyTweakUV33.out" "polyTweakUV34.ip";
connectAttr "polyTweakUV34.out" "polyTweakUV35.ip";
connectAttr "polyTweak12.out" "polyMergeVert2.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert2.mp";
connectAttr "polyTweakUV35.out" "polyTweak12.ip";
connectAttr "polyMergeVert2.out" "polyTweakUV36.ip";
connectAttr "polyTweakUV36.out" "polyTweakUV37.ip";
connectAttr "polyTweakUV37.out" "polyTweakUV38.ip";
connectAttr "polyTweakUV38.out" "polyTweakUV39.ip";
connectAttr "polyTweakUV39.out" "polyTweakUV40.ip";
connectAttr "polyTweakUV40.out" "polyTweakUV41.ip";
connectAttr "polyTweakUV41.out" "polyTweakUV42.ip";
connectAttr "polyTweakUV42.out" "polyTweakUV43.ip";
connectAttr "polyTweakUV43.out" "polyTweakUV44.ip";
connectAttr "polyTweakUV44.out" "polyTweakUV45.ip";
connectAttr "polyTweakUV45.out" "polyTweakUV46.ip";
connectAttr "polyTweakUV46.out" "polyTweakUV47.ip";
connectAttr "polyTweakUV47.out" "polyTweakUV48.ip";
connectAttr "polyTweakUV48.out" "polyTweakUV49.ip";
connectAttr "polyTweakUV49.out" "polyTweakUV50.ip";
connectAttr "polyTweakUV50.out" "polyTweakUV51.ip";
connectAttr "polyTweakUV51.out" "polyTweakUV52.ip";
connectAttr "polyTweakUV52.out" "polyTweakUV53.ip";
connectAttr "polyTweakUV53.out" "polyTweakUV54.ip";
connectAttr "polyTweakUV54.out" "polyTweakUV55.ip";
connectAttr "polyTweakUV55.out" "polyTweakUV56.ip";
connectAttr "polyTweakUV56.out" "polyTweakUV57.ip";
connectAttr "polyTweakUV57.out" "polyTweakUV58.ip";
connectAttr "polyTweakUV58.out" "polyTweakUV59.ip";
connectAttr "polyTweakUV59.out" "polyTweakUV60.ip";
connectAttr "polyTweakUV60.out" "polyTweakUV61.ip";
connectAttr "polyTweakUV61.out" "polyTweakUV62.ip";
connectAttr "polyTweakUV62.out" "polyTweakUV63.ip";
connectAttr "polyTweakUV63.out" "polyTweakUV64.ip";
connectAttr "polyTweakUV64.out" "polyTweakUV65.ip";
connectAttr "polyTweakUV65.out" "polyTweakUV66.ip";
connectAttr "polyTweakUV66.out" "polyTweakUV67.ip";
connectAttr "polyTweakUV67.out" "polyTweakUV68.ip";
connectAttr "polyTweakUV68.out" "polyTweakUV69.ip";
connectAttr "polyTweakUV69.out" "polyTweakUV70.ip";
connectAttr "polyTweak13.out" "polyMergeVert3.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert3.mp";
connectAttr "polyTweakUV70.out" "polyTweak13.ip";
connectAttr "polyMergeVert3.out" "deleteComponent5.ig";
connectAttr "deleteComponent5.og" "polySewEdge1.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySewEdge1.mp";
connectAttr "polySewEdge1.out" "polyTweakUV71.ip";
connectAttr "polyTweakUV71.out" "polyTweakUV72.ip";
connectAttr "polyTweakUV72.out" "polyTweakUV73.ip";
connectAttr "polyTweakUV73.out" "polyTweakUV74.ip";
connectAttr "polyTweakUV74.out" "polyTweakUV75.ip";
connectAttr "polyTweakUV75.out" "polyTweakUV76.ip";
connectAttr "polyTweakUV76.out" "polyTweakUV77.ip";
connectAttr "polyTweakUV77.out" "polyTweakUV78.ip";
connectAttr "polyTweakUV78.out" "polyTweakUV79.ip";
connectAttr "polyTweakUV79.out" "polyTweakUV80.ip";
connectAttr "polyTweakUV80.out" "polyTweakUV81.ip";
connectAttr "polyTweakUV81.out" "polyTweakUV82.ip";
connectAttr "polyTweakUV82.out" "polyTweakUV83.ip";
connectAttr "polyTweakUV83.out" "polyTweakUV84.ip";
connectAttr "polyTweakUV84.out" "polyTweakUV85.ip";
connectAttr "polyTweakUV85.out" "polyTweakUV86.ip";
connectAttr "polyTweakUV86.out" "polyTweakUV87.ip";
connectAttr "polyTweakUV87.out" "polyTweakUV88.ip";
connectAttr "polyTweakUV88.out" "polyTweakUV89.ip";
connectAttr "polyTweakUV89.out" "polyTweakUV90.ip";
connectAttr "polyTweakUV90.out" "polyTweakUV91.ip";
connectAttr "polyTweakUV91.out" "polyTweakUV92.ip";
connectAttr "polyTweakUV92.out" "polyTweakUV93.ip";
connectAttr "polyTweakUV93.out" "polyTweakUV94.ip";
connectAttr "polyTweakUV94.out" "polyTweakUV95.ip";
connectAttr "polyTweakUV95.out" "polyTweakUV96.ip";
connectAttr "polyTweakUV96.out" "polyTweakUV97.ip";
connectAttr "polyTweakUV97.out" "polyTweakUV98.ip";
connectAttr "polyTweakUV98.out" "polyTweakUV99.ip";
connectAttr "polyTweakUV99.out" "polyTweakUV100.ip";
connectAttr "polyTweakUV100.out" "polyTweakUV101.ip";
connectAttr "polyTweakUV101.out" "polyTweakUV102.ip";
connectAttr "polyTweakUV102.out" "polyTweakUV103.ip";
connectAttr "polyTweakUV103.out" "polyTweakUV104.ip";
connectAttr "polyTweakUV104.out" "polyTweakUV105.ip";
connectAttr "polyTweak14.out" "polyMergeVert4.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert4.mp";
connectAttr "polyTweakUV105.out" "polyTweak14.ip";
connectAttr "polyMergeVert4.out" "polyTweakUV106.ip";
connectAttr "polyTweakUV106.out" "polyTweakUV107.ip";
connectAttr "polyTweakUV107.out" "polyTweakUV108.ip";
connectAttr "polyTweakUV108.out" "polyTweakUV109.ip";
connectAttr "polyTweakUV109.out" "polyTweakUV110.ip";
connectAttr "polyTweakUV110.out" "polyTweakUV111.ip";
connectAttr "polyTweakUV111.out" "polyTweakUV112.ip";
connectAttr "polyTweakUV112.out" "polyTweakUV113.ip";
connectAttr "polyTweakUV113.out" "polyTweakUV114.ip";
connectAttr "polyTweakUV114.out" "polyTweakUV115.ip";
connectAttr "polyTweakUV115.out" "polyTweakUV116.ip";
connectAttr "polyTweakUV116.out" "polyTweakUV117.ip";
connectAttr "polyTweakUV117.out" "polyTweakUV118.ip";
connectAttr "polyTweakUV118.out" "polyTweakUV119.ip";
connectAttr "polyTweakUV119.out" "polyTweakUV120.ip";
connectAttr "polyTweakUV120.out" "polyTweakUV121.ip";
connectAttr "polyTweakUV121.out" "polyTweakUV122.ip";
connectAttr "polyTweakUV122.out" "polyTweakUV123.ip";
connectAttr "polyTweakUV123.out" "polyTweakUV124.ip";
connectAttr "polyTweakUV124.out" "polyTweakUV125.ip";
connectAttr "polyTweakUV125.out" "polyTweakUV126.ip";
connectAttr "polyTweakUV126.out" "polyTweakUV127.ip";
connectAttr "polyTweakUV127.out" "polyTweakUV128.ip";
connectAttr "polyTweakUV128.out" "polyTweakUV129.ip";
connectAttr "polyTweakUV129.out" "polyTweakUV130.ip";
connectAttr "polyTweakUV130.out" "polyTweakUV131.ip";
connectAttr "polyTweakUV131.out" "polyTweakUV132.ip";
connectAttr "polyTweakUV132.out" "polyTweakUV133.ip";
connectAttr "polyTweakUV133.out" "polyTweakUV134.ip";
connectAttr "polyTweakUV134.out" "polyTweakUV135.ip";
connectAttr "polyTweakUV135.out" "polyTweakUV136.ip";
connectAttr "polyTweakUV136.out" "polyTweakUV137.ip";
connectAttr "polyTweakUV137.out" "polyTweakUV138.ip";
connectAttr "polyTweakUV138.out" "polyTweakUV139.ip";
connectAttr "polyTweakUV139.out" "polyTweakUV140.ip";
connectAttr "polyTweak15.out" "polyMergeVert5.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert5.mp";
connectAttr "polyTweakUV140.out" "polyTweak15.ip";
connectAttr "polyMergeVert5.out" "polyTweakUV141.ip";
connectAttr "polyTweakUV141.out" "polyTweakUV142.ip";
connectAttr "polyTweakUV142.out" "polyTweakUV143.ip";
connectAttr "polyTweakUV143.out" "polyTweakUV144.ip";
connectAttr "polyTweakUV144.out" "polyTweakUV145.ip";
connectAttr "polyTweakUV145.out" "polyTweakUV146.ip";
connectAttr "polyTweakUV146.out" "polyTweakUV147.ip";
connectAttr "polyTweakUV147.out" "polyTweakUV148.ip";
connectAttr "polyTweakUV148.out" "polyTweakUV149.ip";
connectAttr "polyTweakUV149.out" "polyTweakUV150.ip";
connectAttr "polyTweakUV150.out" "polyTweakUV151.ip";
connectAttr "polyTweakUV151.out" "polyTweakUV152.ip";
connectAttr "polyTweakUV152.out" "polyTweakUV153.ip";
connectAttr "polyTweakUV153.out" "polyTweakUV154.ip";
connectAttr "polyTweakUV154.out" "polyTweakUV155.ip";
connectAttr "polyTweakUV155.out" "polyTweakUV156.ip";
connectAttr "polyTweakUV156.out" "polyTweakUV157.ip";
connectAttr "polyTweakUV157.out" "polyTweakUV158.ip";
connectAttr "polyTweakUV158.out" "polyTweakUV159.ip";
connectAttr "polyTweakUV159.out" "polyTweakUV160.ip";
connectAttr "polyTweakUV160.out" "polyTweakUV161.ip";
connectAttr "polyTweakUV161.out" "polyTweakUV162.ip";
connectAttr "polyTweakUV162.out" "polyTweakUV163.ip";
connectAttr "polyTweakUV163.out" "polyTweakUV164.ip";
connectAttr "polyTweakUV164.out" "polyTweakUV165.ip";
connectAttr "polyTweakUV165.out" "polyTweakUV166.ip";
connectAttr "polyTweakUV166.out" "polyTweakUV167.ip";
connectAttr "polyTweakUV167.out" "polyTweakUV168.ip";
connectAttr "polyTweakUV168.out" "polyTweakUV169.ip";
connectAttr "polyTweakUV169.out" "polyTweakUV170.ip";
connectAttr "polyTweakUV170.out" "polyTweakUV171.ip";
connectAttr "polyTweakUV171.out" "polyTweakUV172.ip";
connectAttr "polyTweakUV172.out" "polyTweakUV173.ip";
connectAttr "polyTweakUV173.out" "polyTweakUV174.ip";
connectAttr "polyTweakUV174.out" "polyTweakUV175.ip";
connectAttr "polyTweak16.out" "polyMergeVert6.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert6.mp";
connectAttr "polyTweakUV175.out" "polyTweak16.ip";
connectAttr "polyMergeVert6.out" "polyMergeVert7.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert7.mp";
connectAttr "polyMergeVert7.out" "polyMergeVert8.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert8.mp";
connectAttr "polyMergeVert8.out" "polySplit1.ip";
connectAttr "polySplit1.out" "deleteComponent6.ig";
connectAttr "deleteComponent6.og" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "polySplitEdge2.ip";
connectAttr "polySplitEdge2.out" "deleteComponent8.ig";
connectAttr "deleteComponent8.og" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "polyTweakUV176.ip";
connectAttr "polyTweakUV176.out" "polyTweakUV177.ip";
connectAttr "polyTweakUV177.out" "polyTweakUV178.ip";
connectAttr "polyTweakUV178.out" "polyTweakUV179.ip";
connectAttr "polyTweakUV179.out" "polyTweakUV180.ip";
connectAttr "polyTweakUV180.out" "polyTweakUV181.ip";
connectAttr "polyTweakUV181.out" "polyTweakUV182.ip";
connectAttr "polyTweakUV182.out" "polyTweakUV183.ip";
connectAttr "polyTweakUV183.out" "polyTweakUV184.ip";
connectAttr "polyTweakUV184.out" "polyTweakUV185.ip";
connectAttr "polyTweakUV185.out" "polyTweakUV186.ip";
connectAttr "polyTweakUV186.out" "polyTweakUV187.ip";
connectAttr "polyTweakUV187.out" "polyTweakUV188.ip";
connectAttr "polyTweakUV188.out" "polyTweakUV189.ip";
connectAttr "polyTweakUV189.out" "polyTweakUV190.ip";
connectAttr "polyTweakUV190.out" "polyTweakUV191.ip";
connectAttr "polyTweakUV191.out" "polyTweakUV192.ip";
connectAttr "polyTweakUV192.out" "polyTweakUV193.ip";
connectAttr "polyTweakUV193.out" "polyTweakUV194.ip";
connectAttr "polyTweakUV194.out" "polyTweakUV195.ip";
connectAttr "polyTweakUV195.out" "polyTweakUV196.ip";
connectAttr "polyTweakUV196.out" "polyTweakUV197.ip";
connectAttr "polyTweakUV197.out" "polyTweakUV198.ip";
connectAttr "polyTweakUV198.out" "polyTweakUV199.ip";
connectAttr "polyTweakUV199.out" "polyTweakUV200.ip";
connectAttr "polyTweakUV200.out" "polyTweakUV201.ip";
connectAttr "polyTweakUV201.out" "polyTweakUV202.ip";
connectAttr "polyTweakUV202.out" "polyTweakUV203.ip";
connectAttr "polyTweakUV203.out" "polyTweakUV204.ip";
connectAttr "polyTweakUV204.out" "polyTweakUV205.ip";
connectAttr "polyTweakUV205.out" "polyTweakUV206.ip";
connectAttr "polyTweakUV206.out" "polyTweakUV207.ip";
connectAttr "polyTweakUV207.out" "polyTweakUV208.ip";
connectAttr "polyTweakUV208.out" "polyTweakUV209.ip";
connectAttr "polyTweakUV209.out" "polyTweakUV210.ip";
connectAttr "polyTweak17.out" "polyMergeVert9.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert9.mp";
connectAttr "polyTweakUV210.out" "polyTweak17.ip";
connectAttr "polyMergeVert9.out" "polyTweakUV211.ip";
connectAttr "polyTweakUV211.out" "polyTweakUV212.ip";
connectAttr "polyTweakUV212.out" "polyTweakUV213.ip";
connectAttr "polyTweakUV213.out" "polyTweakUV214.ip";
connectAttr "polyTweakUV214.out" "polyTweakUV215.ip";
connectAttr "polyTweakUV215.out" "polyTweakUV216.ip";
connectAttr "polyTweakUV216.out" "polyTweakUV217.ip";
connectAttr "polyTweakUV217.out" "polyTweakUV218.ip";
connectAttr "polyTweakUV218.out" "polyTweakUV219.ip";
connectAttr "polyTweakUV219.out" "polyTweakUV220.ip";
connectAttr "polyTweakUV220.out" "polyTweakUV221.ip";
connectAttr "polyTweakUV221.out" "polyTweakUV222.ip";
connectAttr "polyTweakUV222.out" "polyTweakUV223.ip";
connectAttr "polyTweakUV223.out" "polyTweakUV224.ip";
connectAttr "polyTweakUV224.out" "polyTweakUV225.ip";
connectAttr "polyTweakUV225.out" "polyTweakUV226.ip";
connectAttr "polyTweakUV226.out" "polyTweakUV227.ip";
connectAttr "polyTweakUV227.out" "polyTweakUV228.ip";
connectAttr "polyTweakUV228.out" "polyTweakUV229.ip";
connectAttr "polyTweakUV229.out" "polyTweakUV230.ip";
connectAttr "polyTweakUV230.out" "polyTweakUV231.ip";
connectAttr "polyTweakUV231.out" "polyTweakUV232.ip";
connectAttr "polyTweakUV232.out" "polyTweakUV233.ip";
connectAttr "polyTweakUV233.out" "polyTweakUV234.ip";
connectAttr "polyTweakUV234.out" "polyTweakUV235.ip";
connectAttr "polyTweakUV235.out" "polyTweakUV236.ip";
connectAttr "polyTweakUV236.out" "polyTweakUV237.ip";
connectAttr "polyTweakUV237.out" "polyTweakUV238.ip";
connectAttr "polyTweakUV238.out" "polyTweakUV239.ip";
connectAttr "polyTweakUV239.out" "polyTweakUV240.ip";
connectAttr "polyTweakUV240.out" "polyTweakUV241.ip";
connectAttr "polyTweakUV241.out" "polyTweakUV242.ip";
connectAttr "polyTweakUV242.out" "polyTweakUV243.ip";
connectAttr "polyTweakUV243.out" "polyTweakUV244.ip";
connectAttr "polyTweakUV244.out" "polyTweakUV245.ip";
connectAttr "polyTweak18.out" "polyMergeVert10.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert10.mp";
connectAttr "polyTweakUV245.out" "polyTweak18.ip";
connectAttr "polyMergeVert10.out" "polyTweakUV246.ip";
connectAttr "polyTweakUV246.out" "polyTweakUV247.ip";
connectAttr "polyTweakUV247.out" "polyTweakUV248.ip";
connectAttr "polyTweakUV248.out" "polyTweakUV249.ip";
connectAttr "polyTweakUV249.out" "polyTweakUV250.ip";
connectAttr "polyTweakUV250.out" "polyTweakUV251.ip";
connectAttr "polyTweakUV251.out" "polyTweakUV252.ip";
connectAttr "polyTweakUV252.out" "polyTweakUV253.ip";
connectAttr "polyTweakUV253.out" "polyTweakUV254.ip";
connectAttr "polyTweakUV254.out" "polyTweakUV255.ip";
connectAttr "polyTweakUV255.out" "polyTweakUV256.ip";
connectAttr "polyTweakUV256.out" "polyTweakUV257.ip";
connectAttr "polyTweakUV257.out" "polyTweakUV258.ip";
connectAttr "polyTweakUV258.out" "polyTweakUV259.ip";
connectAttr "polyTweakUV259.out" "polyTweakUV260.ip";
connectAttr "polyTweakUV260.out" "polyTweakUV261.ip";
connectAttr "polyTweakUV261.out" "polyTweakUV262.ip";
connectAttr "polyTweakUV262.out" "polyTweakUV263.ip";
connectAttr "polyTweakUV263.out" "polyTweakUV264.ip";
connectAttr "polyTweakUV264.out" "polyTweakUV265.ip";
connectAttr "polyTweakUV265.out" "polyTweakUV266.ip";
connectAttr "polyTweakUV266.out" "polyTweakUV267.ip";
connectAttr "polyTweakUV267.out" "polyTweakUV268.ip";
connectAttr "polyTweakUV268.out" "polyTweakUV269.ip";
connectAttr "polyTweakUV269.out" "polyTweakUV270.ip";
connectAttr "polyTweakUV270.out" "polyTweakUV271.ip";
connectAttr "polyTweakUV271.out" "polyTweakUV272.ip";
connectAttr "polyTweakUV272.out" "polyTweakUV273.ip";
connectAttr "polyTweakUV273.out" "polyTweakUV274.ip";
connectAttr "polyTweakUV274.out" "polyTweakUV275.ip";
connectAttr "polyTweakUV275.out" "polyTweakUV276.ip";
connectAttr "polyTweakUV276.out" "polyTweakUV277.ip";
connectAttr "polyTweakUV277.out" "polyTweakUV278.ip";
connectAttr "polyTweakUV278.out" "polyTweakUV279.ip";
connectAttr "polyTweakUV279.out" "polyTweakUV280.ip";
connectAttr "polyTweak19.out" "polyMergeVert11.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert11.mp";
connectAttr "polyTweakUV280.out" "polyTweak19.ip";
connectAttr "polyMergeVert11.out" "polyTweakUV281.ip";
connectAttr "polyTweakUV281.out" "polyTweakUV282.ip";
connectAttr "polyTweakUV282.out" "polyTweakUV283.ip";
connectAttr "polyTweakUV283.out" "polyTweakUV284.ip";
connectAttr "polyTweakUV284.out" "polyTweakUV285.ip";
connectAttr "polyTweakUV285.out" "polyTweakUV286.ip";
connectAttr "polyTweakUV286.out" "polyTweakUV287.ip";
connectAttr "polyTweakUV287.out" "polyTweakUV288.ip";
connectAttr "polyTweakUV288.out" "polyTweakUV289.ip";
connectAttr "polyTweakUV289.out" "polyTweakUV290.ip";
connectAttr "polyTweakUV290.out" "polyTweakUV291.ip";
connectAttr "polyTweakUV291.out" "polyTweakUV292.ip";
connectAttr "polyTweakUV292.out" "polyTweakUV293.ip";
connectAttr "polyTweakUV293.out" "polyTweakUV294.ip";
connectAttr "polyTweakUV294.out" "polyTweakUV295.ip";
connectAttr "polyTweakUV295.out" "polyTweakUV296.ip";
connectAttr "polyTweakUV296.out" "polyTweakUV297.ip";
connectAttr "polyTweakUV297.out" "polyTweakUV298.ip";
connectAttr "polyTweakUV298.out" "polyTweakUV299.ip";
connectAttr "polyTweakUV299.out" "polyTweakUV300.ip";
connectAttr "polyTweakUV300.out" "polyTweakUV301.ip";
connectAttr "polyTweakUV301.out" "polyTweakUV302.ip";
connectAttr "polyTweakUV302.out" "polyTweakUV303.ip";
connectAttr "polyTweakUV303.out" "polyTweakUV304.ip";
connectAttr "polyTweakUV304.out" "polyTweakUV305.ip";
connectAttr "polyTweakUV305.out" "polyTweakUV306.ip";
connectAttr "polyTweakUV306.out" "polyTweakUV307.ip";
connectAttr "polyTweakUV307.out" "polyTweakUV308.ip";
connectAttr "polyTweakUV308.out" "polyTweakUV309.ip";
connectAttr "polyTweakUV309.out" "polyTweakUV310.ip";
connectAttr "polyTweakUV310.out" "polyTweakUV311.ip";
connectAttr "polyTweakUV311.out" "polyTweakUV312.ip";
connectAttr "polyTweakUV312.out" "polyTweakUV313.ip";
connectAttr "polyTweakUV313.out" "polyTweakUV314.ip";
connectAttr "polyTweakUV314.out" "polyTweakUV315.ip";
connectAttr "polyTweak20.out" "polyMergeVert12.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert12.mp";
connectAttr "polyTweakUV315.out" "polyTweak20.ip";
connectAttr "polyMergeVert12.out" "polyMergeVert13.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert13.mp";
connectAttr "polyMergeVert13.out" "polySplitEdge3.ip";
connectAttr "polyTweak21.out" "polyDelEdge1.ip";
connectAttr "polySplitEdge3.out" "polyTweak21.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_31__pntx.o" "polyTweak21.tk[31].tx"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_31__pnty.o" "polyTweak21.tk[31].ty"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_31__pntz.o" "polyTweak21.tk[31].tz"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_32__pntx.o" "polyTweak21.tk[32].tx"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_32__pnty.o" "polyTweak21.tk[32].ty"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_32__pntz.o" "polyTweak21.tk[32].tz"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_37__pntx.o" "polyTweak21.tk[37].tx"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_37__pnty.o" "polyTweak21.tk[37].ty"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_37__pntz.o" "polyTweak21.tk[37].tz"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_38__pntx.o" "polyTweak21.tk[38].tx"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_38__pnty.o" "polyTweak21.tk[38].ty"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_38__pntz.o" "polyTweak21.tk[38].tz"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_39__pntx.o" "polyTweak21.tk[39].tx"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_39__pnty.o" "polyTweak21.tk[39].ty"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_39__pntz.o" "polyTweak21.tk[39].tz"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_42__pntx.o" "polyTweak21.tk[42].tx"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_42__pnty.o" "polyTweak21.tk[42].ty"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_42__pntz.o" "polyTweak21.tk[42].tz"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_43__pntx.o" "polyTweak21.tk[43].tx"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_43__pnty.o" "polyTweak21.tk[43].ty"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_43__pntz.o" "polyTweak21.tk[43].tz"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_46__pntx.o" "polyTweak21.tk[46].tx"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_46__pnty.o" "polyTweak21.tk[46].ty"
		;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape_pnts_46__pntz.o" "polyTweak21.tk[46].tz"
		;
connectAttr "polyDelEdge1.out" "polyTweak22.ip";
connectAttr "polyTweak22.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polyTweakUV316.ip";
connectAttr "polyTweakUV316.out" "polyTweakUV317.ip";
connectAttr "polyTweakUV317.out" "polyTweakUV318.ip";
connectAttr "polyTweakUV318.out" "polyTweakUV319.ip";
connectAttr "polyTweakUV319.out" "polyTweakUV320.ip";
connectAttr "polyTweakUV320.out" "polyTweakUV321.ip";
connectAttr "polyTweakUV321.out" "polyTweakUV322.ip";
connectAttr "polyTweakUV322.out" "polyTweakUV323.ip";
connectAttr "polyTweakUV323.out" "polyTweakUV324.ip";
connectAttr "polyTweakUV324.out" "polyTweakUV325.ip";
connectAttr "polyTweakUV325.out" "polyTweakUV326.ip";
connectAttr "polyTweakUV326.out" "polyTweakUV327.ip";
connectAttr "polyTweakUV327.out" "polyTweakUV328.ip";
connectAttr "polyTweakUV328.out" "polyTweakUV329.ip";
connectAttr "polyTweakUV329.out" "polyTweakUV330.ip";
connectAttr "polyTweakUV330.out" "polyTweakUV331.ip";
connectAttr "polyTweakUV331.out" "polyTweakUV332.ip";
connectAttr "polyTweakUV332.out" "polyTweakUV333.ip";
connectAttr "polyTweakUV333.out" "polyTweakUV334.ip";
connectAttr "polyTweakUV334.out" "polyTweakUV335.ip";
connectAttr "polyTweakUV335.out" "polyTweakUV336.ip";
connectAttr "polyTweakUV336.out" "polyTweakUV337.ip";
connectAttr "polyTweakUV337.out" "polyTweakUV338.ip";
connectAttr "polyTweakUV338.out" "polyTweakUV339.ip";
connectAttr "polyTweakUV339.out" "polyTweakUV340.ip";
connectAttr "polyTweakUV340.out" "polyTweakUV341.ip";
connectAttr "polyTweakUV341.out" "polyTweakUV342.ip";
connectAttr "polyTweakUV342.out" "polyTweakUV343.ip";
connectAttr "polyTweakUV343.out" "polyTweakUV344.ip";
connectAttr "polyTweakUV344.out" "polyTweakUV345.ip";
connectAttr "polyTweakUV345.out" "polyTweakUV346.ip";
connectAttr "polyTweakUV346.out" "polyTweakUV347.ip";
connectAttr "polyTweakUV347.out" "polyTweakUV348.ip";
connectAttr "polyTweakUV348.out" "polyTweakUV349.ip";
connectAttr "polyTweakUV349.out" "polyTweakUV350.ip";
connectAttr "polyTweak23.out" "polyMergeVert14.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert14.mp";
connectAttr "polyTweakUV350.out" "polyTweak23.ip";
connectAttr "polyMergeVert14.out" "polyTweakUV351.ip";
connectAttr "polyTweakUV351.out" "polyTweakUV352.ip";
connectAttr "polyTweakUV352.out" "polyTweakUV353.ip";
connectAttr "polyTweakUV353.out" "polyTweakUV354.ip";
connectAttr "polyTweakUV354.out" "polyTweakUV355.ip";
connectAttr "polyTweakUV355.out" "polyTweakUV356.ip";
connectAttr "polyTweakUV356.out" "polyTweakUV357.ip";
connectAttr "polyTweakUV357.out" "polyTweakUV358.ip";
connectAttr "polyTweakUV358.out" "polyTweakUV359.ip";
connectAttr "polyTweakUV359.out" "polyTweakUV360.ip";
connectAttr "polyTweakUV360.out" "polyTweakUV361.ip";
connectAttr "polyTweakUV361.out" "polyTweakUV362.ip";
connectAttr "polyTweakUV362.out" "polyTweakUV363.ip";
connectAttr "polyTweakUV363.out" "polyTweakUV364.ip";
connectAttr "polyTweakUV364.out" "polyTweakUV365.ip";
connectAttr "polyTweakUV365.out" "polyTweakUV366.ip";
connectAttr "polyTweakUV366.out" "polyTweakUV367.ip";
connectAttr "polyTweakUV367.out" "polyTweakUV368.ip";
connectAttr "polyTweakUV368.out" "polyTweakUV369.ip";
connectAttr "polyTweakUV369.out" "polyTweakUV370.ip";
connectAttr "polyTweakUV370.out" "polyTweakUV371.ip";
connectAttr "polyTweakUV371.out" "polyTweakUV372.ip";
connectAttr "polyTweakUV372.out" "polyTweakUV373.ip";
connectAttr "polyTweakUV373.out" "polyTweakUV374.ip";
connectAttr "polyTweakUV374.out" "polyTweakUV375.ip";
connectAttr "polyTweakUV375.out" "polyTweakUV376.ip";
connectAttr "polyTweakUV376.out" "polyTweakUV377.ip";
connectAttr "polyTweakUV377.out" "polyTweakUV378.ip";
connectAttr "polyTweakUV378.out" "polyTweakUV379.ip";
connectAttr "polyTweakUV379.out" "polyTweakUV380.ip";
connectAttr "polyTweakUV380.out" "polyTweakUV381.ip";
connectAttr "polyTweakUV381.out" "polyTweakUV382.ip";
connectAttr "polyTweakUV382.out" "polyTweakUV383.ip";
connectAttr "polyTweakUV383.out" "polyTweakUV384.ip";
connectAttr "polyTweakUV384.out" "polyTweakUV385.ip";
connectAttr "polyTweak24.out" "polyMergeVert15.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert15.mp";
connectAttr "polyTweakUV385.out" "polyTweak24.ip";
connectAttr "polyMergeVert15.out" "polyMergeVert16.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert16.mp";
connectAttr "polyMergeVert16.out" "polyBridgeEdge3.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBridgeEdge3.mp";
connectAttr "polyBridgeEdge3.out" "polyMergeVert17.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert17.mp";
connectAttr "polyMergeVert17.out" "polyTweak25.ip";
connectAttr "polyTweak25.out" "deleteComponent10.ig";
connectAttr "deleteComponent10.og" "deleteComponent11.ig";
connectAttr "deleteComponent11.og" "polyTweakUV386.ip";
connectAttr "polyTweakUV386.out" "polyTweakUV387.ip";
connectAttr "polyTweakUV387.out" "polyTweakUV388.ip";
connectAttr "polyTweakUV388.out" "polyTweakUV389.ip";
connectAttr "polyTweakUV389.out" "polyTweakUV390.ip";
connectAttr "polyTweakUV390.out" "polyTweakUV391.ip";
connectAttr "polyTweakUV391.out" "polyTweakUV392.ip";
connectAttr "polyTweakUV392.out" "polyTweakUV393.ip";
connectAttr "polyTweakUV393.out" "polyTweakUV394.ip";
connectAttr "polyTweakUV394.out" "polyTweakUV395.ip";
connectAttr "polyTweakUV395.out" "polyTweakUV396.ip";
connectAttr "polyTweakUV396.out" "polyTweakUV397.ip";
connectAttr "polyTweakUV397.out" "polyTweakUV398.ip";
connectAttr "polyTweakUV398.out" "polyTweakUV399.ip";
connectAttr "polyTweakUV399.out" "polyTweakUV400.ip";
connectAttr "polyTweakUV400.out" "polyTweakUV401.ip";
connectAttr "polyTweakUV401.out" "polyTweakUV402.ip";
connectAttr "polyTweakUV402.out" "polyTweakUV403.ip";
connectAttr "polyTweakUV403.out" "polyTweakUV404.ip";
connectAttr "polyTweakUV404.out" "polyTweakUV405.ip";
connectAttr "polyTweakUV405.out" "polyTweakUV406.ip";
connectAttr "polyTweakUV406.out" "polyTweakUV407.ip";
connectAttr "polyTweakUV407.out" "polyTweakUV408.ip";
connectAttr "polyTweakUV408.out" "polyTweakUV409.ip";
connectAttr "polyTweakUV409.out" "polyTweakUV410.ip";
connectAttr "polyTweakUV410.out" "polyTweakUV411.ip";
connectAttr "polyTweakUV411.out" "polyTweakUV412.ip";
connectAttr "polyTweakUV412.out" "polyTweakUV413.ip";
connectAttr "polyTweakUV413.out" "polyTweakUV414.ip";
connectAttr "polyTweakUV414.out" "polyTweakUV415.ip";
connectAttr "polyTweakUV415.out" "polyTweakUV416.ip";
connectAttr "polyTweakUV416.out" "polyTweakUV417.ip";
connectAttr "polyTweakUV417.out" "polyTweakUV418.ip";
connectAttr "polyTweakUV418.out" "polyTweakUV419.ip";
connectAttr "polyTweakUV419.out" "polyTweakUV420.ip";
connectAttr "polyTweak26.out" "polyMergeVert18.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert18.mp";
connectAttr "polyTweakUV420.out" "polyTweak26.ip";
connectAttr "polyMergeVert18.out" "deleteComponent12.ig";
connectAttr "deleteComponent12.og" "deleteComponent13.ig";
connectAttr "deleteComponent13.og" "polyTweakUV421.ip";
connectAttr "polyTweakUV421.out" "polyTweakUV422.ip";
connectAttr "polyTweakUV422.out" "polyTweakUV423.ip";
connectAttr "polyTweakUV423.out" "polyTweakUV424.ip";
connectAttr "polyTweakUV424.out" "polyTweakUV425.ip";
connectAttr "polyTweakUV425.out" "polyTweakUV426.ip";
connectAttr "polyTweakUV426.out" "polyTweakUV427.ip";
connectAttr "polyTweakUV427.out" "polyTweakUV428.ip";
connectAttr "polyTweakUV428.out" "polyTweakUV429.ip";
connectAttr "polyTweakUV429.out" "polyTweakUV430.ip";
connectAttr "polyTweakUV430.out" "polyTweakUV431.ip";
connectAttr "polyTweakUV431.out" "polyTweakUV432.ip";
connectAttr "polyTweakUV432.out" "polyTweakUV433.ip";
connectAttr "polyTweakUV433.out" "polyTweakUV434.ip";
connectAttr "polyTweakUV434.out" "polyTweakUV435.ip";
connectAttr "polyTweakUV435.out" "polyTweakUV436.ip";
connectAttr "polyTweakUV436.out" "polyTweakUV437.ip";
connectAttr "polyTweakUV437.out" "polyTweakUV438.ip";
connectAttr "polyTweakUV438.out" "polyTweakUV439.ip";
connectAttr "polyTweakUV439.out" "polyTweakUV440.ip";
connectAttr "polyTweakUV440.out" "polyTweakUV441.ip";
connectAttr "polyTweakUV441.out" "polyTweakUV442.ip";
connectAttr "polyTweakUV442.out" "polyTweakUV443.ip";
connectAttr "polyTweakUV443.out" "polyTweakUV444.ip";
connectAttr "polyTweakUV444.out" "polyTweakUV445.ip";
connectAttr "polyTweakUV445.out" "polyTweakUV446.ip";
connectAttr "polyTweakUV446.out" "polyTweakUV447.ip";
connectAttr "polyTweakUV447.out" "polyTweakUV448.ip";
connectAttr "polyTweakUV448.out" "polyTweakUV449.ip";
connectAttr "polyTweakUV449.out" "polyTweakUV450.ip";
connectAttr "polyTweakUV450.out" "polyTweakUV451.ip";
connectAttr "polyTweakUV451.out" "polyTweakUV452.ip";
connectAttr "polyTweakUV452.out" "polyTweakUV453.ip";
connectAttr "polyTweakUV453.out" "polyTweakUV454.ip";
connectAttr "polyTweakUV454.out" "polyTweakUV455.ip";
connectAttr "polyTweak27.out" "polyMergeVert19.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert19.mp";
connectAttr "polyTweakUV455.out" "polyTweak27.ip";
connectAttr "polyMergeVert19.out" "polyTweak28.ip";
connectAttr "polyTweak28.out" "deleteComponent14.ig";
connectAttr "deleteComponent14.og" "deleteComponent15.ig";
connectAttr "deleteComponent15.og" "polyTweakUV456.ip";
connectAttr "polyTweakUV456.out" "polyTweakUV457.ip";
connectAttr "polyTweakUV457.out" "polyTweakUV458.ip";
connectAttr "polyTweakUV458.out" "polyTweakUV459.ip";
connectAttr "polyTweakUV459.out" "polyTweakUV460.ip";
connectAttr "polyTweakUV460.out" "polyTweakUV461.ip";
connectAttr "polyTweakUV461.out" "polyTweakUV462.ip";
connectAttr "polyTweakUV462.out" "polyTweakUV463.ip";
connectAttr "polyTweakUV463.out" "polyTweakUV464.ip";
connectAttr "polyTweakUV464.out" "polyTweakUV465.ip";
connectAttr "polyTweakUV465.out" "polyTweakUV466.ip";
connectAttr "polyTweakUV466.out" "polyTweakUV467.ip";
connectAttr "polyTweakUV467.out" "polyTweakUV468.ip";
connectAttr "polyTweakUV468.out" "polyTweakUV469.ip";
connectAttr "polyTweakUV469.out" "polyTweakUV470.ip";
connectAttr "polyTweakUV470.out" "polyTweakUV471.ip";
connectAttr "polyTweakUV471.out" "polyTweakUV472.ip";
connectAttr "polyTweakUV472.out" "polyTweakUV473.ip";
connectAttr "polyTweakUV473.out" "polyTweakUV474.ip";
connectAttr "polyTweakUV474.out" "polyTweakUV475.ip";
connectAttr "polyTweakUV475.out" "polyTweakUV476.ip";
connectAttr "polyTweakUV476.out" "polyTweakUV477.ip";
connectAttr "polyTweakUV477.out" "polyTweakUV478.ip";
connectAttr "polyTweakUV478.out" "polyTweakUV479.ip";
connectAttr "polyTweakUV479.out" "polyTweakUV480.ip";
connectAttr "polyTweakUV480.out" "polyTweakUV481.ip";
connectAttr "polyTweakUV481.out" "polyTweakUV482.ip";
connectAttr "polyTweakUV482.out" "polyTweakUV483.ip";
connectAttr "polyTweakUV483.out" "polyTweakUV484.ip";
connectAttr "polyTweakUV484.out" "polyTweakUV485.ip";
connectAttr "polyTweakUV485.out" "polyTweakUV486.ip";
connectAttr "polyTweakUV486.out" "polyTweakUV487.ip";
connectAttr "polyTweakUV487.out" "polyTweakUV488.ip";
connectAttr "polyTweakUV488.out" "polyTweakUV489.ip";
connectAttr "polyTweakUV489.out" "polyTweakUV490.ip";
connectAttr "polyTweakUV490.out" "polyMergeVert20.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert20.mp";
connectAttr "polyMergeVert20.out" "polyTweakUV491.ip";
connectAttr "polyTweakUV491.out" "polyTweakUV492.ip";
connectAttr "polyTweakUV492.out" "polyTweakUV493.ip";
connectAttr "polyTweakUV493.out" "polyTweakUV494.ip";
connectAttr "polyTweakUV494.out" "polyTweakUV495.ip";
connectAttr "polyTweakUV495.out" "polyTweakUV496.ip";
connectAttr "polyTweakUV496.out" "polyTweakUV497.ip";
connectAttr "polyTweakUV497.out" "polyTweakUV498.ip";
connectAttr "polyTweakUV498.out" "polyTweakUV499.ip";
connectAttr "polyTweakUV499.out" "polyTweakUV500.ip";
connectAttr "polyTweakUV500.out" "polyTweakUV501.ip";
connectAttr "polyTweakUV501.out" "polyTweakUV502.ip";
connectAttr "polyTweakUV502.out" "polyTweakUV503.ip";
connectAttr "polyTweakUV503.out" "polyTweakUV504.ip";
connectAttr "polyTweakUV504.out" "polyTweakUV505.ip";
connectAttr "polyTweakUV505.out" "polyTweakUV506.ip";
connectAttr "polyTweakUV506.out" "polyTweakUV507.ip";
connectAttr "polyTweakUV507.out" "polyTweakUV508.ip";
connectAttr "polyTweakUV508.out" "polyTweakUV509.ip";
connectAttr "polyTweakUV509.out" "polyTweakUV510.ip";
connectAttr "polyTweakUV510.out" "polyTweakUV511.ip";
connectAttr "polyTweakUV511.out" "polyTweakUV512.ip";
connectAttr "polyTweakUV512.out" "polyTweakUV513.ip";
connectAttr "polyTweakUV513.out" "polyTweakUV514.ip";
connectAttr "polyTweakUV514.out" "polyTweakUV515.ip";
connectAttr "polyTweakUV515.out" "polyTweakUV516.ip";
connectAttr "polyTweakUV516.out" "polyTweakUV517.ip";
connectAttr "polyTweakUV517.out" "polyTweakUV518.ip";
connectAttr "polyTweakUV518.out" "polyTweakUV519.ip";
connectAttr "polyTweakUV519.out" "polyTweakUV520.ip";
connectAttr "polyTweakUV520.out" "polyTweakUV521.ip";
connectAttr "polyTweakUV521.out" "polyTweakUV522.ip";
connectAttr "polyTweakUV522.out" "polyTweakUV523.ip";
connectAttr "polyTweakUV523.out" "polyTweakUV524.ip";
connectAttr "polyTweakUV524.out" "polyTweakUV525.ip";
connectAttr "polyTweak29.out" "polyMergeVert21.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert21.mp";
connectAttr "polyTweakUV525.out" "polyTweak29.ip";
connectAttr "polyMergeVert21.out" "polyTweak30.ip";
connectAttr "polyTweak30.out" "deleteComponent16.ig";
connectAttr "deleteComponent16.og" "deleteComponent17.ig";
connectAttr "deleteComponent17.og" "polyTweakUV526.ip";
connectAttr "polyTweakUV526.out" "polyTweakUV527.ip";
connectAttr "polyTweakUV527.out" "polyTweakUV528.ip";
connectAttr "polyTweakUV528.out" "polyTweakUV529.ip";
connectAttr "polyTweakUV529.out" "polyTweakUV530.ip";
connectAttr "polyTweakUV530.out" "polyTweakUV531.ip";
connectAttr "polyTweakUV531.out" "polyTweakUV532.ip";
connectAttr "polyTweakUV532.out" "polyTweakUV533.ip";
connectAttr "polyTweakUV533.out" "polyTweakUV534.ip";
connectAttr "polyTweakUV534.out" "polyTweakUV535.ip";
connectAttr "polyTweakUV535.out" "polyTweakUV536.ip";
connectAttr "polyTweakUV536.out" "polyTweakUV537.ip";
connectAttr "polyTweakUV537.out" "polyTweakUV538.ip";
connectAttr "polyTweakUV538.out" "polyTweakUV539.ip";
connectAttr "polyTweakUV539.out" "polyTweakUV540.ip";
connectAttr "polyTweakUV540.out" "polyTweakUV541.ip";
connectAttr "polyTweakUV541.out" "polyTweakUV542.ip";
connectAttr "polyTweakUV542.out" "polyTweakUV543.ip";
connectAttr "polyTweakUV543.out" "polyTweakUV544.ip";
connectAttr "polyTweakUV544.out" "polyTweakUV545.ip";
connectAttr "polyTweakUV545.out" "polyTweakUV546.ip";
connectAttr "polyTweakUV546.out" "polyTweakUV547.ip";
connectAttr "polyTweakUV547.out" "polyTweakUV548.ip";
connectAttr "polyTweakUV548.out" "polyTweakUV549.ip";
connectAttr "polyTweakUV549.out" "polyTweakUV550.ip";
connectAttr "polyTweakUV550.out" "polyTweakUV551.ip";
connectAttr "polyTweakUV551.out" "polyTweakUV552.ip";
connectAttr "polyTweakUV552.out" "polyTweakUV553.ip";
connectAttr "polyTweakUV553.out" "polyTweakUV554.ip";
connectAttr "polyTweakUV554.out" "polyTweakUV555.ip";
connectAttr "polyTweakUV555.out" "polyTweakUV556.ip";
connectAttr "polyTweakUV556.out" "polyTweakUV557.ip";
connectAttr "polyTweakUV557.out" "polyTweakUV558.ip";
connectAttr "polyTweakUV558.out" "polyTweakUV559.ip";
connectAttr "polyTweakUV559.out" "polyTweakUV560.ip";
connectAttr "polyTweak31.out" "polyMergeVert22.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert22.mp";
connectAttr "polyTweakUV560.out" "polyTweak31.ip";
connectAttr "polyMergeVert22.out" "polyTweakUV561.ip";
connectAttr "polyTweakUV561.out" "polyTweakUV562.ip";
connectAttr "polyTweakUV562.out" "polyTweakUV563.ip";
connectAttr "polyTweakUV563.out" "polyTweakUV564.ip";
connectAttr "polyTweakUV564.out" "polyTweakUV565.ip";
connectAttr "polyTweakUV565.out" "polyTweakUV566.ip";
connectAttr "polyTweakUV566.out" "polyTweakUV567.ip";
connectAttr "polyTweakUV567.out" "polyTweakUV568.ip";
connectAttr "polyTweakUV568.out" "polyTweakUV569.ip";
connectAttr "polyTweakUV569.out" "polyTweakUV570.ip";
connectAttr "polyTweakUV570.out" "polyTweakUV571.ip";
connectAttr "polyTweakUV571.out" "polyTweakUV572.ip";
connectAttr "polyTweakUV572.out" "polyTweakUV573.ip";
connectAttr "polyTweakUV573.out" "polyTweakUV574.ip";
connectAttr "polyTweakUV574.out" "polyTweakUV575.ip";
connectAttr "polyTweakUV575.out" "polyTweakUV576.ip";
connectAttr "polyTweakUV576.out" "polyTweakUV577.ip";
connectAttr "polyTweakUV577.out" "polyTweakUV578.ip";
connectAttr "polyTweakUV578.out" "polyTweakUV579.ip";
connectAttr "polyTweakUV579.out" "polyTweakUV580.ip";
connectAttr "polyTweakUV580.out" "polyTweakUV581.ip";
connectAttr "polyTweakUV581.out" "polyTweakUV582.ip";
connectAttr "polyTweakUV582.out" "polyTweakUV583.ip";
connectAttr "polyTweakUV583.out" "polyTweakUV584.ip";
connectAttr "polyTweakUV584.out" "polyTweakUV585.ip";
connectAttr "polyTweakUV585.out" "polyTweakUV586.ip";
connectAttr "polyTweakUV586.out" "polyTweakUV587.ip";
connectAttr "polyTweakUV587.out" "polyTweakUV588.ip";
connectAttr "polyTweakUV588.out" "polyTweakUV589.ip";
connectAttr "polyTweakUV589.out" "polyTweakUV590.ip";
connectAttr "polyTweakUV590.out" "polyTweakUV591.ip";
connectAttr "polyTweakUV591.out" "polyTweakUV592.ip";
connectAttr "polyTweakUV592.out" "polyTweakUV593.ip";
connectAttr "polyTweakUV593.out" "polyTweakUV594.ip";
connectAttr "polyTweakUV594.out" "polyTweakUV595.ip";
connectAttr "polyTweak32.out" "polyMergeVert23.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert23.mp";
connectAttr "polyTweakUV595.out" "polyTweak32.ip";
connectAttr "polyMergeVert23.out" "polyTweak33.ip";
connectAttr "polyTweak33.out" "deleteComponent18.ig";
connectAttr "deleteComponent18.og" "deleteComponent19.ig";
connectAttr "deleteComponent19.og" "polyTweakUV596.ip";
connectAttr "polyTweakUV596.out" "polyTweakUV597.ip";
connectAttr "polyTweakUV597.out" "polyTweakUV598.ip";
connectAttr "polyTweakUV598.out" "polyTweakUV599.ip";
connectAttr "polyTweakUV599.out" "polyTweakUV600.ip";
connectAttr "polyTweakUV600.out" "polyTweakUV601.ip";
connectAttr "polyTweakUV601.out" "polyTweakUV602.ip";
connectAttr "polyTweakUV602.out" "polyTweakUV603.ip";
connectAttr "polyTweakUV603.out" "polyTweakUV604.ip";
connectAttr "polyTweakUV604.out" "polyTweakUV605.ip";
connectAttr "polyTweakUV605.out" "polyTweakUV606.ip";
connectAttr "polyTweakUV606.out" "polyTweakUV607.ip";
connectAttr "polyTweakUV607.out" "polyTweakUV608.ip";
connectAttr "polyTweakUV608.out" "polyTweakUV609.ip";
connectAttr "polyTweakUV609.out" "polyTweakUV610.ip";
connectAttr "polyTweakUV610.out" "polyTweakUV611.ip";
connectAttr "polyTweakUV611.out" "polyTweakUV612.ip";
connectAttr "polyTweakUV612.out" "polyTweakUV613.ip";
connectAttr "polyTweakUV613.out" "polyTweakUV614.ip";
connectAttr "polyTweakUV614.out" "polyTweakUV615.ip";
connectAttr "polyTweakUV615.out" "polyTweakUV616.ip";
connectAttr "polyTweakUV616.out" "polyTweakUV617.ip";
connectAttr "polyTweakUV617.out" "polyTweakUV618.ip";
connectAttr "polyTweakUV618.out" "polyTweakUV619.ip";
connectAttr "polyTweakUV619.out" "polyTweakUV620.ip";
connectAttr "polyTweakUV620.out" "polyTweakUV621.ip";
connectAttr "polyTweakUV621.out" "polyTweakUV622.ip";
connectAttr "polyTweakUV622.out" "polyTweakUV623.ip";
connectAttr "polyTweakUV623.out" "polyTweakUV624.ip";
connectAttr "polyTweakUV624.out" "polyTweakUV625.ip";
connectAttr "polyTweakUV625.out" "polyTweakUV626.ip";
connectAttr "polyTweakUV626.out" "polyTweakUV627.ip";
connectAttr "polyTweakUV627.out" "polyTweakUV628.ip";
connectAttr "polyTweakUV628.out" "polyTweakUV629.ip";
connectAttr "polyTweakUV629.out" "polyTweakUV630.ip";
connectAttr "polyTweak34.out" "polyMergeVert24.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert24.mp";
connectAttr "polyTweakUV630.out" "polyTweak34.ip";
connectAttr "polyMergeVert24.out" "polyTweakUV631.ip";
connectAttr "polyTweakUV631.out" "polyTweakUV632.ip";
connectAttr "polyTweakUV632.out" "polyTweakUV633.ip";
connectAttr "polyTweakUV633.out" "polyTweakUV634.ip";
connectAttr "polyTweakUV634.out" "polyTweakUV635.ip";
connectAttr "polyTweakUV635.out" "polyTweakUV636.ip";
connectAttr "polyTweakUV636.out" "polyTweakUV637.ip";
connectAttr "polyTweakUV637.out" "polyTweakUV638.ip";
connectAttr "polyTweakUV638.out" "polyTweakUV639.ip";
connectAttr "polyTweakUV639.out" "polyTweakUV640.ip";
connectAttr "polyTweakUV640.out" "polyTweakUV641.ip";
connectAttr "polyTweakUV641.out" "polyTweakUV642.ip";
connectAttr "polyTweakUV642.out" "polyTweakUV643.ip";
connectAttr "polyTweakUV643.out" "polyTweakUV644.ip";
connectAttr "polyTweakUV644.out" "polyTweakUV645.ip";
connectAttr "polyTweakUV645.out" "polyTweakUV646.ip";
connectAttr "polyTweakUV646.out" "polyTweakUV647.ip";
connectAttr "polyTweakUV647.out" "polyTweakUV648.ip";
connectAttr "polyTweakUV648.out" "polyTweakUV649.ip";
connectAttr "polyTweakUV649.out" "polyTweakUV650.ip";
connectAttr "polyTweakUV650.out" "polyTweakUV651.ip";
connectAttr "polyTweakUV651.out" "polyTweakUV652.ip";
connectAttr "polyTweakUV652.out" "polyTweakUV653.ip";
connectAttr "polyTweakUV653.out" "polyTweakUV654.ip";
connectAttr "polyTweakUV654.out" "polyTweakUV655.ip";
connectAttr "polyTweakUV655.out" "polyTweakUV656.ip";
connectAttr "polyTweakUV656.out" "polyTweakUV657.ip";
connectAttr "polyTweakUV657.out" "polyTweakUV658.ip";
connectAttr "polyTweakUV658.out" "polyTweakUV659.ip";
connectAttr "polyTweakUV659.out" "polyTweakUV660.ip";
connectAttr "polyTweakUV660.out" "polyTweakUV661.ip";
connectAttr "polyTweakUV661.out" "polyTweakUV662.ip";
connectAttr "polyTweakUV662.out" "polyTweakUV663.ip";
connectAttr "polyTweakUV663.out" "polyTweakUV664.ip";
connectAttr "polyTweakUV664.out" "polyTweakUV665.ip";
connectAttr "polyTweak35.out" "polyMergeVert25.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert25.mp";
connectAttr "polyTweakUV665.out" "polyTweak35.ip";
connectAttr "polyMergeVert25.out" "polyTweak36.ip";
connectAttr "polyTweak36.out" "deleteComponent20.ig";
connectAttr "deleteComponent20.og" "deleteComponent21.ig";
connectAttr "deleteComponent21.og" "deleteComponent22.ig";
connectAttr "deleteComponent22.og" "deleteComponent23.ig";
connectAttr "deleteComponent23.og" "deleteComponent24.ig";
connectAttr "deleteComponent24.og" "deleteComponent25.ig";
connectAttr "deleteComponent25.og" "deleteComponent26.ig";
connectAttr "deleteComponent26.og" "deleteComponent27.ig";
connectAttr "deleteComponent27.og" "deleteComponent28.ig";
connectAttr "deleteComponent28.og" "polyMirror1.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1.sp" "polyMirror1.sp";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMirror1.mp";
connectAttr "polyMirror1.out" "polyTweak37.ip";
connectAttr "polyTweak37.out" "deleteComponent29.ig";
connectAttr "deleteComponent29.og" "deleteComponent30.ig";
connectAttr "deleteComponent30.og" "polyTweakUV666.ip";
connectAttr "polyTweakUV666.out" "polyTweakUV667.ip";
connectAttr "polyTweakUV667.out" "polyTweakUV668.ip";
connectAttr "polyTweakUV668.out" "polyTweakUV669.ip";
connectAttr "polyTweakUV669.out" "polyTweakUV670.ip";
connectAttr "polyTweakUV670.out" "polyTweakUV671.ip";
connectAttr "polyTweakUV671.out" "polyTweakUV672.ip";
connectAttr "polyTweakUV672.out" "polyTweakUV673.ip";
connectAttr "polyTweakUV673.out" "polyTweakUV674.ip";
connectAttr "polyTweakUV674.out" "polyTweakUV675.ip";
connectAttr "polyTweakUV675.out" "polyTweakUV676.ip";
connectAttr "polyTweakUV676.out" "polyTweakUV677.ip";
connectAttr "polyTweakUV677.out" "polyTweakUV678.ip";
connectAttr "polyTweakUV678.out" "polyTweakUV679.ip";
connectAttr "polyTweakUV679.out" "polyTweakUV680.ip";
connectAttr "polyTweakUV680.out" "polyTweakUV681.ip";
connectAttr "polyTweakUV681.out" "polyTweakUV682.ip";
connectAttr "polyTweakUV682.out" "polyTweakUV683.ip";
connectAttr "polyTweakUV683.out" "polyTweakUV684.ip";
connectAttr "polyTweakUV684.out" "polyTweakUV685.ip";
connectAttr "polyTweakUV685.out" "polyTweakUV686.ip";
connectAttr "polyTweakUV686.out" "polyTweakUV687.ip";
connectAttr "polyTweakUV687.out" "polyTweakUV688.ip";
connectAttr "polyTweakUV688.out" "polyTweakUV689.ip";
connectAttr "polyTweakUV689.out" "polyTweakUV690.ip";
connectAttr "polyTweakUV690.out" "polyTweakUV691.ip";
connectAttr "polyTweakUV691.out" "polyTweakUV692.ip";
connectAttr "polyTweakUV692.out" "polyTweakUV693.ip";
connectAttr "polyTweakUV693.out" "polyTweakUV694.ip";
connectAttr "polyTweakUV694.out" "polyTweakUV695.ip";
connectAttr "polyTweakUV695.out" "polyTweakUV696.ip";
connectAttr "polyTweakUV696.out" "polyTweakUV697.ip";
connectAttr "polyTweakUV697.out" "polyTweakUV698.ip";
connectAttr "polyTweakUV698.out" "polyTweakUV699.ip";
connectAttr "polyTweakUV699.out" "polyTweakUV700.ip";
connectAttr "polyTweak38.out" "polyMergeVert26.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert26.mp";
connectAttr "polyTweakUV700.out" "polyTweak38.ip";
connectAttr "polyMergeVert26.out" "polyTweakUV701.ip";
connectAttr "polyTweakUV701.out" "polyTweakUV702.ip";
connectAttr "polyTweakUV702.out" "polyTweakUV703.ip";
connectAttr "polyTweakUV703.out" "polyTweakUV704.ip";
connectAttr "polyTweakUV704.out" "polyTweakUV705.ip";
connectAttr "polyTweakUV705.out" "polyTweakUV706.ip";
connectAttr "polyTweakUV706.out" "polyTweakUV707.ip";
connectAttr "polyTweakUV707.out" "polyTweakUV708.ip";
connectAttr "polyTweakUV708.out" "polyTweakUV709.ip";
connectAttr "polyTweakUV709.out" "polyTweakUV710.ip";
connectAttr "polyTweakUV710.out" "polyTweakUV711.ip";
connectAttr "polyTweakUV711.out" "polyTweakUV712.ip";
connectAttr "polyTweakUV712.out" "polyTweakUV713.ip";
connectAttr "polyTweakUV713.out" "polyTweakUV714.ip";
connectAttr "polyTweakUV714.out" "polyTweakUV715.ip";
connectAttr "polyTweakUV715.out" "polyTweakUV716.ip";
connectAttr "polyTweakUV716.out" "polyTweakUV717.ip";
connectAttr "polyTweakUV717.out" "polyTweakUV718.ip";
connectAttr "polyTweakUV718.out" "polyTweakUV719.ip";
connectAttr "polyTweakUV719.out" "polyTweakUV720.ip";
connectAttr "polyTweakUV720.out" "polyTweakUV721.ip";
connectAttr "polyTweakUV721.out" "polyTweakUV722.ip";
connectAttr "polyTweakUV722.out" "polyTweakUV723.ip";
connectAttr "polyTweakUV723.out" "polyTweakUV724.ip";
connectAttr "polyTweakUV724.out" "polyTweakUV725.ip";
connectAttr "polyTweakUV725.out" "polyTweakUV726.ip";
connectAttr "polyTweakUV726.out" "polyTweakUV727.ip";
connectAttr "polyTweakUV727.out" "polyTweakUV728.ip";
connectAttr "polyTweakUV728.out" "polyTweakUV729.ip";
connectAttr "polyTweakUV729.out" "polyTweakUV730.ip";
connectAttr "polyTweakUV730.out" "polyTweakUV731.ip";
connectAttr "polyTweakUV731.out" "polyTweakUV732.ip";
connectAttr "polyTweakUV732.out" "polyTweakUV733.ip";
connectAttr "polyTweakUV733.out" "polyTweakUV734.ip";
connectAttr "polyTweakUV734.out" "polyTweakUV735.ip";
connectAttr "polyTweak39.out" "polyMergeVert27.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert27.mp";
connectAttr "polyTweakUV735.out" "polyTweak39.ip";
connectAttr "polyMergeVert27.out" "polyTweakUV736.ip";
connectAttr "polyTweakUV736.out" "polyTweakUV737.ip";
connectAttr "polyTweakUV737.out" "polyTweakUV738.ip";
connectAttr "polyTweakUV738.out" "polyTweakUV739.ip";
connectAttr "polyTweakUV739.out" "polyTweakUV740.ip";
connectAttr "polyTweakUV740.out" "polyTweakUV741.ip";
connectAttr "polyTweakUV741.out" "polyTweakUV742.ip";
connectAttr "polyTweakUV742.out" "polyTweakUV743.ip";
connectAttr "polyTweakUV743.out" "polyTweakUV744.ip";
connectAttr "polyTweakUV744.out" "polyTweakUV745.ip";
connectAttr "polyTweakUV745.out" "polyTweakUV746.ip";
connectAttr "polyTweakUV746.out" "polyTweakUV747.ip";
connectAttr "polyTweakUV747.out" "polyTweakUV748.ip";
connectAttr "polyTweakUV748.out" "polyTweakUV749.ip";
connectAttr "polyTweakUV749.out" "polyTweakUV750.ip";
connectAttr "polyTweakUV750.out" "polyTweakUV751.ip";
connectAttr "polyTweakUV751.out" "polyTweakUV752.ip";
connectAttr "polyTweakUV752.out" "polyTweakUV753.ip";
connectAttr "polyTweakUV753.out" "polyTweakUV754.ip";
connectAttr "polyTweakUV754.out" "polyTweakUV755.ip";
connectAttr "polyTweakUV755.out" "polyTweakUV756.ip";
connectAttr "polyTweakUV756.out" "polyTweakUV757.ip";
connectAttr "polyTweakUV757.out" "polyTweakUV758.ip";
connectAttr "polyTweakUV758.out" "polyTweakUV759.ip";
connectAttr "polyTweakUV759.out" "polyTweakUV760.ip";
connectAttr "polyTweakUV760.out" "polyTweakUV761.ip";
connectAttr "polyTweakUV761.out" "polyTweakUV762.ip";
connectAttr "polyTweakUV762.out" "polyTweakUV763.ip";
connectAttr "polyTweakUV763.out" "polyTweakUV764.ip";
connectAttr "polyTweakUV764.out" "polyTweakUV765.ip";
connectAttr "polyTweakUV765.out" "polyTweakUV766.ip";
connectAttr "polyTweakUV766.out" "polyTweakUV767.ip";
connectAttr "polyTweakUV767.out" "polyTweakUV768.ip";
connectAttr "polyTweakUV768.out" "polyTweakUV769.ip";
connectAttr "polyTweakUV769.out" "polyTweakUV770.ip";
connectAttr "polyTweak40.out" "polyMergeVert28.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert28.mp";
connectAttr "polyTweakUV770.out" "polyTweak40.ip";
connectAttr "polyMergeVert28.out" "polyTweakUV771.ip";
connectAttr "polyTweakUV771.out" "polyTweakUV772.ip";
connectAttr "polyTweakUV772.out" "polyTweakUV773.ip";
connectAttr "polyTweakUV773.out" "polyTweakUV774.ip";
connectAttr "polyTweakUV774.out" "polyTweakUV775.ip";
connectAttr "polyTweakUV775.out" "polyTweakUV776.ip";
connectAttr "polyTweakUV776.out" "polyTweakUV777.ip";
connectAttr "polyTweakUV777.out" "polyTweakUV778.ip";
connectAttr "polyTweakUV778.out" "polyTweakUV779.ip";
connectAttr "polyTweakUV779.out" "polyTweakUV780.ip";
connectAttr "polyTweakUV780.out" "polyTweakUV781.ip";
connectAttr "polyTweakUV781.out" "polyTweakUV782.ip";
connectAttr "polyTweakUV782.out" "polyTweakUV783.ip";
connectAttr "polyTweakUV783.out" "polyTweakUV784.ip";
connectAttr "polyTweakUV784.out" "polyTweakUV785.ip";
connectAttr "polyTweakUV785.out" "polyTweakUV786.ip";
connectAttr "polyTweakUV786.out" "polyTweakUV787.ip";
connectAttr "polyTweakUV787.out" "polyTweakUV788.ip";
connectAttr "polyTweakUV788.out" "polyTweakUV789.ip";
connectAttr "polyTweakUV789.out" "polyTweakUV790.ip";
connectAttr "polyTweakUV790.out" "polyTweakUV791.ip";
connectAttr "polyTweakUV791.out" "polyTweakUV792.ip";
connectAttr "polyTweakUV792.out" "polyTweakUV793.ip";
connectAttr "polyTweakUV793.out" "polyTweakUV794.ip";
connectAttr "polyTweakUV794.out" "polyTweakUV795.ip";
connectAttr "polyTweakUV795.out" "polyTweakUV796.ip";
connectAttr "polyTweakUV796.out" "polyTweakUV797.ip";
connectAttr "polyTweakUV797.out" "polyTweakUV798.ip";
connectAttr "polyTweakUV798.out" "polyTweakUV799.ip";
connectAttr "polyTweakUV799.out" "polyTweakUV800.ip";
connectAttr "polyTweakUV800.out" "polyTweakUV801.ip";
connectAttr "polyTweakUV801.out" "polyTweakUV802.ip";
connectAttr "polyTweakUV802.out" "polyTweakUV803.ip";
connectAttr "polyTweakUV803.out" "polyTweakUV804.ip";
connectAttr "polyTweakUV804.out" "polyTweakUV805.ip";
connectAttr "polyTweak41.out" "polyMergeVert29.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert29.mp";
connectAttr "polyTweakUV805.out" "polyTweak41.ip";
connectAttr "polyMergeVert29.out" "polyExtrudeFace1.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polyBevel1.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "deleteComponent31.ig";
connectAttr "deleteComponent31.og" "polyTweakUV806.ip";
connectAttr "polyTweakUV806.out" "polyTweakUV807.ip";
connectAttr "polyTweakUV807.out" "polyTweakUV808.ip";
connectAttr "polyTweakUV808.out" "polyTweakUV809.ip";
connectAttr "polyTweakUV809.out" "polyTweakUV810.ip";
connectAttr "polyTweakUV810.out" "polyTweakUV811.ip";
connectAttr "polyTweakUV811.out" "polyTweakUV812.ip";
connectAttr "polyTweakUV812.out" "polyTweakUV813.ip";
connectAttr "polyTweakUV813.out" "polyTweakUV814.ip";
connectAttr "polyTweakUV814.out" "polyTweakUV815.ip";
connectAttr "polyTweakUV815.out" "polyTweakUV816.ip";
connectAttr "polyTweakUV816.out" "polyTweakUV817.ip";
connectAttr "polyTweakUV817.out" "polyTweakUV818.ip";
connectAttr "polyTweakUV818.out" "polyTweakUV819.ip";
connectAttr "polyTweakUV819.out" "polyTweakUV820.ip";
connectAttr "polyTweakUV820.out" "polyTweakUV821.ip";
connectAttr "polyTweakUV821.out" "polyTweakUV822.ip";
connectAttr "polyTweakUV822.out" "polyTweakUV823.ip";
connectAttr "polyTweakUV823.out" "polyTweakUV824.ip";
connectAttr "polyTweakUV824.out" "polyTweakUV825.ip";
connectAttr "polyTweakUV825.out" "polyTweakUV826.ip";
connectAttr "polyTweakUV826.out" "polyTweakUV827.ip";
connectAttr "polyTweakUV827.out" "polyTweakUV828.ip";
connectAttr "polyTweakUV828.out" "polyTweakUV829.ip";
connectAttr "polyTweakUV829.out" "polyTweakUV830.ip";
connectAttr "polyTweakUV830.out" "polyTweakUV831.ip";
connectAttr "polyTweakUV831.out" "polyTweakUV832.ip";
connectAttr "polyTweakUV832.out" "polyTweakUV833.ip";
connectAttr "polyTweakUV833.out" "polyTweakUV834.ip";
connectAttr "polyTweakUV834.out" "polyTweakUV835.ip";
connectAttr "polyTweakUV835.out" "polyTweakUV836.ip";
connectAttr "polyTweakUV836.out" "polyTweakUV837.ip";
connectAttr "polyTweakUV837.out" "polyTweakUV838.ip";
connectAttr "polyTweakUV838.out" "polyTweakUV839.ip";
connectAttr "polyTweakUV839.out" "polyTweakUV840.ip";
connectAttr "polyTweak42.out" "polyMergeVert30.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert30.mp";
connectAttr "polyTweakUV840.out" "polyTweak42.ip";
connectAttr "polyMergeVert30.out" "polyTweakUV841.ip";
connectAttr "polyTweakUV841.out" "polyTweakUV842.ip";
connectAttr "polyTweakUV842.out" "polyTweakUV843.ip";
connectAttr "polyTweakUV843.out" "polyTweakUV844.ip";
connectAttr "polyTweakUV844.out" "polyTweakUV845.ip";
connectAttr "polyTweakUV845.out" "polyTweakUV846.ip";
connectAttr "polyTweakUV846.out" "polyTweakUV847.ip";
connectAttr "polyTweakUV847.out" "polyTweakUV848.ip";
connectAttr "polyTweakUV848.out" "polyTweakUV849.ip";
connectAttr "polyTweakUV849.out" "polyTweakUV850.ip";
connectAttr "polyTweakUV850.out" "polyTweakUV851.ip";
connectAttr "polyTweakUV851.out" "polyTweakUV852.ip";
connectAttr "polyTweakUV852.out" "polyTweakUV853.ip";
connectAttr "polyTweakUV853.out" "polyTweakUV854.ip";
connectAttr "polyTweakUV854.out" "polyTweakUV855.ip";
connectAttr "polyTweakUV855.out" "polyTweakUV856.ip";
connectAttr "polyTweakUV856.out" "polyTweakUV857.ip";
connectAttr "polyTweakUV857.out" "polyTweakUV858.ip";
connectAttr "polyTweakUV858.out" "polyTweakUV859.ip";
connectAttr "polyTweakUV859.out" "polyTweakUV860.ip";
connectAttr "polyTweakUV860.out" "polyTweakUV861.ip";
connectAttr "polyTweakUV861.out" "polyTweakUV862.ip";
connectAttr "polyTweakUV862.out" "polyTweakUV863.ip";
connectAttr "polyTweakUV863.out" "polyTweakUV864.ip";
connectAttr "polyTweakUV864.out" "polyTweakUV865.ip";
connectAttr "polyTweakUV865.out" "polyTweakUV866.ip";
connectAttr "polyTweakUV866.out" "polyTweakUV867.ip";
connectAttr "polyTweakUV867.out" "polyTweakUV868.ip";
connectAttr "polyTweakUV868.out" "polyTweakUV869.ip";
connectAttr "polyTweakUV869.out" "polyTweakUV870.ip";
connectAttr "polyTweakUV870.out" "polyTweakUV871.ip";
connectAttr "polyTweakUV871.out" "polyTweakUV872.ip";
connectAttr "polyTweakUV872.out" "polyTweakUV873.ip";
connectAttr "polyTweakUV873.out" "polyTweakUV874.ip";
connectAttr "polyTweakUV874.out" "polyTweakUV875.ip";
connectAttr "polyTweak43.out" "polyMergeVert31.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert31.mp";
connectAttr "polyTweakUV875.out" "polyTweak43.ip";
connectAttr "polyMergeVert31.out" "polyTweakUV876.ip";
connectAttr "polyTweakUV876.out" "polyTweakUV877.ip";
connectAttr "polyTweakUV877.out" "polyTweakUV878.ip";
connectAttr "polyTweakUV878.out" "polyTweakUV879.ip";
connectAttr "polyTweakUV879.out" "polyTweakUV880.ip";
connectAttr "polyTweakUV880.out" "polyTweakUV881.ip";
connectAttr "polyTweakUV881.out" "polyTweakUV882.ip";
connectAttr "polyTweakUV882.out" "polyTweakUV883.ip";
connectAttr "polyTweakUV883.out" "polyTweakUV884.ip";
connectAttr "polyTweakUV884.out" "polyTweakUV885.ip";
connectAttr "polyTweakUV885.out" "polyTweakUV886.ip";
connectAttr "polyTweakUV886.out" "polyTweakUV887.ip";
connectAttr "polyTweakUV887.out" "polyTweakUV888.ip";
connectAttr "polyTweakUV888.out" "polyTweakUV889.ip";
connectAttr "polyTweakUV889.out" "polyTweakUV890.ip";
connectAttr "polyTweakUV890.out" "polyTweakUV891.ip";
connectAttr "polyTweakUV891.out" "polyTweakUV892.ip";
connectAttr "polyTweakUV892.out" "polyTweakUV893.ip";
connectAttr "polyTweakUV893.out" "polyTweakUV894.ip";
connectAttr "polyTweakUV894.out" "polyTweakUV895.ip";
connectAttr "polyTweakUV895.out" "polyTweakUV896.ip";
connectAttr "polyTweakUV896.out" "polyTweakUV897.ip";
connectAttr "polyTweakUV897.out" "polyTweakUV898.ip";
connectAttr "polyTweakUV898.out" "polyTweakUV899.ip";
connectAttr "polyTweakUV899.out" "polyTweakUV900.ip";
connectAttr "polyTweakUV900.out" "polyTweakUV901.ip";
connectAttr "polyTweakUV901.out" "polyTweakUV902.ip";
connectAttr "polyTweakUV902.out" "polyTweakUV903.ip";
connectAttr "polyTweakUV903.out" "polyTweakUV904.ip";
connectAttr "polyTweakUV904.out" "polyTweakUV905.ip";
connectAttr "polyTweakUV905.out" "polyTweakUV906.ip";
connectAttr "polyTweakUV906.out" "polyTweakUV907.ip";
connectAttr "polyTweakUV907.out" "polyTweakUV908.ip";
connectAttr "polyTweakUV908.out" "polyTweakUV909.ip";
connectAttr "polyTweakUV909.out" "polyTweakUV910.ip";
connectAttr "polyTweak44.out" "polyMergeVert32.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert32.mp";
connectAttr "polyTweakUV910.out" "polyTweak44.ip";
connectAttr "polyMergeVert32.out" "polyTweakUV911.ip";
connectAttr "polyTweakUV911.out" "polyTweakUV912.ip";
connectAttr "polyTweakUV912.out" "polyTweakUV913.ip";
connectAttr "polyTweakUV913.out" "polyTweakUV914.ip";
connectAttr "polyTweakUV914.out" "polyTweakUV915.ip";
connectAttr "polyTweakUV915.out" "polyTweakUV916.ip";
connectAttr "polyTweakUV916.out" "polyTweakUV917.ip";
connectAttr "polyTweakUV917.out" "polyTweakUV918.ip";
connectAttr "polyTweakUV918.out" "polyTweakUV919.ip";
connectAttr "polyTweakUV919.out" "polyTweakUV920.ip";
connectAttr "polyTweakUV920.out" "polyTweakUV921.ip";
connectAttr "polyTweakUV921.out" "polyTweakUV922.ip";
connectAttr "polyTweakUV922.out" "polyTweakUV923.ip";
connectAttr "polyTweakUV923.out" "polyTweakUV924.ip";
connectAttr "polyTweakUV924.out" "polyTweakUV925.ip";
connectAttr "polyTweakUV925.out" "polyTweakUV926.ip";
connectAttr "polyTweakUV926.out" "polyTweakUV927.ip";
connectAttr "polyTweakUV927.out" "polyTweakUV928.ip";
connectAttr "polyTweakUV928.out" "polyTweakUV929.ip";
connectAttr "polyTweakUV929.out" "polyTweakUV930.ip";
connectAttr "polyTweakUV930.out" "polyTweakUV931.ip";
connectAttr "polyTweakUV931.out" "polyTweakUV932.ip";
connectAttr "polyTweakUV932.out" "polyTweakUV933.ip";
connectAttr "polyTweakUV933.out" "polyTweakUV934.ip";
connectAttr "polyTweakUV934.out" "polyTweakUV935.ip";
connectAttr "polyTweakUV935.out" "polyTweakUV936.ip";
connectAttr "polyTweakUV936.out" "polyTweakUV937.ip";
connectAttr "polyTweakUV937.out" "polyTweakUV938.ip";
connectAttr "polyTweakUV938.out" "polyTweakUV939.ip";
connectAttr "polyTweakUV939.out" "polyTweakUV940.ip";
connectAttr "polyTweakUV940.out" "polyTweakUV941.ip";
connectAttr "polyTweakUV941.out" "polyTweakUV942.ip";
connectAttr "polyTweakUV942.out" "polyTweakUV943.ip";
connectAttr "polyTweakUV943.out" "polyTweakUV944.ip";
connectAttr "polyTweakUV944.out" "polyTweakUV945.ip";
connectAttr "polyTweak45.out" "polyMergeVert33.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert33.mp";
connectAttr "polyTweakUV945.out" "polyTweak45.ip";
connectAttr "polyMergeVert33.out" "polyCloseBorder1.ip";
connectAttr "polyCloseBorder1.out" "groupParts2.ig";
connectAttr "groupParts2.og" "polyCloseBorder2.ip";
connectAttr "polyCloseBorder2.out" "groupParts3.ig";
connectAttr "groupParts3.og" "polyCloseBorder3.ip";
connectAttr "polyCloseBorder3.out" "groupParts4.ig";
connectAttr "groupParts4.og" "polyCloseBorder4.ip";
connectAttr "polyCloseBorder4.out" "groupParts5.ig";
connectAttr "polyTweak46.out" "polyBevel2.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBevel2.mp";
connectAttr "groupParts5.og" "polyTweak46.ip";
connectAttr "polyBevel2.out" "polyBevel3.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBevel3.mp";
connectAttr "polyBevel3.out" "polyTweakUV946.ip";
connectAttr "polyTweakUV946.out" "polyTweakUV947.ip";
connectAttr "polyTweakUV947.out" "polyTweakUV948.ip";
connectAttr "polyTweakUV948.out" "polyTweakUV949.ip";
connectAttr "polyTweakUV949.out" "polyTweakUV950.ip";
connectAttr "polyTweakUV950.out" "polyTweakUV951.ip";
connectAttr "polyTweakUV951.out" "polyTweakUV952.ip";
connectAttr "polyTweakUV952.out" "polyTweakUV953.ip";
connectAttr "polyTweakUV953.out" "polyTweakUV954.ip";
connectAttr "polyTweakUV954.out" "polyTweakUV955.ip";
connectAttr "polyTweakUV955.out" "polyTweakUV956.ip";
connectAttr "polyTweakUV956.out" "polyTweakUV957.ip";
connectAttr "polyTweakUV957.out" "polyTweakUV958.ip";
connectAttr "polyTweakUV958.out" "polyTweakUV959.ip";
connectAttr "polyTweakUV959.out" "polyTweakUV960.ip";
connectAttr "polyTweakUV960.out" "polyTweakUV961.ip";
connectAttr "polyTweakUV961.out" "polyTweakUV962.ip";
connectAttr "polyTweakUV962.out" "polyTweakUV963.ip";
connectAttr "polyTweakUV963.out" "polyTweakUV964.ip";
connectAttr "polyTweakUV964.out" "polyTweakUV965.ip";
connectAttr "polyTweakUV965.out" "polyTweakUV966.ip";
connectAttr "polyTweakUV966.out" "polyTweakUV967.ip";
connectAttr "polyTweakUV967.out" "polyTweakUV968.ip";
connectAttr "polyTweakUV968.out" "polyTweakUV969.ip";
connectAttr "polyTweakUV969.out" "polyTweakUV970.ip";
connectAttr "polyTweakUV970.out" "polyTweakUV971.ip";
connectAttr "polyTweakUV971.out" "polyTweakUV972.ip";
connectAttr "polyTweakUV972.out" "polyTweakUV973.ip";
connectAttr "polyTweakUV973.out" "polyTweakUV974.ip";
connectAttr "polyTweakUV974.out" "polyTweakUV975.ip";
connectAttr "polyTweakUV975.out" "polyTweakUV976.ip";
connectAttr "polyTweakUV976.out" "polyTweakUV977.ip";
connectAttr "polyTweakUV977.out" "polyTweakUV978.ip";
connectAttr "polyTweakUV978.out" "polyTweakUV979.ip";
connectAttr "polyTweakUV979.out" "polyTweakUV980.ip";
connectAttr "polyTweak47.out" "polyMergeVert34.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert34.mp";
connectAttr "polyTweakUV980.out" "polyTweak47.ip";
connectAttr "polyMergeVert34.out" "polyTweakUV981.ip";
connectAttr "polyTweakUV981.out" "polyTweakUV982.ip";
connectAttr "polyTweakUV982.out" "polyTweakUV983.ip";
connectAttr "polyTweakUV983.out" "polyTweakUV984.ip";
connectAttr "polyTweakUV984.out" "polyTweakUV985.ip";
connectAttr "polyTweakUV985.out" "polyTweakUV986.ip";
connectAttr "polyTweakUV986.out" "polyTweakUV987.ip";
connectAttr "polyTweakUV987.out" "polyTweakUV988.ip";
connectAttr "polyTweakUV988.out" "polyTweakUV989.ip";
connectAttr "polyTweakUV989.out" "polyTweakUV990.ip";
connectAttr "polyTweakUV990.out" "polyTweakUV991.ip";
connectAttr "polyTweakUV991.out" "polyTweakUV992.ip";
connectAttr "polyTweakUV992.out" "polyTweakUV993.ip";
connectAttr "polyTweakUV993.out" "polyTweakUV994.ip";
connectAttr "polyTweakUV994.out" "polyTweakUV995.ip";
connectAttr "polyTweakUV995.out" "polyTweakUV996.ip";
connectAttr "polyTweakUV996.out" "polyTweakUV997.ip";
connectAttr "polyTweakUV997.out" "polyTweakUV998.ip";
connectAttr "polyTweakUV998.out" "polyTweakUV999.ip";
connectAttr "polyTweakUV999.out" "polyTweakUV1000.ip";
connectAttr "polyTweakUV1000.out" "polyTweakUV1001.ip";
connectAttr "polyTweakUV1001.out" "polyTweakUV1002.ip";
connectAttr "polyTweakUV1002.out" "polyTweakUV1003.ip";
connectAttr "polyTweakUV1003.out" "polyTweakUV1004.ip";
connectAttr "polyTweakUV1004.out" "polyTweakUV1005.ip";
connectAttr "polyTweakUV1005.out" "polyTweakUV1006.ip";
connectAttr "polyTweakUV1006.out" "polyTweakUV1007.ip";
connectAttr "polyTweakUV1007.out" "polyTweakUV1008.ip";
connectAttr "polyTweakUV1008.out" "polyTweakUV1009.ip";
connectAttr "polyTweakUV1009.out" "polyTweakUV1010.ip";
connectAttr "polyTweakUV1010.out" "polyTweakUV1011.ip";
connectAttr "polyTweakUV1011.out" "polyTweakUV1012.ip";
connectAttr "polyTweakUV1012.out" "polyTweakUV1013.ip";
connectAttr "polyTweakUV1013.out" "polyTweakUV1014.ip";
connectAttr "polyTweakUV1014.out" "polyTweakUV1015.ip";
connectAttr "polyTweak48.out" "polyMergeVert35.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert35.mp";
connectAttr "polyTweakUV1015.out" "polyTweak48.ip";
connectAttr "polyMergeVert35.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polyTweak49.out" "polyMergeVert36.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert36.mp";
connectAttr "polySplit5.out" "polyTweak49.ip";
connectAttr "polyMergeVert36.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polyMergeVert37.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert37.mp";
connectAttr "polyMergeVert37.out" "polyMergeVert38.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert38.mp";
connectAttr "polyMergeVert38.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polyTweakUV1016.ip";
connectAttr "polyTweakUV1016.out" "polyTweakUV1017.ip";
connectAttr "polyTweakUV1017.out" "polyTweakUV1018.ip";
connectAttr "polyTweakUV1018.out" "polyTweakUV1019.ip";
connectAttr "polyTweakUV1019.out" "polyTweakUV1020.ip";
connectAttr "polyTweakUV1020.out" "polyTweakUV1021.ip";
connectAttr "polyTweakUV1021.out" "polyTweakUV1022.ip";
connectAttr "polyTweakUV1022.out" "polyTweakUV1023.ip";
connectAttr "polyTweakUV1023.out" "polyTweakUV1024.ip";
connectAttr "polyTweakUV1024.out" "polyTweakUV1025.ip";
connectAttr "polyTweakUV1025.out" "polyTweakUV1026.ip";
connectAttr "polyTweakUV1026.out" "polyTweakUV1027.ip";
connectAttr "polyTweakUV1027.out" "polyTweakUV1028.ip";
connectAttr "polyTweakUV1028.out" "polyTweakUV1029.ip";
connectAttr "polyTweakUV1029.out" "polyTweakUV1030.ip";
connectAttr "polyTweakUV1030.out" "polyTweakUV1031.ip";
connectAttr "polyTweakUV1031.out" "polyTweakUV1032.ip";
connectAttr "polyTweakUV1032.out" "polyTweakUV1033.ip";
connectAttr "polyTweakUV1033.out" "polyTweakUV1034.ip";
connectAttr "polyTweakUV1034.out" "polyTweakUV1035.ip";
connectAttr "polyTweakUV1035.out" "polyTweakUV1036.ip";
connectAttr "polyTweakUV1036.out" "polyTweakUV1037.ip";
connectAttr "polyTweakUV1037.out" "polyTweakUV1038.ip";
connectAttr "polyTweakUV1038.out" "polyTweakUV1039.ip";
connectAttr "polyTweakUV1039.out" "polyTweakUV1040.ip";
connectAttr "polyTweakUV1040.out" "polyTweakUV1041.ip";
connectAttr "polyTweakUV1041.out" "polyTweakUV1042.ip";
connectAttr "polyTweakUV1042.out" "polyTweakUV1043.ip";
connectAttr "polyTweakUV1043.out" "polyTweakUV1044.ip";
connectAttr "polyTweakUV1044.out" "polyTweakUV1045.ip";
connectAttr "polyTweakUV1045.out" "polyTweakUV1046.ip";
connectAttr "polyTweakUV1046.out" "polyTweakUV1047.ip";
connectAttr "polyTweakUV1047.out" "polyTweakUV1048.ip";
connectAttr "polyTweakUV1048.out" "polyTweakUV1049.ip";
connectAttr "polyTweakUV1049.out" "polyTweakUV1050.ip";
connectAttr "polyTweak50.out" "polyMergeVert39.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert39.mp";
connectAttr "polyTweakUV1050.out" "polyTweak50.ip";
connectAttr "polyMergeVert39.out" "polyMergeVert40.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert40.mp";
connectAttr "polyMergeVert40.out" "polyMergeVert41.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert41.mp";
connectAttr "polyMergeVert41.out" "polyMergeVert42.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert42.mp";
connectAttr "polyMergeVert42.out" "polyMergeVert43.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert43.mp";
connectAttr "polyMergeVert43.out" "polyTweakUV1051.ip";
connectAttr "polyTweakUV1051.out" "polyTweakUV1052.ip";
connectAttr "polyTweakUV1052.out" "polyTweakUV1053.ip";
connectAttr "polyTweakUV1053.out" "polyTweakUV1054.ip";
connectAttr "polyTweakUV1054.out" "polyTweakUV1055.ip";
connectAttr "polyTweakUV1055.out" "polyTweakUV1056.ip";
connectAttr "polyTweakUV1056.out" "polyTweakUV1057.ip";
connectAttr "polyTweakUV1057.out" "polyTweakUV1058.ip";
connectAttr "polyTweakUV1058.out" "polyTweakUV1059.ip";
connectAttr "polyTweakUV1059.out" "polyTweakUV1060.ip";
connectAttr "polyTweakUV1060.out" "polyTweakUV1061.ip";
connectAttr "polyTweakUV1061.out" "polyTweakUV1062.ip";
connectAttr "polyTweakUV1062.out" "polyTweakUV1063.ip";
connectAttr "polyTweakUV1063.out" "polyTweakUV1064.ip";
connectAttr "polyTweakUV1064.out" "polyTweakUV1065.ip";
connectAttr "polyTweakUV1065.out" "polyTweakUV1066.ip";
connectAttr "polyTweakUV1066.out" "polyTweakUV1067.ip";
connectAttr "polyTweakUV1067.out" "polyTweakUV1068.ip";
connectAttr "polyTweakUV1068.out" "polyTweakUV1069.ip";
connectAttr "polyTweakUV1069.out" "polyTweakUV1070.ip";
connectAttr "polyTweakUV1070.out" "polyTweakUV1071.ip";
connectAttr "polyTweakUV1071.out" "polyTweakUV1072.ip";
connectAttr "polyTweakUV1072.out" "polyTweakUV1073.ip";
connectAttr "polyTweakUV1073.out" "polyTweakUV1074.ip";
connectAttr "polyTweakUV1074.out" "polyTweakUV1075.ip";
connectAttr "polyTweakUV1075.out" "polyTweakUV1076.ip";
connectAttr "polyTweakUV1076.out" "polyTweakUV1077.ip";
connectAttr "polyTweakUV1077.out" "polyTweakUV1078.ip";
connectAttr "polyTweakUV1078.out" "polyTweakUV1079.ip";
connectAttr "polyTweakUV1079.out" "polyTweakUV1080.ip";
connectAttr "polyTweakUV1080.out" "polyTweakUV1081.ip";
connectAttr "polyTweakUV1081.out" "polyTweakUV1082.ip";
connectAttr "polyTweakUV1082.out" "polyTweakUV1083.ip";
connectAttr "polyTweakUV1083.out" "polyTweakUV1084.ip";
connectAttr "polyTweakUV1084.out" "polyTweakUV1085.ip";
connectAttr "polyTweak51.out" "polyMergeVert44.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert44.mp";
connectAttr "polyTweakUV1085.out" "polyTweak51.ip";
connectAttr "polyMergeVert44.out" "polyBevel4.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBevel4.mp";
connectAttr "polyBevel4.out" "polyExtrudeEdge1.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyExtrudeEdge1.mp";
connectAttr "polyExtrudeEdge1.out" "polyMergeVert45.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyMergeVert45.mp";
connectAttr "polyTweak52.out" "polyBridgeEdge4.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBridgeEdge4.mp";
connectAttr "polyMergeVert45.out" "polyTweak52.ip";
connectAttr "polyBridgeEdge4.out" "polyBridgeEdge5.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyBridgeEdge5.mp";
connectAttr "polyTweak53.out" "polySoftEdge1.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge1.mp";
connectAttr "polyBridgeEdge5.out" "polyTweak53.ip";
connectAttr "polySoftEdge1.out" "deleteComponent32.ig";
connectAttr "deleteComponent32.og" "deleteComponent33.ig";
connectAttr "deleteComponent33.og" "deleteComponent34.ig";
connectAttr "deleteComponent34.og" "deleteComponent35.ig";
connectAttr "deleteComponent35.og" "deleteComponent36.ig";
connectAttr "deleteComponent36.og" "deleteComponent37.ig";
connectAttr "deleteComponent37.og" "deleteComponent38.ig";
connectAttr "deleteComponent38.og" "deleteComponent39.ig";
connectAttr "deleteComponent39.og" "deleteComponent40.ig";
connectAttr "deleteComponent40.og" "deleteComponent41.ig";
connectAttr "deleteComponent41.og" "deleteComponent42.ig";
connectAttr "deleteComponent42.og" "polyExtrudeEdge2.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyExtrudeEdge2.mp";
connectAttr "polyExtrudeEdge2.out" "polyCloseBorder5.ip";
connectAttr "polyCloseBorder5.out" "groupParts6.ig";
connectAttr "groupId1.id" "groupParts6.gi";
connectAttr "groupParts6.og" "polyTweak54.ip";
connectAttr "polyTweak54.out" "polySplit8.ip";
connectAttr "polySplit8.out" "deleteComponent43.ig";
connectAttr "deleteComponent43.og" "deleteComponent44.ig";
connectAttr "polyTweak55.out" "polySoftEdge2.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge2.mp";
connectAttr "deleteComponent44.og" "polyTweak55.ip";
connectAttr "polySoftEdge2.out" "polySoftEdge3.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge3.mp";
connectAttr "polySoftEdge3.out" "polySoftEdge4.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge4.mp";
connectAttr "polySoftEdge4.out" "polySoftEdge5.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge5.mp";
connectAttr "polySoftEdge5.out" "polySoftEdge6.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge6.mp";
connectAttr "polySoftEdge6.out" "polySoftEdge7.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge7.mp";
connectAttr "polySoftEdge7.out" "polySoftEdge8.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge8.mp";
connectAttr "polySoftEdge8.out" "polySoftEdge9.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge9.mp";
connectAttr "polySoftEdge9.out" "polySoftEdge10.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge10.mp";
connectAttr "polySoftEdge10.out" "polyTweak56.ip";
connectAttr "polyTweak56.out" "polySplit9.ip";
connectAttr "polyTweak57.out" "polySoftEdge11.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge11.mp";
connectAttr "polySplit9.out" "polyTweak57.ip";
connectAttr "polyTweak58.out" "polySoftEdge12.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge12.mp";
connectAttr "polySoftEdge11.out" "polyTweak58.ip";
connectAttr "polyTweak59.out" "polySoftEdge13.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge13.mp";
connectAttr "polySoftEdge12.out" "polyTweak59.ip";
connectAttr "polySoftEdge13.out" "polySoftEdge14.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge14.mp";
connectAttr "polySoftEdge14.out" "polySplit10.ip";
connectAttr "polyTweak60.out" "polySoftEdge15.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge15.mp";
connectAttr "polySplit10.out" "polyTweak60.ip";
connectAttr "polySoftEdge15.out" "polySoftEdge16.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge16.mp";
connectAttr "polySoftEdge16.out" "polySoftEdge17.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge17.mp";
connectAttr "polySoftEdge17.out" "polyTweak61.ip";
connectAttr "polyTweak61.out" "polySplit11.ip";
connectAttr "polyTweak62.out" "polySoftEdge18.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polySoftEdge18.mp";
connectAttr "polySplit11.out" "polyTweak62.ip";
connectAttr "polySoftEdge18.out" "polyExtrudeFace2.ip";
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.wm" "polyExtrudeFace2.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|hipsShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|upperLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|lowerLegLeft|lowerLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegLeft|kneeLeft|kneeLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|upperLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|lowerLegRight|lowerLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|upperLegRight|kneeRight|kneeRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelLeft1|panelLeft2|panelLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Legs|hips|panelRight1|panelRight2|panelRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "baseShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontLeftShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontRightShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontLeftShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "jawMidShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethmidShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "leftSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "rightSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "backSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "leftNozzleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "rightNozzleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "frontLeftShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|armRight2|armRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|upperArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Right_Arm|armRight1|upperArmRight|lowerArmRight|lowerArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|upperArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft|lowerArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Left_Arm|armLeft1|armLeft2|armLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|hipsShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|upperLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|lowerLegLeft|lowerLegLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegLeft|kneeLeft|kneeLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|upperLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|lowerLegRight|lowerLegRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|upperLegRight|kneeRight|kneeRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1|panelLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|panelLeft1|panelLeft2|panelLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|panelRight1|panelRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Legs|hips|panelRight1|panelRight2|panelRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|armRight1|armRightShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|upperArmRight|upperArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|lowerArmRight|lowerArmRightShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Right_Arm|armRight2|armRightShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|armLeftShape1.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft|upperArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|upperArmLeft|lowerArmLeft|lowerArmLeftShape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped1|Basic_Left_Arm|armLeft1|armLeft2|armLeftShape2.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "BasicMechGrouped1_Basic_Torso_base1Shape.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
// End of Basic_Mecha.0004.ma
