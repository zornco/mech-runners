//Maya ASCII 2018 scene
//Name: Basic_Mecha.0025.ma
//Last modified: Wed, Apr 04, 2018 10:55:52 AM
//Codeset: 1252
requires maya "2018";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201708311015-002f4fe637";
fileInfo "osv" "Microsoft Windows 8 , 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "57F02CE5-4F31-A6EC-5D05-8897BB4CC603";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -13.700251689096294 8.7008471759569783 25.456551679683713 ;
	setAttr ".r" -type "double3" -11.13835249621374 -8672.1999999996242 0 ;
	setAttr ".rp" -type "double3" -1.5543122344752192e-14 -7.4829031859735551e-14 0 ;
	setAttr ".rpt" -type "double3" 7.1649883269682367e-14 3.4361230584639746e-14 -3.2486321733328545e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "8DA41C8D-4BCD-E8D6-9D20-B899EBCA6DC4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 24.579921994449201;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -7.9999997615814209 2 1 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "DEFBBE81-4D59-C1DB-60E7-FC96822C4ACA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -18 1000.4187395494673 6.4435644149782441 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BEC4DFEB-4727-263C-1835-5A976D6A6F5E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 976.66873954946709;
	setAttr ".ow" 88.041115239802437;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -18 23.75 6.4435644149780273 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "90496FA8-4FA9-6A33-DE77-0AAC0D1B9DAD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -18 23.75 1003.0483715050643 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "D197ED46-4D10-44A6-2CAA-A896A1A3D1A8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 996.6048070900863;
	setAttr ".ow" 54.459533369037338;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" -18 23.75 6.4435644149780273 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "EE308D80-4811-9364-4FA8-00B92704BA63";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1002.5082645165967 25.348052978515625 3.5591077804567695 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "5518C0C6-48E8-F804-B673-848FBDEA0A7C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1020.5082645165965;
	setAttr ".ow" 58.193975233876614;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -18 25.348052978515625 3.559107780456543 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "CenterRoot";
	rename -uid "36260844-4F53-3ABA-4FAF-13818B63E76D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.5444437451708099e-14 -2.5444437451708099e-14 90 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".dl" yes;
	setAttr ".typ" 1;
createNode joint -n "CenterHip" -p "CenterRoot";
	rename -uid "F222B8CA-46BD-39D1-B6EF-BFB5F8060C79";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 11 1.9984031383911763e-15 4.8849813083506888e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 4.4408751578416811e-16 11 4.8849813083506888e-15 1;
	setAttr ".dl" yes;
	setAttr ".typ" 2;
createNode joint -n "CenterSpine" -p "CenterHip";
	rename -uid "E24C910F-4E7B-C60B-21DF-88930C103030";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 7 -2.2204460492503131e-16 -4.8849813083506888e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.2204443551844186e-15 18 0 1;
	setAttr ".dl" yes;
	setAttr ".typ" 6;
createNode joint -n "CenterHead" -p "CenterSpine";
	rename -uid "6A860384-462E-B055-A6AC-20B0C72C74A0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 18.000007629394531 2.4424868425270817e-15 -8.7209893915251267e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -90 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.7747620953737949e-15 36.000007629394531 -8.7209893915251267e-15 1;
	setAttr ".dl" yes;
	setAttr ".typ" 8;
createNode joint -n "LeftCollar" -p "CenterSpine";
	rename -uid "0A945317-4EE3-EB7F-2AB5-03AC8DC39B66";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 15.000007629394531 -12 1.3819974421032562e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -80.537677791974602 ;
	setAttr ".bps" -type "matrix" 0.9863939238321443 0.16439898730535341 0 0 -0.16439898730535341 0.9863939238321443 0 0
		 0 0 1 0 12.000000000000005 33.000007629394531 1.3819974421032562e-14 1;
	setAttr ".dl" yes;
	setAttr ".sd" 1;
	setAttr ".typ" 9;
createNode joint -n "LeftShoulder" -p "LeftCollar";
	rename -uid "E0200340-44B4-E4FC-33E2-6D827B1DC23D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 6.0827627182006836 1.5267468177562488e-15 -6.1108209421442683e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -99.462322208025597 ;
	setAttr ".bps" -type "matrix" -3.4416913763379853e-15 -1.0000000000000002 0 0 1.0000000000000002 -3.4416913763379853e-15 0 0
		 0 0 1 0 18.000000185345858 34.000007660285483 7.7091534788882934e-15 1;
	setAttr ".dl" yes;
	setAttr ".sd" 1;
	setAttr ".typ" 10;
createNode joint -n "LeftElbow" -p "LeftShoulder";
	rename -uid "C2099CCD-4838-926F-D6B9-91A53F63CD9C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 17.000007629394531 6.6058299611349968e-15 2.1111584702637742e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000227312146095 -74.054604098981599 -0.000218566071865877 ;
	setAttr ".bps" -type "matrix" -1.0479780246948464e-06 -0.27472112789698272 0.9615239476403652 0
		 -2.9942229275911704e-07 0.96152394764093652 0.27472112789681963 0 -0.99999999999940647 5.5511151229213494e-17 -1.0899135973740925e-06 0
		 18.000000185345804 17.000000030890948 9.8203119491520677e-15 1;
	setAttr ".dl" yes;
	setAttr ".sd" 1;
	setAttr ".typ" 11;
createNode joint -n "LeftHand" -p "LeftElbow";
	rename -uid "77D0BB3F-4F80-6EA5-945C-FC83CB2AC823";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 7.2801098823547363 5.4889183408418467e-15 6.3938933180069063e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -15.945333453450301 89.999999999994799 0 ;
	setAttr ".bps" -type "matrix" 0.99999999999940647 -2.5035152145537247e-14 1.0899136848028361e-06 0
		 -1.1629494101467571e-12 0.99999999999940647 1.0899135945430509e-06 0 -1.0899136847037292e-06 -1.0899135943209792e-06 0.99999999999881239 0
		 17.99999255595063 15.000000032796491 6.9999999933373722 1;
	setAttr ".dl" yes;
	setAttr ".sd" 1;
	setAttr ".typ" 12;
createNode joint -n "RightCollar" -p "CenterSpine";
	rename -uid "25F5D630-4D5A-3AA1-28AD-02BCC6A52E6D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 15.000007629394531 12 5.5517516683856443e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.999999146226 0 80.537677791974403 ;
	setAttr ".bps" -type "matrix" -0.98639392383214375 0.16439898730535729 0 0 0.16439898730535729 0.98639392383214375 1.4901167533561273e-08 0
		 2.4497368521849423e-09 1.4698421113109652e-08 -1 0 -11.999999999999995 33.000007629394531 5.5517516683856443e-15 1;
	setAttr ".dl" yes;
	setAttr ".sd" 2;
	setAttr ".typ" 9;
createNode joint -n "RightShoulder" -p "RightCollar";
	rename -uid "7351C74A-4037-5BD7-B967-25A1079A1699";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 6.0827627182006836 -1.6592191708167956e-15 -1.0551713040644894e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -99.462322184172507 ;
	setAttr ".bps" -type "matrix" -4.1631526004337616e-10 -1 -1.4698421114129514e-08 0
		 -1 4.1631526004337616e-10 -2.4497368460657649e-09 0 2.4497368521849423e-09 1.4698421113109652e-08 -1 0
		 -18.000000185345844 34.000007660285505 1.6103464684306237e-14 1;
	setAttr ".dl" yes;
	setAttr ".sd" 2;
	setAttr ".typ" 10;
createNode joint -n "RightElbow" -p "RightShoulder";
	rename -uid "8550BA6D-4204-420E-7222-1D882F7D9221";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 17.000007629394531 -1.2780308393880741e-15 2.5826907005719289e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -89.999999997256097 73.944184999090098 0 ;
	setAttr ".bps" -type "matrix" -2.4693211243179696e-09 -0.27657365741139739 0.96099272215042486 0
		 -3.2534662384372184e-10 0.96099272215042486 0.27657365741139756 0 -1.0000000000000002 3.7029318519826965e-10 -2.4629818074259952e-09 0
		 -18.000000192423204 17.000000030890973 -2.4987328080369772e-07 1;
	setAttr ".dl" yes;
	setAttr ".sd" 2;
	setAttr ".typ" 11;
createNode joint -n "RightHand" -p "RightElbow";
	rename -uid "83A0B628-42FF-E34E-AE26-4BB8FAB543BB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 7.2742304801940918 5.0863888025488163e-15 -7.4498313074045086e-10 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -15.919688058435701 89.999999999708194 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000004 -3.7170178509722418e-10 2.4678761790157799e-09 0
		 3.6583767405874305e-10 0.99999717760308504 0.0023758758100450028 0 -2.4687523923625505e-09 -0.0023758758100448918 0.99999717760308526 0
		 -18.000000209640632 14.988139502130233 6.9904823008380328 1;
	setAttr ".dl" yes;
	setAttr ".sd" 2;
	setAttr ".typ" 12;
createNode joint -n "LeftHip" -p "CenterHip";
	rename -uid "292AC345-4BCF-8FC5-27D3-DCBBBE5F9382";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 7.62939453125e-06 -8 1.6940658945086007e-21 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000398 -7.4265055507342996 180 ;
	setAttr ".bps" -type "matrix" -3.4508206831016557e-16 -0.9916114742539015 0.12925433891364821 0
		 -6.7436602215945281e-15 0.12925433891364821 0.9916114742539015 0 -1 -5.4123372450476401e-16 -6.8833827526759706e-15 0
		 8 11.000007629394529 4.8849830024165833e-15 1;
	setAttr ".dl" yes;
	setAttr ".sd" 1;
	setAttr ".typ" 2;
createNode joint -n "LeftKnee" -p "LeftHip";
	rename -uid "0E8B7813-4C3D-3D46-B1FF-44AE78FE8FE1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.04229736328125 1.157182663569833e-16 -6.9975932871519514e-17 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999999999901 0 -25.707336245017501 ;
	setAttr ".bps" -type "matrix" 2.6143011882849258e-15 -0.94953047617381547 -0.31367479149132738 0
		 -1.1216073313283328e-14 0.31367479149132732 -0.9495304761738157 0 1 6.0123318292694715e-15 -9.6782745099686942e-15 0
		 7.9999999999999982 6.0000077073646487 0.65173881229695441 1;
	setAttr ".dl" yes;
	setAttr ".sd" 1;
	setAttr ".typ" 3;
createNode joint -n "LeftFoot" -p "LeftKnee";
	rename -uid "0B3D773A-45B5-A820-0FAD-22A43471D445";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 5.2657685279846191 -5.9586957306415273e-16 3.0730973321624333e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 179.99999999999201 85.7553099189834 ;
	setAttr ".bps" -type "matrix" -1.2857466869812125e-13 -0.24253388348224081 0.97014293553219444 0
		 -3.4372999458016729e-15 0.97014293553219422 0.24253388348224081 0 -1 2.7837267737873232e-14 -1.2572115677428613e-13 0
		 8.0000000000003197 1.000000009566322 -1.0000000327602141 1;
	setAttr ".dl" yes;
	setAttr ".sd" 1;
	setAttr ".typ" 4;
createNode joint -n "LeftToe" -p "LeftFoot";
	rename -uid "E2F77DF6-499F-3965-E3CA-F3A215E21165";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 4.1231036186218262 1.3322676295501878e-15 -2.1471169246988506e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 14.036140613266699 -89.999999999999901 0 ;
	setAttr ".bps" -type "matrix" -1 2.7406441015163251e-14 -1.2399783673555767e-13 0
		 2.741883003503113e-14 1.0000000000000002 -1.7763568394003045e-15 0 1.2390413369348544e-13 -1.7486012637845752e-15 -1.0000000000000004 0
		 7.9999999999997922 7.6769422918676611e-06 2.9999998153129783 1;
	setAttr ".dl" yes;
	setAttr ".sd" 1;
	setAttr ".typ" 5;
createNode joint -n "RightHip" -p "CenterHip";
	rename -uid "FDCED3EE-4275-E132-3983-19A04AE62FE4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 7.62939453125e-06 8 -2.6645335650344812e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000398 -7.4265055507342108 180 ;
	setAttr ".bps" -type "matrix" -3.3120428050235112e-16 -0.99161147425390173 0.12925433891364665 0
		 -6.7436602215945281e-15 0.12925433891364665 0.99161147425390161 0 -1 -5.2735593669694936e-16 -6.6613381477509392e-15 0
		 -8 11.000007629394533 2.2204477433162076e-15 1;
	setAttr ".dl" yes;
	setAttr ".sd" 2;
	setAttr ".typ" 2;
createNode joint -n "RightKnee" -p "RightHip";
	rename -uid "A0CA8E94-4496-707A-ACDF-8093C37D5E34";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.04229736328125 -2.2204460492503131e-16 -4.6287306542793318e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 -5.6498000615042002e-30 -25.707336245017601 ;
	setAttr ".bps" -type "matrix" 2.6268053732990153e-15 -0.94953047617381436 -0.31367479149133026 0
		 6.0973860410066979e-15 0.31367479149133032 -0.94953047617381436 0 1 5.6577001963425649e-16 6.5450542019170268e-15 0
		 -8.0000000000000018 6.0000077073646514 0.65173881229694364 1;
	setAttr ".dl" yes;
	setAttr ".sd" 2;
	setAttr ".typ" 3;
createNode joint -n "RightFoot" -p "RightKnee";
	rename -uid "A251C16D-4684-41C3-943D-26AE5B13DD4F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 5.2657685279846191 -1.6303215036206922e-16 4.7073456244106637e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 179.99999999999901 85.755309918983599 ;
	setAttr ".bps" -type "matrix" -2.3717030881252721e-14 -0.24253388348224733 0.97014293553219255 0
		 -2.1682956276774986e-15 0.97014293553219255 0.24253388348224739 0 -1 3.6644923611973119e-15 -2.3466232823590869e-14 0
		 -7.9999999999999405 1.0000000095663308 -1.0000000327602407 1;
	setAttr ".dl" yes;
	setAttr ".sd" 2;
	setAttr ".typ" 4;
createNode joint -n "RightToe" -p "RightFoot";
	rename -uid "6F3D4236-40B6-DEAC-4BF9-6284F0ABB3BB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 4.1231036186218262 -4.1507049635184073e-16 8.8817841970012523e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -14.0361406132669 90 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 -3.7048824691692529e-15 2.3570574492240232e-14 0
		 3.6902602823853514e-15 1.0000000000000002 1.5265566588595912e-15 0 -2.3645817422826897e-14 -1.5265566588595898e-15 1.0000000000000002 0
		 -8.0000000000000391 7.6769422721056912e-06 2.9999998153129432 1;
	setAttr ".dl" yes;
	setAttr ".sd" 2;
	setAttr ".typ" 5;
createNode transform -n "BasicMechGrouped";
	rename -uid "0177049C-4D00-9488-267F-B69328B58F5D";
	setAttr ".rp" -type "double3" 0 -13 1.0000000000000009 ;
	setAttr ".sp" -type "double3" 0 -13 1.0000000000000009 ;
createNode transform -n "Basic_Torso" -p "BasicMechGrouped";
	rename -uid "38150AC2-4E38-3106-AF69-2AB392F4167C";
	setAttr ".rp" -type "double3" 0 5 -1 ;
	setAttr ".sp" -type "double3" 0 5 -1 ;
createNode transform -n "base" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "5687955E-4776-20EC-B431-EB86A683BF5F";
	setAttr ".rp" -type "double3" 0 -17 -1 ;
	setAttr ".sp" -type "double3" 0 -17 -1 ;
createNode mesh -n "baseShape" -p "base";
	rename -uid "B20F1ADE-4BB5-52FE-2AD2-6FB2C764C8AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  -3 0 0 3 0 0 -3 0 0 3 0 0;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "backSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "D459BBFE-443F-93C5-3C8E-498F2B118BF5";
	setAttr ".rp" -type "double3" 0 -7.5 -11.5 ;
	setAttr ".sp" -type "double3" 0 -7.5 -11.5 ;
createNode mesh -n "backSideShape" -p "backSide";
	rename -uid "8C90774E-404D-A119-5C13-11B5F4956669";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[0:1]" -type "float3"  0 -4 0 0 -4 0;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "FE9902B7-4B05-B77A-9454-A09CCB9ADCF3";
	setAttr ".rp" -type "double3" -1.7763568394002505e-15 0 1.7763568394002505e-15 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-15 0 1.7763568394002505e-15 ;
createNode mesh -n "rightSideShape" -p "rightSide";
	rename -uid "E0DE3407-41EB-E6C2-CEAD-018F87BE1007";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -26 0 0 -26 0 0 -26 0 0 -26 
		0 0 -20 0 0 -20 0 0 -20 0 0 -20 0 0;
	setAttr -s 8 ".vt[0:7]"  13 -15 8 13 -15 -10 13 0 8 13 0 -10 10 0 8
		 10 0 -10 10 -15 8 10 -15 -10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightNozzle" -p "rightSide";
	rename -uid "6E3AAC7B-4D89-5CFF-9166-978B87C5BE70";
	setAttr ".rp" -type "double3" -1.7763568394002505e-15 3.5527136788005009e-15 0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-15 3.5527136788005009e-15 0 ;
createNode mesh -n "rightNozzleShape" -p "rightNozzle";
	rename -uid "EA8B82A3-45D0-60B5-8430-1C8E0660895D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -22.205099 0 0 -25.999722 
		0 0 -24.102966 0 0 -27.897589 0 0 -24.102966 0 0 -27.897589 0 0 -22.205099 0 0 -25.999722 
		0 0;
	setAttr -s 8 ".vt[0:7]"  11.10254955 -15.94851685 -2.5 12.99986076 -16.58113861 -2.5
		 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5 12.051483154 -13.10255051 -5.5
		 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5 12.99986076 -16.58113861 -5.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "06C37FDC-4EEB-D5E2-F652-A48C6BCF8552";
	setAttr ".rp" -type "double3" -1.7763568394002505e-15 0 1.7763568394002505e-15 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-15 0 1.7763568394002505e-15 ;
createNode mesh -n "leftSideShape" -p "leftSide";
	rename -uid "1290F97F-4AEF-7373-D2B6-EFA594447191";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftNozzle" -p "leftSide";
	rename -uid "9E258720-4205-43CC-E9AA-B6ACF03285C0";
	setAttr ".rp" -type "double3" -1.7763568394002505e-15 3.5527136788005009e-15 0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-15 3.5527136788005009e-15 0 ;
createNode mesh -n "leftNozzleShape" -p "leftNozzle";
	rename -uid "756ED554-4D39-2719-C8E7-15A011104AA8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontSide" -p "|BasicMechGrouped|Basic_Torso";
	rename -uid "4EF332D3-40DD-D9F6-22A4-E29160E69659";
	setAttr ".rp" -type "double3" 0 -1.5 11.5 ;
	setAttr ".sp" -type "double3" 0 -1.5 11.5 ;
createNode mesh -n "frontSideShape" -p "frontSide";
	rename -uid "240FBBC4-4A64-C047-B4B0-A49AA879F1E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft1" -p "frontSide";
	rename -uid "DA5A97AE-4AB6-D808-737D-48A54CC76B56";
	setAttr ".rp" -type "double3" 0.99999999999999978 0 13 ;
	setAttr ".sp" -type "double3" 0.99999999999999978 0 13 ;
createNode mesh -n "frontLeftShape1" -p "frontLeft1";
	rename -uid "8981AC15-446A-F817-6A7C-2BA9D2BE092E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft2" -p "frontLeft1";
	rename -uid "EC447CBF-4B7A-68CE-5D43-3387389E5D50";
	setAttr ".rp" -type "double3" -8.8817841970012523e-16 0 8.8817841970012523e-16 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-16 0 8.8817841970012523e-16 ;
createNode mesh -n "frontLeftShape2" -p "frontLeft2";
	rename -uid "C4FBF2AC-4AEF-1CD1-8BE8-D1AA68F5582A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[0]" -type "float3" -0.075828865 0 0 ;
	setAttr ".pt[2]" -type "float3" -0.38835922 8.8817842e-16 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "frontLeft2";
	rename -uid "8F4E599B-471A-D845-EC15-50AA27EF9227";
	setAttr ".rp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" 3.7760666701041603 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "F23315E2-4483-29C2-F6AB-4CA8057855AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "01186FCA-494E-584F-5C95-228E0CF99493";
	setAttr ".rp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" 3.6954436898255012 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8";
	rename -uid "D96C6124-421A-9B22-8816-5CB8F1492E86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "E59C2CDB-44D4-3824-3860-81A55AC3D276";
	setAttr ".rp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" 3.5341977292681803 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11";
	rename -uid "A4C79538-4683-2A6D-69DD-DF9B10E5FD1A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "B5364B96-4B40-02BE-DD0C-38A4F8490A73";
	setAttr ".rp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" 10.757559900686175 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10";
	rename -uid "49274F99-4C74-D279-E5C0-E5A7D437EE54";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "C5F9ED8E-4ADB-B6FE-0679-38816DDCE302";
	setAttr ".rp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" 11.080051821800815 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9";
	rename -uid "A6E870DD-4579-55CB-8072-7CB118EF453B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "A2942867-4BB8-B9AF-4995-66AA739642DC";
	setAttr ".rp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" 6.7496504667852228 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube15";
	rename -uid "A54AB347-497D-39BE-BBCC-77960C5B8ED5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  8.7661715 -10.002368 11.40912 
		6.8029666 -10.002368 9.7271099 8.7687492 -10.004949 11.415309 6.8055449 -10.004949 
		9.7332993 8.1979952 -8.6640949 12.045508 6.2347908 -8.6640949 10.3635 8.1954165 -8.6615152 
		12.03932 6.2322121 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "23A77EF9-4783-E752-C980-89A4DCF51C92";
	setAttr ".rp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" 6.8302734470638811 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube14";
	rename -uid "9462CCF8-4905-AC44-3704-0BAC8F8F7885";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.3451328 -9.0245857 12.22831 
		6.8835888 -9.0245857 9.9206047 7.3477111 -9.0271645 12.234498 6.886168 -9.0271654 
		9.9267941 6.7769575 -7.6863122 12.864698 6.3154144 -7.6863122 10.556993 6.7743788 
		-7.6837316 12.85851 6.3128357 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "5764FA3A-4D93-9542-2DBF-BABA02737ED2";
	setAttr ".rp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" 6.9108964273425428 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube13";
	rename -uid "94145998-4709-D2FF-3583-32AEB55EBF4D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  7.4257555 -8.0468016 12.421804 
		6.9642124 -8.0468016 10.114099 7.4283352 -8.0493822 12.427992 6.9667902 -8.0493822 
		10.120287 6.8575802 -6.708529 13.058191 6.3960366 -6.7085285 10.750487 6.8550024 
		-6.7059488 13.052003 6.3934579 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7";
	rename -uid "5310FC2F-4553-4570-E395-86979156798A";
	setAttr ".rp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" 6.9915194076212028 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12";
	rename -uid "36B47657-41C2-2C16-9608-83A187855E82";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft3" -p "frontLeft1";
	rename -uid "547EA155-44AE-5549-0CE7-2AAF174DF833";
	setAttr ".rp" -type "double3" -8.8817841970012523e-16 0 8.8817841970012523e-16 ;
	setAttr ".sp" -type "double3" -8.8817841970012523e-16 0 8.8817841970012523e-16 ;
createNode mesh -n "frontLeftShape3" -p "frontLeft3";
	rename -uid "3DD55ED4-46B0-DDAB-C522-869D1C2E88B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.076490089 0 0 -25.998447 
		0 0 -0.38087735 8.8817842e-16 0 -26.615368 0 0 0.76427841 0 0 -25.081852 0 0 1.381197 
		0 0 -24.464935 0 0;
	setAttr -s 8 ".vt[0:7]"  0.076159477 -13.017789841 13.38278198 12.99922371 -13.017789841 7.99813509
		 0.38461828 -3.050000191 14.1230793 13.30768394 -3.050000191 8.73843193 -0.38213921 -2.88960266 12.28287315
		 12.54092598 -2.88960218 6.89822626 -0.69059849 -12.85739136 11.54257584 12.23246765 -12.85739136 6.1579299;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7" -p "frontLeft3";
	rename -uid "18C07746-4664-7A3E-40AC-5D91CD75FCEC";
	setAttr ".rp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
	setAttr ".sp" -type "double3" -3.7760666701041621 -5.3872715487119738 14.462545134755448 ;
createNode mesh -n "pCubeShape7" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "6256EB2D-474C-CFDC-4C55-80BEA832B066";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.3908882 0 0 -22.160103 
		0 0 -7.5521345 0 0 -22.321348 0 0 -6.0478439 0 0 -20.817059 0 0 -5.8865986 0 0 -20.655813 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.69544411 -6.36505508 14.26905155 11.080051422 -6.36505508 11.19211197
		 3.77606726 -5.38727188 14.46254539 11.1606741 -5.38727188 11.38560486 3.023921967 -4.96803474 12.65740871
		 10.40852928 -4.96803474 9.58046818 2.94329929 -5.94581842 12.46391582 10.32790661 -5.94581842 9.38697529;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "1668818A-4BB5-549C-6343-30A3FEE4547F";
	setAttr ".rp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
	setAttr ".sp" -type "double3" -3.695443689825503 -6.3650547854705808 14.269051266726381 ;
createNode mesh -n "pCubeShape8" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube8";
	rename -uid "F7A4CEAE-434C-3AE8-2A92-1BAFBD6E3A42";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -8.9145479 
		0 0 -7.3908882 0 0 -9.2370405 0 0 -5.8865986 0 0 -7.7327509 0 0 -5.564106 0 0 -7.4102583 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 4.45727396 -8.32062149 13.49744701
		 3.69544411 -6.36505508 14.26905155 4.61852026 -6.36505508 13.8844347 2.94329929 -5.94581842 12.46391582
		 3.86637545 -5.94581842 12.079298973 2.78205299 -7.90138435 12.076927185 3.70512915 -7.90138435 11.69231033;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "CC8ACE55-4EB5-FDE7-E18B-689B796CA117";
	setAttr ".rp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
	setAttr ".sp" -type "double3" -3.5341977292681821 -8.3206212589877921 13.882063530668246 ;
createNode mesh -n "pCubeShape11" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube11";
	rename -uid "1385D34A-4516-DBA4-9F76-DAA67AB24F03";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.0683966 0 0 -13.969256 
		0 0 -8.3050871 0 0 -15.205947 0 0 -6.8007975 0 0 -13.701655 0 0 -5.564106 0 0 -12.464966 
		0 0;
	setAttr -s 8 ".vt[0:7]"  3.53419828 -8.32062149 13.88206387 6.9846282 -11.25397968 11.76311302
		 4.15254354 -7.53839588 13.80608845 7.60297346 -10.47175312 11.68713665 3.40039873 -7.11915922 12.00095272064
		 6.85082769 -10.052515984 9.88199997 2.78205299 -7.90138435 12.076927185 6.23248291 -10.83474159 9.95797634;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "34D68EA8-4803-4E79-C904-A684F4BD506D";
	setAttr ".rp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
	setAttr ".sp" -type "double3" -10.757559900686177 -10.276187732505003 10.418135311408646 ;
createNode mesh -n "pCubeShape10" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube10";
	rename -uid "396814A8-4D2E-D144-C8D0-2A9FD7C569D4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -21.353872 0 0 -21.51512 
		0 0 -13.969266 0 0 -14.130511 0 0 -12.464975 0 0 -12.626222 0 0 -19.849583 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  10.67693615 -11.2539711 10.2246418 10.75755978 -10.27618885 10.4181366
		 6.98463297 -11.2539711 11.76311302 7.065255642 -10.27618885 11.95660782 6.23248768 -10.83473492 9.95797539
		 6.31311083 -9.85695076 10.15146923 9.92479134 -10.83473492 8.41950512 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "8522D05E-4841-D0FF-366B-F5A8D9869DBB";
	setAttr ".rp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
	setAttr ".sp" -type "double3" -11.080051821800817 -6.3650547854705808 11.192110783524916 ;
createNode mesh -n "pCubeShape9" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube9";
	rename -uid "D38F2CC7-4397-B5F8-0172-CABCEE855294";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -19.668966 0 0 -21.51512 
		0 0 -20.313951 0 0 -22.160103 0 0 -18.809662 0 0 -20.655813 0 0 -18.164677 0 0 -20.010828 
		0 0;
	setAttr -s 8 ".vt[0:7]"  9.83448315 -10.27618885 10.8027544 10.75755978 -10.27618885 10.4181366
		 10.15697575 -6.36505508 11.57672882 11.080051422 -6.36505508 11.19211197 9.40483093 -5.94581842 9.77159309
		 10.32790661 -5.94581842 9.38697529 9.082338333 -9.85695076 8.99761677 10.0054140091 -9.85695076 8.61299896;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "19710E1A-469E-9039-EA80-70A346E1E157";
	setAttr ".rp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
	setAttr ".sp" -type "double3" -6.7496504667852246 -9.3332317420322273 11.199157698846586 ;
createNode mesh -n "pCubeShape15" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube15";
	rename -uid "D8143C51-4CEB-F78A-FE7C-67BD22386525";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.7661715 -10.002368 11.40912 
		-12.802967 -10.002368 9.7271099 -2.7687492 -10.004949 11.415309 -12.805545 -10.004949 
		9.7332993 -2.1979952 -8.6640949 12.045508 -12.234791 -8.6640949 10.3635 -2.1954165 
		-8.6615152 12.03932 -12.232212 -8.6615152 10.35731;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "97D92174-410F-884E-8D68-1385374252ED";
	setAttr ".rp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
	setAttr ".sp" -type "double3" -6.8302734470638828 -8.3554485052736212 11.392651566875651 ;
createNode mesh -n "pCubeShape14" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube14";
	rename -uid "6D077C63-4F2E-29B8-D239-BB905E4595F0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.3451328 -9.0245857 12.22831 
		-12.883589 -9.0245857 9.9206047 -1.3477111 -9.0271645 12.234498 -12.886168 -9.0271654 
		9.9267941 -0.77695751 -7.6863122 12.864698 -12.315414 -7.6863122 10.556993 -0.77437878 
		-7.6837316 12.85851 -12.312836 -7.6837316 10.550804;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "35D6485E-4C33-D003-E0E1-919F8F1C20D7";
	setAttr ".rp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
	setAttr ".sp" -type "double3" -6.9108964273425446 -7.3776652685150141 11.58614543490472 ;
createNode mesh -n "pCubeShape13" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube13";
	rename -uid "DDA181E2-4B40-A65D-2F5C-35AEF94596B8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.4257555 -8.0468016 12.421804 
		-12.964212 -8.0468016 10.114099 -1.4283352 -8.0493822 12.427992 -12.96679 -8.0493822 
		10.120287 -0.85758018 -6.708529 13.058191 -12.396036 -6.7085285 10.750487 -0.8550024 
		-6.7059488 13.052003 -12.393457 -6.7059488 10.744298;
	setAttr -s 8 ".vt[0:7]"  -3 -0.0049999999 1 3 -0.0049999999 1 -3 0.0049999999 1
		 3 0.0049999999 1 -3 0.0049999999 -1 3 0.0049999999 -1 -3 -0.0049999999 -1 3 -0.0049999999 -1;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7";
	rename -uid "9BB721EF-4A61-C40A-CE46-D291E9E0E748";
	setAttr ".rp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
	setAttr ".sp" -type "double3" -6.9915194076212046 -6.3998820317564098 11.779639302933788 ;
createNode mesh -n "pCubeShape12" -p "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube12";
	rename -uid "2F531F50-4CFC-CA61-6F5E-3486BFB98488";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9.0127592 0 0 -20.08967 
		0 0 -9.0179157 0 0 -20.094828 0 0 -7.8764067 0 0 -18.953318 0 0 -7.8712502 0 0 -18.94816 
		0 0;
	setAttr -s 8 ".vt[0:7]"  4.5063796 -7.074018478 13.61529922 10.044835091 -7.074018478 11.30759335
		 4.50895786 -7.066599846 13.62148666 10.047413826 -7.066599846 11.31378174 3.93820333 -5.72574568 12.2516861
		 9.47665882 -5.72574568 9.94398022 3.93562508 -5.73316574 12.2454977 9.47408009 -5.73316574 9.93779278;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontRight1" -p "frontSide";
	rename -uid "4597C8EE-4951-8DB2-268E-CF8D4274CC89";
	setAttr ".rp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
	setAttr ".sp" -type "double3" -1.0000000000000018 0 13.000000000000004 ;
createNode mesh -n "frontRightShape1" -p "frontRight1";
	rename -uid "65BFBE3B-40CA-3AE2-BC95-7FA64C9617C9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -6.4999886 -1.5 6.4999719 
		-7.5 -1.5 11.5 -6.4999886 -1.5 6.4999719 -7.5 -1.5 11.5 -5.3461356 -1.5 6.7307439 
		-6.3461475 -1.5 11.730772 -5.3461356 -1.5 6.7307439 -6.3461475 -1.5 11.730772;
	setAttr -s 8 ".vt[0:7]"  -6.5 -1.5 1.5 6.5 -1.5 1.5 -6.5 1.5 1.5 6.5 1.5 1.5
		 -6.5 1.5 -1.5 6.5 1.5 -1.5 -6.5 -1.5 -1.5 6.5 -1.5 -1.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "jawMid" -p "frontSide";
	rename -uid "89DD58D2-4EBA-FEA5-4620-779AD1CDDDB9";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "jawMidShape" -p "jawMid";
	rename -uid "BBF1F4BB-4B86-E8BB-08F6-36B6004C7BDD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube6" -p "jawMid";
	rename -uid "244AC02B-40FF-1E35-DA16-9E87AD3FC4B0";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "2D86932E-400D-DFAB-892B-19933F24CE01";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  30.304123 -18.700001 12.7 
		6.4461813 -18.700001 12.7 26.457947 -18.700001 12.7 2.6000054 -18.700001 12.7 24.327759 
		-18.700001 12.7 0.46981788 -18.700001 12.7 28.173935 -18.700001 12.7 4.3159933 -18.700001 
		12.7;
	setAttr -s 8 ".vt[0:7]"  -15.15206146 1.20867193 -5.36155796 -3.22309065 -4.54291534 -0.82075399
		 -13.22897339 5.76722908 -4.63955355 -1.30000269 0.01564152 -0.098749459 -12.16387939 5.76210356 -7.4441123
		 -0.23490894 0.01051604 -2.90330839 -14.086967468 1.20354652 -8.16611671 -2.15799665 -4.54804087 -3.62531281;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube5" -p "jawMid";
	rename -uid "5C15DB6A-40CF-A28D-8F27-77A9656A0CDB";
	setAttr ".rp" -type "double3" 0 -18.7 12.7 ;
	setAttr ".sp" -type "double3" 0 -18.7 12.7 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "39E10765-44E7-234D-22B2-F2BA48648DB0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethmid" -p "frontSide";
	rename -uid "298B6792-4E41-C0F5-614B-86BFC82313CE";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11 ;
createNode mesh -n "teethmidShape" -p "teethmid";
	rename -uid "FA1D085F-4E2E-5401-EC68-34BB66F7FDCB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethLeft" -p "teethmid";
	rename -uid "BDFBD9ED-489C-8419-DE9B-FE899DE6FE67";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethLeftShape" -p "teethLeft";
	rename -uid "85E2D2E6-4D90-2E60-D420-6A86E5506DE2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 3 ".pt";
	setAttr ".pt[1]" -type "float3" -1.4901161e-08 2.3436136 0 ;
	setAttr ".pt[3]" -type "float3" -1.4901161e-08 0 0 ;
	setAttr ".pt[7]" -type "float3" 0 2.3436136 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethRight" -p "teethmid";
	rename -uid "0BBCA9C6-4D6B-0074-7A2F-C0A378F8E720";
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethRightShape" -p "teethRight";
	rename -uid "2DC87D9D-46ED-BB5C-9CC4-77A69DB4A2E3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1 0 0 -25.992804 2.3436136 
		0 -1 0 0 -25.992804 0 0 -0.44872528 0 0 -25.441528 0 0 -0.44872528 0 0 -25.441528 
		2.3436136 0;
	setAttr -s 8 ".vt[0:7]"  0.5 -19.5 11.5 12.99640179 -19.5 7.91671467
		 0.5 -12.5 11.5 12.99640179 -12.5 7.91671467 0.22436264 -12.5 10.53873825 12.72076416 -12.5 6.95545292
		 0.22436264 -19.5 10.53873825 12.72076416 -19.5 6.95545292;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Legs" -p "BasicMechGrouped";
	rename -uid "0E955BC1-4964-2BAE-1301-8C89317036A7";
	setAttr ".rp" -type "double3" 0 -25.5 -0.26776695251464844 ;
	setAttr ".sp" -type "double3" 0 -25.5 -0.26776695251464844 ;
createNode transform -n "hips" -p "|BasicMechGrouped|Basic_Legs";
	rename -uid "FF221B08-4D2C-B8C4-D3E0-6A9FB22B3148";
	setAttr ".rp" -type "double3" 0 -22 0 ;
	setAttr ".sp" -type "double3" 0 -22 0 ;
createNode mesh -n "hipsShape" -p "hips";
	rename -uid "77109185-401B-E0D8-99D4-9E8E8A364F90";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegLeft" -p "hips";
	rename -uid "42BBF380-45E3-7FA5-B896-15BC05AED86A";
	setAttr ".rp" -type "double3" 8 -22 0 ;
	setAttr ".sp" -type "double3" 8 -22 0 ;
createNode mesh -n "upperLegLeftShape" -p "upperLegLeft";
	rename -uid "5EA57348-475E-14C4-5813-288368A89BE5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegLeft" -p "upperLegLeft";
	rename -uid "228A07EC-48C7-D1B5-C59A-CA93721A23C5";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "lowerLegLeftShape" -p "lowerLegLeft";
	rename -uid "2F94FD93-495B-848A-C8EC-43804F051E67";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeLeft" -p "upperLegLeft";
	rename -uid "8C34651A-4401-EFE8-7775-988B897B7665";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "kneeLeftShape" -p "kneeLeft";
	rename -uid "DC8270F5-4118-DFD7-DFA5-80B685231A9C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegRight" -p "hips";
	rename -uid "9B70C2EC-4F64-7511-B019-008BE4895002";
	setAttr ".rp" -type "double3" -8 -22 0 ;
	setAttr ".sp" -type "double3" -8 -22 0 ;
createNode mesh -n "upperLegRightShape" -p "upperLegRight";
	rename -uid "5F05A66E-43B5-DCB9-ACE4-44B565BA7A15";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10 -22 0 -22 -22 0 -10 -22 
		0 -22 -22 0 -10 -22 0 -22 -22 0 -10 -22 0 -22 -22 0;
	setAttr -s 8 ".vt[0:7]"  5 -5 3 11 -5 3 5 3 3 11 3 3 5 3 -3 11 3 -3
		 5 -5 -3 11 -5 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegRight" -p "upperLegRight";
	rename -uid "F8F9085D-4AAD-48B5-138A-9A97137A60CF";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "lowerLegRightShape" -p "lowerLegRight";
	rename -uid "CFEB7661-434F-3C86-89D4-ABBEC9BFE28F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9 -22 0 -23 -22 0 -9 -22 
		0 -23 -22 0 -9 -22 0 -23 -22 0 -9 -22 0 -23 -22 0;
	setAttr -s 8 ".vt[0:7]"  4.5 -11 3 11.5 -11 3 4.5 -5 3 11.5 -5 3 4.5 -5 -4
		 11.5 -5 -4 4.5 -11 -4 11.5 -11 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeRight" -p "upperLegRight";
	rename -uid "8CE62129-4288-F4FE-0934-6FBF0BAFC905";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "kneeRightShape" -p "kneeRight";
	rename -uid "D4AE6C18-46FD-A0C5-74F7-55B272B7B015";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8 -22 0 -24 -22 0 -8 -22 
		0 -24 -22 0 -8 -22 0 -24 -22 0 -8 -22 0 -24 -22 0;
	setAttr -s 8 ".vt[0:7]"  4 -8.53553391 -2 12 -8.53553391 -2 4 -5 1.53553391
		 12 -5 1.53553391 4 -1.46446609 -2 12 -1.46446609 -2 4 -5 -5.53553391 12 -5 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft1" -p "hips";
	rename -uid "06472B54-43CE-28A3-B943-D9BDAB2EA2FA";
	setAttr ".rp" -type "double3" 12 -20 -1 ;
	setAttr ".sp" -type "double3" 12 -20 -1 ;
createNode mesh -n "panelLeftShape1" -p "panelLeft1";
	rename -uid "DBFD30E4-4851-2760-62DE-C197E02BF610";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft2" -p "panelLeft1";
	rename -uid "E32EE436-4527-A180-E176-B59AE16172E1";
	setAttr ".rp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
	setAttr ".sp" -type "double3" 11.5 -16.5 3.9999999999999964 ;
createNode mesh -n "panelLeftShape2" -p "panelLeft2";
	rename -uid "8B9BCF4D-47FC-F698-98BB-6DACEA0D097E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight1" -p "hips";
	rename -uid "F68B51CC-4D47-0E73-FF16-88A6A326CC32";
	setAttr ".rp" -type "double3" -12.000000000000004 -20 -1 ;
	setAttr ".sp" -type "double3" -12.000000000000004 -20 -1 ;
createNode mesh -n "panelRightShape1" -p "panelRight1";
	rename -uid "94705DCE-447C-DE62-7C01-7987E04AA3E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.462697 -20.005127 3.5 
		-17.462698 -20.005127 -6.5 -5.5743756 -20.264633 3.5 -15.574375 -20.264633 -6.5 -6.537303 
		-19.994873 4.5 -16.537302 -19.994873 -5.5 -8.4256248 -19.735367 4.5 -18.425625 -19.735367 
		-5.5;
	setAttr -s 8 ".vt[0:7]"  -5 -3.5 0.5 5 -3.5 0.5 -5 3.5 0.5 5 3.5 0.5
		 -5 3.5 -0.5 5 3.5 -0.5 -5 -3.5 -0.5 5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight2" -p "panelRight1";
	rename -uid "83C52A54-4F1F-904E-B235-1FA276B726FB";
	setAttr ".rp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" -11.499999999999998 -16.5 4.0000000000000018 ;
createNode mesh -n "panelRightShape2" -p "panelRight2";
	rename -uid "3E1D1CC0-4F0D-A0E9-D73E-D4A0EA4779E2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8.8865137 -19.740492 5.1672893 
		-9.7058802 -19.740492 8.4535904 -8 -20 3.5 -8.8193665 -20 6.7863011 -7.5479331 -20.26976 
		3.6497855 -8.3673 -20.26976 6.9360862 -8.4344463 -20.010252 5.3170743 -9.2538128 
		-20.010252 8.6033754;
	setAttr -s 8 ".vt[0:7]"  -3.5 -3.5 0.5 3.5 -3.5 0.5 -3.5 3.5 0.5 3.5 3.5 0.5
		 -3.5 3.5 -0.5 3.5 3.5 -0.5 -3.5 -3.5 -0.5 3.5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Right_Arm" -p "BasicMechGrouped";
	rename -uid "F3B002F6-4980-5CC0-B5B0-FB934E143A8D";
	setAttr ".rp" -type "double3" -12 -7.5 4 ;
	setAttr ".sp" -type "double3" -12 -7.5 4 ;
createNode transform -n "armRight1" -p "|BasicMechGrouped|Basic_Right_Arm";
	rename -uid "BD74FAE6-410F-1FC1-D37E-9DA76F5EBB34";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armRightShape1" -p "armRight1";
	rename -uid "F9CA20B8-4D87-D5E4-A152-A59C959A712A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmRight" -p "armRight1";
	rename -uid "55D5A9D1-42CF-366A-9909-E39182F42194";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmRightShape" -p "upperArmRight";
	rename -uid "74D767FC-49D7-8FF4-1FA1-1A841375FD7D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmRight" -p "upperArmRight";
	rename -uid "124826B8-4A44-F82F-FF5C-BF8E08AF2D8B";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmRightShape" -p "lowerArmRight";
	rename -uid "7F70E54D-4C9C-07D2-9469-0CA2707F78A2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armRight2" -p "armRight1";
	rename -uid "5FB98D3E-4CD7-A706-BE56-3E8AC5A0CE51";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armRightShape2" -p "armRight2";
	rename -uid "0473C47A-4B4E-11A3-D0CB-0C8D83367D91";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Left_Arm" -p "BasicMechGrouped";
	rename -uid "DB84E1AA-438F-A86B-DDBC-73B4CCC1BA99";
	setAttr ".rp" -type "double3" 18 -7.5 4.0000000000000018 ;
	setAttr ".sp" -type "double3" 18 -7.5 4.0000000000000018 ;
createNode transform -n "armLeft1" -p "|BasicMechGrouped|Basic_Left_Arm";
	rename -uid "5368470F-49AB-1828-DF8C-DFAA22B8B83D";
	setAttr ".rp" -type "double3" -18 1 0 ;
	setAttr ".rpt" -type "double3" 36 0 2.2043642384652358e-15 ;
	setAttr ".sp" -type "double3" -18 1 0 ;
createNode mesh -n "armLeftShape1" -p "armLeft1";
	rename -uid "3E95B1B2-4291-169C-A142-8995F4B3B91B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -24 -2 6 -12 -2 6 -24 7 6 -12 7 6 -24 7 -6
		 -12 7 -6 -24 -2 -6 -12 -2 -6;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperArmLeft" -p "armLeft1";
	rename -uid "A85A8278-406C-58A5-0534-F4BB74D2C79C";
	setAttr ".rp" -type "double3" -18 -11 0 ;
	setAttr ".sp" -type "double3" -18 -11 0 ;
createNode mesh -n "upperArmLeftShape" -p "upperArmLeft";
	rename -uid "DE602BD4-425D-C2F9-FB0D-A283FC4BDE5E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -21 -17 3 -15 -17 3 -21 -5 3 -15 -5 3 -21 -5 -3
		 -15 -5 -3 -21 -17 -3 -15 -17 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerArmLeft" -p "upperArmLeft";
	rename -uid "40E8FABA-44ED-FEEC-C5DD-8B905F34991C";
	setAttr ".rp" -type "double3" -18 -14 2 ;
	setAttr ".sp" -type "double3" -18 -14 2 ;
createNode mesh -n "lowerArmLeftShape" -p "lowerArmLeft";
	rename -uid "09F812BB-4C5F-0F18-33FB-4397A9EA1DB5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -22 -22 15 -14 -22 15 -22 -14 15 -14 -14 15
		 -22 -14 -7 -14 -14 -7 -22 -22 -7 -14 -22 -7;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "armLeft2" -p "armLeft1";
	rename -uid "3AC2A0E7-42FD-1EB0-DABF-8C97ADFE498B";
	setAttr ".rp" -type "double3" -18 -3.5 0 ;
	setAttr ".sp" -type "double3" -18 -3.5 0 ;
createNode mesh -n "armLeftShape2" -p "armLeft2";
	rename -uid "9E99AC44-4840-37B9-5266-8FAC8A536702";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -23 -5 5 -13 -5 5 -23 -2 5 -13 -2 5 -23 -2 -5
		 -13 -2 -5 -23 -5 -5 -13 -5 -5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "BasicMechGrouped1";
	rename -uid "3976F5F6-4C7B-341A-4D60-AAB8DA883134";
	setAttr ".t" -type "double3" 0 33 0 ;
	setAttr ".rp" -type "double3" 0 -13 -0.7107086181640625 ;
	setAttr ".sp" -type "double3" 0 -13 -0.7107086181640625 ;
createNode transform -n "Cubic_Left_Arm_old";
	rename -uid "9076766E-4158-473E-46B7-A09A1641EE55";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 18.000000000000004 34 0 ;
	setAttr ".sp" -type "double3" 18.000000000000004 34 0 ;
createNode mesh -n "Cubic_Left_Arm_oldShape" -p "Cubic_Left_Arm_old";
	rename -uid "F85ABC13-49D0-7555-6CCE-CB8437058074";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_Left_Arm_oldShapeOrig" -p "Cubic_Left_Arm_old";
	rename -uid "F78BDBA4-4BA1-9DE0-9640-14BD78EFA298";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 56 ".uvst[0].uvsp[0:55]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".vt[0:31]"  12 31 6 24 31 6 12 40 6 24 40 6 12 40 -6
		 24 40 -6 12 31 -6 24 31 -6 15 16 3 21 16 3 15 28 3 21 28 3 15 28 -3 21 28 -3 15 16 -3
		 21 16 -3 14 11 15 22 11 15 14 19 15 22 19 15 14 19 -7 22 19 -7 14 11 -7 22 11 -7
		 13 28 5 23 28 5 13 31 5 23 31 5 13 31 -5 23 31 -5 13 28 -5 23 28 -5;
	setAttr -s 48 ".ed[0:47]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0
		 11 13 0 12 14 0 13 15 0 14 8 0 15 9 0 16 17 0 18 19 0 20 21 0 22 23 0 16 18 0 17 19 0
		 18 20 0 19 21 0 20 22 0 21 23 0 22 16 0 23 17 0 24 25 0 26 27 0 28 29 0 30 31 0 24 26 0
		 25 27 0 26 28 0 27 29 0 28 30 0 29 31 0 30 24 0 31 25 0;
	setAttr -s 24 -ch 96 ".fc[0:23]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 12 17 -14 -17
		mu 0 4 14 15 16 17
		f 4 13 19 -15 -19
		mu 0 4 17 16 18 19
		f 4 14 21 -16 -21
		mu 0 4 19 18 20 21
		f 4 15 23 -13 -23
		mu 0 4 21 20 22 23
		f 4 -24 -22 -20 -18
		mu 0 4 15 24 25 16
		f 4 22 16 18 20
		mu 0 4 26 14 17 27
		f 4 24 29 -26 -29
		mu 0 4 28 29 30 31
		f 4 25 31 -27 -31
		mu 0 4 31 30 32 33
		f 4 26 33 -28 -33
		mu 0 4 33 32 34 35
		f 4 27 35 -25 -35
		mu 0 4 35 34 36 37
		f 4 -36 -34 -32 -30
		mu 0 4 29 38 39 30
		f 4 34 28 30 32
		mu 0 4 40 28 31 41
		f 4 36 41 -38 -41
		mu 0 4 42 43 44 45
		f 4 37 43 -39 -43
		mu 0 4 45 44 46 47
		f 4 38 45 -40 -45
		mu 0 4 47 46 48 49
		f 4 39 47 -37 -47
		mu 0 4 49 48 50 51
		f 4 -48 -46 -44 -42
		mu 0 4 43 52 53 44
		f 4 46 40 42 44
		mu 0 4 54 42 45 55;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pPlane1";
	rename -uid "DDE00D9F-407E-915D-2128-CA839588B754";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 -33 0 ;
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	rename -uid "1CD8CF06-4B3E-EA7E-F44A-9DBAE20B8F31";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Legs";
	rename -uid "34D4EABC-4766-36DC-3763-5BA0A9815CC5";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 7.7525634765625 1.4767951965332031 ;
	setAttr ".sp" -type "double3" 0 7.7525634765625 1.4767951965332031 ;
createNode mesh -n "Basic_LegsShape" -p "|Basic_Legs";
	rename -uid "BFA18339-4DF8-515C-3AE2-BDB97C7FA7A0";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000004470348358 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Basic_LegsShapeOrig" -p "|Basic_Legs";
	rename -uid "6CD7AB68-4DBA-D9CC-5B33-85BA1024A675";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 328 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.42833641 0.5 0.375 0.64426696
		 0.125 0.078117192 0.18749374 -3.7252903e-09 0.32166359 0.25 0.32166359 0.10573305
		 0.29041356 0.066670537 0.42833641 0.93750626 0.375 0.91541356 0.375 0.83458638 0.17833641
		 0.10573304 0.20958623 0.066670544 0.17833641 0.25 0.375 0.30333641 0.625 0.30333641
		 0.625 0.44666359 0.375 0.44666359 0.875 0.25 0.82166356 0.25 0.875 0.078117192 0.82166362
		 0.10573305 0.81250626 -3.7252903e-09 0.57166356 0.25 0.60749179 -3.7252903e-09 0.79041356
		 0.066670537 0.67833638 0.10573304 0.70958626 0.066670544 0.67833638 0.25 0.42833635
		 0.078117192 0.42833641 0.25 0.57166356 0.5 0.57166356 0.67188281 0.42833635 0.81249374
		 0.57166356 0.93750626 0.39250818 -3.7252903e-09 0.57166356 0.078117184 0.42833641
		 0.67188281 0.57166356 0.81249374 0.375 0.81249374 0.43749374 0.75 0.56250626 0.75
		 0.625 0.81249374 0.625 0.93750626 0.56250626 1 0.43749374 1 0.375 0.93750626 0.42833641
		 0.33458641 0.375 0.31249374 0.43749374 0.20999765 0.45958641 0.30333641 0.31250626
		 0.20999765 0.29041359 0.25 0.20958641 0.25 0.18749376 0.20999765 0.375 0.49750778
		 0.42833644 0.41541359 0.45958638 0.44666359 0.43749374 0.54000235 0.56250626 0.20999765
		 0.54041356 0.30333641 0.625 0.31249374 0.5716635 0.33458635 0.70958644 0.25 0.68749374
		 0.20999765 0.81250632 0.20999765 0.79041368 0.25 0.57166362 0.41541359 0.625 0.49750778
		 0.56250626 0.54000235 0.54041344 0.44666356 0.43749374 -7.4505806e-09 0.56250626
		 -7.4505806e-09 0.68749374 -7.4505806e-09 0.81250626 -7.4505806e-09 0.18749374 -7.4505806e-09
		 0.31250626 -7.4505806e-09 0.43749374 0.25 0.43749374 0 0.56250626 3.7252903e-09 0.56250626
		 0.25 0.43749374 0.75 0.43749374 0.5 0.56250626 0.5 0.56250626 0.75 0.68749374 0.25
		 0.68749374 0 0.8125062 0 0.81250626 0.25 0.18749376 0.25 0.18749376 0 0.31250623
		 0 0.31250623 0.25 0.375 0.31249374 0.625 0.31249374 0.625 0.43750623 0.375 0.43750623
		 0.375 0.81249374 0.625 0.81249374 0.625 0.93750626 0.56250626 1 0.43749374 1 0.375
		 0.93750626 0.32166362 0.084586345 0.40833521 0.062493715 0.40833524 0.18750626 0.32166359
		 0.16541359 0.29041365 0.05333636 0.36250746 6.4571126e-09 0.31250626 0.25 0.29041353
		 0.19666353 0.40833524 0.93750626 0.37499997 0.91541362 0.375 0.83458638 0.40833521
		 0.81249368 0.67833644 0.084586397 0.59166479 0.062493723 0.6374926 9.9346753e-10
		 0.70958638 0.053336449 0.67833644 0.16541357 0.59166479 0.18750626 0.70958644 0.19666362
		 0.68749374 0.24999999 0.625 0.91541362 0.59166479 0.93750626 0.59166479 0.81249374
		 0.625 0.83458638 0.375 0.33458641 0.40833527 0.31249374 0.40833524 0.43750626 0.375
		 0.41541362 0.20958635 0.19666357 0.18749379 0.25 0.125 0.18750627 0.17833641 0.16541359
		 0.59166479 0.31249374 0.625 0.33458641 0.625 0.41541362 0.59166479 0.43750626 0.81250626
		 0.25 0.79041356 0.19666354 0.82166356 0.16541363 0.875 0.18750627 0.375 0.58458632
		 0.40833521 0.56249368 0.40833524 0.68750626 0.375 0.66541362 0.17833641 0.084586434
		 0.12500003 0.062493745 0.18749374 2.2351742e-08 0.20958641 0.053336404 0.59166479
		 0.56249374 0.625 0.58458638 0.625 0.66541362 0.59166479 0.68750626 0.875 0.062493756
		 0.82166362 0.084586434 0.79041356 0.053336367 0.81250626 7.4505806e-09 0.43749374
		 0.75 0.375 0.81249374 0.375 0.93750626 0.43749374 1 0.56250626 1 0.625 0.93750626
		 0.625 0.81249374 0.56250626 0.75 0.29041359 0.25 0.31250626 0.20999765 0.18749376
		 0.20999765 0.20958641 0.25 0.375 0.31249374 0.42833641 0.33458641 0.45958638 0.30333641
		 0.43749374 0.20999765 0.42833641 0.41541356 0.375 0.49750778 0.43749374 0.54000235
		 0.45958638 0.44666353 0.54041362 0.30333641 0.56250626 0.20999765 0.57166362 0.33458653
		 0.625 0.31249374 0.68749374 0.20999765 0.70958644 0.25 0.79041362 0.25 0.81250632
		 0.20999765 0.625 0.49750778 0.57166356 0.41541347 0.54041356 0.44666362 0.56250626
		 0.54000235 0.43749374 0 0.56250626 0 0.68749374 0 0.81250632 0 0.18749376 0 0.31250626
		 0 0.43749377 0 0.43749374 0.25 0.56250626 0.25 0.56250626 0 0.43749377 0.5 0.43749374
		 0.75 0.56250626 0.75 0.56250626 0.5 0.6874938 0 0.68749374 0.25 0.81250626 0.25 0.81250632
		 0 0.18749377 0 0.18749376 0.25 0.31250623 0.25 0.31250626 0 0.375 0.31249374 0.375
		 0.4375062 0.625 0.43750623 0.625 0.31249374 0.375 0.81249374 0.375 0.9375062 0.43749374
		 1 0.56250626 1 0.625 0.93750626 0.625 0.81249374 0.67833644 0.084586382 0.59166479
		 0.06249373 0.59166479 0.18750626 0.67833638 0.16541359 0.7095865 0.053336393 0.63749254
		 0 0.68749374 0.24999999 0.70958644 0.19666363 0.59166479 0.93750626 0.625 0.91541362
		 0.625 0.83458638 0.59166479 0.81249374 0.625 0.33458641 0.59166479 0.31249374 0.59166479
		 0.43750626 0.625 0.41541362 0.79041368 0.19666365 0.81250632 0.25 0.875 0.18750627
		 0.82166362 0.16541348 0.625 0.58458638 0.59166479 0.56249374 0.59166479 0.68750626
		 0.625 0.66541362 0.82166368 0.084586471 0.875 0.062493756 0.81250632 1.8310548e-08
		 0.79041368 0.053336415;
	setAttr ".uvst[0].uvsp[250:327]" 0.32166359 0.08458636 0.40833524 0.062493742
		 0.36250737 0 0.29041362 0.053336404 0.32166353 0.16541353 0.40833518 0.18750626 0.29041362
		 0.1966636 0.31250626 0.25 0.18749374 2.4414062e-08 0.20958644 0.053336408 0.2095864
		 0.19666363 0.18749379 0.25 0.17833638 0.16541363 0.125 0.18750624 0.17833641 0.084586412
		 0.12500003 0.062493749 0.40833524 0.31249374 0.40833518 0.43750626 0.40833524 0.56249374
		 0.40833518 0.68750626 0.40833524 0.81249374 0.40833524 0.93750626 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 200 ".vt";
	setAttr ".vt[0:165]"  -5 15 -2.86654329 -2.86654329 15 -5 -5 10.38345718 -2.86654329
		 -2.86654329 9.49975014 -5 -2.86654329 15 5 -5 15 2.86654329 -5 10.38345718 2.86654329
		 -2.86654329 9.49975014 5 -5 9.13345718 1.61654329 -2.86654329 7 2.5002501 -5 9.13345718 -1.61654329
		 -2.86654329 7 -2.5002501 2.86654329 15 -5 5 15 -2.86654329 5 10.38345718 -2.86654329
		 2.86654329 9.49975014 -5 2.86654329 15 5 5 15 2.86654329 5 10.38345718 2.86654329
		 2.86654329 9.49975014 5 5 9.13345718 1.61654329 2.86654329 7 2.5002501 5 9.13345718 -1.61654329
		 2.86654329 7 -2.5002501 5 6 1.50014997 6.4998498 6 3 11 6 1.50014997 9.50014973 6 3
		 5 6 -1.50014997 6.4998498 6 -3 11 6 -1.50014997 9.50014973 6 -3 6.28007412 14 0.96992606
		 5 12.71992493 1.50014997 6.28007412 14 -0.96992606 5 12.71992493 -1.50014997 7.030073643 14 1.719926
		 6.4998498 12.71992493 3 8.96992588 14 1.719926 9.50014973 12.71992493 3 9.71992588 14 0.96992594
		 11 12.71992493 1.50014997 9.71992588 14 -0.96992594 11 12.71992493 -1.50014997 7.030073643 14 -1.719926
		 6.4998498 12.71992493 -3 8.96992588 14 -1.719926 9.50014973 12.71992493 -3 4.5 0 1.250175
		 6.249825 0 3 4.5 6 1.250175 6.249825 6 3 11.5 0 1.250175 9.75017548 0 3 11.5 6 1.250175
		 9.75017548 6 3 4.5 6 -2.250175 6.249825 6 -4 4.5 0 -2.250175 6.249825 0 -4 9.75017548 6 -4
		 11.5 6 -2.250175 11.5 0 -2.250175 9.75017548 0 -4 4 4.41498947 -1.55805826 5.066728115 3.34826088 -1.11620486
		 4 5.55805779 -0.41498923 5.066728115 5.11620522 0.65173882 4 4.41498947 -2.44194174
		 5.066728115 3.34826088 -2.88379502 12 4.41498947 -1.55805826 10.93327236 3.34826088 -1.11620486
		 12 5.55805779 -0.41498923 10.93327236 5.11620522 0.65173882 12 4.41498947 -2.44194174
		 10.93327236 3.34826088 -2.88379502 4 6.44194221 -0.41498923 5.066728115 6.88379478 0.65173882
		 4 7.58501053 -1.55805826 5.066728115 8.65173912 -1.11620486 12 6.44194221 -0.41498923
		 10.93327236 6.88379478 0.65173882 12 7.58501053 -1.55805826 10.93327236 8.65173912 -1.11620486
		 4 7.58501053 -2.44194174 5.066728115 8.65173912 -2.88379502 4 6.44194221 -3.58501053
		 5.066728115 6.88379478 -4.65173864 12 7.58501053 -2.44194174 10.93327236 8.65173912 -2.88379502
		 12 6.44194221 -3.58501053 10.93327236 6.88379478 -4.65173864 4 5.55805779 -3.58501053
		 5.066728115 5.11620522 -4.65173864 12 5.55805779 -3.58501053 10.93327236 5.11620522 -4.65173864
		 -5 6 1.50014997 -6.4998498 6 3 -11 6 1.50014997 -9.50014973 6 3 -5 6 -1.50014997
		 -6.4998498 6 -3 -11 6 -1.50014997 -9.50014973 6 -3 -6.28007412 14 0.969926 -5 12.71992493 1.50014997
		 -6.28007412 14 -0.969926 -5 12.71992493 -1.50014997 -7.030073643 14 1.719926 -6.4998498 12.71992493 3
		 -8.96992588 14 1.719926 -9.50014973 12.71992493 3 -9.71992588 14 0.96992588 -11 12.71992493 1.50014997
		 -9.71992588 14 -0.96992588 -11 12.71992493 -1.50014997 -7.030073643 14 -1.719926
		 -6.4998498 12.71992493 -3 -8.96992588 14 -1.719926 -9.50014973 12.71992493 -3 -4.5 0 1.250175
		 -6.249825 0 3 -4.5 6 1.250175 -6.249825 6 3 -11.5 0 1.250175 -9.75017548 0 3 -11.5 6 1.250175
		 -9.75017548 6 3 -6.249825 6 -4 -4.5 6 -2.250175 -4.5 0 -2.250175 -6.249825 0 -4 -11.5 6 -2.250175
		 -9.75017548 6 -4 -11.5 0 -2.250175 -9.75017548 0 -4 -12 4.41498947 -1.55805826 -10.93327236 3.34826088 -1.11620486
		 -12 5.55805779 -0.41498923 -10.93327236 5.11620522 0.65173882 -12 4.41498947 -2.44194174
		 -10.93327236 3.34826088 -2.88379502 -12 6.44194221 -0.41498923 -10.93327236 6.88379478 0.65173882
		 -12 7.58501053 -1.55805826 -10.93327236 8.65173912 -1.11620486 -12 7.58501053 -2.44194174
		 -10.93327236 8.65173912 -2.88379502 -12 6.44194221 -3.58501053 -10.93327236 6.88379478 -4.65173864
		 -12 5.55805779 -3.58501053 -10.93327236 5.11620522 -4.65173864 -4 4.41498947 -1.55805826
		 -5.066728115 3.34826088 -1.11620486 -4 5.55805779 -0.41498923 -5.066728115 5.11620522 0.65173882
		 -4 4.41498947 -2.44194174 -5.066728115 3.34826088 -2.88379502 -4 6.44194221 -0.41498923
		 -5.066728115 6.88379478 0.65173882 -4 7.58501053 -1.55805826 -5.066728115 8.65173912 -1.11620486
		 -4 7.58501053 -2.44194174 -5.066728115 8.65173912 -2.88379502 -4 6.44194221 -3.58501053
		 -5.066728115 6.88379478 -4.65173864;
	setAttr ".vt[166:199]" -4 5.55805779 -3.58501053 -5.066728115 5.11620522 -4.65173864
		 12.46269703 8.49487305 -6 12.46269703 8.49487305 4 10.57437515 15.23536682 -6 10.57437515 15.23536682 4
		 11.53730297 15.50512695 -6 11.53730297 15.50512695 4 13.42562485 8.76463318 -6 13.42562485 8.76463318 4
		 6.20588017 8.76463318 8.95359039 12.38651276 8.76463318 5.66728878 5.31936646 15.50512695 7.28630066
		 11.5 15.50512695 4 4.86730003 15.23536682 6.43608665 11.047932625 15.23536682 3.14978504
		 5.75381279 8.49487305 8.10337543 11.93444633 8.49487305 4.8170743 -12.46269703 8.49487305 4
		 -12.46269798 8.49487305 -6 -10.57437515 15.23536682 4 -10.57437515 15.23536682 -6
		 -11.53730297 15.50512695 4 -11.53730202 15.50512695 -6 -13.42562485 8.76463318 4
		 -13.42562485 8.76463318 -6 -12.38651371 8.76463509 5.66728926 -6.20588017 8.76463509 8.95359039
		 -11.5 15.50512695 4 -5.31936646 15.50512695 7.28630114 -11.047933578 15.23536682 3.14978552
		 -4.86730003 15.23536682 6.43608618 -11.93444633 8.49487495 4.8170743 -5.75381279 8.49487495 8.10337543;
	setAttr -s 328 ".ed";
	setAttr ".ed[0:165]"  1 12 0 1 0 0 4 16 0 5 0 0 4 5 0 1 3 0 3 2 0 2 0 0 3 11 0
		 11 10 0 10 2 0 5 6 0 6 7 0 7 4 0 6 8 0 8 9 0 9 7 0 8 10 0 11 9 0 13 12 0 17 13 0
		 16 17 0 13 14 0 14 15 0 15 12 0 14 22 0 22 23 0 23 15 0 16 19 0 19 18 0 18 17 0 19 21 0
		 21 20 0 20 18 0 21 23 0 22 20 0 7 19 0 15 3 0 11 23 0 21 9 0 25 27 0 25 24 0 26 27 0
		 28 24 0 29 31 0 29 28 0 30 26 0 31 30 0 32 33 0 33 37 0 37 36 0 36 32 0 32 34 0 34 35 0
		 35 33 0 34 44 0 44 45 0 45 35 0 37 39 0 39 38 0 38 36 0 39 41 0 41 40 0 40 38 0 41 43 0
		 43 42 0 42 40 0 43 47 0 47 46 0 46 42 0 44 46 0 47 45 0 37 25 0 27 39 0 29 45 0 47 31 0
		 41 26 0 30 43 0 35 28 0 24 33 0 49 53 0 49 48 0 50 56 0 51 55 0 51 50 0 52 53 0 54 61 0
		 55 54 0 57 60 0 57 56 0 58 48 0 59 63 0 59 58 0 61 60 0 62 52 0 63 62 0 48 50 0 51 49 0
		 53 55 0 54 52 0 56 58 0 59 57 0 60 63 0 62 61 0 64 65 0 65 67 0 67 66 0 66 64 0 64 68 0
		 68 69 0 69 65 0 67 77 0 77 76 0 76 66 0 68 92 0 92 93 0 93 69 0 70 71 0 71 75 0 75 74 0
		 74 70 0 70 72 0 72 73 0 73 71 0 72 80 0 80 81 0 81 73 0 75 95 0 95 94 0 94 74 0 77 79 0
		 79 78 0 78 76 0 79 85 0 85 84 0 84 78 0 80 82 0 82 83 0 83 81 0 82 88 0 88 89 0 89 83 0
		 85 87 0 87 86 0 86 84 0 87 93 0 92 86 0 88 90 0 90 91 0 91 89 0 90 94 0 95 91 0 65 71 0
		 73 67 0 77 81 0 83 79 0 85 89 0 91 87 0 93 95 0 75 69 0 97 99 0 96 97 0 99 98 0 100 96 0
		 101 103 0 100 101 0;
	setAttr ".ed[166:327]" 102 98 0 102 103 0 104 105 0 105 107 0 107 106 0 106 104 0
		 104 108 0 108 109 0 109 105 0 107 117 0 117 116 0 116 106 0 108 110 0 110 111 0 111 109 0
		 110 112 0 112 113 0 113 111 0 112 114 0 114 115 0 115 113 0 114 118 0 118 119 0 119 115 0
		 117 119 0 118 116 0 97 109 0 111 99 0 117 101 0 103 119 0 98 113 0 115 102 0 100 107 0
		 105 96 0 121 125 0 120 121 0 122 129 0 123 127 0 122 123 0 125 124 0 126 132 0 126 127 0
		 128 133 0 129 128 0 130 120 0 131 135 0 130 131 0 133 132 0 134 124 0 134 135 0 121 123 0
		 122 120 0 124 126 0 127 125 0 128 131 0 130 129 0 132 134 0 135 133 0 136 137 0 137 139 0
		 139 138 0 138 136 0 136 140 0 140 141 0 141 137 0 139 143 0 143 142 0 142 138 0 140 150 0
		 150 151 0 151 141 0 143 145 0 145 144 0 144 142 0 145 147 0 147 146 0 146 144 0 147 149 0
		 149 148 0 148 146 0 149 151 0 150 148 0 152 153 0 153 157 0 157 156 0 156 152 0 152 154 0
		 154 155 0 155 153 0 154 158 0 158 159 0 159 155 0 157 167 0 167 166 0 166 156 0 158 160 0
		 160 161 0 161 159 0 160 162 0 162 163 0 163 161 0 162 164 0 164 165 0 165 163 0 164 166 0
		 167 165 0 137 153 0 155 139 0 143 159 0 161 145 0 147 163 0 165 149 0 151 167 0 157 141 0
		 168 169 0 170 171 0 172 173 0 174 175 0 168 170 0 169 171 0 170 172 0 171 173 0 172 174 0
		 173 175 0 174 168 0 175 169 0 176 177 0 178 179 0 180 181 0 182 183 0 176 178 0 177 179 0
		 178 180 0 179 181 0 180 182 0 181 183 0 182 176 0 183 177 0 184 185 0 186 187 0 188 189 0
		 190 191 0 184 186 0 185 187 0 186 188 0 187 189 0 188 190 0 189 191 0 190 184 0 191 185 0
		 192 193 0 194 195 0 196 197 0 198 199 0 192 194 0 193 195 0 194 196 0 195 197 0 196 198 0
		 197 199 0 198 192 0 199 193 0;
	setAttr -s 150 -ch 656 ".fc[0:149]" -type "polyFaces" 
		f 8 -5 2 21 20 19 -1 1 -4
		mu 0 8 13 29 22 14 15 30 0 16
		f 4 -2 5 6 7
		mu 0 4 16 0 36 1
		f 4 -7 8 9 10
		mu 0 4 10 2 3 11
		f 4 4 11 12 13
		mu 0 4 29 4 5 28
		f 4 -13 14 15 16
		mu 0 4 28 5 6 34
		f 4 -16 17 -10 18
		mu 0 4 7 8 9 32
		f 6 -11 -18 -15 -12 3 -8
		mu 0 6 10 11 6 5 4 12
		f 4 -20 22 23 24
		mu 0 4 17 18 20 19
		f 4 -24 25 26 27
		mu 0 4 19 20 24 21
		f 4 -22 28 29 30
		mu 0 4 27 22 35 25
		f 4 -30 31 32 33
		mu 0 4 25 35 23 26
		f 4 -33 34 -27 35
		mu 0 4 26 23 21 24
		f 6 -34 -36 -26 -23 -21 -31
		mu 0 6 25 26 24 20 18 27
		f 4 36 -29 -3 -14
		mu 0 4 28 35 22 29
		f 4 -6 0 -25 37
		mu 0 4 36 0 30 31
		f 4 38 -35 39 -19
		mu 0 4 32 37 33 7
		f 4 -17 -40 -32 -37
		mu 0 4 28 34 23 35
		f 4 -9 -38 -28 -39
		mu 0 4 32 36 31 37
		f 8 -46 44 47 46 42 -41 41 -44
		mu 0 8 38 39 40 41 42 43 44 45
		f 4 48 49 50 51
		mu 0 4 46 47 48 49
		f 4 -49 52 53 54
		mu 0 4 50 51 52 53
		f 4 -54 55 56 57
		mu 0 4 54 55 56 57
		f 4 -51 58 59 60
		mu 0 4 49 48 58 59
		f 4 -60 61 62 63
		mu 0 4 59 58 60 61
		f 4 -63 64 65 66
		mu 0 4 62 63 64 65
		f 4 -66 67 68 69
		mu 0 4 66 67 68 69
		f 4 -57 70 -69 71
		mu 0 4 57 56 69 68
		f 4 72 40 73 -59
		mu 0 4 48 70 71 58
		f 4 74 -72 75 -45
		mu 0 4 39 57 68 40
		f 4 76 -47 77 -65
		mu 0 4 63 72 73 64
		f 4 78 43 79 -55
		mu 0 4 53 74 75 50
		f 8 -52 -61 -64 -67 -70 -71 -56 -53
		mu 0 8 46 49 59 61 66 69 56 55
		f 4 -42 -73 -50 -80
		mu 0 4 75 70 48 50
		f 4 -43 -77 -62 -74
		mu 0 4 71 72 63 58
		f 4 -58 -75 45 -79
		mu 0 4 54 57 39 38
		f 4 -68 -78 -48 -76
		mu 0 4 68 67 41 40
		f 4 97 80 98 -84
		mu 0 4 76 77 78 79
		f 4 101 88 102 -92
		mu 0 4 80 81 82 83
		f 4 99 -95 103 -87
		mu 0 4 84 85 86 87
		f 4 100 90 96 82
		mu 0 4 88 89 90 91
		f 8 -85 83 87 86 93 -89 89 -83
		mu 0 8 92 76 79 93 94 82 81 95
		f 8 -93 91 95 94 85 -81 81 -91
		mu 0 8 96 80 83 97 98 99 100 101
		f 4 -82 -98 84 -97
		mu 0 4 90 77 76 91
		f 4 -86 -100 -88 -99
		mu 0 4 78 85 84 79
		f 4 -90 -102 92 -101
		mu 0 4 95 81 80 96
		f 4 -94 -104 -96 -103
		mu 0 4 82 94 97 83
		f 4 104 105 106 107
		mu 0 4 102 103 104 105
		f 4 -105 108 109 110
		mu 0 4 103 102 106 107
		f 4 -107 111 112 113
		mu 0 4 105 104 108 109
		f 4 -110 114 115 116
		mu 0 4 110 111 112 113
		f 4 117 118 119 120
		mu 0 4 114 115 116 117
		f 4 -118 121 122 123
		mu 0 4 115 114 118 119
		f 4 -123 124 125 126
		mu 0 4 119 118 120 121
		f 4 -120 127 128 129
		mu 0 4 122 123 124 125
		f 4 -113 130 131 132
		mu 0 4 126 127 128 129
		f 4 -132 133 134 135
		mu 0 4 130 131 132 133
		f 4 -126 136 137 138
		mu 0 4 134 135 136 137
		f 4 -138 139 140 141
		mu 0 4 138 139 140 141
		f 4 -135 142 143 144
		mu 0 4 142 143 144 145
		f 4 -144 145 -116 146
		mu 0 4 146 147 148 149
		f 4 -141 147 148 149
		mu 0 4 150 151 152 153
		f 4 -149 150 -129 151
		mu 0 4 154 155 156 157
		f 4 152 -124 153 -106
		mu 0 4 103 115 119 104
		f 4 154 -139 155 -131
		mu 0 4 127 134 137 128
		f 4 156 -150 157 -143
		mu 0 4 143 150 153 144
		f 4 158 -128 159 -117
		mu 0 4 113 124 123 110
		f 8 -121 -130 -151 -148 -140 -137 -125 -122
		mu 0 8 114 117 156 155 140 139 120 118
		f 8 -147 -115 -109 -108 -114 -133 -136 -145
		mu 0 8 146 149 106 102 105 109 130 133
		f 4 -111 -160 -119 -153
		mu 0 4 103 107 116 115
		f 4 -112 -154 -127 -155
		mu 0 4 127 104 119 134
		f 4 -134 -156 -142 -157
		mu 0 4 143 128 137 150
		f 4 -146 -158 -152 -159
		mu 0 4 113 144 153 124
		f 8 -166 163 161 160 162 -167 167 -165
		mu 0 8 158 159 160 161 162 163 164 165
		f 4 168 169 170 171
		mu 0 4 166 167 168 169
		f 4 -169 172 173 174
		mu 0 4 170 171 172 173
		f 4 -171 175 176 177
		mu 0 4 174 175 176 177
		f 4 -174 178 179 180
		mu 0 4 173 172 178 179
		f 4 -180 181 182 183
		mu 0 4 179 178 180 181
		f 4 -183 184 185 186
		mu 0 4 182 183 184 185
		f 4 -186 187 188 189
		mu 0 4 186 187 188 189
		f 4 -177 190 -189 191
		mu 0 4 177 176 189 188
		f 4 192 -181 193 -161
		mu 0 4 190 173 179 191
		f 4 194 164 195 -191
		mu 0 4 176 158 165 189
		f 4 196 -187 197 166
		mu 0 4 192 182 185 193
		f 4 198 -170 199 -164
		mu 0 4 194 168 167 195
		f 8 -173 -172 -178 -192 -188 -185 -182 -179
		mu 0 8 172 171 174 177 188 187 180 178
		f 4 -162 -200 -175 -193
		mu 0 4 190 195 167 173
		f 4 -163 -194 -184 -197
		mu 0 4 192 191 179 182
		f 4 -176 -199 165 -195
		mu 0 4 176 175 159 158
		f 4 -190 -196 -168 -198
		mu 0 4 186 189 165 164
		f 4 216 203 219 -201
		mu 0 4 196 197 198 199
		f 4 220 211 223 -209
		mu 0 4 200 201 202 203
		f 4 218 206 222 214
		mu 0 4 204 205 206 207
		f 4 221 -203 217 -211
		mu 0 4 208 209 210 211
		f 8 -205 202 209 208 213 -207 207 -204
		mu 0 8 197 212 213 200 203 214 215 198
		f 8 -213 210 201 200 205 -215 215 -212
		mu 0 8 201 216 217 218 219 220 221 202
		f 4 -202 -218 204 -217
		mu 0 4 196 211 210 197
		f 4 -206 -220 -208 -219
		mu 0 4 204 199 198 205
		f 4 -210 -222 212 -221
		mu 0 4 200 213 216 201
		f 4 -214 -224 -216 -223
		mu 0 4 214 203 202 221
		f 4 224 225 226 227
		mu 0 4 222 223 224 225
		f 4 -225 228 229 230
		mu 0 4 223 222 226 227
		f 4 -227 231 232 233
		mu 0 4 225 224 228 229
		f 4 -230 234 235 236
		mu 0 4 230 231 232 233
		f 4 -233 237 238 239
		mu 0 4 234 235 236 237
		f 4 -239 240 241 242
		mu 0 4 238 239 240 241
		f 4 -242 243 244 245
		mu 0 4 242 243 244 245
		f 4 -245 246 -236 247
		mu 0 4 246 247 248 249
		f 8 -229 -228 -234 -240 -243 -246 -248 -235
		mu 0 8 226 222 225 229 238 241 246 249
		f 4 248 249 250 251
		mu 0 4 250 251 252 253
		f 4 -249 252 253 254
		mu 0 4 251 250 254 255
		f 4 -254 255 256 257
		mu 0 4 255 254 256 257
		f 4 -251 258 259 260
		mu 0 4 253 252 258 259
		f 4 -257 261 262 263
		mu 0 4 257 256 260 261
		f 4 -263 264 265 266
		mu 0 4 261 260 262 263
		f 4 -266 267 268 269
		mu 0 4 263 262 264 265
		f 4 -269 270 -260 271
		mu 0 4 265 264 259 258
		f 8 -271 -268 -265 -262 -256 -253 -252 -261
		mu 0 8 259 264 262 260 256 254 250 253
		f 4 272 -255 273 -226
		mu 0 4 223 251 255 224
		f 4 274 -264 275 -238
		mu 0 4 235 266 267 236
		f 4 276 -270 277 -244
		mu 0 4 243 268 269 244
		f 4 278 -259 279 -237
		mu 0 4 233 270 271 230
		f 4 -250 -273 -231 -280
		mu 0 4 252 251 223 227
		f 4 -258 -275 -232 -274
		mu 0 4 255 266 235 224
		f 4 -267 -277 -241 -276
		mu 0 4 267 268 243 236
		f 4 -272 -279 -247 -278
		mu 0 4 269 270 233 244
		f 4 280 285 -282 -285
		mu 0 4 272 273 274 275
		f 4 281 287 -283 -287
		mu 0 4 275 274 276 277
		f 4 282 289 -284 -289
		mu 0 4 277 276 278 279
		f 4 283 291 -281 -291
		mu 0 4 279 278 280 281
		f 4 -292 -290 -288 -286
		mu 0 4 273 282 283 274
		f 4 290 284 286 288
		mu 0 4 284 272 275 285
		f 4 292 297 -294 -297
		mu 0 4 286 287 288 289
		f 4 293 299 -295 -299
		mu 0 4 289 288 290 291
		f 4 294 301 -296 -301
		mu 0 4 291 290 292 293
		f 4 295 303 -293 -303
		mu 0 4 293 292 294 295
		f 4 -304 -302 -300 -298
		mu 0 4 287 296 297 288
		f 4 302 296 298 300
		mu 0 4 298 286 289 299
		f 4 304 309 -306 -309
		mu 0 4 300 301 302 303
		f 4 305 311 -307 -311
		mu 0 4 303 302 304 305
		f 4 306 313 -308 -313
		mu 0 4 305 304 306 307
		f 4 307 315 -305 -315
		mu 0 4 307 306 308 309
		f 4 -316 -314 -312 -310
		mu 0 4 301 310 311 302
		f 4 314 308 310 312
		mu 0 4 312 300 303 313
		f 4 316 321 -318 -321
		mu 0 4 314 315 316 317
		f 4 317 323 -319 -323
		mu 0 4 317 316 318 319
		f 4 318 325 -320 -325
		mu 0 4 319 318 320 321
		f 4 319 327 -317 -327
		mu 0 4 321 320 322 323
		f 4 -328 -326 -324 -322
		mu 0 4 315 324 325 316
		f 4 326 320 322 324
		mu 0 4 326 314 317 327;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Torso";
	rename -uid "2209FA7F-40A5-2C7F-0650-14A3774CB749";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 21 1.0105056762695313 ;
	setAttr ".sp" -type "double3" 0 21 1.0105056762695313 ;
createNode mesh -n "Basic_TorsoShape" -p "|Basic_Torso";
	rename -uid "4380B87F-463B-D4B4-B353-34B8A6D7E718";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr -av ".iog[0].og[10].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 104 ".pt[314:417]" -type "float3"  7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 0 0 7.1525574e-07 
		0 0 7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0 -7.1525574e-07 
		0 0 -7.1525574e-07 0 0 -7.1525574e-07 0 0;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Basic_TorsoShapeOrig" -p "|Basic_Torso";
	rename -uid "DEE34BDA-423B-1551-DAC9-84AF0C826B2D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 190 ".uvst[0].uvsp[0:189]" -type "float2" 0.375 0.75 0.625
		 0.75 0.375 1 0.625 1 0.37500003 0.31083804 0.62499994 0.31083804 0.625 0.47036067
		 0.60124928 0.5 0.39875072 0.5 0.375 0.47036073 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5
		 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1
		 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 202 ".uvst[1].uvsp[0:201]" -type "float2" 0.6217227 0.49701881
		 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.375 0.1 0.39875072 0.25 0.375 0.25 0.38120484
		 0.48778203 0.35888484 0.28088546 0.60124928 0.25 0.39875072 0.1 0.60124928 0.1 0.39875072
		 0 0.60124928 0 0.375 0.875 0.625 0.875 0.37004483 0.38433373 0.37996387 0.54022563
		 0.62237817 0.54761505 0.37589696 0.71209508 0.62452626 0.71343029 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr -s 202 ".uvst[2].uvsp[0:201]" -type "float2" 0.375 0 0.63006991
		 0.25000107 0.625 0 0.625 0.75 0.375 1 0.625 1 0.375 0.75 0.375 0.25 0.5953607 0.5
		 0.625 0.75 0.625 0.65000004 0.375 0.5 0.5953607 0.65000004 0.375 0.64999998 0.43583804
		 0.74999982 0.59536076 0.74999994 0.375 0.875 0.625 0.875 0.375 0.2 0.62905592 0.20000087
		 0.37467626 0.036138751 0.6257329 0.036138956 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5
		 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1
		 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr -s 205 ".uvst[3].uvsp[0:204]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.75 0.625 1 0.375 1 0.375 0.75 0.62899977 0.26061082 0.375 0.5 0.625
		 0.75 0.5953607 0.75 0.625 0.65000004 0.59536076 0.5 0.625 0.5 0.5953607 0.65000004
		 0.375 0.64999998 0.43583804 0.74999976 0.64705247 0.47337312 0.625 0.875 0.375 0.875
		 0.63802612 0.36699197 0.62819982 0.20848866 0.375 0.2 0.62557817 0.03767265 0.37474459
		 0.035461202 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0
		 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1
		 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr -s 192 ".uvst[4].uvsp[0:191]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0
		 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1
		 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr -s 190 ".uvst[5].uvsp[0:189]" -type "float2" 0.37500313 1.3590889e-06
		 0.62499768 1.7585622e-06 0.625 0.25 0.375 0.25 0.55288422 0.49038455 0.44711643 0.49038455
		 0.55288422 0.7596153 0.44711661 0.75961542 0.625 1 0.375 1 0 0 1 0 0.5 1 0.5 1 0
		 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr -s 195 ".uvst[6].uvsp[0:194]" -type "float2" 0.37500006 1.4187806e-06
		 0.62499994 1.7139804e-06 0.625 0.25 0.375 0.25 0.625 0.5 0.39279035 0.49073815 0.625
		 0.75 0.3935003 0.75743693 0.6226567 0.99932677 0.375 1 0 0 1 0 0.5 1 0.5 1 0 0 1
		 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25
		 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.49882835
		 0.99966335 0.50925016 0.7537185 0.50889516 0.49536908 0.5 0.25 0.5 1.5663804e-06;
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr -s 195 ".uvst[7].uvsp[0:194]" -type "float2" 0.37500006 1.3887484e-06
		 0.62499994 1.7361281e-06 0.625 0.25 0.375 0.25 0.60720968 0.49073815 0.375 0.5 0.60649967
		 0.75743681 0.375 0.75 0.625 1 0.37734324 0.99932677 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1
		 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1
		 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.5
		 1.5624382e-06 0.5 0.25 0.49110484 0.49536908 0.49074984 0.75371838 0.50117159 0.99966335;
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr -s 198 ".uvst[8].uvsp[0:197]" -type "float2" 0.36012951 4.4266707e-07
		 0.65064508 4.9997476e-07 0.56850398 0.25000054 0.40680742 0.25000066 0.56942832 0.49999955
		 0.40741879 0.49999931 0.63101113 0.75000042 0.37199745 0.75000048 0.62725317 0.99999857
		 0.37331015 0.99999827 0.65064508 4.9997476e-07 0.56850398 0.25000054 0.40680742 0.25000066
		 0.36012951 4.4266707e-07 0.37199745 0.75000048 0.63101113 0.75000042 0.62725317 0.99999857
		 0.37331015 0.99999827 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5
		 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0
		 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5
		 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr -s 217 ".uvst[9].uvsp[0:216]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.62499374 0.49999389 0.37500557 0.49999169 0.62499297 0.75000536
		 0.37500596 0.75000721 0.625 1 0.375 1 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1
		 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1
		 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.375 0.125 0.625 0.125 0.62499332 0.62499964
		 0.37500578 0.62499946 0.5 0 0.5 1 0.49999946 0.75000632 0.49999955 0.62499952 0.49999964
		 0.49999279 0.5 0.25 0.5 0.125 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5
		 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.5 0.125 0.625 0.125 0.49999955
		 0.62499952 0.62499332 0.62499964 0.49999955 0.62499952 0.37500578 0.62499946 0.5
		 0.125 0.375 0.125 0.375 0.125 0.5 0.125 0.49999955 0.62499952 0.5 0.125 0.625 0.125
		 0.62499332 0.62499964 0.49999955 0.62499952 0.37500578 0.62499946;
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr -s 192 ".uvst[10].uvsp[0:191]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1
		 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1
		 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr -s 205 ".uvst[11].uvsp[0:204]" -type "float2" 0.625 0 0.64846659
		 0.0043042195 0.72517979 0.24964188 0.62606341 0.34817922 0.36960733 0.36637074 0.39229006
		 0.35648113 0.37326795 0.40686047 0.36242685 0.28458983 0.38207865 0.28329083 0.3803657
		 0.98767757 0.37383628 0.94214058 0.38569883 0.77725297 0.37463272 -1.5854432e-05
		 0.36301091 0.27097076 0.38424778 -1.5216793e-05 0.625 0.25 0.37283492 0.27034244
		 0.39533868 0.78538406 0.63669759 0.98896617 0.625 1 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1
		 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1
		 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.50462389 -7.6083966e-06 0.50268286
		 0.99383879 0.51601815 0.88717508 0.5 0 0.5 1 0.5 1 0.5 0 0.50917673 0.35233018 0.50353932
		 0.26664543;
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr -s 200 ".uvst[12].uvsp[0:199]" -type "float2" 0.375 0 0.60848302
		 1.3643329e-05 0.62962103 0.24999531 0.37136903 0.24999572 0.63103151 0.50000489 0.37088689
		 0.5000037 0.61736715 0.7499947 0.375 0.75 0.62660497 0.99999541 0.375 1 0.375 0 0.60848302
		 1.3643329e-05 0.62962103 0.24999531 0.37136903 0.24999572 0.63103151 0.50000489 0.37088689
		 0.5000037 0.61736715 0.7499947 0.375 0.75 0.62660497 0.99999541 0.375 1 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr -s 204 ".uvst[13].uvsp[0:203]" -type "float2" 0.375 0 0.6193431
		 -0.0060015284 0.625 0.25 0.37603259 0.38933682 0.625 0.5 0.37729686 0.34872243 0.61946499
		 0.7426793 0.375 0.75 0.87499684 -0.026260916 0.875 0.25 0.125 0 0.12548056 0.38161317
		 0.375 0 0.6193431 -0.0060015284 0.625 0.25 0.37603259 0.38933682 0.37729686 0.34872243
		 0.625 0.5 0.61946499 0.7426793 0.375 0.75 0.87499684 -0.026260916 0.875 0.25 0.125
		 0 0.12548056 0.38161317 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0
		 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1
		 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5
		 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr -s 200 ".uvst[14].uvsp[0:199]" -type "float2" 0.375 0 0.63427842
		 -0.00022588279 0.6201672 0.25008345 0.37909874 0.24983679 0.61878538 0.49991286 0.37866652
		 0.50018489 0.62929565 0.75010157 0.375 0.75 0.62499952 0.9999969 0.375 1 0.375 0
		 0.63427842 -0.00022588279 0.6201672 0.25008345 0.37909874 0.24983679 0.61878538 0.49991286
		 0.37866652 0.50018489 0.62929565 0.75010157 0.375 0.75 0.62499952 0.9999969 0.375
		 1 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25
		 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr -s 204 ".uvst[15].uvsp[0:203]" -type "float2" 0.37499043 -0.0081199696
		 0.62484956 0.0034516717 0.62577444 0.26952696 0.37500215 0.26897159 0.62576842 0.48323262
		 0.37500995 0.47689846 0.62480825 0.73139822 0.37497076 0.76523322 0.87481654 -0.023973627
		 0.87523842 0.27280691 0.12500358 -0.00066387275 0.12500387 0.26631272 0.37499043
		 -0.0081199696 0.62484956 0.0034516717 0.62577444 0.26952696 0.37500215 0.26897159
		 0.37500995 0.47689846 0.62576842 0.48323262 0.62480825 0.73139822 0.37497076 0.76523322
		 0.87481654 -0.023973627 0.87523842 0.27280691 0.12500358 -0.00066387275 0.12500387
		 0.26631272 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0
		 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1
		 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr -s 204 ".uvst[16].uvsp[0:203]" -type "float2" 0.37500504 4.8307976e-07
		 0.62499589 -0.062499121 0.6249947 0.31249923 0.37500593 0.24999952 0.62498635 0.43750143
		 0.37501699 0.50000072 0.62499225 0.81249857 0.37501249 0.74999923 0.87499684 -0.06249886
		 0.87499553 0.31249899 0.12500305 7.9726237e-07 0.1250027 0.24999928 0.37500504 4.8307976e-07
		 0.62499589 -0.062499121 0.6249947 0.31249923 0.37500593 0.24999952 0.37501699 0.50000072
		 0.62498635 0.43750143 0.62499225 0.81249857 0.37501249 0.74999923 0.87499684 -0.06249886
		 0.87499553 0.31249899 0.12500305 7.9726237e-07 0.1250027 0.24999928 0 0 1 0 0.5 1
		 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr -s 188 ".uvst[17].uvsp[0:187]" -type "float2" 0.625 0.25 0.375
		 0.25 0.625 0.5 0.375 0.5 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0 0 1 0 0.5 1
		 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr -s 188 ".uvst[18].uvsp[0:187]" -type "float2" 0.625 0.25 0.375
		 0.25 0.625 0.5 0.375 0.5 0.625 0.25 0.375 0.25 0.625 0.625 0.375 0.625 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr -s 188 ".uvst[19].uvsp[0:187]" -type "float2" 0.625 0.25 0.375
		 0.25 0.625 0.5 0.375 0.5 0.375 0.125 0.625 0.125 0.625 0.5 0.375 0.625 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr -s 188 ".uvst[20].uvsp[0:187]" -type "float2" 0.625 0.25 0.375
		 0.25 0.625 0.5 0.375 0.5 0.375 0.125 0.625 0.125 0.625 0.625 0.375 0.5 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1
		 0.5 1 0.5 0;
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr -s 198 ".uvst[21].uvsp[0:197]" -type "float2" 0.72517967 0.24964188
		 0.64846659 0.004304219 0.39695302 0.33819023 0.62643218 0.34612468 0.39238954 0.25400087
		 0.625 0.25 0.38463607 1.6688089e-06 0.38300347 0.24142848 0.625 0 0.3957372 0.78521168
		 0.38430235 0.99912709 0.625 1 0.62347412 0.94520628 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1
		 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75 1 0.25 1
		 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.50960565
		 0.86520898 0.50481802 8.3440443e-07 0.50465119 0.99956357 0.50869477 0.25200045 0.51169258
		 0.34215745;
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr -s 180 ".uvst[22].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr -s 180 ".uvst[23].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr -s 180 ".uvst[24].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr -s 180 ".uvst[25].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr -s 180 ".uvst[26].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr -s 180 ".uvst[27].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr -s 180 ".uvst[28].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr -s 180 ".uvst[29].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr -s 180 ".uvst[30].uvsp[0:179]" -type "float2" 0 0 1 0 0.5 1 0.5
		 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5 1 0.75
		 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.5 0 0.5 1 0.5 1
		 0.5 0;
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr -s 197 ".uvst[31].uvsp[0:196]" -type "float2" 0.375 0 0.375 0.25
		 0.62568665 0.24361479 0.62391174 -0.023076372 0.375 0.5 0.60508215 0.47307804 0.375
		 0.75 0.60330725 0.80107921 0.375 1 0.62391186 1.03181994 0.125 0 0.125 0.25 0 0 1
		 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5
		 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.49945593
		 1.01590991 0.49945587 -0.011538186 0.50034332 0.2468074 0.49004108 0.48653901 0.48915362
		 0.77553964 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr -s 197 ".uvst[32].uvsp[0:196]" -type "float2" 0.375 0 0.62391174
		 -0.023076372 0.62568665 0.24361479 0.375 0.25 0.60508215 0.47307804 0.375 0.5 0.60330725
		 0.80107921 0.375 0.75 0.62391186 1.03181994 0.375 1 0.125 0 0.125 0.25 0 0 1 0 0.5
		 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.5 1 0.5
		 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75 1 0.25 1 0.49945587
		 -0.011538186 0.49945593 1.01590991 0.48915362 0.77553964 0.49004108 0.48653901 0.50034332
		 0.2468074 0.5 0 0.5 1 0.5 1 0.5 0;
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr -s 206 ".uvst[33].uvsp[0:205]" -type "float2" 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.38030076 0.48228449 0.625 0.75 0.38030076 0.76775652
		 0.625 1 0.375 1 0.875 0 0.875 0.25 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0
		 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0
		 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.375 0.125 0.625 0.125 0.75 0.375 0.38030076
		 0.6250205 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75
		 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.625 0.125 0.375 0.125 0.75 0.375 0.625 0.125 0.38030076
		 0.6250205 0.75 0.375 0.375 0.125 0.625 0.125 0.75 0.375 0.38030076 0.6250205;
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr -s 206 ".uvst[34].uvsp[0:205]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.38030076 0.48228449 0.625 0.5 0.38030076 0.76775652 0.625 0.75
		 0.375 1 0.625 1 0.875 0.25 0.875 0 0 0 1 0 0.5 1 0.5 1 0 0 1 0 1 1 0.5 1 0 0 1 0
		 0 1 0 0 1 0 0.5 1 0 1 0 0 1 0 1 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 0.5 1 0
		 0 1 0 0.5 1 0 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.375 0.125 0.38030076 0.6250205 0.75 0.375
		 0.625 0.125 0.5 1 0.5 1 0.75 1 0.25 1 0.5 1 1 1 0 1 0.5 1 0 1 0.5 1 0.5 1 1 1 0.75
		 1 0.25 1 0.5 0 0.5 1 0.5 1 0.5 0 0.75 0.375 0.38030076 0.6250205 0.625 0.125 0.75
		 0.375 0.375 0.125 0.625 0.125 0.625 0.125 0.375 0.125 0.38030076 0.6250205 0.75 0.375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 314 ".vt";
	setAttr ".vt[0:165]"  -10 14.1697731 6.56282711 -9.99999905 18.1697731 2.12642479
		 9.99999905 18.16977692 2.12642431 -10 14.16976547 -10 10 14.16975975 -10 -9 33.1697731 -13
		 9 33.1697731 -13 -9 18.1697731 -13 9 18.1697731 -13 -13 18.1697731 7.70090914 -13 18.1697731 -10
		 -12.99999428 33.1697731 7.99998569 -13 33.1697731 -10 -10 33.1697731 6.026508808
		 13 18.1697731 7.70090914 13 18.1697731 -10 12.99999428 33.1697731 7.99998569 13 33.1697731 -10
		 10 33.1697731 6.026508808 11.10254955 17.22125626 -2.5 12.99986076 16.58863449 -2.5
		 12.051483154 20.067222595 -2.5 13.94879436 19.43460083 -2.5 12.051483154 20.067222595 -5.5
		 13.94879436 19.43460083 -5.5 11.10254955 17.22125626 -5.5 12.99986076 16.58863449 -5.5
		 -2.066627502 18.1697731 5.48226643 2.066627502 18.16977501 5.48226643 -1 33.1697731 13
		 1 33.1697731 13 -1.85810363 33.1697731 10.11538601 1.85810351 33.1697731 10.11538601
		 -1.85810363 23.64889145 10.11538601 1.85810351 23.64889145 10.11538601 10 23.64889145 6.026508808
		 -10 23.64889145 6.026508808 -3.5 9.53133011 11.91782761 3.5 9.53133011 11.91782761
		 -1.30000257 14.46977234 12.69999981 1.30000257 14.46977234 12.69999981 -1.30000257 14.93907928 9.73693466
		 1.30000257 14.93907928 9.73693466 -3.5 10.00063705444 8.95476151 3.5 10.00063705444 8.95476151
		 -5.19458866 14.30636215 9.87556839 5.19458866 14.30636215 9.87556934 -5.53147793 20.6697731 10.52668285
		 5.53147793 20.6697731 10.52668285 -5.45742893 20.6697731 9.53720474 5.45742893 20.6697731 9.53720474
		 -5.12768555 14.30636215 8.88609219 5.12768555 14.30636215 8.88609219 -11.10254955 17.22125626 -2.5
		 -12.99986076 16.58863449 -2.5 -12.051483154 20.067222595 -2.5 -13.94879436 19.43460083 -2.5
		 -12.051483154 20.067222595 -5.5 -13.94879436 19.43460083 -5.5 -11.10254955 17.22125626 -5.5
		 -12.99986076 16.58863449 -5.5 12.99922371 20.33810043 8.19139481 13.30768394 30.11977196 8.73843193
		 12.99999237 30.1697731 7.99997997 12.99999905 20.33810043 7.99999666 4.61852026 27.44497681 14.44290066
		 10.15697575 27.44497681 12.13519573 3.77606726 28.42276001 15.021012306 11.1606741 28.42276001 11.94407177
		 3.023921967 28.84199524 12.94467926 10.40852928 28.84199524 9.86773872 3.86637545 27.86421204 12.36656857
		 9.40483093 27.86421204 10.05886364 3.53419828 25.4894104 14.44053078 4.49436903 25.974617 14.15314579
		 2.78205299 25.90864563 12.36419773 3.74222422 26.39385414 12.076813698 6.9846282 22.55605125 12.32157993
		 7.36878347 23.53688812 12.38930988 6.61663771 23.95612717 10.31297684 6.23248291 22.97528839 10.24524689
		 10.67693615 22.55606079 10.78310871 9.83448315 23.53384209 11.36122131 9.92479134 22.97529602 8.70677567
		 9.082338333 23.95307922 9.28488731 6.75969124 23.81008148 12.56088161 9.80554485 23.81008148 11.29176617
		 5.19799519 25.15093613 11.33277893 9.2347908 25.15093613 9.65077019 5.7072773 24.78786659 13.22647572
		 9.88616753 24.78786469 11.48526096 3.77695751 26.12871933 12.15196896 9.31541443 26.12871933 9.84426308
		 4.42833519 25.76564789 13.98645878 9.9667902 25.76564789 11.67875385 3.85758018 27.10650253 12.34546185
		 9.39603615 27.10650253 10.037757874 4.50895786 26.74343109 14.17995358 10.047413826 26.74343109 11.87224865
		 3.93820333 28.084285736 12.53895664 9.47665882 28.084285736 10.23125076 -12.99922371 20.33810043 8.19139481
		 -13.30768394 30.11977386 8.73843193 -12.99999237 30.1697731 7.99997997 -12.99999905 20.33810043 7.99999666
		 15.15206146 15.67844391 7.33844185 13.22897339 20.23699951 8.060445786 12.16387939 20.23187637 5.25588751
		 14.086967468 15.67331696 4.53388405 -15.15206146 15.67844391 7.33844185 -13.22897339 20.23699951 8.060445786
		 -12.16387939 20.23187637 5.25588751 -14.086967468 15.67331696 4.53388405 11.6134901 16.013385773 7.28728104
		 12.85375309 20.6697731 7.91671467 12.58114052 20.6697731 6.95545292 11.36718273 16.013385773 6.32601929
		 -11.6134901 16.013385773 7.28728104 -12.85375309 20.6697731 7.91671467 -12.58114052 20.6697731 6.95545292
		 -11.36718273 16.013385773 6.32601929 10 14.16976357 6.56282711 -1 30.1697731 13 1 30.16977501 13
		 -4.61852026 27.44497681 14.44290066 -10.15697575 27.44497681 12.13519573 -11.1606741 28.42276001 11.94407177
		 -3.77606726 28.42276001 15.021012306 -10.40852928 28.84199524 9.86773872 -3.023921967 28.84199524 12.94467926
		 -9.40483093 27.86421204 10.05886364 -3.86637545 27.86421204 12.36656857 -3.53419828 25.4894104 14.44053078
		 -4.49436903 25.974617 14.15314579 -3.74222422 26.39385414 12.076813698 -2.78205299 25.90864563 12.36419773
		 -6.9846282 22.55605125 12.32157993 -7.36878347 23.53688812 12.38930988 -6.61663771 23.95612717 10.31297684
		 -6.23248291 22.97528839 10.24524689 -10.67693615 22.55606079 10.78310871 -9.83448315 23.53384209 11.36122131
		 -9.082338333 23.95307922 9.28488731 -9.92479134 22.97529602 8.70677567 -9.80554485 23.81008148 11.29176617
		 -6.75969124 23.81008148 12.56088161 -9.2347908 25.15093613 9.65077019 -5.19799519 25.15093613 11.33277893
		 -9.88616753 24.78786469 11.48526096 -5.7072773 24.78786659 13.22647572 -9.31412506 26.12500954 9.8411684
		 -3.77566814 26.12500954 12.14887524 -4.42704535 25.76194 13.98336506 -9.96550179 25.76194 11.67566013
		 -9.39603615 27.10650253 10.037757874 -3.85629129 27.10279083 12.34236813 -4.5076685 26.7397213 14.17685986
		 -10.046124458 26.7397213 11.86915493 -9.47536945 28.080574036 10.22815704 -3.93820333 28.084285736 12.53895664
		 -3.5 8.83022499 12.061686516 3.5 8.83022499 12.061686516 1.30000257 13.48566437 14.32386112
		 -1.30000257 13.48566437 14.32386112 -3.5 9.6587677 8.9006176 3.5 9.6587677 8.9006176;
	setAttr ".vt[166:313]" -0.99641889 30.11977196 13.71850109 1.0022703409 30.11977196 13.71893406
		 -10 18.1697731 -8.099942207 -8.099942207 18.1697731 -10 -10 23.64889145 -8.099942207
		 -8.099942207 23.64889145 -10 -8.099942207 33.1697731 -10 -10 33.1697731 -8.099942207
		 8.099942207 18.1697731 -10 10 18.1697731 -8.099942207 10 23.64889145 -8.099942207
		 8.099942207 23.64889145 -10 8.099942207 33.1697731 -10 10 33.1697731 -8.099942207
		 -11.93161964 15.85074997 7.31981564 -11.93161964 15.85074615 -10.079131126 -9.42501545 15.85074615 -11.86593819
		 9.42501545 15.85074234 -11.86593819 11.93161964 15.85074234 -10.079131126 11.93161964 15.85074615 7.31981564
		 11.41285324 33.14222336 -11.86593819 11.41285324 18.27238846 -11.86593819 11.03839016 16.52436256 -11.30424309
		 -11.41285324 33.14222336 -11.86593819 -11.41285324 18.27238846 -11.86593819 -11.03839016 16.52436447 -11.30424309
		 -13 30.1697731 -10 -11.41285324 30.16825485 -11.86593914 -9 30.1697731 -13.000000953674
		 9 30.1697731 -13.000000953674 11.41285324 30.16825485 -11.86593914 13 30.1697731 -10
		 -13 20.33810043 -10 -11.41285324 20.4219017 -11.86593819 -9 20.33810043 -13 9 20.33810043 -13
		 11.41285324 20.4219017 -11.86593819 13 20.33810043 -10 0.49890172 20.33810043 13.36925125
		 -0.49956685 20.33810043 13.36923885 0.50275213 29.62142372 13.88401508 -0.49571684 29.62065125 13.88405514
		 0.49925989 20.33810043 11.5923624 -0.49920872 20.33810043 11.59237766 -5.35646105 11.12329292 9.56723404
		 5.35646105 11.12328529 9.56723404 -10.185462 18.1697731 7.70091105 -10.18546104 20.33810043 7.92442226
		 -6.39112616 12.16367531 10.11073971 10.185462 18.1697731 7.70091105 10.18546104 20.33810043 7.92442226
		 6.39112616 12.16367149 10.11073971 0.39116865 20.33810043 10.027101517 -0.39112854 20.33810043 10.027104378
		 6.71670723 23.68639755 12.45772076 9.76256084 23.68639755 11.18860531 5.15501118 25.027252197 11.22961807
		 9.19180679 25.027252197 9.54760933 5.66429329 24.66418266 13.12331676 9.84318352 24.66418076 11.38210106
		 3.73397398 26.0050354004 12.048810005 9.27243042 26.0050354004 9.74110317 4.38535118 25.64196396 13.88329792
		 9.92380619 25.64196396 11.57559299 3.81459665 26.9828186 12.24230099 9.35305214 26.9828186 9.93459702
		 4.46597385 26.61974716 14.076792717 10.0044298172 26.61974716 11.76908779 3.8952198 27.96060181 12.43579578
		 9.43367481 27.96060181 10.1280899 -6.71670723 23.68639755 12.45772076 -9.76256084 23.68639755 11.18860531
		 -9.19180679 25.027252197 9.54760933 -5.15501118 25.027252197 11.22961807 -5.6644125 24.66390419 13.12360096
		 -9.84330368 24.66390228 11.38238621 -9.27126122 26.0010471344 9.73829365 -3.73280358 26.0010471344 12.046000481
		 -4.3840642 25.63832474 13.88012028 -9.92246151 25.63846588 11.57227325 -9.35305977 26.98302841 9.93434429
		 -3.81337404 26.97917747 12.23909664 -4.46456385 26.61624908 14.07349968 -10.0030794144 26.61610794 11.76593781
		 -9.43226051 27.95696068 10.12496662 -3.89503431 27.96081161 12.43562317 -5.36086655 17.28330231 10.52668285
		 5.36086655 17.28330231 10.52668285 12.22564507 18.13681412 7.91671467 11.96635342 18.13681412 6.95545292
		 5.29043674 17.28330231 9.53720474 -5.29043674 17.28330231 9.53720474 -11.96635342 18.13681412 6.95545292
		 -12.22564507 18.13681412 7.91671467 0 13.6697731 10.96953487 0 13.6697731 9.96953678
		 0 16.96500778 10.62873363 0 20.6697731 10.62873363 0 20.6697731 11.62873363 0 16.96500778 11.62873363
		 -9.47807503 15.85074806 7.31981564 9.47807503 15.85074615 7.31981564 -6.4882741 14.0072116852 8.71527767
		 6.4882741 14.0072097778 8.71527767 -9.94521809 18.28445816 6.62630224 -9.94521713 19.14027405 7.10106039
		 9.94521809 18.28445816 6.62630224 9.94521713 19.14027405 7.10106039 -9.2545166 15.87042999 6.86615467
		 9.2545166 15.87042999 6.86615467 -6.33523607 14.6562767 6.91899204 6.33523607 14.65628052 6.91899014
		 0 17.1697731 11.62873363 5.36086655 17.48806763 10.52668285 -5.36086655 17.48806763 10.52668285
		 -12.22564507 18.34157944 7.91671467 -11.96635342 18.34157944 6.95545292 -5.29043674 17.48806763 9.53720474
		 0 17.1697731 10.62873363 5.29043674 17.48806763 9.53720474 11.96635342 18.34157944 6.95545292
		 12.22564507 18.34157944 7.91671467 9.32603073 11.026454926 10.34613228 7.26448822 15.77495384 11.098219872
		 6.73194122 16.0070457458 8.21440887 8.79348373 11.25854492 7.46232033 -9.32603073 11.026454926 10.34613228
		 -8.79348373 11.25854492 7.46232033 -6.73194122 16.0070457458 8.21440887 -7.26448822 15.77495384 11.098219872
		 6.74906254 20.33810043 11.76489067 6.7496295 20.33810043 9.79617977 5.28831482 20.33810043 8.97576141
		 -5.28829479 20.33810043 8.97576332 -6.74960375 20.33810043 9.7961874 -6.74939537 20.33810043 11.76488495
		 -7.15205145 30.11977196 12.21303368 -6.99999619 30.1697731 11.48455811 -6.99999714 33.1697731 11.48456001
		 -6.26828623 33.1697731 9.21302605 -6.27162552 23.64889145 9.21017456 -5.94319296 18.1697731 3.84246659
		 5.94319344 18.16977692 3.84246635 6.27162361 23.64889145 9.21017647 6.2682848 33.1697731 9.213027
		 6.99999714 33.1697731 11.48456001 6.99999619 30.1697731 11.48455811 7.15497732 30.11977196 12.21325111;
	setAttr -s 569 ".ed";
	setAttr ".ed[0:165]"  0 121 0 1 2 0 3 4 0 1 168 0 2 175 0 3 0 0 4 121 0 5 6 0
		 7 8 0 5 194 0 6 195 0 7 182 0 8 183 0 9 10 0 11 12 0 13 173 0 9 104 0 10 198 0 11 13 0
		 0 180 0 3 181 0 14 15 0 16 17 0 18 179 0 14 64 0 15 203 0 16 18 0 18 35 0 121 185 0
		 4 184 0 19 20 0 21 22 0 23 24 0 25 26 0 19 21 0 20 22 0 21 23 0 22 24 0 23 25 0 24 26 0
		 25 19 0 26 20 0 27 28 0 29 30 0 31 32 0 33 34 0 122 29 0 123 30 0 29 31 0 30 32 0
		 31 33 0 32 34 0 33 27 0 34 28 0 28 308 0 30 311 0 32 310 0 34 309 0 63 16 0 35 2 0
		 1 307 0 11 304 0 13 305 0 36 306 0 103 11 0 36 1 0 39 40 1 41 42 0 43 44 1 37 39 0
		 38 40 0 39 41 0 40 42 0 41 43 0 42 44 0 43 37 0 44 38 0 45 260 0 47 264 0 49 263 0
		 51 261 0 45 252 0 46 253 0 47 49 0 48 50 0 49 283 0 50 285 0 51 45 0 52 46 0 53 54 0
		 55 56 0 57 58 0 59 60 0 53 55 0 54 56 0 55 57 0 56 58 0 57 59 0 58 60 0 59 53 0 60 54 0
		 61 62 0 62 63 0 63 64 0 64 61 0 65 66 0 67 68 0 69 70 0 71 72 0 65 67 0 66 68 0 67 69 0
		 68 70 0 69 71 0 70 72 0 71 65 0 72 66 0 73 74 0 75 76 0 73 67 0 74 65 0 69 75 0 71 76 0
		 75 73 0 76 74 0 73 77 0 74 78 0 76 79 0 75 80 0 77 78 0 78 79 0 79 80 0 80 77 0 81 82 0
		 83 84 0 81 77 0 82 78 0 80 83 0 79 84 0 83 81 0 84 82 0 82 66 0 81 68 0 72 84 0 70 83 0
		 85 86 0 87 88 0 85 87 0 86 88 0 89 90 0 91 92 0 89 91 0 90 92 0 93 94 0 95 96 0 93 95 0
		 94 96 0 97 98 0 99 100 0 97 99 0 98 100 0 101 102 0 102 103 0 103 104 0 104 101 0
		 105 288 0;
	setAttr ".ed[166:331]" 106 289 0 107 290 0 108 291 0 105 106 0 106 107 0 107 108 0
		 108 105 0 109 292 0 110 295 0 111 294 0 112 293 0 109 110 0 110 111 0 111 112 0 112 109 0
		 46 113 0 48 114 0 50 115 0 52 116 0 113 254 0 114 115 0 115 286 0 116 113 0 45 117 0
		 47 118 0 49 119 0 51 120 0 117 259 0 118 119 0 119 282 0 120 117 0 6 186 0 8 187 0
		 7 190 0 5 189 0 13 36 0 122 123 0 103 303 0 124 125 0 125 126 0 127 126 0 124 127 0
		 126 128 0 129 128 0 127 129 0 128 130 0 131 130 0 129 131 0 130 125 0 131 124 0 132 133 0
		 133 124 0 132 127 0 131 134 0 135 134 0 129 135 0 134 133 0 135 132 0 132 136 0 136 137 0
		 133 137 0 137 138 0 134 138 0 138 139 0 135 139 0 139 136 0 140 141 0 141 137 0 140 136 0
		 138 142 0 143 142 0 139 143 0 142 141 0 143 140 0 140 126 0 141 125 0 128 143 0 130 142 0
		 145 144 0 144 146 0 147 146 0 145 147 0 149 148 0 148 150 0 151 150 0 149 151 0 152 153 0
		 153 154 0 155 154 0 152 155 0 156 157 0 157 158 0 159 158 0 156 159 0 37 160 1 38 161 1
		 160 161 0 40 162 0 161 162 0 39 163 0 163 162 0 160 163 0 43 164 0 44 165 0 164 165 0
		 165 161 0 164 160 0 166 302 0 167 313 0 167 206 0 123 312 0 166 122 1 123 167 1 166 167 0
		 169 174 0 169 168 0 172 178 0 172 5 1 173 12 1 169 171 0 171 170 1 170 168 0 171 172 0
		 172 173 0 173 170 0 36 170 1 175 174 0 178 6 1 17 179 1 175 176 0 176 177 1 177 174 0
		 176 179 0 179 178 0 178 177 0 176 35 1 171 177 1 180 9 0 181 10 0 182 3 0 183 4 0
		 184 15 0 185 14 0 180 181 1 181 191 1 182 183 1 183 188 1 184 185 1 186 17 0 187 15 0
		 188 184 1 186 196 1 187 188 1 189 12 0 190 10 0 191 182 1 189 193 1 190 191 1 192 12 0
		 193 199 1 194 200 0 195 201 0 196 202 1 197 17 0 103 192 1 192 193 1;
	setAttr ".ed[332:497]" 193 194 1 194 195 1 195 196 1 196 197 1 197 63 1 198 192 0
		 199 190 1 200 7 0 201 8 0 202 187 1 203 197 0 104 198 1 198 199 1 199 200 1 200 201 1
		 201 202 1 202 203 1 203 64 1 207 166 0 207 206 0 208 297 1 209 300 1 204 205 0 205 209 1
		 209 208 1 208 204 1 204 206 0 207 205 0 204 296 0 101 301 0 0 210 0 121 211 0 210 211 0
		 9 212 1 104 213 0 212 213 0 180 214 0 210 214 0 14 215 1 64 216 0 215 216 0 185 217 0
		 211 217 0 214 268 1 217 269 1 208 218 1 218 298 1 209 219 1 219 299 1 219 218 1 213 216 0
		 214 217 0 85 220 0 86 221 0 220 221 0 87 222 0 88 223 0 222 223 0 220 222 0 221 223 0
		 89 224 0 90 225 0 224 225 0 91 226 0 92 227 0 226 227 0 224 226 0 225 227 0 93 228 0
		 94 229 0 228 229 0 95 230 0 96 231 0 230 231 0 228 230 0 229 231 0 97 232 0 98 233 0
		 232 233 0 99 234 0 100 235 0 234 235 0 232 234 0 233 235 0 145 236 0 144 237 0 236 237 0
		 146 238 0 237 238 0 147 239 0 239 238 0 236 239 0 149 240 0 148 241 0 240 241 0 150 242 0
		 241 242 0 151 243 0 243 242 0 240 243 0 152 244 0 153 245 0 244 245 0 154 246 0 245 246 0
		 155 247 0 247 246 0 244 247 0 156 248 0 157 249 0 248 249 0 158 250 0 249 250 0 159 251 0
		 251 250 0 248 251 0 280 47 0 279 48 0 287 114 0 255 116 0 256 52 0 257 51 0 258 120 0
		 281 118 0 252 265 0 253 254 0 254 255 0 255 256 0 256 262 0 257 258 0 258 259 0 259 252 0
		 260 46 0 261 52 0 262 257 0 263 50 0 264 48 0 265 253 0 260 261 0 261 262 0 284 263 0
		 263 264 0 264 278 0 265 260 0 266 212 0 267 215 0 180 266 0 267 185 0 268 266 0 269 267 0
		 268 269 0 212 270 1 213 271 0 270 271 0 215 272 1 270 272 1 216 273 0 272 273 0 271 273 0
		 266 274 0 274 270 0 267 275 0 274 275 0 275 272 0 268 276 1 276 274 0;
	setAttr ".ed[498:568]" 269 277 1 276 277 0 277 275 0 278 279 0 280 278 0 281 280 0
		 282 281 0 283 282 0 284 283 0 285 284 0 286 285 0 287 286 0 279 287 0 252 257 0 265 262 0
		 253 256 0 279 285 0 278 284 0 280 283 0 288 38 0 289 40 0 290 42 0 291 44 0 288 289 1
		 289 290 1 290 291 1 291 288 1 292 37 0 293 43 0 294 41 0 295 39 0 292 293 1 293 294 1
		 294 295 1 295 292 1 296 61 0 297 64 1 298 216 1 299 213 1 300 104 1 301 205 0 296 297 1
		 297 298 1 298 299 1 299 300 1 300 301 1 302 102 0 303 122 0 304 29 0 305 31 0 306 33 0
		 307 27 0 308 2 0 309 35 0 310 18 0 311 16 0 312 63 0 313 62 0 302 303 1 303 304 1
		 304 305 1 305 306 1 306 307 1 307 308 1 308 309 1 309 310 1 310 311 1 311 312 1 312 313 1
		 302 301 1 313 296 1;
	setAttr -s 281 -ch 1130 ".fc[0:280]" -type "polyFaces" 
		f 6 1 4 292 -281 281 -4
		mu 0 6 4 5 6 7 8 9
		f 4 2 6 -1 -6
		mu 0 4 0 1 3 2
		f 4 346 340 -9 -340
		mu 1 4 20 21 1 2
		f 4 311 306 -3 -306
		mu 1 4 15 16 3 4
		f 4 16 343 -18 -14
		mu 2 4 0 20 21 2
		f 4 18 15 284 -15
		mu 2 4 7 11 8 1
		f 4 19 309 -21 5
		mu 2 4 6 16 17 3
		f 4 21 25 349 -25
		mu 3 4 0 1 23 24
		f 4 22 294 -24 -27
		mu 3 4 2 7 12 8
		f 4 -7 29 313 -29
		mu 3 4 6 3 18 19
		f 4 30 35 -32 -35
		mu 4 4 0 1 2 3
		f 4 31 37 -33 -37
		mu 4 4 3 2 4 5
		f 4 32 39 -34 -39
		mu 4 4 5 4 6 7
		f 4 33 41 -31 -41
		mu 4 4 7 6 8 9
		f 4 -42 -40 -38 -36
		mu 4 4 1 10 11 2
		f 4 202 47 -44 -47
		mu 5 4 0 1 2 3
		f 4 43 49 -45 -49
		mu 5 4 3 2 4 5
		f 4 44 51 -46 -51
		mu 5 4 5 4 6 7
		f 4 45 53 -43 -53
		mu 5 4 7 6 8 9
		f 4 565 554 58 -554
		mu 6 4 193 194 1 2
		f 4 564 553 26 -553
		mu 6 4 192 193 2 4
		f 4 563 552 27 -552
		mu 6 4 191 192 4 6
		f 4 562 551 59 -551
		mu 6 4 190 191 6 8
		f 4 203 557 -62 -65
		mu 7 4 0 190 191 3
		f 4 61 558 -63 -19
		mu 7 4 3 191 192 5
		f 4 62 559 -64 -202
		mu 7 4 5 192 193 7
		f 4 63 560 -61 -66
		mu 7 4 7 193 194 9
		f 4 262 264 -267 -268
		mu 8 4 13 10 11 12
		f 4 66 72 -68 -72
		mu 8 4 3 2 4 5
		f 4 67 74 -69 -74
		mu 8 4 5 4 6 7
		f 4 270 271 -263 -273
		mu 8 4 14 15 16 17
		f 4 474 501 449 -469
		mu 9 4 181 182 173 2
		f 4 473 468 84 -468
		mu 9 4 180 181 2 4
		f 4 472 467 86 507
		mu 9 4 179 180 4 174
		f 4 470 465 88 -465
		mu 9 4 177 178 6 8
		f 4 93 90 -95 -90
		mu 10 4 0 1 2 3
		f 4 95 91 -97 -91
		mu 10 4 1 4 5 2
		f 4 97 92 -99 -92
		mu 10 4 4 6 7 5
		f 4 99 89 -101 -93
		mu 10 4 6 8 9 7
		f 4 94 96 98 100
		mu 10 4 3 2 10 11
		f 4 566 555 102 -555
		mu 11 4 203 204 15 3
		f 4 -105 -104 -103 -102
		mu 11 4 0 1 2 15
		f 4 105 110 -107 -110
		mu 12 4 0 1 2 3
		f 4 106 112 -108 -112
		mu 12 4 3 2 4 5
		f 4 107 114 -109 -114
		mu 12 4 5 4 6 7
		f 4 108 116 -106 -116
		mu 12 4 7 6 8 9
		f 4 117 120 109 -120
		mu 13 4 0 1 2 3
		f 4 113 122 -119 -122
		mu 13 4 5 4 6 7
		f 4 -125 -123 115 -121
		mu 13 4 1 8 9 2
		f 4 123 119 111 121
		mu 13 4 10 0 3 11
		f 4 125 129 -127 -118
		mu 14 4 0 1 2 3
		f 4 126 130 -128 124
		mu 14 4 3 2 4 5
		f 4 127 131 -129 118
		mu 14 4 5 4 6 7
		f 4 128 132 -126 -124
		mu 14 4 7 6 8 9
		f 4 133 136 -130 -136
		mu 15 4 0 1 2 3
		f 4 -132 138 -135 -138
		mu 15 4 5 4 6 7
		f 4 -141 -139 -131 -137
		mu 15 4 1 8 9 2
		f 4 139 135 -133 137
		mu 15 4 10 0 3 11
		f 4 -134 142 -111 -142
		mu 16 4 0 1 2 3
		f 4 -115 144 134 -144
		mu 16 4 5 4 6 7
		f 4 -140 -145 -113 -143
		mu 16 4 1 8 9 2
		f 4 140 141 -117 143
		mu 16 4 10 0 3 11
		f 4 145 148 -147 -148
		mu 17 4 1 0 2 3
		f 4 149 152 -151 -152
		mu 18 4 1 0 2 3
		f 4 153 156 -155 -156
		mu 19 4 1 0 2 3
		f 4 157 160 -159 -160
		mu 20 4 1 0 2 3
		f 4 556 -204 -163 -545
		mu 21 4 196 197 3 5
		f 4 161 162 163 164
		mu 21 4 8 5 0 1
		f 4 521 518 -71 -518
		mu 31 4 189 190 2 3
		f 4 522 519 -73 -519
		mu 31 4 190 191 5 2
		f 4 523 520 -75 -520
		mu 31 4 191 192 7 5
		f 4 524 517 -77 -521
		mu 31 4 192 188 9 7
		f 4 -172 -171 -170 -173
		mu 31 4 10 11 1 0
		f 4 532 525 69 -529
		mu 32 4 192 188 1 2
		f 4 531 528 71 -528
		mu 32 4 191 192 2 4
		f 4 530 527 73 -527
		mu 32 4 190 191 4 6
		f 4 529 526 75 -526
		mu 32 4 189 190 6 8
		f 4 180 177 178 179
		mu 32 4 10 0 3 11
		f 4 510 450 -183 -450
		mu 33 4 174 175 2 3
		f 4 182 186 -184 -85
		mu 33 4 3 2 4 5
		f 4 183 187 508 -87
		mu 33 4 5 4 176 177
		f 4 184 188 -182 -89
		mu 33 4 7 6 8 9
		f 4 509 -188 -187 -451
		mu 33 4 175 176 11 2
		f 4 503 448 190 -456
		mu 34 4 177 174 1 2
		f 4 83 191 -195 -191
		mu 34 4 1 4 5 2
		f 4 85 505 -196 -192
		mu 34 4 4 175 176 5
		f 4 87 189 -197 -193
		mu 34 4 6 8 9 7
		f 4 504 455 194 195
		mu 34 4 176 177 2 10
		f 4 -341 347 341 -199
		f 4 344 338 320 17
		f 4 -2 60 561 550
		f 4 207 206 -206 -205
		mu 12 4 10 13 12 11
		f 4 210 209 -209 -207
		mu 12 4 13 15 14 12
		f 4 213 212 -212 -210
		mu 12 4 15 17 16 14
		f 4 215 204 -215 -213
		mu 12 4 17 19 18 16
		f 4 218 -208 -218 -217
		mu 13 4 12 15 14 13
		f 4 221 220 -220 -214
		mu 13 4 16 19 18 17
		f 4 217 -216 219 222
		mu 13 4 13 14 21 20
		f 4 -222 -211 -219 -224
		mu 13 4 22 23 15 12
		f 4 216 226 -226 -225
		mu 14 4 10 13 12 11
		f 4 -223 228 -228 -227
		mu 14 4 13 15 14 12
		f 4 -221 230 -230 -229
		mu 14 4 15 17 16 14
		f 4 223 224 -232 -231
		mu 14 4 17 19 18 16
		f 4 234 225 -234 -233
		mu 15 4 12 15 14 13
		f 4 237 236 -236 229
		mu 15 4 16 19 18 17
		f 4 233 227 235 238
		mu 15 4 13 14 21 20
		f 4 -238 231 -235 -240
		mu 15 4 22 23 15 12
		f 4 241 205 -241 232
		mu 16 4 12 15 14 13
		f 4 243 -237 -243 211
		mu 16 4 16 19 18 17
		f 4 240 208 242 239
		mu 16 4 13 14 21 20
		f 4 -244 214 -242 -239
		mu 16 4 22 23 15 12
		f 4 247 246 -246 -245
		mu 17 4 5 7 6 4
		f 4 251 250 -250 -249
		mu 18 4 5 7 6 4
		f 4 255 254 -254 -253
		mu 19 4 4 7 6 5
		f 4 259 258 -258 -257
		mu 20 4 4 7 6 5
		f 4 70 263 -265 -262
		mu 8 4 1 2 11 10
		f 4 -67 265 266 -264
		mu 8 4 2 3 12 11
		f 4 -70 260 267 -266
		mu 8 4 3 0 13 12
		f 4 68 269 -271 -269
		mu 8 4 7 6 15 14
		f 4 76 261 -272 -270
		mu 8 4 6 8 16 15
		f 4 -76 268 272 -261
		mu 8 4 9 7 14 17
		f 4 279 -279 -203 -278
		mu 11 4 4 8 5 6
		f 4 -351 351 -276 -280
		mu 11 4 7 13 16 8
		f 4 -307 312 316 -30
		f 4 20 310 321 305
		f 4 -284 282 293 -8
		mu 1 4 8 6 10 0
		f 4 -282 285 286 287
		mu 2 4 15 9 10 12
		f 4 -287 288 289 290
		mu 1 4 5 11 6 7
		f 4 -291 -16 201 291
		mu 2 4 12 8 11 13
		f 4 -292 65 3 -288
		mu 2 4 12 13 14 15
		f 5 283 200 319 -285 -290
		mu 1 5 6 8 17 9 7
		f 4 -293 295 296 297
		mu 3 4 9 10 14 11
		f 4 -297 298 299 300
		mu 3 4 11 14 12 13
		f 4 301 -28 23 -299
		mu 3 4 14 15 8 12
		f 4 -60 -302 -296 -5
		mu 3 4 16 15 14 10
		f 4 -301 -283 -289 302
		mu 1 4 12 10 6 11
		f 4 -303 -286 280 -298
		mu 1 4 12 11 13 14
		f 5 -300 -295 -315 -198 -294
		mu 3 5 13 12 7 20 17
		f 4 -310 303 13 -305
		mu 2 4 17 16 4 5
		f 4 323 -311 304 -321
		f 4 8 12 -312 -12
		mu 1 4 2 1 16 15
		f 4 318 -313 -13 198
		f 4 -314 307 -22 -309
		mu 3 4 19 18 4 5
		f 4 -342 348 -26 -316
		f 4 -317 -319 315 -308
		f 4 345 339 199 -339
		f 4 -322 -324 -200 11
		f 4 -331 64 14 -325
		mu 2 4 19 18 7 1
		f 4 322 -332 324 -320
		f 4 9 -333 -323 -201
		f 4 7 10 -334 -10
		mu 1 4 8 0 19 18
		f 4 -335 -11 197 317
		f 4 -336 -318 314 -330
		f 4 -337 329 -23 -59
		mu 3 4 22 21 7 2
		f 4 -344 -164 330 -338
		mu 2 4 21 20 18 19
		f 4 331 325 -345 337
		f 4 332 326 -346 -326
		f 4 333 327 -347 -327
		mu 1 4 18 19 21 20
		f 4 -348 -328 334 328
		f 4 -349 -329 335 -343
		f 4 -350 342 336 103
		mu 3 4 24 23 21 22
		f 4 354 355 356 357
		mu 11 4 9 10 11 17
		f 4 -355 358 -352 359
		mu 11 4 12 14 16 13
		f 4 568 533 101 -556
		mu 11 4 204 196 0 15
		f 4 539 534 104 -534
		mu 11 4 197 198 18 19
		f 5 -360 350 273 567 538
		mu 21 5 6 7 4 196 194
		f 4 543 -362 -165 -538
		mu 21 4 193 195 11 12
		f 4 0 363 -365 -363
		mu 0 4 10 11 12 13
		mu 1 4 22 23 24 25
		mu 2 4 22 23 24 25
		mu 3 4 25 26 27 28
		mu 4 4 12 13 14 15
		mu 5 4 10 11 12 13
		mu 6 4 10 11 12 13
		mu 7 4 10 11 12 13
		mu 8 4 18 19 20 21
		mu 9 4 10 11 12 13
		mu 10 4 12 13 14 15
		mu 11 4 20 21 22 23
		mu 12 4 20 21 22 23
		mu 13 4 24 25 26 27
		mu 14 4 20 21 22 23
		mu 15 4 24 25 26 27
		mu 16 4 24 25 26 27
		mu 17 4 8 9 10 11
		mu 18 4 8 9 10 11
		mu 19 4 8 9 10 11
		mu 20 4 8 9 10 11
		mu 21 4 13 14 15 16
		mu 22 4 0 1 2 3
		mu 23 4 0 1 2 3
		mu 24 4 0 1 2 3
		mu 25 4 0 1 2 3
		mu 26 4 0 1 2 3
		mu 27 4 0 1 2 3
		mu 28 4 0 1 2 3
		mu 29 4 0 1 2 3
		mu 30 4 0 1 2 3
		mu 31 4 12 13 14 15
		mu 32 4 12 13 14 15
		mu 33 4 12 13 14 15
		mu 34 4 12 13 14 15
		f 4 -17 365 367 -367
		mu 0 4 14 15 16 17
		mu 1 4 26 27 28 29
		mu 2 4 26 27 28 29
		mu 3 4 29 30 31 32
		mu 4 4 16 17 18 19
		mu 5 4 14 15 16 17
		mu 6 4 14 15 16 17
		mu 7 4 14 15 16 17
		mu 8 4 22 23 24 25
		mu 9 4 14 15 16 17
		mu 10 4 16 17 18 19
		mu 11 4 24 25 26 27
		mu 12 4 24 25 26 27
		mu 13 4 28 29 30 31
		mu 14 4 24 25 26 27
		mu 15 4 28 29 30 31
		mu 16 4 28 29 30 31
		mu 17 4 12 13 14 15
		mu 18 4 12 13 14 15
		mu 19 4 12 13 14 15
		mu 20 4 12 13 14 15
		mu 21 4 17 18 19 20
		mu 22 4 4 5 6 7
		mu 23 4 4 5 6 7
		mu 24 4 4 5 6 7
		mu 25 4 4 5 6 7
		mu 26 4 4 5 6 7
		mu 27 4 4 5 6 7
		mu 28 4 4 5 6 7
		mu 29 4 4 5 6 7
		mu 30 4 4 5 6 7
		mu 31 4 16 17 18 19
		mu 32 4 16 17 18 19
		mu 33 4 16 17 18 19
		mu 34 4 16 17 18 19
		f 4 -20 362 369 -369
		mu 0 4 18 19 13 20
		mu 1 4 30 31 25 32
		mu 2 4 30 31 25 32
		mu 3 4 33 34 28 35
		mu 4 4 20 21 15 22
		mu 5 4 18 19 13 20
		mu 6 4 18 19 13 20
		mu 7 4 18 19 13 20
		mu 8 4 26 27 21 28
		mu 9 4 18 19 13 20
		mu 10 4 20 21 15 22
		mu 11 4 28 29 23 30
		mu 12 4 28 29 23 30
		mu 13 4 32 33 27 34
		mu 14 4 28 29 23 30
		mu 15 4 32 33 27 34
		mu 16 4 32 33 27 34
		mu 17 4 16 17 11 18
		mu 18 4 16 17 11 18
		mu 19 4 16 17 11 18
		mu 20 4 16 17 11 18
		mu 21 4 21 22 16 23
		mu 22 4 8 9 3 10
		mu 23 4 8 9 3 10
		mu 24 4 8 9 3 10
		mu 25 4 8 9 3 10
		mu 26 4 8 9 3 10
		mu 27 4 8 9 3 10
		mu 28 4 8 9 3 10
		mu 29 4 8 9 3 10
		mu 30 4 8 9 3 10
		mu 31 4 20 21 15 22
		mu 32 4 20 21 15 22
		mu 33 4 20 21 15 22
		mu 34 4 20 21 15 22
		f 4 24 371 -373 -371
		mu 0 4 21 22 23 24
		mu 1 4 33 34 35 36
		mu 2 4 33 34 35 36
		mu 3 4 36 37 38 39
		mu 4 4 23 24 25 26
		mu 5 4 21 22 23 24
		mu 6 4 21 22 23 24
		mu 7 4 21 22 23 24
		mu 8 4 29 30 31 32
		mu 9 4 21 22 23 24
		mu 10 4 23 24 25 26
		mu 11 4 31 32 33 34
		mu 12 4 31 32 33 34
		mu 13 4 35 36 37 38
		mu 14 4 31 32 33 34
		mu 15 4 35 36 37 38
		mu 16 4 35 36 37 38
		mu 17 4 19 20 21 22
		mu 18 4 19 20 21 22
		mu 19 4 19 20 21 22
		mu 20 4 19 20 21 22
		mu 21 4 24 25 26 27
		mu 22 4 11 12 13 14
		mu 23 4 11 12 13 14
		mu 24 4 11 12 13 14
		mu 25 4 11 12 13 14
		mu 26 4 11 12 13 14
		mu 27 4 11 12 13 14
		mu 28 4 11 12 13 14
		mu 29 4 11 12 13 14
		mu 30 4 11 12 13 14
		mu 31 4 23 24 25 26
		mu 32 4 23 24 25 26
		mu 33 4 23 24 25 26
		mu 34 4 23 24 25 26
		f 4 28 373 -375 -364
		mu 0 4 25 26 27 12
		mu 1 4 37 38 39 24
		mu 2 4 37 38 39 24
		mu 3 4 40 41 42 27
		mu 4 4 27 28 29 14
		mu 5 4 25 26 27 12
		mu 6 4 25 26 27 12
		mu 7 4 25 26 27 12
		mu 8 4 33 34 35 20
		mu 9 4 25 26 27 12
		mu 10 4 27 28 29 14
		mu 11 4 35 36 37 22
		mu 12 4 35 36 37 22
		mu 13 4 39 40 41 26
		mu 14 4 35 36 37 22
		mu 15 4 39 40 41 26
		mu 16 4 39 40 41 26
		mu 17 4 23 24 25 10
		mu 18 4 23 24 25 10
		mu 19 4 23 24 25 10
		mu 20 4 23 24 25 10
		mu 21 4 28 29 30 15
		mu 22 4 15 16 17 2
		mu 23 4 15 16 17 2
		mu 24 4 15 16 17 2
		mu 25 4 15 16 17 2
		mu 26 4 15 16 17 2
		mu 27 4 15 16 17 2
		mu 28 4 15 16 17 2
		mu 29 4 15 16 17 2
		mu 30 4 15 16 17 2
		mu 31 4 27 28 29 14
		mu 32 4 27 28 29 14
		mu 33 4 27 28 29 14
		mu 34 4 27 28 29 14
		f 4 -304 478 476 -366
		mu 0 4 28 29 172 31
		mu 1 4 40 41 184 43
		mu 2 4 40 41 184 43
		mu 3 4 43 44 187 46
		mu 4 4 30 31 174 33
		mu 5 4 28 29 172 31
		mu 6 4 28 29 172 31
		mu 7 4 28 29 172 31
		mu 8 4 36 37 180 39
		mu 9 4 28 29 183 31
		mu 10 4 30 31 174 33
		mu 11 4 38 39 182 41
		mu 12 4 38 39 182 41
		mu 13 4 42 43 186 45
		mu 14 4 38 39 182 41
		mu 15 4 42 43 186 45
		mu 16 4 42 43 186 45
		mu 17 4 26 27 170 29
		mu 18 4 26 27 170 29
		mu 19 4 26 27 170 29
		mu 20 4 26 27 170 29
		mu 21 4 31 32 175 34
		mu 22 4 18 19 162 21
		mu 23 4 18 19 162 21
		mu 24 4 18 19 162 21
		mu 25 4 18 19 162 21
		mu 26 4 18 19 162 21
		mu 27 4 18 19 162 21
		mu 28 4 18 19 162 21
		mu 29 4 18 19 162 21
		mu 30 4 18 19 162 21
		mu 31 4 30 31 174 33
		mu 32 4 30 31 174 33
		mu 33 4 30 31 178 33
		mu 34 4 30 31 178 33
		f 4 308 370 -478 479
		mu 0 4 32 33 34 173
		mu 1 4 44 45 46 185
		mu 2 4 44 45 46 185
		mu 3 4 47 48 49 188
		mu 4 4 34 35 36 175
		mu 5 4 32 33 34 173
		mu 6 4 32 33 34 173
		mu 7 4 32 33 34 173
		mu 8 4 40 41 42 181
		mu 9 4 32 33 34 184
		mu 10 4 34 35 36 175
		mu 11 4 42 43 44 183
		mu 12 4 42 43 44 183
		mu 13 4 46 47 48 187
		mu 14 4 42 43 44 183
		mu 15 4 46 47 48 187
		mu 16 4 46 47 48 187
		mu 17 4 30 31 32 171
		mu 18 4 30 31 32 171
		mu 19 4 30 31 32 171
		mu 20 4 30 31 32 171
		mu 21 4 35 36 37 176
		mu 22 4 22 23 24 163
		mu 23 4 22 23 24 163
		mu 24 4 22 23 24 163
		mu 25 4 22 23 24 163
		mu 26 4 22 23 24 163
		mu 27 4 22 23 24 163
		mu 28 4 22 23 24 163
		mu 29 4 22 23 24 163
		mu 30 4 22 23 24 163
		mu 31 4 34 35 36 175
		mu 32 4 34 35 36 175
		mu 33 4 34 35 36 179
		mu 34 4 34 35 36 179
		f 4 -535 540 535 -372
		mu 0 4 36 186 187 23
		mu 1 4 48 198 199 35
		mu 2 4 48 198 199 35
		mu 3 4 51 201 202 38
		mu 4 4 38 188 189 25
		mu 5 4 36 186 187 23
		mu 6 4 36 186 187 23
		mu 7 4 36 186 187 23
		mu 8 4 44 194 195 31
		mu 9 4 36 197 198 23
		mu 10 4 38 188 189 25
		mu 11 4 46 199 200 33
		mu 12 4 46 196 197 33
		mu 13 4 50 200 201 37
		mu 14 4 46 196 197 33
		mu 15 4 50 200 201 37
		mu 16 4 50 200 201 37
		mu 17 4 34 184 185 21
		mu 18 4 34 184 185 21
		mu 19 4 34 184 185 21
		mu 20 4 34 184 185 21
		mu 21 4 39 189 190 26
		mu 22 4 26 176 177 13
		mu 23 4 26 176 177 13
		mu 24 4 26 176 177 13
		mu 25 4 26 176 177 13
		mu 26 4 26 176 177 13
		mu 27 4 26 176 177 13
		mu 28 4 26 176 177 13
		mu 29 4 26 176 177 13
		mu 30 4 26 176 177 13
		mu 31 4 38 193 194 25
		mu 32 4 38 193 194 25
		mu 33 4 38 192 193 25
		mu 34 4 38 192 193 25
		f 4 542 537 366 -537
		mu 0 4 188 189 40 17
		mu 1 4 200 201 52 29
		mu 2 4 200 201 52 29
		mu 3 4 203 204 55 32
		mu 4 4 190 191 42 19
		mu 5 4 188 189 40 17
		mu 6 4 188 189 40 17
		mu 7 4 188 189 40 17
		mu 8 4 196 197 48 25
		mu 9 4 199 200 40 17
		mu 10 4 190 191 42 19
		mu 11 4 201 202 50 27
		mu 12 4 198 199 50 27
		mu 13 4 202 203 54 31
		mu 14 4 198 199 50 27
		mu 15 4 202 203 54 31
		mu 16 4 202 203 54 31
		mu 17 4 186 187 38 15
		mu 18 4 186 187 38 15
		mu 19 4 186 187 38 15
		mu 20 4 186 187 38 15
		mu 21 4 191 192 43 20
		mu 22 4 178 179 30 7
		mu 23 4 178 179 30 7
		mu 24 4 178 179 30 7
		mu 25 4 178 179 30 7
		mu 26 4 178 179 30 7
		mu 27 4 178 179 30 7
		mu 28 4 178 179 30 7
		mu 29 4 178 179 30 7
		mu 30 4 178 179 30 7
		mu 31 4 195 196 42 19
		mu 32 4 195 196 42 19
		mu 33 4 194 195 42 19
		mu 34 4 194 195 42 19
		f 4 -357 379 381 -378
		mu 0 4 42 43 41 38
		mu 1 4 54 55 53 50
		mu 2 4 54 55 53 50
		mu 3 4 57 58 56 53
		mu 4 4 44 45 43 40
		mu 5 4 42 43 41 38
		mu 6 4 42 43 41 38
		mu 7 4 42 43 41 38
		mu 8 4 50 51 49 46
		mu 9 4 42 43 41 38
		mu 10 4 44 45 43 40
		mu 11 4 52 53 51 48
		mu 12 4 52 53 51 48
		mu 13 4 56 57 55 52
		mu 14 4 52 53 51 48
		mu 15 4 56 57 55 52
		mu 16 4 56 57 55 52
		mu 17 4 40 41 39 36
		mu 18 4 40 41 39 36
		mu 19 4 40 41 39 36
		mu 20 4 40 41 39 36
		mu 21 4 45 46 44 41
		mu 22 4 32 33 31 28
		mu 23 4 32 33 31 28
		mu 24 4 32 33 31 28
		mu 25 4 32 33 31 28
		mu 26 4 32 33 31 28
		mu 27 4 32 33 31 28
		mu 28 4 32 33 31 28
		mu 29 4 32 33 31 28
		mu 30 4 32 33 31 28
		mu 31 4 44 45 43 40
		mu 32 4 44 45 43 40
		mu 33 4 44 45 43 40
		mu 34 4 44 45 43 40
		f 4 -486 487 489 -491
		mu 0 4 176 177 178 179
		mu 1 4 188 189 190 191
		mu 2 4 188 189 190 191
		mu 3 4 191 192 193 194
		mu 4 4 178 179 180 181
		mu 5 4 176 177 178 179
		mu 6 4 176 177 178 179
		mu 7 4 176 177 178 179
		mu 8 4 184 185 186 187
		mu 9 4 187 188 189 190
		mu 10 4 178 179 180 181
		mu 11 4 186 187 188 189
		mu 12 4 186 187 188 189
		mu 13 4 190 191 192 193
		mu 14 4 186 187 188 189
		mu 15 4 190 191 192 193
		mu 16 4 190 191 192 193
		mu 17 4 174 175 176 177
		mu 18 4 174 175 176 177
		mu 19 4 174 175 176 177
		mu 20 4 174 175 176 177
		mu 21 4 179 180 181 182
		mu 22 4 166 167 168 169
		mu 23 4 166 167 168 169
		mu 24 4 166 167 168 169
		mu 25 4 166 167 168 169
		mu 26 4 166 167 168 169
		mu 27 4 166 167 168 169
		mu 28 4 166 167 168 169
		mu 29 4 166 167 168 169
		mu 30 4 166 167 168 169
		mu 31 4 178 179 180 181
		mu 32 4 178 179 180 181
		mu 33 4 182 183 184 185
		mu 34 4 182 183 184 185
		f 4 -493 494 495 -488
		mu 0 4 180 181 182 183
		mu 1 4 192 193 194 195
		mu 2 4 192 193 194 195
		mu 3 4 195 196 197 198
		mu 4 4 182 183 184 185
		mu 5 4 180 181 182 183
		mu 6 4 180 181 182 183
		mu 7 4 180 181 182 183
		mu 8 4 188 189 190 191
		mu 9 4 191 192 193 194
		mu 10 4 182 183 184 185
		mu 11 4 190 191 192 193
		mu 12 4 190 191 192 193
		mu 13 4 194 195 196 197
		mu 14 4 190 191 192 193
		mu 15 4 194 195 196 197
		mu 16 4 194 195 196 197
		mu 17 4 178 179 180 181
		mu 18 4 178 179 180 181
		mu 19 4 178 179 180 181
		mu 20 4 178 179 180 181
		mu 21 4 183 184 185 186
		mu 22 4 170 171 172 173
		mu 23 4 170 171 172 173
		mu 24 4 170 171 172 173
		mu 25 4 170 171 172 173
		mu 26 4 170 171 172 173
		mu 27 4 170 171 172 173
		mu 28 4 170 171 172 173
		mu 29 4 170 171 172 173
		mu 30 4 170 171 172 173
		mu 31 4 182 183 184 185
		mu 32 4 182 183 184 185
		mu 33 4 186 187 188 189
		mu 34 4 186 187 188 189
		f 4 -370 364 374 -384
		mu 0 4 20 13 12 27
		mu 1 4 32 25 24 39
		mu 2 4 32 25 24 39
		mu 3 4 35 28 27 42
		mu 4 4 22 15 14 29
		mu 5 4 20 13 12 27
		mu 6 4 20 13 12 27
		mu 7 4 20 13 12 27
		mu 8 4 28 21 20 35
		mu 9 4 20 13 12 27
		mu 10 4 22 15 14 29
		mu 11 4 30 23 22 37
		mu 12 4 30 23 22 37
		mu 13 4 34 27 26 41
		mu 14 4 30 23 22 37
		mu 15 4 34 27 26 41
		mu 16 4 34 27 26 41
		mu 17 4 18 11 10 25
		mu 18 4 18 11 10 25
		mu 19 4 18 11 10 25
		mu 20 4 18 11 10 25
		mu 21 4 23 16 15 30
		mu 22 4 10 3 2 17
		mu 23 4 10 3 2 17
		mu 24 4 10 3 2 17
		mu 25 4 10 3 2 17
		mu 26 4 10 3 2 17
		mu 27 4 10 3 2 17
		mu 28 4 10 3 2 17
		mu 29 4 10 3 2 17
		mu 30 4 10 3 2 17
		mu 31 4 22 15 14 29
		mu 32 4 22 15 14 29
		mu 33 4 22 15 14 29
		mu 34 4 22 15 14 29
		f 4 541 536 382 -536
		mu 0 4 187 188 17 23
		mu 1 4 199 200 29 35
		mu 2 4 199 200 29 35
		mu 3 4 202 203 32 38
		mu 4 4 189 190 19 25
		mu 5 4 187 188 17 23
		mu 6 4 187 188 17 23
		mu 7 4 187 188 17 23
		mu 8 4 195 196 25 31
		mu 9 4 198 199 17 23
		mu 10 4 189 190 19 25
		mu 11 4 200 201 27 33
		mu 12 4 197 198 27 33
		mu 13 4 201 202 31 37
		mu 14 4 197 198 27 33
		mu 15 4 201 202 31 37
		mu 16 4 201 202 31 37
		mu 17 4 185 186 15 21
		mu 18 4 185 186 15 21
		mu 19 4 185 186 15 21
		mu 20 4 185 186 15 21
		mu 21 4 190 191 20 26
		mu 22 4 177 178 7 13
		mu 23 4 177 178 7 13
		mu 24 4 177 178 7 13
		mu 25 4 177 178 7 13
		mu 26 4 177 178 7 13
		mu 27 4 177 178 7 13
		mu 28 4 177 178 7 13
		mu 29 4 177 178 7 13
		mu 30 4 177 178 7 13
		mu 31 4 194 195 19 25
		mu 32 4 194 195 19 25
		mu 33 4 193 194 19 25
		mu 34 4 193 194 19 25
		f 4 -146 384 386 -386
		mu 0 4 44 45 46 47
		mu 1 4 56 57 58 59
		mu 2 4 56 57 58 59
		mu 3 4 59 60 61 62
		mu 4 4 46 47 48 49
		mu 5 4 44 45 46 47
		mu 6 4 44 45 46 47
		mu 7 4 44 45 46 47
		mu 8 4 52 53 54 55
		mu 9 4 44 45 46 47
		mu 10 4 46 47 48 49
		mu 11 4 54 55 56 57
		mu 12 4 54 55 56 57
		mu 13 4 58 59 60 61
		mu 14 4 54 55 56 57
		mu 15 4 58 59 60 61
		mu 16 4 58 59 60 61
		mu 17 4 42 43 44 45
		mu 18 4 42 43 44 45
		mu 19 4 42 43 44 45
		mu 20 4 42 43 44 45
		mu 21 4 47 48 49 50
		mu 22 4 34 35 36 37
		mu 23 4 34 35 36 37
		mu 24 4 34 35 36 37
		mu 25 4 34 35 36 37
		mu 26 4 34 35 36 37
		mu 27 4 34 35 36 37
		mu 28 4 34 35 36 37
		mu 29 4 34 35 36 37
		mu 30 4 34 35 36 37
		mu 31 4 46 47 48 49
		mu 32 4 46 47 48 49
		mu 33 4 46 47 48 49
		mu 34 4 46 47 48 49
		f 4 146 388 -390 -388
		mu 0 4 48 49 50 51
		mu 1 4 60 61 62 63
		mu 2 4 60 61 62 63
		mu 3 4 63 64 65 66
		mu 4 4 50 51 52 53
		mu 5 4 48 49 50 51
		mu 6 4 48 49 50 51
		mu 7 4 48 49 50 51
		mu 8 4 56 57 58 59
		mu 9 4 48 49 50 51
		mu 10 4 50 51 52 53
		mu 11 4 58 59 60 61
		mu 12 4 58 59 60 61
		mu 13 4 62 63 64 65
		mu 14 4 58 59 60 61
		mu 15 4 62 63 64 65
		mu 16 4 62 63 64 65
		mu 17 4 46 47 48 49
		mu 18 4 46 47 48 49
		mu 19 4 46 47 48 49
		mu 20 4 46 47 48 49
		mu 21 4 51 52 53 54
		mu 22 4 38 39 40 41
		mu 23 4 38 39 40 41
		mu 24 4 38 39 40 41
		mu 25 4 38 39 40 41
		mu 26 4 38 39 40 41
		mu 27 4 38 39 40 41
		mu 28 4 38 39 40 41
		mu 29 4 38 39 40 41
		mu 30 4 38 39 40 41
		mu 31 4 50 51 52 53
		mu 32 4 50 51 52 53
		mu 33 4 50 51 52 53
		mu 34 4 50 51 52 53
		f 4 147 387 -391 -385
		mu 0 4 52 53 54 55
		mu 1 4 64 65 66 67
		mu 2 4 64 65 66 67
		mu 3 4 67 68 69 70
		mu 4 4 54 55 56 57
		mu 5 4 52 53 54 55
		mu 6 4 52 53 54 55
		mu 7 4 52 53 54 55
		mu 8 4 60 61 62 63
		mu 9 4 52 53 54 55
		mu 10 4 54 55 56 57
		mu 11 4 62 63 64 65
		mu 12 4 62 63 64 65
		mu 13 4 66 67 68 69
		mu 14 4 62 63 64 65
		mu 15 4 66 67 68 69
		mu 16 4 66 67 68 69
		mu 17 4 50 51 52 53
		mu 18 4 50 51 52 53
		mu 19 4 50 51 52 53
		mu 20 4 50 51 52 53
		mu 21 4 55 56 57 58
		mu 22 4 42 43 44 45
		mu 23 4 42 43 44 45
		mu 24 4 42 43 44 45
		mu 25 4 42 43 44 45
		mu 26 4 42 43 44 45
		mu 27 4 42 43 44 45
		mu 28 4 42 43 44 45
		mu 29 4 42 43 44 45
		mu 30 4 42 43 44 45
		mu 31 4 54 55 56 57
		mu 32 4 54 55 56 57
		mu 33 4 54 55 56 57
		mu 34 4 54 55 56 57
		f 4 -149 385 391 -389
		mu 0 4 56 57 58 59
		mu 1 4 68 69 70 71
		mu 2 4 68 69 70 71
		mu 3 4 71 72 73 74
		mu 4 4 58 59 60 61
		mu 5 4 56 57 58 59
		mu 6 4 56 57 58 59
		mu 7 4 56 57 58 59
		mu 8 4 64 65 66 67
		mu 9 4 56 57 58 59
		mu 10 4 58 59 60 61
		mu 11 4 66 67 68 69
		mu 12 4 66 67 68 69
		mu 13 4 70 71 72 73
		mu 14 4 66 67 68 69
		mu 15 4 70 71 72 73
		mu 16 4 70 71 72 73
		mu 17 4 54 55 56 57
		mu 18 4 54 55 56 57
		mu 19 4 54 55 56 57
		mu 20 4 54 55 56 57
		mu 21 4 59 60 61 62
		mu 22 4 46 47 48 49
		mu 23 4 46 47 48 49
		mu 24 4 46 47 48 49
		mu 25 4 46 47 48 49
		mu 26 4 46 47 48 49
		mu 27 4 46 47 48 49
		mu 28 4 46 47 48 49
		mu 29 4 46 47 48 49
		mu 30 4 46 47 48 49
		mu 31 4 58 59 60 61
		mu 32 4 58 59 60 61
		mu 33 4 58 59 60 61
		mu 34 4 58 59 60 61
		f 4 -150 392 394 -394
		mu 0 4 60 61 62 63
		mu 1 4 72 73 74 75
		mu 2 4 72 73 74 75
		mu 3 4 75 76 77 78
		mu 4 4 62 63 64 65
		mu 5 4 60 61 62 63
		mu 6 4 60 61 62 63
		mu 7 4 60 61 62 63
		mu 8 4 68 69 70 71
		mu 9 4 60 61 62 63
		mu 10 4 62 63 64 65
		mu 11 4 70 71 72 73
		mu 12 4 70 71 72 73
		mu 13 4 74 75 76 77
		mu 14 4 70 71 72 73
		mu 15 4 74 75 76 77
		mu 16 4 74 75 76 77
		mu 17 4 58 59 60 61
		mu 18 4 58 59 60 61
		mu 19 4 58 59 60 61
		mu 20 4 58 59 60 61
		mu 21 4 63 64 65 66
		mu 22 4 50 51 52 53
		mu 23 4 50 51 52 53
		mu 24 4 50 51 52 53
		mu 25 4 50 51 52 53
		mu 26 4 50 51 52 53
		mu 27 4 50 51 52 53
		mu 28 4 50 51 52 53
		mu 29 4 50 51 52 53
		mu 30 4 50 51 52 53
		mu 31 4 62 63 64 65
		mu 32 4 62 63 64 65
		mu 33 4 62 63 64 65
		mu 34 4 62 63 64 65
		f 4 150 396 -398 -396
		mu 0 4 64 65 66 67
		mu 1 4 76 77 78 79
		mu 2 4 76 77 78 79
		mu 3 4 79 80 81 82
		mu 4 4 66 67 68 69
		mu 5 4 64 65 66 67
		mu 6 4 64 65 66 67
		mu 7 4 64 65 66 67
		mu 8 4 72 73 74 75
		mu 9 4 64 65 66 67
		mu 10 4 66 67 68 69
		mu 11 4 74 75 76 77
		mu 12 4 74 75 76 77
		mu 13 4 78 79 80 81
		mu 14 4 74 75 76 77
		mu 15 4 78 79 80 81
		mu 16 4 78 79 80 81
		mu 17 4 62 63 64 65
		mu 18 4 62 63 64 65
		mu 19 4 62 63 64 65
		mu 20 4 62 63 64 65
		mu 21 4 67 68 69 70
		mu 22 4 54 55 56 57
		mu 23 4 54 55 56 57
		mu 24 4 54 55 56 57
		mu 25 4 54 55 56 57
		mu 26 4 54 55 56 57
		mu 27 4 54 55 56 57
		mu 28 4 54 55 56 57
		mu 29 4 54 55 56 57
		mu 30 4 54 55 56 57
		mu 31 4 66 67 68 69
		mu 32 4 66 67 68 69
		mu 33 4 66 67 68 69
		mu 34 4 66 67 68 69
		f 4 151 395 -399 -393
		mu 0 4 68 69 70 71
		mu 1 4 80 81 82 83
		mu 2 4 80 81 82 83
		mu 3 4 83 84 85 86
		mu 4 4 70 71 72 73
		mu 5 4 68 69 70 71
		mu 6 4 68 69 70 71
		mu 7 4 68 69 70 71
		mu 8 4 76 77 78 79
		mu 9 4 68 69 70 71
		mu 10 4 70 71 72 73
		mu 11 4 78 79 80 81
		mu 12 4 78 79 80 81
		mu 13 4 82 83 84 85
		mu 14 4 78 79 80 81
		mu 15 4 82 83 84 85
		mu 16 4 82 83 84 85
		mu 17 4 66 67 68 69
		mu 18 4 66 67 68 69
		mu 19 4 66 67 68 69
		mu 20 4 66 67 68 69
		mu 21 4 71 72 73 74
		mu 22 4 58 59 60 61
		mu 23 4 58 59 60 61
		mu 24 4 58 59 60 61
		mu 25 4 58 59 60 61
		mu 26 4 58 59 60 61
		mu 27 4 58 59 60 61
		mu 28 4 58 59 60 61
		mu 29 4 58 59 60 61
		mu 30 4 58 59 60 61
		mu 31 4 70 71 72 73
		mu 32 4 70 71 72 73
		mu 33 4 70 71 72 73
		mu 34 4 70 71 72 73
		f 4 -153 393 399 -397
		mu 0 4 72 73 74 75
		mu 1 4 84 85 86 87
		mu 2 4 84 85 86 87
		mu 3 4 87 88 89 90
		mu 4 4 74 75 76 77
		mu 5 4 72 73 74 75
		mu 6 4 72 73 74 75
		mu 7 4 72 73 74 75
		mu 8 4 80 81 82 83
		mu 9 4 72 73 74 75
		mu 10 4 74 75 76 77
		mu 11 4 82 83 84 85
		mu 12 4 82 83 84 85
		mu 13 4 86 87 88 89
		mu 14 4 82 83 84 85
		mu 15 4 86 87 88 89
		mu 16 4 86 87 88 89
		mu 17 4 70 71 72 73
		mu 18 4 70 71 72 73
		mu 19 4 70 71 72 73
		mu 20 4 70 71 72 73
		mu 21 4 75 76 77 78
		mu 22 4 62 63 64 65
		mu 23 4 62 63 64 65
		mu 24 4 62 63 64 65
		mu 25 4 62 63 64 65
		mu 26 4 62 63 64 65
		mu 27 4 62 63 64 65
		mu 28 4 62 63 64 65
		mu 29 4 62 63 64 65
		mu 30 4 62 63 64 65
		mu 31 4 74 75 76 77
		mu 32 4 74 75 76 77
		mu 33 4 74 75 76 77
		mu 34 4 74 75 76 77
		f 4 -154 400 402 -402
		mu 0 4 76 77 78 79
		mu 1 4 88 89 90 91
		mu 2 4 88 89 90 91
		mu 3 4 91 92 93 94
		mu 4 4 78 79 80 81
		mu 5 4 76 77 78 79
		mu 6 4 76 77 78 79
		mu 7 4 76 77 78 79
		mu 8 4 84 85 86 87
		mu 9 4 76 77 78 79
		mu 10 4 78 79 80 81
		mu 11 4 86 87 88 89
		mu 12 4 86 87 88 89
		mu 13 4 90 91 92 93
		mu 14 4 86 87 88 89
		mu 15 4 90 91 92 93
		mu 16 4 90 91 92 93
		mu 17 4 74 75 76 77
		mu 18 4 74 75 76 77
		mu 19 4 74 75 76 77
		mu 20 4 74 75 76 77
		mu 21 4 79 80 81 82
		mu 22 4 66 67 68 69
		mu 23 4 66 67 68 69
		mu 24 4 66 67 68 69
		mu 25 4 66 67 68 69
		mu 26 4 66 67 68 69
		mu 27 4 66 67 68 69
		mu 28 4 66 67 68 69
		mu 29 4 66 67 68 69
		mu 30 4 66 67 68 69
		mu 31 4 78 79 80 81
		mu 32 4 78 79 80 81
		mu 33 4 78 79 80 81
		mu 34 4 78 79 80 81
		f 4 154 404 -406 -404
		mu 0 4 80 81 82 83
		mu 1 4 92 93 94 95
		mu 2 4 92 93 94 95
		mu 3 4 95 96 97 98
		mu 4 4 82 83 84 85
		mu 5 4 80 81 82 83
		mu 6 4 80 81 82 83
		mu 7 4 80 81 82 83
		mu 8 4 88 89 90 91
		mu 9 4 80 81 82 83
		mu 10 4 82 83 84 85
		mu 11 4 90 91 92 93
		mu 12 4 90 91 92 93
		mu 13 4 94 95 96 97
		mu 14 4 90 91 92 93
		mu 15 4 94 95 96 97
		mu 16 4 94 95 96 97
		mu 17 4 78 79 80 81
		mu 18 4 78 79 80 81
		mu 19 4 78 79 80 81
		mu 20 4 78 79 80 81
		mu 21 4 83 84 85 86
		mu 22 4 70 71 72 73
		mu 23 4 70 71 72 73
		mu 24 4 70 71 72 73
		mu 25 4 70 71 72 73
		mu 26 4 70 71 72 73
		mu 27 4 70 71 72 73
		mu 28 4 70 71 72 73
		mu 29 4 70 71 72 73
		mu 30 4 70 71 72 73
		mu 31 4 82 83 84 85
		mu 32 4 82 83 84 85
		mu 33 4 82 83 84 85
		mu 34 4 82 83 84 85
		f 4 155 403 -407 -401
		mu 0 4 84 85 86 87
		mu 1 4 96 97 98 99
		mu 2 4 96 97 98 99
		mu 3 4 99 100 101 102
		mu 4 4 86 87 88 89
		mu 5 4 84 85 86 87
		mu 6 4 84 85 86 87
		mu 7 4 84 85 86 87
		mu 8 4 92 93 94 95
		mu 9 4 84 85 86 87
		mu 10 4 86 87 88 89
		mu 11 4 94 95 96 97
		mu 12 4 94 95 96 97
		mu 13 4 98 99 100 101
		mu 14 4 94 95 96 97
		mu 15 4 98 99 100 101
		mu 16 4 98 99 100 101
		mu 17 4 82 83 84 85
		mu 18 4 82 83 84 85
		mu 19 4 82 83 84 85
		mu 20 4 82 83 84 85
		mu 21 4 87 88 89 90
		mu 22 4 74 75 76 77
		mu 23 4 74 75 76 77
		mu 24 4 74 75 76 77
		mu 25 4 74 75 76 77
		mu 26 4 74 75 76 77
		mu 27 4 74 75 76 77
		mu 28 4 74 75 76 77
		mu 29 4 74 75 76 77
		mu 30 4 74 75 76 77
		mu 31 4 86 87 88 89
		mu 32 4 86 87 88 89
		mu 33 4 86 87 88 89
		mu 34 4 86 87 88 89
		f 4 -157 401 407 -405
		mu 0 4 88 89 90 91
		mu 1 4 100 101 102 103
		mu 2 4 100 101 102 103
		mu 3 4 103 104 105 106
		mu 4 4 90 91 92 93
		mu 5 4 88 89 90 91
		mu 6 4 88 89 90 91
		mu 7 4 88 89 90 91
		mu 8 4 96 97 98 99
		mu 9 4 88 89 90 91
		mu 10 4 90 91 92 93
		mu 11 4 98 99 100 101
		mu 12 4 98 99 100 101
		mu 13 4 102 103 104 105
		mu 14 4 98 99 100 101
		mu 15 4 102 103 104 105
		mu 16 4 102 103 104 105
		mu 17 4 86 87 88 89
		mu 18 4 86 87 88 89
		mu 19 4 86 87 88 89
		mu 20 4 86 87 88 89
		mu 21 4 91 92 93 94
		mu 22 4 78 79 80 81
		mu 23 4 78 79 80 81
		mu 24 4 78 79 80 81
		mu 25 4 78 79 80 81
		mu 26 4 78 79 80 81
		mu 27 4 78 79 80 81
		mu 28 4 78 79 80 81
		mu 29 4 78 79 80 81
		mu 30 4 78 79 80 81
		mu 31 4 90 91 92 93
		mu 32 4 90 91 92 93
		mu 33 4 90 91 92 93
		mu 34 4 90 91 92 93
		f 4 -158 408 410 -410
		mu 0 4 92 93 94 95
		mu 1 4 104 105 106 107
		mu 2 4 104 105 106 107
		mu 3 4 107 108 109 110
		mu 4 4 94 95 96 97
		mu 5 4 92 93 94 95
		mu 6 4 92 93 94 95
		mu 7 4 92 93 94 95
		mu 8 4 100 101 102 103
		mu 9 4 92 93 94 95
		mu 10 4 94 95 96 97
		mu 11 4 102 103 104 105
		mu 12 4 102 103 104 105
		mu 13 4 106 107 108 109
		mu 14 4 102 103 104 105
		mu 15 4 106 107 108 109
		mu 16 4 106 107 108 109
		mu 17 4 90 91 92 93
		mu 18 4 90 91 92 93
		mu 19 4 90 91 92 93
		mu 20 4 90 91 92 93
		mu 21 4 95 96 97 98
		mu 22 4 82 83 84 85
		mu 23 4 82 83 84 85
		mu 24 4 82 83 84 85
		mu 25 4 82 83 84 85
		mu 26 4 82 83 84 85
		mu 27 4 82 83 84 85
		mu 28 4 82 83 84 85
		mu 29 4 82 83 84 85
		mu 30 4 82 83 84 85
		mu 31 4 94 95 96 97
		mu 32 4 94 95 96 97
		mu 33 4 94 95 96 97
		mu 34 4 94 95 96 97
		f 4 158 412 -414 -412
		mu 0 4 96 97 98 99
		mu 1 4 108 109 110 111
		mu 2 4 108 109 110 111
		mu 3 4 111 112 113 114
		mu 4 4 98 99 100 101
		mu 5 4 96 97 98 99
		mu 6 4 96 97 98 99
		mu 7 4 96 97 98 99
		mu 8 4 104 105 106 107
		mu 9 4 96 97 98 99
		mu 10 4 98 99 100 101
		mu 11 4 106 107 108 109
		mu 12 4 106 107 108 109
		mu 13 4 110 111 112 113
		mu 14 4 106 107 108 109
		mu 15 4 110 111 112 113
		mu 16 4 110 111 112 113
		mu 17 4 94 95 96 97
		mu 18 4 94 95 96 97
		mu 19 4 94 95 96 97
		mu 20 4 94 95 96 97
		mu 21 4 99 100 101 102
		mu 22 4 86 87 88 89
		mu 23 4 86 87 88 89
		mu 24 4 86 87 88 89
		mu 25 4 86 87 88 89
		mu 26 4 86 87 88 89
		mu 27 4 86 87 88 89
		mu 28 4 86 87 88 89
		mu 29 4 86 87 88 89
		mu 30 4 86 87 88 89
		mu 31 4 98 99 100 101
		mu 32 4 98 99 100 101
		mu 33 4 98 99 100 101
		mu 34 4 98 99 100 101
		f 4 159 411 -415 -409
		mu 0 4 100 101 102 103
		mu 1 4 112 113 114 115
		mu 2 4 112 113 114 115
		mu 3 4 115 116 117 118
		mu 4 4 102 103 104 105
		mu 5 4 100 101 102 103
		mu 6 4 100 101 102 103
		mu 7 4 100 101 102 103
		mu 8 4 108 109 110 111
		mu 9 4 100 101 102 103
		mu 10 4 102 103 104 105
		mu 11 4 110 111 112 113
		mu 12 4 110 111 112 113
		mu 13 4 114 115 116 117
		mu 14 4 110 111 112 113
		mu 15 4 114 115 116 117
		mu 16 4 114 115 116 117
		mu 17 4 98 99 100 101
		mu 18 4 98 99 100 101
		mu 19 4 98 99 100 101
		mu 20 4 98 99 100 101
		mu 21 4 103 104 105 106
		mu 22 4 90 91 92 93
		mu 23 4 90 91 92 93
		mu 24 4 90 91 92 93
		mu 25 4 90 91 92 93
		mu 26 4 90 91 92 93
		mu 27 4 90 91 92 93
		mu 28 4 90 91 92 93
		mu 29 4 90 91 92 93
		mu 30 4 90 91 92 93
		mu 31 4 102 103 104 105
		mu 32 4 102 103 104 105
		mu 33 4 102 103 104 105
		mu 34 4 102 103 104 105
		f 4 -161 409 415 -413
		mu 0 4 104 105 106 107
		mu 1 4 116 117 118 119
		mu 2 4 116 117 118 119
		mu 3 4 119 120 121 122
		mu 4 4 106 107 108 109
		mu 5 4 104 105 106 107
		mu 6 4 104 105 106 107
		mu 7 4 104 105 106 107
		mu 8 4 112 113 114 115
		mu 9 4 104 105 106 107
		mu 10 4 106 107 108 109
		mu 11 4 114 115 116 117
		mu 12 4 114 115 116 117
		mu 13 4 118 119 120 121
		mu 14 4 114 115 116 117
		mu 15 4 118 119 120 121
		mu 16 4 118 119 120 121
		mu 17 4 102 103 104 105
		mu 18 4 102 103 104 105
		mu 19 4 102 103 104 105
		mu 20 4 102 103 104 105
		mu 21 4 107 108 109 110
		mu 22 4 94 95 96 97
		mu 23 4 94 95 96 97
		mu 24 4 94 95 96 97
		mu 25 4 94 95 96 97
		mu 26 4 94 95 96 97
		mu 27 4 94 95 96 97
		mu 28 4 94 95 96 97
		mu 29 4 94 95 96 97
		mu 30 4 94 95 96 97
		mu 31 4 106 107 108 109
		mu 32 4 106 107 108 109
		mu 33 4 106 107 108 109
		mu 34 4 106 107 108 109
		f 4 244 417 -419 -417
		mu 0 4 108 109 110 111
		mu 1 4 120 121 122 123
		mu 2 4 120 121 122 123
		mu 3 4 123 124 125 126
		mu 4 4 110 111 112 113
		mu 5 4 108 109 110 111
		mu 6 4 108 109 110 111
		mu 7 4 108 109 110 111
		mu 8 4 116 117 118 119
		mu 9 4 108 109 110 111
		mu 10 4 110 111 112 113
		mu 11 4 118 119 120 121
		mu 12 4 118 119 120 121
		mu 13 4 122 123 124 125
		mu 14 4 118 119 120 121
		mu 15 4 122 123 124 125
		mu 16 4 122 123 124 125
		mu 17 4 106 107 108 109
		mu 18 4 106 107 108 109
		mu 19 4 106 107 108 109
		mu 20 4 106 107 108 109
		mu 21 4 111 112 113 114
		mu 22 4 98 99 100 101
		mu 23 4 98 99 100 101
		mu 24 4 98 99 100 101
		mu 25 4 98 99 100 101
		mu 26 4 98 99 100 101
		mu 27 4 98 99 100 101
		mu 28 4 98 99 100 101
		mu 29 4 98 99 100 101
		mu 30 4 98 99 100 101
		mu 31 4 110 111 112 113
		mu 32 4 110 111 112 113
		mu 33 4 110 111 112 113
		mu 34 4 110 111 112 113
		f 4 245 419 -421 -418
		mu 0 4 112 113 114 115
		mu 1 4 124 125 126 127
		mu 2 4 124 125 126 127
		mu 3 4 127 128 129 130
		mu 4 4 114 115 116 117
		mu 5 4 112 113 114 115
		mu 6 4 112 113 114 115
		mu 7 4 112 113 114 115
		mu 8 4 120 121 122 123
		mu 9 4 112 113 114 115
		mu 10 4 114 115 116 117
		mu 11 4 122 123 124 125
		mu 12 4 122 123 124 125
		mu 13 4 126 127 128 129
		mu 14 4 122 123 124 125
		mu 15 4 126 127 128 129
		mu 16 4 126 127 128 129
		mu 17 4 110 111 112 113
		mu 18 4 110 111 112 113
		mu 19 4 110 111 112 113
		mu 20 4 110 111 112 113
		mu 21 4 115 116 117 118
		mu 22 4 102 103 104 105
		mu 23 4 102 103 104 105
		mu 24 4 102 103 104 105
		mu 25 4 102 103 104 105
		mu 26 4 102 103 104 105
		mu 27 4 102 103 104 105
		mu 28 4 102 103 104 105
		mu 29 4 102 103 104 105
		mu 30 4 102 103 104 105
		mu 31 4 114 115 116 117
		mu 32 4 114 115 116 117
		mu 33 4 114 115 116 117
		mu 34 4 114 115 116 117
		f 4 -247 421 422 -420
		mu 0 4 116 117 118 119
		mu 1 4 128 129 130 131
		mu 2 4 128 129 130 131
		mu 3 4 131 132 133 134
		mu 4 4 118 119 120 121
		mu 5 4 116 117 118 119
		mu 6 4 116 117 118 119
		mu 7 4 116 117 118 119
		mu 8 4 124 125 126 127
		mu 9 4 116 117 118 119
		mu 10 4 118 119 120 121
		mu 11 4 126 127 128 129
		mu 12 4 126 127 128 129
		mu 13 4 130 131 132 133
		mu 14 4 126 127 128 129
		mu 15 4 130 131 132 133
		mu 16 4 130 131 132 133
		mu 17 4 114 115 116 117
		mu 18 4 114 115 116 117
		mu 19 4 114 115 116 117
		mu 20 4 114 115 116 117
		mu 21 4 119 120 121 122
		mu 22 4 106 107 108 109
		mu 23 4 106 107 108 109
		mu 24 4 106 107 108 109
		mu 25 4 106 107 108 109
		mu 26 4 106 107 108 109
		mu 27 4 106 107 108 109
		mu 28 4 106 107 108 109
		mu 29 4 106 107 108 109
		mu 30 4 106 107 108 109
		mu 31 4 118 119 120 121
		mu 32 4 118 119 120 121
		mu 33 4 118 119 120 121
		mu 34 4 118 119 120 121
		f 4 -248 416 423 -422
		mu 0 4 120 121 122 123
		mu 1 4 132 133 134 135
		mu 2 4 132 133 134 135
		mu 3 4 135 136 137 138
		mu 4 4 122 123 124 125
		mu 5 4 120 121 122 123
		mu 6 4 120 121 122 123
		mu 7 4 120 121 122 123
		mu 8 4 128 129 130 131
		mu 9 4 120 121 122 123
		mu 10 4 122 123 124 125
		mu 11 4 130 131 132 133
		mu 12 4 130 131 132 133
		mu 13 4 134 135 136 137
		mu 14 4 130 131 132 133
		mu 15 4 134 135 136 137
		mu 16 4 134 135 136 137
		mu 17 4 118 119 120 121
		mu 18 4 118 119 120 121
		mu 19 4 118 119 120 121
		mu 20 4 118 119 120 121
		mu 21 4 123 124 125 126
		mu 22 4 110 111 112 113
		mu 23 4 110 111 112 113
		mu 24 4 110 111 112 113
		mu 25 4 110 111 112 113
		mu 26 4 110 111 112 113
		mu 27 4 110 111 112 113
		mu 28 4 110 111 112 113
		mu 29 4 110 111 112 113
		mu 30 4 110 111 112 113
		mu 31 4 122 123 124 125
		mu 32 4 122 123 124 125
		mu 33 4 122 123 124 125
		mu 34 4 122 123 124 125
		f 4 248 425 -427 -425
		mu 0 4 124 125 126 127
		mu 1 4 136 137 138 139
		mu 2 4 136 137 138 139
		mu 3 4 139 140 141 142
		mu 4 4 126 127 128 129
		mu 5 4 124 125 126 127
		mu 6 4 124 125 126 127
		mu 7 4 124 125 126 127
		mu 8 4 132 133 134 135
		mu 9 4 124 125 126 127
		mu 10 4 126 127 128 129
		mu 11 4 134 135 136 137
		mu 12 4 134 135 136 137
		mu 13 4 138 139 140 141
		mu 14 4 134 135 136 137
		mu 15 4 138 139 140 141
		mu 16 4 138 139 140 141
		mu 17 4 122 123 124 125
		mu 18 4 122 123 124 125
		mu 19 4 122 123 124 125
		mu 20 4 122 123 124 125
		mu 21 4 127 128 129 130
		mu 22 4 114 115 116 117
		mu 23 4 114 115 116 117
		mu 24 4 114 115 116 117
		mu 25 4 114 115 116 117
		mu 26 4 114 115 116 117
		mu 27 4 114 115 116 117
		mu 28 4 114 115 116 117
		mu 29 4 114 115 116 117
		mu 30 4 114 115 116 117
		mu 31 4 126 127 128 129
		mu 32 4 126 127 128 129
		mu 33 4 126 127 128 129
		mu 34 4 126 127 128 129
		f 4 249 427 -429 -426
		mu 0 4 128 129 130 131
		mu 1 4 140 141 142 143
		mu 2 4 140 141 142 143
		mu 3 4 143 144 145 146
		mu 4 4 130 131 132 133
		mu 5 4 128 129 130 131
		mu 6 4 128 129 130 131
		mu 7 4 128 129 130 131
		mu 8 4 136 137 138 139
		mu 9 4 128 129 130 131
		mu 10 4 130 131 132 133
		mu 11 4 138 139 140 141
		mu 12 4 138 139 140 141
		mu 13 4 142 143 144 145
		mu 14 4 138 139 140 141
		mu 15 4 142 143 144 145
		mu 16 4 142 143 144 145
		mu 17 4 126 127 128 129
		mu 18 4 126 127 128 129
		mu 19 4 126 127 128 129
		mu 20 4 126 127 128 129
		mu 21 4 131 132 133 134
		mu 22 4 118 119 120 121
		mu 23 4 118 119 120 121
		mu 24 4 118 119 120 121
		mu 25 4 118 119 120 121
		mu 26 4 118 119 120 121
		mu 27 4 118 119 120 121
		mu 28 4 118 119 120 121
		mu 29 4 118 119 120 121
		mu 30 4 118 119 120 121
		mu 31 4 130 131 132 133
		mu 32 4 130 131 132 133
		mu 33 4 130 131 132 133
		mu 34 4 130 131 132 133
		f 4 -251 429 430 -428
		mu 0 4 132 133 134 135
		mu 1 4 144 145 146 147
		mu 2 4 144 145 146 147
		mu 3 4 147 148 149 150
		mu 4 4 134 135 136 137
		mu 5 4 132 133 134 135
		mu 6 4 132 133 134 135
		mu 7 4 132 133 134 135
		mu 8 4 140 141 142 143
		mu 9 4 132 133 134 135
		mu 10 4 134 135 136 137
		mu 11 4 142 143 144 145
		mu 12 4 142 143 144 145
		mu 13 4 146 147 148 149
		mu 14 4 142 143 144 145
		mu 15 4 146 147 148 149
		mu 16 4 146 147 148 149
		mu 17 4 130 131 132 133
		mu 18 4 130 131 132 133
		mu 19 4 130 131 132 133
		mu 20 4 130 131 132 133
		mu 21 4 135 136 137 138
		mu 22 4 122 123 124 125
		mu 23 4 122 123 124 125
		mu 24 4 122 123 124 125
		mu 25 4 122 123 124 125
		mu 26 4 122 123 124 125
		mu 27 4 122 123 124 125
		mu 28 4 122 123 124 125
		mu 29 4 122 123 124 125
		mu 30 4 122 123 124 125
		mu 31 4 134 135 136 137
		mu 32 4 134 135 136 137
		mu 33 4 134 135 136 137
		mu 34 4 134 135 136 137
		f 4 -252 424 431 -430
		mu 0 4 136 137 138 139
		mu 1 4 148 149 150 151
		mu 2 4 148 149 150 151
		mu 3 4 151 152 153 154
		mu 4 4 138 139 140 141
		mu 5 4 136 137 138 139
		mu 6 4 136 137 138 139
		mu 7 4 136 137 138 139
		mu 8 4 144 145 146 147
		mu 9 4 136 137 138 139
		mu 10 4 138 139 140 141
		mu 11 4 146 147 148 149
		mu 12 4 146 147 148 149
		mu 13 4 150 151 152 153
		mu 14 4 146 147 148 149
		mu 15 4 150 151 152 153
		mu 16 4 150 151 152 153
		mu 17 4 134 135 136 137
		mu 18 4 134 135 136 137
		mu 19 4 134 135 136 137
		mu 20 4 134 135 136 137
		mu 21 4 139 140 141 142
		mu 22 4 126 127 128 129
		mu 23 4 126 127 128 129
		mu 24 4 126 127 128 129
		mu 25 4 126 127 128 129
		mu 26 4 126 127 128 129
		mu 27 4 126 127 128 129
		mu 28 4 126 127 128 129
		mu 29 4 126 127 128 129
		mu 30 4 126 127 128 129
		mu 31 4 138 139 140 141
		mu 32 4 138 139 140 141
		mu 33 4 138 139 140 141
		mu 34 4 138 139 140 141
		f 4 252 433 -435 -433
		mu 0 4 140 141 142 143
		mu 1 4 152 153 154 155
		mu 2 4 152 153 154 155
		mu 3 4 155 156 157 158
		mu 4 4 142 143 144 145
		mu 5 4 140 141 142 143
		mu 6 4 140 141 142 143
		mu 7 4 140 141 142 143
		mu 8 4 148 149 150 151
		mu 9 4 140 141 142 143
		mu 10 4 142 143 144 145
		mu 11 4 150 151 152 153
		mu 12 4 150 151 152 153
		mu 13 4 154 155 156 157
		mu 14 4 150 151 152 153
		mu 15 4 154 155 156 157
		mu 16 4 154 155 156 157
		mu 17 4 138 139 140 141
		mu 18 4 138 139 140 141
		mu 19 4 138 139 140 141
		mu 20 4 138 139 140 141
		mu 21 4 143 144 145 146
		mu 22 4 130 131 132 133
		mu 23 4 130 131 132 133
		mu 24 4 130 131 132 133
		mu 25 4 130 131 132 133
		mu 26 4 130 131 132 133
		mu 27 4 130 131 132 133
		mu 28 4 130 131 132 133
		mu 29 4 130 131 132 133
		mu 30 4 130 131 132 133
		mu 31 4 142 143 144 145
		mu 32 4 142 143 144 145
		mu 33 4 142 143 144 145
		mu 34 4 142 143 144 145
		f 4 253 435 -437 -434
		mu 0 4 144 145 146 147
		mu 1 4 156 157 158 159
		mu 2 4 156 157 158 159
		mu 3 4 159 160 161 162
		mu 4 4 146 147 148 149
		mu 5 4 144 145 146 147
		mu 6 4 144 145 146 147
		mu 7 4 144 145 146 147
		mu 8 4 152 153 154 155
		mu 9 4 144 145 146 147
		mu 10 4 146 147 148 149
		mu 11 4 154 155 156 157
		mu 12 4 154 155 156 157
		mu 13 4 158 159 160 161
		mu 14 4 154 155 156 157
		mu 15 4 158 159 160 161
		mu 16 4 158 159 160 161
		mu 17 4 142 143 144 145
		mu 18 4 142 143 144 145
		mu 19 4 142 143 144 145
		mu 20 4 142 143 144 145
		mu 21 4 147 148 149 150
		mu 22 4 134 135 136 137
		mu 23 4 134 135 136 137
		mu 24 4 134 135 136 137
		mu 25 4 134 135 136 137
		mu 26 4 134 135 136 137
		mu 27 4 134 135 136 137
		mu 28 4 134 135 136 137
		mu 29 4 134 135 136 137
		mu 30 4 134 135 136 137
		mu 31 4 146 147 148 149
		mu 32 4 146 147 148 149
		mu 33 4 146 147 148 149
		mu 34 4 146 147 148 149
		f 4 -255 437 438 -436
		mu 0 4 148 149 150 151
		mu 1 4 160 161 162 163
		mu 2 4 160 161 162 163
		mu 3 4 163 164 165 166
		mu 4 4 150 151 152 153
		mu 5 4 148 149 150 151
		mu 6 4 148 149 150 151
		mu 7 4 148 149 150 151
		mu 8 4 156 157 158 159
		mu 9 4 148 149 150 151
		mu 10 4 150 151 152 153
		mu 11 4 158 159 160 161
		mu 12 4 158 159 160 161
		mu 13 4 162 163 164 165
		mu 14 4 158 159 160 161
		mu 15 4 162 163 164 165
		mu 16 4 162 163 164 165
		mu 17 4 146 147 148 149
		mu 18 4 146 147 148 149
		mu 19 4 146 147 148 149
		mu 20 4 146 147 148 149
		mu 21 4 151 152 153 154
		mu 22 4 138 139 140 141
		mu 23 4 138 139 140 141
		mu 24 4 138 139 140 141
		mu 25 4 138 139 140 141
		mu 26 4 138 139 140 141
		mu 27 4 138 139 140 141
		mu 28 4 138 139 140 141
		mu 29 4 138 139 140 141
		mu 30 4 138 139 140 141
		mu 31 4 150 151 152 153
		mu 32 4 150 151 152 153
		mu 33 4 150 151 152 153
		mu 34 4 150 151 152 153
		f 4 -256 432 439 -438
		mu 0 4 152 153 154 155
		mu 1 4 164 165 166 167
		mu 2 4 164 165 166 167
		mu 3 4 167 168 169 170
		mu 4 4 154 155 156 157
		mu 5 4 152 153 154 155
		mu 6 4 152 153 154 155
		mu 7 4 152 153 154 155
		mu 8 4 160 161 162 163
		mu 9 4 152 153 154 155
		mu 10 4 154 155 156 157
		mu 11 4 162 163 164 165
		mu 12 4 162 163 164 165
		mu 13 4 166 167 168 169
		mu 14 4 162 163 164 165
		mu 15 4 166 167 168 169
		mu 16 4 166 167 168 169
		mu 17 4 150 151 152 153
		mu 18 4 150 151 152 153
		mu 19 4 150 151 152 153
		mu 20 4 150 151 152 153
		mu 21 4 155 156 157 158
		mu 22 4 142 143 144 145
		mu 23 4 142 143 144 145
		mu 24 4 142 143 144 145
		mu 25 4 142 143 144 145
		mu 26 4 142 143 144 145
		mu 27 4 142 143 144 145
		mu 28 4 142 143 144 145
		mu 29 4 142 143 144 145
		mu 30 4 142 143 144 145
		mu 31 4 154 155 156 157
		mu 32 4 154 155 156 157
		mu 33 4 154 155 156 157
		mu 34 4 154 155 156 157
		f 4 256 441 -443 -441
		mu 0 4 156 157 158 159
		mu 1 4 168 169 170 171
		mu 2 4 168 169 170 171
		mu 3 4 171 172 173 174
		mu 4 4 158 159 160 161
		mu 5 4 156 157 158 159
		mu 6 4 156 157 158 159
		mu 7 4 156 157 158 159
		mu 8 4 164 165 166 167
		mu 9 4 156 157 158 159
		mu 10 4 158 159 160 161
		mu 11 4 166 167 168 169
		mu 12 4 166 167 168 169
		mu 13 4 170 171 172 173
		mu 14 4 166 167 168 169
		mu 15 4 170 171 172 173
		mu 16 4 170 171 172 173
		mu 17 4 154 155 156 157
		mu 18 4 154 155 156 157
		mu 19 4 154 155 156 157
		mu 20 4 154 155 156 157
		mu 21 4 159 160 161 162
		mu 22 4 146 147 148 149
		mu 23 4 146 147 148 149
		mu 24 4 146 147 148 149
		mu 25 4 146 147 148 149
		mu 26 4 146 147 148 149
		mu 27 4 146 147 148 149
		mu 28 4 146 147 148 149
		mu 29 4 146 147 148 149
		mu 30 4 146 147 148 149
		mu 31 4 158 159 160 161
		mu 32 4 158 159 160 161
		mu 33 4 158 159 160 161
		mu 34 4 158 159 160 161
		f 4 257 443 -445 -442
		mu 0 4 160 161 162 163
		mu 1 4 172 173 174 175
		mu 2 4 172 173 174 175
		mu 3 4 175 176 177 178
		mu 4 4 162 163 164 165
		mu 5 4 160 161 162 163
		mu 6 4 160 161 162 163
		mu 7 4 160 161 162 163
		mu 8 4 168 169 170 171
		mu 9 4 160 161 162 163
		mu 10 4 162 163 164 165
		mu 11 4 170 171 172 173
		mu 12 4 170 171 172 173
		mu 13 4 174 175 176 177
		mu 14 4 170 171 172 173
		mu 15 4 174 175 176 177
		mu 16 4 174 175 176 177
		mu 17 4 158 159 160 161
		mu 18 4 158 159 160 161
		mu 19 4 158 159 160 161
		mu 20 4 158 159 160 161
		mu 21 4 163 164 165 166
		mu 22 4 150 151 152 153
		mu 23 4 150 151 152 153
		mu 24 4 150 151 152 153
		mu 25 4 150 151 152 153
		mu 26 4 150 151 152 153
		mu 27 4 150 151 152 153
		mu 28 4 150 151 152 153
		mu 29 4 150 151 152 153
		mu 30 4 150 151 152 153
		mu 31 4 162 163 164 165
		mu 32 4 162 163 164 165
		mu 33 4 162 163 164 165
		mu 34 4 162 163 164 165
		f 4 -259 445 446 -444
		mu 0 4 164 165 166 167
		mu 1 4 176 177 178 179
		mu 2 4 176 177 178 179
		mu 3 4 179 180 181 182
		mu 4 4 166 167 168 169
		mu 5 4 164 165 166 167
		mu 6 4 164 165 166 167
		mu 7 4 164 165 166 167
		mu 8 4 172 173 174 175
		mu 9 4 164 165 166 167
		mu 10 4 166 167 168 169
		mu 11 4 174 175 176 177
		mu 12 4 174 175 176 177
		mu 13 4 178 179 180 181
		mu 14 4 174 175 176 177
		mu 15 4 178 179 180 181
		mu 16 4 178 179 180 181
		mu 17 4 162 163 164 165
		mu 18 4 162 163 164 165
		mu 19 4 162 163 164 165
		mu 20 4 162 163 164 165
		mu 21 4 167 168 169 170
		mu 22 4 154 155 156 157
		mu 23 4 154 155 156 157
		mu 24 4 154 155 156 157
		mu 25 4 154 155 156 157
		mu 26 4 154 155 156 157
		mu 27 4 154 155 156 157
		mu 28 4 154 155 156 157
		mu 29 4 154 155 156 157
		mu 30 4 154 155 156 157
		mu 31 4 166 167 168 169
		mu 32 4 166 167 168 169
		mu 33 4 166 167 168 169
		mu 34 4 166 167 168 169
		f 4 -260 440 447 -446
		mu 0 4 168 169 170 171
		mu 1 4 180 181 182 183
		mu 2 4 180 181 182 183
		mu 3 4 183 184 185 186
		mu 4 4 170 171 172 173
		mu 5 4 168 169 170 171
		mu 6 4 168 169 170 171
		mu 7 4 168 169 170 171
		mu 8 4 176 177 178 179
		mu 9 4 168 169 170 171
		mu 10 4 170 171 172 173
		mu 11 4 178 179 180 181
		mu 12 4 178 179 180 181
		mu 13 4 182 183 184 185
		mu 14 4 178 179 180 181
		mu 15 4 182 183 184 185
		mu 16 4 182 183 184 185
		mu 17 4 166 167 168 169
		mu 18 4 166 167 168 169
		mu 19 4 166 167 168 169
		mu 20 4 166 167 168 169
		mu 21 4 171 172 173 174
		mu 22 4 158 159 160 161
		mu 23 4 158 159 160 161
		mu 24 4 158 159 160 161
		mu 25 4 158 159 160 161
		mu 26 4 158 159 160 161
		mu 27 4 158 159 160 161
		mu 28 4 158 159 160 161
		mu 29 4 158 159 160 161
		mu 30 4 158 159 160 161
		mu 31 4 170 171 172 173
		mu 32 4 170 171 172 173
		mu 33 4 170 171 172 173
		mu 34 4 170 171 172 173
		f 4 -387 390 389 -392
		mu 0 4 47 55 51 59
		mu 1 4 59 67 63 71
		mu 2 4 59 67 63 71
		mu 3 4 62 70 66 74
		mu 4 4 49 57 53 61
		mu 5 4 47 55 51 59
		mu 6 4 47 55 51 59
		mu 7 4 47 55 51 59
		mu 8 4 55 63 59 67
		mu 9 4 47 55 51 59
		mu 10 4 49 57 53 61
		mu 11 4 57 65 61 69
		mu 12 4 57 65 61 69
		mu 13 4 61 69 65 73
		mu 14 4 57 65 61 69
		mu 15 4 61 69 65 73
		mu 16 4 61 69 65 73
		mu 17 4 45 53 49 57
		mu 18 4 45 53 49 57
		mu 19 4 45 53 49 57
		mu 20 4 45 53 49 57
		mu 21 4 50 58 54 62
		mu 22 4 37 45 41 49
		mu 23 4 37 45 41 49
		mu 24 4 37 45 41 49
		mu 25 4 37 45 41 49
		mu 26 4 37 45 41 49
		mu 27 4 37 45 41 49
		mu 28 4 37 45 41 49
		mu 29 4 37 45 41 49
		mu 30 4 37 45 41 49
		mu 31 4 49 57 53 61
		mu 32 4 49 57 53 61
		mu 33 4 49 57 53 61
		mu 34 4 49 57 53 61
		f 4 -395 398 397 -400
		mu 0 4 63 71 67 75
		mu 1 4 75 83 79 87
		mu 2 4 75 83 79 87
		mu 3 4 78 86 82 90
		mu 4 4 65 73 69 77
		mu 5 4 63 71 67 75
		mu 6 4 63 71 67 75
		mu 7 4 63 71 67 75
		mu 8 4 71 79 75 83
		mu 9 4 63 71 67 75
		mu 10 4 65 73 69 77
		mu 11 4 73 81 77 85
		mu 12 4 73 81 77 85
		mu 13 4 77 85 81 89
		mu 14 4 73 81 77 85
		mu 15 4 77 85 81 89
		mu 16 4 77 85 81 89
		mu 17 4 61 69 65 73
		mu 18 4 61 69 65 73
		mu 19 4 61 69 65 73
		mu 20 4 61 69 65 73
		mu 21 4 66 74 70 78
		mu 22 4 53 61 57 65
		mu 23 4 53 61 57 65
		mu 24 4 53 61 57 65
		mu 25 4 53 61 57 65
		mu 26 4 53 61 57 65
		mu 27 4 53 61 57 65
		mu 28 4 53 61 57 65
		mu 29 4 53 61 57 65
		mu 30 4 53 61 57 65
		mu 31 4 65 73 69 77
		mu 32 4 65 73 69 77
		mu 33 4 65 73 69 77
		mu 34 4 65 73 69 77
		f 4 -403 406 405 -408
		mu 0 4 79 87 83 91
		mu 1 4 91 99 95 103
		mu 2 4 91 99 95 103
		mu 3 4 94 102 98 106
		mu 4 4 81 89 85 93
		mu 5 4 79 87 83 91
		mu 6 4 79 87 83 91
		mu 7 4 79 87 83 91
		mu 8 4 87 95 91 99
		mu 9 4 79 87 83 91
		mu 10 4 81 89 85 93
		mu 11 4 89 97 93 101
		mu 12 4 89 97 93 101
		mu 13 4 93 101 97 105
		mu 14 4 89 97 93 101
		mu 15 4 93 101 97 105
		mu 16 4 93 101 97 105
		mu 17 4 77 85 81 89
		mu 18 4 77 85 81 89
		mu 19 4 77 85 81 89
		mu 20 4 77 85 81 89
		mu 21 4 82 90 86 94
		mu 22 4 69 77 73 81
		mu 23 4 69 77 73 81
		mu 24 4 69 77 73 81
		mu 25 4 69 77 73 81
		mu 26 4 69 77 73 81
		mu 27 4 69 77 73 81
		mu 28 4 69 77 73 81
		mu 29 4 69 77 73 81
		mu 30 4 69 77 73 81
		mu 31 4 81 89 85 93
		mu 32 4 81 89 85 93
		mu 33 4 81 89 85 93
		mu 34 4 81 89 85 93
		f 4 -411 414 413 -416
		mu 0 4 95 103 99 107
		mu 1 4 107 115 111 119
		mu 2 4 107 115 111 119
		mu 3 4 110 118 114 122
		mu 4 4 97 105 101 109
		mu 5 4 95 103 99 107
		mu 6 4 95 103 99 107
		mu 7 4 95 103 99 107
		mu 8 4 103 111 107 115
		mu 9 4 95 103 99 107
		mu 10 4 97 105 101 109
		mu 11 4 105 113 109 117
		mu 12 4 105 113 109 117
		mu 13 4 109 117 113 121
		mu 14 4 105 113 109 117
		mu 15 4 109 117 113 121
		mu 16 4 109 117 113 121
		mu 17 4 93 101 97 105
		mu 18 4 93 101 97 105
		mu 19 4 93 101 97 105
		mu 20 4 93 101 97 105
		mu 21 4 98 106 102 110
		mu 22 4 85 93 89 97
		mu 23 4 85 93 89 97
		mu 24 4 85 93 89 97
		mu 25 4 85 93 89 97
		mu 26 4 85 93 89 97
		mu 27 4 85 93 89 97
		mu 28 4 85 93 89 97
		mu 29 4 85 93 89 97
		mu 30 4 85 93 89 97
		mu 31 4 97 105 101 109
		mu 32 4 97 105 101 109
		mu 33 4 97 105 101 109
		mu 34 4 97 105 101 109
		f 4 420 -423 -424 418
		mu 0 4 115 119 123 111
		mu 1 4 127 131 135 123
		mu 2 4 127 131 135 123
		mu 3 4 130 134 138 126
		mu 4 4 117 121 125 113
		mu 5 4 115 119 123 111
		mu 6 4 115 119 123 111
		mu 7 4 115 119 123 111
		mu 8 4 123 127 131 119
		mu 9 4 115 119 123 111
		mu 10 4 117 121 125 113
		mu 11 4 125 129 133 121
		mu 12 4 125 129 133 121
		mu 13 4 129 133 137 125
		mu 14 4 125 129 133 121
		mu 15 4 129 133 137 125
		mu 16 4 129 133 137 125
		mu 17 4 113 117 121 109
		mu 18 4 113 117 121 109
		mu 19 4 113 117 121 109
		mu 20 4 113 117 121 109
		mu 21 4 118 122 126 114
		mu 22 4 105 109 113 101
		mu 23 4 105 109 113 101
		mu 24 4 105 109 113 101
		mu 25 4 105 109 113 101
		mu 26 4 105 109 113 101
		mu 27 4 105 109 113 101
		mu 28 4 105 109 113 101
		mu 29 4 105 109 113 101
		mu 30 4 105 109 113 101
		mu 31 4 117 121 125 113
		mu 32 4 117 121 125 113
		mu 33 4 117 121 125 113
		mu 34 4 117 121 125 113
		f 4 428 -431 -432 426
		mu 0 4 131 135 139 127
		mu 1 4 143 147 151 139
		mu 2 4 143 147 151 139
		mu 3 4 146 150 154 142
		mu 4 4 133 137 141 129
		mu 5 4 131 135 139 127
		mu 6 4 131 135 139 127
		mu 7 4 131 135 139 127
		mu 8 4 139 143 147 135
		mu 9 4 131 135 139 127
		mu 10 4 133 137 141 129
		mu 11 4 141 145 149 137
		mu 12 4 141 145 149 137
		mu 13 4 145 149 153 141
		mu 14 4 141 145 149 137
		mu 15 4 145 149 153 141
		mu 16 4 145 149 153 141
		mu 17 4 129 133 137 125
		mu 18 4 129 133 137 125
		mu 19 4 129 133 137 125
		mu 20 4 129 133 137 125
		mu 21 4 134 138 142 130
		mu 22 4 121 125 129 117
		mu 23 4 121 125 129 117
		mu 24 4 121 125 129 117
		mu 25 4 121 125 129 117
		mu 26 4 121 125 129 117
		mu 27 4 121 125 129 117
		mu 28 4 121 125 129 117
		mu 29 4 121 125 129 117
		mu 30 4 121 125 129 117
		mu 31 4 133 137 141 129
		mu 32 4 133 137 141 129
		mu 33 4 133 137 141 129
		mu 34 4 133 137 141 129
		f 4 436 -439 -440 434
		mu 0 4 147 151 155 143
		mu 1 4 159 163 167 155
		mu 2 4 159 163 167 155
		mu 3 4 162 166 170 158
		mu 4 4 149 153 157 145
		mu 5 4 147 151 155 143
		mu 6 4 147 151 155 143
		mu 7 4 147 151 155 143
		mu 8 4 155 159 163 151
		mu 9 4 147 151 155 143
		mu 10 4 149 153 157 145
		mu 11 4 157 161 165 153
		mu 12 4 157 161 165 153
		mu 13 4 161 165 169 157
		mu 14 4 157 161 165 153
		mu 15 4 161 165 169 157
		mu 16 4 161 165 169 157
		mu 17 4 145 149 153 141
		mu 18 4 145 149 153 141
		mu 19 4 145 149 153 141
		mu 20 4 145 149 153 141
		mu 21 4 150 154 158 146
		mu 22 4 137 141 145 133
		mu 23 4 137 141 145 133
		mu 24 4 137 141 145 133
		mu 25 4 137 141 145 133
		mu 26 4 137 141 145 133
		mu 27 4 137 141 145 133
		mu 28 4 137 141 145 133
		mu 29 4 137 141 145 133
		mu 30 4 137 141 145 133
		mu 31 4 149 153 157 145
		mu 32 4 149 153 157 145
		mu 33 4 149 153 157 145
		mu 34 4 149 153 157 145
		f 4 444 -447 -448 442
		mu 0 4 163 167 171 159
		mu 1 4 175 179 183 171
		mu 2 4 175 179 183 171
		mu 3 4 178 182 186 174
		mu 4 4 165 169 173 161
		mu 5 4 163 167 171 159
		mu 6 4 163 167 171 159
		mu 7 4 163 167 171 159
		mu 8 4 171 175 179 167
		mu 9 4 163 167 171 159
		mu 10 4 165 169 173 161
		mu 11 4 173 177 181 169
		mu 12 4 173 177 181 169
		mu 13 4 177 181 185 173
		mu 14 4 173 177 181 169
		mu 15 4 177 181 185 173
		mu 16 4 177 181 185 173
		mu 17 4 161 165 169 157
		mu 18 4 161 165 169 157
		mu 19 4 161 165 169 157
		mu 20 4 161 165 169 157
		mu 21 4 166 170 174 162
		mu 22 4 153 157 161 149
		mu 23 4 153 157 161 149
		mu 24 4 153 157 161 149
		mu 25 4 153 157 161 149
		mu 26 4 153 157 161 149
		mu 27 4 153 157 161 149
		mu 28 4 153 157 161 149
		mu 29 4 153 157 161 149
		mu 30 4 153 157 161 149
		mu 31 4 165 169 173 161
		mu 32 4 165 169 173 161
		mu 33 4 165 169 173 161
		mu 34 4 165 169 173 161
		f 4 475 464 82 -470
		mu 9 4 201 176 1 202
		f 4 181 185 -458 -83
		mu 33 4 0 1 196 197
		f 4 -189 -452 -459 -186
		mu 33 4 1 10 198 199
		f 4 -460 451 -185 -453
		mu 33 4 200 201 6 7
		f 4 471 -461 452 -466
		mu 9 4 178 203 204 6
		f 4 -462 453 192 -455
		mu 34 4 196 197 6 7
		f 4 193 -463 454 196
		mu 34 4 3 198 199 11
		f 4 81 -464 -194 -190
		mu 34 4 0 200 201 3
		f 4 80 -471 -78 -88
		mu 9 4 7 178 177 9
		f 4 -467 -472 -81 -454
		mu 9 4 175 205 178 7
		f 4 79 -473 506 -86
		mu 9 4 5 180 179 206
		f 4 78 -474 -80 -84
		mu 9 4 3 181 180 5
		f 4 502 -475 -79 -449
		mu 9 4 172 182 181 3
		f 4 77 -476 -457 -82
		mu 9 4 0 176 207 208
		f 4 -479 368 375 480
		mu 0 4 172 29 30 174
		mu 1 4 184 41 42 186
		mu 2 4 184 41 42 186
		mu 3 4 187 44 45 189
		mu 4 4 174 31 32 176
		mu 5 4 172 29 30 174
		mu 6 4 172 29 30 174
		mu 7 4 172 29 30 174
		mu 8 4 180 37 38 182
		mu 9 4 183 29 30 185
		mu 10 4 174 31 32 176
		mu 11 4 182 39 40 184
		mu 12 4 182 39 40 184
		mu 13 4 186 43 44 188
		mu 14 4 182 39 40 184
		mu 15 4 186 43 44 188
		mu 16 4 186 43 44 188
		mu 17 4 170 27 28 172
		mu 18 4 170 27 28 172
		mu 19 4 170 27 28 172
		mu 20 4 170 27 28 172
		mu 21 4 175 32 33 177
		mu 22 4 162 19 20 164
		mu 23 4 162 19 20 164
		mu 24 4 162 19 20 164
		mu 25 4 162 19 20 164
		mu 26 4 162 19 20 164
		mu 27 4 162 19 20 164
		mu 28 4 162 19 20 164
		mu 29 4 162 19 20 164
		mu 30 4 162 19 20 164
		mu 31 4 174 31 32 176
		mu 32 4 174 31 32 176
		mu 33 4 178 31 32 180
		mu 34 4 178 31 32 180
		f 4 -495 -498 499 500
		mu 0 4 182 181 184 185
		mu 1 4 194 193 196 197
		mu 2 4 194 193 196 197
		mu 3 4 197 196 199 200
		mu 4 4 184 183 186 187
		mu 5 4 182 181 184 185
		mu 6 4 182 181 184 185
		mu 7 4 182 181 184 185
		mu 8 4 190 189 192 193
		mu 9 4 193 192 195 196
		mu 10 4 184 183 186 187
		mu 11 4 192 191 194 195
		mu 12 4 192 191 194 195
		mu 13 4 196 195 198 199
		mu 14 4 192 191 194 195
		mu 15 4 196 195 198 199
		mu 16 4 196 195 198 199
		mu 17 4 180 179 182 183
		mu 18 4 180 179 182 183
		mu 19 4 180 179 182 183
		mu 20 4 180 179 182 183
		mu 21 4 185 184 187 188
		mu 22 4 172 171 174 175
		mu 23 4 172 171 174 175
		mu 24 4 172 171 174 175
		mu 25 4 172 171 174 175
		mu 26 4 172 171 174 175
		mu 27 4 172 171 174 175
		mu 28 4 172 171 174 175
		mu 29 4 172 171 174 175
		mu 30 4 172 171 174 175
		mu 31 4 184 183 186 187
		mu 32 4 184 183 186 187
		mu 33 4 188 187 190 191
		mu 34 4 188 187 190 191
		f 4 -480 -482 -377 -374
		mu 0 4 32 173 175 35
		mu 1 4 44 185 187 47
		mu 2 4 44 185 187 47
		mu 3 4 47 188 190 50
		mu 4 4 34 175 177 37
		mu 5 4 32 173 175 35
		mu 6 4 32 173 175 35
		mu 7 4 32 173 175 35
		mu 8 4 40 181 183 43
		mu 9 4 32 184 186 35
		mu 10 4 34 175 177 37
		mu 11 4 42 183 185 45
		mu 12 4 42 183 185 45
		mu 13 4 46 187 189 49
		mu 14 4 42 183 185 45
		mu 15 4 46 187 189 49
		mu 16 4 46 187 189 49
		mu 17 4 30 171 173 33
		mu 18 4 30 171 173 33
		mu 19 4 30 171 173 33
		mu 20 4 30 171 173 33
		mu 21 4 35 176 178 38
		mu 22 4 22 163 165 25
		mu 23 4 22 163 165 25
		mu 24 4 22 163 165 25
		mu 25 4 22 163 165 25
		mu 26 4 22 163 165 25
		mu 27 4 22 163 165 25
		mu 28 4 22 163 165 25
		mu 29 4 22 163 165 25
		mu 30 4 22 163 165 25
		mu 31 4 34 175 177 37
		mu 32 4 34 175 177 37
		mu 33 4 34 179 181 37
		mu 34 4 34 179 181 37
		f 4 -483 -376 383 376
		mu 0 4 175 174 30 35
		mu 1 4 187 186 42 47
		mu 2 4 187 186 42 47
		mu 3 4 190 189 45 50
		mu 4 4 177 176 32 37
		mu 5 4 175 174 30 35
		mu 6 4 175 174 30 35
		mu 7 4 175 174 30 35
		mu 8 4 183 182 38 43
		mu 9 4 186 185 30 35
		mu 10 4 177 176 32 37
		mu 11 4 185 184 40 45
		mu 12 4 185 184 40 45
		mu 13 4 189 188 44 49
		mu 14 4 185 184 40 45
		mu 15 4 189 188 44 49
		mu 16 4 189 188 44 49
		mu 17 4 173 172 28 33
		mu 18 4 173 172 28 33
		mu 19 4 173 172 28 33
		mu 20 4 173 172 28 33
		mu 21 4 178 177 33 38
		mu 22 4 165 164 20 25
		mu 23 4 165 164 20 25
		mu 24 4 165 164 20 25
		mu 25 4 165 164 20 25
		mu 26 4 165 164 20 25
		mu 27 4 165 164 20 25
		mu 28 4 165 164 20 25
		mu 29 4 165 164 20 25
		mu 30 4 165 164 20 25
		mu 31 4 177 176 32 37
		mu 32 4 177 176 32 37
		mu 33 4 181 180 32 37
		mu 34 4 181 180 32 37
		f 4 -368 483 485 -485
		mu 0 4 17 16 177 176
		mu 1 4 29 28 189 188
		mu 2 4 29 28 189 188
		mu 3 4 32 31 192 191
		mu 4 4 19 18 179 178
		mu 5 4 17 16 177 176
		mu 6 4 17 16 177 176
		mu 7 4 17 16 177 176
		mu 8 4 25 24 185 184
		mu 9 4 17 16 188 187
		mu 10 4 19 18 179 178
		mu 11 4 27 26 187 186
		mu 12 4 27 26 187 186
		mu 13 4 31 30 191 190
		mu 14 4 27 26 187 186
		mu 15 4 31 30 191 190
		mu 16 4 31 30 191 190
		mu 17 4 15 14 175 174
		mu 18 4 15 14 175 174
		mu 19 4 15 14 175 174
		mu 20 4 15 14 175 174
		mu 21 4 20 19 180 179
		mu 22 4 7 6 167 166
		mu 23 4 7 6 167 166
		mu 24 4 7 6 167 166
		mu 25 4 7 6 167 166
		mu 26 4 7 6 167 166
		mu 27 4 7 6 167 166
		mu 28 4 7 6 167 166
		mu 29 4 7 6 167 166
		mu 30 4 7 6 167 166
		mu 31 4 19 18 179 178
		mu 32 4 19 18 179 178
		mu 33 4 19 18 183 182
		mu 34 4 19 18 183 182
		f 4 372 488 -490 -487
		mu 0 4 24 23 179 178
		mu 1 4 36 35 191 190
		mu 2 4 36 35 191 190
		mu 3 4 39 38 194 193
		mu 4 4 26 25 181 180
		mu 5 4 24 23 179 178
		mu 6 4 24 23 179 178
		mu 7 4 24 23 179 178
		mu 8 4 32 31 187 186
		mu 9 4 24 23 190 189
		mu 10 4 26 25 181 180
		mu 11 4 34 33 189 188
		mu 12 4 34 33 189 188
		mu 13 4 38 37 193 192
		mu 14 4 34 33 189 188
		mu 15 4 38 37 193 192
		mu 16 4 38 37 193 192
		mu 17 4 22 21 177 176
		mu 18 4 22 21 177 176
		mu 19 4 22 21 177 176
		mu 20 4 22 21 177 176
		mu 21 4 27 26 182 181
		mu 22 4 14 13 169 168
		mu 23 4 14 13 169 168
		mu 24 4 14 13 169 168
		mu 25 4 14 13 169 168
		mu 26 4 14 13 169 168
		mu 27 4 14 13 169 168
		mu 28 4 14 13 169 168
		mu 29 4 14 13 169 168
		mu 30 4 14 13 169 168
		mu 31 4 26 25 181 180
		mu 32 4 26 25 181 180
		mu 33 4 26 25 185 184
		mu 34 4 26 25 185 184
		f 4 -383 484 490 -489
		mu 0 4 23 17 176 179
		mu 1 4 35 29 188 191
		mu 2 4 35 29 188 191
		mu 3 4 38 32 191 194
		mu 4 4 25 19 178 181
		mu 5 4 23 17 176 179
		mu 6 4 23 17 176 179
		mu 7 4 23 17 176 179
		mu 8 4 31 25 184 187
		mu 9 4 23 17 187 190
		mu 10 4 25 19 178 181
		mu 11 4 33 27 186 189
		mu 12 4 33 27 186 189
		mu 13 4 37 31 190 193
		mu 14 4 33 27 186 189
		mu 15 4 37 31 190 193
		mu 16 4 37 31 190 193
		mu 17 4 21 15 174 177
		mu 18 4 21 15 174 177
		mu 19 4 21 15 174 177
		mu 20 4 21 15 174 177
		mu 21 4 26 20 179 182
		mu 22 4 13 7 166 169
		mu 23 4 13 7 166 169
		mu 24 4 13 7 166 169
		mu 25 4 13 7 166 169
		mu 26 4 13 7 166 169
		mu 27 4 13 7 166 169
		mu 28 4 13 7 166 169
		mu 29 4 13 7 166 169
		mu 30 4 13 7 166 169
		mu 31 4 25 19 178 181
		mu 32 4 25 19 178 181
		mu 33 4 25 19 182 185
		mu 34 4 25 19 182 185
		f 4 -477 491 492 -484
		mu 0 4 31 172 181 180
		mu 1 4 43 184 193 192
		mu 2 4 43 184 193 192
		mu 3 4 46 187 196 195
		mu 4 4 33 174 183 182
		mu 5 4 31 172 181 180
		mu 6 4 31 172 181 180
		mu 7 4 31 172 181 180
		mu 8 4 39 180 189 188
		mu 9 4 31 183 192 191
		mu 10 4 33 174 183 182
		mu 11 4 41 182 191 190
		mu 12 4 41 182 191 190
		mu 13 4 45 186 195 194
		mu 14 4 41 182 191 190
		mu 15 4 45 186 195 194
		mu 16 4 45 186 195 194
		mu 17 4 29 170 179 178
		mu 18 4 29 170 179 178
		mu 19 4 29 170 179 178
		mu 20 4 29 170 179 178
		mu 21 4 34 175 184 183
		mu 22 4 21 162 171 170
		mu 23 4 21 162 171 170
		mu 24 4 21 162 171 170
		mu 25 4 21 162 171 170
		mu 26 4 21 162 171 170
		mu 27 4 21 162 171 170
		mu 28 4 21 162 171 170
		mu 29 4 21 162 171 170
		mu 30 4 21 162 171 170
		mu 31 4 33 174 183 182
		mu 32 4 33 174 183 182
		mu 33 4 33 178 187 186
		mu 34 4 33 178 187 186
		f 4 477 486 -496 -494
		mu 0 4 173 34 183 182
		mu 1 4 185 46 195 194
		mu 2 4 185 46 195 194
		mu 3 4 188 49 198 197
		mu 4 4 175 36 185 184
		mu 5 4 173 34 183 182
		mu 6 4 173 34 183 182
		mu 7 4 173 34 183 182
		mu 8 4 181 42 191 190
		mu 9 4 184 34 194 193
		mu 10 4 175 36 185 184
		mu 11 4 183 44 193 192
		mu 12 4 183 44 193 192
		mu 13 4 187 48 197 196
		mu 14 4 183 44 193 192
		mu 15 4 187 48 197 196
		mu 16 4 187 48 197 196
		mu 17 4 171 32 181 180
		mu 18 4 171 32 181 180
		mu 19 4 171 32 181 180
		mu 20 4 171 32 181 180
		mu 21 4 176 37 186 185
		mu 22 4 163 24 173 172
		mu 23 4 163 24 173 172
		mu 24 4 163 24 173 172
		mu 25 4 163 24 173 172
		mu 26 4 163 24 173 172
		mu 27 4 163 24 173 172
		mu 28 4 163 24 173 172
		mu 29 4 163 24 173 172
		mu 30 4 163 24 173 172
		mu 31 4 175 36 185 184
		mu 32 4 175 36 185 184
		mu 33 4 179 36 189 188
		mu 34 4 179 36 189 188
		f 4 -481 496 497 -492
		mu 0 4 172 174 184 181
		mu 1 4 184 186 196 193
		mu 2 4 184 186 196 193
		mu 3 4 187 189 199 196
		mu 4 4 174 176 186 183
		mu 5 4 172 174 184 181
		mu 6 4 172 174 184 181
		mu 7 4 172 174 184 181
		mu 8 4 180 182 192 189
		mu 9 4 183 185 195 192
		mu 10 4 174 176 186 183
		mu 11 4 182 184 194 191
		mu 12 4 182 184 194 191
		mu 13 4 186 188 198 195
		mu 14 4 182 184 194 191
		mu 15 4 186 188 198 195
		mu 16 4 186 188 198 195
		mu 17 4 170 172 182 179
		mu 18 4 170 172 182 179
		mu 19 4 170 172 182 179
		mu 20 4 170 172 182 179
		mu 21 4 175 177 187 184
		mu 22 4 162 164 174 171
		mu 23 4 162 164 174 171
		mu 24 4 162 164 174 171
		mu 25 4 162 164 174 171
		mu 26 4 162 164 174 171
		mu 27 4 162 164 174 171
		mu 28 4 162 164 174 171
		mu 29 4 162 164 174 171
		mu 30 4 162 164 174 171
		mu 31 4 174 176 186 183
		mu 32 4 174 176 186 183
		mu 33 4 178 180 190 187
		mu 34 4 178 180 190 187
		f 4 482 498 -500 -497
		mu 0 4 174 175 185 184
		mu 1 4 186 187 197 196
		mu 2 4 186 187 197 196
		mu 3 4 189 190 200 199
		mu 4 4 176 177 187 186
		mu 5 4 174 175 185 184
		mu 6 4 174 175 185 184
		mu 7 4 174 175 185 184
		mu 8 4 182 183 193 192
		mu 9 4 185 186 196 195
		mu 10 4 176 177 187 186
		mu 11 4 184 185 195 194
		mu 12 4 184 185 195 194
		mu 13 4 188 189 199 198
		mu 14 4 184 185 195 194
		mu 15 4 188 189 199 198
		mu 16 4 188 189 199 198
		mu 17 4 172 173 183 182
		mu 18 4 172 173 183 182
		mu 19 4 172 173 183 182
		mu 20 4 172 173 183 182
		mu 21 4 177 178 188 187
		mu 22 4 164 165 175 174
		mu 23 4 164 165 175 174
		mu 24 4 164 165 175 174
		mu 25 4 164 165 175 174
		mu 26 4 164 165 175 174
		mu 27 4 164 165 175 174
		mu 28 4 164 165 175 174
		mu 29 4 164 165 175 174
		mu 30 4 164 165 175 174
		mu 31 4 176 177 187 186
		mu 32 4 176 177 187 186
		mu 33 4 180 181 191 190
		mu 34 4 180 181 191 190
		f 4 481 493 -501 -499
		mu 0 4 175 173 182 185
		mu 1 4 187 185 194 197
		mu 2 4 187 185 194 197
		mu 3 4 190 188 197 200
		mu 4 4 177 175 184 187
		mu 5 4 175 173 182 185
		mu 6 4 175 173 182 185
		mu 7 4 175 173 182 185
		mu 8 4 183 181 190 193
		mu 9 4 186 184 193 196
		mu 10 4 177 175 184 187
		mu 11 4 185 183 192 195
		mu 12 4 185 183 192 195
		mu 13 4 189 187 196 199
		mu 14 4 185 183 192 195
		mu 15 4 189 187 196 199
		mu 16 4 189 187 196 199
		mu 17 4 173 171 180 183
		mu 18 4 173 171 180 183
		mu 19 4 173 171 180 183
		mu 20 4 173 171 180 183
		mu 21 4 178 176 185 188
		mu 22 4 165 163 172 175
		mu 23 4 165 163 172 175
		mu 24 4 165 163 172 175
		mu 25 4 165 163 172 175
		mu 26 4 165 163 172 175
		mu 27 4 165 163 172 175
		mu 28 4 165 163 172 175
		mu 29 4 165 163 172 175
		mu 30 4 165 163 172 175
		mu 31 4 177 175 184 187
		mu 32 4 177 175 184 187
		mu 33 4 181 179 188 191
		mu 34 4 181 179 188 191
		f 4 463 511 461 462
		mu 34 4 202 203 204 205
		f 4 456 512 466 -512
		mu 9 4 209 210 211 175
		f 4 469 513 460 -513
		mu 9 4 212 213 214 215
		f 4 457 458 459 -514
		mu 33 4 202 203 204 205
		f 4 -511 514 -509 -510
		mu 33 4 175 174 177 176
		f 4 -502 515 -508 -515
		mu 9 4 173 182 179 174
		f 4 -503 516 -507 -516
		mu 9 4 182 172 216 179
		f 4 -504 -505 -506 -517
		mu 34 4 174 177 176 175
		f 4 169 166 -522 -166
		mu 31 4 0 1 190 189
		f 4 170 167 -523 -167
		mu 31 4 1 4 191 190
		f 4 171 168 -524 -168
		mu 31 4 4 6 192 191
		f 4 172 165 -525 -169
		mu 31 4 6 8 188 192
		f 4 176 -530 -174 -181
		mu 32 4 7 190 189 9
		f 4 175 -531 -177 -180
		mu 32 4 5 191 190 7
		f 4 174 -532 -176 -179
		mu 32 4 3 192 191 5
		f 4 173 -533 -175 -178
		mu 32 4 0 188 192 3
		f 4 -358 352 -540 -361
		mu 11 4 9 17 198 197
		f 4 -541 -353 377 378
		mu 0 4 187 186 37 38
		mu 1 4 199 198 49 50
		mu 2 4 199 198 49 50
		mu 3 4 202 201 52 53
		mu 4 4 189 188 39 40
		mu 5 4 187 186 37 38
		mu 6 4 187 186 37 38
		mu 7 4 187 186 37 38
		mu 8 4 195 194 45 46
		mu 9 4 198 197 37 38
		mu 10 4 189 188 39 40
		mu 11 4 200 199 47 48
		mu 12 4 197 196 47 48
		mu 13 4 201 200 51 52
		mu 14 4 197 196 47 48
		mu 15 4 201 200 51 52
		mu 16 4 201 200 51 52
		mu 17 4 185 184 35 36
		mu 18 4 185 184 35 36
		mu 19 4 185 184 35 36
		mu 20 4 185 184 35 36
		mu 21 4 190 189 40 41
		mu 22 4 177 176 27 28
		mu 23 4 177 176 27 28
		mu 24 4 177 176 27 28
		mu 25 4 177 176 27 28
		mu 26 4 177 176 27 28
		mu 27 4 177 176 27 28
		mu 28 4 177 176 27 28
		mu 29 4 177 176 27 28
		mu 30 4 177 176 27 28
		mu 31 4 194 193 39 40
		mu 32 4 194 193 39 40
		mu 33 4 193 192 39 40
		mu 34 4 193 192 39 40
		f 4 -382 380 -542 -379
		mu 0 4 38 41 188 187
		mu 1 4 50 53 200 199
		mu 2 4 50 53 200 199
		mu 3 4 53 56 203 202
		mu 4 4 40 43 190 189
		mu 5 4 38 41 188 187
		mu 6 4 38 41 188 187
		mu 7 4 38 41 188 187
		mu 8 4 46 49 196 195
		mu 9 4 38 41 199 198
		mu 10 4 40 43 190 189
		mu 11 4 48 51 201 200
		mu 12 4 48 51 198 197
		mu 13 4 52 55 202 201
		mu 14 4 48 51 198 197
		mu 15 4 52 55 202 201
		mu 16 4 52 55 202 201
		mu 17 4 36 39 186 185
		mu 18 4 36 39 186 185
		mu 19 4 36 39 186 185
		mu 20 4 36 39 186 185
		mu 21 4 41 44 191 190
		mu 22 4 28 31 178 177
		mu 23 4 28 31 178 177
		mu 24 4 28 31 178 177
		mu 25 4 28 31 178 177
		mu 26 4 28 31 178 177
		mu 27 4 28 31 178 177
		mu 28 4 28 31 178 177
		mu 29 4 28 31 178 177
		mu 30 4 28 31 178 177
		mu 31 4 40 43 195 194
		mu 32 4 40 43 195 194
		mu 33 4 40 43 194 193
		mu 34 4 40 43 194 193
		f 4 353 -543 -381 -380
		mu 0 4 39 189 188 41
		mu 1 4 51 201 200 53
		mu 2 4 51 201 200 53
		mu 3 4 54 204 203 56
		mu 4 4 41 191 190 43
		mu 5 4 39 189 188 41
		mu 6 4 39 189 188 41
		mu 7 4 39 189 188 41
		mu 8 4 47 197 196 49
		mu 9 4 39 200 199 41
		mu 10 4 41 191 190 43
		mu 11 4 49 202 201 51
		mu 12 4 49 199 198 51
		mu 13 4 53 203 202 55
		mu 14 4 49 199 198 51
		mu 15 4 53 203 202 55
		mu 16 4 53 203 202 55
		mu 17 4 37 187 186 39
		mu 18 4 37 187 186 39
		mu 19 4 37 187 186 39
		mu 20 4 37 187 186 39
		mu 21 4 42 192 191 44
		mu 22 4 29 179 178 31
		mu 23 4 29 179 178 31
		mu 24 4 29 179 178 31
		mu 25 4 29 179 178 31
		mu 26 4 29 179 178 31
		mu 27 4 29 179 178 31
		mu 28 4 29 179 178 31
		mu 29 4 29 179 178 31
		mu 30 4 29 179 178 31
		mu 31 4 41 196 195 43
		mu 32 4 41 196 195 43
		mu 33 4 41 195 194 43
		mu 34 4 41 195 194 43
		f 4 -356 -539 -544 -354
		mu 21 4 9 10 195 193
		f 4 277 -546 -557 -274
		mu 21 4 4 2 197 196
		f 4 -558 545 46 -547
		mu 7 4 191 190 1 2
		f 4 -559 546 48 -548
		mu 7 4 192 191 2 4
		f 4 -560 547 50 -549
		mu 7 4 193 192 4 6
		f 4 -561 548 52 -550
		mu 7 4 194 193 6 8
		f 4 -562 549 42 54
		f 4 57 -563 -55 -54
		mu 6 4 7 191 190 9
		f 4 56 -564 -58 -52
		mu 6 4 5 192 191 7
		f 4 55 -565 -57 -50
		mu 6 4 3 193 192 5
		f 4 276 -566 -56 -48
		mu 6 4 0 194 193 3
		f 4 278 274 -567 -277
		mu 11 4 5 8 204 203
		f 4 -568 544 -162 361
		mu 21 4 194 196 5 8
		f 5 360 -569 -275 275 -359
		mu 11 5 14 196 204 8 16;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 35 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[2]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[3]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[4]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[5]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[6]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[7]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[8]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[9]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[10]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[11]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[12]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[13]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[14]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[15]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[16]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[17]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[18]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[19]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[20]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[21]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[22]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[23]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[24]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[25]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[26]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[27]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[28]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[29]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[30]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[31]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[32]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[33]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[34]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Back";
	rename -uid "E468920E-4B54-81C6-9DA4-7F976E6941C3";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 27 -16.000000953674316 ;
	setAttr ".sp" -type "double3" 0 27 -16.000000953674316 ;
createNode mesh -n "Basic_BackShape" -p "Basic_Back";
	rename -uid "BD53FCB8-4B13-2697-956B-55B7F1A251B1";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.71455603837966919 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Basic_BackShapeOrig" -p "Basic_Back";
	rename -uid "8B5F5061-40C9-84B6-138C-C1B7F3203088";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 200 ".uvst[0].uvsp[0:199]" -type "float2" 0.375 0.25 0.69588798
		 0.25 0.375 0.5 0.625 0.42911205 0.625 0.5 0.625 0.5 0.375 0.35805225 0.51694769 0.25
		 0.625 0.39194781 0.48305234 0.5 0.55411196 0.25 0.44588798 0.5 0.80411208 0.25 0.30411199
		 0.25 0.19588797 0.25 0.44588798 0.25 0.625 0.32088798 0.55411202 0.5 0.375 0.42911205
		 0.375 0.32088798 0.51694775 0.75 0.625 0.89194775 0.48305228 1 0.375 0.85805219 0.48305231
		 0.25 0.625 0.35805225 0.51694769 0.5 0.375 0.39194781 0.48305231 0.75 0.51694769
		 0.75 0.625 0.85805219 0.625 0.89194775 0.51694769 1 0.48305243 1 0.375 0.89194769
		 0.375 0.85805219 0.48305231 0.75 0.55213338 0.75 0.625 0.85805213 0.625 0.92713344
		 0.51694775 1 0.44786662 1 0.375 0.89194775 0.375 0.82286656 0.44786662 0.75 0.625
		 0.82286656 0.55213338 1 0.375 0.92713344 0.44588798 0.083333239 0.55411202 0.083333246
		 0.69588792 0.083333239 0.625 0.66666675 0.80411208 0.083333246 0.55411196 0.66666675
		 0.44588798 0.66666675 0.375 0.66666675 0.19588797 0.083333239 0.30411202 0.083333246
		 0.44588798 0.16666663 0.55411196 0.16666663 0.69588792 0.16666663 0.625 0.58333337
		 0.80411208 0.16666663 0.55411196 0.58333337 0.44588798 0.58333337 0.375 0.58333337
		 0.19588797 0.16666663 0.30411202 0.16666663 0.30411202 0.083333246 0.44588798 0.083333239
		 0.44588798 0.16666663 0.30411202 0.16666663 0.44588798 0.083333239 0.44588798 0.16666663
		 0.44588798 0.16666663 0.44588798 0.083333239 0.44786659 0.75 0.55213338 0.75 0.375
		 0.92713344 0.375 0.82286656 0.625 0.82286656 0.625 0.92713344 0.55213338 1 0.44786662
		 1 0.40624684 0.84375316 0.5312469 0.7812469 0.59375316 0.90624678 0.4687531 0.9687531
		 0.46875313 0.7812469 0.46875313 0.7812469 0.40624687 0.9062469 0.4062469 0.9062469
		 0.59375316 0.84375316 0.59375316 0.8437531 0.53124684 0.9687531 0.53124684 0.9687531
		 0.5312469 0.7812469 0.40624684 0.84375316 0.59375316 0.90624684 0.46875307 0.9687531
		 0.375 0.35805225 0.48305231 0.25 0.51694769 0.25 0.625 0.35805225 0.625 0.39194781
		 0.51694769 0.5 0.48305234 0.5 0.375 0.39194781 0.375 0.89194769 0.48305228 1 0.44588798
		 0.083333239 0.30411202 0.083333246 0.30411202 0.16666663 0.44588798 0.16666663 0.44588798
		 0.25 0.30411199 0.25 0.375 0.25 0.51694769 1 0.625 0.89194775 0.69588792 0.083333239
		 0.55411202 0.083333246 0.55411196 0.16666663 0.69588792 0.16666663 0.69588798 0.25
		 0.55411196 0.25 0.44588798 0.5 0.375 0.5 0.44588798 0.58333337 0.375 0.58333337 0.625
		 0.42911205 0.625 0.5 0.55411202 0.5 0.625 0.5 0.625 0.58333337 0.55411196 0.58333337
		 0.375 0.85805219 0.48305231 0.75 0.48305231 0.75 0.375 0.85805219 0.51694775 0.75
		 0.625 0.85805219 0.625 0.85805213 0.51694769 0.75 0.51694775 1 0.625 0.89194775 0.375
		 0.89194775 0.48305243 1 0.44786662 0.75 0.375 0.82286656 0.625 0.82286656 0.55213338
		 0.75 0.55213338 1 0.625 0.92713344 0.375 0.92713344 0.44786662 1 0.44786659 0.75
		 0.375 0.82286656 0.625 0.82286656 0.55213338 0.75 0.55213338 1 0.625 0.92713344 0.375
		 0.92713344 0.44786662 1 0.46875313 0.7812469 0.40624684 0.84375316 0.59375316 0.84375316
		 0.5312469 0.7812469 0.53124684 0.9687531 0.59375316 0.90624684 0.40624687 0.9062469
		 0.46875307 0.9687531 0.40624684 0.84375316 0.46875313 0.7812469 0.5312469 0.7812469
		 0.59375316 0.8437531 0.59375316 0.90624678 0.53124684 0.9687531 0.4687531 0.9687531
		 0.4062469 0.9062469 0.80411208 0.16666663 0.80411208 0.25 0.19588797 0.16666663 0.19588797
		 0.25 0.625 0.32088798 0.375 0.42911205 0.375 0.32088798 0.80411208 0.083333246 0.55411196
		 0.66666675 0.625 0.66666675 0.44588798 0.66666675 0.375 0.66666675 0.19588797 0.083333239
		 0.44588798 0.083333239 0.30411202 0.083333246 0.44588798 0.083333239 0.44588798 0.16666663
		 0.44588798 0.16666663 0.44588798 0.083333239 0.30411202 0.16666663 0.44588798 0.16666663;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 170 ".vt";
	setAttr ".vt[0:165]"  1.96036863 30.16566277 -17.20300865 1.96036839 30.16566277 -14.79699135
		 3.79699063 30.16566277 -12.96036911 6.20300913 30.16566277 -12.96036911 6.20300913 30.16566277 -19.03963089
		 3.79699135 30.16566277 -19.039632797 8.039631844 30.16566277 -14.79699135 8.03963089 30.16566277 -17.20300865
		 3.79699206 23 -17.58038712 6.20301008 23 -17.58038712 6.58038664 23 -17.20301056
		 6.58038759 23 -14.7969923 6.20300865 23 -14.41961288 3.79698992 23 -14.41961288 3.41961336 23 -14.79699039
		 3.41961312 23 -17.20300865 3.41961312 32.10040665 -17.20300865 3.41961336 32.10040665 -14.79699039
		 6.20300865 32.10040665 -14.41961288 3.79698992 32.10040665 -14.41961288 6.58038664 32.10040665 -17.20301056
		 6.58038759 32.10040665 -14.7969923 3.79699206 32.10040665 -17.58038712 6.20301008 32.10040665 -17.58038712
		 3.79699206 21.89959335 -17.58038712 6.20301008 21.89959335 -17.58038712 6.58038664 21.89959335 -17.20301056
		 6.58038759 21.89959335 -14.7969923 6.20300865 21.89959335 -14.41961288 3.79698992 21.89959335 -14.41961288
		 3.41961336 21.89959335 -14.79699039 3.41961312 21.89959335 -17.20300865 3.79699302 21.21913338 -18.92443085
		 6.20301056 21.21913338 -18.92442894 7.92442894 21.21913338 -17.20301056 7.92442989 21.21913338 -14.79699326
		 6.20300865 21.21913338 -13.075572968 3.79698992 21.21913338 -13.075572968 2.075572014 21.21913338 -14.79699039
		 2.075572014 21.21913338 -17.20300865 3.79699326 20.24882317 -18.92443085 6.20301056 20.24882317 -18.92442894
		 7.92442894 20.24882317 -17.20301056 7.92442989 20.24882317 -14.79699326 6.20300865 20.24882317 -13.075572968
		 3.79698992 20.24882317 -13.075572968 2.075572014 20.24882317 -14.79699039 2.075572014 20.24882317 -17.20300865
		 1.96036863 24.60251045 -14.79699135 3.79699063 24.60251045 -12.96036911 6.20300913 24.60251045 -12.96036911
		 8.039631844 24.60251045 -14.79699135 8.03963089 24.60251045 -17.20300865 6.20300913 24.60251045 -19.03963089
		 3.79699135 24.60251045 -19.039632797 1.96036863 24.60251045 -17.20300865 1.96036863 29.39748573 -14.79699135
		 3.79699063 29.39748573 -12.96036911 6.20300913 29.39748573 -12.96036911 8.039631844 29.39748573 -14.79699135
		 8.03963089 29.39748573 -17.20300865 6.20300913 29.39748573 -19.03963089 3.79699135 29.39748573 -19.039632797
		 1.96036863 29.39748573 -17.20300865 0 24.60251045 -17.20300865 0 24.60251045 -14.79699135
		 0 29.39748573 -14.79699135 0 29.39748573 -17.20300865 1.96036863 24.60251045 -13
		 1.96036863 29.39748573 -13 0 29.39748573 -13 0 24.60251045 -13 5.6016264 20.24882317 -17.20313072
		 6.2031312 20.24882317 -16.60162544 6.2031312 22.61545563 -16.60162544 5.6016264 22.61545563 -17.20313072
		 3.79687095 20.24882317 -16.60162544 4.39837551 20.24882317 -17.20313072 4.39837551 22.61545563 -17.20313072
		 3.79687095 22.61545563 -16.60162544 6.20313025 20.24882317 -15.39837551 5.60162592 20.24882317 -14.79687119
		 5.60162592 22.61545563 -14.79687119 6.20313025 22.61545563 -15.39837551 4.39837456 20.24882317 -14.79687023
		 3.79686999 20.24882317 -15.39837551 3.79686999 22.61545563 -15.39837551 4.39837456 22.61545563 -14.79687023
		 -1.96036863 30.16566277 -17.20300865 -1.96036816 30.16566277 -14.79699135 -3.79699039 30.16566277 -12.96036911
		 -6.20300865 30.16566277 -12.96036911 -6.20300865 30.16566277 -19.03963089 -3.79699135 30.16566277 -19.039632797
		 -8.039631844 30.16566277 -14.79699135 -8.03963089 30.16566277 -17.20300865 -3.7969923 23 -17.58038712
		 -6.20301056 23 -17.58038712 -6.58038712 23 -17.20301056 -6.58038712 23 -14.7969923
		 -6.20300865 23 -14.41961288 -3.79698944 23 -14.41961288 -3.41961288 23 -14.79699039
		 -3.41961288 23 -17.20300865 -3.41961288 32.10040665 -17.20300865 -3.41961288 32.10040665 -14.79699039
		 -6.20300865 32.10040665 -14.41961288 -3.79698944 32.10040665 -14.41961288 -6.58038712 32.10040665 -17.20301056
		 -6.58038712 32.10040665 -14.7969923 -3.7969923 32.10040665 -17.58038712 -6.20301056 32.10040665 -17.58038712
		 -3.7969923 21.89959335 -17.58038712 -6.20301056 21.89959335 -17.58038712 -6.58038712 21.89959335 -17.20301056
		 -6.58038712 21.89959335 -14.7969923 -6.20300865 21.89959335 -14.41961288 -3.79698944 21.89959335 -14.41961288
		 -3.41961288 21.89959335 -14.79699039 -3.41961288 21.89959335 -17.20300865 -3.79699326 21.21913338 -18.92443085
		 -6.20301056 21.21913338 -18.92442894 -7.92442894 21.21913338 -17.20301056 -7.92442989 21.21913338 -14.79699326
		 -6.20300865 21.21913338 -13.075572968 -3.79698944 21.21913338 -13.075572968 -2.075572014 21.21913338 -14.79699039
		 -2.075572014 21.21913338 -17.20300865 -3.79699326 20.24882317 -18.92443085 -6.20301056 20.24882317 -18.92442894
		 -7.92442894 20.24882317 -17.20301056 -7.92442989 20.24882317 -14.79699326 -6.20300865 20.24882317 -13.075572968
		 -3.79698944 20.24882317 -13.075572968 -2.075572014 20.24882317 -14.79699039 -2.075572014 20.24882317 -17.20300865
		 -1.96036863 24.60251045 -14.79699135 -3.79699039 24.60251045 -12.96036911 -6.20300865 24.60251045 -12.96036911
		 -8.039631844 24.60251045 -14.79699135 -8.03963089 24.60251045 -17.20300865 -6.20300865 24.60251045 -19.03963089
		 -3.79699135 24.60251045 -19.039632797 -1.96036863 24.60251045 -17.20300865 -1.96036863 29.39748573 -14.79699135
		 -3.79699039 29.39748573 -12.96036911 -6.20300865 29.39748573 -12.96036911 -8.039631844 29.39748573 -14.79699135
		 -8.03963089 29.39748573 -17.20300865 -6.20300865 29.39748573 -19.03963089 -3.79699135 29.39748573 -19.039632797
		 -1.96036863 29.39748573 -17.20300865 -1.96036863 24.60251045 -13 -1.96036863 29.39748573 -13
		 -5.6016264 20.24882317 -17.20313072 -6.20313072 20.24882317 -16.60162544 -6.20313072 22.61545563 -16.60162544
		 -5.6016264 22.61545563 -17.20313072 -3.79687119 20.24882317 -16.60162544 -4.39837551 20.24882317 -17.20313072
		 -4.39837551 22.61545563 -17.20313072 -3.79687119 22.61545563 -16.60162544 -6.20313072 20.24882317 -15.39837551
		 -5.60162544 20.24882317 -14.79687119 -5.60162544 22.61545563 -14.79687119 -6.20313072 22.61545563 -15.39837551;
	setAttr ".vt[166:169]" -4.39837456 20.24882317 -14.79687023 -3.79687023 20.24882317 -15.39837551
		 -3.79687023 22.61545563 -15.39837551 -4.39837456 22.61545563 -14.79687023;
	setAttr -s 328 ".ed";
	setAttr ".ed[0:165]"  16 22 0 17 19 0 17 16 0 18 21 0 19 18 0 21 20 0 23 20 0
		 23 22 0 15 14 0 14 48 0 1 0 0 0 63 0 1 17 0 13 12 0 12 50 0 3 2 0 2 57 0 3 18 0 4 5 0
		 5 22 0 4 61 0 9 8 0 8 54 0 6 7 0 7 20 0 6 59 0 11 10 0 10 52 0 16 0 0 19 2 0 21 6 0
		 23 4 0 9 25 0 25 24 0 24 8 0 11 27 0 27 26 0 26 10 0 13 29 0 29 28 0 28 12 0 15 31 0
		 31 30 0 30 14 0 25 33 1 33 32 0 32 24 1 27 35 1 35 34 0 34 26 1 29 37 1 37 36 0 36 28 1
		 31 39 1 39 38 0 38 30 1 33 41 0 41 40 0 40 32 0 35 43 0 43 42 0 42 34 0 37 45 0 45 44 0
		 44 36 0 39 47 0 47 46 0 46 38 0 41 72 1 43 80 1 45 84 1 47 76 1 2 1 0 4 7 0 6 3 0
		 0 5 0 10 9 0 12 11 0 14 13 0 8 15 0 26 25 0 28 27 0 30 29 0 24 31 0 34 33 0 36 35 0
		 38 37 0 32 39 0 42 41 0 44 43 0 46 45 0 40 47 0 48 56 0 49 13 0 50 58 0 51 11 0 52 60 0
		 53 9 0 54 62 0 55 15 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 48 0
		 56 1 0 57 49 0 58 3 0 59 51 0 60 7 0 61 53 0 62 5 0 63 55 0 56 57 1 57 58 1 58 59 1
		 59 60 1 60 61 1 61 62 1 62 63 1 63 56 0 55 64 0 48 65 1 64 65 1 56 66 1 63 67 0 67 66 1
		 67 64 1 48 68 0 56 69 0 68 69 0 66 70 1 69 70 0 65 71 1 71 70 1 68 71 0 72 77 0 73 42 1
		 74 83 0 75 78 0 75 74 0 76 85 0 77 40 1 78 79 0 80 73 0 81 44 1 82 87 0 83 82 0 84 81 0
		 85 46 1 86 79 0 87 86 0 72 75 0 74 73 0 76 79 0 78 77 0 80 83 0 82 81 0 84 87 0 86 85 0
		 72 73 0 76 77 0 80 81 0;
	setAttr ".ed[166:327]" 84 85 0 105 104 0 105 107 0 107 106 0 106 109 0 109 108 0
		 111 108 0 111 110 0 104 110 0 103 102 0 102 136 0 143 136 0 143 103 0 151 144 0 144 89 0
		 89 88 0 88 151 0 89 105 0 104 88 0 101 100 0 100 138 0 137 138 0 137 101 0 145 146 1
		 146 91 0 91 90 0 90 145 0 91 106 0 107 90 0 92 93 0 93 110 0 111 92 0 92 149 0 149 150 1
		 150 93 0 94 95 0 95 108 0 109 94 0 94 147 0 147 148 1 148 95 0 97 96 0 97 113 0 113 112 0
		 112 96 0 99 98 0 99 115 0 115 114 0 114 98 0 101 117 0 117 116 0 116 100 0 103 119 0
		 119 118 0 118 102 0 113 121 1 121 120 0 120 112 1 115 123 1 123 122 0 122 114 1 117 125 1
		 125 124 0 124 116 1 119 127 1 127 126 0 126 118 1 121 129 0 129 128 0 128 120 0 123 131 0
		 131 130 0 130 122 0 125 133 0 133 132 0 132 124 0 127 135 0 135 134 0 134 126 0 129 154 1
		 154 159 0 159 128 1 131 162 1 162 155 0 155 130 1 133 166 1 166 163 0 163 132 1 135 158 1
		 158 167 0 167 134 1 160 159 0 154 157 0 157 160 0 156 155 0 162 165 0 156 165 0 164 163 0
		 166 169 0 164 169 0 168 167 0 158 161 0 168 161 0 144 145 1 90 89 0 92 95 0 148 149 1
		 146 147 1 94 91 0 150 151 1 88 93 0 98 97 0 114 113 0 100 99 0 116 115 0 102 101 0
		 118 117 0 96 103 0 112 119 0 122 121 0 124 123 0 126 125 0 120 127 0 130 129 0 132 131 0
		 134 133 0 128 135 0 154 155 0 162 163 0 166 167 0 158 159 0 136 137 0 139 99 0 138 139 0
		 139 140 0 98 140 0 141 97 0 140 141 0 141 142 0 96 142 0 142 143 0 145 137 0 136 144 0
		 138 146 0 147 139 0 140 148 0 149 141 0 142 150 0 151 143 0 136 65 1 143 64 0 152 153 0
		 153 70 0 152 71 0 151 67 0 144 66 1 144 153 0 136 152 0 160 161 0 157 156 0 165 164 0
		 169 168 0;
	setAttr -s 160 -ch 656 ".fc[0:159]" -type "polyFaces" 
		f 8 -3 1 4 3 5 -7 7 -1
		mu 0 8 6 24 7 25 8 26 9 27
		f 4 8 9 -108 99
		mu 0 4 34 22 48 57
		f 4 123 108 10 11
		mu 0 4 67 58 15 13
		f 4 -11 12 2 28
		mu 0 4 0 15 24 6
		f 4 13 14 -102 93
		mu 0 4 32 21 50 49
		f 4 117 110 15 16
		mu 0 4 59 60 1 10
		f 4 -16 17 -5 29
		mu 0 4 10 1 25 7
		f 4 18 19 -8 31
		mu 0 4 11 2 27 9
		f 4 -19 20 121 114
		mu 0 4 2 11 64 65
		f 4 23 24 -6 30
		mu 0 4 3 4 26 8
		f 4 -24 25 119 112
		mu 0 4 17 5 61 63
		f 4 -22 32 33 34
		mu 0 4 23 28 36 35
		f 4 -27 35 36 37
		mu 0 4 20 30 38 29
		f 4 -14 38 39 40
		mu 0 4 21 32 40 31
		f 4 -9 41 42 43
		mu 0 4 22 34 42 33
		f 4 -34 44 45 46
		mu 0 4 35 36 44 43
		f 4 -37 47 48 49
		mu 0 4 29 38 45 37
		f 4 -40 50 51 52
		mu 0 4 31 40 46 39
		f 4 -43 53 54 55
		mu 0 4 33 42 47 41
		f 4 -46 56 57 58
		mu 0 4 43 44 76 79
		f 4 -49 59 60 61
		mu 0 4 37 45 80 77
		f 4 -52 62 63 64
		mu 0 4 39 46 82 81
		f 4 -55 65 66 67
		mu 0 4 41 47 78 83
		f 4 -58 68 139 145
		mu 0 4 79 76 88 97
		f 4 -61 69 147 140
		mu 0 4 77 80 92 96
		f 4 -64 70 151 148
		mu 0 4 81 82 94 98
		f 4 -67 71 144 152
		mu 0 4 83 78 90 99
		f 4 158 -140 155 142
		mu 0 4 84 97 88 89
		f 4 156 -148 159 -142
		mu 0 4 85 96 92 93
		f 4 160 -152 161 -150
		mu 0 4 86 98 94 95
		f 4 162 -145 157 -154
		mu 0 4 87 99 90 91
		f 4 116 -17 72 -109
		mu 0 4 58 59 10 15
		f 4 73 -113 120 -21
		mu 0 4 11 17 63 64
		f 4 118 -26 74 -111
		mu 0 4 60 62 12 1
		f 4 122 -12 75 -115
		mu 0 4 66 67 13 14
		f 4 -13 -73 -30 -2
		mu 0 4 24 15 10 7
		f 4 -18 -75 -31 -4
		mu 0 4 25 16 3 8
		f 4 -25 -74 -32 6
		mu 0 4 26 17 11 9
		f 4 -20 -76 -29 0
		mu 0 4 27 18 19 6
		f 4 -77 -38 80 -33
		mu 0 4 28 20 29 36
		f 4 -78 -41 81 -36
		mu 0 4 30 21 31 38
		f 4 -79 -44 82 -39
		mu 0 4 32 22 33 40
		f 4 -80 -35 83 -42
		mu 0 4 34 23 35 42
		f 4 -81 -50 84 -45
		mu 0 4 36 29 37 44
		f 4 -82 -53 85 -48
		mu 0 4 38 31 39 45
		f 4 -83 -56 86 -51
		mu 0 4 40 33 41 46
		f 4 -84 -47 87 -54
		mu 0 4 42 35 43 47
		f 4 -85 -62 88 -57
		mu 0 4 44 37 77 76
		f 4 -86 -65 89 -60
		mu 0 4 45 39 81 80
		f 4 -87 -68 90 -63
		mu 0 4 46 41 83 82
		f 4 -88 -59 91 -66
		mu 0 4 47 43 79 78
		f 4 -69 -89 -141 -164
		mu 0 4 88 76 77 96
		f 4 -70 -90 -149 -166
		mu 0 4 92 80 81 98
		f 4 -71 -91 -153 -167
		mu 0 4 94 82 83 99
		f 4 -72 -92 -146 -165
		mu 0 4 90 78 79 97
		f 4 -94 -101 -10 78
		mu 0 4 32 49 48 22
		f 4 -96 -103 -15 77
		mu 0 4 30 52 50 21
		f 4 -104 95 26 27
		mu 0 4 53 51 30 20
		f 4 -98 -105 -28 76
		mu 0 4 28 54 53 20
		f 4 -106 97 21 22
		mu 0 4 55 54 28 23
		f 4 -100 -107 -23 79
		mu 0 4 34 57 56 23
		f 4 100 -110 -117 -93
		mu 0 4 48 49 59 58
		f 4 101 94 -118 109
		mu 0 4 49 50 60 59
		f 4 102 -112 -119 -95
		mu 0 4 50 52 62 60
		f 4 -120 111 103 96
		mu 0 4 63 61 51 53
		f 4 -121 -97 104 -114
		mu 0 4 64 63 53 54
		f 4 -122 113 105 98
		mu 0 4 65 64 54 55
		f 4 106 -116 -123 -99
		mu 0 4 56 57 67 66
		f 4 107 125 -127 -125
		mu 0 4 57 48 69 68
		f 4 133 135 -138 -139
		mu 0 4 72 73 74 75
		f 4 -124 128 129 -128
		mu 0 4 58 67 71 70
		f 4 115 124 -131 -129
		mu 0 4 67 57 68 71
		f 4 92 132 -134 -132
		mu 0 4 48 58 73 72
		f 4 127 134 -136 -133
		mu 0 4 58 70 74 73
		f 4 -126 131 138 -137
		mu 0 4 69 48 72 75
		f 8 -147 -143 143 141 150 149 154 153
		mu 0 8 91 84 89 85 93 86 95 87
		f 4 163 -157 -144 -156
		mu 0 4 88 96 85 89
		f 4 164 -159 146 -158
		mu 0 4 90 97 84 91
		f 4 165 -161 -151 -160
		mu 0 4 92 98 86 93
		f 4 166 -163 -155 -162
		mu 0 4 94 99 87 95
		f 8 174 -174 172 -172 -171 -170 -169 167
		mu 0 8 100 107 106 105 104 103 102 101
		f 4 -179 177 -177 -176
		mu 0 4 108 111 110 109
		f 4 -183 -182 -181 -180
		mu 0 4 112 115 114 113
		f 4 -185 -168 -184 181
		mu 0 4 116 100 101 114
		f 4 -189 187 -187 -186
		mu 0 4 117 120 119 118
		f 4 -193 -192 -191 -190
		mu 0 4 121 124 123 122
		f 4 -195 169 -194 191
		mu 0 4 124 102 103 123
		f 4 -198 173 -197 -196
		mu 0 4 125 106 107 126
		f 4 -201 -200 -199 195
		mu 0 4 126 128 127 125
		f 4 -204 171 -203 -202
		mu 0 4 129 104 105 130
		f 4 -207 -206 -205 201
		mu 0 4 131 134 133 132
		f 4 -211 -210 -209 207
		mu 0 4 135 138 137 136
		f 4 -215 -214 -213 211
		mu 0 4 139 142 141 140
		f 4 -218 -217 -216 185
		mu 0 4 118 144 143 117
		f 4 -221 -220 -219 175
		mu 0 4 109 146 145 108
		f 4 -224 -223 -222 209
		mu 0 4 138 148 147 137
		f 4 -227 -226 -225 213
		mu 0 4 142 150 149 141
		f 4 -230 -229 -228 216
		mu 0 4 144 152 151 143
		f 4 -233 -232 -231 219
		mu 0 4 146 154 153 145
		f 4 -236 -235 -234 222
		mu 0 4 148 156 155 147
		f 4 -239 -238 -237 225
		mu 0 4 150 158 157 149
		f 4 -242 -241 -240 228
		mu 0 4 152 160 159 151
		f 4 -245 -244 -243 231
		mu 0 4 154 162 161 153
		f 4 -248 -247 -246 234
		mu 0 4 156 164 163 155
		f 4 -251 -250 -249 237
		mu 0 4 158 166 165 157
		f 4 -254 -253 -252 240
		mu 0 4 160 168 167 159
		f 4 -257 -256 -255 243
		mu 0 4 162 170 169 161
		f 4 -260 -259 246 -258
		mu 0 4 171 172 163 164
		f 4 262 -262 249 -261
		mu 0 4 173 174 165 166
		f 4 265 -265 252 -264
		mu 0 4 175 176 167 168
		f 4 268 -268 255 -267
		mu 0 4 177 178 169 170
		f 4 180 -271 192 -270
		mu 0 4 113 114 124 121
		f 4 198 -273 206 -272
		mu 0 4 125 127 134 131
		f 4 190 -275 204 -274
		mu 0 4 122 123 180 179
		f 4 200 -277 182 -276
		mu 0 4 181 182 115 112
		f 4 168 194 270 183
		mu 0 4 101 102 124 114
		f 4 170 203 274 193
		mu 0 4 103 104 129 183
		f 4 -173 197 271 202
		mu 0 4 105 106 125 131
		f 4 -175 184 276 196
		mu 0 4 107 100 185 184
		f 4 208 -279 214 277
		mu 0 4 136 137 142 139
		f 4 212 -281 217 279
		mu 0 4 140 141 144 118
		f 4 215 -283 220 281
		mu 0 4 117 143 146 109
		f 4 218 -285 210 283
		mu 0 4 108 145 138 135
		f 4 221 -286 226 278
		mu 0 4 137 147 150 142
		f 4 224 -287 229 280
		mu 0 4 141 149 152 144
		f 4 227 -288 232 282
		mu 0 4 143 151 154 146
		f 4 230 -289 223 284
		mu 0 4 145 153 148 138
		f 4 233 -290 238 285
		mu 0 4 147 155 158 150
		f 4 236 -291 241 286
		mu 0 4 149 157 160 152
		f 4 239 -292 244 287
		mu 0 4 151 159 162 154
		f 4 242 -293 235 288
		mu 0 4 153 161 156 148
		f 4 293 250 289 245
		mu 0 4 163 166 158 155
		f 4 294 253 290 248
		mu 0 4 165 168 160 157
		f 4 295 256 291 251
		mu 0 4 167 170 162 159
		f 4 296 247 292 254
		mu 0 4 169 164 156 161
		f 4 -282 176 297 188
		mu 0 4 117 109 110 120
		f 4 -280 186 299 298
		mu 0 4 140 118 119 186
		f 4 -302 -212 -299 300
		mu 0 4 187 139 140 188
		f 4 -278 301 303 302
		mu 0 4 136 139 187 189
		f 4 -306 -208 -303 304
		mu 0 4 190 135 136 189
		f 4 -284 305 306 178
		mu 0 4 108 135 191 111
		f 4 308 269 307 -298
		mu 0 4 110 113 121 120
		f 4 -308 189 -310 -188
		mu 0 4 120 121 122 119
		f 4 309 273 310 -300
		mu 0 4 119 122 179 186
		f 4 -312 -301 -311 205
		mu 0 4 134 187 188 133
		f 4 312 -304 311 272
		mu 0 4 127 189 187 134
		f 4 -314 -305 -313 199
		mu 0 4 128 190 189 127
		f 4 313 275 314 -307
		mu 0 4 191 181 112 111
		f 4 316 126 -316 -178
		mu 0 4 111 193 192 110
		f 4 319 137 -319 -318
		mu 0 4 194 197 196 195
		f 4 321 -130 -321 179
		mu 0 4 113 199 198 112
		f 4 320 130 -317 -315
		mu 0 4 112 198 193 111
		f 4 323 317 -323 -309
		mu 0 4 110 194 195 113
		f 4 322 318 -135 -322
		mu 0 4 113 195 196 199
		f 4 136 -320 -324 315
		mu 0 4 192 197 194 110
		f 8 -269 -328 -266 -327 -263 -326 259 324
		mu 0 8 178 177 176 175 174 173 172 171
		f 4 258 325 260 -294
		mu 0 4 163 172 173 166
		f 4 267 -325 257 -297
		mu 0 4 169 178 171 164
		f 4 261 326 263 -295
		mu 0 4 165 174 175 168
		f 4 264 327 266 -296
		mu 0 4 167 176 177 170;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Right_Arm";
	rename -uid "9206F8AA-4FD9-CF0C-4B15-37A5996AE7EF";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 25.348052978515625 3.559107780456543 ;
	setAttr ".sp" -type "double3" -18 25.348052978515625 3.559107780456543 ;
createNode mesh -n "Basic_Right_ArmShape" -p "|Basic_Right_Arm";
	rename -uid "89D154CB-47E1-F1F2-095F-2B858EF37E5C";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Basic_Right_ArmShapeOrig" -p "|Basic_Right_Arm";
	rename -uid "596C6957-48E5-7097-FA29-1EAC64BFB8DD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 409 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.43749374 0.25 0.43749377
		 3.7252903e-09 0.5625062 3.7252903e-09 0.56250626 0.25 0.43749374 0.75 0.43749377
		 0.5 0.5625062 0.5 0.56250626 0.75 0.68749374 0.25 0.6874938 3.7252903e-09 0.8125062
		 3.7252903e-09 0.81250632 0.25 0.18749376 0.25 0.18749379 3.7252903e-09 0.3125062
		 3.7252903e-09 0.31250626 0.25 0.375 0.31249374 0.625 0.31249374 0.625 0.43750626
		 0.375 0.43750626 0.375 0.81249374 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.40103906 0.44792187 0.40103906 0.30207813 0.42707813
		 0.30207813 0.42707813 0.44792187 0.375 3.7252903e-09 0.375 0.1805625 0.32292187 0.1805625
		 0.32292187 0 0.42707813 0.24132031 0.57292187 0.24132031 0.57292187 0.30207813 0.59896094
		 0.30207813 0.59896094 0.44792187 0.57292187 0.44792187 0.625 3.7252903e-09 0.625
		 0.1805625 0.57292187 0.1805625 0.57292187 0 0.40103906 0.75 0.40103906 0.5694375
		 0.42707813 0.5694375 0.42707813 0.75 0.57292187 0.50867969 0.42707813 0.50867969
		 0.59896094 0.75 0.59896094 0.5694375 0.625 0.5694375 0.625 0.75 0.42707813 7.4505806e-09
		 0.42707813 0.1805625 0.57292187 0.5694375 0.57292187 0.75 0.67707813 7.4505806e-09
		 0.82292187 0 0.82292187 0.1805625 0.67707813 0.1805625 0.17707813 7.4505806e-09 0.17707813
		 0.1805625 0.375 0.24132031 0.40103906 0.24132031 0.59896094 0.24132031 0.625 0.24132031
		 0.4140586 0.47830078 0.58594143 0.47830078 0.625 0.30207813 0.625 0.44792187 0.59896094
		 0.50867969 0.40103906 0.50867969 0.375 0.30207813 0.375 0.44792187 0.375 0.5694375
		 0.375 0.75 0.5 0.25 0.5 0.18749997 0.625 0.18749997 0.625 0.25 0.59375 0.32499999
		 0.59375 0.28749999 0.625 0.28749999 0.625 0.32499999 0.62669051 0.79823244 0.5625
		 0.96249998 0.5625 0.92500001 0.63268542 0.83147663 0.29999998 0.25 0.29999995 0.18749997
		 0.33749998 0.18749997 0.33749998 0.25 0.61875004 0.26875001 0.69441587 0.2877098
		 0.60000002 0.37500006 0.54312503 0.34437501 0.5 0.0625 0.5 0 0.625 0 0.625 0.0625
		 0.50562501 0.46937501 0.5625 0.92500001 0.51875001 0.92499995 0.33749998 0 0.33749998
		 0.0625 0.29999998 0.0625 0.29999998 0 0.5 0.12499993 0.625 0.12499993 0.66250002
		 0.12499994 0.66250002 0.0625 0.70000005 0.0625 0.70000005 0.12499994 0.5625 0.58750004
		 0.58125001 0.48125011 0.66250002 0.58750015 0.33749998 0.12499993 0.29999995 0.12499993
		 0.66250002 0.18749997 0.70000005 0.18749997 0.5 0.92500001 0.5 0.96249998 0.375 0.96249998
		 0.375 0.92500001 0.39999998 0.25 0.48124999 0.41875002 0.39999998 0.25 0.41874999
		 0.26874995 0.5 0.37500003 0.48124999 0.41875002 0.4375 0.28749996 0.51875001 0.33125001
		 0.375 0.32499999 0.5 0.32499999 0.51875001 0.33125001 0.4375 0.28749996 0.5 0.28749999
		 0.375 0.28749999 0.5 0.25 0.5 0.28749999 0.375 0.25 0.64375001 0.25 0.64375001 0.18749997
		 0.65296507 0.19737829 0.66218007 0.20725662 0.65312505 0.12499994 0.66250002 0.12499994
		 0.65296388 0.056829706 0.6621778 0.051159408 0.64375001 0.0625 0.64375001 0 0.5 0.96249998
		 0.5 1 0.375 1 0.37452209 0.18738748 0.63062501 0.25 0.63062501 0.18749997 0.63062501
		 0.12499994 0.64375001 0.12499994 0.63062501 0.0625 0.63062501 0 0.5625 0.96249998
		 0.51875001 0.96249998 0.50562501 0.46937501 0.52437502 0.40687507 0.58125001 0.48125011
		 0.5625 0.58750004 0.54312503 0.34437501 0.60000002 0.37500006 0.51875001 0.32499999
		 0.51875001 0.28749999 0.5625 0.28749999 0.5625 0.32499999 0.5 0.25 0.375 0.25 0.5
		 0.28749999 0.5 0.96249998 0.5 1 0.375 1 0.51875001 0.96249998 0.51875001 0.28749999
		 0.5625 0.28749999 0.5625 0.32499999 0.5 0.22113958 0.5 0.19227916 0.5 0.18749997
		 0.625 0.25 0.5 0 0.625 0 0.5 0.96249998 0.5 0.28749999 0.5 0.25 0.375 0.25 0.375
		 0 0.5 0 0.375 0 0.5 0.25 0.375 0.25 0.5 0 0.375 0 0.5 0.25 0.375 0.25 0.5 0 0.375
		 0 0.69999993 0.050078563 0.70000005 0.12499994 0.5625 0.28749999 0.5625 0.32499999
		 0.62666768 0.28135902 0.625 0.28749999 0.59375 0.28749999 0.62652498 0.32181895 0.625
		 0.32499999 0.59375 0.32499999 0.59375 0.32499999 0.5625 0.28749999 0.5625 0.32499999
		 0.59375 0.28749999 0.625 0.28749999 0.625 0.32499999 0.59375 0.32499999 0.5625 0.28749999
		 0.5625 0.32499999 0.37452209 0.062612481 0.375 0 0.37452209 0.12499993 0.5 0.18272077
		 0.5 0.15624996 0.5 0.12977915 0.5 0.12499993 0.5 0.12022072 0.5 0.09374997 0.5 0.06727922
		 0.5 0.0625 0.5 0.05772078 0.5 0.02886039 0.5 0.19228107 0.5 0.22114053 0.375 0.22114033
		 0.375 0.19228064 0.5 0.1297795 0.5 0.1562496 0.375 0.15624985 0.375 0.12977952 0.5
		 0.06728024 0.5 0.093750291 0.375 0.09375006 0.375 0.067279749;
	setAttr ".uvst[0].uvsp[250:408]" 0.5 0.028859444 0.375 0.028859658 0.375 0.028860366
		 0.375 0.057720732 0.375 0.06727922 0.375 0.09374997 0.375 0.12022072 0.375 0.12977916
		 0.375 0.15624996 0.375 0.18272075 0.375 0.19227916 0.375 0.22113958 0.5 0.19227916
		 0.5 0.22113958 0.375 0.22113958 0.375 0.19227917 0.5 0.12977915 0.5 0.15624994 0.375
		 0.15624994 0.375 0.12977915 0.5 0.06727922 0.5 0.09374997 0.375 0.09374997 0.375
		 0.067279227 0.5 0.02886039 0.375 0.02886039 0.5 0.22113979 0.5 0.19227958 0.375 0.2211397
		 0.375 0.1922794 0.5 0.15624997 0.5 0.1297795 0.375 0.15624996 0.375 0.1297794 0.5
		 0.09374994 0.5 0.067279533 0.375 0.09374997 0.375 0.067279458 0.5 0.02886021 0.375
		 0.028860271 0.5 0.18271969 0.5 0.18272045 0.375 0.18272051 0.37499997 0.1827202 0.5
		 0.12022034 0.5 0.12022036 0.375 0.12022048 0.375 0.12022036 0.49999997 0.057718888
		 0.5 0.057720419 0.375 0.057720542 0.375 0.057719316 0.5 0.05772078 0.375 0.05772078
		 0.5 0.12022071 0.375 0.12022071 0.5 0.18272075 0.375 0.18272075 0.41874999 0.26874995
		 0.65937507 0.25937501 0.375 0.5467304 0.4217304 0.5 0.5782696 0.5 0.625 0.5467304
		 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75 0.375 0.7032696 0.6437481 0.24375807
		 0.54375809 0.30625185 0.38124189 0.26874813 0.31874812 0.41873318 0.35625187 0.75626677
		 0.45624191 0.69377303 0.61875814 0.231227 0.68125188 0.081241898 0.375 0.7032696
		 0.4217304 0.75 0.4217304 0.92500001 0.375 0.92500001 0.36098087 0.79882789 0.375
		 0.92500001 0.45624194 0.69377297 0.35625184 0.75626677 0.57826966 0.75 0.625 0.7032696
		 0.625 0.92500001 0.5782696 0.92500001 0.63925052 0.17290242 0.70000005 0 0.68125188
		 0.081241921 0.61875814 0.231227 0.4217304 0.5 0.375 0.5467304 0.375 0.32499999 0.4217304
		 0.32499999 0.36074951 0.26401913 0.29999998 0.25 0.31874812 0.41873324 0.38124189
		 0.26874813 0.625 0.5467304 0.5782696 0.5 0.57826966 0.32499999 0.625 0.32499999 0.56425053
		 0.31098089 0.54375809 0.30625185 0.64374816 0.24375805 0.70000005 0.2032696 0.70000005
		 0.046730354 0.875 0.046730399 0.875 0.20326962 0.125 0.046730399 0.29999998 0.046730399
		 0.29999998 0.20326963 0.125 0.20326963 0.31401908 0.37617201 0.43574953 0.75209755
		 0.68598092 0.060749516 0.63901913 0.26425049 0.4217304 0.5 0.375 0.5467304 0.5782696
		 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75 0.375 0.7032696
		 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375 0.5467304 0.5782696 0.5 0.5782696
		 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696 0.625 0.7032696 0.57826966 0.75
		 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.7032696 0.375
		 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375 0.5467304 0.5782696 0.5 0.5782696 0.5
		 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696 0.625 0.7032696 0.57826966 0.75 0.57826966
		 0.75 0.4217304 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.7032696;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 37 ".uvst[1].uvsp[0:36]" -type "float2" 0.375 0.81249374
		 0.40624687 0.7812469 0.40624687 0.9687531 0.375 0.93750626 0.31250626 -3.7252903e-09
		 0.375 0 0.43749374 3.7252903e-09 0.43749374 0.25 0.40624687 0.25 0.375 0.25 0.56250626
		 -3.7252903e-09 0.625 -3.7252903e-09 0.68749374 -3.7252903e-09 0.625 0.25 0.5937531
		 0.25 0.56250626 0.25 0.375 0.5 0.40624687 0.5 0.43749374 0.5 0.43749374 0.75 0.56250626
		 0.5 0.5937531 0.5 0.625 0.5 0.625 0.81249374 0.5937531 0.7812469 0.56250626 0.75
		 0.68749374 0.25 0.81250626 -3.7252903e-09 0.81250632 0.25 0.18749374 0.25 0.18749374
		 -3.7252903e-09 0.31250626 0.25 0.5937531 0.9687531 0.56250626 1 0.43749374 1 0.625
		 0.93750626 0.5 0.875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 364 ".vt";
	setAttr ".vt[0:165]"  -21 16 1.50014997 -19.50015068 16 3 -21 28 1.50014997
		 -19.50015068 28 3 -15 16 1.50014997 -16.49984932 16 3 -15 28 1.50014997 -16.49984932 28 3
		 -21 28 -1.50014997 -19.50015068 28 -3 -21 16 -1.50014997 -19.50015068 16 -3 -16.49984932 28 -3
		 -15 28 -1.50014997 -15 16 -1.50014997 -16.49984932 16 -3 -24 37.50025177 3.5002501
		 -21.50024986 37.50025177 6 -21.50024986 40 3.5002501 -14.49975014 40 3.5002501 -14.49975014 37.50025177 6
		 -12 37.50025177 3.5002501 -24 37.50025177 -3.5002501 -21.50024986 40 -3.5002501 -21.50024986 37.50025177 -6
		 -14.49975014 37.50025177 -6 -14.49975014 40 -3.5002501 -12 37.50025177 -3.5002501
		 -21.50024986 31 6 -24 31 3.5002501 -12 31 3.5002501 -14.49975014 31 6 -24 31 -3.5002501
		 -21.50024986 31 -6 -14.49975014 31 -6 -12 31 -3.5002501 -23 28 2.5002501 -20.50024986 28 5
		 -23 31 2.5002501 -20.50024986 31 5 -13 28 2.5002501 -15.49975014 28 5 -15.49975014 31 5
		 -13 31 2.5002501 -20.50024986 31 -5 -23 31 -2.5002501 -23 28 -2.5002501 -20.50024986 28 -5
		 -13 31 -2.5002501 -15.49975014 31 -5 -13 28 -2.5002501 -15.49975014 28 -5 -23.03966713 39.077758789 3.5002501
		 -21.50024986 39.077758789 5.039667606 -14.49975014 39.077758789 5.039667606 -12.96033287 39.077758789 3.5002501
		 -12.96033287 39.077758789 -3.5002501 -14.49975014 39.077758789 -5.03966713 -21.50024986 39.077758789 -5.039667606
		 -23.03966713 39.077758789 -3.5002501 -21.97871399 31 3.9787128 -23.03966713 31 5.039667606
		 -23.03966713 37.50025177 5.039667606 -22.58447266 38.6620903 4.58447266 -14.021286011 31 3.9787128
		 -12.96033287 31 5.039667606 -12.96033287 37.50025177 5.039667606 -13.41552734 38.6620903 4.58447266
		 -21.97871399 31 -3.9787128 -23.03966713 31 -5.039667606 -23.03966713 37.50025177 -5.039667606
		 -22.58447266 38.6620903 -4.58447266 -14.021286011 31 -3.9787128 -12.96033287 31 -5.039667606
		 -12.96033287 37.50025177 -5.039667606 -13.41552734 38.6620903 -4.58447266 -14.021286011 28 -3.9787128
		 -21.97871399 28 3.97871256 -21.97871399 28 -3.97871256 -18 28 0 -14.021286011 28 3.9787128
		 -21.96157646 11 14.73168659 -18 11 11.70988083 -22.13616371 19 14.83248425 -18 19 11.70988083
		 -14 17.1514492 7 -14 12.8485508 7 -14 14.99999619 7 -18 11 13.49056625 -19.49567032 12.0089569092 7
		 -19.49567032 17.99104309 7 -18 19 13.49056625 -14 16.88254547 11 -14 14.99999809 11
		 -14 13.11745453 11 -16 18.71984863 11 -16 17 11 -16 14.99999809 11 -16 13 11 -16 11 11
		 -16 11 7 -15.0089569092 13.50432968 7 -15.0089569092 14.99999809 7 -15.0089569092 16.49567032 7
		 -16 19.09085083 7 -22.67245674 17 7 -22.15194702 19.15194702 7 -22.67245674 17 11
		 -22.21488571 19.21488571 11 -22.21488571 10.78511429 11 -22.67245674 13 11 -22.67245674 13 7
		 -22.15194702 10.84805298 7 -22.80763054 14.99999809 11 -22.80763054 14.99999809 7
		 -19.43179131 10.69610596 7 -18 10.69610596 11.70988083 -18 19.30389404 11.70988083
		 -19.43179131 19.30389404 7 -22.21357727 19.15194702 14.87717915 -18 19.30389404 13.49056625
		 -18 10.69610596 13.49056625 -22.21357727 10.84805298 14.87717915 -16 10.69610596 11
		 -16 10.69610596 7 -16 19.30389404 11 -16 19.30389404 7 -17.38764954 19 13.49056625
		 -17.38764954 17 13.49056625 -17.39999962 17 11.70988083 -17.39999962 19 11.70988083
		 -17.38764954 13 13.49056625 -17.38764954 11 13.49056625 -17.39999962 11 11.70988083
		 -17.39999962 13 11.70988083 -17.38764954 14.99999809 13.49056625 -17.39999962 14.99999809 11.70988083
		 -17.39999962 10.69610596 11.70988083 -17.39999962 10.69610596 7 -16.50432968 12.0089569092 7
		 -16.50432968 17.99104309 7 -17.39999962 19.30389404 7 -17.39999962 19.30389404 11.70988083
		 -17.42310524 19 13.82323742 -17.3177948 19 16.8607235 -17.3177948 11 16.8607235 -17.42310524 11 13.82323742
		 -17.23379326 19 13.54300213 -15.077960968 19 14.41306496 -15.18754578 11 14.44242859
		 -17.23379326 11 13.54300213 -17.27010345 19.018013 12.018017769 -16.33641624 19.043708801 11.85221672
		 -16.26112366 11.045482635 12.24091625 -17.19481087 11.019786835 12.4067173 -13.71835136 14.99999809 11
		 -13.71835136 13.11745453 11 -13.71835136 12.8485508 7 -13.71835136 14.99999619 7
		 -13.71835136 16.88254547 11 -13.71835136 17.1514492 7 -15.62994576 20.67459106 9.92006111
		 -15.90540123 19.17694473 11.18325233 -13.90795135 18.76401329 11.15792656 -13.63249207 20.26165962 9.89473534
		 -15.29007912 20.26345825 13.3777504;
	setAttr ".vt[166:331]" -15.83712196 19.057929993 11.90740204 -13.88194275 18.69046593 11.45662308
		 -13.33489799 19.89599228 12.92696762 -15.20109749 17.91022873 14.16748238 -15.68040085 17.23917007 13.349226
		 -13.96732521 17.035030365 13.098031998 -13.48801804 17.70608711 13.91628647 -22.16117287 13.15293503 14.84692287
		 -21.96157646 12.84706497 14.73168659 -22.67842102 13 15.12247467 -22.34792519 15.15293312 14.95474434
		 -22.16117287 14.84706306 14.84692287 -22.81671143 14.99999619 15.1242733 -22.13616371 17.15293503 14.83248425
		 -22.34792519 16.84706497 14.95474434 -22.67842102 17 15.12247467 -17.3177948 17.15293503 16.8607235
		 -17.3177948 16.84706497 17.26873589 -18 16.84706497 13.49056625 -17.69385529 17 13.49056625
		 -18 17.15293503 13.49056625 -17.42310524 16.84706497 13.82323742 -17.42310524 17.15293503 13.82323742
		 -17.3177948 15.15293312 17.26873589 -17.3177948 14.84706306 17.10641098 -18 14.84706306 13.49056625
		 -17.69385529 14.99999809 13.49056625 -18 15.15293312 13.49056625 -17.42310524 14.84706306 13.82323742
		 -17.42310524 15.15293312 13.82323742 -17.3177948 13.15293503 17.10641098 -17.3177948 12.84706497 16.8607235
		 -18 12.84706497 13.49056625 -17.69385529 13 13.49056625 -18 13.15293503 13.49056625
		 -17.42310524 12.84706497 13.82323742 -17.42310524 13.15293503 13.82323742 -15.077960968 17.15294266 14.41306496
		 -14.79501534 16.84705734 14.33725071 -17.23379326 16.84705544 13.54300213 -17.23379326 17.15294647 13.54300213
		 -14.79501534 15.15294075 14.33725071 -14.95831108 14.84705544 14.38100529 -17.23379326 14.84705162 13.54300213
		 -17.23379326 15.15294456 13.54300213 -14.95831108 13.15294266 14.38100529 -15.18754578 12.84705734 14.44242859
		 -17.23379326 12.84705353 13.54300213 -17.23379326 13.15294456 13.54300213 -16.30164909 17.19750977 11.86960125
		 -16.298769 16.89164352 11.5025835 -17.23533821 17.17182541 12.035404205 -17.23245811 16.86593056 11.66838646
		 -16.2987709 15.19754219 11.50258255 -16.2987709 14.89164925 11.83088589 -17.23246002 15.17184448 11.66838551
		 -17.23246002 14.86595154 11.99668884 -16.2987709 13.19754791 11.83088589 -16.29589081 12.89168167 12.22353268
		 -17.23245811 13.17186737 11.99668694 -17.22957802 12.86597252 12.38933372 -18.032951355 18.076467514 13.46891785
		 -17.43033791 18.076467514 13.83822346 -17.23258781 18.076473236 13.52712727 -17.25235748 18.094919205 11.69549084
		 -16.2770462 18.12060928 11.51143074 -14.92634201 18.076471329 14.49419689 -17.32033348 18.076467514 17.21020126
		 -22.31890869 18.076467514 14.91299438 -18.032951355 16 13.46891785 -17.43033791 16 13.83822346
		 -17.23258781 16 13.52712822 -17.23119354 16.018886566 11.33828259 -16.25588226 16.044593811 11.15422058
		 -14.64339447 16 14.41838264 -17.32033348 16 17.61821556 -22.53067017 16 15.035255432
		 -18.032951355 14 13.46891785 -17.43033791 14 13.83822346 -17.23258781 13.99999809 13.52712727
		 -17.23119354 14.018909454 11.66658401 -16.25588226 14.044597626 11.48252392 -14.80669022 14 14.46213722
		 -17.32033348 14 17.45588684 -22.34391785 14 14.92743397 -18.032951355 11.92353249 13.46891785
		 -17.43033791 11.92353249 13.83822346 -17.23258781 11.92352676 13.52712727 -17.21002579 11.94287872 12.069037437
		 -16.23471451 11.96858215 11.88497829 -15.035926819 11.92352867 14.52356052 -17.32033348 11.92353249 17.21020126
		 -22.14432144 11.92353249 14.81219673 -15.030960083 19.1717453 7 -14.62896729 20.6125412 9.801157
		 -14.28152847 20.17045212 13.33837032 -14.31648827 17.5848732 14.30800915 -14.83895874 16.82105255 13.41605854
		 -14.87784195 18.7982769 11.73559952 -14.92922974 18.90786743 11.17811584 -20.99104309 13.50432968 7
		 -20.99104309 14.99999809 7 -20.99104309 16.49567032 7 -20.99104309 13.50432968 9.51961899
		 -20.99104309 14.99999809 9.51961899 -19.49567032 14.99999809 9.51961899 -19.49567032 12.0089569092 9.51961899
		 -20.99104309 16.49567032 9.51961899 -19.49567032 17.99104309 9.51961899 -16.50432968 12.0089569092 9.51961899
		 -16.50432968 14.99999809 9.51961899 -15.0089569092 14.99999809 9.51961899 -15.0089569092 13.50432968 9.51961899
		 -16.50432968 17.99104309 9.51961899 -15.0089569092 16.49567032 9.51961899 -15 16.2881012 11
		 -15 15 11 -15 13.7118988 11 -22 12.49537277 -7 -20.50462723 11 -7 -22 12.49537277 6
		 -20.50462723 11 6 -15.49537277 11 -7 -14 12.49537277 -7 -15.49537277 11 6 -14 12.49537277 6
		 -20.50462723 19 -7 -22 17.50462723 -7 -20.50462723 19 6 -22 17.50462723 6 -14 17.50462723 -7
		 -15.49537277 19 -7 -14 17.50462723 6 -15.49537277 19 6 -19.49567032 17.99104309 6
		 -20.99104309 16.49567032 6 -15.0089569092 16.49567032 6 -16.50432968 17.99104309 6
		 -20.99104309 13.50432968 6 -19.49567032 12.0089569092 6 -16.50432968 12.0089569092 6
		 -15.0089569092 13.50432968 6 -20.99104309 16.49567032 8 -19.49567032 17.99104309 8
		 -15.0089569092 16.49567032 8 -16.50432968 17.99104309 8 -19.49567032 12.0089569092 8
		 -20.99104309 13.50432968 8 -15.0089569092 13.50432968 8 -16.50432968 12.0089569092 8
		 -20.060417175 18.29057884 -7 -21.29058075 17.060417175 -7 -15.93958282 18.29057884 -7
		 -14.70942116 17.060417175 -7 -14.70942116 12.93958282 -7 -15.93958282 11.70941925 -7
		 -20.060417175 11.70941925 -7 -21.29058075 12.93958282 -7 -19.1934967 16.90606689 -10.5
		 -19.90607071 16.1934967 -10.5 -16.8065033 16.90606689 -10.5 -16.093931198 16.1934967 -10.5
		 -16.093931198 13.8065033 -10.5 -16.8065033 13.093929291 -10.5 -19.1934967 13.093929291 -10.5
		 -19.90607071 13.8065033 -10.5;
	setAttr ".vt[332:363]" -20.060417175 18.29057884 -7.49999905 -21.29058075 17.060417175 -7.49999905
		 -20.060417175 18.29057884 -8.96627808 -21.29058075 17.060417175 -8.96627808 -15.93958282 18.29057884 -7.49999905
		 -15.93958282 18.29057884 -8.96627808 -14.70942116 17.060417175 -7.49999905 -14.70942116 17.060417175 -8.96627808
		 -14.70942116 12.93958282 -7.49999905 -14.70942116 12.93958282 -8.96627808 -15.93958282 11.70941925 -7.49999905
		 -15.93958282 11.70941925 -8.96627808 -20.060417175 11.70941925 -7.49999905 -20.060417175 11.70941925 -8.96627808
		 -21.29058075 12.93958282 -7.49999905 -21.29058075 12.93958282 -8.96627808 -19.73308945 17.50033569 -7
		 -20.50033951 16.73308945 -7 -19.73308945 17.50033569 -7.49999905 -20.50033951 16.73308945 -7.49999905
		 -16.26691055 17.5003376 -7 -16.26691055 17.5003376 -7.49999905 -15.4996624 16.73308945 -7
		 -15.4996624 16.73308945 -7.49999905 -15.49966431 13.26691055 -7 -15.49966431 13.26691055 -7.49999905
		 -16.26691055 12.49966049 -7 -16.26691055 12.49966049 -7.49999905 -19.73308754 12.4996624 -7
		 -19.73308754 12.4996624 -7.49999905 -20.5003376 13.26691246 -7 -20.5003376 13.26691246 -7.49999905;
	setAttr -s 697 ".ed";
	setAttr ".ed[0:165]"  3 7 0 5 7 0 1 5 0 3 1 0 11 15 0 12 15 0 9 12 0 11 9 0
		 6 13 0 14 13 0 14 4 0 6 4 0 2 8 0 0 2 0 10 0 0 8 10 0 9 8 0 13 12 0 7 6 0 3 2 0 1 0 0
		 4 5 0 15 14 0 11 10 0 46 36 0 77 36 0 78 77 1 78 46 0 23 59 0 18 23 0 52 18 0 59 52 1
		 29 61 0 16 29 0 62 16 0 61 62 1 19 18 0 54 19 0 53 54 1 18 53 0 26 19 0 56 26 0 55 56 1
		 19 55 0 31 65 0 20 31 0 66 20 0 65 66 1 33 69 0 24 33 0 70 24 0 69 70 1 26 57 0 23 26 0
		 58 23 0 57 58 1 35 73 0 27 35 0 74 27 0 73 74 1 38 60 0 29 38 1 60 61 1 42 64 0 31 42 1
		 64 65 1 44 68 0 33 44 1 68 69 1 48 72 0 35 48 1 72 73 1 38 36 0 60 39 0 37 39 0 37 77 0
		 42 41 0 64 43 0 40 43 0 40 80 0 80 41 0 46 45 0 47 78 0 44 47 0 68 45 0 51 49 0 51 76 0
		 76 50 0 48 50 0 72 49 0 28 17 0 17 20 0 28 31 0 34 33 0 34 25 0 25 24 0 30 21 0 21 27 0
		 30 35 0 32 22 0 22 16 0 32 29 0 42 39 0 37 41 0 47 51 0 44 49 0 48 43 0 50 40 0 38 45 0
		 39 28 1 45 32 1 49 34 1 43 30 1 63 53 1 52 63 1 67 55 1 54 67 1 71 59 1 58 71 1 75 57 1
		 56 75 1 16 52 0 62 63 1 20 54 0 53 17 0 66 67 1 27 56 0 55 21 0 74 75 1 57 25 0 24 58 0
		 70 71 1 59 22 0 61 28 0 17 62 0 65 30 0 21 66 0 69 32 0 22 70 0 73 34 0 25 74 0 77 80 1
		 76 78 1 80 76 1 76 79 1 79 80 1 78 79 1 79 77 1 127 130 0 129 130 0 128 129 1 127 128 0
		 262 172 0 171 172 0 263 171 0 262 263 0 94 86 0 86 100 0 99 100 0 94 99 0 108 106 0
		 107 108 1 105 107 1 105 106 0 140 104 1 103 140 0;
	setAttr ".ed[166:331]" 85 103 1 104 85 1 131 134 1 133 134 0 132 133 0 131 132 0
		 138 139 1 124 138 0 100 124 0 139 100 1 112 109 0 112 111 0 111 110 1 109 110 1 135 136 1
		 134 136 0 135 131 0 155 158 1 158 157 0 156 157 0 155 156 0 86 101 1 87 86 1 87 102 1
		 101 102 0 111 114 0 114 113 1 110 113 1 136 129 0 128 135 0 159 160 0 160 158 0 159 155 0
		 85 87 1 102 103 0 114 105 0 113 107 1 115 112 0 116 109 1 116 115 1 89 115 1 89 266 0
		 266 111 1 272 269 0 272 271 1 271 270 1 269 270 0 271 274 1 274 273 0 270 273 0 268 105 1
		 90 268 0 90 118 1 106 118 0 108 117 1 118 117 1 119 120 0 119 108 0 120 117 0 95 92 1
		 281 92 1 96 281 1 95 96 0 92 93 0 282 93 1 281 282 1 93 94 0 283 94 1 282 283 1 98 283 1
		 98 99 0 109 122 0 122 121 0 121 116 0 181 119 0 107 181 1 130 95 1 129 96 1 96 97 0
		 136 97 1 97 98 0 134 98 1 99 133 1 137 138 1 123 137 0 123 124 0 278 275 0 278 277 0
		 277 276 1 275 276 1 277 280 0 280 279 0 276 279 1 141 126 0 126 125 0 142 125 0 141 142 1
		 83 91 0 83 119 1 91 120 0 84 117 0 91 84 0 82 116 0 88 121 0 88 82 0 81 122 1 81 88 0
		 99 123 0 133 137 1 95 125 0 142 130 1 104 126 0 104 95 0 91 127 0 185 128 1 186 185 0
		 227 186 0 91 227 0 84 130 0 88 132 0 82 133 0 137 116 0 138 115 0 139 89 0 275 272 0
		 276 271 1 279 274 0 140 141 1 118 141 0 140 90 0 117 142 0 144 143 1 83 144 0 91 143 0
		 81 145 0 145 146 1 88 146 0 148 147 1 144 148 0 143 147 0 145 149 0 149 150 1 146 150 0
		 152 151 0 148 152 0 147 151 0 149 153 0 153 154 0 150 154 0 93 155 1 94 156 0 86 157 0
		 87 158 1 92 159 0 85 160 0 92 85 0 104 161 0 161 162 1 95 162 0 162 265 0 265 163 0
		 92 163 0 163 164 1 85 164 0 260 164 0 259 260 1;
	setAttr ".ed[332:497]" 259 85 0 161 165 0 165 166 1 162 166 0 264 167 0 163 167 0
		 264 265 1 167 168 1 164 168 0 261 168 0 260 261 1 165 169 0 169 170 0 166 170 0 167 171 0
		 263 264 1 168 172 0 261 262 1 122 175 0 110 175 1 175 178 0 113 178 1 178 181 0 192 135 1
		 193 192 0 235 193 0 184 235 0 184 185 0 199 131 1 200 199 0 243 200 0 191 243 0 191 192 0
		 251 88 0 198 251 0 198 199 0 217 215 0 231 215 0 230 231 0 217 230 0 221 219 0 239 219 0
		 238 239 0 221 238 0 225 223 0 247 223 0 246 247 0 225 246 0 255 153 0 254 255 0 154 254 0
		 175 174 0 258 174 0 81 258 0 173 175 0 178 177 0 250 177 0 173 250 0 176 178 0 181 180 0
		 242 180 0 176 242 0 179 181 0 234 83 0 179 234 0 228 188 0 227 228 1 186 188 0 182 233 0
		 182 179 0 233 234 1 236 195 0 235 236 1 193 195 0 189 241 0 189 176 0 241 242 1 244 202 0
		 243 244 1 200 202 0 196 249 0 196 173 0 249 250 1 252 146 0 251 252 1 145 257 0 257 258 1
		 188 206 0 229 206 0 228 229 1 203 232 0 203 182 0 232 233 1 195 210 0 237 210 0 236 237 1
		 207 240 0 207 189 0 240 241 1 202 214 0 245 214 0 244 245 1 211 248 0 211 196 0 248 249 1
		 253 150 0 252 253 1 149 256 0 256 257 1 206 217 0 229 230 1 215 203 0 231 232 1 210 221 0
		 237 238 1 219 207 0 239 240 1 214 225 0 245 246 1 223 211 0 247 248 1 253 254 1 255 256 1
		 179 180 0 176 177 0 173 174 0 179 186 0 182 188 0 203 206 0 216 218 0 204 216 0 204 205 0
		 218 205 0 207 210 0 220 222 0 208 220 0 208 209 0 222 209 0 211 214 0 224 226 0 212 224 0
		 212 213 0 226 213 0 197 212 0 197 201 0 213 201 0 196 202 0 190 208 0 190 194 0 209 194 0
		 189 195 0 183 204 0 183 187 0 205 187 0 180 183 0 180 184 0 187 184 0 176 193 0 177 190 0
		 177 191 0 194 191 0 173 200 0 174 197 0 174 198 0 201 198 0 200 198 0;
	setAttr ".ed[498:663]" 193 191 0 186 184 0 143 228 0 147 229 0 230 151 0 152 231 0
		 232 148 0 233 144 0 187 236 0 205 237 0 238 218 0 216 239 0 240 204 0 241 183 0 194 244 0
		 209 245 0 246 222 0 220 247 0 248 208 0 249 190 0 201 252 0 213 253 0 254 226 0 224 255 0
		 256 212 0 257 197 0 161 260 0 104 259 0 165 261 0 169 262 0 170 263 0 166 264 0 266 267 0
		 267 114 1 267 268 0 266 269 0 267 270 1 89 272 0 268 273 0 90 274 0 102 277 1 101 278 0
		 139 275 0 101 139 0 140 279 0 103 280 0 97 282 1 325 331 0 330 331 0 330 329 0 328 329 0
		 327 328 0 326 327 0 324 326 0 324 325 0 310 314 0 315 314 0 315 312 0 313 312 0 313 308 0
		 309 308 0 309 311 0 311 310 0 286 284 0 287 286 0 285 287 0 285 284 0 304 286 1 305 304 0
		 287 305 1 290 288 0 291 290 0 289 291 0 289 288 0 306 290 1 307 306 0 291 307 1 294 292 0
		 295 294 0 293 295 0 292 293 0 300 294 1 301 300 0 295 301 1 298 296 0 299 298 0 297 299 0
		 297 296 0 302 298 1 303 302 0 299 303 1 309 300 0 301 308 0 310 302 0 303 311 0 313 304 0
		 305 312 0 315 306 0 307 314 0 292 297 0 294 299 0 290 287 0 285 288 0 296 289 0 298 291 0
		 293 284 0 286 295 0 300 303 0 304 301 0 306 305 0 302 307 0 293 317 1 316 317 1 292 316 1
		 316 318 1 297 318 1 318 319 1 296 319 1 319 320 1 289 320 1 320 321 1 288 321 1 322 321 1
		 285 322 1 322 323 1 284 323 1 317 323 1 333 335 0 334 335 0 332 334 0 332 333 0 334 337 0
		 336 337 0 332 336 0 337 339 0 338 339 0 336 338 0 339 341 0 340 341 0 338 340 0 341 343 0
		 342 343 0 340 342 0 345 343 0 344 345 0 344 342 0 345 347 0 346 347 0 344 346 0 335 347 0
		 333 346 0 335 325 0 334 324 0 337 326 0 339 327 0 341 328 0 343 329 0 345 330 0 347 331 0
		 349 351 0 350 351 0 348 350 0 348 349 0 350 353 0 352 353 0 348 352 0;
	setAttr ".ed[664:696]" 353 355 0 354 355 0 352 354 0 355 357 0 356 357 0 354 356 0
		 357 359 0 358 359 0 356 358 0 361 359 0 360 361 0 360 358 0 361 363 0 362 363 0 360 362 0
		 351 363 0 349 362 0 317 349 1 316 348 1 332 350 1 333 351 1 318 352 1 336 353 1 319 354 1
		 338 355 1 320 356 1 340 357 1 321 358 1 342 359 1 322 360 1 344 361 1 323 362 1 346 363 1;
	setAttr -s 341 -ch 1394 ".fc[0:340]" -type "polyFaces" 
		f 4 3 2 1 -1
		mu 0 4 0 1 2 3
		f 4 7 6 5 -5
		mu 0 4 4 5 6 7
		f 4 11 -11 9 -9
		mu 0 4 8 9 10 11
		f 4 15 14 13 12
		mu 0 4 12 13 14 15
		f 8 -20 0 18 8 17 -7 16 -13
		mu 0 8 16 0 3 17 18 6 5 19
		f 8 -24 4 22 10 21 -3 20 -15
		mu 0 8 20 4 7 21 22 23 24 25
		f 4 -21 -4 19 -14
		mu 0 4 14 1 0 15
		f 4 -22 -12 -19 -2
		mu 0 4 2 9 8 3
		f 4 -17 -8 23 -16
		mu 0 4 19 5 4 20
		f 4 -18 -10 -23 -6
		mu 0 4 6 18 21 7
		f 4 -28 26 25 -25
		mu 1 4 0 1 2 3
		f 4 31 30 29 28
		mu 0 4 26 27 28 29
		f 4 35 34 33 32
		mu 0 4 30 31 32 33
		f 4 39 38 37 36
		mu 0 4 28 34 35 36
		f 4 43 42 41 40
		mu 0 4 36 37 38 39
		f 4 47 46 45 44
		mu 0 4 40 41 42 43
		f 4 51 50 49 48
		mu 0 4 44 45 46 47
		f 4 55 54 53 52
		mu 0 4 48 49 29 39
		f 4 59 58 57 56
		mu 0 4 50 51 52 53
		f 4 62 -33 61 60
		f 4 65 -45 64 63
		f 4 68 -49 67 66
		f 4 71 -57 70 69
		f 6 -26 -76 74 -74 -61 72
		mu 1 6 4 5 6 7 8 9
		f 6 -81 -80 78 -78 -64 76
		mu 1 6 10 11 12 13 14 15
		f 6 -85 -67 83 82 27 81
		mu 1 6 16 17 18 19 1 0
		f 6 -90 -70 88 -88 -87 85
		mu 1 6 20 21 22 23 24 25
		f 4 92 -46 -92 -91
		mu 0 4 54 43 42 55
		f 4 -37 -41 -54 -30
		mu 0 4 28 36 39 29
		f 4 -96 -95 93 -50
		mu 0 4 46 56 57 47
		f 4 98 -58 -98 -97
		mu 0 4 58 59 60 61
		f 4 101 -34 -101 -100
		mu 0 4 62 33 32 63
		f 4 -75 103 -77 102
		mu 1 4 7 6 10 15
		f 4 105 -86 -105 -84
		mu 1 4 18 20 25 19
		f 4 -79 -108 -89 106
		mu 1 4 26 12 27 28
		f 4 -82 24 -73 108
		mu 1 4 29 30 4 31
		f 4 -93 -110 -103 -65
		f 4 -102 -111 -109 -62
		f 4 -94 -112 -106 -68
		f 4 -99 -113 -107 -71
		f 4 114 113 -40 -31
		mu 0 4 64 65 34 28
		f 4 -38 116 115 -44
		mu 0 4 36 35 66 67
		f 4 118 117 -29 -55
		mu 0 4 49 68 26 29
		f 4 120 119 -53 -42
		mu 0 4 38 69 48 39
		f 4 -35 122 -115 -122
		mu 0 4 32 31 65 64
		f 4 -39 124 91 123
		mu 0 4 35 34 55 42
		f 4 125 -117 -124 -47
		mu 0 4 41 66 35 42
		f 4 -43 127 97 126
		mu 0 4 38 37 70 71
		f 4 128 -121 -127 -59
		mu 0 4 72 69 38 71
		f 4 130 -56 129 95
		mu 0 4 46 49 48 56
		f 4 131 -119 -131 -51
		mu 0 4 73 68 49 46
		f 4 121 -32 132 100
		mu 0 4 74 27 26 75
		f 4 -134 -63 73 109
		f 4 134 -36 133 90
		mu 0 4 55 31 30 54
		f 4 -123 -135 -125 -114
		mu 0 4 65 31 55 34
		f 4 -136 -66 77 112
		f 4 136 -48 135 96
		mu 0 4 61 41 40 58
		f 4 -116 -126 -137 -128
		mu 0 4 67 66 41 61
		f 4 -138 -69 84 110
		f 4 138 -52 137 99
		mu 0 4 76 45 44 77
		f 4 -133 -118 -132 -139
		mu 0 4 75 26 68 73
		f 4 -140 -72 89 111
		f 4 140 -60 139 94
		mu 0 4 56 51 50 57
		f 4 -130 -120 -129 -141
		mu 0 4 56 48 69 72
		f 4 141 80 -104 75
		mu 1 4 2 32 33 34
		f 4 142 -83 104 86
		mu 1 4 24 1 19 25
		f 4 143 87 107 79
		mu 1 4 32 24 23 35
		f 3 -146 -145 -144
		mu 1 3 32 36 24
		f 3 -147 -143 144
		mu 1 3 36 1 24
		f 3 -27 146 147
		mu 1 3 2 1 36
		f 3 -148 145 -142
		mu 1 3 2 36 32
		f 4 151 150 149 -149
		mu 0 4 78 79 80 81
		f 4 155 154 153 -153
		mu 0 4 82 83 84 85
		f 4 159 158 -158 -157
		mu 0 4 86 87 88 89
		f 4 -164 162 161 160
		mu 0 4 90 91 92 93
		f 4 167 166 165 164
		mu 0 4 94 95 96 97
		f 4 171 170 169 -169
		mu 0 4 98 99 100 101
		f 4 175 174 173 172
		mu 0 4 102 88 103 104
		f 4 179 -179 -178 176
		mu 0 4 105 106 107 108
		f 4 182 168 181 -181
		mu 0 4 109 98 101 110
		f 4 186 185 -185 -184
		mu 0 4 111 112 113 114
		f 4 190 -190 188 187
		mu 0 4 115 116 117 89
		f 4 193 -193 -192 178
		mu 0 4 106 118 119 107
		f 4 195 180 194 -151
		mu 0 4 79 109 110 80
		f 4 198 183 -198 -197
		mu 0 4 120 111 114 121
		f 4 200 -167 199 189
		mu 0 4 116 96 95 117
		f 4 202 -163 -202 192
		mu 0 4 118 92 91 119
		f 4 -206 204 -177 -204
		mu 0 4 122 123 124 125
		f 5 177 -209 -208 206 203
		mu 0 5 108 107 126 127 122
		f 4 212 -212 -211 209
		mu 0 4 128 129 130 131
		f 4 215 -215 -214 211
		mu 0 4 129 132 133 130
		f 5 219 -219 217 216 163
		mu 0 5 134 135 136 137 91
		f 4 -222 -220 -161 220
		mu 0 4 138 135 134 139
		f 4 224 -221 -224 222
		mu 0 4 140 141 139 142
		f 4 228 227 226 -226
		mu 0 4 143 144 145 146
		f 4 231 230 -230 -227
		mu 0 4 145 147 148 146
		f 4 234 233 -233 -231
		mu 0 4 147 149 150 148
		f 4 236 -160 -234 -236
		mu 0 4 151 152 150 149
		f 4 -240 -239 -238 -205
		mu 0 4 153 154 155 124
		f 4 -162 241 240 223
		mu 0 4 93 92 156 142
		f 4 -150 243 -229 -243
		mu 0 4 157 158 144 143
		f 4 -195 245 -245 -244
		mu 0 4 158 159 160 144
		f 4 -182 247 -247 -246
		mu 0 4 159 161 151 160
		f 4 -170 -249 -237 -248
		mu 0 4 161 162 152 151
		f 4 -252 250 249 -174
		mu 0 4 103 163 164 104
		f 4 255 -255 -254 252
		mu 0 4 165 166 167 168
		f 4 258 -258 -257 254
		mu 0 4 166 169 170 167
		f 4 262 261 -261 -260
		mu 0 4 171 172 173 174
		f 4 265 -223 -265 263
		mu 0 4 175 140 142 176
		f 4 267 266 -225 -266
		mu 0 4 175 177 141 140
		f 4 -271 269 239 -269
		mu 0 4 178 179 154 153
		f 4 -270 -273 271 238
		mu 0 4 154 179 180 155
		f 4 -159 273 251 -175
		mu 0 4 88 87 163 103
		f 4 248 274 -251 -274
		mu 0 4 87 181 164 163
		f 4 276 242 275 -262
		mu 0 4 172 182 183 173
		f 4 -279 277 260 -276
		mu 0 4 183 184 174 173
		f 6 283 282 281 280 -152 -280
		mu 0 6 175 185 186 187 79 78
		f 4 -268 279 148 -285
		mu 0 4 188 175 78 81
		f 4 270 286 -171 -286
		mu 0 4 189 190 100 99
		f 4 -275 -287 268 -288
		mu 0 4 164 181 191 153
		f 4 -250 287 205 -289
		mu 0 4 104 164 153 122
		f 4 -290 -173 288 -207
		mu 0 4 127 102 104 122
		f 4 210 -292 -256 290
		mu 0 4 131 130 166 165
		f 4 213 -293 -259 291
		mu 0 4 130 133 169 166
		f 4 295 218 294 -294
		mu 0 4 97 136 135 171
		f 4 221 296 -263 -295
		mu 0 4 135 141 172 171
		f 4 284 -277 -297 -267
		mu 0 4 192 182 172 141
		f 4 -300 -264 298 297
		mu 0 4 193 175 176 194
		f 4 272 302 -302 -301
		mu 0 4 195 189 196 197
		f 4 -306 -298 304 303
		mu 0 4 198 193 194 199
		f 4 308 -308 -307 301
		mu 0 4 196 200 201 197
		f 4 -312 -304 310 309
		mu 0 4 202 198 199 203
		f 4 314 -314 -313 307
		mu 0 4 200 204 205 201
		f 4 232 316 -187 -316
		mu 0 4 148 150 112 111
		f 4 156 317 -186 -317
		mu 0 4 150 206 113 112
		f 4 -189 318 184 -318
		mu 0 4 206 207 114 113
		f 4 229 315 -199 -320
		mu 0 4 146 148 111 120
		f 4 -200 320 197 -319
		mu 0 4 207 95 121 114
		f 4 -322 319 196 -321
		mu 0 4 95 146 120 121
		f 4 278 324 -324 -323
		mu 0 4 184 183 208 209
		f 5 225 327 -327 -326 -325
		mu 0 5 183 210 211 212 208
		f 4 321 329 -329 -328
		mu 0 4 210 213 214 211
		f 4 -333 331 330 -330
		mu 0 4 213 215 216 214
		f 4 323 335 -335 -334
		mu 0 4 209 208 217 218
		f 4 338 326 337 -337
		mu 0 4 219 212 211 220
		f 4 328 340 -340 -338
		mu 0 4 211 214 221 220
		f 4 -331 342 341 -341
		mu 0 4 214 216 222 221
		f 4 334 345 -345 -344
		mu 0 4 218 217 223 224
		f 4 347 336 346 -155
		mu 0 4 83 219 220 84
		f 4 339 348 -154 -347
		mu 0 4 220 221 85 84
		f 4 -342 349 152 -349
		mu 0 4 221 222 82 85
		f 4 -352 -180 237 350
		mu 0 4 225 106 105 226
		f 4 -354 -194 351 352
		mu 0 4 227 118 106 225
		f 4 -242 -203 353 354
		mu 0 4 156 92 118 227
		f 7 -360 358 357 356 355 -196 -281
		mu 0 7 187 228 229 230 231 109 79
		f 7 -365 363 362 361 360 -183 -356
		mu 0 7 231 232 233 234 235 98 109
		f 6 -368 366 365 285 -172 -361
		mu 0 6 235 236 237 189 99 98
		f 4 371 370 369 -369
		mu 0 4 238 239 240 241
		f 4 375 374 373 -373
		mu 0 4 242 243 244 245
		f 4 379 378 377 -377
		mu 0 4 246 247 248 249
		f 4 382 381 380 313
		mu 0 4 204 250 251 205
		f 5 385 384 -384 -351 -272
		mu 0 5 195 252 253 225 226
		f 5 389 388 -388 -353 -387
		mu 0 5 254 255 256 227 225
		f 5 393 392 -392 -355 -391
		mu 0 5 257 258 259 156 227
		f 5 396 395 264 -241 -395
		mu 0 5 260 261 176 142 156
		f 4 -400 -283 398 397
		mu 0 4 262 186 185 263
		f 4 402 -397 -402 400
		mu 0 4 264 261 260 265
		f 4 -406 -358 404 403
		mu 0 4 266 230 229 267
		f 4 408 -394 -408 406
		mu 0 4 268 258 257 269
		f 4 -412 -363 410 409
		mu 0 4 270 234 233 271
		f 4 414 -390 -414 412
		mu 0 4 272 255 254 273
		f 4 -366 416 415 -303
		mu 0 4 189 237 274 196
		f 4 418 -386 300 417
		mu 0 4 275 252 195 197
		f 4 -398 421 420 -420
		mu 0 4 262 263 276 277
		f 4 424 -401 -424 422
		mu 0 4 278 264 265 279
		f 4 -404 427 426 -426
		mu 0 4 266 267 280 281
		f 4 430 -407 -430 428
		mu 0 4 282 268 269 283
		f 4 -410 433 432 -432
		mu 0 4 270 271 284 285
		f 4 436 -413 -436 434
		mu 0 4 286 272 273 287
		f 4 -416 438 437 -309
		mu 0 4 196 274 288 200
		f 4 440 -418 306 439
		mu 0 4 289 275 197 201
		f 4 -421 442 -372 -442
		mu 0 4 277 276 239 238
		f 4 444 -423 -444 -370
		mu 0 4 240 278 279 241
		f 4 -427 446 -376 -446
		mu 0 4 281 280 243 242
		f 4 448 -429 -448 -374
		mu 0 4 244 282 283 245
		f 4 -433 450 -380 -450
		mu 0 4 285 284 247 246
		f 4 452 -435 -452 -378
		mu 0 4 248 286 287 249
		f 4 -438 453 -383 -315
		mu 0 4 200 288 250 204
		f 4 454 -440 312 -381
		mu 0 4 251 289 201 205
		f 3 394 391 -456
		mu 0 3 260 156 259
		f 3 390 387 -457
		mu 0 3 257 227 256
		f 3 386 383 -458
		mu 0 3 254 225 253
		f 4 399 -460 401 458
		mu 0 4 186 262 265 260
		f 4 419 -461 423 459
		mu 0 4 262 277 279 265
		f 4 441 368 443 460
		mu 0 4 277 238 241 279
		f 4 464 -464 462 461
		mu 0 4 290 291 292 293
		f 4 445 372 447 465
		mu 0 4 281 242 245 283
		f 4 469 -469 467 466
		mu 0 4 294 295 296 297
		f 4 449 376 451 470
		mu 0 4 285 246 249 287
		f 4 474 -474 472 471
		mu 0 4 298 299 300 301
		f 4 477 -477 475 473
		mu 0 4 299 302 303 300
		f 4 431 -471 435 478
		mu 0 4 270 285 287 273
		f 4 481 -481 479 468
		mu 0 4 295 304 305 296
		f 4 425 -466 429 482
		mu 0 4 266 281 283 269
		f 4 485 -485 483 463
		mu 0 4 291 306 307 292
		f 4 488 -488 486 484
		mu 0 4 306 228 259 307
		f 4 405 -483 407 489
		mu 0 4 230 266 269 257
		f 4 492 -492 490 480
		mu 0 4 304 232 256 305
		f 4 411 -479 413 493
		mu 0 4 234 270 273 254
		f 4 496 -496 494 476
		mu 0 4 302 236 253 303
		f 4 -498 -494 457 495
		mu 0 4 236 234 254 253
		f 4 -499 -490 456 491
		mu 0 4 232 230 257 256
		f 4 -500 -459 455 487
		mu 0 4 228 186 260 259
		f 3 -282 499 359
		mu 0 3 187 186 228
		f 3 -357 498 364
		mu 0 3 231 230 232
		f 3 -362 497 367
		mu 0 3 235 234 236
		f 4 -399 -284 299 500
		mu 0 4 263 185 175 193
		f 4 -422 -501 305 501
		mu 0 4 276 263 193 198
		f 4 -443 -502 311 -503
		mu 0 4 239 276 198 202
		f 4 502 -310 503 -371
		mu 0 4 239 202 203 240
		f 4 -505 -445 -504 -311
		mu 0 4 199 278 240 203
		f 4 -506 -425 504 -305
		mu 0 4 194 264 278 199
		f 4 -396 -403 505 -299
		mu 0 4 176 261 264 194
		f 4 -405 -359 -489 506
		mu 0 4 267 229 228 306
		f 4 -428 -507 -486 507
		mu 0 4 280 267 306 291
		f 4 -447 -508 -465 -509
		mu 0 4 243 280 291 290
		f 4 508 -462 509 -375
		mu 0 4 243 290 293 244
		f 4 -511 -449 -510 -463
		mu 0 4 292 282 244 293
		f 4 -512 -431 510 -484
		mu 0 4 307 268 282 292
		f 4 -393 -409 511 -487
		mu 0 4 259 258 268 307
		f 4 -411 -364 -493 512
		mu 0 4 271 233 232 304
		f 4 -434 -513 -482 513
		mu 0 4 284 271 304 295
		f 4 -451 -514 -470 -515
		mu 0 4 247 284 295 294
		f 4 514 -467 515 -379
		mu 0 4 247 294 297 248
		f 4 -517 -453 -516 -468
		mu 0 4 296 286 248 297
		f 4 -518 -437 516 -480
		mu 0 4 305 272 286 296
		f 4 -389 -415 517 -491
		mu 0 4 256 255 272 305
		f 4 -417 -367 -497 518
		mu 0 4 274 237 236 302
		f 4 -439 -519 -478 519
		mu 0 4 288 274 302 299
		f 4 -454 -520 -475 -521
		mu 0 4 250 288 299 298
		f 4 520 -472 521 -382
		mu 0 4 250 298 301 251
		f 4 -523 -455 -522 -473
		mu 0 4 300 289 251 301
		f 4 -524 -441 522 -476
		mu 0 4 303 275 289 300
		f 4 -385 -419 523 -495
		mu 0 4 253 252 275 303
		f 4 -332 -526 322 524
		mu 0 4 216 215 184 209
		f 4 -343 -525 333 526
		mu 0 4 222 216 209 218
		f 4 -350 -527 343 527
		mu 0 4 82 222 218 224
		f 4 344 528 -156 -528
		mu 0 4 224 223 83 82
		f 4 529 -348 -529 -346
		mu 0 4 217 219 83 223
		f 4 325 -339 -530 -336
		mu 0 4 208 212 219 217
		f 4 -532 -531 208 191
		mu 0 4 119 308 126 107
		f 4 -217 -533 531 201
		mu 0 4 91 137 308 119
		f 4 530 534 -213 -534
		mu 0 4 126 308 129 128
		f 4 207 533 -210 -536
		mu 0 4 127 126 128 131
		f 4 532 536 -216 -535
		mu 0 4 308 137 132 129
		f 4 -218 537 214 -537
		mu 0 4 137 136 133 132
		f 4 -191 539 253 -539
		mu 0 4 116 115 168 167
		f 4 541 540 -253 -540
		mu 0 4 115 102 165 168
		f 4 -166 543 257 -543
		mu 0 4 97 96 170 169
		f 4 -201 538 256 -544
		mu 0 4 96 116 167 170
		f 4 289 535 -291 -541
		mu 0 4 102 127 131 165
		f 4 -296 542 292 -538
		mu 0 4 136 97 169 133
		f 4 -188 157 -176 -542
		mu 0 4 115 89 88 102
		f 4 -165 293 259 -278
		mu 0 4 94 97 171 174
		f 3 525 332 -168
		mu 0 3 94 309 95
		f 4 244 544 -232 -228
		mu 0 4 144 160 147 145
		f 4 246 235 -235 -545
		mu 0 4 160 151 149 147
		f 8 -553 551 550 549 548 -548 546 -546
		mu 0 8 310 311 312 313 314 315 316 317
		f 8 -561 -560 558 -558 556 -556 554 -554
		mu 0 8 318 319 320 321 322 323 324 325
		f 4 -565 563 562 561
		mu 0 4 326 327 328 329
		f 4 -563 567 566 565
		mu 0 4 330 331 332 333
		f 4 -572 570 569 568
		mu 0 4 334 335 336 337
		f 4 -570 574 573 572
		mu 0 4 338 339 340 341
		f 4 578 577 576 575
		mu 0 4 342 343 344 345
		f 4 -577 581 580 579
		mu 0 4 346 347 348 349
		f 4 -586 584 583 582
		mu 0 4 350 351 352 353
		f 4 -584 588 587 586
		mu 0 4 353 354 355 356
		f 4 -581 590 -559 589
		mu 0 4 349 348 321 320
		f 4 -588 592 560 591
		mu 0 4 356 355 319 318
		f 4 -567 594 -557 593
		mu 0 4 333 332 323 322
		f 4 -574 596 -555 595
		mu 0 4 341 340 325 324
		f 4 598 -585 -598 -576
		mu 0 4 345 352 351 342
		f 4 -564 600 -569 599
		mu 0 4 328 327 334 337
		f 4 602 -571 -602 -583
		mu 0 4 357 358 359 360
		f 4 -562 604 -578 603
		mu 0 4 361 362 363 364
		f 4 -599 -580 605 -589
		mu 0 4 354 346 349 355
		f 4 -605 -566 606 -582
		mu 0 4 365 330 333 348
		f 4 -600 -573 607 -568
		mu 0 4 366 338 341 332
		f 4 -603 -587 608 -575
		mu 0 4 367 368 356 340
		f 4 -606 -590 559 -593
		mu 0 4 355 349 320 319
		f 4 -607 -594 557 -591
		mu 0 4 348 333 322 321
		f 4 -608 -596 555 -595
		mu 0 4 332 341 324 323
		f 4 -609 -592 553 -597
		mu 0 4 340 356 318 325
		f 4 -579 611 610 -610
		mu 0 4 343 342 369 370
		f 4 597 613 -613 -612
		mu 0 4 342 351 371 369
		f 4 585 615 -615 -614
		mu 0 4 351 350 372 371
		f 4 601 617 -617 -616
		mu 0 4 350 335 373 372
		f 4 571 619 -619 -618
		mu 0 4 335 334 374 373
		f 4 -601 621 620 -620
		mu 0 4 334 327 375 374
		f 4 564 623 -623 -622
		mu 0 4 327 326 376 375
		f 4 -604 609 624 -624
		mu 0 4 326 343 370 376
		f 4 -629 627 626 -626
		mu 0 4 377 378 379 380
		f 4 631 630 -630 -628
		mu 0 4 378 381 382 379
		f 4 634 633 -633 -631
		mu 0 4 381 383 384 382
		f 4 637 636 -636 -634
		mu 0 4 383 385 386 384
		f 4 640 639 -639 -637
		mu 0 4 385 387 388 386
		f 4 -644 642 641 -640
		mu 0 4 387 389 390 388
		f 4 646 645 -645 -643
		mu 0 4 389 391 392 390
		f 4 -649 625 647 -646
		mu 0 4 391 377 380 392
		f 4 -627 650 552 -650
		mu 0 4 380 379 311 310
		f 4 629 651 -552 -651
		mu 0 4 379 382 312 311
		f 4 632 652 -551 -652
		mu 0 4 382 384 313 312
		f 4 635 653 -550 -653
		mu 0 4 384 386 314 313
		f 4 638 654 -549 -654
		mu 0 4 386 388 315 314
		f 4 -642 655 547 -655
		mu 0 4 388 390 316 315
		f 4 644 656 -547 -656
		mu 0 4 390 392 317 316
		f 4 -648 649 545 -657
		mu 0 4 392 380 310 317
		f 4 -661 659 658 -658
		mu 0 4 393 394 395 396
		f 4 663 662 -662 -660
		mu 0 4 394 397 398 395
		f 4 666 665 -665 -663
		mu 0 4 397 399 400 398
		f 4 669 668 -668 -666
		mu 0 4 399 401 402 400
		f 4 672 671 -671 -669
		mu 0 4 401 403 404 402
		f 4 -676 674 673 -672
		mu 0 4 403 405 406 404
		f 4 678 677 -677 -675
		mu 0 4 405 407 408 406
		f 4 -681 657 679 -678
		mu 0 4 407 393 396 408
		f 4 -611 682 660 -682
		mu 0 4 370 369 394 393
		f 4 628 684 -659 -684
		mu 0 4 378 377 396 395
		f 4 612 685 -664 -683
		mu 0 4 369 371 397 394
		f 4 -632 683 661 -687
		mu 0 4 381 378 395 398
		f 4 614 687 -667 -686
		mu 0 4 371 372 399 397
		f 4 -635 686 664 -689
		mu 0 4 383 381 398 400
		f 4 616 689 -670 -688
		mu 0 4 372 373 401 399
		f 4 -638 688 667 -691
		mu 0 4 385 383 400 402
		f 4 618 691 -673 -690
		mu 0 4 373 374 403 401
		f 4 -641 690 670 -693
		mu 0 4 387 385 402 404
		f 4 -621 693 675 -692
		mu 0 4 374 375 405 403
		f 4 643 692 -674 -695
		mu 0 4 389 387 404 406
		f 4 622 695 -679 -694
		mu 0 4 375 376 407 405
		f 4 -647 694 676 -697
		mu 0 4 391 389 406 408
		f 4 -625 681 680 -696
		mu 0 4 376 370 393 407
		f 4 648 696 -680 -685
		mu 0 4 377 391 408 396;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Left_Arm";
	rename -uid "747ACE4D-418D-1EAF-ED17-19800E78BEB8";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "Basic_Left_ArmShape" -p "|Basic_Left_Arm";
	rename -uid "5A571285-4450-2DF7-9D58-27B0D7EAF29D";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.62583383917808533 0.30317950248718262 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Basic_Left_ArmShapeOrig" -p "|Basic_Left_Arm";
	rename -uid "AE424FBE-4BFF-98E0-AF97-FCB9F0161602";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 409 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.375 0.31249374 0.625 0.31249374
		 0.375 0.43750626 0.625 0.81249374 0.625 0.93750626 0.56250626 1 0.43749374 1 0.375
		 0.93750626 0.43749374 0.25 0.5625062 3.7252903e-09 0.18749376 0.25 0.18749379 3.7252903e-09
		 0.3125062 3.7252903e-09 0.43749374 0.75 0.5625062 0.5 0.68749374 0.25 0.8125062 3.7252903e-09
		 0.81250632 0.25 0.43749377 3.7252903e-09 0.31250626 0.25 0.6874938 3.7252903e-09
		 0.56250626 0.25 0.43749377 0.5 0.375 0.81249374 0.625 0.43750626 0.56250626 0.75
		 0.40103906 0.44792187 0.42707813 0.44792187 0.42707813 0.30207813 0.40103906 0.30207813
		 0.375 3.7252903e-09 0.32292187 0 0.32292187 0.1805625 0.375 0.1805625 0.57292187
		 0.30207813 0.57292187 0.24132031 0.42707813 0.24132031 0.57292187 0.44792187 0.59896094
		 0.44792187 0.59896094 0.30207813 0.625 3.7252903e-09 0.57292187 0 0.57292187 0.1805625
		 0.625 0.1805625 0.40103906 0.75 0.42707813 0.75 0.42707813 0.5694375 0.40103906 0.5694375
		 0.57292187 0.50867969 0.42707813 0.50867969 0.59896094 0.75 0.625 0.75 0.625 0.5694375
		 0.59896094 0.5694375 0.42707813 7.4505806e-09 0.42707813 0.1805625 0.57292187 0.75
		 0.57292187 0.5694375 0.67707813 7.4505806e-09 0.67707813 0.1805625 0.82292187 0.1805625
		 0.82292187 0 0.17707813 7.4505806e-09 0.17707813 0.1805625 0.375 0.24132031 0.40103906
		 0.24132031 0.625 0.24132031 0.59896094 0.24132031 0.4140586 0.47830078 0.58594143
		 0.47830078 0.625 0.44792187 0.625 0.30207813 0.59896094 0.50867969 0.40103906 0.50867969
		 0.375 0.30207813 0.375 0.44792187 0.375 0.5694375 0.375 0.75 0.5 0.25 0.625 0.25
		 0.625 0.18749997 0.5 0.18749997 0.59375 0.32499999 0.625 0.32499999 0.625 0.28749999
		 0.59375 0.28749999 0.62669051 0.79823244 0.63268542 0.83147663 0.5625 0.92500001
		 0.5625 0.96249998 0.29999998 0.25 0.33749998 0.25 0.33749998 0.18749997 0.29999995
		 0.18749997 0.61875004 0.26875001 0.54312503 0.34437501 0.60000002 0.37500006 0.69441587
		 0.2877098 0.5 0.0625 0.625 0.0625 0.625 0 0.5 0 0.50562501 0.46937501 0.51875001
		 0.92499995 0.5625 0.92500001 0.33749998 0 0.29999998 0 0.29999998 0.0625 0.33749998
		 0.0625 0.5 0.12499993 0.625 0.12499993 0.66250002 0.12499994 0.70000005 0.12499994
		 0.70000005 0.0625 0.66250002 0.0625 0.5625 0.58750004 0.66250002 0.58750015 0.58125001
		 0.48125011 0.29999995 0.12499993 0.33749998 0.12499993 0.66250002 0.18749997 0.70000005
		 0.18749997 0.5 0.92500001 0.375 0.92500001 0.375 0.96249998 0.5 0.96249998 0.48124999
		 0.41875002 0.39999998 0.25 0.39999998 0.25 0.48124999 0.41875002 0.5 0.37500003 0.41874999
		 0.26874995 0.51875001 0.33125001 0.4375 0.28749996 0.375 0.32499999 0.4375 0.28749996
		 0.51875001 0.33125001 0.5 0.32499999 0.5 0.28749999 0.375 0.28749999 0.5 0.25 0.375
		 0.25 0.5 0.28749999 0.64375001 0.25 0.66218007 0.20725662 0.65296507 0.19737829 0.64375001
		 0.18749997 0.66250002 0.12499994 0.65312505 0.12499994 0.6621778 0.051159408 0.65296388
		 0.056829706 0.64375001 0.0625 0.64375001 0 0.5 0.96249998 0.375 1 0.5 1 0.37452209
		 0.18738748 0.63062501 0.25 0.63062501 0.18749997 0.64375001 0.12499994 0.63062501
		 0.12499994 0.63062501 0.0625 0.63062501 0 0.51875001 0.96249998 0.5625 0.96249998
		 0.50562501 0.46937501 0.5625 0.58750004 0.58125001 0.48125011 0.52437502 0.40687507
		 0.60000002 0.37500006 0.54312503 0.34437501 0.51875001 0.32499999 0.5625 0.32499999
		 0.5625 0.28749999 0.51875001 0.28749999 0.5 0.25 0.375 0.25 0.5 0.28749999 0.5 0.96249998
		 0.5 1 0.375 1 0.51875001 0.96249998 0.5625 0.28749999 0.51875001 0.28749999 0.5625
		 0.32499999 0.5 0.18749997 0.5 0.19227916 0.5 0.22113958 0.625 0.25 0.5 0 0.625 0
		 0.5 0.96249998 0.5 0.28749999 0.5 0.25 0.375 0.25 0.375 0 0.375 0 0.5 0 0.5 0.25
		 0.375 0.25 0.375 0 0.5 0 0.5 0.25 0.375 0.25 0.375 0 0.5 0 0.69999993 0.050078563
		 0.70000005 0.12499994 0.5625 0.32499999 0.5625 0.28749999 0.59375 0.28749999 0.625
		 0.28749999 0.62666768 0.28135902 0.625 0.32499999 0.62652498 0.32181895 0.59375 0.32499999
		 0.59375 0.32499999 0.5625 0.32499999 0.5625 0.28749999 0.59375 0.28749999 0.625 0.28749999
		 0.625 0.32499999 0.59375 0.32499999 0.5625 0.32499999 0.5625 0.28749999 0.37452209
		 0.062612481 0.375 0 0.37452209 0.12499993 0.5 0.12499993 0.5 0.12977915 0.5 0.15624996
		 0.5 0.18272077 0.5 0.0625 0.5 0.06727922 0.5 0.09374997 0.5 0.12022072 0.5 0.02886039
		 0.5 0.05772078 0.5 0.19228107 0.375 0.19228064 0.375 0.22114033 0.5 0.22114053 0.5
		 0.1297795 0.375 0.12977952 0.375 0.15624985 0.5 0.1562496 0.5 0.06728024 0.375 0.067279749
		 0.375 0.09375006 0.5 0.093750291;
	setAttr ".uvst[0].uvsp[250:408]" 0.375 0.028859658 0.5 0.028859444 0.375 0.057720732
		 0.375 0.028860366 0.375 0.06727922 0.375 0.12022072 0.375 0.09374997 0.375 0.12977916
		 0.375 0.18272075 0.375 0.15624996 0.375 0.19227916 0.375 0.22113958 0.5 0.19227916
		 0.5 0.22113958 0.375 0.22113958 0.375 0.19227917 0.5 0.12977915 0.5 0.15624994 0.375
		 0.15624994 0.375 0.12977915 0.5 0.06727922 0.5 0.09374997 0.375 0.09374997 0.375
		 0.067279227 0.5 0.02886039 0.375 0.02886039 0.5 0.19227958 0.5 0.22113979 0.375 0.2211397
		 0.375 0.1922794 0.5 0.1297795 0.5 0.15624997 0.375 0.15624996 0.375 0.1297794 0.5
		 0.067279533 0.5 0.09374994 0.375 0.09374997 0.375 0.067279458 0.5 0.02886021 0.375
		 0.028860271 0.5 0.18271969 0.37499997 0.1827202 0.375 0.18272051 0.5 0.18272045 0.5
		 0.12022034 0.375 0.12022036 0.375 0.12022048 0.5 0.12022036 0.49999997 0.057718888
		 0.375 0.057719316 0.375 0.057720542 0.5 0.057720419 0.375 0.05772078 0.5 0.05772078
		 0.375 0.12022071 0.5 0.12022071 0.375 0.18272075 0.5 0.18272075 0.41874999 0.26874995
		 0.65937507 0.25937501 0.375 0.5467304 0.375 0.7032696 0.4217304 0.75 0.57826966 0.75
		 0.625 0.7032696 0.625 0.5467304 0.5782696 0.5 0.4217304 0.5 0.6437481 0.24375807
		 0.68125188 0.081241898 0.61875814 0.231227 0.45624191 0.69377303 0.35625187 0.75626677
		 0.31874812 0.41873318 0.38124189 0.26874813 0.54375809 0.30625185 0.375 0.7032696
		 0.375 0.92500001 0.4217304 0.92500001 0.4217304 0.75 0.36098087 0.79882789 0.35625184
		 0.75626677 0.45624194 0.69377297 0.375 0.92500001 0.57826966 0.75 0.5782696 0.92500001
		 0.625 0.92500001 0.625 0.7032696 0.63925052 0.17290242 0.61875814 0.231227 0.68125188
		 0.081241921 0.70000005 0 0.4217304 0.5 0.4217304 0.32499999 0.375 0.32499999 0.375
		 0.5467304 0.36074951 0.26401913 0.38124189 0.26874813 0.31874812 0.41873324 0.29999998
		 0.25 0.625 0.5467304 0.625 0.32499999 0.57826966 0.32499999 0.5782696 0.5 0.64374816
		 0.24375805 0.54375809 0.30625185 0.56425053 0.31098089 0.70000005 0.2032696 0.875
		 0.20326962 0.875 0.046730399 0.70000005 0.046730354 0.125 0.046730399 0.125 0.20326963
		 0.29999998 0.20326963 0.29999998 0.046730399 0.31401908 0.37617201 0.43574953 0.75209755
		 0.68598092 0.060749516 0.63901913 0.26425049 0.375 0.5467304 0.4217304 0.5 0.5782696
		 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75 0.375 0.7032696
		 0.375 0.5467304 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.5782696 0.5 0.5782696
		 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696 0.625 0.7032696 0.57826966 0.75
		 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.7032696 0.375
		 0.5467304 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.5782696 0.5 0.5782696 0.5
		 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696 0.625 0.7032696 0.57826966 0.75 0.57826966
		 0.75 0.4217304 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.7032696;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 37 ".uvst[1].uvsp[0:36]" -type "float2" 0.375 0.81249374
		 0.375 0.93750626 0.40624687 0.9687531 0.40624687 0.7812469 0.31250626 -3.7252903e-09
		 0.375 0.25 0.40624687 0.25 0.43749374 0.25 0.43749374 3.7252903e-09 0.375 0 0.56250626
		 -3.7252903e-09 0.56250626 0.25 0.5937531 0.25 0.625 0.25 0.68749374 -3.7252903e-09
		 0.625 -3.7252903e-09 0.375 0.5 0.43749374 0.75 0.43749374 0.5 0.40624687 0.5 0.56250626
		 0.5 0.56250626 0.75 0.5937531 0.7812469 0.625 0.81249374 0.625 0.5 0.5937531 0.5
		 0.68749374 0.25 0.81250632 0.25 0.81250626 -3.7252903e-09 0.18749374 0.25 0.31250626
		 0.25 0.18749374 -3.7252903e-09 0.43749374 1 0.56250626 1 0.5937531 0.9687531 0.625
		 0.93750626 0.5 0.875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 364 ".vt";
	setAttr ".vt[0:165]"  21 16 1.50014997 19.50015068 16 3 21 28 1.50014997
		 19.50015068 28 3 15 16 1.50014997 16.49984932 16 3 15 28 1.50014997 16.49984932 28 3
		 21 28 -1.50014997 19.50015068 28 -3 21 16 -1.50014997 19.50015068 16 -3 16.49984932 28 -3
		 15 28 -1.50014997 15 16 -1.50014997 16.49984932 16 -3 24 37.50025177 3.5002501 21.50024986 37.50025177 6
		 21.50024986 40 3.5002501 14.49975014 40 3.5002501 14.49975014 37.50025177 6 12 37.50025177 3.5002501
		 24 37.50025177 -3.5002501 21.50024986 40 -3.5002501 21.50024986 37.50025177 -6 14.49975014 37.50025177 -6
		 14.49975014 40 -3.5002501 12 37.50025177 -3.5002501 21.50024986 31 6 24 31 3.5002501
		 12 31 3.5002501 14.49975014 31 6 24 31 -3.5002501 21.50024986 31 -6 14.49975014 31 -6
		 12 31 -3.5002501 23 28 2.5002501 20.50024986 28 5 23 31 2.5002501 20.50024986 31 5
		 13 28 2.5002501 15.49975014 28 5 15.49975014 31 5 13 31 2.5002501 20.50024986 31 -5
		 23 31 -2.5002501 23 28 -2.5002501 20.50024986 28 -5 13 31 -2.5002501 15.49975014 31 -5
		 13 28 -2.5002501 15.49975014 28 -5 23.03966713 39.077758789 3.5002501 21.50024986 39.077758789 5.039667606
		 14.49975014 39.077758789 5.039667606 12.96033287 39.077758789 3.5002501 12.96033287 39.077758789 -3.5002501
		 14.49975014 39.077758789 -5.03966713 21.50024986 39.077758789 -5.039667606 23.03966713 39.077758789 -3.5002501
		 21.97871399 31 3.9787128 23.03966713 31 5.039667606 23.03966713 37.50025177 5.039667606
		 22.58447266 38.6620903 4.58447266 14.021286011 31 3.9787128 12.96033287 31 5.039667606
		 12.96033287 37.50025177 5.039667606 13.41552734 38.6620903 4.58447266 21.97871399 31 -3.9787128
		 23.03966713 31 -5.039667606 23.03966713 37.50025177 -5.039667606 22.58447266 38.6620903 -4.58447266
		 14.021286011 31 -3.9787128 12.96033287 31 -5.039667606 12.96033287 37.50025177 -5.039667606
		 13.41552734 38.6620903 -4.58447266 14.021286011 28 -3.9787128 21.97871399 28 3.97871256
		 21.97871399 28 -3.97871256 18 28 0 14.021286011 28 3.9787128 21.96157646 11 14.73168659
		 18 11 11.70988083 22.13616371 19 14.83248425 18 19 11.70988083 14 17.1514492 7 14 12.8485508 7
		 14 14.99999619 7 18 11 13.49056625 19.49567032 12.0089569092 7 19.49567032 17.99104309 7
		 18 19 13.49056625 14 16.88254547 11 14 14.99999809 11 14 13.11745453 11 16 18.71984863 11
		 16 17 11 16 14.99999809 11 16 13 11 16 11 11 16 11 7 15.0089569092 13.50432968 7
		 15.0089569092 14.99999809 7 15.0089569092 16.49567032 7 16 19.09085083 7 22.67245674 17 7
		 22.15194702 19.15194702 7 22.67245674 17 11 22.21488571 19.21488571 11 22.21488571 10.78511429 11
		 22.67245674 13 11 22.67245674 13 7 22.15194702 10.84805298 7 22.80763054 14.99999809 11
		 22.80763054 14.99999809 7 19.43179131 10.69610596 7 18 10.69610596 11.70988083 18 19.30389404 11.70988083
		 19.43179131 19.30389404 7 22.21357727 19.15194702 14.87717915 18 19.30389404 13.49056625
		 18 10.69610596 13.49056625 22.21357727 10.84805298 14.87717915 16 10.69610596 11
		 16 10.69610596 7 16 19.30389404 11 16 19.30389404 7 17.38764954 19 13.49056625 17.38764954 17 13.49056625
		 17.39999962 17 11.70988083 17.39999962 19 11.70988083 17.38764954 13 13.49056625
		 17.38764954 11 13.49056625 17.39999962 11 11.70988083 17.39999962 13 11.70988083
		 17.38764954 14.99999809 13.49056625 17.39999962 14.99999809 11.70988083 17.39999962 10.69610596 11.70988083
		 17.39999962 10.69610596 7 16.50432968 12.0089569092 7 16.50432968 17.99104309 7 17.39999962 19.30389404 7
		 17.39999962 19.30389404 11.70988083 17.42310524 19 13.82323742 17.3177948 19 16.8607235
		 17.3177948 11 16.8607235 17.42310524 11 13.82323742 17.23379326 19 13.54300213 15.077960968 19 14.41306496
		 15.18754578 11 14.44242859 17.23379326 11 13.54300213 17.27010345 19.018013 12.018017769
		 16.33641624 19.043708801 11.85221672 16.26112366 11.045482635 12.24091625 17.19481087 11.019786835 12.4067173
		 13.71835136 14.99999809 11 13.71835136 13.11745453 11 13.71835136 12.8485508 7 13.71835136 14.99999619 7
		 13.71835136 16.88254547 11 13.71835136 17.1514492 7 15.62994576 20.67459106 9.92006111
		 15.90540123 19.17694473 11.18325233 13.90795135 18.76401329 11.15792656 13.63249207 20.26165962 9.89473534
		 15.29007912 20.26345825 13.3777504;
	setAttr ".vt[166:331]" 15.83712196 19.057929993 11.90740204 13.88194275 18.69046593 11.45662308
		 13.33489799 19.89599228 12.92696762 15.20109749 17.91022873 14.16748238 15.68040085 17.23917007 13.349226
		 13.96732521 17.035030365 13.098031998 13.48801804 17.70608711 13.91628647 22.16117287 13.15293503 14.84692287
		 21.96157646 12.84706497 14.73168659 22.67842102 13 15.12247467 22.34792519 15.15293312 14.95474434
		 22.16117287 14.84706306 14.84692287 22.81671143 14.99999619 15.1242733 22.13616371 17.15293503 14.83248425
		 22.34792519 16.84706497 14.95474434 22.67842102 17 15.12247467 17.3177948 17.15293503 16.8607235
		 17.3177948 16.84706497 17.26873589 18 16.84706497 13.49056625 17.69385529 17 13.49056625
		 18 17.15293503 13.49056625 17.42310524 16.84706497 13.82323742 17.42310524 17.15293503 13.82323742
		 17.3177948 15.15293312 17.26873589 17.3177948 14.84706306 17.10641098 18 14.84706306 13.49056625
		 17.69385529 14.99999809 13.49056625 18 15.15293312 13.49056625 17.42310524 14.84706306 13.82323742
		 17.42310524 15.15293312 13.82323742 17.3177948 13.15293503 17.10641098 17.3177948 12.84706497 16.8607235
		 18 12.84706497 13.49056625 17.69385529 13 13.49056625 18 13.15293503 13.49056625
		 17.42310524 12.84706497 13.82323742 17.42310524 13.15293503 13.82323742 15.077960968 17.15294266 14.41306496
		 14.79501534 16.84705734 14.33725071 17.23379326 16.84705544 13.54300213 17.23379326 17.15294647 13.54300213
		 14.79501534 15.15294075 14.33725071 14.95831108 14.84705544 14.38100529 17.23379326 14.84705162 13.54300213
		 17.23379326 15.15294456 13.54300213 14.95831108 13.15294266 14.38100529 15.18754578 12.84705734 14.44242859
		 17.23379326 12.84705353 13.54300213 17.23379326 13.15294456 13.54300213 16.30164909 17.19750977 11.86960125
		 16.298769 16.89164352 11.5025835 17.23533821 17.17182541 12.035404205 17.23245811 16.86593056 11.66838646
		 16.2987709 15.19754219 11.50258255 16.2987709 14.89164925 11.83088589 17.23246002 15.17184448 11.66838551
		 17.23246002 14.86595154 11.99668884 16.2987709 13.19754791 11.83088589 16.29589081 12.89168167 12.22353268
		 17.23245811 13.17186737 11.99668694 17.22957802 12.86597252 12.38933372 18.032951355 18.076467514 13.46891785
		 17.43033791 18.076467514 13.83822346 17.23258781 18.076473236 13.52712727 17.25235748 18.094919205 11.69549084
		 16.2770462 18.12060928 11.51143074 14.92634201 18.076471329 14.49419689 17.32033348 18.076467514 17.21020126
		 22.31890869 18.076467514 14.91299438 18.032951355 16 13.46891785 17.43033791 16 13.83822346
		 17.23258781 16 13.52712822 17.23119354 16.018886566 11.33828259 16.25588226 16.044593811 11.15422058
		 14.64339447 16 14.41838264 17.32033348 16 17.61821556 22.53067017 16 15.035255432
		 18.032951355 14 13.46891785 17.43033791 14 13.83822346 17.23258781 13.99999809 13.52712727
		 17.23119354 14.018909454 11.66658401 16.25588226 14.044597626 11.48252392 14.80669022 14 14.46213722
		 17.32033348 14 17.45588684 22.34391785 14 14.92743397 18.032951355 11.92353249 13.46891785
		 17.43033791 11.92353249 13.83822346 17.23258781 11.92352676 13.52712727 17.21002579 11.94287872 12.069037437
		 16.23471451 11.96858215 11.88497829 15.035926819 11.92352867 14.52356052 17.32033348 11.92353249 17.21020126
		 22.14432144 11.92353249 14.81219673 15.030960083 19.1717453 7 14.62896729 20.6125412 9.801157
		 14.28152847 20.17045212 13.33837032 14.31648827 17.5848732 14.30800915 14.83895874 16.82105255 13.41605854
		 14.87784195 18.7982769 11.73559952 14.92922974 18.90786743 11.17811584 20.99104309 13.50432968 7
		 20.99104309 14.99999809 7 20.99104309 16.49567032 7 20.99104309 13.50432968 9.51961899
		 20.99104309 14.99999809 9.51961899 19.49567032 14.99999809 9.51961899 19.49567032 12.0089569092 9.51961899
		 20.99104309 16.49567032 9.51961899 19.49567032 17.99104309 9.51961899 16.50432968 12.0089569092 9.51961899
		 16.50432968 14.99999809 9.51961899 15.0089569092 14.99999809 9.51961899 15.0089569092 13.50432968 9.51961899
		 16.50432968 17.99104309 9.51961899 15.0089569092 16.49567032 9.51961899 15 16.2881012 11
		 15 15 11 15 13.7118988 11 22 12.49537277 -7 20.50462723 11 -7 22 12.49537277 6 20.50462723 11 6
		 15.49537277 11 -7 14 12.49537277 -7 15.49537277 11 6 14 12.49537277 6 20.50462723 19 -7
		 22 17.50462723 -7 20.50462723 19 6 22 17.50462723 6 14 17.50462723 -7 15.49537277 19 -7
		 14 17.50462723 6 15.49537277 19 6 19.49567032 17.99104309 6 20.99104309 16.49567032 6
		 15.0089569092 16.49567032 6 16.50432968 17.99104309 6 20.99104309 13.50432968 6 19.49567032 12.0089569092 6
		 16.50432968 12.0089569092 6 15.0089569092 13.50432968 6 20.99104309 16.49567032 8
		 19.49567032 17.99104309 8 15.0089569092 16.49567032 8 16.50432968 17.99104309 8 19.49567032 12.0089569092 8
		 20.99104309 13.50432968 8 15.0089569092 13.50432968 8 16.50432968 12.0089569092 8
		 20.060417175 18.29057884 -7 21.29058075 17.060417175 -7 15.93958282 18.29057884 -7
		 14.70942116 17.060417175 -7 14.70942116 12.93958282 -7 15.93958282 11.70941925 -7
		 20.060417175 11.70941925 -7 21.29058075 12.93958282 -7 19.1934967 16.90606689 -10.5
		 19.90607071 16.1934967 -10.5 16.8065033 16.90606689 -10.5 16.093931198 16.1934967 -10.5
		 16.093931198 13.8065033 -10.5 16.8065033 13.093929291 -10.5 19.1934967 13.093929291 -10.5
		 19.90607071 13.8065033 -10.5;
	setAttr ".vt[332:363]" 20.060417175 18.29057884 -7.49999905 21.29058075 17.060417175 -7.49999905
		 20.060417175 18.29057884 -8.96627808 21.29058075 17.060417175 -8.96627808 15.93958282 18.29057884 -7.49999905
		 15.93958282 18.29057884 -8.96627808 14.70942116 17.060417175 -7.49999905 14.70942116 17.060417175 -8.96627808
		 14.70942116 12.93958282 -7.49999905 14.70942116 12.93958282 -8.96627808 15.93958282 11.70941925 -7.49999905
		 15.93958282 11.70941925 -8.96627808 20.060417175 11.70941925 -7.49999905 20.060417175 11.70941925 -8.96627808
		 21.29058075 12.93958282 -7.49999905 21.29058075 12.93958282 -8.96627808 19.73308945 17.50033569 -7
		 20.50033951 16.73308945 -7 19.73308945 17.50033569 -7.49999905 20.50033951 16.73308945 -7.49999905
		 16.26691055 17.5003376 -7 16.26691055 17.5003376 -7.49999905 15.4996624 16.73308945 -7
		 15.4996624 16.73308945 -7.49999905 15.49966431 13.26691055 -7 15.49966431 13.26691055 -7.49999905
		 16.26691055 12.49966049 -7 16.26691055 12.49966049 -7.49999905 19.73308754 12.4996624 -7
		 19.73308754 12.4996624 -7.49999905 20.5003376 13.26691246 -7 20.5003376 13.26691246 -7.49999905;
	setAttr -s 697 ".ed";
	setAttr ".ed[0:165]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0 7 6 0 9 12 0
		 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0 6 4 0 8 10 0
		 11 9 0 12 15 0 14 13 0 37 41 0 37 77 0 40 80 0 46 36 0 47 51 0 47 78 0 50 40 0 51 76 0
		 16 52 0 18 23 0 23 59 0 22 16 0 17 62 0 16 29 0 29 61 0 28 17 0 18 53 0 17 20 0 20 54 0
		 19 18 0 19 55 0 21 27 0 27 56 0 26 19 0 21 66 0 20 31 0 31 65 0 30 21 0 22 70 0 24 33 0
		 33 69 0 32 22 0 24 58 0 23 26 0 26 57 0 25 24 0 25 74 0 27 35 0 35 73 0 34 25 0 29 38 1
		 38 60 0 39 28 1 31 42 1 42 64 0 43 30 1 33 44 1 44 68 0 45 32 1 35 48 1 48 72 0 49 34 1
		 37 39 0 38 36 0 40 43 0 42 41 0 44 47 0 46 45 0 48 50 0 51 49 0 28 31 0 34 33 0 30 35 0
		 32 29 0 42 39 0 44 49 0 48 43 0 38 45 0 52 18 0 53 17 0 54 19 0 55 21 0 56 26 0 57 25 0
		 58 23 0 59 22 0 52 63 1 53 54 1 54 67 1 55 56 1 56 75 1 57 58 1 58 71 1 59 52 1 60 39 0
		 61 28 0 62 16 0 63 53 1 60 61 1 61 62 1 62 63 1 64 43 0 65 30 0 66 20 0 67 55 1 64 65 1
		 65 66 1 66 67 1 68 45 0 69 32 0 70 24 0 71 59 1 68 69 1 69 70 1 70 71 1 72 49 0 73 34 0
		 74 27 0 75 57 1 72 73 1 73 74 1 74 75 1 76 50 0 77 36 0 76 79 1 78 46 0 79 77 1 80 41 0
		 78 79 1 79 80 1 80 76 1 76 78 1 78 77 1 77 80 1 83 91 0 86 100 0 86 101 1 87 86 1
		 87 102 1 85 87 1 85 103 1 88 82 0 89 266 0 90 268 0 91 84 0 91 227 0 92 85 0 94 86 0
		 94 99 0 92 93 0 93 94 0 95 92 1;
	setAttr ".ed[166:331]" 96 281 1 97 282 1 98 283 1 99 133 1 101 139 0 103 140 0
		 104 259 0 95 96 0 96 97 0 97 98 0 98 99 0 99 100 0 101 102 0 102 103 0 104 95 0 105 106 0
		 105 107 1 107 108 1 108 106 0 109 110 1 111 110 1 112 111 0 112 109 0 110 113 1 114 113 1
		 111 114 0 113 107 1 114 105 0 89 115 1 82 116 0 116 115 1 84 117 0 108 117 1 90 118 1
		 118 117 1 106 118 0 83 119 1 91 120 0 119 120 0 120 117 0 119 108 0 88 121 0 121 116 0
		 81 122 1 122 121 0 109 122 0 110 175 1 122 175 0 113 178 1 107 181 1 99 123 0 100 124 0
		 123 124 0 123 137 0 124 138 0 95 125 0 117 142 0 104 126 0 126 125 0 118 141 0 91 127 0
		 127 128 0 128 129 1 84 130 0 129 130 0 127 130 0 88 132 0 131 132 0 82 133 0 132 133 0
		 133 134 0 131 134 1 135 131 0 134 136 0 135 136 1 128 135 0 136 129 0 130 95 1 129 96 1
		 136 97 1 134 98 1 137 116 0 138 115 0 139 89 0 140 90 0 141 126 0 142 125 0 133 137 1
		 137 138 1 138 139 1 140 141 1 141 142 1 142 130 1 91 143 0 83 144 0 144 143 1 81 145 0
		 88 146 0 145 146 1 143 147 0 144 148 0 148 147 1 145 149 0 146 150 0 149 150 1 147 151 0
		 148 152 0 152 151 0 149 153 0 150 154 0 153 154 0 93 155 1 94 156 0 155 156 0 86 157 0
		 156 157 0 87 158 1 158 157 0 155 158 1 92 159 0 159 155 0 85 160 0 160 158 0 159 160 0
		 104 161 0 95 162 0 161 162 1 92 163 0 162 265 0 85 164 0 163 164 1 161 260 0 161 165 0
		 162 166 0 165 166 1 163 167 0 166 264 0 164 168 0 167 168 1 165 261 0 165 169 0 166 170 0
		 169 170 0 167 171 0 170 263 0 168 172 0 171 172 0 169 262 0 175 178 0 178 181 0 181 119 0
		 184 235 0 185 128 1 184 185 0 186 185 0 191 243 0 192 135 1 191 192 0 193 192 0 198 251 0
		 199 131 1 198 199 0 200 199 0 174 197 0 196 173 0 173 175 0;
	setAttr ".ed[332:497]" 175 174 0 177 190 0 189 176 0 176 178 0 178 177 0 180 183 0
		 182 179 0 179 181 0 181 180 0 183 204 0 203 182 0 186 188 0 187 184 0 188 206 0 205 187 0
		 190 208 0 207 189 0 193 195 0 194 191 0 195 210 0 209 194 0 197 212 0 211 196 0 200 202 0
		 201 198 0 202 214 0 213 201 0 204 216 0 215 203 0 206 217 0 218 205 0 208 220 0 219 207 0
		 210 221 0 222 209 0 212 224 0 223 211 0 214 225 0 226 213 0 216 218 0 220 222 0 224 226 0
		 217 230 0 152 231 0 221 238 0 216 239 0 225 246 0 220 247 0 154 254 0 224 255 0 81 258 0
		 173 250 0 176 242 0 179 234 0 143 228 0 182 233 0 187 236 0 189 241 0 194 244 0 196 249 0
		 201 252 0 145 257 0 147 229 0 203 232 0 205 237 0 207 240 0 209 245 0 211 248 0 213 253 0
		 149 256 0 179 180 0 176 177 0 173 174 0 179 186 0 182 188 0 203 206 0 204 205 0 207 210 0
		 208 209 0 211 214 0 212 213 0 197 201 0 196 202 0 190 194 0 189 195 0 183 187 0 180 184 0
		 176 193 0 177 191 0 173 200 0 174 198 0 200 198 0 193 191 0 186 184 0 227 186 0 228 188 0
		 229 206 0 230 151 0 231 215 0 232 148 0 233 144 0 234 83 0 227 228 1 228 229 1 229 230 1
		 231 232 1 232 233 1 233 234 1 235 193 0 236 195 0 237 210 0 238 218 0 239 219 0 240 204 0
		 241 183 0 242 180 0 235 236 1 236 237 1 237 238 1 239 240 1 240 241 1 241 242 1 243 200 0
		 244 202 0 245 214 0 246 222 0 247 223 0 248 208 0 249 190 0 250 177 0 243 244 1 244 245 1
		 245 246 1 247 248 1 248 249 1 249 250 1 251 88 0 252 146 0 253 150 0 254 226 0 255 153 0
		 256 212 0 257 197 0 258 174 0 251 252 1 252 253 1 253 254 1 255 256 1 256 257 1 257 258 1
		 259 85 0 260 164 0 261 168 0 262 172 0 263 171 0 264 167 0 265 163 0 259 260 1 260 261 1
		 261 262 1 262 263 0 263 264 1 264 265 1 81 88 0 116 109 1 115 112 0;
	setAttr ".ed[498:663]" 266 111 1 267 114 1 268 105 1 230 231 0 217 215 0 238 239 0
		 221 219 0 246 247 0 225 223 0 254 255 0 266 267 0 267 268 0 266 269 0 267 270 1 269 270 0
		 271 270 1 89 272 0 272 271 1 272 269 0 268 273 0 270 273 0 90 274 0 274 273 0 271 274 1
		 139 275 0 275 276 1 102 277 1 277 276 1 101 278 0 278 277 0 278 275 0 140 279 0 276 279 1
		 103 280 0 280 279 0 277 280 0 276 271 1 275 272 0 279 274 0 139 100 1 140 104 1 104 85 1
		 281 92 1 282 93 1 283 94 1 281 282 1 282 283 1 285 288 0 285 284 0 289 288 0 292 297 0
		 293 284 0 292 293 0 296 289 0 297 296 0 309 311 0 309 308 0 310 314 0 311 310 0 313 308 0
		 313 312 0 315 312 0 315 314 0 285 287 0 287 286 0 286 284 0 287 305 1 305 304 0 304 286 1
		 289 291 0 291 290 0 290 288 0 291 307 1 307 306 0 306 290 1 293 295 0 295 294 0 294 292 0
		 295 301 1 301 300 0 300 294 1 297 299 0 299 298 0 298 296 0 299 303 1 303 302 0 302 298 1
		 301 308 0 309 300 0 303 311 0 310 302 0 305 312 0 313 304 0 307 314 0 315 306 0 294 299 0
		 290 287 0 298 291 0 286 295 0 300 303 0 304 301 0 306 305 0 302 307 0 292 316 1 293 317 1
		 316 317 1 297 318 1 316 318 1 296 319 1 318 319 1 289 320 1 319 320 1 288 321 1 320 321 1
		 285 322 1 322 321 1 284 323 1 322 323 1 317 323 1 324 325 0 324 326 0 326 327 0 327 328 0
		 328 329 0 330 329 0 330 331 0 325 331 0 332 333 0 332 334 0 334 335 0 333 335 0 332 336 0
		 336 337 0 334 337 0 336 338 0 338 339 0 337 339 0 338 340 0 340 341 0 339 341 0 340 342 0
		 342 343 0 341 343 0 344 342 0 344 345 0 345 343 0 344 346 0 346 347 0 345 347 0 333 346 0
		 335 347 0 334 324 0 335 325 0 337 326 0 339 327 0 341 328 0 343 329 0 345 330 0 347 331 0
		 316 348 1 317 349 1 348 349 0 332 350 1 348 350 0 333 351 1 350 351 0;
	setAttr ".ed[664:696]" 349 351 0 318 352 1 348 352 0 336 353 1 352 353 0 350 353 0
		 319 354 1 352 354 0 338 355 1 354 355 0 353 355 0 320 356 1 354 356 0 340 357 1 356 357 0
		 355 357 0 321 358 1 356 358 0 342 359 1 358 359 0 357 359 0 322 360 1 360 358 0 344 361 1
		 360 361 0 361 359 0 323 362 1 360 362 0 346 363 1 362 363 0 361 363 0 349 362 0 351 363 0;
	setAttr -s 341 -ch 1394 ".fc[0:340]" -type "polyFaces" 
		f 4 3 -19 -1 -18
		mu 0 4 8 21 9 18
		f 4 11 -23 -9 -22
		mu 0 4 13 25 14 22
		f 4 6 -24 14 -20
		mu 0 4 15 17 16 20
		f 4 -3 -17 -11 -21
		mu 0 4 10 19 12 11
		f 8 2 -10 8 -14 -7 -8 -4 4
		mu 0 8 0 2 22 14 24 1 21 8
		f 8 10 -2 0 -6 -15 -16 -12 12
		mu 0 8 23 7 6 5 4 3 25 13
		f 4 16 -5 17 1
		mu 0 4 12 19 8 18
		f 4 18 7 19 5
		mu 0 4 9 21 15 20
		f 4 20 -13 21 9
		mu 0 4 2 23 13 22
		f 4 22 15 23 13
		mu 0 4 14 25 3 24
		f 4 27 -138 -147 139
		mu 1 4 0 1 2 3
		f 4 -35 -34 -93 -108
		mu 0 4 26 27 28 29
		f 4 -39 -38 -111 -114
		mu 0 4 30 31 32 33
		f 4 -44 -95 -102 -41
		mu 0 4 28 34 35 36
		f 4 -48 -97 -104 -45
		mu 0 4 34 37 38 39
		f 4 -51 -50 -118 -121
		mu 0 4 40 41 42 43
		f 4 -55 -54 -125 -128
		mu 0 4 44 45 46 47
		f 4 -59 -58 -99 -106
		mu 0 4 48 37 27 49
		f 4 -63 -62 -132 -135
		mu 0 4 50 51 52 53
		f 4 -66 -65 38 -113
		f 4 -69 -68 50 -120
		f 4 -72 -71 54 -127
		f 4 -75 -74 62 -134
		f 6 -78 65 108 -77 25 137
		mu 1 6 4 5 6 7 8 9
		f 6 -80 68 115 -79 26 141
		mu 1 6 10 11 12 13 14 15
		f 6 -82 -140 -30 -81 71 122
		mu 1 6 16 0 3 17 18 19
		f 6 -84 31 136 -83 74 129
		mu 1 6 20 21 22 23 24 25
		f 4 39 41 49 -85
		mu 0 4 54 55 42 41
		f 4 33 57 47 43
		mu 0 4 28 27 37 34
		f 4 53 -86 63 59
		mu 0 4 46 45 56 57
		f 4 51 45 61 -87
		mu 0 4 58 59 60 61
		f 4 55 35 37 -88
		mu 0 4 62 63 32 31
		f 4 -89 79 -25 76
		mu 1 4 7 11 10 8
		f 4 80 28 83 -90
		mu 1 4 18 17 21 20
		f 4 -91 82 30 78
		mu 1 4 26 27 28 14
		f 4 -92 77 -28 81
		mu 1 4 29 30 4 31
		f 4 67 88 66 84
		f 4 64 91 72 87
		f 4 70 89 75 85
		f 4 73 90 69 86
		f 4 92 40 -112 -101
		mu 0 4 64 28 36 65
		f 4 44 -119 -103 94
		mu 0 4 34 66 67 35
		f 4 98 34 -126 -107
		mu 0 4 49 27 26 68
		f 4 96 58 -133 -105
		mu 0 4 38 37 48 69
		f 4 32 100 -115 110
		mu 0 4 32 64 65 33
		f 4 -43 -42 -94 101
		mu 0 4 35 42 55 36
		f 4 117 42 102 -122
		mu 0 4 43 42 35 67
		f 4 -47 -46 -96 103
		mu 0 4 38 70 71 39
		f 4 131 46 104 -136
		mu 0 4 72 70 38 69
		f 4 -60 -98 105 -57
		mu 0 4 46 57 48 49
		f 4 124 56 106 -129
		mu 0 4 73 46 49 68
		f 4 -36 -100 107 -33
		mu 0 4 74 75 26 29
		f 4 -67 -109 112 109
		f 4 -40 -110 113 -37
		mu 0 4 55 54 30 33
		f 4 111 93 36 114
		mu 0 4 65 36 55 33
		f 4 -70 -116 119 116
		f 4 -52 -117 120 -49
		mu 0 4 59 58 40 43
		f 4 95 48 121 118
		mu 0 4 66 59 43 67
		f 4 -73 -123 126 123
		f 4 -56 -124 127 -53
		mu 0 4 76 77 44 47
		f 4 52 128 125 99
		mu 0 4 75 73 68 26
		f 4 -76 -130 133 130
		f 4 -64 -131 134 -61
		mu 0 4 57 56 50 53
		f 4 60 135 132 97
		mu 0 4 57 72 69 48
		f 4 -26 24 -142 -148
		mu 1 4 2 32 33 34
		f 4 -32 -29 29 -146
		mu 1 4 22 21 17 3
		f 4 -27 -31 -137 -145
		mu 1 4 34 35 23 22
		f 3 144 138 143
		mu 1 3 34 22 36
		f 3 -139 145 142
		mu 1 3 36 22 3
		f 3 -141 -143 146
		mu 1 3 2 36 3
		f 3 147 -144 140
		mu 1 3 2 34 36
		f 4 231 -231 -229 -228
		mu 0 4 78 79 80 81
		f 4 485 -313 -487 -493
		mu 0 4 82 83 84 85
		f 4 161 149 -178 -163
		mu 0 4 86 87 88 89
		f 4 -185 -184 -183 181
		mu 0 4 90 91 92 93
		f 4 -539 -172 -155 -540
		mu 0 4 94 95 96 97
		f 4 237 -237 -236 -234
		mu 0 4 98 99 100 101
		f 4 -256 -221 -218 -538
		mu 0 4 102 103 104 88
		f 4 -189 187 186 -186
		mu 0 4 105 106 107 108
		f 4 240 -240 -238 -239
		mu 0 4 109 110 99 98
		f 4 284 283 -282 -280
		mu 0 4 111 112 113 114
		f 4 -151 -152 152 -179
		mu 0 4 115 87 116 117
		f 4 -187 191 190 -190
		mu 0 4 108 107 118 119
		f 4 228 -243 -241 -242
		mu 0 4 81 80 110 109
		f 4 289 288 -285 -287
		mu 0 4 120 121 112 111
		f 4 -153 -154 154 -180
		mu 0 4 117 116 97 96
		f 4 -191 193 182 -193
		mu 0 4 119 118 93 92
		f 4 497 188 -497 196
		mu 0 4 122 123 124 125
		f 5 -498 -195 156 498 -188
		mu 0 5 106 122 126 127 107
		f 4 -517 515 513 -513
		mu 0 4 128 129 130 131
		f 4 -514 521 520 -519
		mu 0 4 131 130 132 133
		f 5 -182 -501 -158 199 -202
		mu 0 5 134 93 135 136 137
		f 4 -199 184 201 200
		mu 0 4 138 139 134 137
		f 4 -205 206 198 -206
		mu 0 4 140 141 139 142
		f 4 165 -541 -167 -174
		mu 0 4 143 144 145 146
		f 4 540 163 -542 -544
		mu 0 4 145 144 147 148
		f 4 541 164 -543 -545
		mu 0 4 148 147 149 150
		f 4 168 542 162 -177
		mu 0 4 151 150 149 152
		f 4 496 211 210 208
		mu 0 4 153 124 154 155
		f 4 -207 -317 -216 183
		mu 0 4 91 141 156 92
		f 4 243 173 -245 230
		mu 0 4 157 143 146 158
		f 4 244 174 -246 242
		mu 0 4 158 146 159 160
		f 4 245 175 -247 239
		mu 0 4 160 159 151 161
		f 4 246 176 169 236
		mu 0 4 161 151 152 162
		f 4 220 -255 -220 218
		mu 0 4 104 103 163 164
		f 4 -529 527 525 -524
		mu 0 4 165 166 167 168
		f 4 -526 533 532 -531
		mu 0 4 168 167 169 170
		f 4 251 224 -253 -258
		mu 0 4 171 172 173 174
		f 4 -149 202 204 -204
		mu 0 4 175 176 141 140
		f 4 203 205 -198 -159
		mu 0 4 175 140 142 177
		f 4 195 -209 -208 155
		mu 0 4 178 153 155 179
		f 4 -211 -210 495 207
		mu 0 4 155 154 180 179
		f 4 217 -219 -217 177
		mu 0 4 88 104 164 89
		f 4 216 219 -254 -170
		mu 0 4 89 164 163 181
		f 4 252 -222 -244 -259
		mu 0 4 174 173 182 183
		f 4 221 -225 -224 180
		mu 0 4 182 173 172 184
		f 6 226 227 -319 -321 -427 -160
		mu 0 6 175 78 81 185 186 187
		f 4 229 -232 -227 158
		mu 0 4 188 79 78 175
		f 4 232 235 -235 -156
		mu 0 4 189 101 100 190
		f 4 247 -196 234 253
		mu 0 4 163 153 191 181
		f 4 248 -197 -248 254
		mu 0 4 103 122 153 163
		f 4 194 -249 255 249
		mu 0 4 126 122 103 102
		f 4 -536 523 534 -516
		mu 0 4 129 165 168 130
		f 4 -535 530 536 -522
		mu 0 4 130 168 170 132
		f 4 256 -226 -200 -251
		mu 0 4 95 171 137 136
		f 4 225 257 -223 -201
		mu 0 4 137 171 174 142
		f 4 197 222 258 -230
		mu 0 4 192 142 174 183
		f 4 -262 -261 148 259
		mu 0 4 193 194 176 175
		f 4 262 264 -264 -496
		mu 0 4 195 196 197 189
		f 4 -268 -267 261 265
		mu 0 4 198 199 194 193
		f 4 -265 268 270 -270
		mu 0 4 197 196 200 201
		f 4 -274 -273 267 271
		mu 0 4 202 203 199 198
		f 4 -271 274 276 -276
		mu 0 4 201 200 204 205
		f 4 277 279 -279 -165
		mu 0 4 147 111 114 149
		f 4 278 281 -281 -162
		mu 0 4 149 114 113 206
		f 4 280 -284 -283 151
		mu 0 4 206 113 112 207
		f 4 285 286 -278 -164
		mu 0 4 144 120 111 147
		f 4 282 -289 -288 153
		mu 0 4 207 112 121 97
		f 4 287 -290 -286 160
		mu 0 4 97 121 120 144
		f 4 290 292 -292 -181
		mu 0 4 184 208 209 182
		f 5 291 294 488 -294 -166
		mu 0 5 182 209 210 211 212
		f 4 293 296 -296 -161
		mu 0 4 212 211 213 214
		f 4 295 -484 -490 482
		mu 0 4 214 213 215 216
		f 4 298 300 -300 -293
		mu 0 4 208 217 218 209
		f 4 487 -302 -489 -495
		mu 0 4 219 220 211 210
		f 4 301 304 -304 -297
		mu 0 4 211 220 221 213
		f 4 303 -485 -491 483
		mu 0 4 213 221 222 215
		f 4 306 308 -308 -301
		mu 0 4 217 223 224 218
		f 4 486 -310 -488 -494
		mu 0 4 85 84 220 219
		f 4 309 312 -312 -305
		mu 0 4 220 84 83 221
		f 4 311 -486 -492 484
		mu 0 4 221 83 82 222
		f 4 -214 -212 185 212
		mu 0 4 225 226 105 108
		f 4 -315 -213 189 214
		mu 0 4 227 225 108 119
		f 4 -316 -215 192 215
		mu 0 4 156 227 119 92
		f 7 318 241 -323 -325 -441 -318 319
		mu 0 7 185 81 109 228 229 230 231
		f 7 322 238 -327 -329 -455 -322 323
		mu 0 7 228 109 98 232 233 234 235
		f 6 326 233 -233 -469 -326 327
		mu 0 6 232 98 101 189 236 237
		f 4 502 -431 -502 -375
		mu 0 4 238 239 240 241
		f 4 504 -445 -504 -377
		mu 0 4 242 243 244 245
		f 4 506 -459 -506 -379
		mu 0 4 246 247 248 249
		f 4 -277 -473 -508 -381
		mu 0 4 205 204 250 251
		f 5 209 213 332 -476 -383
		mu 0 5 195 226 225 252 253
		f 5 331 314 336 -462 -384
		mu 0 5 254 225 227 255 256
		f 5 335 315 340 -448 -385
		mu 0 5 257 227 156 258 259
		f 5 339 316 -203 -434 -386
		mu 0 5 260 156 141 176 261
		f 4 -428 -435 426 343
		mu 0 4 262 263 187 186
		f 4 -388 338 385 -440
		mu 0 4 264 265 260 261
		f 4 -442 -449 440 349
		mu 0 4 266 267 230 229
		f 4 -390 334 384 -454
		mu 0 4 268 269 257 259
		f 4 -456 -463 454 355
		mu 0 4 270 271 234 233
		f 4 -392 330 383 -468
		mu 0 4 272 273 254 256
		f 4 263 -470 -477 468
		mu 0 4 189 197 274 236
		f 4 -394 -263 382 -482
		mu 0 4 275 196 195 253
		f 4 345 -429 -436 427
		mu 0 4 262 276 277 263
		f 4 -396 342 387 -439
		mu 0 4 278 279 265 264
		f 4 351 -443 -450 441
		mu 0 4 266 280 281 267
		f 4 -398 348 389 -453
		mu 0 4 282 283 269 268
		f 4 357 -457 -464 455
		mu 0 4 270 284 285 271
		f 4 -400 354 391 -467
		mu 0 4 286 287 273 272
		f 4 269 -471 -478 469
		mu 0 4 197 201 288 274
		f 4 -402 -269 393 -481
		mu 0 4 289 200 196 275
		f 4 361 374 -437 428
		mu 0 4 276 238 241 277
		f 4 430 360 395 -438
		mu 0 4 240 239 279 278
		f 4 365 376 -451 442
		mu 0 4 280 242 245 281
		f 4 444 364 397 -452
		mu 0 4 244 243 283 282
		f 4 369 378 -465 456
		mu 0 4 284 246 249 285
		f 4 458 368 399 -466
		mu 0 4 248 247 287 286
		f 4 275 380 -479 470
		mu 0 4 201 205 251 288
		f 4 472 -275 401 -480
		mu 0 4 250 204 200 289
		f 3 402 -341 -340
		mu 0 3 260 258 156
		f 3 403 -337 -336
		mu 0 3 257 255 227
		f 3 404 -333 -332
		mu 0 3 254 252 225
		f 4 -406 -339 406 -344
		mu 0 4 186 260 265 262
		f 4 -407 -343 407 -346
		mu 0 4 262 265 279 276
		f 4 -408 -361 -503 -362
		mu 0 4 276 279 239 238
		f 4 -372 -360 408 -363
		mu 0 4 290 291 292 293
		f 4 -410 -365 -505 -366
		mu 0 4 280 283 243 242
		f 4 -373 -364 410 -367
		mu 0 4 294 295 296 297
		f 4 -412 -369 -507 -370
		mu 0 4 284 287 247 246
		f 4 -374 -368 412 -371
		mu 0 4 298 299 300 301
		f 4 -413 -354 413 -359
		mu 0 4 301 300 302 303
		f 4 -415 -355 411 -358
		mu 0 4 270 273 287 284
		f 4 -411 -348 415 -353
		mu 0 4 297 296 304 305
		f 4 -417 -349 409 -352
		mu 0 4 266 269 283 280
		f 4 -409 -342 417 -347
		mu 0 4 293 292 306 307
		f 4 -418 -338 418 -345
		mu 0 4 307 306 258 231
		f 4 -420 -335 416 -350
		mu 0 4 229 257 269 266
		f 4 -416 -334 420 -351
		mu 0 4 305 304 255 235
		f 4 -422 -331 414 -356
		mu 0 4 233 254 273 270
		f 4 -414 -330 422 -357
		mu 0 4 303 302 252 237
		f 4 -423 -405 421 423
		mu 0 4 237 252 254 233
		f 4 -421 -404 419 424
		mu 0 4 235 255 257 229
		f 4 -419 -403 405 425
		mu 0 4 231 258 260 186
		f 3 -320 -426 320
		mu 0 3 185 231 186
		f 3 -324 -425 324
		mu 0 3 228 235 229
		f 3 -328 -424 328
		mu 0 3 232 237 233
		f 4 -387 -260 159 434
		mu 0 4 263 193 175 187
		f 4 -395 -266 386 435
		mu 0 4 277 198 193 263
		f 4 429 -272 394 436
		mu 0 4 241 202 198 277
		f 4 501 -376 273 -430
		mu 0 4 241 240 203 202
		f 4 272 375 437 431
		mu 0 4 199 203 240 278
		f 4 266 -432 438 432
		mu 0 4 194 199 278 264
		f 4 260 -433 439 433
		mu 0 4 176 194 264 261
		f 4 -389 344 317 448
		mu 0 4 267 307 231 230
		f 4 -397 346 388 449
		mu 0 4 281 293 307 267
		f 4 443 362 396 450
		mu 0 4 245 290 293 281
		f 4 503 -378 371 -444
		mu 0 4 245 244 291 290
		f 4 359 377 451 445
		mu 0 4 292 291 244 282
		f 4 341 -446 452 446
		mu 0 4 306 292 282 268
		f 4 337 -447 453 447
		mu 0 4 258 306 268 259
		f 4 -391 350 321 462
		mu 0 4 271 305 235 234
		f 4 -399 352 390 463
		mu 0 4 285 297 305 271
		f 4 457 366 398 464
		mu 0 4 249 294 297 285
		f 4 505 -380 372 -458
		mu 0 4 249 248 295 294
		f 4 363 379 465 459
		mu 0 4 296 295 248 286
		f 4 347 -460 466 460
		mu 0 4 304 296 286 272
		f 4 333 -461 467 461
		mu 0 4 255 304 272 256
		f 4 -393 356 325 476
		mu 0 4 274 303 237 236
		f 4 -401 358 392 477
		mu 0 4 288 301 303 274
		f 4 471 370 400 478
		mu 0 4 251 298 301 288
		f 4 507 -382 373 -472
		mu 0 4 251 250 299 298
		f 4 367 381 479 473
		mu 0 4 300 299 250 289
		f 4 353 -474 480 474
		mu 0 4 302 300 289 275
		f 4 329 -475 481 475
		mu 0 4 252 302 275 253
		f 4 -298 -291 172 489
		mu 0 4 215 208 184 216
		f 4 -306 -299 297 490
		mu 0 4 222 217 208 215
		f 4 -314 -307 305 491
		mu 0 4 82 223 217 222
		f 4 313 492 -311 -309
		mu 0 4 223 82 85 224
		f 4 307 310 493 -303
		mu 0 4 218 224 85 219
		f 4 299 302 494 -295
		mu 0 4 209 218 219 210
		f 4 -192 -499 508 499
		mu 0 4 118 107 127 308
		f 4 -194 -500 509 500
		mu 0 4 93 118 308 135
		f 4 510 512 -512 -509
		mu 0 4 127 128 131 308
		f 4 514 516 -511 -157
		mu 0 4 126 129 128 127
		f 4 511 518 -518 -510
		mu 0 4 308 131 133 135
		f 4 517 -521 -520 157
		mu 0 4 135 133 132 136
		f 4 524 -528 -527 178
		mu 0 4 117 167 166 115
		f 4 526 528 -523 -171
		mu 0 4 115 166 165 102
		f 4 529 -533 -532 171
		mu 0 4 95 170 169 96
		f 4 531 -534 -525 179
		mu 0 4 96 169 167 117
		f 4 522 535 -515 -250
		mu 0 4 102 165 129 126
		f 4 519 -537 -530 250
		mu 0 4 136 132 170 95
		f 4 170 537 -150 150
		mu 0 4 115 102 88 87
		f 4 223 -252 -257 538
		mu 0 4 94 172 171 95
		f 3 539 -483 -173
		mu 0 3 94 97 309
		f 4 166 543 -168 -175
		mu 0 4 146 145 148 159
		f 4 167 544 -169 -176
		mu 0 4 159 148 150 151
		f 8 624 -624 622 -622 -621 -620 -619 617
		mu 0 8 310 311 312 313 314 315 316 317
		f 8 555 -561 559 -559 557 -555 553 556
		mu 0 8 318 319 320 321 322 323 324 325
		f 4 -564 -563 -562 546
		mu 0 4 326 327 328 329
		f 4 -567 -566 -565 562
		mu 0 4 330 331 332 333
		f 4 -570 -569 -568 547
		mu 0 4 334 335 336 337
		f 4 -573 -572 -571 568
		mu 0 4 338 339 340 341
		f 4 -576 -575 -574 -551
		mu 0 4 342 343 344 345
		f 4 -579 -578 -577 574
		mu 0 4 346 347 348 349
		f 4 -582 -581 -580 552
		mu 0 4 350 351 352 353
		f 4 -585 -584 -583 580
		mu 0 4 351 354 355 356
		f 4 -587 554 -586 577
		mu 0 4 347 324 323 348
		f 4 -589 -557 -588 583
		mu 0 4 354 318 325 355
		f 4 -591 558 -590 565
		mu 0 4 331 322 321 332
		f 4 -593 560 -592 571
		mu 0 4 339 320 319 340
		f 4 575 548 579 -594
		mu 0 4 343 342 353 352
		f 4 -595 569 -546 561
		mu 0 4 328 335 334 329
		f 4 581 551 567 -596
		mu 0 4 357 358 359 360
		f 4 -550 573 -597 563
		mu 0 4 361 362 363 364
		f 4 582 -598 578 593
		mu 0 4 356 355 347 346
		f 4 576 -599 566 596
		mu 0 4 365 348 331 330
		f 4 564 -600 572 594
		mu 0 4 366 332 339 338
		f 4 570 -601 584 595
		mu 0 4 367 340 354 368
		f 4 587 -554 586 597
		mu 0 4 355 325 324 347
		f 4 585 -558 590 598
		mu 0 4 348 323 322 331
		f 4 589 -560 592 599
		mu 0 4 332 321 320 339
		f 4 591 -556 588 600
		mu 0 4 340 319 318 354
		f 4 602 -604 -602 550
		mu 0 4 345 369 370 342
		f 4 601 605 -605 -549
		mu 0 4 342 370 371 353
		f 4 604 607 -607 -553
		mu 0 4 353 371 372 350
		f 4 606 609 -609 -552
		mu 0 4 350 372 373 337
		f 4 608 611 -611 -548
		mu 0 4 337 373 374 334
		f 4 610 -614 -613 545
		mu 0 4 334 374 375 329
		f 4 612 615 -615 -547
		mu 0 4 329 375 376 326
		f 4 614 -617 -603 549
		mu 0 4 326 376 369 345
		f 4 628 -628 -627 625
		mu 0 4 377 378 379 380
		f 4 626 631 -631 -630
		mu 0 4 380 379 381 382
		f 4 630 634 -634 -633
		mu 0 4 382 381 383 384
		f 4 633 637 -637 -636
		mu 0 4 384 383 385 386
		f 4 636 640 -640 -639
		mu 0 4 386 385 387 388
		f 4 639 -644 -643 641
		mu 0 4 388 387 389 390
		f 4 642 646 -646 -645
		mu 0 4 390 389 391 392
		f 4 645 -649 -629 647
		mu 0 4 392 391 378 377
		f 4 650 -618 -650 627
		mu 0 4 378 310 317 379
		f 4 649 618 -652 -632
		mu 0 4 379 317 316 381
		f 4 651 619 -653 -635
		mu 0 4 381 316 315 383
		f 4 652 620 -654 -638
		mu 0 4 383 315 314 385
		f 4 653 621 -655 -641
		mu 0 4 385 314 313 387
		f 4 654 -623 -656 643
		mu 0 4 387 313 312 389
		f 4 655 623 -657 -647
		mu 0 4 389 312 311 391
		f 4 656 -625 -651 648
		mu 0 4 391 311 310 378
		f 4 664 -664 -662 659
		mu 0 4 393 394 395 396
		f 4 661 669 -669 -667
		mu 0 4 396 395 397 398
		f 4 668 674 -674 -672
		mu 0 4 398 397 399 400
		f 4 673 679 -679 -677
		mu 0 4 400 399 401 402
		f 4 678 684 -684 -682
		mu 0 4 402 401 403 404
		f 4 683 -690 -689 686
		mu 0 4 404 403 405 406
		f 4 688 694 -694 -692
		mu 0 4 406 405 407 408
		f 4 693 -697 -665 695
		mu 0 4 408 407 394 393
		f 4 658 -660 -658 603
		mu 0 4 369 393 396 370
		f 4 660 663 -663 -626
		mu 0 4 380 395 394 377
		f 4 657 666 -666 -606
		mu 0 4 370 396 398 371
		f 4 667 -670 -661 629
		mu 0 4 382 397 395 380
		f 4 665 671 -671 -608
		mu 0 4 371 398 400 372
		f 4 672 -675 -668 632
		mu 0 4 384 399 397 382
		f 4 670 676 -676 -610
		mu 0 4 372 400 402 373
		f 4 677 -680 -673 635
		mu 0 4 386 401 399 384
		f 4 675 681 -681 -612
		mu 0 4 373 402 404 374
		f 4 682 -685 -678 638
		mu 0 4 388 403 401 386
		f 4 680 -687 -686 613
		mu 0 4 374 404 406 375
		f 4 687 689 -683 -642
		mu 0 4 390 405 403 388
		f 4 685 691 -691 -616
		mu 0 4 375 406 408 376
		f 4 692 -695 -688 644
		mu 0 4 392 407 405 390
		f 4 690 -696 -659 616
		mu 0 4 376 408 393 369
		f 4 662 696 -693 -648
		mu 0 4 377 394 407 392;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Basic_Head";
	rename -uid "4080A5D6-423D-F98B-C8A4-7F891612AF16";
	addAttr -is true -ci true -k true -sn "currentUVSet" -ln "currentUVSet" -dt "string";
	setAttr ".v" no;
	setAttr -l on ".t" -type "double3" 0 34.747142791748047 0 ;
	setAttr -l on ".t";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".r";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".s";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr -k on ".currentUVSet" -type "string" "map1";
createNode mesh -n "Basic_HeadShape" -p "Basic_Head";
	rename -uid "1E3D4C19-4BB8-C6F1-3216-EEBFD8DFC96B";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000005960464478 0.6875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Basic_HeadShapeOrig" -p "Basic_Head";
	rename -uid "B9A01B41-45E3-C011-C8E9-BFBBC54FF138";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 94 ".uvst[0].uvsp[0:93]" -type "float2" 0 0.5 0.1 0.5 0.2
		 0.5 0.30000001 0.5 0.40000001 0.5 0.5 0.5 0.60000002 0.5 0.70000005 0.5 0.80000007
		 0.5 0.9000001 0.5 1.000000119209 0.5 0 0.625 0.1 0.625 0.2 0.625 0.30000001 0.625
		 0.40000001 0.625 0.5 0.625 0.60000002 0.625 0.70000005 0.625 0.80000007 0.625 0.9000001
		 0.625 1.000000119209 0.625 0 0.75 0.1 0.75 0.2 0.75 0.30000001 0.75 0.40000001 0.75
		 0.5 0.75 0.60000002 0.75 0.70000005 0.75 0.80000007 0.75 0.9000001 0.75 1.000000119209
		 0.75 0 0.875 0.1 0.875 0.2 0.875 0.30000001 0.875 0.40000001 0.875 0.5 0.875 0.60000002
		 0.875 0.70000005 0.875 0.80000007 0.875 0.9000001 0.875 1.000000119209 0.875 1.000000119209
		 0.5625 0 0.5625 0.1 0.5625 0.2 0.5625 0.30000001 0.5625 0.40000001 0.5625 0.5 0.5625
		 0.60000002 0.5625 0.70000005 0.5625 0.80000007 0.5625 0.9000001 0.5625 0 0.5 0.1
		 0.5 0.1 0.5625 0 0.5625 0.2 0.5 0.2 0.5625 0.30000001 0.5 0.30000001 0.5625 0.40000001
		 0.5 0.40000001 0.5625 0.5 0.5 0.5 0.5625 0.60000002 0.5 0.60000002 0.5625 0.70000005
		 0.5 0.70000005 0.5625 0.80000007 0.5 0.80000007 0.5625 0.9000001 0.5 0.9000001 0.5625
		 1.000000119209 0.5 1.000000119209 0.5625 0.15000001 0.5 0.15000001 0.5 0.15000001
		 0.5625 0.15000001 0.5625 0.15000001 0.625 0.15000001 0.75 0.15000001 0.875 0.15000001
		 0.875 0.65000004 0.875 0.65000004 0.875 0.65000004 0.875 0.65000004 0.75 0.65000004
		 0.625 0.65000004 0.5625 0.65000004 0.5625 0.65000004 0.5 0.65000004 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 87 ".vt[0:86]"  6.88318539 -1.57736993 9.44599152 11.1372242 -1.57736993 3.5908072
		 11.13722324 -1.57736993 -6.77491045 6.8831811 -1.57736993 -10.9535656 -4.3426412e-06 -1.57736993 -11.54128933
		 -6.88318443 -1.57736993 -10.95355797 -11.13722515 -1.57736993 -6.77489567 -11.13722515 -1.57736993 3.59080648
		 -6.88318634 -1.57736993 9.44599152 -9.0637093e-07 -1.57736993 10.97887611 6.35923052 2.70730996 8.7248354
		 10.28945255 2.70730996 3.31535077 10.28945351 2.70730996 -6.23254967 6.35922956 2.70730996 -10.23240948
		 -1.7654386e-06 2.70730996 -10.88729 -6.35923195 2.70730996 -10.2324028 -10.28945541 2.70730996 -6.23251867
		 -10.28945255 2.70730996 3.3153491 -6.35923243 2.70730996 8.7248354 -4.7303367e-08 2.70730996 10.087478638
		 4.86714506 6.33968639 6.67115498 7.87520647 6.33968639 2.53091455 7.87520552 6.33968639 -4.68803644
		 4.86714411 6.33968639 -8.1787262 -3.4835737e-06 6.33968639 -9.024856567 -4.86714649 6.33968639 -8.1787281
		 -7.87520838 6.33968639 -4.68802547 -7.87520504 6.33968639 2.53091288 -4.86714602 6.33968639 6.67115259
		 -9.0637093e-07 6.33968639 7.54898882 2.63407969 8.76676369 3.59760427 4.26203012 8.76676369 1.35692143
		 4.2620306 8.76676369 -2.37650394 2.63407898 8.76676369 -5.10517645 -1.3359047e-06 8.76676369 -5.60426188
		 -2.6340816 8.76676369 -5.10517645 -4.26203299 8.76676369 -2.37650204 -4.26203299 8.76676369 1.35692191
		 -2.63408089 8.76676369 3.59760427 -1.7654386e-06 8.76676369 3.74987221 6.62120819 0.5649699 9.085413933
		 10.71333885 0.5649699 3.45307899 10.71333885 0.5649699 -6.50372982 6.62120533 0.5649699 -10.59298801
		 -3.05404e-06 0.5649699 -11.21428967 -6.62120819 0.5649699 -10.59298134 -10.71334076 0.5649699 -6.50370693
		 -10.71333885 0.5649699 3.45307779 -6.62120914 0.5649699 9.085413933 -4.7683716e-07 0.5649699 10.53317738
		 7.3448329 -1.57736993 10.09838295 11.88418484 -1.57736993 3.85049868 11.43186951 0.5649699 3.70353317
		 7.065284729 0.5649699 9.71362114 11.88418388 -1.57736993 -7.21043539 11.43186951 0.5649699 -6.92106724
		 7.34482765 -1.57736993 -11.66934776 7.065281868 0.5649699 -11.28458691 -4.6019163e-06 -1.57736993 -12.29648972
		 -3.2268897e-06 0.5649699 -11.9475584 -7.34483194 -1.57736993 -11.66934013 -7.065285206 0.5649699 -11.28457928
		 -11.88418579 -1.57736993 -7.21041965 -11.43187141 0.5649699 -6.92104244 -11.88418579 -1.57736993 3.85049772
		 -11.43187046 0.5649699 3.70353198 -7.34483385 -1.57736993 10.09838295 -7.065285683 0.5649699 9.71362114
		 -9.3517929e-07 -1.57736993 11.7340765 -4.7683716e-07 0.5649699 11.25848484 11.93354607 -1.57736993 -1.59205163
		 12.73391533 -1.57737112 -1.67996836 12.24925995 0.56496871 -1.60876703 11.4793539 0.56496871 -1.52532542
		 11.025160789 2.70730877 -1.45859945 8.4382906 6.33968639 -1.078560948 4.56677008 8.76676369 -0.50979125
		 2.82241869 9.22521877 -0.75378609 -1.6274519e-06 9.22521877 -0.61055756 -2.8224206 9.22521877 -0.75378609
		 -4.56677246 8.76676369 -0.50979006 -8.43829155 6.33968639 -1.078556299 -11.025158882 2.70730996 -1.45858479
		 -11.4793539 0.5649699 -1.52531457 -12.2492609 0.5649699 -1.60875523 -12.73391628 -1.57736993 -1.67996097
		 -11.93354797 -1.57736993 -1.59204459;
	setAttr -s 166 ".ed[0:165]"  40 41 0 41 11 1 11 10 1 10 40 1 41 73 0 73 74 1
		 74 11 1 42 43 0 43 13 1 13 12 1 12 42 1 43 44 0 44 14 1 14 13 1 44 45 0 45 15 1 15 14 1
		 45 46 0 46 16 1 16 15 1 82 83 1 83 47 0 47 17 1 17 82 1 47 48 0 48 18 1 18 17 1 48 49 0
		 49 19 1 19 18 1 49 40 0 10 19 1 11 21 1 21 20 1 20 10 1 74 75 1 75 21 1 13 23 1 23 22 1
		 22 12 1 14 24 1 24 23 1 15 25 1 25 24 1 16 26 1 26 25 1 81 82 1 17 27 1 27 81 1 18 28 1
		 28 27 1 19 29 1 29 28 1 20 29 1 21 31 1 31 30 1 30 20 1 75 76 1 76 31 1 23 33 1 33 32 1
		 32 22 1 24 34 1 34 33 1 25 35 1 35 34 1 26 36 1 36 35 1 80 81 1 27 37 1 37 80 1 28 38 1
		 38 37 1 29 39 1 39 38 1 30 39 1 38 79 1 79 80 1 50 51 0 51 52 1 52 53 0 53 50 1 51 71 0
		 71 72 1 72 52 0 54 56 0 56 57 1 57 55 0 55 54 1 56 58 0 58 59 1 59 57 0 58 60 0 60 61 1
		 61 59 0 60 62 0 62 63 1 63 61 0 84 85 1 85 64 0 64 65 1 65 84 0 64 66 0 66 67 1 67 65 0
		 66 68 0 68 69 1 69 67 0 68 50 0 53 69 0 0 1 0 1 51 1 50 0 1 40 53 1 52 41 1 1 70 0
		 70 71 1 72 73 1 2 3 0 3 56 1 54 2 1 42 55 1 57 43 1 3 4 0 4 58 1 59 44 1 4 5 0 5 60 1
		 61 45 1 5 6 0 6 62 1 63 46 1 85 86 1 86 7 0 7 64 1 83 84 1 65 47 1 7 8 0 8 66 1 67 48 1
		 8 9 0 9 68 1 69 49 1 9 0 0 30 77 1 77 78 1 78 39 1 78 79 1 76 77 1 70 2 0 54 71 0
		 55 72 0 42 73 0 12 74 1 22 75 1 32 76 1 33 77 1 34 78 1 35 79 1 36 80 1 26 81 1 16 82 1
		 46 83 0 63 84 0 62 85 0 6 86 0;
	setAttr -s 123 ".n[0:122]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20;
	setAttr -s 80 -ch 320 ".fc[0:79]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 45 46 12 11
		f 4 4 5 6 -2
		mu 0 4 46 80 81 12
		f 4 7 8 9 10
		mu 0 4 47 48 14 13
		f 4 11 12 13 -9
		mu 0 4 48 49 15 14
		f 4 14 15 16 -13
		mu 0 4 49 50 16 15
		f 4 17 18 19 -16
		mu 0 4 50 51 17 16
		f 4 20 21 22 23
		mu 0 4 89 90 52 18
		f 4 24 25 26 -23
		mu 0 4 52 53 19 18
		f 4 27 28 29 -26
		mu 0 4 53 54 20 19
		f 4 30 -4 31 -29
		mu 0 4 54 44 21 20
		f 4 -3 32 33 34
		mu 0 4 11 12 23 22
		f 4 -7 35 36 -33
		mu 0 4 12 81 82 23
		f 4 -10 37 38 39
		mu 0 4 13 14 25 24
		f 4 -14 40 41 -38
		mu 0 4 14 15 26 25
		f 4 -17 42 43 -41
		mu 0 4 15 16 27 26
		f 4 -20 44 45 -43
		mu 0 4 16 17 28 27
		f 4 46 -24 47 48
		mu 0 4 88 89 18 29
		f 4 -27 49 50 -48
		mu 0 4 18 19 30 29
		f 4 -30 51 52 -50
		mu 0 4 19 20 31 30
		f 4 -32 -35 53 -52
		mu 0 4 20 21 32 31
		f 4 -34 54 55 56
		mu 0 4 22 23 34 33
		f 4 -37 57 58 -55
		mu 0 4 23 82 83 34
		f 4 -39 59 60 61
		mu 0 4 24 25 36 35
		f 4 -42 62 63 -60
		mu 0 4 25 26 37 36
		f 4 -44 64 65 -63
		mu 0 4 26 27 38 37
		f 4 -46 66 67 -65
		mu 0 4 27 28 39 38
		f 4 68 -49 69 70
		mu 0 4 87 88 29 40
		f 4 -51 71 72 -70
		mu 0 4 29 30 41 40
		f 4 -53 73 74 -72
		mu 0 4 30 31 42 41
		f 4 -54 -57 75 -74
		mu 0 4 31 32 43 42
		f 4 76 77 -71 -73
		mu 0 4 41 86 87 40
		f 4 78 79 80 81
		mu 0 4 55 56 57 58
		f 4 82 83 84 -80
		mu 0 4 56 78 79 57
		f 4 85 86 87 88
		mu 0 4 59 61 62 60
		f 4 89 90 91 -87
		mu 0 4 61 63 64 62
		f 4 92 93 94 -91
		mu 0 4 63 65 66 64
		f 4 95 96 97 -94
		mu 0 4 65 67 68 66
		f 4 98 99 100 101
		mu 0 4 91 92 69 70
		f 4 102 103 104 -101
		mu 0 4 69 71 72 70
		f 4 105 106 107 -104
		mu 0 4 71 73 74 72
		f 4 108 -82 109 -107
		mu 0 4 73 75 76 74
		f 4 110 111 -79 112
		mu 0 4 0 1 56 55
		f 4 -1 113 -81 114
		mu 0 4 46 45 58 57
		f 4 115 116 -83 -112
		mu 0 4 1 77 78 56
		f 4 117 -5 -115 -85
		mu 0 4 79 80 46 57
		f 4 118 119 -86 120
		mu 0 4 2 3 61 59
		f 4 -8 121 -88 122
		mu 0 4 48 47 60 62
		f 4 123 124 -90 -120
		mu 0 4 3 4 63 61
		f 4 -12 -123 -92 125
		mu 0 4 49 48 62 64
		f 4 126 127 -93 -125
		mu 0 4 4 5 65 63
		f 4 -15 -126 -95 128
		mu 0 4 50 49 64 66
		f 4 129 130 -96 -128
		mu 0 4 5 6 67 65
		f 4 -18 -129 -98 131
		mu 0 4 51 50 66 68
		f 4 132 133 134 -100
		mu 0 4 92 93 7 69
		f 4 -22 135 -102 136
		mu 0 4 52 90 91 70
		f 4 137 138 -103 -135
		mu 0 4 7 8 71 69
		f 4 -25 -137 -105 139
		mu 0 4 53 52 70 72
		f 4 140 141 -106 -139
		mu 0 4 8 9 73 71
		f 4 -28 -140 -108 142
		mu 0 4 54 53 72 74
		f 4 143 -113 -109 -142
		mu 0 4 9 10 75 73
		f 4 -31 -143 -110 -114
		mu 0 4 44 54 74 76
		f 4 -76 144 145 146
		mu 0 4 42 33 84 85
		f 4 -147 147 -77 -75
		mu 0 4 42 85 86 41
		f 4 148 -145 -56 -59
		mu 0 4 83 84 33 34
		f 4 -117 149 -121 150
		mu 0 4 78 77 2 59
		f 4 -84 -151 -89 151
		mu 0 4 79 78 59 60
		f 4 152 -118 -152 -122
		mu 0 4 47 80 79 60
		f 4 -6 -153 -11 153
		mu 0 4 81 80 47 13
		f 4 -36 -154 -40 154
		mu 0 4 82 81 13 24
		f 4 -58 -155 -62 155
		mu 0 4 83 82 24 35
		f 4 156 -149 -156 -61
		mu 0 4 36 84 83 35
		f 4 -146 -157 -64 157
		mu 0 4 85 84 36 37
		f 4 -148 -158 -66 158
		mu 0 4 86 85 37 38
		f 4 -78 -159 -68 159
		mu 0 4 87 86 38 39
		f 4 160 -69 -160 -67
		mu 0 4 28 88 87 39
		f 4 161 -47 -161 -45
		mu 0 4 17 89 88 28
		f 4 162 -21 -162 -19
		mu 0 4 51 90 89 17
		f 4 -136 -163 -132 163
		mu 0 4 91 90 51 68
		f 4 164 -99 -164 -97
		mu 0 4 67 92 91 68
		f 4 165 -133 -165 -131
		mu 0 4 6 93 92 67;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Basic_HeadShapeOrig1" -p "Basic_Head";
	rename -uid "7AA5EC3E-41FE-A5CC-78F3-938283E58DC1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 94 ".uvst[0].uvsp[0:93]" -type "float2" 0 0.5 0.1 0.5 0.2
		 0.5 0.30000001 0.5 0.40000001 0.5 0.5 0.5 0.60000002 0.5 0.70000005 0.5 0.80000007
		 0.5 0.9000001 0.5 1.000000119209 0.5 0 0.625 0.1 0.625 0.2 0.625 0.30000001 0.625
		 0.40000001 0.625 0.5 0.625 0.60000002 0.625 0.70000005 0.625 0.80000007 0.625 0.9000001
		 0.625 1.000000119209 0.625 0 0.75 0.1 0.75 0.2 0.75 0.30000001 0.75 0.40000001 0.75
		 0.5 0.75 0.60000002 0.75 0.70000005 0.75 0.80000007 0.75 0.9000001 0.75 1.000000119209
		 0.75 0 0.875 0.1 0.875 0.2 0.875 0.30000001 0.875 0.40000001 0.875 0.5 0.875 0.60000002
		 0.875 0.70000005 0.875 0.80000007 0.875 0.9000001 0.875 1.000000119209 0.875 1.000000119209
		 0.5625 0 0.5625 0.1 0.5625 0.2 0.5625 0.30000001 0.5625 0.40000001 0.5625 0.5 0.5625
		 0.60000002 0.5625 0.70000005 0.5625 0.80000007 0.5625 0.9000001 0.5625 0 0.5 0.1
		 0.5 0.1 0.5625 0 0.5625 0.2 0.5 0.2 0.5625 0.30000001 0.5 0.30000001 0.5625 0.40000001
		 0.5 0.40000001 0.5625 0.5 0.5 0.5 0.5625 0.60000002 0.5 0.60000002 0.5625 0.70000005
		 0.5 0.70000005 0.5625 0.80000007 0.5 0.80000007 0.5625 0.9000001 0.5 0.9000001 0.5625
		 1.000000119209 0.5 1.000000119209 0.5625 0.15000001 0.5 0.15000001 0.5 0.15000001
		 0.5625 0.15000001 0.5625 0.15000001 0.625 0.15000001 0.75 0.15000001 0.875 0.15000001
		 0.875 0.65000004 0.875 0.65000004 0.875 0.65000004 0.875 0.65000004 0.75 0.65000004
		 0.625 0.65000004 0.5625 0.65000004 0.5625 0.65000004 0.5 0.65000004 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 87 ".vt[0:86]"  6.88318539 -1.57736826 10.21079159 11.1372242 -1.57736826 7.037137032
		 11.13722324 -1.57736826 -9.825243 6.8831811 -1.57736826 -12.024295807 -4.3426412e-06 -1.57736826 -12.32829571
		 -6.88318443 -1.57736826 -12.024288177 -11.13722515 -1.57736826 -9.82522869 -11.13722515 -1.57736826 7.037136078
		 -6.88318634 -1.57736826 10.21079159 -9.0637093e-07 -1.57736826 11.76371479 6.35923052 2.71253681 9.52237797
		 10.28945255 2.70731163 6.37029552 10.28945351 2.70731163 -8.93268108 6.35922956 2.70994925 -11.22751236
		 -1.7654386e-06 2.78328514 -11.55166054 -6.35923195 2.70994925 -11.22750568 -10.28945541 2.70731163 -8.93264961
		 -10.28945255 2.70731163 6.37029362 -6.35923243 2.71253681 9.52237797 -4.7303363e-08 2.7135725 10.76035976
		 4.86714506 7.30032635 5.71471834 7.87520647 7.32714558 4.51990795 7.87520552 7.36877537 -6.43766832
		 4.86714411 7.33299208 -7.28804445 -3.4835737e-06 7.27889872 -7.7418232 -4.86714649 7.33299208 -7.28804588
		 -7.87520838 7.36877537 -6.43765545 -7.87520504 7.32714558 4.51990652 -4.86714602 7.30032635 5.71471596
		 -9.0637093e-07 7.27889872 6.44403219 2.63407969 8.77926636 3.58894658 4.26203012 8.76898289 1.3584615
		 4.2620306 8.76676559 -2.37650394 2.63407898 8.7752142 -5.09903574 -1.3359047e-06 8.82568169 -5.55664587
		 -2.6340816 8.7752142 -5.09903574 -4.26203299 8.76676559 -2.37650204 -4.26203299 8.76898289 1.35846198
		 -2.63408089 8.77926636 3.58894658 -1.7654386e-06 8.7774868 3.74218082 6.62120819 0.56497151 9.99960804
		 10.71333885 0.56497151 6.89940882 10.71333885 0.56497151 -9.55406284 6.62120533 0.56497151 -11.74577141
		 -3.05404e-06 0.56497151 -12.0012960434 -6.62120819 0.56497151 -11.74576473 -10.71334076 0.56497151 -9.55403996
		 -10.71333885 0.56497151 6.89940739 -6.62120914 0.56497151 9.99960804 -4.7683716e-07 0.56497151 11.31887722
		 7.3448329 -1.57736826 10.71343613 11.88418484 -1.57736826 7.29682827 11.43186951 0.56497151 7.14986324
		 7.065284729 0.56497151 10.47073936 11.88418388 -1.57736826 -10.26076794 11.43186951 0.56497151 -9.97140026
		 7.34482765 -1.57736826 -12.63673973 7.065281868 0.56497151 -12.3321991 -4.6019163e-06 -1.57736826 -13.083496094
		 -3.2268897e-06 0.56497151 -12.73456478 -7.34483194 -1.57736826 -12.6367321 -7.065285206 0.56497151 -12.33219147
		 -11.88418579 -1.57736826 -10.26075268 -11.43187141 0.56497151 -9.97137547 -11.88418579 -1.57736826 7.29682732
		 -11.43187046 0.56497151 7.14986181 -7.34483385 -1.57736826 10.71343613 -7.065285683 0.56497151 10.47073936
		 -9.3517929e-07 -1.57736826 12.53006363 -4.7683716e-07 0.56497151 12.05447197 11.93354607 -1.57736826 -0.70018363
		 12.73391533 -1.57736945 -0.9640305 12.24925995 0.56497031 -0.81527096 11.4793539 0.56497031 -0.56056672
		 11.025160789 2.73172736 -0.59226036 8.4382906 7.20939684 -0.75328529 4.56677008 8.78191471 -0.50765419
		 2.82241869 9.22522068 -0.75378609 -1.6274519e-06 9.22522068 -0.61055756 -2.8224206 9.22522068 -0.75378609
		 -4.56677246 8.78191471 -0.507653 -8.43829155 7.20939684 -0.75327945 -11.025158882 2.73172855 -0.59224558
		 -11.4793539 0.56497151 -0.56055588 -12.2492609 0.56497151 -0.81525916 -12.73391628 -1.57736826 -0.96402311
		 -11.93354797 -1.57736826 -0.7001766;
	setAttr -s 166 ".ed[0:165]"  40 41 0 41 11 0 11 10 1 10 40 1 41 73 0 73 74 1
		 74 11 1 42 43 0 43 13 1 13 12 1 12 42 0 43 44 0 44 14 1 14 13 1 44 45 0 45 15 1 15 14 1
		 45 46 0 46 16 0 16 15 1 82 83 1 83 47 0 47 17 0 17 82 1 47 48 0 48 18 1 18 17 1 48 49 0
		 49 19 1 19 18 1 49 40 0 10 19 1 11 21 0 21 20 0 20 10 1 74 75 1 75 21 0 13 23 1 23 22 0
		 22 12 0 14 24 1 24 23 0 15 25 1 25 24 0 16 26 0 26 25 0 81 82 1 17 27 0 27 81 0 18 28 1
		 28 27 0 19 29 1 29 28 0 20 29 0 21 31 1 31 30 1 30 20 1 75 76 1 76 31 1 23 33 1 33 32 1
		 32 22 1 24 34 1 34 33 1 25 35 1 35 34 1 26 36 1 36 35 1 80 81 1 27 37 1 37 80 1 28 38 1
		 38 37 1 29 39 1 39 38 1 30 39 1 38 79 1 79 80 1 50 51 0 51 52 0 52 53 0 53 50 1 51 71 0
		 71 72 1 72 52 0 54 56 0 56 57 1 57 55 0 55 54 0 56 58 0 58 59 1 59 57 0 58 60 0 60 61 1
		 61 59 0 60 62 0 62 63 0 63 61 0 84 85 1 85 64 0 64 65 0 65 84 0 64 66 0 66 67 1 67 65 0
		 66 68 0 68 69 1 69 67 0 68 50 0 53 69 0 0 1 0 1 51 0 50 0 1 40 53 1 52 41 0 1 70 0
		 70 71 1 72 73 1 2 3 0 3 56 1 54 2 0 42 55 0 57 43 1 3 4 0 4 58 1 59 44 1 4 5 0 5 60 1
		 61 45 1 5 6 0 6 62 0 63 46 0 85 86 1 86 7 0 7 64 0 83 84 1 65 47 0 7 8 0 8 66 1 67 48 1
		 8 9 0 9 68 1 69 49 1 9 0 0 30 77 1 77 78 1 78 39 1 78 79 1 76 77 1 70 2 0 54 71 0
		 55 72 0 42 73 0 12 74 1 22 75 0 32 76 1 33 77 1 34 78 1 35 79 1 36 80 1 26 81 0 16 82 1
		 46 83 0 63 84 0 62 85 0 6 86 0;
	setAttr -s 171 ".n";
	setAttr ".n[0:165]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20;
	setAttr ".n[166:170]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20;
	setAttr -s 80 -ch 320 ".fc[0:79]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 45 46 12 11
		f 4 4 5 6 -2
		mu 0 4 46 80 81 12
		f 4 7 8 9 10
		mu 0 4 47 48 14 13
		f 4 11 12 13 -9
		mu 0 4 48 49 15 14
		f 4 14 15 16 -13
		mu 0 4 49 50 16 15
		f 4 17 18 19 -16
		mu 0 4 50 51 17 16
		f 4 20 21 22 23
		mu 0 4 89 90 52 18
		f 4 24 25 26 -23
		mu 0 4 52 53 19 18
		f 4 27 28 29 -26
		mu 0 4 53 54 20 19
		f 4 30 -4 31 -29
		mu 0 4 54 44 21 20
		f 4 -3 32 33 34
		mu 0 4 11 12 23 22
		f 4 -7 35 36 -33
		mu 0 4 12 81 82 23
		f 4 -10 37 38 39
		mu 0 4 13 14 25 24
		f 4 -14 40 41 -38
		mu 0 4 14 15 26 25
		f 4 -17 42 43 -41
		mu 0 4 15 16 27 26
		f 4 -20 44 45 -43
		mu 0 4 16 17 28 27
		f 4 46 -24 47 48
		mu 0 4 88 89 18 29
		f 4 -27 49 50 -48
		mu 0 4 18 19 30 29
		f 4 -30 51 52 -50
		mu 0 4 19 20 31 30
		f 4 -32 -35 53 -52
		mu 0 4 20 21 32 31
		f 4 -34 54 55 56
		mu 0 4 22 23 34 33
		f 4 -37 57 58 -55
		mu 0 4 23 82 83 34
		f 4 -39 59 60 61
		mu 0 4 24 25 36 35
		f 4 -42 62 63 -60
		mu 0 4 25 26 37 36
		f 4 -44 64 65 -63
		mu 0 4 26 27 38 37
		f 4 -46 66 67 -65
		mu 0 4 27 28 39 38
		f 4 68 -49 69 70
		mu 0 4 87 88 29 40
		f 4 -51 71 72 -70
		mu 0 4 29 30 41 40
		f 4 -53 73 74 -72
		mu 0 4 30 31 42 41
		f 4 -54 -57 75 -74
		mu 0 4 31 32 43 42
		f 4 76 77 -71 -73
		mu 0 4 41 86 87 40
		f 4 78 79 80 81
		mu 0 4 55 56 57 58
		f 4 82 83 84 -80
		mu 0 4 56 78 79 57
		f 4 85 86 87 88
		mu 0 4 59 61 62 60
		f 4 89 90 91 -87
		mu 0 4 61 63 64 62
		f 4 92 93 94 -91
		mu 0 4 63 65 66 64
		f 4 95 96 97 -94
		mu 0 4 65 67 68 66
		f 4 98 99 100 101
		mu 0 4 91 92 69 70
		f 4 102 103 104 -101
		mu 0 4 69 71 72 70
		f 4 105 106 107 -104
		mu 0 4 71 73 74 72
		f 4 108 -82 109 -107
		mu 0 4 73 75 76 74
		f 4 110 111 -79 112
		mu 0 4 0 1 56 55
		f 4 -1 113 -81 114
		mu 0 4 46 45 58 57
		f 4 115 116 -83 -112
		mu 0 4 1 77 78 56
		f 4 117 -5 -115 -85
		mu 0 4 79 80 46 57
		f 4 118 119 -86 120
		mu 0 4 2 3 61 59
		f 4 -8 121 -88 122
		mu 0 4 48 47 60 62
		f 4 123 124 -90 -120
		mu 0 4 3 4 63 61
		f 4 -12 -123 -92 125
		mu 0 4 49 48 62 64
		f 4 126 127 -93 -125
		mu 0 4 4 5 65 63
		f 4 -15 -126 -95 128
		mu 0 4 50 49 64 66
		f 4 129 130 -96 -128
		mu 0 4 5 6 67 65
		f 4 -18 -129 -98 131
		mu 0 4 51 50 66 68
		f 4 132 133 134 -100
		mu 0 4 92 93 7 69
		f 4 -22 135 -102 136
		mu 0 4 52 90 91 70
		f 4 137 138 -103 -135
		mu 0 4 7 8 71 69
		f 4 -25 -137 -105 139
		mu 0 4 53 52 70 72
		f 4 140 141 -106 -139
		mu 0 4 8 9 73 71
		f 4 -28 -140 -108 142
		mu 0 4 54 53 72 74
		f 4 143 -113 -109 -142
		mu 0 4 9 10 75 73
		f 4 -31 -143 -110 -114
		mu 0 4 44 54 74 76
		f 4 -76 144 145 146
		mu 0 4 42 33 84 85
		f 4 -147 147 -77 -75
		mu 0 4 42 85 86 41
		f 4 148 -145 -56 -59
		mu 0 4 83 84 33 34
		f 4 -117 149 -121 150
		mu 0 4 78 77 2 59
		f 4 -84 -151 -89 151
		mu 0 4 79 78 59 60
		f 4 152 -118 -152 -122
		mu 0 4 47 80 79 60
		f 4 -6 -153 -11 153
		mu 0 4 81 80 47 13
		f 4 -36 -154 -40 154
		mu 0 4 82 81 13 24
		f 4 -58 -155 -62 155
		mu 0 4 83 82 24 35
		f 4 156 -149 -156 -61
		mu 0 4 36 84 83 35
		f 4 -146 -157 -64 157
		mu 0 4 85 84 36 37
		f 4 -148 -158 -66 158
		mu 0 4 86 85 37 38
		f 4 -78 -159 -68 159
		mu 0 4 87 86 38 39
		f 4 160 -69 -160 -67
		mu 0 4 28 88 87 39
		f 4 161 -47 -161 -45
		mu 0 4 17 89 88 28
		f 4 162 -21 -162 -19
		mu 0 4 51 90 89 17
		f 4 -136 -163 -132 163
		mu 0 4 91 90 51 68
		f 4 164 -99 -164 -97
		mu 0 4 67 92 91 68
		f 4 165 -133 -165 -131
		mu 0 4 6 93 92 67;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Gun_Right_Arm";
	rename -uid "9E6F8F8D-4FC2-D231-1937-7290328D606E";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 22.876542091369629 4.790226936340332 ;
	setAttr ".sp" -type "double3" -18 22.876542091369629 4.790226936340332 ;
createNode mesh -n "Gun_Right_ArmShape" -p "Gun_Right_Arm";
	rename -uid "522695C4-4697-3768-0CE7-769E09221194";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Gun_Right_ArmShapeOrig" -p "Gun_Right_Arm";
	rename -uid "6C67CE62-4E99-13D1-C55A-06A33A04F31C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 236 ".uvst[0].uvsp[0:235]" -type "float2" 0.375 0.31249374
		 0.625 0.31249374 0.375 0.43750626 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.43749374 0.25 0.5625062 3.7252903e-09 0.18749376
		 0.25 0.18749379 3.7252903e-09 0.3125062 3.7252903e-09 0.43749374 0.75 0.5625062 0.5
		 0.68749374 0.25 0.8125062 3.7252903e-09 0.81250632 0.25 0.43749377 3.7252903e-09
		 0.31250626 0.25 0.6874938 3.7252903e-09 0.56250626 0.25 0.43749377 0.5 0.375 0.81249374
		 0.625 0.43750626 0.56250626 0.75 0.375 0.5467304 0.4217304 0.5 0.5782696 0.5 0.625
		 0.5467304 0.625 0.7032696 0.57826966 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.7032696
		 0.4217304 0.75 0.4217304 0.92500001 0.375 0.92500001 0.57826966 0.75 0.625 0.7032696
		 0.625 0.92500001 0.5782696 0.92500001 0.4217304 0.5 0.375 0.5467304 0.375 0.32499999
		 0.4217304 0.32499999 0.625 0.5467304 0.5782696 0.5 0.57826966 0.32499999 0.625 0.32499999
		 0.70000005 0.2032696 0.70000005 0.046730354 0.875 0.046730399 0.875 0.20326962 0.125
		 0.046730399 0.29999998 0.046730399 0.29999998 0.20326963 0.125 0.20326963 0.4217304
		 0.5 0.375 0.5467304 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696 0.57826966 0.75
		 0.4217304 0.75 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375
		 0.5467304 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696
		 0.625 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375
		 0.7032696 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5 0.375 0.5467304
		 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625 0.7032696 0.625
		 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304 0.75 0.375 0.7032696
		 0.375 0.7032696 0.62232637 0.21951714 0.66087526 0.16954048 0.59295321 0.26463816
		 0.44455051 0.57287049 0.33587527 0.57287741 0.32842079 0.41435975 0.425953 0.27906609
		 0.49904695 0.2959339 0.6437481 0.24375807 0.68125188 0.081241898 0.68125188 0.081241898
		 0.6437481 0.24375807 0.35625187 0.75626677 0.31874812 0.41873318 0.31874812 0.41873318
		 0.35625187 0.75626677 0.38124189 0.26874813 0.38124189 0.26874813 0.54375809 0.30625185
		 0.54375809 0.30625185 0.65000498 0.15623444 0.65000498 0.15623444 0.61875814 0.231227
		 0.61875814 0.231227 0.61875814 0.231227 0.45624191 0.69377303 0.45624191 0.69377303
		 0.61875814 0.231227 0.45624191 0.69377303 0.4062469 0.72501993 0.4062469 0.72501993
		 0.45624191 0.69377303 0.70125204 0.032712124 0.6437481 0.24375807 0.34285599 0.81839383
		 0.31874812 0.41873318 0.38231337 0.25929666 0.54042125 0.31069157 0.66087526 0.16954048
		 0.6123848 0.26102608 0.44455057 0.57287043 0.59295321 0.26463822 0.33985224 0.37760419
		 0.33587524 0.57287729 0.42595297 0.27906609 0.49904698 0.2959339 0.68125182 0.081241898
		 0.35625187 0.75626677 0.65000498 0.15623444 0.61875814 0.231227 0.45624194 0.69377303
		 0.4062469 0.72501993 0.66087526 0.16954048 0.62232637 0.21951714 0.59295321 0.26463816
		 0.44455051 0.57287049 0.33587527 0.57287741 0.32842079 0.41435975 0.425953 0.27906609
		 0.49904695 0.2959339 0.35625187 0.75626677 0.68125188 0.081241898 0.65000498 0.15623444
		 0.61875814 0.231227 0.45624191 0.69377303 0.4062469 0.72501993 0.4062469 0.72501993
		 0.65000498 0.15623444 0.61875814 0.231227 0.45624191 0.69377303 0.35625187 0.75626677
		 0.68125188 0.081241898 0.66087526 0.16954048 0.62232637 0.21951714 0.59295321 0.26463816
		 0.44455051 0.57287049 0.33587527 0.57287741 0.32842079 0.41435975 0.425953 0.27906609
		 0.49904695 0.2959339 0.61875814 0.231227 0.65000498 0.15623444 0.45624191 0.69377303
		 0.4062469 0.72501993 0.68125188 0.081241898 0.35625187 0.75626677 0.40103906 0.44792187
		 0.40103906 0.30207813 0.42707813 0.30207813 0.42707813 0.44792187 0.375 3.7252903e-09
		 0.375 0.1805625 0.32292187 0.1805625 0.32292187 0 0.42707813 0.24132031 0.57292187
		 0.24132031 0.57292187 0.30207813 0.59896094 0.30207813 0.59896094 0.44792187 0.57292187
		 0.44792187 0.625 3.7252903e-09 0.625 0.1805625 0.57292187 0.1805625 0.57292187 0
		 0.40103906 0.75 0.40103906 0.5694375 0.42707813 0.5694375 0.42707813 0.75 0.57292187
		 0.50867969 0.42707813 0.50867969 0.59896094 0.75 0.59896094 0.5694375 0.625 0.5694375
		 0.625 0.75 0.42707813 7.4505806e-09 0.42707813 0.1805625 0.57292187 0.5694375 0.57292187
		 0.75 0.67707813 7.4505806e-09 0.82292187 0 0.82292187 0.1805625 0.67707813 0.1805625
		 0.17707813 7.4505806e-09 0.17707813 0.1805625 0.375 0.24132031 0.40103906 0.24132031
		 0.59896094 0.24132031 0.625 0.24132031 0.4140586 0.47830078 0.58594143 0.47830078
		 0.625 0.30207813 0.625 0.44792187 0.59896094 0.50867969 0.40103906 0.50867969 0.375
		 0.30207813 0.375 0.44792187 0.375 0.5694375 0.375 0.75;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 37 ".uvst[1].uvsp[0:36]" -type "float2" 0.375 0.81249374
		 0.40624687 0.7812469 0.40624687 0.9687531 0.375 0.93750626 0.31250626 -3.7252903e-09
		 0.375 0 0.43749374 3.7252903e-09 0.43749374 0.25 0.40624687 0.25 0.375 0.25 0.56250626
		 -3.7252903e-09 0.625 -3.7252903e-09 0.68749374 -3.7252903e-09 0.625 0.25 0.5937531
		 0.25 0.56250626 0.25 0.375 0.5 0.40624687 0.5 0.43749374 0.5 0.43749374 0.75 0.56250626
		 0.5 0.5937531 0.5 0.625 0.5 0.625 0.81249374 0.5937531 0.7812469 0.56250626 0.75
		 0.68749374 0.25 0.81250626 -3.7252903e-09 0.81250632 0.25 0.18749374 0.25 0.18749374
		 -3.7252903e-09 0.31250626 0.25 0.5937531 0.9687531 0.56250626 1 0.43749374 1 0.625
		 0.93750626 0.5 0.875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 223 ".vt";
	setAttr ".vt[0:165]"  -21 16 1.5001502 -19.50015068 16 3.000000238419 -21 28 1.50015008
		 -19.50015068 28 3 -15 16 1.5001502 -16.49984932 16 3.000000238419 -15 28 1.50015008
		 -16.49984932 28 3 -21 28 -1.50014985 -19.50015068 28 -3 -21 16 -1.50014973 -19.50015068 16 -2.99999976
		 -16.49984932 28 -3 -15 28 -1.50014985 -15 16 -1.50014973 -16.49984932 16 -2.99999976
		 -22.05241394 13.40835381 -7 -20.87148285 11.52752304 -7 -22.05241394 13.40835381 0.61562443
		 -20.87148285 11.52752304 0.61562443 -15.12851715 11.52752304 -7 -13.94758606 13.40835381 -7
		 -15.12851715 11.52752304 0.61562443 -13.94758606 13.40835381 0.61562443 -20.5293045 19 -7
		 -22.05241394 16.69583702 -7 -20.5293045 19 0.61562443 -22.05241394 16.69583702 0.61562443
		 -13.94758606 16.69583702 -7 -15.4706955 19 -7 -13.94758606 16.69583702 0.61562443
		 -15.4706955 19 0.61562443 -20.0060157776 18.29057884 -7 -21.21670532 16.25162697 -7
		 -15.99398422 18.29057884 -7 -14.78330231 16.25162697 -7 -14.78330231 13.85256386 -7
		 -15.65180206 12.23694229 -7 -20.34819794 12.23694229 -7 -21.21670532 13.85256386 -7
		 -18.9847641 16.90606689 -10.5 -19.58572006 15.38470459 -10.5 -17.015235901 16.90606689 -10.5
		 -16.41428375 15.38470459 -10.5 -16.41428375 14.71948433 -10.5 -16.67305756 13.62145233 -10.5
		 -19.32694626 13.62145233 -10.5 -19.58572006 14.71948433 -10.5 -20.0060157776 18.29057884 -7.49999905
		 -21.21670532 16.25162697 -7.49999905 -20.0060157776 18.29057884 -8.96627808 -21.21670532 16.25162697 -8.96627808
		 -15.99398422 18.29057884 -7.49999905 -15.99398422 18.29057884 -8.96627808 -14.78330231 16.25162697 -7.49999905
		 -14.78330231 16.25162697 -8.96627808 -14.78330231 13.85256386 -7.49999905 -14.78330231 13.85256386 -8.96627808
		 -15.65180206 12.23694229 -7.49999905 -15.65180206 12.23694229 -8.96627808 -20.34819794 12.23694229 -7.49999905
		 -20.34819794 12.23694229 -8.96627808 -21.21670532 13.85256386 -7.49999905 -21.21670532 13.85256386 -8.96627808
		 -19.62041473 17.50033569 -7 -20.28578186 15.92429733 -7 -19.62041473 17.50033569 -7.49999905
		 -20.28578186 15.92429733 -7.49999905 -16.37958527 17.5003376 -7 -16.37958527 17.5003376 -7.49999905
		 -15.71422195 15.92429733 -7 -15.71422195 15.92429733 -7.49999905 -15.71422195 14.17989159 -7
		 -15.71422195 14.17989159 -7.49999905 -16.037406921 13.027183533 -7 -16.037406921 13.027183533 -7.49999905
		 -19.96259308 13.02718544 -7 -19.96259308 13.02718544 -7.49999905 -20.28578186 14.17989349 -7
		 -20.28578186 14.17989349 -7.49999905 -16.054801941 9.64669228 0.61562395 -19.94519424 9.64669228 0.6156249
		 -22.05241394 16.69583702 8.77538395 -22.05241394 13.40835381 8.77538395 -13.94758606 13.40835381 8.77538395
		 -13.94758606 16.69583702 8.77538395 -15.4706955 19 8.77538395 -20.5293045 19 8.7753849
		 -19.94519424 9.64669228 8.7753849 -16.054801941 9.64669228 8.77538395 -22.05241394 16.69583702 15.9171381
		 -22.05241394 13.40835381 15.9171381 -13.94758606 13.40835381 15.9171381 -13.94758606 16.69583702 15.9171381
		 -15.4706955 19 15.9171381 -20.5293045 19 15.9171381 -20.22785187 17.085075378 15.9171381
		 -15.77215195 17.085075378 15.9171381 -20.22785187 17.085075378 20.080453873 -15.77215195 17.085075378 20.080453873
		 -20.25120544 15.4619236 15.9171381 -19.027954102 14.23867226 15.9171381 -19.027954102 14.23867226 20.080453873
		 -20.25120544 15.4619236 20.080453873 -16.9720459 14.23867226 15.9171381 -15.74879456 15.4619236 15.9171381
		 -15.74879456 15.4619236 20.080453873 -16.9720459 14.23867226 20.080453873 -17.019161224 18.28409576 15.9171381
		 -17.019161224 18.28409576 20.080453873 -18.98083878 18.28409576 15.9171381 -18.98083878 18.28409576 20.080453873
		 -17.065746307 12.86323166 8.77538395 -17.065746307 12.86323357 14.9582243 -18.9050293 10.20274544 8.77538586
		 -18.9050293 10.20274544 14.9582243 -17.094970703 10.20274544 8.77538395 -17.094970703 10.20274544 14.9582243
		 -18.93425369 12.86323166 8.77538395 -18.93425369 12.86323357 14.9582243 -15.12851715 11.52752304 8.77538395
		 -16.15098572 11.5329895 8.77538395 -16.15098572 11.5329895 14.9582243 -19.84901428 11.5329895 14.9582243
		 -19.84901428 11.5329895 8.7753849 -20.87148285 11.52752304 8.77538395 -19.77819061 16.91727066 20.080453873
		 -19.79683304 15.62172508 20.080453873 -18.82047653 14.64536858 20.080453873 -17.17952728 14.64536858 20.080453873
		 -16.20316696 15.62172508 20.080453873 -16.22180939 16.91726875 20.080453873 -17.21712875 17.87428665 20.080453873
		 -18.78287125 17.87428665 20.080453873 -16.52418137 11.53347588 14.9582243 -19.47582245 11.53347588 14.9582243
		 -18.72236633 10.3658886 14.9582243 -17.27763748 10.3658886 14.9582243 -18.74568939 12.70106316 14.9582243
		 -17.25431061 12.70106316 14.9582243 -19.77819061 16.91727066 14.83047676 -19.79683304 15.62172508 14.83047676
		 -18.82047653 14.64536858 14.83047676 -17.17952728 14.64536858 14.83047676 -16.20316696 15.62172508 14.83047676
		 -16.22180939 16.91726875 14.83047676 -17.21712875 17.87428665 14.83047676 -18.78287125 17.87428665 14.83047676
		 -16.52418137 11.53347588 9.70824623 -19.47582245 11.53347588 9.70824623 -18.72236633 10.3658886 9.70824623
		 -17.27763748 10.3658886 9.70824623 -18.74568939 12.70106316 9.70824623 -17.25431061 12.70106316 9.70824623
		 -19.94519424 5.75308418 0.6156249 -16.054801941 5.75308418 0.61562395 -16.054801941 5.75308418 8.77538395
		 -19.94519424 5.75308418 8.7753849 -24 37.50025177 3.5002501 -21.50024986 37.50025177 6
		 -21.50024986 40 3.5002501 -14.49975014 40 3.5002501 -14.49975014 37.50025177 6 -12 37.50025177 3.5002501
		 -24 37.50025177 -3.5002501 -21.50024986 40 -3.5002501;
	setAttr ".vt[166:222]" -21.50024986 37.50025177 -6 -14.49975014 37.50025177 -6
		 -14.49975014 40 -3.5002501 -12 37.50025177 -3.5002501 -21.50024986 31 6 -24 31 3.5002501
		 -12 31 3.5002501 -14.49975014 31 6 -24 31 -3.5002501 -21.50024986 31 -6 -14.49975014 31 -6
		 -12 31 -3.5002501 -23 28 2.5002501 -20.50024986 28 5 -23 31 2.5002501 -20.50024986 31 5
		 -13 28 2.5002501 -15.49975014 28 5 -15.49975014 31 5 -13 31 2.5002501 -20.50024986 31 -5
		 -23 31 -2.5002501 -23 28 -2.5002501 -20.50024986 28 -5 -13 31 -2.5002501 -15.49975014 31 -5
		 -13 28 -2.5002501 -15.49975014 28 -5 -23.03966713 39.077758789 3.5002501 -21.50024986 39.077758789 5.039667606
		 -14.49975014 39.077758789 5.039667606 -12.96033287 39.077758789 3.5002501 -12.96033287 39.077758789 -3.5002501
		 -14.49975014 39.077758789 -5.03966713 -21.50024986 39.077758789 -5.039667606 -23.03966713 39.077758789 -3.5002501
		 -21.97871399 31 3.9787128 -23.03966713 31 5.039667606 -23.03966713 37.50025177 5.039667606
		 -22.58447266 38.6620903 4.58447266 -14.021286964 31 3.9787128 -12.96033287 31 5.039667606
		 -12.96033287 37.50025177 5.039667606 -13.4155283 38.6620903 4.58447266 -21.97871399 31 -3.9787128
		 -23.03966713 31 -5.039667606 -23.03966713 37.50025177 -5.039667606 -22.58447266 38.6620903 -4.58447266
		 -14.021286964 31 -3.9787128 -12.96033287 31 -5.039667606 -12.96033287 37.50025177 -5.039667606
		 -13.4155283 38.6620903 -4.58447266 -14.021286964 28 -3.9787128 -21.97871399 28 3.97871256
		 -21.97871399 28 -3.97871256 -18 28 8.8190603e-08 -14.021286964 28 3.9787128;
	setAttr -s 425 ".ed";
	setAttr ".ed[0:165]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0 7 6 0 9 12 0
		 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0 6 4 0 8 10 0
		 11 9 0 12 15 0 14 13 0 17 20 0 17 16 0 21 20 0 24 29 0 25 16 0 24 25 0 28 21 0 29 28 0
		 17 19 0 19 18 1 18 16 0 21 23 0 23 22 1 22 20 0 25 27 0 27 26 1 26 24 0 29 31 0 31 30 1
		 30 28 0 26 31 1 22 19 0 30 23 1 18 27 1 24 32 1 25 33 1 32 33 1 29 34 1 32 34 1 28 35 1
		 34 35 1 21 36 1 35 36 1 20 37 1 36 37 1 17 38 1 38 37 1 16 39 0 38 39 1 33 39 1 40 41 0
		 40 42 0 42 43 0 43 44 0 44 45 0 46 45 0 46 47 0 41 47 0 48 49 0 48 50 0 50 51 0 49 51 0
		 48 52 0 52 53 0 50 53 0 52 54 0 54 55 0 53 55 0 54 56 0 56 57 0 55 57 0 56 58 0 58 59 0
		 57 59 0 60 58 0 60 61 0 61 59 0 60 62 0 62 63 0 61 63 0 49 62 0 51 63 0 50 40 0 51 41 0
		 53 42 0 55 43 0 57 44 0 59 45 0 61 46 0 63 47 0 32 64 1 33 65 1 64 65 0 48 66 1 64 66 0
		 49 67 1 66 67 0 65 67 0 34 68 1 64 68 0 52 69 1 68 69 0 66 69 0 35 70 1 68 70 0 54 71 1
		 70 71 0 69 71 0 36 72 1 70 72 0 56 73 1 72 73 0 71 73 0 37 74 1 72 74 0 58 75 1 74 75 0
		 73 75 0 38 76 1 76 74 0 60 77 1 76 77 0 77 75 0 39 78 0 76 78 0 62 79 0 78 79 0 77 79 0
		 65 78 0 67 79 0 81 80 0 81 19 0 27 82 0 18 83 0 82 83 1 23 84 0 83 84 0 30 85 0 84 85 1
		 31 86 0 86 85 1 26 87 0 86 87 1 87 82 1 81 88 0 88 125 0 80 89 0 88 89 1 84 120 0
		 82 90 0 83 91 0 90 91 0;
	setAttr ".ed[166:331]" 84 92 0 91 92 0 85 93 0 92 93 0 86 94 0 94 93 0 87 95 0
		 94 95 0 95 90 0 90 96 1 96 100 0 93 97 1 83 118 1 84 112 1 88 114 1 89 116 1 96 98 0
		 98 103 0 97 99 0 101 104 0 91 100 1 101 91 1 102 107 0 103 102 0 105 97 0 92 104 1
		 105 92 1 106 99 0 107 106 0 108 110 0 108 94 1 109 111 0 99 109 0 95 110 1 111 98 0
		 100 103 0 102 101 0 104 107 0 106 105 0 109 108 0 110 111 0 100 101 0 104 105 0 97 108 0
		 110 96 0 112 121 0 113 122 0 114 124 0 114 116 0 115 117 0 115 123 0 112 113 0 115 114 0
		 117 116 0 118 112 0 119 113 0 119 118 0 22 80 0 120 89 0 121 116 0 122 117 0 123 119 0
		 124 118 0 125 83 0 22 120 0 120 121 0 121 122 0 123 124 0 124 125 0 125 19 0 98 126 1
		 103 127 1 126 127 0 102 128 1 127 128 0 107 129 1 128 129 0 106 130 1 129 130 0 99 131 1
		 130 131 0 109 132 1 131 132 0 111 133 1 132 133 0 133 126 0 122 134 0 123 135 0 115 136 1
		 136 135 0 117 137 1 136 137 0 134 137 0 119 138 1 113 139 1 138 139 0 135 138 0 139 134 0
		 126 140 0 127 141 0 140 141 0 128 142 0 141 142 0 129 143 0 142 143 0 130 144 0 143 144 0
		 131 145 0 144 145 0 132 146 0 145 146 0 133 147 0 146 147 0 147 140 0 134 148 0 135 149 0
		 148 149 0 136 150 0 150 149 0 137 151 0 150 151 0 148 151 0 138 152 0 139 153 0 152 153 0
		 149 152 0 153 148 0 81 154 0 80 155 0 154 155 0 89 156 0 155 156 0 88 157 0 157 156 0
		 154 157 0 179 183 0 179 219 0 182 222 0 188 178 0 189 193 0 189 220 0 192 182 0 193 218 0
		 158 194 0 160 165 0 165 201 0 164 158 0 159 204 0 158 171 0 171 203 0 170 159 0 160 195 0
		 159 162 0 162 196 0 161 160 0 161 197 0 163 169 0 169 198 0 168 161 0 163 208 0 162 173 0
		 173 207 0 172 163 0 164 212 0 166 175 0 175 211 0;
	setAttr ".ed[332:424]" 174 164 0 166 200 0 165 168 0 168 199 0 167 166 0 167 216 0
		 169 177 0 177 215 0 176 167 0 171 180 1 180 202 0 181 170 1 173 184 1 184 206 0 185 172 1
		 175 186 1 186 210 0 187 174 1 177 190 1 190 214 0 191 176 1 179 181 0 180 178 0 182 185 0
		 184 183 0 186 189 0 188 187 0 190 192 0 193 191 0 170 173 0 176 175 0 172 177 0 174 171 0
		 184 181 0 186 191 0 190 185 0 180 187 0 194 160 0 195 159 0 196 161 0 197 163 0 198 168 0
		 199 167 0 200 165 0 201 164 0 194 205 1 195 196 1 196 209 1 197 198 1 198 217 1 199 200 1
		 200 213 1 201 194 1 202 181 0 203 170 0 204 158 0 205 195 1 202 203 1 203 204 1 204 205 1
		 206 185 0 207 172 0 208 162 0 209 197 1 206 207 1 207 208 1 208 209 1 210 187 0 211 174 0
		 212 166 0 213 201 1 210 211 1 211 212 1 212 213 1 214 191 0 215 176 0 216 169 0 217 199 1
		 214 215 1 215 216 1 216 217 1 218 192 0 219 178 0 218 221 1 220 188 0 221 219 1 222 183 0
		 220 221 1 221 222 1 222 218 1 218 220 1 220 219 1 219 222 1;
	setAttr -s 208 -ch 850 ".fc[0:207]" -type "polyFaces" 
		f 4 17 0 18 -4
		mu 0 4 8 18 9 21
		f 4 21 8 22 -12
		mu 0 4 13 22 14 25
		f 4 19 -15 23 -7
		mu 0 4 15 20 16 17
		f 4 20 10 16 2
		mu 0 4 10 11 12 19
		f 8 -5 3 7 6 13 -9 9 -3
		mu 0 8 0 8 21 1 24 14 22 2
		f 8 -13 11 15 14 5 -1 1 -11
		mu 0 8 23 13 25 3 4 5 6 7
		f 4 -2 -18 4 -17
		mu 0 4 12 18 8 19
		f 4 -6 -20 -8 -19
		mu 0 4 9 20 15 21
		f 4 -10 -22 12 -21
		mu 0 4 2 22 13 23
		f 4 -14 -24 -16 -23
		mu 0 4 14 24 3 25
		f 8 -65 65 66 67 68 -70 70 -72
		mu 0 8 26 27 28 29 30 31 32 33
		f 4 -26 32 33 34
		mu 0 4 34 35 36 37
		f 4 -27 35 36 37
		mu 0 4 38 39 40 41
		f 4 29 38 39 40
		mu 0 4 42 43 44 45
		f 4 -32 41 42 43
		mu 0 4 46 47 48 49
		f 4 44 -42 -28 -41
		mu 0 4 45 48 47 42
		f 4 -33 24 -38 45
		mu 0 4 36 35 38 41
		f 4 46 -36 -31 -44
		mu 0 4 50 51 52 53
		f 4 -35 47 -39 28
		mu 0 4 54 55 56 57
		f 4 -30 48 50 -50
		mu 0 4 43 42 58 59
		f 4 27 51 -53 -49
		mu 0 4 42 47 60 58
		f 4 31 53 -55 -52
		mu 0 4 47 46 61 60
		f 4 30 55 -57 -54
		mu 0 4 46 39 62 61
		f 4 26 57 -59 -56
		mu 0 4 39 38 63 62
		f 4 -25 59 60 -58
		mu 0 4 38 35 64 63
		f 4 25 61 -63 -60
		mu 0 4 35 34 65 64
		f 4 -29 49 63 -62
		mu 0 4 34 43 59 65
		f 4 -73 73 74 -76
		mu 0 4 66 67 68 69
		f 4 76 77 -79 -74
		mu 0 4 67 70 71 68
		f 4 79 80 -82 -78
		mu 0 4 70 72 73 71
		f 4 82 83 -85 -81
		mu 0 4 72 74 75 73
		f 4 85 86 -88 -84
		mu 0 4 74 76 77 75
		f 4 -89 89 90 -87
		mu 0 4 76 78 79 77
		f 4 91 92 -94 -90
		mu 0 4 78 80 81 79
		f 4 -95 75 95 -93
		mu 0 4 80 66 69 81
		f 4 -75 96 64 -98
		mu 0 4 69 68 27 26
		f 4 78 98 -66 -97
		mu 0 4 68 71 28 27
		f 4 81 99 -67 -99
		mu 0 4 71 73 29 28
		f 4 84 100 -68 -100
		mu 0 4 73 75 30 29
		f 4 87 101 -69 -101
		mu 0 4 75 77 31 30
		f 4 -91 102 69 -102
		mu 0 4 77 79 32 31
		f 4 93 103 -71 -103
		mu 0 4 79 81 33 32
		f 4 -96 97 71 -104
		mu 0 4 81 69 26 33
		f 4 -107 108 110 -112
		mu 0 4 82 83 84 85
		f 4 113 115 -117 -109
		mu 0 4 83 86 87 84
		f 4 118 120 -122 -116
		mu 0 4 86 88 89 87
		f 4 123 125 -127 -121
		mu 0 4 88 90 91 89
		f 4 128 130 -132 -126
		mu 0 4 90 92 93 91
		f 4 -134 135 136 -131
		mu 0 4 92 94 95 93
		f 4 138 140 -142 -136
		mu 0 4 94 96 97 95
		f 4 -143 111 143 -141
		mu 0 4 96 82 85 97
		f 4 -51 104 106 -106
		mu 0 4 59 58 83 82
		f 4 72 109 -111 -108
		mu 0 4 67 66 85 84
		f 4 52 112 -114 -105
		mu 0 4 58 60 86 83
		f 4 -77 107 116 -115
		mu 0 4 70 67 84 87
		f 4 54 117 -119 -113
		mu 0 4 60 61 88 86
		f 4 -80 114 121 -120
		mu 0 4 72 70 87 89
		f 4 56 122 -124 -118
		mu 0 4 61 62 90 88
		f 4 -83 119 126 -125
		mu 0 4 74 72 89 91
		f 4 58 127 -129 -123
		mu 0 4 62 63 92 90
		f 4 -86 124 131 -130
		mu 0 4 76 74 91 93
		f 4 -61 132 133 -128
		mu 0 4 63 64 94 92
		f 4 88 129 -137 -135
		mu 0 4 78 76 93 95
		f 4 62 137 -139 -133
		mu 0 4 64 65 96 94
		f 4 -92 134 141 -140
		mu 0 4 80 78 95 97
		f 4 -64 105 142 -138
		mu 0 4 65 59 82 96
		f 4 94 139 -144 -110
		mu 0 4 66 80 97 85
		f 8 266 268 270 272 274 276 278 279
		mu 0 8 98 99 100 101 102 103 104 105
		f 4 -48 147 -149 -147
		mu 0 4 106 107 108 109
		f 4 -47 151 -153 -150
		mu 0 4 110 111 112 113
		f 4 -43 153 154 -152
		mu 0 4 111 114 115 112
		f 4 -45 155 -157 -154
		mu 0 4 114 116 117 115
		f 4 -40 146 -158 -156
		mu 0 4 116 106 109 117
		f 4 235 -146 158 159
		mu 0 4 118 119 120 121
		f 4 295 297 -300 -301
		mu 0 4 122 123 124 125
		f 4 -224 230 224 -161
		mu 0 4 126 127 128 129
		f 4 148 164 -166 -164
		mu 0 4 109 108 130 131
		f 4 150 166 -168 -165
		mu 0 4 108 113 132 130
		f 4 152 168 -170 -167
		mu 0 4 113 112 133 132
		f 4 -155 170 171 -169
		mu 0 4 112 115 134 133
		f 4 156 172 -174 -171
		mu 0 4 115 117 135 134
		f 4 157 163 -175 -173
		mu 0 4 117 109 131 135
		f 4 165 186 -177 -176
		mu 0 4 131 130 136 137
		f 4 167 191 -186 187
		mu 0 4 130 132 138 139
		f 4 169 177 -191 192
		mu 0 4 132 133 140 141
		f 4 -178 -172 -197 -210
		mu 0 4 140 133 134 142
		f 4 173 199 -196 196
		mu 0 4 134 135 143 142
		f 4 174 175 -211 -200
		mu 0 4 135 131 137 143
		f 4 -151 178 220 -180
		mu 0 4 113 108 144 145
		f 4 234 -160 180 213
		mu 0 4 146 118 121 147
		f 4 161 181 -215 -181
		mu 0 4 121 129 148 147
		f 4 -225 231 225 -182
		mu 0 4 129 128 149 148
		f 4 176 201 -184 -183
		mu 0 4 137 136 150 151
		f 4 202 185 203 -189
		mu 0 4 152 139 138 153
		f 4 204 190 184 -194
		mu 0 4 154 141 140 155
		f 4 205 195 206 -198
		mu 0 4 156 142 143 157
		f 4 -218 -221 -223 221
		mu 0 4 158 145 144 159
		f 4 233 -214 -219 216
		mu 0 4 160 146 147 161
		f 4 218 214 -220 -216
		mu 0 4 161 147 148 162
		f 4 219 -226 232 226
		mu 0 4 162 148 149 163
		f 4 207 -203 -190 -202
		mu 0 4 136 139 152 150
		f 4 208 -205 -195 -204
		mu 0 4 138 141 154 153
		f 4 209 -206 -199 -185
		mu 0 4 140 142 156 155
		f 4 210 182 -201 -207
		mu 0 4 143 137 151 157
		f 3 -187 -188 -208
		mu 0 3 136 130 139
		f 3 -192 -193 -209
		mu 0 3 138 132 141
		f 4 282 -285 286 -288
		mu 0 4 164 165 166 167
		f 4 -231 -37 149 162
		mu 0 4 128 127 110 113
		f 4 -232 -163 179 211
		mu 0 4 149 128 113 145
		f 4 -233 -212 217 212
		mu 0 4 163 149 145 158
		f 4 -291 -292 -283 -293
		mu 0 4 168 169 165 164
		f 4 222 -229 -234 227
		mu 0 4 159 144 146 160
		f 4 -230 -235 228 -179
		mu 0 4 108 118 146 144
		f 4 -34 -236 229 -148
		mu 0 4 107 119 118 108
		f 4 183 237 -239 -237
		mu 0 4 151 150 170 171
		f 4 189 239 -241 -238
		mu 0 4 150 152 172 170
		f 4 188 241 -243 -240
		mu 0 4 152 153 173 172
		f 4 194 243 -245 -242
		mu 0 4 153 154 174 173
		f 4 193 245 -247 -244
		mu 0 4 154 155 175 174
		f 4 198 247 -249 -246
		mu 0 4 155 156 176 175
		f 4 197 249 -251 -248
		mu 0 4 156 157 177 176
		f 4 200 236 -252 -250
		mu 0 4 157 151 171 177
		f 4 -217 254 255 -254
		mu 0 4 160 161 178 179
		f 4 215 256 -258 -255
		mu 0 4 161 162 180 178
		f 4 -227 252 258 -257
		mu 0 4 162 163 181 180
		f 4 -222 259 261 -261
		mu 0 4 158 159 182 183
		f 4 -228 253 262 -260
		mu 0 4 159 160 179 182
		f 4 -213 260 263 -253
		mu 0 4 163 158 183 181
		f 4 238 265 -267 -265
		mu 0 4 171 170 99 98
		f 4 240 267 -269 -266
		mu 0 4 170 172 100 99
		f 4 242 269 -271 -268
		mu 0 4 172 173 101 100
		f 4 244 271 -273 -270
		mu 0 4 173 174 102 101
		f 4 246 273 -275 -272
		mu 0 4 174 175 103 102
		f 4 248 275 -277 -274
		mu 0 4 175 176 104 103
		f 4 250 277 -279 -276
		mu 0 4 176 177 105 104
		f 4 251 264 -280 -278
		mu 0 4 177 171 98 105
		f 4 -256 283 284 -282
		mu 0 4 179 178 166 165
		f 4 257 285 -287 -284
		mu 0 4 178 180 167 166
		f 4 -259 280 287 -286
		mu 0 4 180 181 164 167
		f 4 -262 288 290 -290
		mu 0 4 183 182 169 168
		f 4 -263 281 291 -289
		mu 0 4 182 179 165 169
		f 4 -264 289 292 -281
		mu 0 4 181 183 168 164
		f 4 -46 223 -145 145
		mu 0 4 36 127 126 120
		f 4 144 294 -296 -294
		mu 0 4 120 126 123 122
		f 4 160 296 -298 -295
		mu 0 4 126 129 124 123
		f 4 -162 298 299 -297
		mu 0 4 129 121 125 124
		f 4 -159 293 300 -299
		mu 0 4 121 120 122 125
		f 4 -417 423 414 -305
		mu 1 4 0 1 2 3
		f 4 384 369 310 311
		mu 0 4 184 185 186 187
		f 4 390 387 314 315
		mu 0 4 188 189 190 191
		f 4 317 378 371 320
		mu 0 4 186 192 193 194
		f 4 321 380 373 324
		mu 0 4 194 195 196 197
		f 4 397 394 326 327
		mu 0 4 198 199 200 201
		f 4 404 401 330 331
		mu 0 4 202 203 204 205
		f 4 382 375 334 335
		mu 0 4 206 207 187 197
		f 4 411 408 338 339
		mu 0 4 208 209 210 211
		f 4 389 -316 341 342
		f 4 396 -328 344 345
		f 4 403 -332 347 348
		f 4 410 -340 350 351
		f 6 -415 -303 353 -386 -343 354
		mu 1 6 4 5 6 7 8 9
		f 6 -419 -304 355 -393 -346 356
		mu 1 6 10 11 12 13 14 15
		f 6 -400 -349 357 306 416 358
		mu 1 6 16 17 18 19 1 0
		f 6 -407 -352 359 -414 -309 360
		mu 1 6 20 21 22 23 24 25
		f 4 361 -327 -319 -317
		mu 0 4 212 201 200 213
		f 4 -321 -325 -335 -311
		mu 0 4 186 194 197 187
		f 4 -337 -341 362 -331
		mu 0 4 204 214 215 205
		f 4 363 -339 -323 -329
		mu 0 4 216 217 218 219
		f 4 364 -315 -313 -333
		mu 0 4 220 191 190 221
		f 4 -354 301 -357 365
		mu 1 4 7 6 10 15
		f 4 366 -361 -306 -358
		mu 1 4 18 20 25 19
		f 4 -356 -308 -360 367
		mu 1 4 26 12 27 28
		f 4 -359 304 -355 368
		mu 1 4 29 30 4 31
		f 4 -362 -344 -366 -345
		f 4 -365 -350 -369 -342
		f 4 -363 -353 -367 -348
		f 4 -364 -347 -368 -351
		f 4 377 388 -318 -370
		mu 0 4 222 223 192 186
		f 4 -372 379 395 -322
		mu 0 4 194 193 224 225
		f 4 383 402 -312 -376
		mu 0 4 207 226 184 187
		f 4 381 409 -336 -374
		mu 0 4 196 227 206 197
		f 4 -388 391 -378 -310
		mu 0 4 190 189 223 222
		f 4 -379 370 318 319
		mu 0 4 193 192 213 200
		f 4 398 -380 -320 -395
		mu 0 4 199 224 193 200
		f 4 -381 372 322 323
		mu 0 4 196 195 228 229
		f 4 412 -382 -324 -409
		mu 0 4 230 227 196 229
		f 4 333 -383 374 336
		mu 0 4 204 207 206 214
		f 4 405 -384 -334 -402
		mu 0 4 231 226 207 204
		f 4 309 -385 376 312
		mu 0 4 232 185 184 233
		f 4 -387 -390 385 343
		f 4 313 -391 386 316
		mu 0 4 213 189 188 212
		f 4 -392 -314 -371 -389
		mu 0 4 223 189 213 192
		f 4 -394 -397 392 346
		f 4 325 -398 393 328
		mu 0 4 219 199 198 216
		f 4 -396 -399 -326 -373
		mu 0 4 225 224 199 219
		f 4 -401 -404 399 349
		f 4 329 -405 400 332
		mu 0 4 234 203 202 235
		f 4 -377 -403 -406 -330
		mu 0 4 233 184 226 231
		f 4 -408 -411 406 352
		f 4 337 -412 407 340
		mu 0 4 214 209 208 215
		f 4 -375 -410 -413 -338
		mu 0 4 214 206 227 230
		f 4 424 418 -302 302
		mu 1 4 2 32 33 34
		f 4 422 -307 305 308
		mu 1 4 24 1 19 25
		f 4 421 413 307 303
		mu 1 4 32 24 23 35
		f 3 -421 -416 -422
		mu 1 3 32 36 24
		f 3 -420 -423 415
		mu 1 3 36 1 24
		f 3 -424 419 417
		mu 1 3 2 1 36
		f 3 -418 420 -425
		mu 1 3 2 36 32;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Drill_Right_Arm";
	rename -uid "45BAD282-4358-810F-08E5-C9B1DAF24AD9";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 23.75 6.4435644149780273 ;
	setAttr ".sp" -type "double3" -18 23.75 6.4435644149780273 ;
createNode mesh -n "Drill_Right_ArmShape" -p "Drill_Right_Arm";
	rename -uid "0E37F2E2-4937-FCCC-8908-33A01F326C98";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Drill_Right_ArmShapeOrig" -p "Drill_Right_Arm";
	rename -uid "760F9D5F-45D1-CB8C-1A26-DC86EB42B3D7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 177 ".uvst[0].uvsp[0:176]" -type "float2" 0.375 0.31249374
		 0.625 0.31249374 0.375 0.43750626 0.625 0.81249374 0.625 0.93750626 0.56250626 1
		 0.43749374 1 0.375 0.93750626 0.43749374 0.25 0.5625062 3.7252903e-09 0.18749376
		 0.25 0.18749379 3.7252903e-09 0.3125062 3.7252903e-09 0.43749374 0.75 0.5625062 0.5
		 0.68749374 0.25 0.8125062 3.7252903e-09 0.81250632 0.25 0.43749377 3.7252903e-09
		 0.31250626 0.25 0.6874938 3.7252903e-09 0.56250626 0.25 0.43749377 0.5 0.375 0.81249374
		 0.625 0.43750626 0.56250626 0.75 0.40103906 0.44792187 0.40103906 0.30207813 0.42707813
		 0.30207813 0.42707813 0.44792187 0.375 3.7252903e-09 0.375 0.1805625 0.32292187 0.1805625
		 0.32292187 0 0.42707813 0.24132031 0.57292187 0.24132031 0.57292187 0.30207813 0.59896094
		 0.30207813 0.59896094 0.44792187 0.57292187 0.44792187 0.625 3.7252903e-09 0.625
		 0.1805625 0.57292187 0.1805625 0.57292187 0 0.40103906 0.75 0.40103906 0.5694375
		 0.42707813 0.5694375 0.42707813 0.75 0.57292187 0.50867969 0.42707813 0.50867969
		 0.59896094 0.75 0.59896094 0.5694375 0.625 0.5694375 0.625 0.75 0.42707813 7.4505806e-09
		 0.42707813 0.1805625 0.57292187 0.5694375 0.57292187 0.75 0.67707813 7.4505806e-09
		 0.82292187 0 0.82292187 0.1805625 0.67707813 0.1805625 0.17707813 7.4505806e-09 0.17707813
		 0.1805625 0.375 0.24132031 0.40103906 0.24132031 0.59896094 0.24132031 0.625 0.24132031
		 0.4140586 0.47830078 0.58594143 0.47830078 0.625 0.30207813 0.625 0.44792187 0.59896094
		 0.50867969 0.40103906 0.50867969 0.375 0.30207813 0.375 0.44792187 0.375 0.5694375
		 0.375 0.75 0.375 0.5467304 0.4217304 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696
		 0.57826966 0.75 0.4217304 0.75 0.375 0.7032696 0.6437481 0.24375807 0.54375809 0.30625185
		 0.38124189 0.26874813 0.31874812 0.41873318 0.35625187 0.75626677 0.45624191 0.69377303
		 0.61875814 0.231227 0.68125188 0.081241898 0.375 0.7032696 0.4217304 0.75 0.4217304
		 0.92500001 0.375 0.92500001 0.36098087 0.79882789 0.375 0.92500001 0.45624194 0.69377297
		 0.35625184 0.75626677 0.57826966 0.75 0.625 0.7032696 0.625 0.92500001 0.5782696
		 0.92500001 0.63925052 0.17290242 0.70000005 0 0.68125188 0.081241921 0.61875814 0.231227
		 0.4217304 0.5 0.375 0.5467304 0.375 0.32499999 0.4217304 0.32499999 0.36074951 0.26401913
		 0.29999998 0.25 0.31874812 0.41873324 0.38124189 0.26874813 0.625 0.5467304 0.5782696
		 0.5 0.57826966 0.32499999 0.625 0.32499999 0.56425053 0.31098089 0.54375809 0.30625185
		 0.64374816 0.24375805 0.70000005 0.2032696 0.70000005 0.046730354 0.875 0.046730399
		 0.875 0.20326962 0.125 0.046730399 0.29999998 0.046730399 0.29999998 0.20326963 0.125
		 0.20326963 0.31401908 0.37617201 0.43574953 0.75209755 0.68598092 0.060749516 0.63901913
		 0.26425049 0.4217304 0.5 0.375 0.5467304 0.5782696 0.5 0.625 0.5467304 0.625 0.7032696
		 0.57826966 0.75 0.4217304 0.75 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304
		 0.5 0.375 0.5467304 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625
		 0.7032696 0.625 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304
		 0.75 0.375 0.7032696 0.375 0.7032696 0.375 0.5467304 0.4217304 0.5 0.4217304 0.5
		 0.375 0.5467304 0.5782696 0.5 0.5782696 0.5 0.625 0.5467304 0.625 0.5467304 0.625
		 0.7032696 0.625 0.7032696 0.57826966 0.75 0.57826966 0.75 0.4217304 0.75 0.4217304
		 0.75 0.375 0.7032696 0.375 0.7032696;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 347 ".uvst[1].uvsp";
	setAttr ".uvst[1].uvsp[0:249]" -type "float2" 0.375 0.81249374 0.40624687
		 0.7812469 0.40624687 0.9687531 0.375 0.93750626 0.31250626 -3.7252903e-09 0.375 0
		 0.43749374 3.7252903e-09 0.43749374 0.25 0.40624687 0.25 0.375 0.25 0.56250626 -3.7252903e-09
		 0.625 -3.7252903e-09 0.68749374 -3.7252903e-09 0.625 0.25 0.5937531 0.25 0.56250626
		 0.25 0.375 0.5 0.40624687 0.5 0.43749374 0.5 0.43749374 0.75 0.56250626 0.5 0.5937531
		 0.5 0.625 0.5 0.625 0.81249374 0.5937531 0.7812469 0.56250626 0.75 0.68749374 0.25
		 0.81250626 -3.7252903e-09 0.81250632 0.25 0.18749374 0.25 0.18749374 -3.7252903e-09
		 0.31250626 0.25 0.5937531 0.9687531 0.56250626 1 0.43749374 1 0.625 0.93750626 0.5
		 0.875 0.58838832 0.16161166 0.5 0.12500001 0.5 2.9802322e-08 0.6742484 0.075751543
		 0.41161168 0.16161166 0.32575154 0.075751543 0.375 0.25 0.25000003 0.25 0.41161168
		 0.33838832 0.32575154 0.42424846 0.5 0.375 0.5 0.5 0.58838832 0.33838835 0.67424846
		 0.42424849 0.625 0.25 0.75 0.25 0.24724899 0.49960732 0.3125 0.5 0.34375 0.58333331
		 0.3125 0.5 0.37500015 0.50000107 0.39583334 0.58333331 0.36639407 0.49632704 0.4375
		 0.5 0.44791669 0.58333331 0.4375 0.5 0.49999988 0.50000107 0.5 0.58333331 0.49231228
		 0.49632704 0.5625 0.5 0.55208331 0.58333331 0.5625 0.5 0.62499958 0.50000107 0.60416663
		 0.58333331 0.61823052 0.49632704 0.6875 0.5 0.65624994 0.58333331 0.6875 0.5 0.74999857
		 0.50000226 0.70833325 0.58333331 0.34375 0.58333331 0.37499997 0.66666663 0.29166666
		 0.58333331 0.39583334 0.58333331 0.41666663 0.66666663 0.44791669 0.58333331 0.4569672
		 0.66377592 0.5 0.58333331 0.49998885 0.66663706 0.55208331 0.58333331 0.54166663
		 0.66666663 0.60416663 0.58333331 0.58333331 0.66666663 0.65624994 0.58333331 0.625
		 0.66666663 0.70833325 0.58333331 0.66666669 0.66666663 0.37499997 0.66666663 0.40624997
		 0.74999994 0.33333331 0.66666663 0.29166666 0.58333331 0.41666663 0.66666663 0.43749997
		 0.74999994 0.4569672 0.66377592 0.47111931 0.75379086 0.39583334 0.58333331 0.49998885
		 0.66663706 0.49999997 0.74999994 0.54166663 0.66666663 0.53124994 0.74999994 0.5
		 0.58333331 0.58333331 0.66666663 0.56249994 0.74999994 0.625 0.66666663 0.59374994
		 0.74999994 0.60416663 0.58333331 0.66666669 0.66666663 0.62499994 0.74999994 0.65624994
		 0.58333331 0.40624997 0.74999994 0.43749994 0.83333325 0.37499997 0.74999994 0.33333331
		 0.66666663 0.43749997 0.74999994 0.45833325 0.83333325 0.37499997 0.66666663 0.47111931
		 0.75379086 0.47916657 0.83333325 0.49999997 0.74999994 0.49999988 0.83333325 0.4569672
		 0.66377592 0.53124994 0.74999994 0.52083319 0.83333325 0.49998885 0.66663706 0.56249994
		 0.74999994 0.54166651 0.83333325 0.54166663 0.66666663 0.59374994 0.74999994 0.56249982
		 0.83333325 0.62499994 0.74999994 0.58333313 0.83333325 0.625 0.66666663 0.43749994
		 0.83333325 0.46874994 0.91666657 0.41666663 0.83333325 0.37499997 0.74999994 0.45833325
		 0.83333325 0.4791666 0.91666657 0.47916657 0.83333325 0.48957956 0.916637 0.43749997
		 0.74999994 0.49999988 0.83333325 0.49999991 0.91287559 0.47111931 0.75379086 0.52083319
		 0.83333325 0.51041657 0.91666657 0.49999997 0.74999994 0.54166651 0.83333325 0.52083325
		 0.91666657 0.56249982 0.83333325 0.53124994 0.91666657 0.56249994 0.74999994 0.58333313
		 0.83333325 0.54166663 0.91666657 0.59374994 0.74999994 0.46874994 0.91666657 0.5
		 1 0.45833328 0.91666657 0.41666663 0.83333325 0.4791666 0.91666657 0.5 1 0.43749994
		 0.83333325 0.48957956 0.916637 0.49999991 0.91287559 0.47916657 0.83333325 0.51041657
		 0.91666657 0.52083325 0.91666657 0.52083319 0.83333325 0.53124994 0.91666657 0.54166663
		 0.91666657 0.56249982 0.83333325 0.37500015 0.50000107 0.4375 0.5 0.4375 0.5 0.36639407
		 0.49632704 0.44791669 0.58333331 0.37500015 0.50000107 0.36639407 0.49632704 0.44791669
		 0.58333331 0.49999988 0.50000107 0.5625 0.5 0.5625 0.5 0.49231228 0.49632704 0.55208331
		 0.58333331 0.49999988 0.50000107 0.49231228 0.49632704 0.55208331 0.58333331 0.62499958
		 0.50000107 0.6875 0.5 0.61823052 0.49632704 0.65624994 0.58333331 0.62499958 0.50000107
		 0.61823052 0.49632704 0.41666663 0.66666663 0.34375 0.58333331 0.34375 0.58333331
		 0.41666663 0.66666663 0.49998885 0.66663706 0.44791669 0.58333331 0.44791669 0.58333331
		 0.49998885 0.66663706 0.58333331 0.66666663 0.55208331 0.58333331 0.55208331 0.58333331
		 0.58333331 0.66666663 0.66666669 0.66666663 0.65624994 0.58333331 0.65624994 0.58333331
		 0.66666669 0.66666663 0.40624997 0.74999994 0.33333331 0.66666663 0.33333331 0.66666663
		 0.40624997 0.74999994 0.47111931 0.75379086 0.41666663 0.66666663 0.41666663 0.66666663
		 0.53124994 0.74999994 0.49998885 0.66663706 0.53124994 0.74999994 0.59374994 0.74999994
		 0.58333331 0.66666663 0.58333331 0.66666663 0.59374994 0.74999994 0.45833325 0.83333325
		 0.40624997 0.74999994 0.40624997 0.74999994 0.45833325 0.83333325 0.49999988 0.83333325
		 0.47111931 0.75379086 0.47111931 0.75379086 0.49999988 0.83333325 0.54166651 0.83333325
		 0.53124994 0.74999994 0.53124994 0.74999994 0.54166651 0.83333325 0.58333313 0.83333325
		 0.59374994 0.74999994 0.58333313 0.83333325 0.46874994 0.91666657 0.41666663 0.83333325
		 0.41666663 0.83333325 0.48957956 0.916637 0.45833325 0.83333325;
	setAttr ".uvst[1].uvsp[250:346]" 0.45833325 0.83333325 0.48957956 0.916637
		 0.51041657 0.91666657 0.49999988 0.83333325 0.49999988 0.83333325 0.51041657 0.91666657
		 0.53124994 0.91666657 0.54166651 0.83333325 0.54166651 0.83333325 0.53124994 0.91666657
		 0.4791666 0.91666657 0.5 1 0.5 1 0.4791666 0.91666657 0.5 1 0.46874994 0.91666657
		 0.46874994 0.91666657 0.5 1 0.49999991 0.91287559 0.5 1 0.5 1 0.5 1 0.48957956 0.916637
		 0.5 1 0.52083325 0.91666657 0.5 1 0.5 1 0.52083325 0.91666657 0.5 1 0.51041657 0.91666657
		 0.51041657 0.91666657 0.5 1 0.54166663 0.91666657 0.5 1 0.5 1 0.54166663 0.91666657
		 0.5 1 0.53124994 0.91666657 0.53124994 0.91666657 0.5 1 0.43749994 0.83333325 0.4791666
		 0.91666657 0.43749994 0.83333325 0.37499997 0.74999994 0.43749994 0.83333325 0.43749994
		 0.83333325 0.625 0.66666663 0.62499994 0.74999994 0.62499994 0.74999994 0.625 0.66666663
		 0.60416663 0.58333331 0.625 0.66666663 0.625 0.66666663 0.60416663 0.58333331 0.5625
		 0.5 0.60416663 0.58333331 0.60416663 0.58333331 0.5625 0.5 0.52083319 0.83333325
		 0.52083325 0.91666657 0.52083325 0.91666657 0.52083319 0.83333325 0.49999997 0.74999994
		 0.52083319 0.83333325 0.52083319 0.83333325 0.49999997 0.74999994 0.4569672 0.66377592
		 0.49999997 0.74999994 0.49999997 0.74999994 0.39583334 0.58333331 0.4569672 0.66377592
		 0.39583334 0.58333331 0.3125 0.5 0.39583334 0.58333331 0.39583334 0.58333331 0.3125
		 0.5 0.56249982 0.83333325 0.54166663 0.91666657 0.54166663 0.91666657 0.56249982
		 0.83333325 0.56249994 0.74999994 0.56249982 0.83333325 0.56249982 0.83333325 0.56249994
		 0.74999994 0.54166663 0.66666663 0.56249994 0.74999994 0.56249994 0.74999994 0.54166663
		 0.66666663 0.5 0.58333331 0.54166663 0.66666663 0.5 0.58333331 0.4375 0.5 0.5 0.58333331
		 0.5 0.58333331 0.49999991 0.91287559 0.6875 0.5 0.70833325 0.58333331;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 262 ".vt";
	setAttr ".vt[0:165]"  -21 16 1.50014997 -19.50015068 16 3 -21 28 1.50014997
		 -19.50015068 28 3 -15 16 1.50014997 -16.49984932 16 3 -15 28 1.50014997 -16.49984932 28 3
		 -21 28 -1.50014997 -19.50015068 28 -3 -21 16 -1.50014997 -19.50015068 16 -3 -16.49984932 28 -3
		 -15 28 -1.50014997 -15 16 -1.50014997 -16.49984932 16 -3 -24 37.50025177 3.5002501
		 -21.50024986 37.50025177 6 -21.50024986 40 3.5002501 -14.49975014 40 3.5002501 -14.49975014 37.50025177 6
		 -12 37.50025177 3.5002501 -24 37.50025177 -3.5002501 -21.50024986 40 -3.5002501 -21.50024986 37.50025177 -6
		 -14.49975014 37.50025177 -6 -14.49975014 40 -3.5002501 -12 37.50025177 -3.5002501
		 -21.50024986 31 6 -24 31 3.5002501 -12 31 3.5002501 -14.49975014 31 6 -24 31 -3.5002501
		 -21.50024986 31 -6 -14.49975014 31 -6 -12 31 -3.5002501 -23 28 2.5002501 -20.50024986 28 5
		 -23 31 2.5002501 -20.50024986 31 5 -13 28 2.5002501 -15.49975014 28 5 -15.49975014 31 5
		 -13 31 2.5002501 -20.50024986 31 -5 -23 31 -2.5002501 -23 28 -2.5002501 -20.50024986 28 -5
		 -13 31 -2.5002501 -15.49975014 31 -5 -13 28 -2.5002501 -15.49975014 28 -5 -23.03966713 39.077758789 3.5002501
		 -21.50024986 39.077758789 5.039667606 -14.49975014 39.077758789 5.039667606 -12.96033287 39.077758789 3.5002501
		 -12.96033287 39.077758789 -3.5002501 -14.49975014 39.077758789 -5.03966713 -21.50024986 39.077758789 -5.039667606
		 -23.03966713 39.077758789 -3.5002501 -21.97871399 31 3.9787128 -23.03966713 31 5.039667606
		 -23.03966713 37.50025177 5.039667606 -22.58447266 38.6620903 4.58447266 -14.021286964 31 3.9787128
		 -12.96033287 31 5.039667606 -12.96033287 37.50025177 5.039667606 -13.4155283 38.6620903 4.58447266
		 -21.97871399 31 -3.9787128 -23.03966713 31 -5.039667606 -23.03966713 37.50025177 -5.039667606
		 -22.58447266 38.6620903 -4.58447266 -14.021286964 31 -3.9787128 -12.96033287 31 -5.039667606
		 -12.96033287 37.50025177 -5.039667606 -13.4155283 38.6620903 -4.58447266 -14.021286964 28 -3.9787128
		 -21.97871399 28 3.97871256 -21.97871399 28 -3.97871256 -18 28 -1.5979197e-17 -14.021286964 28 3.9787128
		 -22 12.49537277 -7 -20.50462723 11 -7 -22 12.49537277 6 -20.50462723 11 6 -15.49537182 11 -7
		 -14 12.49537277 -7 -15.49537182 11 6 -14 12.49537277 6 -20.50462723 19 -7 -22 17.50462723 -7
		 -20.50462723 19 6 -22 17.50462723 6 -14 17.50462723 -7 -15.49537182 19 -7 -14 17.50462723 6
		 -15.49537182 19 6 -19.49567032 17.99104309 6 -20.99104309 16.49567032 6 -15.0089569092 16.49567032 6
		 -16.50432968 17.99104309 6 -20.99104309 13.50432968 6 -19.49567032 12.0089569092 6
		 -16.50432968 12.0089569092 6 -15.0089569092 13.50432968 6 -20.99104309 16.49567032 8
		 -19.49567032 17.99104309 8 -15.0089569092 16.49567032 8 -16.50432968 17.99104309 8
		 -19.49567032 12.0089569092 8 -20.99104309 13.50432968 8 -15.0089569092 13.50432968 8
		 -16.50432968 12.0089569092 8 -20.060417175 18.29057884 -7 -21.29058075 17.060417175 -7
		 -15.93958187 18.29057884 -7 -14.70942116 17.060417175 -7 -14.70942116 12.93958282 -7
		 -15.93958187 11.70941925 -7 -20.060417175 11.70941925 -7 -21.29058075 12.93958282 -7
		 -19.1934967 16.90606689 -10.5 -19.90607071 16.1934967 -10.5 -16.8065033 16.90606689 -10.5
		 -16.093931198 16.1934967 -10.5 -16.093931198 13.8065033 -10.5 -16.8065033 13.093929291 -10.5
		 -19.1934967 13.093929291 -10.5 -19.90607071 13.8065033 -10.5 -20.060417175 18.29057884 -7.49999905
		 -21.29058075 17.060417175 -7.49999905 -20.060417175 18.29057884 -8.96627808 -21.29058075 17.060417175 -8.96627808
		 -15.93958187 18.29057884 -7.49999905 -15.93958187 18.29057884 -8.96627808 -14.70942116 17.060417175 -7.49999905
		 -14.70942116 17.060417175 -8.96627808 -14.70942116 12.93958282 -7.49999905 -14.70942116 12.93958282 -8.96627808
		 -15.93958187 11.70941925 -7.49999905 -15.93958187 11.70941925 -8.96627808 -20.060417175 11.70941925 -7.49999905
		 -20.060417175 11.70941925 -8.96627808 -21.29058075 12.93958282 -7.49999905 -21.29058075 12.93958282 -8.96627808
		 -19.73308945 17.50033569 -7 -20.50033951 16.73308945 -7 -19.73308945 17.50033569 -7.49999905
		 -20.50033951 16.73308945 -7.49999905 -16.26691055 17.5003376 -7 -16.26691055 17.5003376 -7.49999905
		 -15.4996624 16.73308945 -7 -15.4996624 16.73308945 -7.49999905 -15.49966335 13.26691055 -7
		 -15.49966335 13.26691055 -7.49999905 -16.26691055 12.49966049 -7 -16.26691055 12.49966049 -7.49999905
		 -19.73308754 12.4996624 -7 -19.73308754 12.4996624 -7.49999905 -20.5003376 13.26691246 -7
		 -20.5003376 13.26691246 -7.49999905 -14.25 18.75 6.000000953674 -18 20.30330086 6.000000953674
		 -21.75 18.75 6.000000953674 -23.30330276 15 6.000000953674 -21.75 11.25 6.000000953674;
	setAttr ".vt[166:261]" -18 9.69669914 6.000000953674 -14.25 11.25 6.000000953674
		 -12.69669724 15 6.000000953674 -12.69669724 20.30330276 8.38713074 -18 22.5 8.38713074
		 -23.30330276 20.30330276 8.38713074 -25.5 15 8.38713074 -23.30330276 9.69669914 8.38713074
		 -18 7.5 8.38713074 -12.69669724 9.69669914 8.38713074 -10.5 15 8.38713074 -13.58058548 19.41941833 10.88713074
		 -18 21.25 10.88713074 -22.41941452 19.41941833 10.88713074 -24.24999619 15 10.88713074
		 -22.41941452 10.58058357 10.88713074 -18 8.75000381 10.88713074 -13.58058548 10.58058357 10.88713074
		 -11.75000381 15 10.88713074 -14.46446609 18.53553391 13.38712978 -18 20 13.38712978
		 -21.53553391 18.53553391 13.38712978 -22.99999619 15 13.38712978 -21.53553391 11.464468 13.38712978
		 -18 10.000001907349 13.38712978 -14.46446609 11.464468 13.38712978 -13.000003814697 15 13.38712978
		 -15.34834671 17.65165329 15.88713074 -18 18.75 15.88713074 -20.65165329 17.65165329 15.88713074
		 -21.75 15 15.88713074 -20.65165329 12.34835243 15.88713074 -18 11.25 15.88713074
		 -15.34834671 12.34835243 15.88713074 -14.25 15 15.88713074 -16.23223114 16.76776886 18.38712883
		 -18 17.50000191 18.38712883 -19.76776886 16.76776886 18.38712883 -20.49999619 15 18.38712883
		 -19.76776886 13.23223686 18.38712883 -18 12.50000381 18.38712883 -16.23223114 13.23223686 18.38712883
		 -15.50000381 15 18.38712883 -16.79259109 15.32352448 20.88713074 -17.37499619 16.082536697 20.88713074
		 -18.32352448 16.20740891 20.88713074 -19.082530975 15.62500191 20.88713074 -19.20740891 14.67647552 20.88713074
		 -18.62500381 13.91746902 20.88713074 -17.67647552 13.79259109 20.88713074 -16.91746902 14.375 20.88713074
		 -18 15 23.38712883 -17.96364975 22.013191223 8.38713741 -18.02645874 20.89758492 10.7962532
		 -22.010353088 19.22375488 10.96702385 -21.32316208 18.29549789 13.30181408 -22.60968018 15.14392853 13.45916462
		 -21.46184158 14.99048996 15.80989456 -20.49746323 12.69171143 15.94774628 -19.58965683 13.41852379 18.322155
		 -18.11827469 12.81132889 18.43042183 -18.53594971 14.13082123 20.84119415 -17.77189636 14.045305252 20.88635445
		 -25.013191223 15.036352158 8.38713741 -23.89758682 14.97354317 10.7962532 -22.22375107 10.9896431 10.96702385
		 -21.29549789 11.67683792 13.30181408 -18.14393234 10.39031982 13.45916557 -17.99048996 11.53815651 15.80989456
		 -15.69170761 12.50253868 15.94774818 -16.41852188 13.41034126 18.322155 -15.81132889 14.8817215 18.43042183
		 -17.13082123 14.46405602 20.84119415 -17.045310974 15.22810936 20.88635445 -18.03635788 7.98680496 8.38713741
		 -17.97354126 9.10241318 10.7962532 -13.98964691 10.77624321 10.96702385 -14.67683792 11.70450211 13.30181408
		 -13.39031982 14.85606956 13.45916557 -14.53815842 15.0095100403 15.80989456 -15.50253677 17.30828857 15.94774628
		 -16.41034317 16.58147621 18.322155 -17.88172531 17.18867111 18.43042183 -17.46405411 15.86917877 20.84119415
		 -18.22811508 15.95469093 20.88635254 -10.98680878 14.96364403 8.38713741 -12.10241318 15.026454926 10.7962532
		 -13.7762413 19.010356903 10.96702385 -14.70450211 18.32316208 13.30181408 -17.85606766 19.60968018 13.45916557
		 -18.0095100403 18.4618454 15.80989456 -20.30828476 17.49746132 15.94774628 -19.58147812 16.58965874 18.322155
		 -20.18867111 15.1182766 18.43042374 -18.86917877 15.53594589 20.84119415 -18.95469284 14.77189064 20.88635445;
	setAttr -s 508 ".ed";
	setAttr ".ed[0:165]"  1 5 0 1 0 0 2 8 0 3 7 0 3 2 0 4 5 0 6 13 0 7 6 0 9 12 0
		 9 8 0 10 0 0 11 15 0 11 10 0 13 12 0 14 4 0 15 14 0 0 2 0 3 1 0 5 7 0 6 4 0 8 10 0
		 11 9 0 12 15 0 14 13 0 37 41 0 37 77 0 40 80 0 46 36 0 47 51 0 47 78 0 50 40 0 51 76 0
		 16 52 0 18 23 0 23 59 0 22 16 0 17 62 0 16 29 0 29 61 0 28 17 0 18 53 0 17 20 0 20 54 0
		 19 18 0 19 55 0 21 27 0 27 56 0 26 19 0 21 66 0 20 31 0 31 65 0 30 21 0 22 70 0 24 33 0
		 33 69 0 32 22 0 24 58 0 23 26 0 26 57 0 25 24 0 25 74 0 27 35 0 35 73 0 34 25 0 29 38 1
		 38 60 0 39 28 1 31 42 1 42 64 0 43 30 1 33 44 1 44 68 0 45 32 1 35 48 1 48 72 0 49 34 1
		 37 39 0 38 36 0 40 43 0 42 41 0 44 47 0 46 45 0 48 50 0 51 49 0 28 31 0 34 33 0 30 35 0
		 32 29 0 42 39 0 44 49 0 48 43 0 38 45 0 52 18 0 53 17 0 54 19 0 55 21 0 56 26 0 57 25 0
		 58 23 0 59 22 0 52 63 1 53 54 1 54 67 1 55 56 1 56 75 1 57 58 1 58 71 1 59 52 1 60 39 0
		 61 28 0 62 16 0 63 53 1 60 61 1 61 62 1 62 63 1 64 43 0 65 30 0 66 20 0 67 55 1 64 65 1
		 65 66 1 66 67 1 68 45 0 69 32 0 70 24 0 71 59 1 68 69 1 69 70 1 70 71 1 72 49 0 73 34 0
		 74 27 0 75 57 1 72 73 1 73 74 1 74 75 1 76 50 0 77 36 0 76 79 1 78 46 0 79 77 1 80 41 0
		 78 79 1 79 80 1 80 76 1 76 78 1 78 77 1 77 80 1 82 85 0 82 81 0 86 85 0 89 94 0 90 81 0
		 89 90 0 93 86 0 94 93 0 106 108 0 106 105 0 107 111 0 108 107 0 110 105 0 110 109 0
		 112 109 0 112 111 0 82 84 0 84 83 0;
	setAttr ".ed[166:331]" 83 81 0 84 102 1 102 101 0 101 83 1 86 88 0 88 87 0
		 87 85 0 88 104 1 104 103 0 103 87 1 90 92 0 92 91 0 91 89 0 92 98 1 98 97 0 97 91 1
		 94 96 0 96 95 0 95 93 0 96 100 1 100 99 0 99 95 1 98 105 0 106 97 0 100 108 0 107 99 0
		 102 109 0 110 101 0 104 111 0 112 103 0 91 96 0 87 84 0 95 88 0 83 92 0 97 100 0
		 101 98 0 103 102 0 99 104 0 89 113 1 90 114 1 113 114 1 94 115 1 113 115 1 93 116 1
		 115 116 1 86 117 1 116 117 1 85 118 1 117 118 1 82 119 1 119 118 1 81 120 1 119 120 1
		 114 120 1 121 122 0 121 123 0 123 124 0 124 125 0 125 126 0 127 126 0 127 128 0 122 128 0
		 129 130 0 129 131 0 131 132 0 130 132 0 129 133 0 133 134 0 131 134 0 133 135 0 135 136 0
		 134 136 0 135 137 0 137 138 0 136 138 0 137 139 0 139 140 0 138 140 0 141 139 0 141 142 0
		 142 140 0 141 143 0 143 144 0 142 144 0 130 143 0 132 144 0 131 121 0 132 122 0 134 123 0
		 136 124 0 138 125 0 140 126 0 142 127 0 144 128 0 113 145 1 114 146 1 145 146 0 129 147 1
		 145 147 0 130 148 1 147 148 0 146 148 0 115 149 1 145 149 0 133 150 1 149 150 0 147 150 0
		 116 151 1 149 151 0 135 152 1 151 152 0 150 152 0 117 153 1 151 153 0 137 154 1 153 154 0
		 152 154 0 118 155 1 153 155 0 139 156 1 155 156 0 154 156 0 119 157 1 157 155 0 141 158 1
		 157 158 0 158 156 0 120 159 1 157 159 0 143 160 1 159 160 0 158 160 0 146 159 0 148 160 0
		 161 162 0 162 163 0 163 164 0 164 165 0 165 166 0 166 167 0 167 168 0 168 161 0 169 170 0
		 170 171 0 171 172 0 172 173 0 173 174 0 174 175 0 175 176 0 176 169 0 161 169 0 162 170 0
		 163 171 0 164 172 0 165 173 0 166 174 0 167 175 0 168 176 0 169 177 1 171 179 1 173 181 1
		 178 186 1 180 188 1 182 190 1 187 195 1 189 197 1;
	setAttr ".ed[332:497]" 191 199 1 196 204 1 198 206 1 200 208 1 201 209 1 205 213 1
		 207 215 1 209 217 0 210 217 0 211 217 0 212 217 0 213 217 0 214 217 0 215 217 0 216 217 0
		 212 203 0 203 194 0 194 185 0 185 184 0 184 175 0 211 202 0 202 193 0 193 192 0 192 183 0
		 183 174 0 175 183 1 184 192 1 185 193 1 194 202 1 203 211 1 216 207 0 207 198 0 198 189 0
		 189 180 0 180 171 0 215 206 0 206 197 0 197 188 0 188 179 0 179 170 0 210 201 0 201 200 0
		 200 191 0 191 182 0 182 173 0 209 208 0 208 199 0 199 190 0 190 181 0 181 172 0 214 205 0
		 205 196 0 196 187 0 187 178 0 178 169 0 213 204 0 204 195 0 195 186 0 186 177 0 177 176 0
		 170 218 1 169 218 0 178 219 1 218 219 1 219 169 0 179 220 1 187 221 1 220 221 1 221 219 0
		 188 222 1 196 223 1 222 223 1 223 221 0 197 224 1 205 225 1 224 225 1 225 223 0 206 226 1
		 214 227 1 226 227 1 227 225 0 215 228 1 228 217 0 227 217 0 228 226 0 226 224 0 224 222 0
		 222 220 0 220 218 0 172 229 1 171 229 0 180 230 1 229 230 1 230 171 0 181 231 1 189 232 1
		 231 232 1 232 230 0 190 233 1 198 234 1 233 234 1 234 232 0 199 235 1 207 236 1 235 236 1
		 236 234 0 208 237 1 216 238 1 237 238 1 238 236 0 209 239 1 239 217 0 238 217 0 239 237 0
		 237 235 0 235 233 0 233 231 0 231 229 0 174 240 1 173 240 0 182 241 1 240 241 1 241 173 0
		 183 242 1 191 243 1 242 243 1 243 241 0 192 244 1 200 245 1 244 245 1 245 243 0 193 246 1
		 201 247 1 246 247 1 247 245 0 202 248 1 210 249 1 248 249 1 249 247 0 211 250 1 250 217 0
		 249 217 0 250 248 0 248 246 0 246 244 0 244 242 0 242 240 0 176 251 1 175 251 0 184 252 1
		 251 252 1 252 175 0 177 253 1 185 254 1 253 254 1 254 252 0 186 255 1 194 256 1 255 256 1
		 256 254 0 195 257 1 203 258 1 257 258 1 258 256 0 204 259 1 212 260 1;
	setAttr ".ed[498:507]" 259 260 1 260 258 0 213 261 1 261 217 0 260 217 0 261 259 0
		 259 257 0 257 255 0 255 253 0 253 251 0;
	setAttr -s 254 -ch 1016 ".fc[0:253]" -type "polyFaces" 
		f 4 17 0 18 -4
		mu 0 4 8 18 9 21
		f 4 21 8 22 -12
		mu 0 4 13 22 14 25
		f 4 19 -15 23 -7
		mu 0 4 15 20 16 17
		f 4 20 10 16 2
		mu 0 4 10 11 12 19
		f 8 -5 3 7 6 13 -9 9 -3
		mu 0 8 0 8 21 1 24 14 22 2
		f 8 -13 11 15 14 5 -1 1 -11
		mu 0 8 23 13 25 3 4 5 6 7
		f 4 -2 -18 4 -17
		mu 0 4 12 18 8 19
		f 4 -6 -20 -8 -19
		mu 0 4 9 20 15 21
		f 4 -10 -22 12 -21
		mu 0 4 2 22 13 23
		f 4 -14 -24 -16 -23
		mu 0 4 14 24 3 25
		f 4 -140 146 137 -28
		mu 1 4 0 1 2 3
		f 4 107 92 33 34
		mu 0 4 26 27 28 29
		f 4 113 110 37 38
		mu 0 4 30 31 32 33
		f 4 40 101 94 43
		mu 0 4 28 34 35 36
		f 4 44 103 96 47
		mu 0 4 36 37 38 39
		f 4 120 117 49 50
		mu 0 4 40 41 42 43
		f 4 127 124 53 54
		mu 0 4 44 45 46 47
		f 4 105 98 57 58
		mu 0 4 48 49 29 39
		f 4 134 131 61 62
		mu 0 4 50 51 52 53
		f 4 112 -39 64 65
		f 4 119 -51 67 68
		f 4 126 -55 70 71
		f 4 133 -63 73 74
		f 6 -138 -26 76 -109 -66 77
		mu 1 6 4 5 6 7 8 9
		f 6 -142 -27 78 -116 -69 79
		mu 1 6 10 11 12 13 14 15
		f 6 -123 -72 80 29 139 81
		mu 1 6 16 17 18 19 1 0
		f 6 -130 -75 82 -137 -32 83
		mu 1 6 20 21 22 23 24 25
		f 4 84 -50 -42 -40
		mu 0 4 54 43 42 55
		f 4 -44 -48 -58 -34
		mu 0 4 28 36 39 29
		f 4 -60 -64 85 -54
		mu 0 4 46 56 57 47
		f 4 86 -62 -46 -52
		mu 0 4 58 59 60 61
		f 4 87 -38 -36 -56
		mu 0 4 62 33 32 63
		f 4 -77 24 -80 88
		mu 1 4 7 6 10 15
		f 4 89 -84 -29 -81
		mu 1 4 18 20 25 19
		f 4 -79 -31 -83 90
		mu 1 4 26 12 27 28
		f 4 -82 27 -78 91
		mu 1 4 29 30 4 31
		f 4 -85 -67 -89 -68
		f 4 -88 -73 -92 -65
		f 4 -86 -76 -90 -71
		f 4 -87 -70 -91 -74
		f 4 100 111 -41 -93
		mu 0 4 64 65 34 28
		f 4 -95 102 118 -45
		mu 0 4 36 35 66 67
		f 4 106 125 -35 -99
		mu 0 4 49 68 26 29
		f 4 104 132 -59 -97
		mu 0 4 38 69 48 39
		f 4 -111 114 -101 -33
		mu 0 4 32 31 65 64
		f 4 -102 93 41 42
		mu 0 4 35 34 55 42
		f 4 121 -103 -43 -118
		mu 0 4 41 66 35 42
		f 4 -104 95 45 46
		mu 0 4 38 37 70 71
		f 4 135 -105 -47 -132
		mu 0 4 72 69 38 71
		f 4 56 -106 97 59
		mu 0 4 46 49 48 56
		f 4 128 -107 -57 -125
		mu 0 4 73 68 49 46
		f 4 32 -108 99 35
		mu 0 4 74 27 26 75
		f 4 -110 -113 108 66
		f 4 36 -114 109 39
		mu 0 4 55 31 30 54
		f 4 -115 -37 -94 -112
		mu 0 4 65 31 55 34
		f 4 -117 -120 115 69
		f 4 48 -121 116 51
		mu 0 4 61 41 40 58
		f 4 -119 -122 -49 -96
		mu 0 4 67 66 41 61
		f 4 -124 -127 122 72
		f 4 52 -128 123 55
		mu 0 4 76 45 44 77
		f 4 -100 -126 -129 -53
		mu 0 4 75 26 68 73
		f 4 -131 -134 129 75
		f 4 60 -135 130 63
		mu 0 4 56 51 50 57
		f 4 -98 -133 -136 -61
		mu 0 4 56 48 69 72
		f 4 147 141 -25 25
		mu 1 4 2 32 33 34
		f 4 145 -30 28 31
		mu 1 4 24 1 19 25
		f 4 144 136 30 26
		mu 1 4 32 24 23 35
		f 3 -144 -139 -145
		mu 1 3 32 36 24
		f 3 -143 -146 138
		mu 1 3 36 1 24
		f 3 -147 142 140
		mu 1 3 2 1 36
		f 3 -141 143 -148
		mu 1 3 2 36 32
		f 8 -221 221 222 223 224 -226 226 -228
		mu 0 8 78 79 80 81 82 83 84 85
		f 8 -160 -157 157 -161 161 -163 163 -159
		mu 0 8 86 87 88 89 90 91 92 93
		f 4 -150 164 165 166
		mu 0 4 94 95 96 97
		f 4 -166 167 168 169
		mu 0 4 98 99 100 101
		f 4 -151 170 171 172
		mu 0 4 102 103 104 105
		f 4 -172 173 174 175
		mu 0 4 106 107 108 109
		f 4 153 176 177 178
		mu 0 4 110 111 112 113
		f 4 -178 179 180 181
		mu 0 4 114 115 116 117
		f 4 -156 182 183 184
		mu 0 4 118 119 120 121
		f 4 -184 185 186 187
		mu 0 4 121 122 123 124
		f 4 -181 188 -158 189
		mu 0 4 117 116 89 88
		f 4 -187 190 159 191
		mu 0 4 124 123 87 86
		f 4 -169 192 -162 193
		mu 0 4 101 100 91 90
		f 4 -175 194 -164 195
		mu 0 4 109 108 93 92
		f 4 196 -183 -152 -179
		mu 0 4 113 120 119 110
		f 4 -165 148 -173 197
		mu 0 4 96 95 102 105
		f 4 198 -171 -155 -185
		mu 0 4 125 126 127 128
		f 4 -167 199 -177 152
		mu 0 4 129 130 131 132
		f 4 -197 -182 200 -186
		mu 0 4 122 114 117 123
		f 4 -200 -170 201 -180
		mu 0 4 133 98 101 116
		f 4 -198 -176 202 -168
		mu 0 4 134 106 109 100
		f 4 -199 -188 203 -174
		mu 0 4 135 136 124 108
		f 4 -201 -190 156 -191
		mu 0 4 123 117 88 87
		f 4 -202 -194 160 -189
		mu 0 4 116 101 90 89
		f 4 -203 -196 162 -193
		mu 0 4 100 109 92 91
		f 4 -204 -192 158 -195
		mu 0 4 108 124 86 93
		f 4 -154 204 206 -206
		mu 0 4 111 110 137 138
		f 4 151 207 -209 -205
		mu 0 4 110 119 139 137
		f 4 155 209 -211 -208
		mu 0 4 119 118 140 139
		f 4 154 211 -213 -210
		mu 0 4 118 103 141 140
		f 4 150 213 -215 -212
		mu 0 4 103 102 142 141
		f 4 -149 215 216 -214
		mu 0 4 102 95 143 142
		f 4 149 217 -219 -216
		mu 0 4 95 94 144 143
		f 4 -153 205 219 -218
		mu 0 4 94 111 138 144
		f 4 -229 229 230 -232
		mu 0 4 145 146 147 148
		f 4 232 233 -235 -230
		mu 0 4 146 149 150 147
		f 4 235 236 -238 -234
		mu 0 4 149 151 152 150
		f 4 238 239 -241 -237
		mu 0 4 151 153 154 152
		f 4 241 242 -244 -240
		mu 0 4 153 155 156 154
		f 4 -245 245 246 -243
		mu 0 4 155 157 158 156
		f 4 247 248 -250 -246
		mu 0 4 157 159 160 158
		f 4 -251 231 251 -249
		mu 0 4 159 145 148 160
		f 4 -231 252 220 -254
		mu 0 4 148 147 79 78
		f 4 234 254 -222 -253
		mu 0 4 147 150 80 79
		f 4 237 255 -223 -255
		mu 0 4 150 152 81 80
		f 4 240 256 -224 -256
		mu 0 4 152 154 82 81
		f 4 243 257 -225 -257
		mu 0 4 154 156 83 82
		f 4 -247 258 225 -258
		mu 0 4 156 158 84 83
		f 4 249 259 -227 -259
		mu 0 4 158 160 85 84
		f 4 -252 253 227 -260
		mu 0 4 160 148 78 85
		f 4 -263 264 266 -268
		mu 0 4 161 162 163 164
		f 4 269 271 -273 -265
		mu 0 4 162 165 166 163
		f 4 274 276 -278 -272
		mu 0 4 165 167 168 166
		f 4 279 281 -283 -277
		mu 0 4 167 169 170 168
		f 4 284 286 -288 -282
		mu 0 4 169 171 172 170
		f 4 -290 291 292 -287
		mu 0 4 171 173 174 172
		f 4 294 296 -298 -292
		mu 0 4 173 175 176 174
		f 4 -299 267 299 -297
		mu 0 4 175 161 164 176
		f 4 -207 260 262 -262
		mu 0 4 138 137 162 161
		f 4 228 265 -267 -264
		mu 0 4 146 145 164 163
		f 4 208 268 -270 -261
		mu 0 4 137 139 165 162
		f 4 -233 263 272 -271
		mu 0 4 149 146 163 166
		f 4 210 273 -275 -269
		mu 0 4 139 140 167 165
		f 4 -236 270 277 -276
		mu 0 4 151 149 166 168
		f 4 212 278 -280 -274
		mu 0 4 140 141 169 167
		f 4 -239 275 282 -281
		mu 0 4 153 151 168 170
		f 4 214 283 -285 -279
		mu 0 4 141 142 171 169
		f 4 -242 280 287 -286
		mu 0 4 155 153 170 172
		f 4 -217 288 289 -284
		mu 0 4 142 143 173 171
		f 4 244 285 -293 -291
		mu 0 4 157 155 172 174
		f 4 218 293 -295 -289
		mu 0 4 143 144 175 173
		f 4 -248 290 297 -296
		mu 0 4 159 157 174 176
		f 4 -220 261 298 -294
		mu 0 4 144 138 161 175
		f 4 250 295 -300 -266
		mu 0 4 145 159 176 164
		f 4 300 317 -309 -317
		mu 1 4 37 38 39 40
		f 4 301 318 -310 -318
		mu 1 4 38 41 42 39
		f 4 302 319 -311 -319
		mu 1 4 41 43 44 42
		f 4 303 320 -312 -320
		mu 1 4 43 45 46 44
		f 4 304 321 -313 -321
		mu 1 4 45 47 48 46
		f 4 305 322 -314 -322
		mu 1 4 47 49 50 48
		f 4 306 323 -315 -323
		mu 1 4 49 51 52 50
		f 4 307 316 -316 -324
		mu 1 4 51 37 40 52
		f 3 393 395 396
		mu 1 3 53 54 55
		f 3 309 325 371
		mu 1 3 56 57 58
		f 3 422 424 425
		mu 1 3 59 60 61
		f 3 311 326 381
		mu 1 3 62 63 64
		f 3 451 453 454
		mu 1 3 65 66 67
		f 3 313 357 356
		mu 1 3 68 69 70
		f 3 480 482 483
		mu 1 3 71 72 73
		f 3 315 324 391
		mu 1 3 74 75 76
		f 4 327 390 -325 -387
		mu 1 4 77 78 79 53
		f 4 399 400 -396 -421
		mu 1 4 80 81 55 54
		f 4 328 370 -326 -367
		mu 1 4 82 83 58 57
		f 4 428 429 -425 -450
		mu 1 4 84 85 61 60
		f 4 329 380 -327 -377
		mu 1 4 86 87 64 63
		f 4 457 458 -454 -479
		mu 1 4 88 89 67 66
		f 4 358 355 -358 -352
		mu 1 4 90 91 70 69
		f 4 486 487 -483 -508
		mu 1 4 92 93 73 72
		f 4 490 491 -487 -507
		mu 1 4 94 95 96 97
		f 4 330 389 -328 -386
		mu 1 4 98 99 78 77
		f 4 403 404 -400 -420
		mu 1 4 100 101 81 102
		f 4 331 369 -329 -366
		mu 1 4 103 104 83 82
		f 4 432 433 -429 -449
		mu 1 4 105 106 85 107
		f 4 332 379 -330 -376
		mu 1 4 108 109 87 86
		f 4 461 462 -458 -478
		mu 1 4 110 111 89 112
		f 4 359 354 -359 -351
		mu 1 4 113 114 91 115
		f 4 360 353 -360 -350
		mu 1 4 116 117 118 119
		f 4 494 495 -491 -506
		mu 1 4 120 121 95 122
		f 4 333 388 -331 -385
		mu 1 4 123 124 99 98
		f 4 407 408 -404 -419
		mu 1 4 125 126 101 127
		f 4 334 368 -332 -365
		mu 1 4 128 129 104 130
		f 4 436 437 -433 -448
		mu 1 4 131 132 106 133
		f 4 335 378 -333 -375
		mu 1 4 134 135 109 108
		f 4 465 466 -462 -477
		mu 1 4 136 137 111 138
		f 4 469 470 -466 -476
		mu 1 4 139 140 141 142
		f 4 361 352 -361 -349
		mu 1 4 143 144 117 116
		f 4 498 499 -495 -505
		mu 1 4 145 146 121 147
		f 4 337 387 -334 -384
		mu 1 4 148 149 124 150
		f 4 411 412 -408 -418
		mu 1 4 151 152 126 153
		f 4 338 367 -335 -364
		mu 1 4 154 155 129 128
		f 4 440 441 -437 -447
		mu 1 4 156 157 132 158
		f 4 336 377 -336 -374
		mu 1 4 159 160 135 161
		f 8 -306 -305 -304 -303 -302 -301 -308 -307
		mu 1 8 49 47 45 43 41 38 37 51
		f 4 340 -340 -337 -373
		mu 1 4 162 163 164 165
		f 4 472 -474 -470 -475
		mu 1 4 166 167 140 168
		f 4 342 -342 -362 -348
		mu 1 4 169 163 144 143
		f 4 501 -503 -499 -504
		mu 1 4 170 167 146 171
		f 4 344 -344 -338 -383
		mu 1 4 172 163 149 148
		f 4 414 -416 -412 -417
		mu 1 4 173 167 152 174
		f 4 346 -346 -339 -363
		mu 1 4 175 163 155 154
		f 4 443 -445 -441 -446
		mu 1 4 176 167 157 177
		f 3 308 392 -394
		mu 1 3 53 56 54
		f 3 386 -397 -395
		mu 1 3 77 53 55
		f 4 385 394 -401 -399
		mu 1 4 178 179 180 181
		f 4 384 398 -405 -403
		mu 1 4 182 183 184 185
		f 4 383 402 -409 -407
		mu 1 4 186 187 188 189
		f 4 382 406 -413 -411
		mu 1 4 190 191 192 193
		f 3 345 -415 -414
		mu 1 3 194 195 196
		f 3 -345 410 415
		mu 1 3 197 198 199
		f 4 -368 413 416 -410
		mu 1 4 200 201 202 203
		f 4 -369 409 417 -406
		mu 1 4 204 205 206 207
		f 4 -370 405 418 -402
		mu 1 4 208 209 210 211
		f 4 -371 401 419 -398
		mu 1 4 212 213 214 215
		f 4 -372 397 420 -393
		mu 1 4 216 217 218 219
		f 3 310 421 -423
		mu 1 3 220 221 222
		f 3 366 -426 -424
		mu 1 3 223 224 225
		f 4 365 423 -430 -428
		mu 1 4 226 227 228 229
		f 4 364 427 -434 -432
		mu 1 4 230 231 232 233
		f 4 363 431 -438 -436
		mu 1 4 234 235 236 237
		f 4 362 435 -442 -440
		mu 1 4 238 239 240 241
		f 3 339 -444 -443
		mu 1 3 242 243 244
		f 3 -347 439 444
		mu 1 3 245 246 247
		f 4 -378 442 445 -439
		mu 1 4 248 249 250 251
		f 4 -379 438 446 -435
		mu 1 4 252 253 254 255
		f 4 -380 434 447 -431
		mu 1 4 256 257 258 259
		f 4 -381 430 448 -427
		mu 1 4 260 261 262 263
		f 4 -382 426 449 -422
		mu 1 4 264 265 266 267
		f 3 312 450 -452
		mu 1 3 268 269 270
		f 3 376 -455 -453
		mu 1 3 271 272 273
		f 4 375 452 -459 -457
		mu 1 4 274 275 276 277
		f 4 374 456 -463 -461
		mu 1 4 278 279 280 281
		f 4 373 460 -467 -465
		mu 1 4 282 283 284 285
		f 4 372 464 -471 -469
		mu 1 4 286 287 288 289
		f 3 341 -473 -472
		mu 1 3 290 291 292
		f 3 -341 468 473
		mu 1 3 293 294 295
		f 4 -353 471 474 -468
		mu 1 4 296 297 298 299
		f 4 -354 467 475 -464
		mu 1 4 300 301 302 303
		f 4 -355 463 476 -460
		mu 1 4 304 305 306 307
		f 4 -356 459 477 -456
		mu 1 4 308 309 310 311
		f 4 -357 455 478 -451
		mu 1 4 312 313 314 315
		f 3 314 479 -481
		mu 1 3 316 317 318
		f 3 351 -484 -482
		mu 1 3 319 320 321
		f 4 350 481 -488 -486
		mu 1 4 322 323 324 325
		f 4 349 485 -492 -490
		mu 1 4 326 327 328 329
		f 4 348 489 -496 -494
		mu 1 4 330 331 332 333
		f 4 347 493 -500 -498
		mu 1 4 334 335 336 337
		f 3 343 -502 -501
		mu 1 3 338 339 340
		f 3 -343 497 502
		mu 1 3 341 342 343
		f 4 -388 500 503 -497
		mu 1 4 124 149 344 171
		f 4 -389 496 504 -493
		mu 1 4 99 124 171 147
		f 4 -390 492 505 -489
		mu 1 4 78 99 147 122
		f 4 -391 488 506 -485
		mu 1 4 79 78 122 97
		f 4 -392 484 507 -480
		mu 1 4 345 76 346 72;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 2 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Falcon_Back";
	rename -uid "5D898561-459F-971E-2DF1-74A2EBC0325E";
	setAttr ".v" no;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 27 -16.000000953674316 ;
	setAttr ".sp" -type "double3" 0 27 -16.000000953674316 ;
createNode mesh -n "Falcon_BackShape" -p "Falcon_Back";
	rename -uid "64C2838F-4AA1-36C4-D207-ED90E5784F73";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.36458329856395721 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Falcon_BackShapeOrig" -p "Falcon_Back";
	rename -uid "79554953-4CE7-0916-2600-55B1B7FF9F76";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 200 ".uvst[0].uvsp[0:199]" -type "float2" 0.375 0.25 0.69588798
		 0.25 0.375 0.5 0.625 0.42911205 0.625 0.5 0.625 0.5 0.375 0.35805225 0.51694769 0.25
		 0.625 0.39194781 0.48305234 0.5 0.55411196 0.25 0.44588798 0.5 0.80411208 0.25 0.30411199
		 0.25 0.19588797 0.25 0.44588798 0.25 0.625 0.32088798 0.55411202 0.5 0.375 0.42911205
		 0.375 0.32088798 0.51694775 0.75 0.625 0.89194775 0.48305228 1 0.375 0.85805219 0.48305231
		 0.25 0.625 0.35805225 0.51694769 0.5 0.375 0.39194781 0.48305231 0.75 0.51694769
		 0.75 0.625 0.85805219 0.625 0.89194775 0.51694769 1 0.48305243 1 0.375 0.89194769
		 0.375 0.85805219 0.48305231 0.75 0.55213338 0.75 0.625 0.85805213 0.625 0.92713344
		 0.51694775 1 0.44786662 1 0.375 0.89194775 0.375 0.82286656 0.44786662 0.75 0.625
		 0.82286656 0.55213338 1 0.375 0.92713344 0.44588798 0.083333239 0.55411202 0.083333246
		 0.69588792 0.083333239 0.625 0.66666675 0.80411208 0.083333246 0.55411196 0.66666675
		 0.44588798 0.66666675 0.375 0.66666675 0.19588797 0.083333239 0.30411202 0.083333246
		 0.44588798 0.16666663 0.55411196 0.16666663 0.69588792 0.16666663 0.625 0.58333337
		 0.80411208 0.16666663 0.55411196 0.58333337 0.44588798 0.58333337 0.375 0.58333337
		 0.19588797 0.16666663 0.30411202 0.16666663 0.30411202 0.083333246 0.44588798 0.083333239
		 0.44588798 0.16666663 0.30411202 0.16666663 0.44588798 0.083333239 0.44588798 0.16666663
		 0.44588798 0.16666663 0.44588798 0.083333239 0.44786659 0.75 0.55213338 0.75 0.375
		 0.92713344 0.375 0.82286656 0.625 0.82286656 0.625 0.92713344 0.55213338 1 0.44786662
		 1 0.40624684 0.84375316 0.5312469 0.7812469 0.59375316 0.90624678 0.4687531 0.9687531
		 0.46875313 0.7812469 0.46875313 0.7812469 0.40624687 0.9062469 0.4062469 0.9062469
		 0.59375316 0.84375316 0.59375316 0.8437531 0.53124684 0.9687531 0.53124684 0.9687531
		 0.5312469 0.7812469 0.40624684 0.84375316 0.59375316 0.90624684 0.46875307 0.9687531
		 0.375 0.35805225 0.48305231 0.25 0.51694769 0.25 0.625 0.35805225 0.625 0.39194781
		 0.51694769 0.5 0.48305234 0.5 0.375 0.39194781 0.375 0.89194769 0.48305228 1 0.44588798
		 0.083333239 0.30411202 0.083333246 0.30411202 0.16666663 0.44588798 0.16666663 0.44588798
		 0.25 0.30411199 0.25 0.375 0.25 0.51694769 1 0.625 0.89194775 0.69588792 0.083333239
		 0.55411202 0.083333246 0.55411196 0.16666663 0.69588792 0.16666663 0.69588798 0.25
		 0.55411196 0.25 0.44588798 0.5 0.375 0.5 0.44588798 0.58333337 0.375 0.58333337 0.625
		 0.42911205 0.625 0.5 0.55411202 0.5 0.625 0.5 0.625 0.58333337 0.55411196 0.58333337
		 0.375 0.85805219 0.48305231 0.75 0.48305231 0.75 0.375 0.85805219 0.51694775 0.75
		 0.625 0.85805219 0.625 0.85805213 0.51694769 0.75 0.51694775 1 0.625 0.89194775 0.375
		 0.89194775 0.48305243 1 0.44786662 0.75 0.375 0.82286656 0.625 0.82286656 0.55213338
		 0.75 0.55213338 1 0.625 0.92713344 0.375 0.92713344 0.44786662 1 0.44786659 0.75
		 0.375 0.82286656 0.625 0.82286656 0.55213338 0.75 0.55213338 1 0.625 0.92713344 0.375
		 0.92713344 0.44786662 1 0.46875313 0.7812469 0.40624684 0.84375316 0.59375316 0.84375316
		 0.5312469 0.7812469 0.53124684 0.9687531 0.59375316 0.90624684 0.40624687 0.9062469
		 0.46875307 0.9687531 0.40624684 0.84375316 0.46875313 0.7812469 0.5312469 0.7812469
		 0.59375316 0.8437531 0.59375316 0.90624678 0.53124684 0.9687531 0.4687531 0.9687531
		 0.4062469 0.9062469 0.80411208 0.16666663 0.80411208 0.25 0.19588797 0.16666663 0.19588797
		 0.25 0.625 0.32088798 0.375 0.42911205 0.375 0.32088798 0.80411208 0.083333246 0.55411196
		 0.66666675 0.625 0.66666675 0.44588798 0.66666675 0.375 0.66666675 0.19588797 0.083333239
		 0.44588798 0.083333239 0.30411202 0.083333246 0.44588798 0.083333239 0.44588798 0.16666663
		 0.44588798 0.16666663 0.44588798 0.083333239 0.30411202 0.16666663 0.44588798 0.16666663;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 170 ".vt";
	setAttr ".vt[0:165]"  1.96036863 30.16566277 -17.20300865 1.96036839 30.16566277 -14.79699135
		 3.79699063 30.16566277 -12.96036911 6.20300913 30.16566277 -12.96036911 6.20300913 30.16566277 -19.03963089
		 3.79699135 30.16566277 -19.039632797 8.039631844 30.16566277 -14.79699135 8.03963089 30.16566277 -17.20300865
		 3.79699206 23 -17.58038712 6.20301008 23 -17.58038712 6.58038664 23 -17.20301056
		 6.58038759 23 -14.7969923 6.20300865 23 -14.41961288 3.79698992 23 -14.41961288 3.41961336 23 -14.79699039
		 3.41961312 23 -17.20300865 3.41961312 32.10040665 -17.20300865 3.41961336 32.10040665 -14.79699039
		 6.20300865 32.10040665 -14.41961288 3.79698992 32.10040665 -14.41961288 6.58038664 32.10040665 -17.20301056
		 6.58038759 32.10040665 -14.7969923 3.79699206 32.10040665 -17.58038712 6.20301008 32.10040665 -17.58038712
		 3.79699206 21.89959335 -17.58038712 6.20301008 21.89959335 -17.58038712 6.58038664 21.89959335 -17.20301056
		 6.58038759 21.89959335 -14.7969923 6.20300865 21.89959335 -14.41961288 3.79698992 21.89959335 -14.41961288
		 3.41961336 21.89959335 -14.79699039 3.41961312 21.89959335 -17.20300865 3.79699302 21.21913338 -18.92443085
		 6.20301056 21.21913338 -18.92442894 7.92442894 21.21913338 -17.20301056 7.92442989 21.21913338 -14.79699326
		 6.20300865 21.21913338 -13.075572968 3.79698992 21.21913338 -13.075572968 2.075572014 21.21913338 -14.79699039
		 2.075572014 21.21913338 -17.20300865 3.79699326 20.24882317 -18.92443085 6.20301056 20.24882317 -18.92442894
		 7.92442894 20.24882317 -17.20301056 7.92442989 20.24882317 -14.79699326 6.20300865 20.24882317 -13.075572968
		 3.79698992 20.24882317 -13.075572968 2.075572014 20.24882317 -14.79699039 2.075572014 20.24882317 -17.20300865
		 1.96036863 24.60251045 -14.79699135 3.79699063 24.60251045 -12.96036911 6.20300913 24.60251045 -12.96036911
		 8.039631844 24.60251045 -14.79699135 8.03963089 24.60251045 -17.20300865 6.20300913 24.60251045 -19.03963089
		 3.79699135 24.60251045 -19.039632797 1.96036863 24.60251045 -17.20300865 1.96036863 29.39748573 -14.79699135
		 3.79699063 29.39748573 -12.96036911 6.20300913 29.39748573 -12.96036911 8.039631844 29.39748573 -14.79699135
		 8.03963089 29.39748573 -17.20300865 6.20300913 29.39748573 -19.03963089 3.79699135 29.39748573 -19.039632797
		 1.96036863 29.39748573 -17.20300865 0 24.60251045 -17.20300865 0 24.60251045 -14.79699135
		 0 29.39748573 -14.79699135 0 29.39748573 -17.20300865 1.96036863 24.60251045 -13
		 1.96036863 29.39748573 -13 0 29.39748573 -13 0 24.60251045 -13 5.6016264 20.24882317 -17.20313072
		 6.2031312 20.24882317 -16.60162544 6.2031312 22.61545563 -16.60162544 5.6016264 22.61545563 -17.20313072
		 3.79687095 20.24882317 -16.60162544 4.39837551 20.24882317 -17.20313072 4.39837551 22.61545563 -17.20313072
		 3.79687095 22.61545563 -16.60162544 6.20313025 20.24882317 -15.39837551 5.60162592 20.24882317 -14.79687119
		 5.60162592 22.61545563 -14.79687119 6.20313025 22.61545563 -15.39837551 4.39837456 20.24882317 -14.79687023
		 3.79686999 20.24882317 -15.39837551 3.79686999 22.61545563 -15.39837551 4.39837456 22.61545563 -14.79687023
		 -1.96036863 30.16566277 -17.20300865 -1.96036816 30.16566277 -14.79699135 -3.79699039 30.16566277 -12.96036911
		 -6.20300865 30.16566277 -12.96036911 -6.20300865 30.16566277 -19.03963089 -3.79699135 30.16566277 -19.039632797
		 -8.039631844 30.16566277 -14.79699135 -8.03963089 30.16566277 -17.20300865 -3.7969923 23 -17.58038712
		 -6.20301056 23 -17.58038712 -6.58038712 23 -17.20301056 -6.58038712 23 -14.7969923
		 -6.20300865 23 -14.41961288 -3.79698944 23 -14.41961288 -3.41961288 23 -14.79699039
		 -3.41961288 23 -17.20300865 -3.41961288 32.10040665 -17.20300865 -3.41961288 32.10040665 -14.79699039
		 -6.20300865 32.10040665 -14.41961288 -3.79698944 32.10040665 -14.41961288 -6.58038712 32.10040665 -17.20301056
		 -6.58038712 32.10040665 -14.7969923 -3.7969923 32.10040665 -17.58038712 -6.20301056 32.10040665 -17.58038712
		 -3.7969923 21.89959335 -17.58038712 -6.20301056 21.89959335 -17.58038712 -6.58038712 21.89959335 -17.20301056
		 -6.58038712 21.89959335 -14.7969923 -6.20300865 21.89959335 -14.41961288 -3.79698944 21.89959335 -14.41961288
		 -3.41961288 21.89959335 -14.79699039 -3.41961288 21.89959335 -17.20300865 -3.79699326 21.21913338 -18.92443085
		 -6.20301056 21.21913338 -18.92442894 -7.92442894 21.21913338 -17.20301056 -7.92442989 21.21913338 -14.79699326
		 -6.20300865 21.21913338 -13.075572968 -3.79698944 21.21913338 -13.075572968 -2.075572014 21.21913338 -14.79699039
		 -2.075572014 21.21913338 -17.20300865 -3.79699326 20.24882317 -18.92443085 -6.20301056 20.24882317 -18.92442894
		 -7.92442894 20.24882317 -17.20301056 -7.92442989 20.24882317 -14.79699326 -6.20300865 20.24882317 -13.075572968
		 -3.79698944 20.24882317 -13.075572968 -2.075572014 20.24882317 -14.79699039 -2.075572014 20.24882317 -17.20300865
		 -1.96036863 24.60251045 -14.79699135 -3.79699039 24.60251045 -12.96036911 -6.20300865 24.60251045 -12.96036911
		 -8.039631844 24.60251045 -14.79699135 -8.03963089 24.60251045 -17.20300865 -6.20300865 24.60251045 -19.03963089
		 -3.79699135 24.60251045 -19.039632797 -1.96036863 24.60251045 -17.20300865 -1.96036863 29.39748573 -14.79699135
		 -3.79699039 29.39748573 -12.96036911 -6.20300865 29.39748573 -12.96036911 -8.039631844 29.39748573 -14.79699135
		 -8.03963089 29.39748573 -17.20300865 -6.20300865 29.39748573 -19.03963089 -3.79699135 29.39748573 -19.039632797
		 -1.96036863 29.39748573 -17.20300865 -1.96036863 24.60251045 -13 -1.96036863 29.39748573 -13
		 -5.6016264 20.24882317 -17.20313072 -6.20313072 20.24882317 -16.60162544 -6.20313072 22.61545563 -16.60162544
		 -5.6016264 22.61545563 -17.20313072 -3.79687119 20.24882317 -16.60162544 -4.39837551 20.24882317 -17.20313072
		 -4.39837551 22.61545563 -17.20313072 -3.79687119 22.61545563 -16.60162544 -6.20313072 20.24882317 -15.39837551
		 -5.60162544 20.24882317 -14.79687119 -5.60162544 22.61545563 -14.79687119 -6.20313072 22.61545563 -15.39837551;
	setAttr ".vt[166:169]" -4.39837456 20.24882317 -14.79687023 -3.79687023 20.24882317 -15.39837551
		 -3.79687023 22.61545563 -15.39837551 -4.39837456 22.61545563 -14.79687023;
	setAttr -s 328 ".ed";
	setAttr ".ed[0:165]"  16 22 0 17 19 0 17 16 0 18 21 0 19 18 0 21 20 0 23 20 0
		 23 22 0 15 14 0 14 48 0 1 0 0 0 63 0 1 17 0 13 12 0 12 50 0 3 2 0 2 57 0 3 18 0 4 5 0
		 5 22 0 4 61 0 9 8 0 8 54 0 6 7 0 7 20 0 6 59 0 11 10 0 10 52 0 16 0 0 19 2 0 21 6 0
		 23 4 0 9 25 0 25 24 0 24 8 0 11 27 0 27 26 0 26 10 0 13 29 0 29 28 0 28 12 0 15 31 0
		 31 30 0 30 14 0 25 33 1 33 32 0 32 24 1 27 35 1 35 34 0 34 26 1 29 37 1 37 36 0 36 28 1
		 31 39 1 39 38 0 38 30 1 33 41 0 41 40 0 40 32 0 35 43 0 43 42 0 42 34 0 37 45 0 45 44 0
		 44 36 0 39 47 0 47 46 0 46 38 0 41 72 1 43 80 1 45 84 1 47 76 1 2 1 0 4 7 0 6 3 0
		 0 5 0 10 9 0 12 11 0 14 13 0 8 15 0 26 25 0 28 27 0 30 29 0 24 31 0 34 33 0 36 35 0
		 38 37 0 32 39 0 42 41 0 44 43 0 46 45 0 40 47 0 48 56 0 49 13 0 50 58 0 51 11 0 52 60 0
		 53 9 0 54 62 0 55 15 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 48 0
		 56 1 0 57 49 0 58 3 0 59 51 0 60 7 0 61 53 0 62 5 0 63 55 0 56 57 1 57 58 1 58 59 1
		 59 60 1 60 61 1 61 62 1 62 63 1 63 56 0 55 64 0 48 65 1 64 65 1 56 66 1 63 67 0 67 66 1
		 67 64 1 48 68 0 56 69 0 68 69 0 66 70 1 69 70 0 65 71 1 71 70 1 68 71 0 72 77 0 73 42 1
		 74 83 0 75 78 0 75 74 0 76 85 0 77 40 1 78 79 0 80 73 0 81 44 1 82 87 0 83 82 0 84 81 0
		 85 46 1 86 79 0 87 86 0 72 75 0 74 73 0 76 79 0 78 77 0 80 83 0 82 81 0 84 87 0 86 85 0
		 72 73 0 76 77 0 80 81 0;
	setAttr ".ed[166:327]" 84 85 0 105 104 0 105 107 0 107 106 0 106 109 0 109 108 0
		 111 108 0 111 110 0 104 110 0 103 102 0 102 136 0 143 136 0 143 103 0 151 144 0 144 89 0
		 89 88 0 88 151 0 89 105 0 104 88 0 101 100 0 100 138 0 137 138 0 137 101 0 145 146 1
		 146 91 0 91 90 0 90 145 0 91 106 0 107 90 0 92 93 0 93 110 0 111 92 0 92 149 0 149 150 1
		 150 93 0 94 95 0 95 108 0 109 94 0 94 147 0 147 148 1 148 95 0 97 96 0 97 113 0 113 112 0
		 112 96 0 99 98 0 99 115 0 115 114 0 114 98 0 101 117 0 117 116 0 116 100 0 103 119 0
		 119 118 0 118 102 0 113 121 1 121 120 0 120 112 1 115 123 1 123 122 0 122 114 1 117 125 1
		 125 124 0 124 116 1 119 127 1 127 126 0 126 118 1 121 129 0 129 128 0 128 120 0 123 131 0
		 131 130 0 130 122 0 125 133 0 133 132 0 132 124 0 127 135 0 135 134 0 134 126 0 129 154 1
		 154 159 0 159 128 1 131 162 1 162 155 0 155 130 1 133 166 1 166 163 0 163 132 1 135 158 1
		 158 167 0 167 134 1 160 159 0 154 157 0 157 160 0 156 155 0 162 165 0 156 165 0 164 163 0
		 166 169 0 164 169 0 168 167 0 158 161 0 168 161 0 144 145 1 90 89 0 92 95 0 148 149 1
		 146 147 1 94 91 0 150 151 1 88 93 0 98 97 0 114 113 0 100 99 0 116 115 0 102 101 0
		 118 117 0 96 103 0 112 119 0 122 121 0 124 123 0 126 125 0 120 127 0 130 129 0 132 131 0
		 134 133 0 128 135 0 154 155 0 162 163 0 166 167 0 158 159 0 136 137 0 139 99 0 138 139 0
		 139 140 0 98 140 0 141 97 0 140 141 0 141 142 0 96 142 0 142 143 0 145 137 0 136 144 0
		 138 146 0 147 139 0 140 148 0 149 141 0 142 150 0 151 143 0 136 65 1 143 64 0 152 153 0
		 153 70 0 152 71 0 151 67 0 144 66 1 144 153 0 136 152 0 160 161 0 157 156 0 165 164 0
		 169 168 0;
	setAttr -s 160 -ch 656 ".fc[0:159]" -type "polyFaces" 
		f 8 -3 1 4 3 5 -7 7 -1
		mu 0 8 6 24 7 25 8 26 9 27
		f 4 8 9 -108 99
		mu 0 4 34 22 48 57
		f 4 123 108 10 11
		mu 0 4 67 58 15 13
		f 4 -11 12 2 28
		mu 0 4 0 15 24 6
		f 4 13 14 -102 93
		mu 0 4 32 21 50 49
		f 4 117 110 15 16
		mu 0 4 59 60 1 10
		f 4 -16 17 -5 29
		mu 0 4 10 1 25 7
		f 4 18 19 -8 31
		mu 0 4 11 2 27 9
		f 4 -19 20 121 114
		mu 0 4 2 11 64 65
		f 4 23 24 -6 30
		mu 0 4 3 4 26 8
		f 4 -24 25 119 112
		mu 0 4 17 5 61 63
		f 4 -22 32 33 34
		mu 0 4 23 28 36 35
		f 4 -27 35 36 37
		mu 0 4 20 30 38 29
		f 4 -14 38 39 40
		mu 0 4 21 32 40 31
		f 4 -9 41 42 43
		mu 0 4 22 34 42 33
		f 4 -34 44 45 46
		mu 0 4 35 36 44 43
		f 4 -37 47 48 49
		mu 0 4 29 38 45 37
		f 4 -40 50 51 52
		mu 0 4 31 40 46 39
		f 4 -43 53 54 55
		mu 0 4 33 42 47 41
		f 4 -46 56 57 58
		mu 0 4 43 44 76 79
		f 4 -49 59 60 61
		mu 0 4 37 45 80 77
		f 4 -52 62 63 64
		mu 0 4 39 46 82 81
		f 4 -55 65 66 67
		mu 0 4 41 47 78 83
		f 4 -58 68 139 145
		mu 0 4 79 76 88 97
		f 4 -61 69 147 140
		mu 0 4 77 80 92 96
		f 4 -64 70 151 148
		mu 0 4 81 82 94 98
		f 4 -67 71 144 152
		mu 0 4 83 78 90 99
		f 4 158 -140 155 142
		mu 0 4 84 97 88 89
		f 4 156 -148 159 -142
		mu 0 4 85 96 92 93
		f 4 160 -152 161 -150
		mu 0 4 86 98 94 95
		f 4 162 -145 157 -154
		mu 0 4 87 99 90 91
		f 4 116 -17 72 -109
		mu 0 4 58 59 10 15
		f 4 73 -113 120 -21
		mu 0 4 11 17 63 64
		f 4 118 -26 74 -111
		mu 0 4 60 62 12 1
		f 4 122 -12 75 -115
		mu 0 4 66 67 13 14
		f 4 -13 -73 -30 -2
		mu 0 4 24 15 10 7
		f 4 -18 -75 -31 -4
		mu 0 4 25 16 3 8
		f 4 -25 -74 -32 6
		mu 0 4 26 17 11 9
		f 4 -20 -76 -29 0
		mu 0 4 27 18 19 6
		f 4 -77 -38 80 -33
		mu 0 4 28 20 29 36
		f 4 -78 -41 81 -36
		mu 0 4 30 21 31 38
		f 4 -79 -44 82 -39
		mu 0 4 32 22 33 40
		f 4 -80 -35 83 -42
		mu 0 4 34 23 35 42
		f 4 -81 -50 84 -45
		mu 0 4 36 29 37 44
		f 4 -82 -53 85 -48
		mu 0 4 38 31 39 45
		f 4 -83 -56 86 -51
		mu 0 4 40 33 41 46
		f 4 -84 -47 87 -54
		mu 0 4 42 35 43 47
		f 4 -85 -62 88 -57
		mu 0 4 44 37 77 76
		f 4 -86 -65 89 -60
		mu 0 4 45 39 81 80
		f 4 -87 -68 90 -63
		mu 0 4 46 41 83 82
		f 4 -88 -59 91 -66
		mu 0 4 47 43 79 78
		f 4 -69 -89 -141 -164
		mu 0 4 88 76 77 96
		f 4 -70 -90 -149 -166
		mu 0 4 92 80 81 98
		f 4 -71 -91 -153 -167
		mu 0 4 94 82 83 99
		f 4 -72 -92 -146 -165
		mu 0 4 90 78 79 97
		f 4 -94 -101 -10 78
		mu 0 4 32 49 48 22
		f 4 -96 -103 -15 77
		mu 0 4 30 52 50 21
		f 4 -104 95 26 27
		mu 0 4 53 51 30 20
		f 4 -98 -105 -28 76
		mu 0 4 28 54 53 20
		f 4 -106 97 21 22
		mu 0 4 55 54 28 23
		f 4 -100 -107 -23 79
		mu 0 4 34 57 56 23
		f 4 100 -110 -117 -93
		mu 0 4 48 49 59 58
		f 4 101 94 -118 109
		mu 0 4 49 50 60 59
		f 4 102 -112 -119 -95
		mu 0 4 50 52 62 60
		f 4 -120 111 103 96
		mu 0 4 63 61 51 53
		f 4 -121 -97 104 -114
		mu 0 4 64 63 53 54
		f 4 -122 113 105 98
		mu 0 4 65 64 54 55
		f 4 106 -116 -123 -99
		mu 0 4 56 57 67 66
		f 4 107 125 -127 -125
		mu 0 4 57 48 69 68
		f 4 133 135 -138 -139
		mu 0 4 72 73 74 75
		f 4 -124 128 129 -128
		mu 0 4 58 67 71 70
		f 4 115 124 -131 -129
		mu 0 4 67 57 68 71
		f 4 92 132 -134 -132
		mu 0 4 48 58 73 72
		f 4 127 134 -136 -133
		mu 0 4 58 70 74 73
		f 4 -126 131 138 -137
		mu 0 4 69 48 72 75
		f 8 -147 -143 143 141 150 149 154 153
		mu 0 8 91 84 89 85 93 86 95 87
		f 4 163 -157 -144 -156
		mu 0 4 88 96 85 89
		f 4 164 -159 146 -158
		mu 0 4 90 97 84 91
		f 4 165 -161 -151 -160
		mu 0 4 92 98 86 93
		f 4 166 -163 -155 -162
		mu 0 4 94 99 87 95
		f 8 174 -174 172 -172 -171 -170 -169 167
		mu 0 8 100 107 106 105 104 103 102 101
		f 4 -179 177 -177 -176
		mu 0 4 108 111 110 109
		f 4 -183 -182 -181 -180
		mu 0 4 112 115 114 113
		f 4 -185 -168 -184 181
		mu 0 4 116 100 101 114
		f 4 -189 187 -187 -186
		mu 0 4 117 120 119 118
		f 4 -193 -192 -191 -190
		mu 0 4 121 124 123 122
		f 4 -195 169 -194 191
		mu 0 4 124 102 103 123
		f 4 -198 173 -197 -196
		mu 0 4 125 106 107 126
		f 4 -201 -200 -199 195
		mu 0 4 126 128 127 125
		f 4 -204 171 -203 -202
		mu 0 4 129 104 105 130
		f 4 -207 -206 -205 201
		mu 0 4 131 134 133 132
		f 4 -211 -210 -209 207
		mu 0 4 135 138 137 136
		f 4 -215 -214 -213 211
		mu 0 4 139 142 141 140
		f 4 -218 -217 -216 185
		mu 0 4 118 144 143 117
		f 4 -221 -220 -219 175
		mu 0 4 109 146 145 108
		f 4 -224 -223 -222 209
		mu 0 4 138 148 147 137
		f 4 -227 -226 -225 213
		mu 0 4 142 150 149 141
		f 4 -230 -229 -228 216
		mu 0 4 144 152 151 143
		f 4 -233 -232 -231 219
		mu 0 4 146 154 153 145
		f 4 -236 -235 -234 222
		mu 0 4 148 156 155 147
		f 4 -239 -238 -237 225
		mu 0 4 150 158 157 149
		f 4 -242 -241 -240 228
		mu 0 4 152 160 159 151
		f 4 -245 -244 -243 231
		mu 0 4 154 162 161 153
		f 4 -248 -247 -246 234
		mu 0 4 156 164 163 155
		f 4 -251 -250 -249 237
		mu 0 4 158 166 165 157
		f 4 -254 -253 -252 240
		mu 0 4 160 168 167 159
		f 4 -257 -256 -255 243
		mu 0 4 162 170 169 161
		f 4 -260 -259 246 -258
		mu 0 4 171 172 163 164
		f 4 262 -262 249 -261
		mu 0 4 173 174 165 166
		f 4 265 -265 252 -264
		mu 0 4 175 176 167 168
		f 4 268 -268 255 -267
		mu 0 4 177 178 169 170
		f 4 180 -271 192 -270
		mu 0 4 113 114 124 121
		f 4 198 -273 206 -272
		mu 0 4 125 127 134 131
		f 4 190 -275 204 -274
		mu 0 4 122 123 180 179
		f 4 200 -277 182 -276
		mu 0 4 181 182 115 112
		f 4 168 194 270 183
		mu 0 4 101 102 124 114
		f 4 170 203 274 193
		mu 0 4 103 104 129 183
		f 4 -173 197 271 202
		mu 0 4 105 106 125 131
		f 4 -175 184 276 196
		mu 0 4 107 100 185 184
		f 4 208 -279 214 277
		mu 0 4 136 137 142 139
		f 4 212 -281 217 279
		mu 0 4 140 141 144 118
		f 4 215 -283 220 281
		mu 0 4 117 143 146 109
		f 4 218 -285 210 283
		mu 0 4 108 145 138 135
		f 4 221 -286 226 278
		mu 0 4 137 147 150 142
		f 4 224 -287 229 280
		mu 0 4 141 149 152 144
		f 4 227 -288 232 282
		mu 0 4 143 151 154 146
		f 4 230 -289 223 284
		mu 0 4 145 153 148 138
		f 4 233 -290 238 285
		mu 0 4 147 155 158 150
		f 4 236 -291 241 286
		mu 0 4 149 157 160 152
		f 4 239 -292 244 287
		mu 0 4 151 159 162 154
		f 4 242 -293 235 288
		mu 0 4 153 161 156 148
		f 4 293 250 289 245
		mu 0 4 163 166 158 155
		f 4 294 253 290 248
		mu 0 4 165 168 160 157
		f 4 295 256 291 251
		mu 0 4 167 170 162 159
		f 4 296 247 292 254
		mu 0 4 169 164 156 161
		f 4 -282 176 297 188
		mu 0 4 117 109 110 120
		f 4 -280 186 299 298
		mu 0 4 140 118 119 186
		f 4 -302 -212 -299 300
		mu 0 4 187 139 140 188
		f 4 -278 301 303 302
		mu 0 4 136 139 187 189
		f 4 -306 -208 -303 304
		mu 0 4 190 135 136 189
		f 4 -284 305 306 178
		mu 0 4 108 135 191 111
		f 4 308 269 307 -298
		mu 0 4 110 113 121 120
		f 4 -308 189 -310 -188
		mu 0 4 120 121 122 119
		f 4 309 273 310 -300
		mu 0 4 119 122 179 186
		f 4 -312 -301 -311 205
		mu 0 4 134 187 188 133
		f 4 312 -304 311 272
		mu 0 4 127 189 187 134
		f 4 -314 -305 -313 199
		mu 0 4 128 190 189 127
		f 4 313 275 314 -307
		mu 0 4 191 181 112 111
		f 4 316 126 -316 -178
		mu 0 4 111 193 192 110
		f 4 319 137 -319 -318
		mu 0 4 194 197 196 195
		f 4 321 -130 -321 179
		mu 0 4 113 199 198 112
		f 4 320 130 -317 -315
		mu 0 4 112 198 193 111
		f 4 323 317 -323 -309
		mu 0 4 110 194 195 113
		f 4 322 318 -135 -322
		mu 0 4 113 195 196 199
		f 4 136 -320 -324 315
		mu 0 4 192 197 194 110
		f 8 -269 -328 -266 -327 -263 -326 259 324
		mu 0 8 178 177 176 175 174 173 172 171
		f 4 258 325 260 -294
		mu 0 4 163 172 173 166
		f 4 267 -325 257 -297
		mu 0 4 169 178 171 164
		f 4 261 326 263 -295
		mu 0 4 165 174 175 168
		f 4 264 327 266 -296
		mu 0 4 167 176 177 170;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape2" -p "Falcon_Back";
	rename -uid "E4B7BFAA-4012-953A-8F88-38A6F4D5C337";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.59374998509883881 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.30411202 0.083333246
		 0.30411202 0.16666663 0.44588798 0.083333239 0.44588798 0.16666663 0.5 0.5 0.5 0.5
		 0.5 0.5 0.375 0.37500003 0.375 0.375 0.24999997 0.25 0.375 0.375 0.625 0.37500003
		 0.625 0.375 0.75 0.25 0.625 0.375 0.5 0.25 0.5 0.25 0.49999997 0.25 0.5 0.54166663
		 0.6875 0.47916663 0.5 0.70833337 0.3125 0.4791666 0.5 0.25 0.625 0.37500003 0.5 0.5
		 0.375 0.37500003 0.3125 0.4791666 0.5 0.54166663 0.24999997 0.25 0.49999997 0.25
		 0.375 0.375 0.375 0.375 0.5 0.25 0.6875 0.47916663 0.75 0.25 0.625 0.375 0.625 0.375
		 0.5 0.5 0.5 0.5 0.5 0.70833337 0.30411202 0.083333246 0.44588798 0.083333239 0.44588798
		 0.16666663 0.30411202 0.16666663;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 40 ".vt[0:39]"  3 21 -18 3 30 -18 3 21 -13 3 30 -13 8.076147079 32.10040665 -18.076147079
		 8.076145172 31 -18.076147079 9.03963089 31 -19.03963089 3.9238534 32.10040665 -18.076147079
		 3.9238534 31 -18.076147079 2.96036839 31 -19.039632797 8.076147079 32.10040665 -13.92385483
		 8.076145172 31 -13.92385387 9.039631844 31 -12.96036911 3.92385387 32.10040665 -13.92385197
		 3.9238534 31 -13.92385387 2.96036816 31 -12.96036911 2.96036863 23 -13 9.03963089 23 -13
		 9.03963089 23 -19.03963089 2.96036839 23 -19.03963089 -3 21 -18 -3 30 -18 -3 21 -13
		 -3 30 -13 -8.076147079 32.10040665 -18.076147079 -8.076145172 31 -18.076147079 -9.03963089 31 -19.03963089
		 -3.9238534 32.10040665 -18.076147079 -3.9238534 31 -18.076147079 -2.96036839 31 -19.039632797
		 -8.076147079 32.10040665 -13.92385483 -8.076145172 31 -13.92385387 -9.039631844 31 -12.96036911
		 -3.92385387 32.10040665 -13.92385197 -3.9238534 31 -13.92385387 -2.96036816 31 -12.96036911
		 -2.96036863 23 -13 -9.03963089 23 -13 -9.03963089 23 -19.03963089 -2.96036839 23 -19.03963089;
	setAttr -s 68 ".ed[0:67]"  13 7 0 1 0 0 0 2 0 1 3 0 2 3 0 5 11 0 6 12 0
		 4 5 0 5 6 0 6 18 0 7 4 0 8 5 0 9 6 0 7 8 0 8 9 0 9 19 0 10 4 0 11 14 0 12 15 0 10 11 0
		 11 12 0 12 17 0 13 10 0 14 8 0 15 9 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0
		 19 16 0 33 30 0 30 24 0 27 24 0 33 27 0 39 36 0 35 29 0 28 29 0 34 35 0 34 28 0 36 37 0
		 32 35 0 31 32 0 31 34 0 29 26 0 28 25 0 25 26 0 26 32 0 25 31 0 37 38 0 38 39 0 35 36 0
		 32 37 0 26 38 0 29 39 0 20 0 0 20 22 0 22 2 0 22 23 0 23 3 0 21 20 0 21 1 0 21 23 0
		 27 28 0 33 34 0 24 25 0 30 31 0;
	setAttr -s 32 -ch 128 ".fc[0:31]" -type "polyFaces" 
		f 4 22 16 -11 -1
		mu 0 4 15 11 4 7
		f 4 24 15 31 -28
		mu 0 4 17 9 21 18
		f 4 14 -25 -27 23
		mu 0 4 8 10 17 16
		f 4 -22 18 27 28
		mu 0 4 19 13 17 18
		f 4 26 -19 -21 17
		mu 0 4 16 17 14 12
		f 4 -13 -15 11 8
		mu 0 4 6 10 8 5
		f 4 12 9 30 -16
		mu 0 4 9 6 20 21
		f 4 -7 -9 5 20
		mu 0 4 14 6 5 12
		f 4 -10 6 21 29
		mu 0 4 20 6 13 19
		f 4 3 -5 -3 -2
		mu 0 4 1 3 2 0
		f 4 -4 -63 63 60
		mu 0 4 3 1 43 42
		f 4 0 13 -24 -26
		mu 0 4 15 7 8 16
		f 4 -12 -14 10 7
		mu 0 4 5 8 7 4
		f 4 -6 -8 -17 19
		mu 0 4 12 5 4 11
		f 4 -23 25 -18 -20
		mu 0 4 11 15 16 12
		f 4 35 34 -34 -33
		mu 0 4 22 25 24 23
		f 4 52 -37 -56 -38
		mu 0 4 29 27 26 28
		f 4 -41 39 37 -39
		mu 0 4 30 32 29 31
		f 4 53 -42 -53 -43
		mu 0 4 34 33 27 29
		f 4 -45 43 42 -40
		mu 0 4 32 36 35 29
		f 4 -48 -47 38 45
		mu 0 4 37 38 30 31
		f 4 -44 -50 47 48
		mu 0 4 35 36 38 37
		f 4 -51 -54 -49 54
		mu 0 4 39 33 34 37
		f 4 -52 -55 -46 55
		mu 0 4 26 39 37 28
		f 4 -59 -58 56 2
		mu 0 4 2 41 40 0
		f 4 -61 -60 58 4
		mu 0 4 3 42 41 2
		f 4 -57 -62 62 1
		mu 0 4 0 40 43 1
		f 4 61 57 59 -64
		mu 0 4 43 40 41 42
		f 4 65 40 -65 -36
		mu 0 4 22 32 30 25
		f 4 -67 -35 64 46
		mu 0 4 38 24 25 30
		f 4 -68 33 66 49
		mu 0 4 36 23 24 38
		f 4 67 44 -66 32
		mu 0 4 23 36 32 22;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cubic_Back";
	rename -uid "A5C062F2-4665-32A1-61D0-AF9E4CAC5F57";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 25.550203323364258 -16.000000953674316 ;
	setAttr ".sp" -type "double3" 0 25.550203323364258 -16.000000953674316 ;
createNode mesh -n "Cubic_BackShape" -p "Cubic_Back";
	rename -uid "DBE2276D-431C-0BBB-261B-3E85399B2589";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.54166661947965622 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_BackShapeOrig" -p "Cubic_Back";
	rename -uid "3BBDF75F-4BA2-996A-51BA-41AD7CF2B214";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 200 ".uvst[0].uvsp[0:199]" -type "float2" 0.375 0.25 0.69588798
		 0.25 0.375 0.5 0.625 0.42911205 0.625 0.5 0.625 0.5 0.375 0.35805225 0.51694769 0.25
		 0.625 0.39194781 0.48305234 0.5 0.55411196 0.25 0.44588798 0.5 0.80411208 0.25 0.30411199
		 0.25 0.19588797 0.25 0.44588798 0.25 0.625 0.32088798 0.55411202 0.5 0.375 0.42911205
		 0.375 0.32088798 0.51694775 0.75 0.625 0.89194775 0.48305228 1 0.375 0.85805219 0.48305231
		 0.25 0.625 0.35805225 0.51694769 0.5 0.375 0.39194781 0.48305231 0.75 0.51694769
		 0.75 0.625 0.85805219 0.625 0.89194775 0.51694769 1 0.48305243 1 0.375 0.89194769
		 0.375 0.85805219 0.48305231 0.75 0.55213338 0.75 0.625 0.85805213 0.625 0.92713344
		 0.51694775 1 0.44786662 1 0.375 0.89194775 0.375 0.82286656 0.44786662 0.75 0.625
		 0.82286656 0.55213338 1 0.375 0.92713344 0.44588798 0.083333239 0.55411202 0.083333246
		 0.69588792 0.083333239 0.625 0.66666675 0.80411208 0.083333246 0.55411196 0.66666675
		 0.44588798 0.66666675 0.375 0.66666675 0.19588797 0.083333239 0.30411202 0.083333246
		 0.44588798 0.16666663 0.55411196 0.16666663 0.69588792 0.16666663 0.625 0.58333337
		 0.80411208 0.16666663 0.55411196 0.58333337 0.44588798 0.58333337 0.375 0.58333337
		 0.19588797 0.16666663 0.30411202 0.16666663 0.30411202 0.083333246 0.44588798 0.083333239
		 0.44588798 0.16666663 0.30411202 0.16666663 0.44588798 0.083333239 0.44588798 0.16666663
		 0.44588798 0.16666663 0.44588798 0.083333239 0.44786659 0.75 0.55213338 0.75 0.375
		 0.92713344 0.375 0.82286656 0.625 0.82286656 0.625 0.92713344 0.55213338 1 0.44786662
		 1 0.40624684 0.84375316 0.5312469 0.7812469 0.59375316 0.90624678 0.4687531 0.9687531
		 0.46875313 0.7812469 0.46875313 0.7812469 0.40624687 0.9062469 0.4062469 0.9062469
		 0.59375316 0.84375316 0.59375316 0.8437531 0.53124684 0.9687531 0.53124684 0.9687531
		 0.5312469 0.7812469 0.40624684 0.84375316 0.59375316 0.90624684 0.46875307 0.9687531
		 0.375 0.35805225 0.48305231 0.25 0.51694769 0.25 0.625 0.35805225 0.625 0.39194781
		 0.51694769 0.5 0.48305234 0.5 0.375 0.39194781 0.375 0.89194769 0.48305228 1 0.44588798
		 0.083333239 0.30411202 0.083333246 0.30411202 0.16666663 0.44588798 0.16666663 0.44588798
		 0.25 0.30411199 0.25 0.375 0.25 0.51694769 1 0.625 0.89194775 0.69588792 0.083333239
		 0.55411202 0.083333246 0.55411196 0.16666663 0.69588792 0.16666663 0.69588798 0.25
		 0.55411196 0.25 0.44588798 0.5 0.375 0.5 0.44588798 0.58333337 0.375 0.58333337 0.625
		 0.42911205 0.625 0.5 0.55411202 0.5 0.625 0.5 0.625 0.58333337 0.55411196 0.58333337
		 0.375 0.85805219 0.48305231 0.75 0.48305231 0.75 0.375 0.85805219 0.51694775 0.75
		 0.625 0.85805219 0.625 0.85805213 0.51694769 0.75 0.51694775 1 0.625 0.89194775 0.375
		 0.89194775 0.48305243 1 0.44786662 0.75 0.375 0.82286656 0.625 0.82286656 0.55213338
		 0.75 0.55213338 1 0.625 0.92713344 0.375 0.92713344 0.44786662 1 0.44786659 0.75
		 0.375 0.82286656 0.625 0.82286656 0.55213338 0.75 0.55213338 1 0.625 0.92713344 0.375
		 0.92713344 0.44786662 1 0.46875313 0.7812469 0.40624684 0.84375316 0.59375316 0.84375316
		 0.5312469 0.7812469 0.53124684 0.9687531 0.59375316 0.90624684 0.40624687 0.9062469
		 0.46875307 0.9687531 0.40624684 0.84375316 0.46875313 0.7812469 0.5312469 0.7812469
		 0.59375316 0.8437531 0.59375316 0.90624678 0.53124684 0.9687531 0.4687531 0.9687531
		 0.4062469 0.9062469 0.80411208 0.16666663 0.80411208 0.25 0.19588797 0.16666663 0.19588797
		 0.25 0.625 0.32088798 0.375 0.42911205 0.375 0.32088798 0.80411208 0.083333246 0.55411196
		 0.66666675 0.625 0.66666675 0.44588798 0.66666675 0.375 0.66666675 0.19588797 0.083333239
		 0.44588798 0.083333239 0.30411202 0.083333246 0.44588798 0.083333239 0.44588798 0.16666663
		 0.44588798 0.16666663 0.44588798 0.083333239 0.30411202 0.16666663 0.44588798 0.16666663;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 170 ".vt";
	setAttr ".vt[0:165]"  1.96036863 30.16566277 -17.20300865 1.96036839 30.16566277 -14.79699135
		 3.79699063 30.16566277 -12.96036911 6.20300913 30.16566277 -12.96036911 6.20300913 30.16566277 -19.03963089
		 3.79699135 30.16566277 -19.039632797 8.039631844 30.16566277 -14.79699135 8.03963089 30.16566277 -17.20300865
		 3.79699206 23 -17.58038712 6.20301008 23 -17.58038712 6.58038664 23 -17.20301056
		 6.58038759 23 -14.7969923 6.20300865 23 -14.41961288 3.79698992 23 -14.41961288 3.41961336 23 -14.79699039
		 3.41961312 23 -17.20300865 3.41961312 32.10040665 -17.20300865 3.41961336 32.10040665 -14.79699039
		 6.20300865 32.10040665 -14.41961288 3.79698992 32.10040665 -14.41961288 6.58038664 32.10040665 -17.20301056
		 6.58038759 32.10040665 -14.7969923 3.79699206 32.10040665 -17.58038712 6.20301008 32.10040665 -17.58038712
		 3.79699206 21.89959335 -17.58038712 6.20301008 21.89959335 -17.58038712 6.58038664 21.89959335 -17.20301056
		 6.58038759 21.89959335 -14.7969923 6.20300865 21.89959335 -14.41961288 3.79698992 21.89959335 -14.41961288
		 3.41961336 21.89959335 -14.79699039 3.41961312 21.89959335 -17.20300865 3.79699302 21.21913338 -18.92443085
		 6.20301056 21.21913338 -18.92442894 7.92442894 21.21913338 -17.20301056 7.92442989 21.21913338 -14.79699326
		 6.20300865 21.21913338 -13.075572968 3.79698992 21.21913338 -13.075572968 2.075572014 21.21913338 -14.79699039
		 2.075572014 21.21913338 -17.20300865 3.79699326 20.24882317 -18.92443085 6.20301056 20.24882317 -18.92442894
		 7.92442894 20.24882317 -17.20301056 7.92442989 20.24882317 -14.79699326 6.20300865 20.24882317 -13.075572968
		 3.79698992 20.24882317 -13.075572968 2.075572014 20.24882317 -14.79699039 2.075572014 20.24882317 -17.20300865
		 1.96036863 24.60251045 -14.79699135 3.79699063 24.60251045 -12.96036911 6.20300913 24.60251045 -12.96036911
		 8.039631844 24.60251045 -14.79699135 8.03963089 24.60251045 -17.20300865 6.20300913 24.60251045 -19.03963089
		 3.79699135 24.60251045 -19.039632797 1.96036863 24.60251045 -17.20300865 1.96036863 29.39748573 -14.79699135
		 3.79699063 29.39748573 -12.96036911 6.20300913 29.39748573 -12.96036911 8.039631844 29.39748573 -14.79699135
		 8.03963089 29.39748573 -17.20300865 6.20300913 29.39748573 -19.03963089 3.79699135 29.39748573 -19.039632797
		 1.96036863 29.39748573 -17.20300865 0 24.60251045 -17.20300865 0 24.60251045 -14.79699135
		 0 29.39748573 -14.79699135 0 29.39748573 -17.20300865 1.96036863 24.60251045 -13
		 1.96036863 29.39748573 -13 0 29.39748573 -13 0 24.60251045 -13 5.6016264 20.24882317 -17.20313072
		 6.2031312 20.24882317 -16.60162544 6.2031312 22.61545563 -16.60162544 5.6016264 22.61545563 -17.20313072
		 3.79687095 20.24882317 -16.60162544 4.39837551 20.24882317 -17.20313072 4.39837551 22.61545563 -17.20313072
		 3.79687095 22.61545563 -16.60162544 6.20313025 20.24882317 -15.39837551 5.60162592 20.24882317 -14.79687119
		 5.60162592 22.61545563 -14.79687119 6.20313025 22.61545563 -15.39837551 4.39837456 20.24882317 -14.79687023
		 3.79686999 20.24882317 -15.39837551 3.79686999 22.61545563 -15.39837551 4.39837456 22.61545563 -14.79687023
		 -1.96036863 30.16566277 -17.20300865 -1.96036816 30.16566277 -14.79699135 -3.79699039 30.16566277 -12.96036911
		 -6.20300865 30.16566277 -12.96036911 -6.20300865 30.16566277 -19.03963089 -3.79699135 30.16566277 -19.039632797
		 -8.039631844 30.16566277 -14.79699135 -8.03963089 30.16566277 -17.20300865 -3.7969923 23 -17.58038712
		 -6.20301056 23 -17.58038712 -6.58038712 23 -17.20301056 -6.58038712 23 -14.7969923
		 -6.20300865 23 -14.41961288 -3.79698944 23 -14.41961288 -3.41961288 23 -14.79699039
		 -3.41961288 23 -17.20300865 -3.41961288 32.10040665 -17.20300865 -3.41961288 32.10040665 -14.79699039
		 -6.20300865 32.10040665 -14.41961288 -3.79698944 32.10040665 -14.41961288 -6.58038712 32.10040665 -17.20301056
		 -6.58038712 32.10040665 -14.7969923 -3.7969923 32.10040665 -17.58038712 -6.20301056 32.10040665 -17.58038712
		 -3.7969923 21.89959335 -17.58038712 -6.20301056 21.89959335 -17.58038712 -6.58038712 21.89959335 -17.20301056
		 -6.58038712 21.89959335 -14.7969923 -6.20300865 21.89959335 -14.41961288 -3.79698944 21.89959335 -14.41961288
		 -3.41961288 21.89959335 -14.79699039 -3.41961288 21.89959335 -17.20300865 -3.79699326 21.21913338 -18.92443085
		 -6.20301056 21.21913338 -18.92442894 -7.92442894 21.21913338 -17.20301056 -7.92442989 21.21913338 -14.79699326
		 -6.20300865 21.21913338 -13.075572968 -3.79698944 21.21913338 -13.075572968 -2.075572014 21.21913338 -14.79699039
		 -2.075572014 21.21913338 -17.20300865 -3.79699326 20.24882317 -18.92443085 -6.20301056 20.24882317 -18.92442894
		 -7.92442894 20.24882317 -17.20301056 -7.92442989 20.24882317 -14.79699326 -6.20300865 20.24882317 -13.075572968
		 -3.79698944 20.24882317 -13.075572968 -2.075572014 20.24882317 -14.79699039 -2.075572014 20.24882317 -17.20300865
		 -1.96036863 24.60251045 -14.79699135 -3.79699039 24.60251045 -12.96036911 -6.20300865 24.60251045 -12.96036911
		 -8.039631844 24.60251045 -14.79699135 -8.03963089 24.60251045 -17.20300865 -6.20300865 24.60251045 -19.03963089
		 -3.79699135 24.60251045 -19.039632797 -1.96036863 24.60251045 -17.20300865 -1.96036863 29.39748573 -14.79699135
		 -3.79699039 29.39748573 -12.96036911 -6.20300865 29.39748573 -12.96036911 -8.039631844 29.39748573 -14.79699135
		 -8.03963089 29.39748573 -17.20300865 -6.20300865 29.39748573 -19.03963089 -3.79699135 29.39748573 -19.039632797
		 -1.96036863 29.39748573 -17.20300865 -1.96036863 24.60251045 -13 -1.96036863 29.39748573 -13
		 -5.6016264 20.24882317 -17.20313072 -6.20313072 20.24882317 -16.60162544 -6.20313072 22.61545563 -16.60162544
		 -5.6016264 22.61545563 -17.20313072 -3.79687119 20.24882317 -16.60162544 -4.39837551 20.24882317 -17.20313072
		 -4.39837551 22.61545563 -17.20313072 -3.79687119 22.61545563 -16.60162544 -6.20313072 20.24882317 -15.39837551
		 -5.60162544 20.24882317 -14.79687119 -5.60162544 22.61545563 -14.79687119 -6.20313072 22.61545563 -15.39837551;
	setAttr ".vt[166:169]" -4.39837456 20.24882317 -14.79687023 -3.79687023 20.24882317 -15.39837551
		 -3.79687023 22.61545563 -15.39837551 -4.39837456 22.61545563 -14.79687023;
	setAttr -s 328 ".ed";
	setAttr ".ed[0:165]"  16 22 0 17 19 0 17 16 0 18 21 0 19 18 0 21 20 0 23 20 0
		 23 22 0 15 14 0 14 48 0 1 0 0 0 63 0 1 17 0 13 12 0 12 50 0 3 2 0 2 57 0 3 18 0 4 5 0
		 5 22 0 4 61 0 9 8 0 8 54 0 6 7 0 7 20 0 6 59 0 11 10 0 10 52 0 16 0 0 19 2 0 21 6 0
		 23 4 0 9 25 0 25 24 0 24 8 0 11 27 0 27 26 0 26 10 0 13 29 0 29 28 0 28 12 0 15 31 0
		 31 30 0 30 14 0 25 33 1 33 32 0 32 24 1 27 35 1 35 34 0 34 26 1 29 37 1 37 36 0 36 28 1
		 31 39 1 39 38 0 38 30 1 33 41 0 41 40 0 40 32 0 35 43 0 43 42 0 42 34 0 37 45 0 45 44 0
		 44 36 0 39 47 0 47 46 0 46 38 0 41 72 1 43 80 1 45 84 1 47 76 1 2 1 0 4 7 0 6 3 0
		 0 5 0 10 9 0 12 11 0 14 13 0 8 15 0 26 25 0 28 27 0 30 29 0 24 31 0 34 33 0 36 35 0
		 38 37 0 32 39 0 42 41 0 44 43 0 46 45 0 40 47 0 48 56 0 49 13 0 50 58 0 51 11 0 52 60 0
		 53 9 0 54 62 0 55 15 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0 53 54 0 54 55 0 55 48 0
		 56 1 0 57 49 0 58 3 0 59 51 0 60 7 0 61 53 0 62 5 0 63 55 0 56 57 1 57 58 1 58 59 1
		 59 60 1 60 61 1 61 62 1 62 63 1 63 56 0 55 64 0 48 65 1 64 65 1 56 66 1 63 67 0 67 66 1
		 67 64 1 48 68 0 56 69 0 68 69 0 66 70 1 69 70 0 65 71 1 71 70 1 68 71 0 72 77 0 73 42 1
		 74 83 0 75 78 0 75 74 0 76 85 0 77 40 1 78 79 0 80 73 0 81 44 1 82 87 0 83 82 0 84 81 0
		 85 46 1 86 79 0 87 86 0 72 75 0 74 73 0 76 79 0 78 77 0 80 83 0 82 81 0 84 87 0 86 85 0
		 72 73 0 76 77 0 80 81 0;
	setAttr ".ed[166:327]" 84 85 0 105 104 0 105 107 0 107 106 0 106 109 0 109 108 0
		 111 108 0 111 110 0 104 110 0 103 102 0 102 136 0 143 136 0 143 103 0 151 144 0 144 89 0
		 89 88 0 88 151 0 89 105 0 104 88 0 101 100 0 100 138 0 137 138 0 137 101 0 145 146 1
		 146 91 0 91 90 0 90 145 0 91 106 0 107 90 0 92 93 0 93 110 0 111 92 0 92 149 0 149 150 1
		 150 93 0 94 95 0 95 108 0 109 94 0 94 147 0 147 148 1 148 95 0 97 96 0 97 113 0 113 112 0
		 112 96 0 99 98 0 99 115 0 115 114 0 114 98 0 101 117 0 117 116 0 116 100 0 103 119 0
		 119 118 0 118 102 0 113 121 1 121 120 0 120 112 1 115 123 1 123 122 0 122 114 1 117 125 1
		 125 124 0 124 116 1 119 127 1 127 126 0 126 118 1 121 129 0 129 128 0 128 120 0 123 131 0
		 131 130 0 130 122 0 125 133 0 133 132 0 132 124 0 127 135 0 135 134 0 134 126 0 129 154 1
		 154 159 0 159 128 1 131 162 1 162 155 0 155 130 1 133 166 1 166 163 0 163 132 1 135 158 1
		 158 167 0 167 134 1 160 159 0 154 157 0 157 160 0 156 155 0 162 165 0 156 165 0 164 163 0
		 166 169 0 164 169 0 168 167 0 158 161 0 168 161 0 144 145 1 90 89 0 92 95 0 148 149 1
		 146 147 1 94 91 0 150 151 1 88 93 0 98 97 0 114 113 0 100 99 0 116 115 0 102 101 0
		 118 117 0 96 103 0 112 119 0 122 121 0 124 123 0 126 125 0 120 127 0 130 129 0 132 131 0
		 134 133 0 128 135 0 154 155 0 162 163 0 166 167 0 158 159 0 136 137 0 139 99 0 138 139 0
		 139 140 0 98 140 0 141 97 0 140 141 0 141 142 0 96 142 0 142 143 0 145 137 0 136 144 0
		 138 146 0 147 139 0 140 148 0 149 141 0 142 150 0 151 143 0 136 65 1 143 64 0 152 153 0
		 153 70 0 152 71 0 151 67 0 144 66 1 144 153 0 136 152 0 160 161 0 157 156 0 165 164 0
		 169 168 0;
	setAttr -s 160 -ch 656 ".fc[0:159]" -type "polyFaces" 
		f 8 -3 1 4 3 5 -7 7 -1
		mu 0 8 6 24 7 25 8 26 9 27
		f 4 8 9 -108 99
		mu 0 4 34 22 48 57
		f 4 123 108 10 11
		mu 0 4 67 58 15 13
		f 4 -11 12 2 28
		mu 0 4 0 15 24 6
		f 4 13 14 -102 93
		mu 0 4 32 21 50 49
		f 4 117 110 15 16
		mu 0 4 59 60 1 10
		f 4 -16 17 -5 29
		mu 0 4 10 1 25 7
		f 4 18 19 -8 31
		mu 0 4 11 2 27 9
		f 4 -19 20 121 114
		mu 0 4 2 11 64 65
		f 4 23 24 -6 30
		mu 0 4 3 4 26 8
		f 4 -24 25 119 112
		mu 0 4 17 5 61 63
		f 4 -22 32 33 34
		mu 0 4 23 28 36 35
		f 4 -27 35 36 37
		mu 0 4 20 30 38 29
		f 4 -14 38 39 40
		mu 0 4 21 32 40 31
		f 4 -9 41 42 43
		mu 0 4 22 34 42 33
		f 4 -34 44 45 46
		mu 0 4 35 36 44 43
		f 4 -37 47 48 49
		mu 0 4 29 38 45 37
		f 4 -40 50 51 52
		mu 0 4 31 40 46 39
		f 4 -43 53 54 55
		mu 0 4 33 42 47 41
		f 4 -46 56 57 58
		mu 0 4 43 44 76 79
		f 4 -49 59 60 61
		mu 0 4 37 45 80 77
		f 4 -52 62 63 64
		mu 0 4 39 46 82 81
		f 4 -55 65 66 67
		mu 0 4 41 47 78 83
		f 4 -58 68 139 145
		mu 0 4 79 76 88 97
		f 4 -61 69 147 140
		mu 0 4 77 80 92 96
		f 4 -64 70 151 148
		mu 0 4 81 82 94 98
		f 4 -67 71 144 152
		mu 0 4 83 78 90 99
		f 4 158 -140 155 142
		mu 0 4 84 97 88 89
		f 4 156 -148 159 -142
		mu 0 4 85 96 92 93
		f 4 160 -152 161 -150
		mu 0 4 86 98 94 95
		f 4 162 -145 157 -154
		mu 0 4 87 99 90 91
		f 4 116 -17 72 -109
		mu 0 4 58 59 10 15
		f 4 73 -113 120 -21
		mu 0 4 11 17 63 64
		f 4 118 -26 74 -111
		mu 0 4 60 62 12 1
		f 4 122 -12 75 -115
		mu 0 4 66 67 13 14
		f 4 -13 -73 -30 -2
		mu 0 4 24 15 10 7
		f 4 -18 -75 -31 -4
		mu 0 4 25 16 3 8
		f 4 -25 -74 -32 6
		mu 0 4 26 17 11 9
		f 4 -20 -76 -29 0
		mu 0 4 27 18 19 6
		f 4 -77 -38 80 -33
		mu 0 4 28 20 29 36
		f 4 -78 -41 81 -36
		mu 0 4 30 21 31 38
		f 4 -79 -44 82 -39
		mu 0 4 32 22 33 40
		f 4 -80 -35 83 -42
		mu 0 4 34 23 35 42
		f 4 -81 -50 84 -45
		mu 0 4 36 29 37 44
		f 4 -82 -53 85 -48
		mu 0 4 38 31 39 45
		f 4 -83 -56 86 -51
		mu 0 4 40 33 41 46
		f 4 -84 -47 87 -54
		mu 0 4 42 35 43 47
		f 4 -85 -62 88 -57
		mu 0 4 44 37 77 76
		f 4 -86 -65 89 -60
		mu 0 4 45 39 81 80
		f 4 -87 -68 90 -63
		mu 0 4 46 41 83 82
		f 4 -88 -59 91 -66
		mu 0 4 47 43 79 78
		f 4 -69 -89 -141 -164
		mu 0 4 88 76 77 96
		f 4 -70 -90 -149 -166
		mu 0 4 92 80 81 98
		f 4 -71 -91 -153 -167
		mu 0 4 94 82 83 99
		f 4 -72 -92 -146 -165
		mu 0 4 90 78 79 97
		f 4 -94 -101 -10 78
		mu 0 4 32 49 48 22
		f 4 -96 -103 -15 77
		mu 0 4 30 52 50 21
		f 4 -104 95 26 27
		mu 0 4 53 51 30 20
		f 4 -98 -105 -28 76
		mu 0 4 28 54 53 20
		f 4 -106 97 21 22
		mu 0 4 55 54 28 23
		f 4 -100 -107 -23 79
		mu 0 4 34 57 56 23
		f 4 100 -110 -117 -93
		mu 0 4 48 49 59 58
		f 4 101 94 -118 109
		mu 0 4 49 50 60 59
		f 4 102 -112 -119 -95
		mu 0 4 50 52 62 60
		f 4 -120 111 103 96
		mu 0 4 63 61 51 53
		f 4 -121 -97 104 -114
		mu 0 4 64 63 53 54
		f 4 -122 113 105 98
		mu 0 4 65 64 54 55
		f 4 106 -116 -123 -99
		mu 0 4 56 57 67 66
		f 4 107 125 -127 -125
		mu 0 4 57 48 69 68
		f 4 133 135 -138 -139
		mu 0 4 72 73 74 75
		f 4 -124 128 129 -128
		mu 0 4 58 67 71 70
		f 4 115 124 -131 -129
		mu 0 4 67 57 68 71
		f 4 92 132 -134 -132
		mu 0 4 48 58 73 72
		f 4 127 134 -136 -133
		mu 0 4 58 70 74 73
		f 4 -126 131 138 -137
		mu 0 4 69 48 72 75
		f 8 -147 -143 143 141 150 149 154 153
		mu 0 8 91 84 89 85 93 86 95 87
		f 4 163 -157 -144 -156
		mu 0 4 88 96 85 89
		f 4 164 -159 146 -158
		mu 0 4 90 97 84 91
		f 4 165 -161 -151 -160
		mu 0 4 92 98 86 93
		f 4 166 -163 -155 -162
		mu 0 4 94 99 87 95
		f 8 174 -174 172 -172 -171 -170 -169 167
		mu 0 8 100 107 106 105 104 103 102 101
		f 4 -179 177 -177 -176
		mu 0 4 108 111 110 109
		f 4 -183 -182 -181 -180
		mu 0 4 112 115 114 113
		f 4 -185 -168 -184 181
		mu 0 4 116 100 101 114
		f 4 -189 187 -187 -186
		mu 0 4 117 120 119 118
		f 4 -193 -192 -191 -190
		mu 0 4 121 124 123 122
		f 4 -195 169 -194 191
		mu 0 4 124 102 103 123
		f 4 -198 173 -197 -196
		mu 0 4 125 106 107 126
		f 4 -201 -200 -199 195
		mu 0 4 126 128 127 125
		f 4 -204 171 -203 -202
		mu 0 4 129 104 105 130
		f 4 -207 -206 -205 201
		mu 0 4 131 134 133 132
		f 4 -211 -210 -209 207
		mu 0 4 135 138 137 136
		f 4 -215 -214 -213 211
		mu 0 4 139 142 141 140
		f 4 -218 -217 -216 185
		mu 0 4 118 144 143 117
		f 4 -221 -220 -219 175
		mu 0 4 109 146 145 108
		f 4 -224 -223 -222 209
		mu 0 4 138 148 147 137
		f 4 -227 -226 -225 213
		mu 0 4 142 150 149 141
		f 4 -230 -229 -228 216
		mu 0 4 144 152 151 143
		f 4 -233 -232 -231 219
		mu 0 4 146 154 153 145
		f 4 -236 -235 -234 222
		mu 0 4 148 156 155 147
		f 4 -239 -238 -237 225
		mu 0 4 150 158 157 149
		f 4 -242 -241 -240 228
		mu 0 4 152 160 159 151
		f 4 -245 -244 -243 231
		mu 0 4 154 162 161 153
		f 4 -248 -247 -246 234
		mu 0 4 156 164 163 155
		f 4 -251 -250 -249 237
		mu 0 4 158 166 165 157
		f 4 -254 -253 -252 240
		mu 0 4 160 168 167 159
		f 4 -257 -256 -255 243
		mu 0 4 162 170 169 161
		f 4 -260 -259 246 -258
		mu 0 4 171 172 163 164
		f 4 262 -262 249 -261
		mu 0 4 173 174 165 166
		f 4 265 -265 252 -264
		mu 0 4 175 176 167 168
		f 4 268 -268 255 -267
		mu 0 4 177 178 169 170
		f 4 180 -271 192 -270
		mu 0 4 113 114 124 121
		f 4 198 -273 206 -272
		mu 0 4 125 127 134 131
		f 4 190 -275 204 -274
		mu 0 4 122 123 180 179
		f 4 200 -277 182 -276
		mu 0 4 181 182 115 112
		f 4 168 194 270 183
		mu 0 4 101 102 124 114
		f 4 170 203 274 193
		mu 0 4 103 104 129 183
		f 4 -173 197 271 202
		mu 0 4 105 106 125 131
		f 4 -175 184 276 196
		mu 0 4 107 100 185 184
		f 4 208 -279 214 277
		mu 0 4 136 137 142 139
		f 4 212 -281 217 279
		mu 0 4 140 141 144 118
		f 4 215 -283 220 281
		mu 0 4 117 143 146 109
		f 4 218 -285 210 283
		mu 0 4 108 145 138 135
		f 4 221 -286 226 278
		mu 0 4 137 147 150 142
		f 4 224 -287 229 280
		mu 0 4 141 149 152 144
		f 4 227 -288 232 282
		mu 0 4 143 151 154 146
		f 4 230 -289 223 284
		mu 0 4 145 153 148 138
		f 4 233 -290 238 285
		mu 0 4 147 155 158 150
		f 4 236 -291 241 286
		mu 0 4 149 157 160 152
		f 4 239 -292 244 287
		mu 0 4 151 159 162 154
		f 4 242 -293 235 288
		mu 0 4 153 161 156 148
		f 4 293 250 289 245
		mu 0 4 163 166 158 155
		f 4 294 253 290 248
		mu 0 4 165 168 160 157
		f 4 295 256 291 251
		mu 0 4 167 170 162 159
		f 4 296 247 292 254
		mu 0 4 169 164 156 161
		f 4 -282 176 297 188
		mu 0 4 117 109 110 120
		f 4 -280 186 299 298
		mu 0 4 140 118 119 186
		f 4 -302 -212 -299 300
		mu 0 4 187 139 140 188
		f 4 -278 301 303 302
		mu 0 4 136 139 187 189
		f 4 -306 -208 -303 304
		mu 0 4 190 135 136 189
		f 4 -284 305 306 178
		mu 0 4 108 135 191 111
		f 4 308 269 307 -298
		mu 0 4 110 113 121 120
		f 4 -308 189 -310 -188
		mu 0 4 120 121 122 119
		f 4 309 273 310 -300
		mu 0 4 119 122 179 186
		f 4 -312 -301 -311 205
		mu 0 4 134 187 188 133
		f 4 312 -304 311 272
		mu 0 4 127 189 187 134
		f 4 -314 -305 -313 199
		mu 0 4 128 190 189 127
		f 4 313 275 314 -307
		mu 0 4 191 181 112 111
		f 4 316 126 -316 -178
		mu 0 4 111 193 192 110
		f 4 319 137 -319 -318
		mu 0 4 194 197 196 195
		f 4 321 -130 -321 179
		mu 0 4 113 199 198 112
		f 4 320 130 -317 -315
		mu 0 4 112 198 193 111
		f 4 323 317 -323 -309
		mu 0 4 110 194 195 113
		f 4 322 318 -135 -322
		mu 0 4 113 195 196 199
		f 4 136 -320 -324 315
		mu 0 4 192 197 194 110
		f 8 -269 -328 -266 -327 -263 -326 259 324
		mu 0 8 178 177 176 175 174 173 172 171
		f 4 258 325 260 -294
		mu 0 4 163 172 173 166
		f 4 267 -325 257 -297
		mu 0 4 169 178 171 164
		f 4 261 326 263 -295
		mu 0 4 165 174 175 168
		f 4 264 327 266 -296
		mu 0 4 167 176 177 170;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_BackShapeOrig1" -p "Cubic_Back";
	rename -uid "D1AB251D-4BBE-5C18-EF6B-68B2ECA5F6AB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 92 ".uvst[0].uvsp[0:91]" -type "float2" 0.30411202 0.083333246
		 0.30411202 0.16666663 0.44588798 0.083333239 0.44588798 0.16666663 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.75 0.5 0.75 0.5 0.75 0.5 0.75 0.5 0.7812469 0.5 0.7812469 0.375 0.37500003
		 0.375 0.375 0.24999997 0.25 0.375 0.375 0.375 0.87499994 0.375 0.875 0.375 0.875
		 0.375 0.875 0.40624684 0.875 0.40624687 0.875 0.625 0.37500003 0.625 0.375 0.75 0.25
		 0.625 0.375 0.625 0.875 0.625 0.87499994 0.625 0.875 0.625 0.875 0.59375316 0.875
		 0.59375316 0.87499994 0.5 0.25 0.5 0.25 0.49999997 0.25 0.5 1 0.50000012 1 0.5 1
		 0.5 1 0.49999994 0.9687531 0.49999997 0.9687531 0.5 0.54166663 0.6875 0.47916663
		 0.5 0.70833337 0.3125 0.4791666 0.5 0.25 0.625 0.37500003 0.5 0.5 0.375 0.37500003
		 0.3125 0.4791666 0.5 0.54166663 0.24999997 0.25 0.49999997 0.25 0.375 0.375 0.375
		 0.375 0.5 0.25 0.6875 0.47916663 0.75 0.25 0.625 0.375 0.625 0.375 0.5 0.5 0.5 0.5
		 0.375 0.875 0.375 0.87499994 0.5 0.75 0.5 0.75 0.625 0.875 0.625 0.87499994 0.5 1
		 0.50000012 1 0.375 0.875 0.5 0.75 0.625 0.875 0.5 1 0.375 0.875 0.5 0.75 0.625 0.875
		 0.5 1 0.5 0.7812469 0.40624684 0.875 0.59375316 0.875 0.49999994 0.9687531 0.5 0.7812469
		 0.40624687 0.875 0.59375316 0.87499994 0.49999997 0.9687531 0.5 0.70833337 0.30411202
		 0.083333246 0.44588798 0.083333239 0.44588798 0.16666663 0.30411202 0.16666663;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 88 ".pt[0:87]" -type "float3"  1 0 -0.79699135 1 0.60251427 
		-0.79699135 1 0 0 1 0.60251427 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 
		1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 
		0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 
		0 0 1 0 0 1 0 0 1 0 0 1 0 0 -1 0 -0.79699135 -1 0.60251427 -0.79699135 -1 0 0 -1 
		0.60251427 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 
		0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 
		0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 
		-1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0;
	setAttr -s 88 ".vt[0:87]"  2 21 -17.20300865 2 29.39748573 -17.20300865
		 2 21 -13 2 29.39748573 -13 7.076146603 32.10040665 -18.076147079 7.076145649 31 -18.076147079
		 8.03963089 31 -19.03963089 7.076145649 23 -18.076147079 7.076145649 22 -18.076147079
		 7.92442894 22 -18.92442894 7.92442894 19 -18.92442894 6.20313072 19 -17.20313072
		 6.20313072 22.61545563 -17.20313072 2.9238534 32.10040665 -18.076147079 2.9238534 31 -18.076147079
		 1.96036839 31 -19.039632797 2.92385364 23 -18.076147079 2.92385364 22 -18.076147079
		 2.075572014 22 -18.92443085 2.075572014 19 -18.92443085 3.79687047 19 -17.20313072
		 3.79687047 22.61545563 -17.20313072 7.076146603 32.10040665 -13.92385483 7.076145649 31 -13.92385387
		 8.039631844 31 -12.96036911 7.076147079 23 -13.92385387 7.076147079 22 -13.92385387
		 7.92442989 22 -13.075572968 7.92442989 19 -13.075572968 6.20312977 19 -14.79687119
		 6.20312977 22.61545563 -14.79687119 2.92385387 32.10040665 -13.92385197 2.9238534 31 -13.92385387
		 1.96036816 31 -12.96036911 2.92385387 23 -13.92385387 2.92385387 22 -13.92385387
		 2.075572014 22 -13.075572968 2.075572014 19 -13.075572968 3.79686952 19 -14.79686928
		 3.79686952 22.61545563 -14.79686928 1.96036863 23 -13 8.03963089 23 -13 8.03963089 23 -19.03963089
		 1.96036839 23 -19.03963089 -2 21 -17.20300865 -2 29.39748573 -17.20300865 -2 21 -13
		 -2 29.39748573 -13 -7.076146603 32.10040665 -18.076147079 -7.076145649 31 -18.076147079
		 -8.03963089 31 -19.03963089 -7.076145649 23 -18.076147079 -7.076145649 22 -18.076147079
		 -7.92442894 22 -18.92442894 -7.92442894 19 -18.92442894 -6.20313072 19 -17.20313072
		 -6.20313072 22.61545563 -17.20313072 -2.9238534 32.10040665 -18.076147079 -2.9238534 31 -18.076147079
		 -1.96036839 31 -19.039632797 -2.92385364 23 -18.076147079 -2.92385364 22 -18.076147079
		 -2.075572014 22 -18.92443085 -2.075572014 19 -18.92443085 -3.79687047 19 -17.20313072
		 -3.79687047 22.61545563 -17.20313072 -7.076146603 32.10040665 -13.92385483 -7.076145649 31 -13.92385387
		 -8.039631844 31 -12.96036911 -7.076147079 23 -13.92385387 -7.076147079 22 -13.92385387
		 -7.92442989 22 -13.075572968 -7.92442989 19 -13.075572968 -6.20312977 19 -14.79687119
		 -6.20312977 22.61545563 -14.79687119 -2.92385387 32.10040665 -13.92385197 -2.9238534 31 -13.92385387
		 -1.96036816 31 -12.96036911 -2.92385387 23 -13.92385387 -2.92385387 22 -13.92385387
		 -2.075572014 22 -13.075572968 -2.075572014 19 -13.075572968 -3.79686952 19 -14.79686928
		 -3.79686952 22.61545563 -14.79686928 -1.96036863 23 -13 -8.03963089 23 -13 -8.03963089 23 -19.03963089
		 -1.96036839 23 -19.03963089;
	setAttr -s 164 ".ed[0:163]"  31 13 0 25 7 0 34 25 0 16 34 0 7 16 0 1 0 0
		 0 2 0 1 3 0 2 3 0 12 21 0 5 23 0 6 24 0 8 17 0 9 18 0 10 19 0 11 29 0 12 30 0 4 5 0
		 5 6 0 6 42 0 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 13 4 0 14 5 0 15 6 0 17 35 0 18 36 0
		 19 37 0 20 11 0 13 14 0 14 15 0 15 43 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0 22 4 0
		 23 32 0 24 33 0 26 8 0 27 9 0 28 10 0 29 38 0 30 39 0 22 23 0 23 24 0 24 41 0 25 26 0
		 26 27 0 27 28 0 28 29 0 29 30 0 31 22 0 32 14 0 33 15 0 35 26 0 36 27 0 37 28 0 38 20 0
		 39 21 0 31 32 0 32 33 0 33 40 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 40 34 0 41 25 0
		 42 7 0 43 16 0 40 41 0 41 42 0 42 43 0 43 40 0 75 66 0 66 48 0 57 48 0 75 57 0 87 84 0
		 77 59 0 58 59 0 76 77 0 76 58 0 84 85 0 68 77 0 67 68 0 67 76 0 59 50 0 58 49 0 49 50 0
		 50 68 0 49 67 0 60 61 0 51 60 0 51 52 0 52 61 0 69 51 0 69 70 0 70 52 0 78 79 0 79 70 0
		 78 69 0 61 79 0 60 78 0 61 62 0 52 53 0 53 62 0 70 71 0 71 53 0 79 80 0 80 71 0 62 80 0
		 62 63 0 53 54 0 54 63 0 71 72 0 72 54 0 80 81 0 81 72 0 63 81 0 64 55 0 63 64 0 54 55 0
		 55 73 0 72 73 0 81 82 0 73 82 0 82 64 0 56 65 0 64 65 0 55 56 0 56 74 0 73 74 0 82 83 0
		 74 83 0 83 65 0 85 86 0 86 87 0 77 84 0 68 85 0 50 86 0 59 87 0 44 0 0 44 46 0 46 2 0
		 46 47 0 47 3 0 45 44 0 45 1 0 45 47 0 57 58 0 75 76 0 48 49 0 66 67 0 84 78 0 85 69 0
		 86 51 0 87 60 0;
	setAttr -s 82 -ch 328 ".fc[0:81]" -type "polyFaces" 
		f 4 56 40 -26 -1
		mu 0 4 33 23 4 13
		f 4 58 34 79 -67
		mu 0 4 35 15 45 42
		f 4 33 -59 -66 57
		mu 0 4 14 16 35 34
		f 4 -51 42 66 76
		mu 0 4 43 25 35 42
		f 4 65 -43 -50 41
		mu 0 4 34 35 26 24
		f 4 -28 -34 26 18
		mu 0 4 6 16 14 5
		f 4 27 19 78 -35
		mu 0 4 15 6 44 45
		f 4 -12 -19 10 49
		mu 0 4 26 6 5 24
		f 4 -20 11 50 77
		mu 0 4 44 6 25 43
		f 4 -36 -5 20 12
		mu 0 4 18 17 7 8
		f 4 -21 -2 51 43
		mu 0 4 8 7 27 28
		f 4 67 59 -52 -3
		mu 0 4 36 37 28 27
		f 4 35 28 -68 -4
		mu 0 4 17 18 37 36
		f 4 -37 -13 21 13
		mu 0 4 19 18 8 9
		f 4 -22 -44 52 44
		mu 0 4 9 8 28 29
		f 4 -60 68 60 -53
		mu 0 4 28 37 38 29
		f 4 -29 36 29 -69
		mu 0 4 37 18 19 38
		f 4 -38 -14 22 14
		mu 0 4 20 19 9 10
		f 4 -23 -45 53 45
		mu 0 4 10 9 29 30
		f 4 -61 69 61 -54
		mu 0 4 29 38 39 30
		f 4 -30 37 30 -70
		mu 0 4 38 19 20 39
		f 4 -32 -39 -15 23
		mu 0 4 11 21 20 10
		f 4 -16 -24 -46 54
		mu 0 4 31 11 10 30
		f 4 -62 70 -47 -55
		mu 0 4 30 39 40 31
		f 4 -31 38 -63 -71
		mu 0 4 39 20 21 40
		f 4 9 -40 31 24
		mu 0 4 12 22 21 11
		f 4 -17 -25 15 55
		mu 0 4 32 12 11 31
		f 4 71 -48 -56 46
		mu 0 4 40 41 32 31
		f 4 39 -64 -72 62
		mu 0 4 21 22 41 40
		f 4 7 -9 -7 -6
		mu 0 4 1 3 2 0
		f 4 -8 -155 155 152
		mu 0 4 3 1 91 90
		f 4 -10 16 47 63
		mu 0 4 22 12 32 41
		f 4 0 32 -58 -65
		mu 0 4 33 13 14 34
		f 4 -27 -33 25 17
		mu 0 4 5 14 13 4
		f 4 -11 -18 -41 48
		mu 0 4 24 5 4 23
		f 4 -57 64 -42 -49
		mu 0 4 23 33 34 24
		f 4 72 2 -74 -77
		mu 0 4 42 36 27 43
		f 4 1 -75 -78 73
		mu 0 4 27 7 44 43
		f 4 4 -76 -79 74
		mu 0 4 7 17 45 44
		f 4 -80 75 3 -73
		mu 0 4 42 45 17 36
		f 4 83 82 -82 -81
		mu 0 4 46 49 48 47
		f 4 144 -85 -148 -86
		mu 0 4 53 51 50 52
		f 4 -89 87 85 -87
		mu 0 4 54 56 53 55
		f 4 145 -90 -145 -91
		mu 0 4 58 57 51 53
		f 4 -93 91 90 -88
		mu 0 4 56 60 59 53
		f 4 -96 -95 86 93
		mu 0 4 61 62 54 55
		f 4 -92 -98 95 96
		mu 0 4 59 60 62 61
		f 4 -102 -101 99 98
		mu 0 4 63 66 65 64
		f 4 -105 -104 102 100
		mu 0 4 66 68 67 65
		f 4 107 103 -107 -106
		mu 0 4 69 67 68 70
		f 4 109 105 -109 -99
		mu 0 4 64 69 70 63
		f 4 -113 -112 101 110
		mu 0 4 71 72 66 63
		f 4 -115 -114 104 111
		mu 0 4 72 73 68 66
		f 4 113 -117 -116 106
		mu 0 4 68 73 74 70
		f 4 115 -118 -111 108
		mu 0 4 70 74 71 63
		f 4 -121 -120 112 118
		mu 0 4 75 76 72 71
		f 4 -123 -122 114 119
		mu 0 4 76 77 73 72
		f 4 121 -125 -124 116
		mu 0 4 73 77 78 74
		f 4 123 -126 -119 117
		mu 0 4 74 78 75 71
		f 4 -129 120 127 126
		mu 0 4 79 76 75 80
		f 4 -131 122 128 129
		mu 0 4 81 77 76 79
		f 4 130 132 -132 124
		mu 0 4 77 81 82 78
		f 4 131 133 -128 125
		mu 0 4 78 82 80 75
		f 4 -137 -127 135 -135
		mu 0 4 83 79 80 84
		f 4 -139 -130 136 137
		mu 0 4 85 81 79 83
		f 4 -133 138 140 -140
		mu 0 4 82 81 85 86
		f 4 -134 139 141 -136
		mu 0 4 80 82 86 84
		f 4 -143 -146 -97 146
		mu 0 4 87 57 58 61
		f 4 -144 -147 -94 147
		mu 0 4 50 87 61 52
		f 4 -151 -150 148 6
		mu 0 4 2 89 88 0
		f 4 -153 -152 150 8
		mu 0 4 3 90 89 2
		f 4 -149 -154 154 5
		mu 0 4 0 88 91 1
		f 4 153 149 151 -156
		mu 0 4 91 88 89 90
		f 4 -142 -141 -138 134
		mu 0 4 84 86 85 83
		f 4 157 88 -157 -84
		mu 0 4 46 56 54 49
		f 4 -159 -83 156 94
		mu 0 4 62 48 49 54
		f 4 -160 81 158 97
		mu 0 4 60 47 48 62
		f 4 159 92 -158 80
		mu 0 4 47 60 56 46
		f 4 89 161 -108 -161
		mu 0 4 51 57 67 69
		f 4 -162 142 162 -103
		mu 0 4 67 57 87 65
		f 4 -163 143 163 -100
		mu 0 4 65 87 50 64
		f 4 160 -110 -164 84
		mu 0 4 51 69 64 50;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cubic_Left_Arm";
	rename -uid "F40C263B-4655-BBE7-BEB3-E6A2BF5199EF";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 18 25.5 2.5 ;
	setAttr ".sp" -type "double3" 18 25.5 2.5 ;
createNode mesh -n "Cubic_Left_ArmShape" -p "Cubic_Left_Arm";
	rename -uid "A4503E61-40F5-6E9E-C34A-4292DCE0637C";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.4375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_Left_ArmShapeOrig" -p "Cubic_Left_Arm";
	rename -uid "BDCFBA3C-4887-ABAB-B9F2-198CDB3B7003";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 116 ".uvst[0].uvsp[0:115]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.625 0.375 0.75 0.25 0.625
		 0.875 0.75 0 0.25 0 0.375 0.875 0.1875 0 0.375 0.8125 0.625 0.8125 0.8125 0 0.625
		 0.4375 0.8125 0.25 0.1875 0.25 0.375 0.4375 0.375 0.375 0.625 0.375 0.625 0.4375
		 0.375 0.4375 0.375 0.8125 0.625 0.8125 0.625 0.875 0.375 0.875 0.75 0.25 0.75 0 0.8125
		 0 0.8125 0.25 0.1875 0.25 0.1875 0 0.25 0 0.25 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 10 ".pt";
	setAttr ".pt[52]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".pt[53]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".pt[54]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".pt[55]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".pt[56]" -type "float3" 0 4.7683716e-07 2.3040946 ;
	setAttr ".pt[57]" -type "float3" 0 4.7683716e-07 2.3040946 ;
	setAttr ".pt[58]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".pt[59]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".pt[62]" -type "float3" 0 0 2.3040946 ;
	setAttr ".pt[63]" -type "float3" 0 0 2.3040946 ;
	setAttr -s 64 ".vt[0:63]"  12 31 6 24 31 6 12 40 6 24 40 6 12 40 -6
		 24 40 -6 12 31 -6 24 31 -6 15 16 3 21 16 3 15 28 3 21 28 3 15 28 -3 21 28 -3 15 16 -3
		 21 16 -3 14 11 15 22 11 15 14 19 15 22 19 15 14 19 -7 22 19 -7 14 11 -7 22 11 -7
		 13 28 5 23 28 5 13 31 5 23 31 5 13 31 -5 23 31 -5 13 28 -5 23 28 -5 13 15 10 19 15 10
		 13 20 10 19 20 10 13 20 7.5 19 20 7.5 13 15 7.5 19 15 7.5 15 12 -7 21 12 -7 21 18 -7
		 15 18 -7 21 18 -10 15 18 -10 21 12 -10 15 12 -10 14 19 7 22 19 7 22 11 7 14 11 7
		 14 11 6 22 11 6 22 19 6 14 19 6 15.10976887 17.89023018 7 20.89023018 17.89023018 7
		 20.89023018 17.89023018 6 15.10976887 17.89023018 6 15.10976887 12.10976887 6 20.89023018 12.10976887 6
		 20.89023018 12.10976887 7 15.10976887 12.10976887 7;
	setAttr -s 100 ".ed[0:99]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0
		 11 13 0 12 14 0 13 15 0 14 8 0 15 9 0 16 17 0 18 19 0 20 21 0 22 23 0 16 18 0 17 19 0
		 18 48 0 19 49 0 20 22 0 21 23 0 22 52 0 23 53 0 24 25 0 26 27 0 28 29 0 30 31 0 24 26 0
		 25 27 0 26 28 0 27 29 0 28 30 0 29 31 0 30 24 0 31 25 0 32 33 0 34 35 0 36 37 0 38 39 0
		 32 34 0 33 35 0 34 36 0 35 37 0 36 38 0 37 39 0 38 32 0 39 33 0 40 41 0 41 42 0 43 42 0
		 40 43 0 42 44 0 45 44 0 43 45 0 44 46 0 47 46 0 45 47 0 46 41 0 47 40 0 50 17 0 51 16 0
		 48 49 0 49 50 0 50 51 0 51 48 0 54 21 0 55 20 0 52 53 0 53 54 0 54 55 0 55 52 0 56 57 0
		 54 58 1 57 58 0 55 59 1 58 59 0 56 59 0 52 60 1 53 61 1 60 61 0 61 62 0 62 63 0 60 63 0
		 57 62 0 61 58 0 59 60 0 63 56 0;
	setAttr -s 50 -ch 200 ".fc[0:49]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 12 17 -14 -17
		mu 0 4 14 15 16 17
		f 4 13 19 -15 -19
		mu 0 4 17 16 18 19
		f 4 14 21 -16 -21
		mu 0 4 19 18 20 21
		f 4 15 23 -13 -23
		mu 0 4 21 20 22 23
		f 4 -24 -22 -20 -18
		mu 0 4 15 24 25 16
		f 4 22 16 18 20
		mu 0 4 26 14 17 27
		f 4 24 29 -26 -29
		mu 0 4 28 29 30 31
		f 4 84 86 88 -90
		mu 0 4 100 101 102 103
		f 4 26 33 -28 -33
		mu 0 4 33 32 34 35
		f 4 92 93 94 -96
		mu 0 4 104 105 106 107
		f 4 96 -94 97 -87
		mu 0 4 108 109 110 111
		f 4 98 95 99 89
		mu 0 4 112 113 114 115
		f 4 36 41 -38 -41
		mu 0 4 42 43 44 45
		f 4 37 43 -39 -43
		mu 0 4 45 44 46 47
		f 4 38 45 -40 -45
		mu 0 4 47 46 48 49
		f 4 39 47 -37 -47
		mu 0 4 49 48 50 51
		f 4 -48 -46 -44 -42
		mu 0 4 43 52 53 44
		f 4 46 40 42 44
		mu 0 4 54 42 45 55
		f 4 48 53 -50 -53
		mu 0 4 56 57 58 59
		f 4 49 55 -51 -55
		mu 0 4 59 58 60 61
		f 4 50 57 -52 -57
		mu 0 4 61 60 62 63
		f 4 51 59 -49 -59
		mu 0 4 63 62 64 65
		f 4 -60 -58 -56 -54
		mu 0 4 57 66 67 58
		f 4 58 52 54 56
		mu 0 4 68 56 59 69
		f 4 60 61 -63 -64
		mu 0 4 70 71 72 73
		f 4 62 64 -66 -67
		mu 0 4 73 72 74 75
		f 4 65 67 -69 -70
		mu 0 4 75 74 76 77
		f 4 68 70 -61 -72
		mu 0 4 77 76 78 79
		f 4 -71 -68 -65 -62
		mu 0 4 71 80 81 72
		f 4 71 63 66 69
		mu 0 4 82 70 73 83
		f 4 25 31 -75 -31
		mu 0 4 31 30 86 85
		f 4 -73 -76 -32 -30
		mu 0 4 29 89 87 30
		f 4 -77 72 -25 -74
		mu 0 4 91 88 36 37
		f 4 -78 73 28 30
		mu 0 4 84 90 28 31
		f 4 27 35 -81 -35
		mu 0 4 35 34 94 93
		f 4 -82 -36 -34 -79
		mu 0 4 97 95 38 39
		f 4 -83 78 -27 -80
		mu 0 4 99 96 32 33
		f 4 34 -84 79 32
		mu 0 4 40 92 98 41
		f 4 82 87 -89 -86
		mu 0 4 96 99 103 102
		f 4 80 91 -93 -91
		mu 0 4 93 94 105 104
		f 4 81 85 -98 -92
		mu 0 4 95 97 111 110
		f 4 83 90 -99 -88
		mu 0 4 98 92 113 112
		f 4 75 76 77 74
		mu 0 4 87 88 90 85
		f 4 -85 -100 -95 -97
		mu 0 4 101 115 107 109;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cubic_Right_Arm";
	rename -uid "F492DB79-49E9-545C-4E85-49B5B83594B4";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -18 25.5 2.5 ;
	setAttr ".sp" -type "double3" -18 25.5 2.5 ;
createNode mesh -n "Cubic_Right_ArmShape" -p "Cubic_Right_Arm";
	rename -uid "301E76F7-4DAB-6ADC-AB6B-F2B6DB368643";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.4375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_Right_ArmShapeOrig" -p "Cubic_Right_Arm";
	rename -uid "BF252CD1-4367-5DDD-D910-2AA0C50CDA35";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 116 ".uvst[0].uvsp[0:115]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1
		 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25
		 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125
		 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75
		 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0
		 0.625 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875
		 0 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.625 0.375 0.75 0.25 0.625
		 0.875 0.75 0 0.25 0 0.375 0.875 0.1875 0 0.375 0.8125 0.625 0.8125 0.8125 0 0.625
		 0.4375 0.8125 0.25 0.1875 0.25 0.375 0.4375 0.375 0.375 0.625 0.375 0.625 0.4375
		 0.375 0.4375 0.375 0.8125 0.625 0.8125 0.625 0.875 0.375 0.875 0.75 0.25 0.75 0 0.8125
		 0 0.8125 0.25 0.1875 0.25 0.1875 0 0.25 0 0.25 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 64 ".pt[0:63]" -type "float3"  -24 0 -3.5527137e-15 -48 
		0 -3.5527137e-15 -24 0 -3.5527137e-15 -48 0 -3.5527137e-15 -24 0 -3.5527137e-15 -48 
		0 -3.5527137e-15 -24 0 -3.5527137e-15 -48 0 -3.5527137e-15 -30 0 -3.1086245e-15 -42 
		0 -3.9968029e-15 -30 0 -3.1086245e-15 -42 0 -3.9968029e-15 -30 0 -3.1086245e-15 -42 
		0 -3.9968029e-15 -30 0 -3.1086245e-15 -42 0 -3.9968029e-15 -28 0 -3.5527137e-15 -44 
		0 -5.3290705e-15 -28 0 -3.5527137e-15 -44 0 -5.3290705e-15 -28 0 -3.5527137e-15 -44 
		0 -3.5527137e-15 -28 0 -3.5527137e-15 -44 0 -3.5527137e-15 -26 0 -3.5527137e-15 -46 
		0 -3.5527137e-15 -26 0 -3.5527137e-15 -46 0 -3.5527137e-15 -26 0 -3.5527137e-15 -46 
		0 -3.5527137e-15 -26 0 -3.5527137e-15 -46 0 -3.5527137e-15 -26 0 -3.5527137e-15 -38 
		0 -3.5527137e-15 -26 0 -3.5527137e-15 -38 0 -3.5527137e-15 -26 0 -3.5527137e-15 -38 
		0 -3.5527137e-15 -26 0 -3.5527137e-15 -38 0 -3.5527137e-15 -30 0 -3.5527137e-15 -42 
		0 -3.5527137e-15 -42 0 -3.5527137e-15 -30 0 -3.5527137e-15 -42 0 -3.5527137e-15 -30 
		0 -3.5527137e-15 -42 0 -3.5527137e-15 -30 0 -3.5527137e-15 -28 0 -3.5527137e-15 -44 
		0 -3.5527137e-15 -44 0 -3.5527137e-15 -28 0 -3.5527137e-15 -28 4.7683716e-07 -3.5527137e-15 
		-44 4.7683716e-07 -3.5527137e-15 -44 4.7683716e-07 -3.5527137e-15 -28 4.7683716e-07 
		-3.5527137e-15 -30.219538 4.7683716e-07 2.3040946 -41.78046 4.7683716e-07 2.3040946 
		-41.78046 4.7683716e-07 -3.5527137e-15 -30.219538 4.7683716e-07 -3.5527137e-15 -30.219538 
		0 -3.5527137e-15 -41.78046 0 -3.5527137e-15 -41.78046 0 2.3040946 -30.219538 0 2.3040946;
	setAttr -s 64 ".vt[0:63]"  12 31 6 24 31 6 12 40 6 24 40 6 12 40 -6
		 24 40 -6 12 31 -6 24 31 -6 15 16 3 21 16 3 15 28 3 21 28 3 15 28 -3 21 28 -3 15 16 -3
		 21 16 -3 14 11 15 22 11 15 14 19 15 22 19 15 14 19 -7 22 19 -7 14 11 -7 22 11 -7
		 13 28 5 23 28 5 13 31 5 23 31 5 13 31 -5 23 31 -5 13 28 -5 23 28 -5 13 15 10 19 15 10
		 13 20 10 19 20 10 13 20 7.5 19 20 7.5 13 15 7.5 19 15 7.5 15 12 -7 21 12 -7 21 18 -7
		 15 18 -7 21 18 -10 15 18 -10 21 12 -10 15 12 -10 14 19 7 22 19 7 22 11 7 14 11 7
		 14 11 6 22 11 6 22 19 6 14 19 6 15.10976887 17.89023018 7 20.89023018 17.89023018 7
		 20.89023018 17.89023018 6 15.10976887 17.89023018 6 15.10976887 12.10976887 6 20.89023018 12.10976887 6
		 20.89023018 12.10976887 7 15.10976887 12.10976887 7;
	setAttr -s 100 ".ed[0:99]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0
		 11 13 0 12 14 0 13 15 0 14 8 0 15 9 0 16 17 0 18 19 0 20 21 0 22 23 0 16 18 0 17 19 0
		 18 48 0 19 49 0 20 22 0 21 23 0 22 52 0 23 53 0 24 25 0 26 27 0 28 29 0 30 31 0 24 26 0
		 25 27 0 26 28 0 27 29 0 28 30 0 29 31 0 30 24 0 31 25 0 32 33 0 34 35 0 36 37 0 38 39 0
		 32 34 0 33 35 0 34 36 0 35 37 0 36 38 0 37 39 0 38 32 0 39 33 0 40 41 0 41 42 0 43 42 0
		 40 43 0 42 44 0 45 44 0 43 45 0 44 46 0 47 46 0 45 47 0 46 41 0 47 40 0 50 17 0 51 16 0
		 48 49 0 49 50 0 50 51 0 51 48 0 54 21 0 55 20 0 52 53 0 53 54 0 54 55 0 55 52 0 56 57 0
		 54 58 1 57 58 0 55 59 1 58 59 0 56 59 0 52 60 1 53 61 1 60 61 0 61 62 0 62 63 0 60 63 0
		 57 62 0 61 58 0 59 60 0 63 56 0;
	setAttr -s 50 -ch 200 ".fc[0:49]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0
		f 4 16 13 -18 -13
		mu 0 4 14 17 16 15
		f 4 18 14 -20 -14
		mu 0 4 17 19 18 16
		f 4 20 15 -22 -15
		mu 0 4 19 21 20 18
		f 4 22 12 -24 -16
		mu 0 4 21 23 22 20
		f 4 17 19 21 23
		mu 0 4 15 16 25 24
		f 4 -21 -19 -17 -23
		mu 0 4 26 27 17 14
		f 4 28 25 -30 -25
		mu 0 4 28 31 30 29
		f 4 89 -89 -87 -85
		mu 0 4 100 103 102 101
		f 4 32 27 -34 -27
		mu 0 4 33 35 34 32
		f 4 95 -95 -94 -93
		mu 0 4 104 107 106 105
		f 4 86 -98 93 -97
		mu 0 4 108 111 110 109
		f 4 -90 -100 -96 -99
		mu 0 4 112 115 114 113
		f 4 40 37 -42 -37
		mu 0 4 42 45 44 43
		f 4 42 38 -44 -38
		mu 0 4 45 47 46 44
		f 4 44 39 -46 -39
		mu 0 4 47 49 48 46
		f 4 46 36 -48 -40
		mu 0 4 49 51 50 48
		f 4 41 43 45 47
		mu 0 4 43 44 53 52
		f 4 -45 -43 -41 -47
		mu 0 4 54 55 45 42
		f 4 52 49 -54 -49
		mu 0 4 56 59 58 57
		f 4 54 50 -56 -50
		mu 0 4 59 61 60 58
		f 4 56 51 -58 -51
		mu 0 4 61 63 62 60
		f 4 58 48 -60 -52
		mu 0 4 63 65 64 62
		f 4 53 55 57 59
		mu 0 4 57 58 67 66
		f 4 -57 -55 -53 -59
		mu 0 4 68 69 59 56
		f 4 63 62 -62 -61
		mu 0 4 70 73 72 71
		f 4 66 65 -65 -63
		mu 0 4 73 75 74 72
		f 4 69 68 -68 -66
		mu 0 4 75 77 76 74
		f 4 71 60 -71 -69
		mu 0 4 77 79 78 76
		f 4 61 64 67 70
		mu 0 4 71 72 81 80
		f 4 -70 -67 -64 -72
		mu 0 4 82 83 73 70
		f 4 30 74 -32 -26
		mu 0 4 31 85 86 30
		f 4 29 31 75 72
		mu 0 4 29 30 87 89
		f 4 73 24 -73 76
		mu 0 4 91 37 36 88
		f 4 -31 -29 -74 77
		mu 0 4 84 31 28 90
		f 4 34 80 -36 -28
		mu 0 4 35 93 94 34
		f 4 78 33 35 81
		mu 0 4 97 39 38 95
		f 4 79 26 -79 82
		mu 0 4 99 33 32 96
		f 4 -33 -80 83 -35
		mu 0 4 40 41 98 92
		f 4 85 88 -88 -83
		mu 0 4 96 102 103 99
		f 4 90 92 -92 -81
		mu 0 4 93 104 105 94
		f 4 91 97 -86 -82
		mu 0 4 95 110 111 97
		f 4 87 98 -91 -84
		mu 0 4 98 112 113 92
		f 4 -75 -78 -77 -76
		mu 0 4 87 85 90 88
		f 4 96 94 99 84
		mu 0 4 101 109 107 115;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cubic_Legs";
	rename -uid "AFE71ECD-4CE4-7921-8011-D597344FA6CD";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 0 7.5 0.22773265838623047 ;
	setAttr ".sp" -type "double3" 0 7.5 0.22773265838623047 ;
createNode mesh -n "Cubic_LegsShape" -p "Cubic_Legs";
	rename -uid "AB1DEA92-46DF-C787-45CF-8089D566CCED";
	setAttr -k off ".v";
	setAttr -s 8 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_LegsShapeOrig" -p "Cubic_Legs";
	rename -uid "512E1C6A-4D35-195E-AD23-32B3638ABE15";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 418 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.375 0 0.625 0 0.375 0.25
		 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0 0.875
		 0.25 0.125 0 0.125 0.25 0.125 0 0.375 0 0.375 0.25 0.125 0.25 0.125 0 0.375 0 0.375
		 0.25 0.125 0.25 0.54166663 0.25 0.54166663 0 0.625 0 0.625 0.25 0.54166663 0.75 0.54166663
		 0.5 0.625 0.5 0.625 0.75 0.54166663 1 0.625 1 0.625 0 0.875 0 0.87499964 0.27619034
		 0.62500036 0.27618164 0.125 0 0.375 0 0.37499967 0.27618164 0.12500042 0.27619034
		 0.54166663 0.25 0.625 0.25 0.54166663 0.5 0.625 0.5 0.875 0 0.875 0 0.625 0 0.875
		 0.25 0.875 0.25 0.125 0 0.375 0 0.375 0 0.125 0 0.375 0.25 0.375 0.25 0.125 0.25
		 0.125 0.25 0.875 0.25 0.625 0.25 0.625 0.25 0.875 0.25 0.375 0.25 0.125 0.25 0.125
		 0.25 0.375 0.25 0.375 0.75 0.45833325 0.75 0.45833325 1 0.375 1 0.375 0.5 0.45833325
		 0.5 0.45833325 0.5 0.375 0.5 0.45833325 0.25 0.45833325 0.25 0.45833325 0 0.375 0
		 0.625 0 0.625 0.25 0.375 0.25 0.50275618 0.41317618 0.60973936 0.41113845 0.625 0.5
		 0.375 0.5 0.375 0.5 0.625 0.5 0.625 0.75 0.375 0.75 0.625 0.83333337 0.375 0.83333337
		 0.79166663 0.25 0.79166663 0 0.875 0 0.875 0.25 0.125 0 0.20833337 0 0.20833337 0.25
		 0.125 0.25 0.375 0.25 0.625 0.25 0.49547306 0.33724371 0.38726872 0.33866385 0.70833325
		 0 0.70833325 0.25 0.375 0.91666675 0.625 0.91666675 0.625 1 0.375 1 0.29166675 0.25
		 0.29166675 0 0.49547306 0.33724371 0.38726872 0.33866385 0.39428756 0.41394868 0.375
		 0.41666663 0.70833325 0.25 0.70833325 0 0.79166663 0 0.79166663 0.25 0.20833337 0.25
		 0.20833337 0 0.29166675 0 0.29166675 0.25 0.50275618 0.41317618 0.625 0.5 0.60973936
		 0.41113845 0.375 0.5 0.375 0.41666663 0.625 0.25 0.375 0.25 0.625 0.33333325 0.49547306
		 0.33724371 0.38726872 0.33866385 0.60973936 0.41113845 0.59958345 0.33559322 0.625
		 0.33333325 0.50275618 0.41317618 0.39428756 0.41394868 0.38726872 0.33866385 0.59958345
		 0.33559322 0.38726872 0.33866385 0.49547306 0.33724371 0.39428756 0.41394868 0.375
		 0.41666663 0.39428756 0.41394868 0.59958345 0.33559322 0.625 0.33333325 0.59958345
		 0.33559322 0.60973936 0.41113845 0.50275618 0.41317618 0.625 0.25 0.375 0.25 0.49547306
		 0.33724371 0.625 0.25 0.375 0.25 0.49547306 0.33724371 0.38726872 0.33866385 0.50275618
		 0.41317618 0.49547306 0.33724371 0.49547306 0.33724371 0.70833325 0.25 0.70833325
		 0 0.70833325 0 0.70833325 0.25 0.70833325 0 0.79166663 0 0.79166663 0 0.70833325
		 0 0.79166663 0 0.79166663 0.25 0.79166663 0.25 0.79166663 0 0.20833337 0.25 0.20833337
		 0 0.20833337 0 0.20833337 0.25 0.20833337 0 0.29166675 0 0.29166675 0 0.20833337
		 0 0.29166675 0 0.29166675 0.25 0.29166675 0.25 0.29166675 0 0.50275618 0.41317618
		 0.38726872 0.33866385 0.39428756 0.41394868 0.39428756 0.41394868 0.38726872 0.33866385
		 0.60973936 0.41113845 0.59958345 0.33559322 0.59958345 0.33559322 0.60973936 0.41113845
		 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375
		 1 0.625 1 0.875 0.25 0.875 0 0.125 0 0.125 0.25 0.375 0.25 0.375 0 0.125 0 0.125
		 0 0.375 0 0.375 0.25 0.125 0.25 0.125 0.25 0.54166663 0.25 0.625 0.25 0.625 0 0.54166663
		 0 0.54166663 0.75 0.625 0.75 0.625 0.5 0.54166663 0.5 0.54166663 1 0.625 1 0.625
		 0 0.62500036 0.27618164 0.87499964 0.27619034 0.875 0 0.125 0 0.12500042 0.27619034
		 0.37499967 0.27618164 0.375 0 0.54166663 0.25 0.625 0.25 0.625 0.5 0.54166663 0.5
		 0.625 0 0.875 0 0.875 0 0.875 0.25 0.875 0.25 0.125 0 0.125 0 0.375 0 0.375 0 0.375
		 0.25 0.375 0.25 0.125 0.25 0.125 0.25 0.875 0.25;
	setAttr ".uvst[0].uvsp[250:417]" 0.875 0.25 0.625 0.25 0.625 0.25 0.375 0.25
		 0.375 0.25 0.125 0.25 0.125 0.25 0.375 0.75 0.375 1 0.45833325 1 0.45833325 0.75
		 0.375 0.5 0.45833325 0.5 0.45833325 0.5 0.375 0.5 0.45833325 0.25 0.45833325 0.25
		 0.45833325 0 0.375 0 0.375 0.25 0.625 0.25 0.625 0 0.50275618 0.41317618 0.375 0.5
		 0.625 0.5 0.60973936 0.41113845 0.375 0.5 0.375 0.75 0.625 0.75 0.625 0.5 0.375 0.83333337
		 0.625 0.83333337 0.79166663 0.25 0.875 0.25 0.875 0 0.79166663 0 0.125 0 0.125 0.25
		 0.20833337 0.25 0.20833337 0 0.375 0.25 0.38726872 0.33866385 0.49547306 0.33724371
		 0.625 0.25 0.70833325 0.25 0.70833325 0 0.375 0.91666675 0.375 1 0.625 1 0.625 0.91666675
		 0.29166675 0.25 0.29166675 0 0.49547306 0.33724371 0.375 0.41666663 0.39428756 0.41394868
		 0.38726872 0.33866385 0.70833325 0.25 0.79166663 0.25 0.79166663 0 0.70833325 0 0.20833337
		 0.25 0.29166675 0.25 0.29166675 0 0.20833337 0 0.50275618 0.41317618 0.60973936 0.41113845
		 0.625 0.5 0.375 0.5 0.50275618 0.41317618 0.375 0.41666663 0.375 0.25 0.625 0.25
		 0.49547306 0.33724371 0.625 0.33333325 0.49547306 0.33724371 0.38726872 0.33866385
		 0.60973936 0.41113845 0.50275618 0.41317618 0.625 0.33333325 0.59958345 0.33559322
		 0.39428756 0.41394868 0.59958345 0.33559322 0.38726872 0.33866385 0.49547306 0.33724371
		 0.49547306 0.33724371 0.38726872 0.33866385 0.39428756 0.41394868 0.39428756 0.41394868
		 0.375 0.41666663 0.70833325 0.25 0.70833325 0.25 0.70833325 0 0.70833325 0 0.70833325
		 0 0.70833325 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0 0.79166663 0.25
		 0.79166663 0.25 0.20833337 0.25 0.20833337 0.25 0.20833337 0 0.20833337 0 0.20833337
		 0 0.20833337 0 0.29166675 0 0.29166675 0 0.29166675 0 0.29166675 0 0.29166675 0.25
		 0.29166675 0.25 0.59958345 0.33559322 0.59958345 0.33559322 0.625 0.33333325 0.50275618
		 0.41317618 0.60973936 0.41113845 0.50275618 0.41317618 0.375 0.25 0.625 0.25 0.49547306
		 0.33724371 0.375 0.25 0.625 0.25 0.49547306 0.33724371 0.38726872 0.33866385 0.38726872
		 0.33866385 0.38726872 0.33866385 0.39428756 0.41394868 0.39428756 0.41394868 0.60973936
		 0.41113845 0.60973936 0.41113845 0.59958345 0.33559322 0.59958345 0.33559322 0.375
		 0 0.625 0 0.625 0.25 0.375 0.25 0.375 0.41666663 0.625 0.41666663 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 0.83333337 0.375 0.83333337 0.79166663 0.25 0.79166663
		 0 0.875 0 0.875 0.25 0.125 0 0.20833337 0 0.20833337 0.25 0.125 0.25 0.625 0.33333325
		 0.375 0.33333325 0.70833325 0 0.70833325 0.25 0.375 0.91666675 0.625 0.91666675 0.625
		 1 0.375 1 0.29166675 0.25 0.29166675 0 0.375 0.83333337 0.625 0.83333337 0.625 0.91666675
		 0.375 0.91666675;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 260 ".vt";
	setAttr ".vt[0:165]"  5.5 7 3 10.5 7 3 5.5 14 3 10.5 14 3 5.5 14 -3 10.5 14 -3
		 5.5 7 -3 10.5 7 -3 5.5 10 -1 5.5 10 1 5.5 12 1 5.5 12 -1 2 10 -1 2 10 1 2 12 1 2 12 -1
		 5 2 3 11 2 3 5 7 3 11 7 3 5 7 -3 11 7 -3 5 2 -3 11 2 -3 5 7 1.20163441 11 7 1.20163441
		 11 7 -1.20163441 5 7 -1.20163441 11 5.79836559 -1.20163441 11 5.79836559 1.20163441
		 5 5.79836559 -1.20163441 5 5.79836559 1.20163441 11.5203104 5.79836559 -1.20163441
		 11.5203104 5.79836559 1.20163441 11.5203104 8.20163441 -1.20163441 11.5203104 8.20163441 1.20163441
		 4.47969007 5.79836559 -1.20163441 4.47969007 5.79836559 1.20163441 4.47969007 8.20163441 1.20163441
		 4.47969007 8.20163441 -1.20163441 10 7 1.20163441 10 7 -1.20163441 10 8.20163441 1.20163441
		 10 8.20163441 -1.20163441 6 7 1.20163441 6 7 -1.20163441 6 8.20163441 -1.20163441
		 6 8.20163441 1.20163441 6 2 3 6 2 -3 6 7 -3 6 7 3 10 2 3 10 2 -3 10 7 -3 10 7 3 4 0 9
		 12 0 9 4 2 9 12 2 9 4 2 -7 12 2 -7 4 0 -7 12 0 -7 4 2 2 12 2 2 12 0 1.99999988 4 0 1.99999988
		 4 2 -2 12 2 -2 12 0 -2 4 0 -2 4.63799334 2 -2 11.36200619 2 -2 11.36200619 2 -5 4.63799334 2 -5
		 4.63799334 2 7 11.36200619 2 7 11.36200619 2 2 4.63799334 2 2 3.47006273 3 2 4.63799334 3 2
		 4.63799334 3 -2 3.47006273 3 -2 3.47006273 0 -2 3.47006273 0 1.99999988 12.52993679 3 2
		 12.52993679 0 1.99999988 12.52993679 0 -2 12.52993679 3 -2 11.36200619 3 2 11.36200619 3 -2
		 11.36200619 3 -5 4.63799334 3 -5 4.63799334 3 7 11.36200619 3 7 5.30366898 3 5 10.69633102 3 5
		 10.69633102 3 3 5.30366898 3 3 5.30366898 4 5 10.69633102 4 5 10.69633102 4 3 5.30366898 4 3
		 3.47006273 4 2 4.63799334 4 2 4.63799334 4 -2 3.47006273 4 -2 11.36200619 4 2 11.36200619 4 -2
		 12.52993679 4 2 12.52993679 4 -2 6.000000476837 3 2 6.000000476837 3 -2 6.000000476837 4 -2
		 6.000000476837 4 2 9.99999905 3 2 9.99999905 3 -2 9.99999905 4 2 9.99999905 4 -2
		 -5.5 7 3 -10.5 7 3 -5.5 14 3 -10.5 14 3 -5.5 14 -3 -10.5 14 -3 -5.5 7 -3 -10.5 7 -3
		 -5.5 10 -1 -5.5 10 1 -5.5 12 1 -5.5 12 -1 -2 10 -1 -2 10 1 -2 12 1 -2 12 -1 -5 2 3
		 -11 2 3 -5 7 3 -11 7 3 -5 7 -3 -11 7 -3 -5 2 -3 -11 2 -3 -5 7 1.20163441 -11 7 1.20163441
		 -11 7 -1.20163441 -5 7 -1.20163441 -11 5.79836559 -1.20163441 -11 5.79836559 1.20163441
		 -5 5.79836559 -1.20163441 -5 5.79836559 1.20163441 -11.52031136 5.79836559 -1.20163441
		 -11.52031136 5.79836559 1.20163441 -11.52031136 8.20163441 -1.20163441 -11.52031136 8.20163441 1.20163441
		 -4.47969055 5.79836559 -1.20163441 -4.47969055 5.79836559 1.20163441 -4.47969055 8.20163441 1.20163441
		 -4.47969055 8.20163441 -1.20163441 -10 7 1.20163441 -10 7 -1.20163441 -10 8.20163441 1.20163441
		 -10 8.20163441 -1.20163441 -6 7 1.20163441 -6 7 -1.20163441;
	setAttr ".vt[166:259]" -6 8.20163441 -1.20163441 -6 8.20163441 1.20163441 -6 2 3
		 -6 2 -3 -6 7 -3 -6 7 3 -10 2 3 -10 2 -3 -10 7 -3 -10 7 3 -4 0 9 -12 0 9 -4 2 9 -12 2 9
		 -4 2 -7 -12 2 -7 -4 0 -7 -12 0 -7 -4 2 2 -12 2 2 -12 0 1.99999988 -4 0 1.99999988
		 -4 2 -2 -12 2 -2 -12 0 -2 -4 0 -2 -4.63799334 2 -2 -11.36200619 2 -2 -11.36200619 2 -5
		 -4.63799334 2 -5 -4.63799334 2 7 -11.36200619 2 7 -11.36200619 2 2 -4.63799334 2 2
		 -3.47006273 3 2 -4.63799334 3 2 -4.63799334 3 -2 -3.47006273 3 -2 -3.47006273 0 -2
		 -3.47006273 0 1.99999988 -12.52993679 3 2 -12.52993679 0 1.99999988 -12.52993679 0 -2
		 -12.52993679 3 -2 -11.36200619 3 2 -11.36200619 3 -2 -11.36200619 3 -5 -4.63799334 3 -5
		 -4.63799334 3 7 -11.36200619 3 7 -5.30366898 3 5 -10.69633102 3 5 -10.69633102 3 3
		 -5.30366898 3 3 -5.30366898 4 5 -10.69633102 4 5 -10.69633102 4 3 -5.30366898 4 3
		 -3.47006273 4 2 -4.63799334 4 2 -4.63799334 4 -2 -3.47006273 4 -2 -11.36200619 4 2
		 -11.36200619 4 -2 -12.52993679 4 2 -12.52993679 4 -2 -6.000000476837 3 2 -6.000000476837 3 -2
		 -6.000000476837 4 -2 -6.000000476837 4 2 -9.99999905 3 2 -9.99999905 3 -2 -9.99999905 4 2
		 -9.99999905 4 -2 -5 11 8.54453468 5 11 8.54453468 -5 15 8.54453468 5 15 8.54453468
		 -5 15 -8.54453468 5 15 -8.54453468 -5 11 -8.54453468 5 11 -8.54453468 -5 15 6.83562803
		 5 15 6.83562803 5 11 6.83562803 -5 11 6.83562803 -5 15 -6.83562803 5 15 -6.83562803
		 5 11 -6.83562803 -5 11 -6.83562803 5 8 -6.83562803 -5 8 -6.83562803 5 8 6.83562803
		 -5 8 6.83562803;
	setAttr -s 492 ".ed";
	setAttr ".ed[0:165]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0 3 5 0 4 6 0
		 5 7 0 6 0 0 7 1 0 6 8 1 0 9 1 8 9 0 2 10 1 9 10 0 4 11 1 10 11 0 11 8 0 8 12 0 9 13 0
		 12 13 0 10 14 0 13 14 0 11 15 0 14 15 0 15 12 0 16 48 0 18 51 0 20 50 0 22 49 0 16 18 0
		 17 19 0 20 22 0 21 23 0 22 16 0 23 17 0 18 24 0 19 25 0 21 26 0 20 27 0 23 28 0 17 29 0
		 28 29 0 26 28 0 29 25 0 22 30 0 16 31 0 30 31 0 31 24 0 27 30 0 28 32 0 29 33 0 32 33 0
		 26 34 0 34 32 0 25 35 0 35 34 0 33 35 0 30 36 0 31 37 0 36 37 0 24 38 0 37 38 0 27 39 0
		 38 39 0 39 36 0 25 40 0 26 41 0 40 41 0 35 42 0 40 42 0 34 43 0 42 43 0 41 43 0 24 44 0
		 27 45 0 44 45 0 39 46 0 45 46 0 38 47 0 47 46 0 44 47 0 48 52 0 49 53 0 50 54 0 45 41 1
		 44 40 1 51 55 0 48 49 0 49 50 0 50 45 0 44 51 0 51 48 0 52 17 0 53 23 0 54 21 0 55 19 0
		 52 53 0 53 54 0 54 41 0 40 55 0 55 52 0 56 57 0 58 59 0 60 61 0 62 63 0 56 58 0 57 59 0
		 58 64 0 59 65 0 60 62 0 61 63 0 62 71 0 63 70 0 66 57 0 67 56 0 65 66 0 66 67 0 67 64 0
		 68 60 0 69 61 0 70 66 0 71 67 0 69 70 0 70 71 0 71 68 0 68 72 0 69 73 0 61 74 0 73 74 0
		 60 75 0 75 74 0 72 75 0 58 76 0 59 77 0 76 77 0 65 78 0 77 78 0 64 79 0 76 79 0 64 80 0
		 79 81 0 80 81 1 72 82 0 81 82 0 68 83 0 83 82 1 80 83 1 71 84 0 84 83 0 67 85 0 84 85 0
		 85 80 0 65 86 0 66 87 0 86 87 0 70 88 0 88 87 0 69 89 0 89 88 0 86 89 1 78 90 0 73 91 0
		 90 91 0;
	setAttr ".ed[166:331]" 86 90 1 89 91 1 82 91 1 74 92 0 91 92 0 75 93 0 93 92 0
		 82 93 0 76 94 0 77 95 0 94 95 0 95 90 0 81 90 1 94 81 0 94 96 1 95 97 1 96 97 0 90 98 1
		 97 98 0 81 99 1 99 98 0 96 99 0 96 100 0 97 101 0 100 101 0 98 102 0 101 102 0 99 103 0
		 103 102 0 100 103 0 80 104 0 81 105 1 104 105 0 82 106 1 105 106 1 83 107 0 107 106 0
		 104 107 0 90 108 1 91 109 1 108 109 1 86 110 0 110 108 0 89 111 0 110 111 0 111 109 0
		 81 112 0 82 113 0 112 113 0 106 114 0 113 114 0 105 115 0 115 114 0 112 115 0 90 116 0
		 91 117 0 116 117 0 108 118 0 116 118 0 109 119 0 118 119 0 117 119 0 120 121 0 122 123 0
		 124 125 0 126 127 0 120 122 0 121 123 0 122 124 0 123 125 0 124 126 0 125 127 0 126 120 0
		 127 121 0 126 128 1 120 129 1 128 129 0 122 130 1 129 130 0 124 131 1 130 131 0 131 128 0
		 128 132 0 129 133 0 132 133 0 130 134 0 133 134 0 131 135 0 134 135 0 135 132 0 136 168 0
		 138 171 0 140 170 0 142 169 0 136 138 0 137 139 0 140 142 0 141 143 0 142 136 0 143 137 0
		 138 144 0 139 145 0 141 146 0 140 147 0 143 148 0 137 149 0 148 149 0 146 148 0 149 145 0
		 142 150 0 136 151 0 150 151 0 151 144 0 147 150 0 148 152 0 149 153 0 152 153 0 146 154 0
		 154 152 0 145 155 0 155 154 0 153 155 0 150 156 0 151 157 0 156 157 0 144 158 0 157 158 0
		 147 159 0 158 159 0 159 156 0 145 160 0 146 161 0 160 161 0 155 162 0 160 162 0 154 163 0
		 162 163 0 161 163 0 144 164 0 147 165 0 164 165 0 159 166 0 165 166 0 158 167 0 167 166 0
		 164 167 0 168 172 0 169 173 0 170 174 0 165 161 1 164 160 1 171 175 0 168 169 0 169 170 0
		 170 165 0 164 171 0 171 168 0 172 137 0 173 143 0 174 141 0 175 139 0 172 173 0 173 174 0
		 174 161 0 160 175 0 175 172 0;
	setAttr ".ed[332:491]" 176 177 0 178 179 0 180 181 0 182 183 0 176 178 0 177 179 0
		 178 184 0 179 185 0 180 182 0 181 183 0 182 191 0 183 190 0 186 177 0 187 176 0 185 186 0
		 186 187 0 187 184 0 188 180 0 189 181 0 190 186 0 191 187 0 189 190 0 190 191 0 191 188 0
		 188 192 0 189 193 0 181 194 0 193 194 0 180 195 0 195 194 0 192 195 0 178 196 0 179 197 0
		 196 197 0 185 198 0 197 198 0 184 199 0 196 199 0 184 200 0 199 201 0 200 201 1 192 202 0
		 201 202 0 188 203 0 203 202 1 200 203 1 191 204 0 204 203 0 187 205 0 204 205 0 205 200 0
		 185 206 0 186 207 0 206 207 0 190 208 0 208 207 0 189 209 0 209 208 0 206 209 1 198 210 0
		 193 211 0 210 211 0 206 210 1 209 211 1 202 211 1 194 212 0 211 212 0 195 213 0 213 212 0
		 202 213 0 196 214 0 197 215 0 214 215 0 215 210 0 201 210 1 214 201 0 214 216 1 215 217 1
		 216 217 0 210 218 1 217 218 0 201 219 1 219 218 0 216 219 0 216 220 0 217 221 0 220 221 0
		 218 222 0 221 222 0 219 223 0 223 222 0 220 223 0 200 224 0 201 225 1 224 225 0 202 226 1
		 225 226 1 203 227 0 227 226 0 224 227 0 210 228 1 211 229 1 228 229 1 206 230 0 230 228 0
		 209 231 0 230 231 0 231 229 0 201 232 0 202 233 0 232 233 0 226 234 0 233 234 0 225 235 0
		 235 234 0 232 235 0 210 236 0 211 237 0 236 237 0 228 238 0 236 238 0 229 239 0 238 239 0
		 237 239 0 240 241 0 242 243 0 244 245 0 246 247 0 240 242 0 241 243 0 242 248 0 243 249 0
		 244 246 0 245 247 0 246 255 0 247 254 0 248 252 0 249 253 0 250 241 0 251 240 0 248 249 1
		 249 250 1 250 251 0 251 248 1 252 244 0 253 245 0 254 250 1 255 251 1 252 253 1 253 254 1
		 254 255 0 255 252 1 254 256 0 255 257 0 256 257 0 250 258 0 256 258 0 251 259 0 258 259 0
		 257 259 0;
	setAttr -s 246 -ch 984 ".fc[0:245]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 22 24 26 27
		mu 0 4 18 19 20 21
		f 4 10 13 -15 -13
		mu 0 4 12 0 15 14
		f 4 4 15 -17 -14
		mu 0 4 0 2 16 15
		f 4 6 17 -19 -16
		mu 0 4 2 13 17 16
		f 4 8 12 -20 -18
		mu 0 4 13 12 14 17
		f 4 14 21 -23 -21
		mu 0 4 14 15 19 18
		f 4 16 23 -25 -22
		mu 0 4 15 16 20 19
		f 4 18 25 -27 -24
		mu 0 4 16 17 21 20
		f 4 19 20 -28 -26
		mu 0 4 17 14 18 21
		f 4 103 95 33 -99
		mu 0 4 22 23 24 25
		f 4 100 97 35 -97
		mu 0 4 26 27 28 29
		f 4 99 96 37 -96
		mu 0 4 30 26 29 31
		f 4 -55 -57 -59 -60
		mu 0 4 32 33 34 35
		f 4 62 64 66 67
		mu 0 4 36 37 38 39
		f 4 102 98 39 68
		mu 0 4 40 22 25 41
		f 4 -98 101 -70 -41
		mu 0 4 28 27 42 43
		f 4 -38 42 44 -44
		mu 0 4 24 44 45 46
		f 4 -36 40 45 -43
		mu 0 4 44 47 48 45
		f 4 -34 43 46 -40
		mu 0 4 25 24 46 41
		f 4 36 48 -50 -48
		mu 0 4 49 50 51 52
		f 4 32 38 -51 -49
		mu 0 4 50 53 54 51
		f 4 34 47 -52 -42
		mu 0 4 55 49 52 56
		f 4 -45 52 54 -54
		mu 0 4 46 45 33 32
		f 4 -46 55 56 -53
		mu 0 4 45 48 34 33
		f 4 -47 53 59 -58
		mu 0 4 41 46 32 35
		f 4 49 61 -63 -61
		mu 0 4 52 51 37 36
		f 4 50 63 -65 -62
		mu 0 4 51 54 38 37
		f 4 51 60 -68 -66
		mu 0 4 56 52 36 39
		f 4 -71 72 74 -76
		mu 0 4 57 58 59 60
		f 4 78 80 -83 -84
		mu 0 4 61 62 63 64
		f 4 57 71 -73 -69
		mu 0 4 41 35 59 58
		f 4 58 73 -75 -72
		mu 0 4 35 34 60 59
		f 4 -56 69 75 -74
		mu 0 4 34 48 57 60
		f 4 65 79 -81 -78
		mu 0 4 56 39 63 62
		f 4 -67 81 82 -80
		mu 0 4 39 38 64 63
		f 4 -64 76 83 -82
		mu 0 4 38 54 61 64
		f 4 31 -91 -29 -37
		mu 0 4 65 66 67 68
		f 4 30 -92 -32 -35
		mu 0 4 69 70 66 65
		f 4 -93 -31 41 77
		mu 0 4 71 70 69 72
		f 4 29 -94 -77 -39
		mu 0 4 53 73 74 54
		f 4 28 -95 -30 -33
		mu 0 4 50 75 73 53
		f 4 90 85 -100 -85
		mu 0 4 67 66 26 30
		f 4 91 86 -101 -86
		mu 0 4 66 70 27 26
		f 4 -102 -87 92 87
		mu 0 4 42 27 70 71
		f 4 -79 88 70 -88
		mu 0 4 71 74 40 42
		f 4 93 89 -103 -89
		mu 0 4 74 73 22 40
		f 4 94 84 -104 -90
		mu 0 4 73 75 23 22
		f 4 104 109 -106 -109
		mu 0 4 76 77 78 79
		f 4 168 170 -173 -174
		mu 0 4 80 81 82 83
		f 4 106 113 -108 -113
		mu 0 4 84 85 86 87
		f 4 107 115 126 -115
		mu 0 4 87 86 88 89
		f 4 125 -116 -114 -123
		mu 0 4 90 91 92 93
		f 4 114 127 121 112
		mu 0 4 94 95 96 97
		f 4 190 192 -195 -196
		mu 0 4 98 99 100 101
		f 4 -117 -119 -112 -110
		mu 0 4 77 102 103 78
		f 4 -120 116 -105 -118
		mu 0 4 104 105 106 107
		f 4 -121 117 108 110
		mu 0 4 108 109 76 79
		f 4 198 200 -203 -204
		mu 0 4 110 111 112 113
		f 4 157 -160 -162 -163
		mu 0 4 114 115 116 117
		f 4 -127 123 119 -125
		mu 0 4 89 88 105 104
		f 4 -152 153 154 149
		mu 0 4 118 119 120 121
		f 4 122 130 -132 -130
		mu 0 4 122 85 123 124
		f 4 -107 132 133 -131
		mu 0 4 85 84 125 123
		f 4 -122 128 134 -133
		mu 0 4 84 126 156 125
		f 4 105 136 -138 -136
		mu 0 4 79 78 127 128
		f 4 111 138 -140 -137
		mu 0 4 78 129 130 127
		f 4 -111 135 141 -141
		mu 0 4 157 79 128 131
		f 4 -207 -209 210 211
		mu 0 4 132 133 134 135
		f 4 -147 178 165 -169
		mu 0 4 136 137 138 81
		f 4 140 143 -145 -143
		mu 0 4 158 131 139 140
		f 4 -129 147 148 -146
		mu 0 4 141 126 142 143
		f 4 -128 150 151 -148
		mu 0 4 159 160 161 162
		f 4 124 152 -154 -151
		mu 0 4 163 164 165 166
		f 4 120 142 -155 -153
		mu 0 4 167 168 169 170
		f 4 118 156 -158 -156
		mu 0 4 171 172 173 174
		f 4 -124 158 159 -157
		mu 0 4 175 176 177 178
		f 4 -126 160 161 -159
		mu 0 4 179 180 181 182
		f 4 -139 155 166 -164
		mu 0 4 144 129 145 146
		f 4 129 164 -168 -161
		mu 0 4 122 124 147 148
		f 4 131 169 -171 -165
		mu 0 4 124 123 82 81
		f 4 -134 171 172 -170
		mu 0 4 123 125 83 82
		f 4 -135 145 173 -172
		mu 0 4 125 183 80 83
		f 4 137 175 -177 -175
		mu 0 4 128 127 149 150
		f 4 139 163 -178 -176
		mu 0 4 127 130 151 149
		f 4 -142 174 179 -144
		mu 0 4 131 128 150 137
		f 4 176 181 -183 -181
		mu 0 4 150 149 152 153
		f 4 177 183 -185 -182
		mu 0 4 149 151 154 152
		f 4 -179 185 186 -184
		mu 0 4 151 137 155 154
		f 4 -180 180 187 -186
		mu 0 4 137 150 153 155
		f 4 182 189 -191 -189
		mu 0 4 153 152 99 98
		f 4 184 191 -193 -190
		mu 0 4 152 154 100 99
		f 4 -187 193 194 -192
		mu 0 4 154 155 101 100
		f 4 -188 188 195 -194
		mu 0 4 155 153 98 101
		f 4 144 197 -199 -197
		mu 0 4 140 139 111 110
		f 4 214 216 -219 -220
		mu 0 4 184 185 186 187
		f 4 -149 201 202 -200
		mu 0 4 143 142 113 112
		f 4 -150 196 203 -202
		mu 0 4 142 140 110 113
		f 4 -223 224 226 -228
		mu 0 4 188 189 190 191
		f 4 -167 207 208 -205
		mu 0 4 146 145 134 133
		f 4 162 209 -211 -208
		mu 0 4 145 148 135 134
		f 4 167 205 -212 -210
		mu 0 4 148 147 132 135
		f 4 146 213 -215 -213
		mu 0 4 139 143 185 184
		f 4 199 215 -217 -214
		mu 0 4 143 112 186 185
		f 4 -201 217 218 -216
		mu 0 4 112 111 187 186
		f 4 -198 212 219 -218
		mu 0 4 111 139 184 187
		f 4 -166 220 222 -222
		mu 0 4 147 146 189 188
		f 4 204 223 -225 -221
		mu 0 4 146 133 190 189
		f 4 206 225 -227 -224
		mu 0 4 133 132 191 190
		f 4 -206 221 227 -226
		mu 0 4 132 147 188 191
		f 4 232 229 -234 -229
		mu 0 4 192 193 194 195
		f 4 234 230 -236 -230
		mu 0 4 193 196 197 194
		f 4 236 231 -238 -231
		mu 0 4 196 198 199 197
		f 4 238 228 -240 -232
		mu 0 4 198 200 201 199
		f 4 233 235 237 239
		mu 0 4 195 194 202 203
		f 4 -256 -255 -253 -251
		mu 0 4 204 205 206 207
		f 4 240 242 -242 -239
		mu 0 4 208 209 210 192
		f 4 241 244 -244 -233
		mu 0 4 192 210 211 193
		f 4 243 246 -246 -235
		mu 0 4 193 211 212 213
		f 4 245 247 -241 -237
		mu 0 4 213 212 209 208
		f 4 248 250 -250 -243
		mu 0 4 209 204 207 210
		f 4 249 252 -252 -245
		mu 0 4 210 207 206 211
		f 4 251 254 -254 -247
		mu 0 4 211 206 205 212
		f 4 253 255 -249 -248
		mu 0 4 212 205 204 209
		f 4 326 -262 -324 -332
		mu 0 4 214 215 216 217
		f 4 324 -264 -326 -329
		mu 0 4 218 219 220 221
		f 4 323 -266 -325 -328
		mu 0 4 222 223 219 218
		f 4 287 286 284 282
		mu 0 4 224 225 226 227
		f 4 -296 -295 -293 -291
		mu 0 4 228 229 230 231
		f 4 -297 -268 -327 -331
		mu 0 4 232 233 215 214
		f 4 268 297 -330 325
		mu 0 4 220 234 235 221
		f 4 271 -273 -271 265
		mu 0 4 216 236 237 238
		f 4 270 -274 -269 263
		mu 0 4 238 237 239 240
		f 4 267 -275 -272 261
		mu 0 4 215 233 236 216
		f 4 275 277 -277 -265
		mu 0 4 241 242 243 244
		f 4 276 278 -267 -261
		mu 0 4 244 243 245 246
		f 4 269 279 -276 -263
		mu 0 4 247 248 242 241
		f 4 281 -283 -281 272
		mu 0 4 236 224 227 237
		f 4 280 -285 -284 273
		mu 0 4 237 227 226 239
		f 4 285 -288 -282 274
		mu 0 4 233 225 224 236
		f 4 288 290 -290 -278
		mu 0 4 242 228 231 243
		f 4 289 292 -292 -279
		mu 0 4 243 231 230 245
		f 4 293 295 -289 -280
		mu 0 4 248 229 228 242
		f 4 303 -303 -301 298
		mu 0 4 249 250 251 252
		f 4 311 310 -309 -307
		mu 0 4 253 254 255 256
		f 4 296 300 -300 -286
		mu 0 4 233 252 251 225
		f 4 299 302 -302 -287
		mu 0 4 225 251 250 226
		f 4 301 -304 -298 283
		mu 0 4 226 250 249 239
		f 4 305 308 -308 -294
		mu 0 4 248 256 255 229
		f 4 307 -311 -310 294
		mu 0 4 229 255 254 230
		f 4 309 -312 -305 291
		mu 0 4 230 254 253 245
		f 4 264 256 318 -260
		mu 0 4 257 258 259 260
		f 4 262 259 319 -259
		mu 0 4 261 257 260 262
		f 4 -306 -270 258 320
		mu 0 4 263 264 261 262
		f 4 266 304 321 -258
		mu 0 4 246 245 265 266
		f 4 260 257 322 -257
		mu 0 4 244 246 266 267
		f 4 312 327 -314 -319
		mu 0 4 259 222 218 260
		f 4 313 328 -315 -320
		mu 0 4 260 218 221 262
		f 4 -316 -321 314 329
		mu 0 4 235 263 262 221
		f 4 315 -299 -317 306
		mu 0 4 263 235 232 265
		f 4 316 330 -318 -322
		mu 0 4 265 232 214 266
		f 4 317 331 -313 -323
		mu 0 4 266 214 217 267
		f 4 336 333 -338 -333
		mu 0 4 268 269 270 271
		f 4 401 400 -399 -397
		mu 0 4 272 273 274 275
		f 4 340 335 -342 -335
		mu 0 4 276 277 278 279
		f 4 342 -355 -344 -336
		mu 0 4 277 280 281 278
		f 4 350 341 343 -354
		mu 0 4 282 283 284 285
		f 4 -341 -350 -356 -343
		mu 0 4 286 287 288 289
		f 4 423 422 -421 -419
		mu 0 4 290 291 292 293
		f 4 337 339 346 344
		mu 0 4 271 270 294 295
		f 4 345 332 -345 347
		mu 0 4 296 297 298 299
		f 4 -339 -337 -346 348
		mu 0 4 300 269 268 301
		f 4 431 430 -429 -427
		mu 0 4 302 303 304 305
		f 4 390 389 387 -386
		mu 0 4 306 307 308 309
		f 4 352 -348 -352 354
		mu 0 4 280 296 299 281
		f 4 -378 -383 -382 379
		mu 0 4 310 311 312 313
		f 4 357 359 -359 -351
		mu 0 4 314 315 316 279
		f 4 358 -362 -361 334
		mu 0 4 279 316 317 276
		f 4 360 -363 -357 349
		mu 0 4 276 317 318 319
		f 4 363 365 -365 -334
		mu 0 4 269 320 321 270
		f 4 364 367 -367 -340
		mu 0 4 270 321 322 323
		f 4 368 -370 -364 338
		mu 0 4 324 325 320 269
		f 4 -440 -439 436 434
		mu 0 4 326 327 328 329
		f 4 396 -394 -407 374
		mu 0 4 330 275 331 332
		f 4 370 372 -372 -369
		mu 0 4 333 334 335 325
		f 4 373 -377 -376 356
		mu 0 4 336 337 338 319
		f 4 375 -380 -379 355
		mu 0 4 339 340 341 342
		f 4 378 381 -381 -353
		mu 0 4 343 344 345 346
		f 4 380 382 -371 -349
		mu 0 4 347 348 349 350
		f 4 383 385 -385 -347
		mu 0 4 351 352 353 354
		f 4 384 -388 -387 351
		mu 0 4 355 356 357 358
		f 4 386 -390 -389 353
		mu 0 4 359 360 361 362
		f 4 391 -395 -384 366
		mu 0 4 363 364 365 323
		f 4 388 395 -393 -358
		mu 0 4 314 366 367 315
		f 4 392 398 -398 -360
		mu 0 4 315 275 274 316
		f 4 397 -401 -400 361
		mu 0 4 316 274 273 317
		f 4 399 -402 -374 362
		mu 0 4 317 273 272 368
		f 4 402 404 -404 -366
		mu 0 4 320 369 370 321
		f 4 403 405 -392 -368
		mu 0 4 321 370 371 322
		f 4 371 -408 -403 369
		mu 0 4 325 332 369 320
		f 4 408 410 -410 -405
		mu 0 4 369 372 373 370
		f 4 409 412 -412 -406
		mu 0 4 370 373 374 371
		f 4 411 -415 -414 406
		mu 0 4 371 374 375 332
		f 4 413 -416 -409 407
		mu 0 4 332 375 372 369
		f 4 416 418 -418 -411
		mu 0 4 372 290 293 373
		f 4 417 420 -420 -413
		mu 0 4 373 293 292 374
		f 4 419 -423 -422 414
		mu 0 4 374 292 291 375
		f 4 421 -424 -417 415
		mu 0 4 375 291 290 372
		f 4 424 426 -426 -373
		mu 0 4 334 302 305 335
		f 4 447 446 -445 -443
		mu 0 4 376 377 378 379
		f 4 427 -431 -430 376
		mu 0 4 337 304 303 338
		f 4 429 -432 -425 377
		mu 0 4 338 303 302 334
		f 4 455 -455 -453 450
		mu 0 4 380 381 382 383
		f 4 432 -437 -436 394
		mu 0 4 364 329 328 365
		f 4 435 438 -438 -391
		mu 0 4 365 328 327 366
		f 4 437 439 -434 -396
		mu 0 4 366 327 326 367
		f 4 440 442 -442 -375
		mu 0 4 335 376 379 337
		f 4 441 444 -444 -428
		mu 0 4 337 379 378 304
		f 4 443 -447 -446 428
		mu 0 4 304 378 377 305
		f 4 445 -448 -441 425
		mu 0 4 305 377 376 335
		f 4 449 -451 -449 393
		mu 0 4 367 380 383 364
		f 4 448 452 -452 -433
		mu 0 4 364 383 382 329
		f 4 451 454 -454 -435
		mu 0 4 329 382 381 326
		f 4 453 -456 -450 433
		mu 0 4 326 381 380 367
		f 4 456 461 -458 -461
		mu 0 4 384 385 386 387
		f 4 480 477 -459 -477
		mu 0 4 388 389 390 391
		f 4 458 465 -460 -465
		mu 0 4 391 390 392 393
		f 4 459 467 482 -467
		mu 0 4 393 392 394 395
		f 4 481 -468 -466 -478
		mu 0 4 396 397 398 399
		f 4 466 483 476 464
		mu 0 4 400 401 402 403
		f 4 457 463 -473 -463
		mu 0 4 387 386 404 405
		f 4 -471 -474 -464 -462
		mu 0 4 385 406 407 386
		f 4 -475 470 -457 -472
		mu 0 4 408 409 410 411
		f 4 -476 471 460 462
		mu 0 4 412 413 384 387
		f 4 472 469 -481 -469
		mu 0 4 405 404 389 388
		f 4 473 -479 -482 -470
		mu 0 4 407 406 397 396
		f 4 -487 488 490 -492
		mu 0 4 414 415 416 417
		f 4 -484 479 475 468
		mu 0 4 402 401 413 412
		f 4 -483 484 486 -486
		mu 0 4 395 394 415 414
		f 4 478 487 -489 -485
		mu 0 4 394 409 416 415
		f 4 474 489 -491 -488
		mu 0 4 409 408 417 416
		f 4 -480 485 491 -490
		mu 0 4 408 395 414 417;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cubic_Head";
	rename -uid "33D79FE8-4D55-C92A-C59A-E98F83165811";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "Cubic_HeadShape" -p "Cubic_Head";
	rename -uid "B519ADF7-42E7-E579-A692-AC8539F0A901";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.41666662693023682 ;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_HeadShape1" -p "Cubic_Head";
	rename -uid "020FC530-402E-F54B-6091-93AAFFDECDCF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 12 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]" "f[8]" "f[9]" "f[10]" "f[11]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 24 ".uvst[0].uvsp[0:23]" -type "float2" 0.375 0.25 0.625
		 0.25 0.625 0.25 0.375 0.25 0.625 0.41666663 0.625 0.41666663 0.625 0.5 0.625 0.5
		 0.375 0.5 0.375 0.5 0.375 0.41666663 0.375 0.41666663 0.375 0.33333325 0.375 0.33333325
		 0.625 0.33333325 0.625 0.33333325 0.375 0.41666663 0.375 0.41666663 0.375 0.33333325
		 0.375 0.33333325 0.625 0.33333325 0.625 0.33333325 0.625 0.41666663 0.625 0.41666663;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  -12.96033287 33.1697731 13 12.96033287 33.1697731 13
		 10.14636612 33.1697731 10.17742062 -10.14636612 33.1697731 10.17742062 10.14636612 33.1697731 -5.23993778
		 12.96033287 33.1697731 -5.23993635 12.96033287 33.1697731 -12.99999905 10.14636612 33.1697731 -10.17741966
		 -12.96033287 33.1697731 -12.99999905 -10.14636612 33.1697731 -10.17741966 -12.96033287 33.1697731 -5.23993635
		 -10.14636612 33.1697731 -5.23993778 -10.14636612 33.1697731 5.23995113 -12.96033287 33.1697731 5.23995018
		 12.96033287 33.1697731 5.23995018 10.14636612 33.1697731 5.23995113 -10.14636612 37 -5.23993778
		 -12.96033287 37 -5.23993635 -12.96033287 37 5.23995018 -10.14636612 37 5.23995113
		 10.14636612 37 5.23995113 12.96033287 37 5.23995018 12.96033287 37 -5.23993635 10.14636612 37 -5.23993778;
	setAttr -s 36 ".ed[0:35]"  0 1 0 1 2 0 3 2 0 0 3 0 4 5 0 5 6 0 6 7 0
		 4 7 0 8 6 0 8 9 0 9 7 0 10 8 0 10 11 0 11 9 0 13 12 0 0 13 0 3 12 0 1 14 0 15 14 0
		 2 15 0 17 16 0 18 17 0 18 19 0 19 16 0 20 21 0 21 22 0 23 22 0 20 23 0 10 17 0 11 16 0
		 12 19 0 13 18 0 14 21 0 15 20 0 4 23 0 5 22 0;
	setAttr -s 12 -ch 48 ".fc[0:11]" -type "polyFaces" 
		f 4 0 1 -3 -4
		mu 0 4 0 1 2 3
		f 4 4 5 6 -8
		mu 0 4 4 5 6 7
		f 4 -9 9 10 -7
		mu 0 4 6 8 9 7
		f 4 -12 12 13 -10
		mu 0 4 8 10 11 9
		f 4 -15 -16 3 16
		mu 0 4 12 13 0 3
		f 4 17 -19 -20 -2
		mu 0 4 1 14 15 2
		f 4 -21 -22 22 23
		mu 0 4 16 17 18 19
		f 4 24 25 -27 -28
		mu 0 4 20 21 22 23
		f 4 -13 28 20 -30
		mu 0 4 11 10 17 16
		f 4 14 30 -23 -32
		mu 0 4 13 12 19 18
		f 4 18 32 -25 -34
		mu 0 4 15 14 21 20
		f 4 -5 34 26 -36
		mu 0 4 5 4 23 22;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 35 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[2]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[3]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[4]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[5]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[6]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[7]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[8]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[9]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[10]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[11]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[12]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[13]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[14]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[15]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[16]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[17]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[18]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[19]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[20]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[21]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[22]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[23]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[24]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[25]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[26]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[27]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[28]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[29]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[30]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[31]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[32]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[33]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[34]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_HeadShape4Orig" -p "Cubic_Head";
	rename -uid "12793DB2-44A8-2DF1-15DB-8295425609FB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Cubic_Torso";
	rename -uid "EBDF34C7-4A49-305C-1030-239AF0608763";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "Cubic_TorsoShape" -p "Cubic_Torso";
	rename -uid "38FF3758-4000-3CE1-624C-D5BA278755EC";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "Cubic_TorsoShapeOrig" -p "Cubic_Torso";
	rename -uid "80EF308C-4A7D-9C46-DF0F-1585611DA8F0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 35 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 182 ".uvst[0].uvsp[0:181]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0.375 0.25 0.625 0.25 0.625 0.5
		 0.375 0.5 0.375 0.25 0.625 0.25 0.625 0.5 0.375 0.5 0.29166675 0.25 0.375 0.33333325
		 0.375 0.33333325 0.375 0.33333325 0.625 0.33333325 0.625 0.33333325 0.625 0.33333325
		 0.70833325 0.25 0.625 0.91666675 0.70833325 0 0.29166675 0 0.375 0.91666675 0.20833337
		 0.25 0.375 0.41666663 0.375 0.41666663 0.375 0.41666663 0.625 0.41666663 0.625 0.41666663
		 0.625 0.41666663 0.79166663 0.25 0.625 0.83333337 0.79166663 0 0.20833337 0 0.375
		 0.83333337 0.375 0.41666663 0.375 0.41666663 0.375 0.33333325 0.375 0.33333325 0.625
		 0.33333325 0.625 0.33333325 0.625 0.41666663 0.625 0.41666663;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr -s 128 ".uvst[1].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1;
	setAttr ".uvst[2].uvsn" -type "string" "map12";
	setAttr -s 128 ".uvst[2].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1;
	setAttr ".uvst[3].uvsn" -type "string" "map13";
	setAttr -s 128 ".uvst[3].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1;
	setAttr ".uvst[4].uvsn" -type "string" "map14";
	setAttr -s 140 ".uvst[4].uvsp[0:139]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".uvst[5].uvsn" -type "string" "map15";
	setAttr -s 128 ".uvst[5].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1;
	setAttr ".uvst[6].uvsn" -type "string" "map16";
	setAttr -s 128 ".uvst[6].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1;
	setAttr ".uvst[7].uvsn" -type "string" "map17";
	setAttr -s 128 ".uvst[7].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1;
	setAttr ".uvst[8].uvsn" -type "string" "map18";
	setAttr -s 128 ".uvst[8].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1;
	setAttr ".uvst[9].uvsn" -type "string" "map19";
	setAttr -s 128 ".uvst[9].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1;
	setAttr ".uvst[10].uvsn" -type "string" "map110";
	setAttr -s 140 ".uvst[10].uvsp[0:139]" -type "float2" 0.375 0 0.375 0.25
		 0.625 0.25 0.625 0 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875
		 0.25 0.875 0 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".uvst[11].uvsn" -type "string" "map111";
	setAttr -s 128 ".uvst[11].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[12].uvsn" -type "string" "map112";
	setAttr -s 148 ".uvst[12].uvsp[0:147]" -type "float2" 0.375 0 0.60848302
		 1.3643329e-05 0.62962103 0.24999531 0.37136903 0.24999572 0.63103151 0.50000489 0.37088689
		 0.5000037 0.61736715 0.7499947 0.375 0.75 0.62660497 0.99999541 0.375 1 0.375 0 0.37136903
		 0.24999572 0.62962103 0.24999531 0.60848302 1.3643329e-05 0.37088689 0.5000037 0.63103151
		 0.50000489 0.375 0.75 0.61736715 0.7499947 0.375 1 0.62660497 0.99999541 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1;
	setAttr ".uvst[13].uvsn" -type "string" "map113";
	setAttr -s 152 ".uvst[13].uvsp[0:151]" -type "float2" 0.375 0 0.6193431
		 -0.0060015284 0.625 0.25 0.37603259 0.38933682 0.37729686 0.34872243 0.625 0.5 0.61946499
		 0.7426793 0.375 0.75 0.87499684 -0.026260916 0.875 0.25 0.125 0 0.12548056 0.38161317
		 0.375 0 0.37603259 0.38933682 0.625 0.25 0.6193431 -0.0060015284 0.37729686 0.34872243
		 0.375 0.75 0.61946499 0.7426793 0.625 0.5 0.875 0.25 0.87499684 -0.026260916 0.125
		 0 0.12548056 0.38161317 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".uvst[14].uvsn" -type "string" "map114";
	setAttr -s 148 ".uvst[14].uvsp[0:147]" -type "float2" 0.375 0 0.63427842
		 -0.00022588279 0.6201672 0.25008345 0.37909874 0.24983679 0.61878538 0.49991286 0.37866652
		 0.50018489 0.62929565 0.75010157 0.375 0.75 0.62499952 0.9999969 0.375 1 0.375 0
		 0.37909874 0.24983679 0.6201672 0.25008345 0.63427842 -0.00022588279 0.37866652 0.50018489
		 0.61878538 0.49991286 0.375 0.75 0.62929565 0.75010157 0.375 1 0.62499952 0.9999969
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1;
	setAttr ".uvst[15].uvsn" -type "string" "map115";
	setAttr -s 152 ".uvst[15].uvsp[0:151]" -type "float2" 0.37499043 -0.0081199696
		 0.62484956 0.0034516717 0.62577444 0.26952696 0.37500215 0.26897159 0.37500995 0.47689846
		 0.62576842 0.48323262 0.62480825 0.73139822 0.37497076 0.76523322 0.87481654 -0.023973627
		 0.87523842 0.27280691 0.12500358 -0.00066387275 0.12500387 0.26631272 0.37499043
		 -0.0081199696 0.37500215 0.26897159 0.62577444 0.26952696 0.62484956 0.0034516717
		 0.37500995 0.47689846 0.37497076 0.76523322 0.62480825 0.73139822 0.62576842 0.48323262
		 0.87523842 0.27280691 0.87481654 -0.023973627 0.12500358 -0.00066387275 0.12500387
		 0.26631272 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1;
	setAttr ".uvst[16].uvsn" -type "string" "map116";
	setAttr -s 152 ".uvst[16].uvsp[0:151]" -type "float2" 0.37500504 4.8307976e-07
		 0.62499589 -0.062499121 0.6249947 0.31249923 0.37500593 0.24999952 0.37501699 0.50000072
		 0.62498635 0.43750143 0.62499225 0.81249857 0.37501249 0.74999923 0.87499684 -0.06249886
		 0.87499553 0.31249899 0.12500305 7.9726237e-07 0.1250027 0.24999928 0.37500504 4.8307976e-07
		 0.37500593 0.24999952 0.6249947 0.31249923 0.62499589 -0.062499121 0.37501699 0.50000072
		 0.37501249 0.74999923 0.62499225 0.81249857 0.62498635 0.43750143 0.87499553 0.31249899
		 0.87499684 -0.06249886 0.12500305 7.9726237e-07 0.1250027 0.24999928 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1;
	setAttr ".uvst[17].uvsn" -type "string" "map117";
	setAttr -s 136 ".uvst[17].uvsp[0:135]" -type "float2" 0.375 0.25 0.625
		 0.25 0.625 0.5 0.375 0.5 0.375 0.25 0.375 0.5 0.625 0.5 0.625 0.25 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1;
	setAttr ".uvst[18].uvsn" -type "string" "map118";
	setAttr -s 136 ".uvst[18].uvsp[0:135]" -type "float2" 0.375 0.25 0.625
		 0.25 0.625 0.5 0.375 0.5 0.375 0.25 0.375 0.625 0.625 0.625 0.625 0.25 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1;
	setAttr ".uvst[19].uvsn" -type "string" "map119";
	setAttr -s 136 ".uvst[19].uvsp[0:135]" -type "float2" 0.375 0.25 0.625
		 0.25 0.625 0.5 0.375 0.5 0.375 0.125 0.375 0.625 0.625 0.5 0.625 0.125 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1;
	setAttr ".uvst[20].uvsn" -type "string" "map120";
	setAttr -s 136 ".uvst[20].uvsp[0:135]" -type "float2" 0.375 0.25 0.625
		 0.25 0.625 0.5 0.375 0.5 0.375 0.125 0.375 0.5 0.625 0.625 0.625 0.125 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0
		 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1;
	setAttr ".uvst[21].uvsn" -type "string" "map121";
	setAttr -s 128 ".uvst[21].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[22].uvsn" -type "string" "map122";
	setAttr -s 128 ".uvst[22].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[23].uvsn" -type "string" "map123";
	setAttr -s 128 ".uvst[23].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[24].uvsn" -type "string" "map124";
	setAttr -s 128 ".uvst[24].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[25].uvsn" -type "string" "map125";
	setAttr -s 128 ".uvst[25].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[26].uvsn" -type "string" "map126";
	setAttr -s 128 ".uvst[26].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[27].uvsn" -type "string" "map127";
	setAttr -s 128 ".uvst[27].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[28].uvsn" -type "string" "map128";
	setAttr -s 128 ".uvst[28].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[29].uvsn" -type "string" "map129";
	setAttr -s 128 ".uvst[29].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[30].uvsn" -type "string" "map130";
	setAttr -s 128 ".uvst[30].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[31].uvsn" -type "string" "map131";
	setAttr -s 128 ".uvst[31].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[32].uvsn" -type "string" "map132";
	setAttr -s 128 ".uvst[32].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[33].uvsn" -type "string" "map133";
	setAttr -s 128 ".uvst[33].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".uvst[34].uvsn" -type "string" "map134";
	setAttr -s 128 ".uvst[34].uvsp[0:127]" -type "float2" 0 0 1 0 1 1 0 1
		 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
		 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0
		 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1
		 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0
		 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1
		 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
		 0 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 160 ".vt[0:159]"  -12.96033287 15 13 12.96033287 15 13 -12.96033287 33.1697731 13
		 12.96033287 33.1697731 13 -12.96033287 33.1697731 -12.99999905 12.96033287 33.1697731 -12.99999905
		 -12.96033287 15 -12.99999905 12.96033287 15 -12.99999905 11.54031849 16.81826591 -0.5
		 13.54022312 16.83773232 -0.5 11.5111208 19.81812477 -0.5 13.51102543 19.83759117 -0.5
		 11.5111208 19.81812477 -3.5 13.51102543 19.83759117 -3.5 11.54031849 16.81826591 -3.5
		 13.54022312 16.83773232 -3.5 -11.54031849 16.81826591 -0.5 -13.54022312 16.83773232 -0.5
		 -11.5111208 19.81812477 -0.5 -13.51102543 19.83759117 -0.5 -11.5111208 19.81812477 -3.5
		 -13.51102543 19.83759117 -3.5 -11.54031849 16.81826591 -3.5 -13.54022312 16.83773232 -3.5
		 3.82710981 27.72319221 14.17924213 9.82659817 27.70745659 14.10252571 2.82993054 28.72577095 14.20089722
		 10.82924843 28.70479202 14.098607063 2.90543652 28.69300652 11.95459652 10.90475464 28.67202568 11.85230637
		 3.90261531 27.69042587 11.93294334 9.90210342 27.67469215 11.85622692 2.82172275 25.72590065 14.17429352
		 3.81983948 26.21949768 14.16594982 2.89722872 25.69313431 11.92799282 3.89534521 26.18673325 11.91964912
		 6.81316853 22.7155304 14.09654808 7.1444397 23.71774864 14.10124111 7.21994495 23.68498611 11.85494041
		 6.88867426 22.68276596 11.85024834 10.81283188 22.70504951 14.045401573 9.81565285 23.70762825 14.067056656
		 10.88833809 22.67228508 11.79910088 9.89115906 23.67486382 11.82075596 6.51527452 23.97177124 13.98004436
		 9.81467152 23.9631176 13.93785286 5.52350664 24.9393959 12.024250984 9.89633274 24.92792702 11.96833611
		 5.290658 24.97494888 14.0046043396 9.81740761 24.96307373 13.94672012 3.89958048 25.94361877 12.053919792
		 9.89906979 25.92788315 11.97720337 3.82065582 25.97876549 14.03230381 9.8201437 25.96303177 13.95558739
		 3.90231633 26.94357491 12.062786102 9.90180492 26.92784119 11.98607063 3.82339096 26.97872353 14.041172028
		 9.82287979 26.9629879 13.96445656 3.90505195 27.94353294 12.07165432 9.90454102 27.92779732 11.9949379
		 -3.82710981 27.72319221 14.17924213 -9.82659817 27.70745659 14.10252571 -10.82924843 28.70479202 14.098607063
		 -2.82993054 28.72577095 14.20089722 -10.90475464 28.67202568 11.85230637 -2.90543652 28.69300652 11.95459652
		 -9.90210342 27.67469215 11.85622692 -3.90261531 27.69042587 11.93294334 -2.82172275 25.72590065 14.17429352
		 -3.81983948 26.21949768 14.16594982 -3.89534521 26.18673325 11.91964912 -2.89722872 25.69313431 11.92799282
		 -6.81316853 22.7155304 14.09654808 -7.1444397 23.71774864 14.10124111 -7.21994495 23.68498611 11.85494041
		 -6.88867426 22.68276596 11.85024834 -10.81283188 22.70504951 14.045401573 -9.81565285 23.70762825 14.067056656
		 -9.89115906 23.67486382 11.82075596 -10.88833809 22.67228508 11.79910088 -9.81467152 23.9631176 13.93785286
		 -6.51527452 23.97177124 13.98004436 -9.89633274 24.92792702 11.96833611 -5.52350664 24.9393959 12.024250984
		 -9.81740761 24.96307373 13.94672012 -5.290658 24.97494888 14.0046043396 -9.89902687 25.92357635 11.97466373
		 -3.89953637 25.93931007 12.051383018 -3.82061124 25.9744606 14.029766083 -9.82009983 25.95872498 13.95304966
		 -9.90180492 26.92784119 11.98607063 -3.9022727 26.9392662 12.060250282 -3.82334709 26.97441483 14.0386343
		 -9.82283592 26.95868111 13.96191692 -9.90449619 27.92348862 11.99240112 -3.90505195 27.94353294 12.07165432
		 6.51381588 23.82815552 13.89542484 9.81321335 23.81950188 13.85323334 5.52204752 24.79578018 11.93963337
		 9.89487457 24.7843132 11.88371849 5.2891984 24.83133316 13.91998672 9.81594849 24.81945992 13.86210251
		 3.89812112 25.80000496 11.96930408 9.89761162 25.78426933 11.89258575 3.81919718 25.83515167 13.9476862
		 9.81868458 25.81941605 13.87096786 3.90085816 26.79996109 11.97816753 9.9003458 26.78422546 11.90145302
		 3.82193279 26.8351078 13.95655346 9.82142067 26.81937408 13.87983894 3.90359378 27.79991722 11.98703671
		 9.90308189 27.78418159 11.91031837 -6.51381588 23.82815552 13.89542484 -9.81321335 23.81950188 13.85323334
		 -9.89487457 24.7843132 11.88371849 -5.52204752 24.79578018 11.93963337 -5.28920317 24.83112335 13.92034531
		 -9.81595325 24.81925011 13.862463 -9.89757252 25.77975082 11.89040661 -3.89808178 25.79548645 11.96712399
		 -3.81918669 25.83089828 13.94505787 -9.81867409 25.81526947 13.86816311 -9.90044689 26.78438568 11.90118217
		 -3.90091705 26.7957058 11.97554207 -3.82185078 26.83096313 13.9537487 -9.82134151 26.8151207 13.87721252
		 -9.90293312 27.77992821 11.90769577 -3.90348577 27.80007744 11.98677063 -10.14636612 33.1697731 10.17742062
		 10.14636612 33.1697731 10.17742062 10.14636612 33.1697731 -10.17741966 -10.14636612 33.1697731 -10.17741966
		 -10.14636612 18 10.17742062 10.14636612 18 10.17742062 10.14636612 18 -10.17741966
		 -10.14636612 18 -10.17741966 -12.96033287 33.1697731 5.23995018 -10.14636612 33.1697731 5.23995113
		 -10.14636612 18 5.23995113 10.14636612 18 5.23995113 10.14636612 33.1697731 5.23995113
		 12.96033287 33.1697731 5.23995018 12.96033287 15 5.23995018 -12.96033287 15 5.23995018
		 -12.96033287 33.1697731 -5.23993635 -10.14636612 33.1697731 -5.23993778 -10.14636612 18 -5.23993778
		 10.14636612 18 -5.23993778 10.14636612 33.1697731 -5.23993778 12.96033287 33.1697731 -5.23993635
		 12.96033287 15 -5.23993635 -12.96033287 15 -5.23993635 -12.96033287 37 -5.23993635
		 -10.14636612 37 -5.23993778 -12.96033287 37 5.23995018 -10.14636612 37 5.23995113
		 10.14636612 37 5.23995113 12.96033287 37 5.23995018 12.96033287 37 -5.23993635 10.14636612 37 -5.23993778;
	setAttr -s 276 ".ed";
	setAttr ".ed[0:165]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 136 0 3 141 0
		 4 6 0 5 7 0 6 151 0 7 150 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0 11 13 0
		 12 14 0 13 15 0 14 8 0 15 9 0 16 17 0 18 19 0 20 21 0 22 23 0 16 18 0 17 19 0 18 20 0
		 19 21 0 20 22 0 21 23 0 22 16 0 23 17 0 24 25 0 26 27 0 28 29 0 30 31 0 24 26 0 25 27 0
		 26 28 0 27 29 0 28 30 0 29 31 0 30 24 0 31 25 0 32 33 0 34 35 0 32 26 0 33 24 0 28 34 0
		 30 35 0 34 32 0 35 33 0 32 36 0 33 37 0 35 38 0 34 39 0 36 37 0 37 38 0 38 39 0 39 36 0
		 40 41 0 42 43 0 40 36 0 41 37 0 39 42 0 38 43 0 42 40 0 43 41 0 41 25 0 40 27 0 31 43 0
		 29 42 0 44 45 0 46 47 0 44 46 0 45 47 0 48 49 0 50 51 0 48 50 0 49 51 0 52 53 0 54 55 0
		 52 54 0 53 55 0 56 57 0 58 59 0 56 58 0 57 59 0 60 61 0 61 62 0 63 62 0 60 63 0 62 64 0
		 65 64 0 63 65 0 64 66 0 67 66 0 65 67 0 66 61 0 67 60 0 68 69 0 69 60 0 68 63 0 67 70 0
		 71 70 0 65 71 0 70 69 0 71 68 0 68 72 0 72 73 0 69 73 0 73 74 0 70 74 0 74 75 0 71 75 0
		 75 72 0 76 77 0 77 73 0 76 72 0 74 78 0 79 78 0 75 79 0 78 77 0 79 76 0 76 62 0 77 61 0
		 64 79 0 66 78 0 81 80 0 80 82 0 83 82 0 81 83 0 85 84 0 84 86 0 87 86 0 85 87 0 88 89 0
		 89 90 0 91 90 0 88 91 0 92 93 0 93 94 0 95 94 0 92 95 0 44 96 0 45 97 0 96 97 0 46 98 0
		 47 99 0 98 99 0 96 98 0 97 99 0 48 100 0 49 101 0 100 101 0 50 102 0 51 103 0 102 103 0
		 100 102 0 101 103 0 52 104 0 53 105 0;
	setAttr ".ed[166:275]" 104 105 0 54 106 0 55 107 0 106 107 0 104 106 0 105 107 0
		 56 108 0 57 109 0 108 109 0 58 110 0 59 111 0 110 111 0 108 110 0 109 111 0 81 112 0
		 80 113 0 112 113 0 82 114 0 113 114 0 83 115 0 115 114 0 112 115 0 85 116 0 84 117 0
		 116 117 0 86 118 0 117 118 0 87 119 0 119 118 0 116 119 0 88 120 0 89 121 0 120 121 0
		 90 122 0 121 122 0 91 123 0 123 122 0 120 123 0 92 124 0 93 125 0 124 125 0 94 126 0
		 125 126 0 95 127 0 127 126 0 124 127 0 2 128 0 3 129 0 128 129 0 5 130 0 129 140 0
		 4 131 0 131 130 0 128 137 0 128 132 0 129 133 0 132 133 0 130 134 0 133 139 0 131 135 0
		 135 134 0 132 138 0 136 144 1 137 145 1 138 146 0 139 147 0 140 148 1 141 149 1 142 1 0
		 143 0 0 136 137 0 137 138 1 138 139 1 139 140 1 140 141 0 141 142 1 142 143 1 143 136 1
		 144 4 0 145 131 0 146 135 0 147 134 0 148 130 0 149 5 0 150 142 0 151 143 0 144 145 0
		 145 146 1 146 147 1 147 148 1 148 149 0 149 150 1 150 151 1 151 144 1 144 152 0 145 153 0
		 152 153 0 136 154 0 154 152 0 137 155 0 154 155 0 155 153 0 140 156 0 141 157 0 156 157 0
		 149 158 0 157 158 0 148 159 0 159 158 0 156 159 0;
	setAttr -s 136 -ch 544 ".fc[0:135]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 254 247 -227 -247
		mu 0 4 165 166 148 149
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 258 -11
		mu 0 4 6 7 170 173
		f 4 257 -12 -10 -250
		mu 0 4 169 171 10 11
		f 4 10 259 244 8
		mu 0 4 12 172 162 13
		f 4 12 17 -14 -17
		mu 4 4 0 1 2 3
		f 4 13 19 -15 -19
		mu 4 4 3 2 4 5
		f 4 14 21 -16 -21
		mu 4 4 5 4 6 7
		f 4 15 23 -13 -23
		mu 4 4 7 6 8 9
		f 4 -24 -22 -20 -18
		mu 4 4 1 10 11 2
		f 4 28 25 -30 -25
		mu 10 4 0 1 2 3
		f 4 30 26 -32 -26
		mu 10 4 1 4 5 2
		f 4 32 27 -34 -27
		mu 10 4 4 6 7 5
		f 4 34 24 -36 -28
		mu 10 4 6 8 9 7
		f 4 29 31 33 35
		mu 10 4 3 2 10 11
		f 4 36 41 -38 -41
		mu 12 4 0 1 2 3
		f 4 37 43 -39 -43
		mu 12 4 3 2 4 5
		f 4 38 45 -40 -45
		mu 12 4 5 4 6 7
		f 4 39 47 -37 -47
		mu 12 4 7 6 8 9
		f 4 48 51 40 -51
		mu 13 4 0 1 2 3
		f 4 44 53 -50 -53
		mu 13 4 4 5 6 7
		f 4 -56 -54 46 -52
		mu 13 4 1 8 9 2
		f 4 54 50 42 52
		mu 13 4 10 0 3 11
		f 4 56 60 -58 -49
		mu 14 4 0 1 2 3
		f 4 57 61 -59 55
		mu 14 4 3 2 4 5
		f 4 58 62 -60 49
		mu 14 4 5 4 6 7
		f 4 59 63 -57 -55
		mu 14 4 7 6 8 9
		f 4 64 67 -61 -67
		mu 15 4 0 1 2 3
		f 4 -63 69 -66 -69
		mu 15 4 4 5 6 7
		f 4 -72 -70 -62 -68
		mu 15 4 1 8 9 2
		f 4 70 66 -64 68
		mu 15 4 10 0 3 11
		f 4 -65 73 -42 -73
		mu 16 4 0 1 2 3
		f 4 -46 75 65 -75
		mu 16 4 4 5 6 7
		f 4 -71 -76 -44 -74
		mu 16 4 1 8 9 2
		f 4 71 72 -48 74
		mu 16 4 10 0 3 11
		f 4 76 79 -78 -79
		mu 17 4 0 1 2 3
		f 4 80 83 -82 -83
		mu 18 4 0 1 2 3
		f 4 84 87 -86 -87
		mu 19 4 0 1 2 3
		f 4 88 91 -90 -91
		mu 20 4 0 1 2 3
		f 4 95 94 -94 -93
		mu 12 4 10 11 12 13
		f 4 98 97 -97 -95
		mu 12 4 11 14 15 12
		f 4 101 100 -100 -98
		mu 12 4 14 16 17 15
		f 4 103 92 -103 -101
		mu 12 4 16 18 19 17
		f 4 106 -96 -106 -105
		mu 13 4 12 13 14 15
		f 4 109 108 -108 -102
		mu 13 4 16 17 18 19
		f 4 105 -104 107 110
		mu 13 4 15 14 20 21
		f 4 -110 -99 -107 -112
		mu 13 4 22 23 13 12
		f 4 104 114 -114 -113
		mu 14 4 10 11 12 13
		f 4 -111 116 -116 -115
		mu 14 4 11 14 15 12
		f 4 -109 118 -118 -117
		mu 14 4 14 16 17 15
		f 4 111 112 -120 -119
		mu 14 4 16 18 19 17
		f 4 122 113 -122 -121
		mu 15 4 12 13 14 15
		f 4 125 124 -124 117
		mu 15 4 16 17 18 19
		f 4 121 115 123 126
		mu 15 4 15 14 20 21
		f 4 -126 119 -123 -128
		mu 15 4 22 23 13 12
		f 4 129 93 -129 120
		mu 16 4 12 13 14 15
		f 4 131 -125 -131 99
		mu 16 4 16 17 18 19
		f 4 128 96 130 127
		mu 16 4 15 14 20 21
		f 4 -132 102 -130 -127
		mu 16 4 22 23 13 12
		f 4 135 134 -134 -133
		mu 17 4 4 5 6 7
		f 4 139 138 -138 -137
		mu 18 4 4 5 6 7
		f 4 143 142 -142 -141
		mu 19 4 4 5 6 7
		f 4 147 146 -146 -145
		mu 20 4 4 5 6 7
		f 4 -77 148 150 -150
		mu 0 4 14 15 16 17
		mu 1 4 0 1 2 3
		mu 2 4 0 1 2 3
		mu 3 4 0 1 2 3
		mu 4 4 12 13 14 15
		mu 5 4 0 1 2 3
		mu 6 4 0 1 2 3
		mu 7 4 0 1 2 3
		mu 8 4 0 1 2 3
		mu 9 4 0 1 2 3
		mu 10 4 12 13 14 15
		mu 11 4 0 1 2 3
		mu 12 4 20 21 22 23
		mu 13 4 24 25 26 27
		mu 14 4 20 21 22 23
		mu 15 4 24 25 26 27
		mu 16 4 24 25 26 27
		mu 17 4 8 9 10 11
		mu 18 4 8 9 10 11
		mu 19 4 8 9 10 11
		mu 20 4 8 9 10 11
		mu 21 4 0 1 2 3
		mu 22 4 0 1 2 3
		mu 23 4 0 1 2 3
		mu 24 4 0 1 2 3
		mu 25 4 0 1 2 3
		mu 26 4 0 1 2 3
		mu 27 4 0 1 2 3
		mu 28 4 0 1 2 3
		mu 29 4 0 1 2 3
		mu 30 4 0 1 2 3
		mu 31 4 0 1 2 3
		mu 32 4 0 1 2 3
		mu 33 4 0 1 2 3
		mu 34 4 0 1 2 3
		f 4 77 152 -154 -152
		mu 0 4 18 19 20 21
		mu 1 4 4 5 6 7
		mu 2 4 4 5 6 7
		mu 3 4 4 5 6 7
		mu 4 4 16 17 18 19
		mu 5 4 4 5 6 7
		mu 6 4 4 5 6 7
		mu 7 4 4 5 6 7
		mu 8 4 4 5 6 7
		mu 9 4 4 5 6 7
		mu 10 4 16 17 18 19
		mu 11 4 4 5 6 7
		mu 12 4 24 25 26 27
		mu 13 4 28 29 30 31
		mu 14 4 24 25 26 27
		mu 15 4 28 29 30 31
		mu 16 4 28 29 30 31
		mu 17 4 12 13 14 15
		mu 18 4 12 13 14 15
		mu 19 4 12 13 14 15
		mu 20 4 12 13 14 15
		mu 21 4 4 5 6 7
		mu 22 4 4 5 6 7
		mu 23 4 4 5 6 7
		mu 24 4 4 5 6 7
		mu 25 4 4 5 6 7
		mu 26 4 4 5 6 7
		mu 27 4 4 5 6 7
		mu 28 4 4 5 6 7
		mu 29 4 4 5 6 7
		mu 30 4 4 5 6 7
		mu 31 4 4 5 6 7
		mu 32 4 4 5 6 7
		mu 33 4 4 5 6 7
		mu 34 4 4 5 6 7
		f 4 78 151 -155 -149
		mu 0 4 22 23 24 25
		mu 1 4 8 9 10 11
		mu 2 4 8 9 10 11
		mu 3 4 8 9 10 11
		mu 4 4 20 21 22 23
		mu 5 4 8 9 10 11
		mu 6 4 8 9 10 11
		mu 7 4 8 9 10 11
		mu 8 4 8 9 10 11
		mu 9 4 8 9 10 11
		mu 10 4 20 21 22 23
		mu 11 4 8 9 10 11
		mu 12 4 28 29 30 31
		mu 13 4 32 33 34 35
		mu 14 4 28 29 30 31
		mu 15 4 32 33 34 35
		mu 16 4 32 33 34 35
		mu 17 4 16 17 18 19
		mu 18 4 16 17 18 19
		mu 19 4 16 17 18 19
		mu 20 4 16 17 18 19
		mu 21 4 8 9 10 11
		mu 22 4 8 9 10 11
		mu 23 4 8 9 10 11
		mu 24 4 8 9 10 11
		mu 25 4 8 9 10 11
		mu 26 4 8 9 10 11
		mu 27 4 8 9 10 11
		mu 28 4 8 9 10 11
		mu 29 4 8 9 10 11
		mu 30 4 8 9 10 11
		mu 31 4 8 9 10 11
		mu 32 4 8 9 10 11
		mu 33 4 8 9 10 11
		mu 34 4 8 9 10 11
		f 4 -80 149 155 -153
		mu 0 4 26 27 28 29
		mu 1 4 12 13 14 15
		mu 2 4 12 13 14 15
		mu 3 4 12 13 14 15
		mu 4 4 24 25 26 27
		mu 5 4 12 13 14 15
		mu 6 4 12 13 14 15
		mu 7 4 12 13 14 15
		mu 8 4 12 13 14 15
		mu 9 4 12 13 14 15
		mu 10 4 24 25 26 27
		mu 11 4 12 13 14 15
		mu 12 4 32 33 34 35
		mu 13 4 36 37 38 39
		mu 14 4 32 33 34 35
		mu 15 4 36 37 38 39
		mu 16 4 36 37 38 39
		mu 17 4 20 21 22 23
		mu 18 4 20 21 22 23
		mu 19 4 20 21 22 23
		mu 20 4 20 21 22 23
		mu 21 4 12 13 14 15
		mu 22 4 12 13 14 15
		mu 23 4 12 13 14 15
		mu 24 4 12 13 14 15
		mu 25 4 12 13 14 15
		mu 26 4 12 13 14 15
		mu 27 4 12 13 14 15
		mu 28 4 12 13 14 15
		mu 29 4 12 13 14 15
		mu 30 4 12 13 14 15
		mu 31 4 12 13 14 15
		mu 32 4 12 13 14 15
		mu 33 4 12 13 14 15
		mu 34 4 12 13 14 15
		f 4 -81 156 158 -158
		mu 0 4 30 31 32 33
		mu 1 4 16 17 18 19
		mu 2 4 16 17 18 19
		mu 3 4 16 17 18 19
		mu 4 4 28 29 30 31
		mu 5 4 16 17 18 19
		mu 6 4 16 17 18 19
		mu 7 4 16 17 18 19
		mu 8 4 16 17 18 19
		mu 9 4 16 17 18 19
		mu 10 4 28 29 30 31
		mu 11 4 16 17 18 19
		mu 12 4 36 37 38 39
		mu 13 4 40 41 42 43
		mu 14 4 36 37 38 39
		mu 15 4 40 41 42 43
		mu 16 4 40 41 42 43
		mu 17 4 24 25 26 27
		mu 18 4 24 25 26 27
		mu 19 4 24 25 26 27
		mu 20 4 24 25 26 27
		mu 21 4 16 17 18 19
		mu 22 4 16 17 18 19
		mu 23 4 16 17 18 19
		mu 24 4 16 17 18 19
		mu 25 4 16 17 18 19
		mu 26 4 16 17 18 19
		mu 27 4 16 17 18 19
		mu 28 4 16 17 18 19
		mu 29 4 16 17 18 19
		mu 30 4 16 17 18 19
		mu 31 4 16 17 18 19
		mu 32 4 16 17 18 19
		mu 33 4 16 17 18 19
		mu 34 4 16 17 18 19
		f 4 81 160 -162 -160
		mu 0 4 34 35 36 37
		mu 1 4 20 21 22 23
		mu 2 4 20 21 22 23
		mu 3 4 20 21 22 23
		mu 4 4 32 33 34 35
		mu 5 4 20 21 22 23
		mu 6 4 20 21 22 23
		mu 7 4 20 21 22 23
		mu 8 4 20 21 22 23
		mu 9 4 20 21 22 23
		mu 10 4 32 33 34 35
		mu 11 4 20 21 22 23
		mu 12 4 40 41 42 43
		mu 13 4 44 45 46 47
		mu 14 4 40 41 42 43
		mu 15 4 44 45 46 47
		mu 16 4 44 45 46 47
		mu 17 4 28 29 30 31
		mu 18 4 28 29 30 31
		mu 19 4 28 29 30 31
		mu 20 4 28 29 30 31
		mu 21 4 20 21 22 23
		mu 22 4 20 21 22 23
		mu 23 4 20 21 22 23
		mu 24 4 20 21 22 23
		mu 25 4 20 21 22 23
		mu 26 4 20 21 22 23
		mu 27 4 20 21 22 23
		mu 28 4 20 21 22 23
		mu 29 4 20 21 22 23
		mu 30 4 20 21 22 23
		mu 31 4 20 21 22 23
		mu 32 4 20 21 22 23
		mu 33 4 20 21 22 23
		mu 34 4 20 21 22 23
		f 4 82 159 -163 -157
		mu 0 4 38 39 40 41
		mu 1 4 24 25 26 27
		mu 2 4 24 25 26 27
		mu 3 4 24 25 26 27
		mu 4 4 36 37 38 39
		mu 5 4 24 25 26 27
		mu 6 4 24 25 26 27
		mu 7 4 24 25 26 27
		mu 8 4 24 25 26 27
		mu 9 4 24 25 26 27
		mu 10 4 36 37 38 39
		mu 11 4 24 25 26 27
		mu 12 4 44 45 46 47
		mu 13 4 48 49 50 51
		mu 14 4 44 45 46 47
		mu 15 4 48 49 50 51
		mu 16 4 48 49 50 51
		mu 17 4 32 33 34 35
		mu 18 4 32 33 34 35
		mu 19 4 32 33 34 35
		mu 20 4 32 33 34 35
		mu 21 4 24 25 26 27
		mu 22 4 24 25 26 27
		mu 23 4 24 25 26 27
		mu 24 4 24 25 26 27
		mu 25 4 24 25 26 27
		mu 26 4 24 25 26 27
		mu 27 4 24 25 26 27
		mu 28 4 24 25 26 27
		mu 29 4 24 25 26 27
		mu 30 4 24 25 26 27
		mu 31 4 24 25 26 27
		mu 32 4 24 25 26 27
		mu 33 4 24 25 26 27
		mu 34 4 24 25 26 27
		f 4 -84 157 163 -161
		mu 0 4 42 43 44 45
		mu 1 4 28 29 30 31
		mu 2 4 28 29 30 31
		mu 3 4 28 29 30 31
		mu 4 4 40 41 42 43
		mu 5 4 28 29 30 31
		mu 6 4 28 29 30 31
		mu 7 4 28 29 30 31
		mu 8 4 28 29 30 31
		mu 9 4 28 29 30 31
		mu 10 4 40 41 42 43
		mu 11 4 28 29 30 31
		mu 12 4 48 49 50 51
		mu 13 4 52 53 54 55
		mu 14 4 48 49 50 51
		mu 15 4 52 53 54 55
		mu 16 4 52 53 54 55
		mu 17 4 36 37 38 39
		mu 18 4 36 37 38 39
		mu 19 4 36 37 38 39
		mu 20 4 36 37 38 39
		mu 21 4 28 29 30 31
		mu 22 4 28 29 30 31
		mu 23 4 28 29 30 31
		mu 24 4 28 29 30 31
		mu 25 4 28 29 30 31
		mu 26 4 28 29 30 31
		mu 27 4 28 29 30 31
		mu 28 4 28 29 30 31
		mu 29 4 28 29 30 31
		mu 30 4 28 29 30 31
		mu 31 4 28 29 30 31
		mu 32 4 28 29 30 31
		mu 33 4 28 29 30 31
		mu 34 4 28 29 30 31
		f 4 -85 164 166 -166
		mu 0 4 46 47 48 49
		mu 1 4 32 33 34 35
		mu 2 4 32 33 34 35
		mu 3 4 32 33 34 35
		mu 4 4 44 45 46 47
		mu 5 4 32 33 34 35
		mu 6 4 32 33 34 35
		mu 7 4 32 33 34 35
		mu 8 4 32 33 34 35
		mu 9 4 32 33 34 35
		mu 10 4 44 45 46 47
		mu 11 4 32 33 34 35
		mu 12 4 52 53 54 55
		mu 13 4 56 57 58 59
		mu 14 4 52 53 54 55
		mu 15 4 56 57 58 59
		mu 16 4 56 57 58 59
		mu 17 4 40 41 42 43
		mu 18 4 40 41 42 43
		mu 19 4 40 41 42 43
		mu 20 4 40 41 42 43
		mu 21 4 32 33 34 35
		mu 22 4 32 33 34 35
		mu 23 4 32 33 34 35
		mu 24 4 32 33 34 35
		mu 25 4 32 33 34 35
		mu 26 4 32 33 34 35
		mu 27 4 32 33 34 35
		mu 28 4 32 33 34 35
		mu 29 4 32 33 34 35
		mu 30 4 32 33 34 35
		mu 31 4 32 33 34 35
		mu 32 4 32 33 34 35
		mu 33 4 32 33 34 35
		mu 34 4 32 33 34 35
		f 4 85 168 -170 -168
		mu 0 4 50 51 52 53
		mu 1 4 36 37 38 39
		mu 2 4 36 37 38 39
		mu 3 4 36 37 38 39
		mu 4 4 48 49 50 51
		mu 5 4 36 37 38 39
		mu 6 4 36 37 38 39
		mu 7 4 36 37 38 39
		mu 8 4 36 37 38 39
		mu 9 4 36 37 38 39
		mu 10 4 48 49 50 51
		mu 11 4 36 37 38 39
		mu 12 4 56 57 58 59
		mu 13 4 60 61 62 63
		mu 14 4 56 57 58 59
		mu 15 4 60 61 62 63
		mu 16 4 60 61 62 63
		mu 17 4 44 45 46 47
		mu 18 4 44 45 46 47
		mu 19 4 44 45 46 47
		mu 20 4 44 45 46 47
		mu 21 4 36 37 38 39
		mu 22 4 36 37 38 39
		mu 23 4 36 37 38 39
		mu 24 4 36 37 38 39
		mu 25 4 36 37 38 39
		mu 26 4 36 37 38 39
		mu 27 4 36 37 38 39
		mu 28 4 36 37 38 39
		mu 29 4 36 37 38 39
		mu 30 4 36 37 38 39
		mu 31 4 36 37 38 39
		mu 32 4 36 37 38 39
		mu 33 4 36 37 38 39
		mu 34 4 36 37 38 39
		f 4 86 167 -171 -165
		mu 0 4 54 55 56 57
		mu 1 4 40 41 42 43
		mu 2 4 40 41 42 43
		mu 3 4 40 41 42 43
		mu 4 4 52 53 54 55
		mu 5 4 40 41 42 43
		mu 6 4 40 41 42 43
		mu 7 4 40 41 42 43
		mu 8 4 40 41 42 43
		mu 9 4 40 41 42 43
		mu 10 4 52 53 54 55
		mu 11 4 40 41 42 43
		mu 12 4 60 61 62 63
		mu 13 4 64 65 66 67
		mu 14 4 60 61 62 63
		mu 15 4 64 65 66 67
		mu 16 4 64 65 66 67
		mu 17 4 48 49 50 51
		mu 18 4 48 49 50 51
		mu 19 4 48 49 50 51
		mu 20 4 48 49 50 51
		mu 21 4 40 41 42 43
		mu 22 4 40 41 42 43
		mu 23 4 40 41 42 43
		mu 24 4 40 41 42 43
		mu 25 4 40 41 42 43
		mu 26 4 40 41 42 43
		mu 27 4 40 41 42 43
		mu 28 4 40 41 42 43
		mu 29 4 40 41 42 43
		mu 30 4 40 41 42 43
		mu 31 4 40 41 42 43
		mu 32 4 40 41 42 43
		mu 33 4 40 41 42 43
		mu 34 4 40 41 42 43
		f 4 -88 165 171 -169
		mu 0 4 58 59 60 61
		mu 1 4 44 45 46 47
		mu 2 4 44 45 46 47
		mu 3 4 44 45 46 47
		mu 4 4 56 57 58 59
		mu 5 4 44 45 46 47
		mu 6 4 44 45 46 47
		mu 7 4 44 45 46 47
		mu 8 4 44 45 46 47
		mu 9 4 44 45 46 47
		mu 10 4 56 57 58 59
		mu 11 4 44 45 46 47
		mu 12 4 64 65 66 67
		mu 13 4 68 69 70 71
		mu 14 4 64 65 66 67
		mu 15 4 68 69 70 71
		mu 16 4 68 69 70 71
		mu 17 4 52 53 54 55
		mu 18 4 52 53 54 55
		mu 19 4 52 53 54 55
		mu 20 4 52 53 54 55
		mu 21 4 44 45 46 47
		mu 22 4 44 45 46 47
		mu 23 4 44 45 46 47
		mu 24 4 44 45 46 47
		mu 25 4 44 45 46 47
		mu 26 4 44 45 46 47
		mu 27 4 44 45 46 47
		mu 28 4 44 45 46 47
		mu 29 4 44 45 46 47
		mu 30 4 44 45 46 47
		mu 31 4 44 45 46 47
		mu 32 4 44 45 46 47
		mu 33 4 44 45 46 47
		mu 34 4 44 45 46 47
		f 4 -89 172 174 -174
		mu 0 4 62 63 64 65
		mu 1 4 48 49 50 51
		mu 2 4 48 49 50 51
		mu 3 4 48 49 50 51
		mu 4 4 60 61 62 63
		mu 5 4 48 49 50 51
		mu 6 4 48 49 50 51
		mu 7 4 48 49 50 51
		mu 8 4 48 49 50 51
		mu 9 4 48 49 50 51
		mu 10 4 60 61 62 63
		mu 11 4 48 49 50 51
		mu 12 4 68 69 70 71
		mu 13 4 72 73 74 75
		mu 14 4 68 69 70 71
		mu 15 4 72 73 74 75
		mu 16 4 72 73 74 75
		mu 17 4 56 57 58 59
		mu 18 4 56 57 58 59
		mu 19 4 56 57 58 59
		mu 20 4 56 57 58 59
		mu 21 4 48 49 50 51
		mu 22 4 48 49 50 51
		mu 23 4 48 49 50 51
		mu 24 4 48 49 50 51
		mu 25 4 48 49 50 51
		mu 26 4 48 49 50 51
		mu 27 4 48 49 50 51
		mu 28 4 48 49 50 51
		mu 29 4 48 49 50 51
		mu 30 4 48 49 50 51
		mu 31 4 48 49 50 51
		mu 32 4 48 49 50 51
		mu 33 4 48 49 50 51
		mu 34 4 48 49 50 51
		f 4 89 176 -178 -176
		mu 0 4 66 67 68 69
		mu 1 4 52 53 54 55
		mu 2 4 52 53 54 55
		mu 3 4 52 53 54 55
		mu 4 4 64 65 66 67
		mu 5 4 52 53 54 55
		mu 6 4 52 53 54 55
		mu 7 4 52 53 54 55
		mu 8 4 52 53 54 55
		mu 9 4 52 53 54 55
		mu 10 4 64 65 66 67
		mu 11 4 52 53 54 55
		mu 12 4 72 73 74 75
		mu 13 4 76 77 78 79
		mu 14 4 72 73 74 75
		mu 15 4 76 77 78 79
		mu 16 4 76 77 78 79
		mu 17 4 60 61 62 63
		mu 18 4 60 61 62 63
		mu 19 4 60 61 62 63
		mu 20 4 60 61 62 63
		mu 21 4 52 53 54 55
		mu 22 4 52 53 54 55
		mu 23 4 52 53 54 55
		mu 24 4 52 53 54 55
		mu 25 4 52 53 54 55
		mu 26 4 52 53 54 55
		mu 27 4 52 53 54 55
		mu 28 4 52 53 54 55
		mu 29 4 52 53 54 55
		mu 30 4 52 53 54 55
		mu 31 4 52 53 54 55
		mu 32 4 52 53 54 55
		mu 33 4 52 53 54 55
		mu 34 4 52 53 54 55
		f 4 90 175 -179 -173
		mu 0 4 70 71 72 73
		mu 1 4 56 57 58 59
		mu 2 4 56 57 58 59
		mu 3 4 56 57 58 59
		mu 4 4 68 69 70 71
		mu 5 4 56 57 58 59
		mu 6 4 56 57 58 59
		mu 7 4 56 57 58 59
		mu 8 4 56 57 58 59
		mu 9 4 56 57 58 59
		mu 10 4 68 69 70 71
		mu 11 4 56 57 58 59
		mu 12 4 76 77 78 79
		mu 13 4 80 81 82 83
		mu 14 4 76 77 78 79
		mu 15 4 80 81 82 83
		mu 16 4 80 81 82 83
		mu 17 4 64 65 66 67
		mu 18 4 64 65 66 67
		mu 19 4 64 65 66 67
		mu 20 4 64 65 66 67
		mu 21 4 56 57 58 59
		mu 22 4 56 57 58 59
		mu 23 4 56 57 58 59
		mu 24 4 56 57 58 59
		mu 25 4 56 57 58 59
		mu 26 4 56 57 58 59
		mu 27 4 56 57 58 59
		mu 28 4 56 57 58 59
		mu 29 4 56 57 58 59
		mu 30 4 56 57 58 59
		mu 31 4 56 57 58 59
		mu 32 4 56 57 58 59
		mu 33 4 56 57 58 59
		mu 34 4 56 57 58 59
		f 4 -92 173 179 -177
		mu 0 4 74 75 76 77
		mu 1 4 60 61 62 63
		mu 2 4 60 61 62 63
		mu 3 4 60 61 62 63
		mu 4 4 72 73 74 75
		mu 5 4 60 61 62 63
		mu 6 4 60 61 62 63
		mu 7 4 60 61 62 63
		mu 8 4 60 61 62 63
		mu 9 4 60 61 62 63
		mu 10 4 72 73 74 75
		mu 11 4 60 61 62 63
		mu 12 4 80 81 82 83
		mu 13 4 84 85 86 87
		mu 14 4 80 81 82 83
		mu 15 4 84 85 86 87
		mu 16 4 84 85 86 87
		mu 17 4 68 69 70 71
		mu 18 4 68 69 70 71
		mu 19 4 68 69 70 71
		mu 20 4 68 69 70 71
		mu 21 4 60 61 62 63
		mu 22 4 60 61 62 63
		mu 23 4 60 61 62 63
		mu 24 4 60 61 62 63
		mu 25 4 60 61 62 63
		mu 26 4 60 61 62 63
		mu 27 4 60 61 62 63
		mu 28 4 60 61 62 63
		mu 29 4 60 61 62 63
		mu 30 4 60 61 62 63
		mu 31 4 60 61 62 63
		mu 32 4 60 61 62 63
		mu 33 4 60 61 62 63
		mu 34 4 60 61 62 63
		f 4 132 181 -183 -181
		mu 0 4 78 79 80 81
		mu 1 4 64 65 66 67
		mu 2 4 64 65 66 67
		mu 3 4 64 65 66 67
		mu 4 4 76 77 78 79
		mu 5 4 64 65 66 67
		mu 6 4 64 65 66 67
		mu 7 4 64 65 66 67
		mu 8 4 64 65 66 67
		mu 9 4 64 65 66 67
		mu 10 4 76 77 78 79
		mu 11 4 64 65 66 67
		mu 12 4 84 85 86 87
		mu 13 4 88 89 90 91
		mu 14 4 84 85 86 87
		mu 15 4 88 89 90 91
		mu 16 4 88 89 90 91
		mu 17 4 72 73 74 75
		mu 18 4 72 73 74 75
		mu 19 4 72 73 74 75
		mu 20 4 72 73 74 75
		mu 21 4 64 65 66 67
		mu 22 4 64 65 66 67
		mu 23 4 64 65 66 67
		mu 24 4 64 65 66 67
		mu 25 4 64 65 66 67
		mu 26 4 64 65 66 67
		mu 27 4 64 65 66 67
		mu 28 4 64 65 66 67
		mu 29 4 64 65 66 67
		mu 30 4 64 65 66 67
		mu 31 4 64 65 66 67
		mu 32 4 64 65 66 67
		mu 33 4 64 65 66 67
		mu 34 4 64 65 66 67
		f 4 133 183 -185 -182
		mu 0 4 82 83 84 85
		mu 1 4 68 69 70 71
		mu 2 4 68 69 70 71
		mu 3 4 68 69 70 71
		mu 4 4 80 81 82 83
		mu 5 4 68 69 70 71
		mu 6 4 68 69 70 71
		mu 7 4 68 69 70 71
		mu 8 4 68 69 70 71
		mu 9 4 68 69 70 71
		mu 10 4 80 81 82 83
		mu 11 4 68 69 70 71
		mu 12 4 88 89 90 91
		mu 13 4 92 93 94 95
		mu 14 4 88 89 90 91
		mu 15 4 92 93 94 95
		mu 16 4 92 93 94 95
		mu 17 4 76 77 78 79
		mu 18 4 76 77 78 79
		mu 19 4 76 77 78 79
		mu 20 4 76 77 78 79
		mu 21 4 68 69 70 71
		mu 22 4 68 69 70 71
		mu 23 4 68 69 70 71
		mu 24 4 68 69 70 71
		mu 25 4 68 69 70 71
		mu 26 4 68 69 70 71
		mu 27 4 68 69 70 71
		mu 28 4 68 69 70 71
		mu 29 4 68 69 70 71
		mu 30 4 68 69 70 71
		mu 31 4 68 69 70 71
		mu 32 4 68 69 70 71
		mu 33 4 68 69 70 71
		mu 34 4 68 69 70 71
		f 4 -135 185 186 -184
		mu 0 4 86 87 88 89
		mu 1 4 72 73 74 75
		mu 2 4 72 73 74 75
		mu 3 4 72 73 74 75
		mu 4 4 84 85 86 87
		mu 5 4 72 73 74 75
		mu 6 4 72 73 74 75
		mu 7 4 72 73 74 75
		mu 8 4 72 73 74 75
		mu 9 4 72 73 74 75
		mu 10 4 84 85 86 87
		mu 11 4 72 73 74 75
		mu 12 4 92 93 94 95
		mu 13 4 96 97 98 99
		mu 14 4 92 93 94 95
		mu 15 4 96 97 98 99
		mu 16 4 96 97 98 99
		mu 17 4 80 81 82 83
		mu 18 4 80 81 82 83
		mu 19 4 80 81 82 83
		mu 20 4 80 81 82 83
		mu 21 4 72 73 74 75
		mu 22 4 72 73 74 75
		mu 23 4 72 73 74 75
		mu 24 4 72 73 74 75
		mu 25 4 72 73 74 75
		mu 26 4 72 73 74 75
		mu 27 4 72 73 74 75
		mu 28 4 72 73 74 75
		mu 29 4 72 73 74 75
		mu 30 4 72 73 74 75
		mu 31 4 72 73 74 75
		mu 32 4 72 73 74 75
		mu 33 4 72 73 74 75
		mu 34 4 72 73 74 75
		f 4 -136 180 187 -186
		mu 0 4 90 91 92 93
		mu 1 4 76 77 78 79
		mu 2 4 76 77 78 79
		mu 3 4 76 77 78 79
		mu 4 4 88 89 90 91
		mu 5 4 76 77 78 79
		mu 6 4 76 77 78 79
		mu 7 4 76 77 78 79
		mu 8 4 76 77 78 79
		mu 9 4 76 77 78 79
		mu 10 4 88 89 90 91
		mu 11 4 76 77 78 79
		mu 12 4 96 97 98 99
		mu 13 4 100 101 102 103
		mu 14 4 96 97 98 99
		mu 15 4 100 101 102 103
		mu 16 4 100 101 102 103
		mu 17 4 84 85 86 87
		mu 18 4 84 85 86 87
		mu 19 4 84 85 86 87
		mu 20 4 84 85 86 87
		mu 21 4 76 77 78 79
		mu 22 4 76 77 78 79
		mu 23 4 76 77 78 79
		mu 24 4 76 77 78 79
		mu 25 4 76 77 78 79
		mu 26 4 76 77 78 79
		mu 27 4 76 77 78 79
		mu 28 4 76 77 78 79
		mu 29 4 76 77 78 79
		mu 30 4 76 77 78 79
		mu 31 4 76 77 78 79
		mu 32 4 76 77 78 79
		mu 33 4 76 77 78 79
		mu 34 4 76 77 78 79
		f 4 136 189 -191 -189
		mu 0 4 94 95 96 97
		mu 1 4 80 81 82 83
		mu 2 4 80 81 82 83
		mu 3 4 80 81 82 83
		mu 4 4 92 93 94 95
		mu 5 4 80 81 82 83
		mu 6 4 80 81 82 83
		mu 7 4 80 81 82 83
		mu 8 4 80 81 82 83
		mu 9 4 80 81 82 83
		mu 10 4 92 93 94 95
		mu 11 4 80 81 82 83
		mu 12 4 100 101 102 103
		mu 13 4 104 105 106 107
		mu 14 4 100 101 102 103
		mu 15 4 104 105 106 107
		mu 16 4 104 105 106 107
		mu 17 4 88 89 90 91
		mu 18 4 88 89 90 91
		mu 19 4 88 89 90 91
		mu 20 4 88 89 90 91
		mu 21 4 80 81 82 83
		mu 22 4 80 81 82 83
		mu 23 4 80 81 82 83
		mu 24 4 80 81 82 83
		mu 25 4 80 81 82 83
		mu 26 4 80 81 82 83
		mu 27 4 80 81 82 83
		mu 28 4 80 81 82 83
		mu 29 4 80 81 82 83
		mu 30 4 80 81 82 83
		mu 31 4 80 81 82 83
		mu 32 4 80 81 82 83
		mu 33 4 80 81 82 83
		mu 34 4 80 81 82 83
		f 4 137 191 -193 -190
		mu 0 4 98 99 100 101
		mu 1 4 84 85 86 87
		mu 2 4 84 85 86 87
		mu 3 4 84 85 86 87
		mu 4 4 96 97 98 99
		mu 5 4 84 85 86 87
		mu 6 4 84 85 86 87
		mu 7 4 84 85 86 87
		mu 8 4 84 85 86 87
		mu 9 4 84 85 86 87
		mu 10 4 96 97 98 99
		mu 11 4 84 85 86 87
		mu 12 4 104 105 106 107
		mu 13 4 108 109 110 111
		mu 14 4 104 105 106 107
		mu 15 4 108 109 110 111
		mu 16 4 108 109 110 111
		mu 17 4 92 93 94 95
		mu 18 4 92 93 94 95
		mu 19 4 92 93 94 95
		mu 20 4 92 93 94 95
		mu 21 4 84 85 86 87
		mu 22 4 84 85 86 87
		mu 23 4 84 85 86 87
		mu 24 4 84 85 86 87
		mu 25 4 84 85 86 87
		mu 26 4 84 85 86 87
		mu 27 4 84 85 86 87
		mu 28 4 84 85 86 87
		mu 29 4 84 85 86 87
		mu 30 4 84 85 86 87
		mu 31 4 84 85 86 87
		mu 32 4 84 85 86 87
		mu 33 4 84 85 86 87
		mu 34 4 84 85 86 87
		f 4 -139 193 194 -192
		mu 0 4 102 103 104 105
		mu 1 4 88 89 90 91
		mu 2 4 88 89 90 91
		mu 3 4 88 89 90 91
		mu 4 4 100 101 102 103
		mu 5 4 88 89 90 91
		mu 6 4 88 89 90 91
		mu 7 4 88 89 90 91
		mu 8 4 88 89 90 91
		mu 9 4 88 89 90 91
		mu 10 4 100 101 102 103
		mu 11 4 88 89 90 91
		mu 12 4 108 109 110 111
		mu 13 4 112 113 114 115
		mu 14 4 108 109 110 111
		mu 15 4 112 113 114 115
		mu 16 4 112 113 114 115
		mu 17 4 96 97 98 99
		mu 18 4 96 97 98 99
		mu 19 4 96 97 98 99
		mu 20 4 96 97 98 99
		mu 21 4 88 89 90 91
		mu 22 4 88 89 90 91
		mu 23 4 88 89 90 91
		mu 24 4 88 89 90 91
		mu 25 4 88 89 90 91
		mu 26 4 88 89 90 91
		mu 27 4 88 89 90 91
		mu 28 4 88 89 90 91
		mu 29 4 88 89 90 91
		mu 30 4 88 89 90 91
		mu 31 4 88 89 90 91
		mu 32 4 88 89 90 91
		mu 33 4 88 89 90 91
		mu 34 4 88 89 90 91
		f 4 -140 188 195 -194
		mu 0 4 106 107 108 109
		mu 1 4 92 93 94 95
		mu 2 4 92 93 94 95
		mu 3 4 92 93 94 95
		mu 4 4 104 105 106 107
		mu 5 4 92 93 94 95
		mu 6 4 92 93 94 95
		mu 7 4 92 93 94 95
		mu 8 4 92 93 94 95
		mu 9 4 92 93 94 95
		mu 10 4 104 105 106 107
		mu 11 4 92 93 94 95
		mu 12 4 112 113 114 115
		mu 13 4 116 117 118 119
		mu 14 4 112 113 114 115
		mu 15 4 116 117 118 119
		mu 16 4 116 117 118 119
		mu 17 4 100 101 102 103
		mu 18 4 100 101 102 103
		mu 19 4 100 101 102 103
		mu 20 4 100 101 102 103
		mu 21 4 92 93 94 95
		mu 22 4 92 93 94 95
		mu 23 4 92 93 94 95
		mu 24 4 92 93 94 95
		mu 25 4 92 93 94 95
		mu 26 4 92 93 94 95
		mu 27 4 92 93 94 95
		mu 28 4 92 93 94 95
		mu 29 4 92 93 94 95
		mu 30 4 92 93 94 95
		mu 31 4 92 93 94 95
		mu 32 4 92 93 94 95
		mu 33 4 92 93 94 95
		mu 34 4 92 93 94 95
		f 4 140 197 -199 -197
		mu 0 4 110 111 112 113
		mu 1 4 96 97 98 99
		mu 2 4 96 97 98 99
		mu 3 4 96 97 98 99
		mu 4 4 108 109 110 111
		mu 5 4 96 97 98 99
		mu 6 4 96 97 98 99
		mu 7 4 96 97 98 99
		mu 8 4 96 97 98 99
		mu 9 4 96 97 98 99
		mu 10 4 108 109 110 111
		mu 11 4 96 97 98 99
		mu 12 4 116 117 118 119
		mu 13 4 120 121 122 123
		mu 14 4 116 117 118 119
		mu 15 4 120 121 122 123
		mu 16 4 120 121 122 123
		mu 17 4 104 105 106 107
		mu 18 4 104 105 106 107
		mu 19 4 104 105 106 107
		mu 20 4 104 105 106 107
		mu 21 4 96 97 98 99
		mu 22 4 96 97 98 99
		mu 23 4 96 97 98 99
		mu 24 4 96 97 98 99
		mu 25 4 96 97 98 99
		mu 26 4 96 97 98 99
		mu 27 4 96 97 98 99
		mu 28 4 96 97 98 99
		mu 29 4 96 97 98 99
		mu 30 4 96 97 98 99
		mu 31 4 96 97 98 99
		mu 32 4 96 97 98 99
		mu 33 4 96 97 98 99
		mu 34 4 96 97 98 99
		f 4 141 199 -201 -198
		mu 0 4 114 115 116 117
		mu 1 4 100 101 102 103
		mu 2 4 100 101 102 103
		mu 3 4 100 101 102 103
		mu 4 4 112 113 114 115
		mu 5 4 100 101 102 103
		mu 6 4 100 101 102 103
		mu 7 4 100 101 102 103
		mu 8 4 100 101 102 103
		mu 9 4 100 101 102 103
		mu 10 4 112 113 114 115
		mu 11 4 100 101 102 103
		mu 12 4 120 121 122 123
		mu 13 4 124 125 126 127
		mu 14 4 120 121 122 123
		mu 15 4 124 125 126 127
		mu 16 4 124 125 126 127
		mu 17 4 108 109 110 111
		mu 18 4 108 109 110 111
		mu 19 4 108 109 110 111
		mu 20 4 108 109 110 111
		mu 21 4 100 101 102 103
		mu 22 4 100 101 102 103
		mu 23 4 100 101 102 103
		mu 24 4 100 101 102 103
		mu 25 4 100 101 102 103
		mu 26 4 100 101 102 103
		mu 27 4 100 101 102 103
		mu 28 4 100 101 102 103
		mu 29 4 100 101 102 103
		mu 30 4 100 101 102 103
		mu 31 4 100 101 102 103
		mu 32 4 100 101 102 103
		mu 33 4 100 101 102 103
		mu 34 4 100 101 102 103
		f 4 -143 201 202 -200
		mu 0 4 118 119 120 121
		mu 1 4 104 105 106 107
		mu 2 4 104 105 106 107
		mu 3 4 104 105 106 107
		mu 4 4 116 117 118 119
		mu 5 4 104 105 106 107
		mu 6 4 104 105 106 107
		mu 7 4 104 105 106 107
		mu 8 4 104 105 106 107
		mu 9 4 104 105 106 107
		mu 10 4 116 117 118 119
		mu 11 4 104 105 106 107
		mu 12 4 124 125 126 127
		mu 13 4 128 129 130 131
		mu 14 4 124 125 126 127
		mu 15 4 128 129 130 131
		mu 16 4 128 129 130 131
		mu 17 4 112 113 114 115
		mu 18 4 112 113 114 115
		mu 19 4 112 113 114 115
		mu 20 4 112 113 114 115
		mu 21 4 104 105 106 107
		mu 22 4 104 105 106 107
		mu 23 4 104 105 106 107
		mu 24 4 104 105 106 107
		mu 25 4 104 105 106 107
		mu 26 4 104 105 106 107
		mu 27 4 104 105 106 107
		mu 28 4 104 105 106 107
		mu 29 4 104 105 106 107
		mu 30 4 104 105 106 107
		mu 31 4 104 105 106 107
		mu 32 4 104 105 106 107
		mu 33 4 104 105 106 107
		mu 34 4 104 105 106 107
		f 4 -144 196 203 -202
		mu 0 4 122 123 124 125
		mu 1 4 108 109 110 111
		mu 2 4 108 109 110 111
		mu 3 4 108 109 110 111
		mu 4 4 120 121 122 123
		mu 5 4 108 109 110 111
		mu 6 4 108 109 110 111
		mu 7 4 108 109 110 111
		mu 8 4 108 109 110 111
		mu 9 4 108 109 110 111
		mu 10 4 120 121 122 123
		mu 11 4 108 109 110 111
		mu 12 4 128 129 130 131
		mu 13 4 132 133 134 135
		mu 14 4 128 129 130 131
		mu 15 4 132 133 134 135
		mu 16 4 132 133 134 135
		mu 17 4 116 117 118 119
		mu 18 4 116 117 118 119
		mu 19 4 116 117 118 119
		mu 20 4 116 117 118 119
		mu 21 4 108 109 110 111
		mu 22 4 108 109 110 111
		mu 23 4 108 109 110 111
		mu 24 4 108 109 110 111
		mu 25 4 108 109 110 111
		mu 26 4 108 109 110 111
		mu 27 4 108 109 110 111
		mu 28 4 108 109 110 111
		mu 29 4 108 109 110 111
		mu 30 4 108 109 110 111
		mu 31 4 108 109 110 111
		mu 32 4 108 109 110 111
		mu 33 4 108 109 110 111
		mu 34 4 108 109 110 111
		f 4 144 205 -207 -205
		mu 0 4 126 127 128 129
		mu 1 4 112 113 114 115
		mu 2 4 112 113 114 115
		mu 3 4 112 113 114 115
		mu 4 4 124 125 126 127
		mu 5 4 112 113 114 115
		mu 6 4 112 113 114 115
		mu 7 4 112 113 114 115
		mu 8 4 112 113 114 115
		mu 9 4 112 113 114 115
		mu 10 4 124 125 126 127
		mu 11 4 112 113 114 115
		mu 12 4 132 133 134 135
		mu 13 4 136 137 138 139
		mu 14 4 132 133 134 135
		mu 15 4 136 137 138 139
		mu 16 4 136 137 138 139
		mu 17 4 120 121 122 123
		mu 18 4 120 121 122 123
		mu 19 4 120 121 122 123
		mu 20 4 120 121 122 123
		mu 21 4 112 113 114 115
		mu 22 4 112 113 114 115
		mu 23 4 112 113 114 115
		mu 24 4 112 113 114 115
		mu 25 4 112 113 114 115
		mu 26 4 112 113 114 115
		mu 27 4 112 113 114 115
		mu 28 4 112 113 114 115
		mu 29 4 112 113 114 115
		mu 30 4 112 113 114 115
		mu 31 4 112 113 114 115
		mu 32 4 112 113 114 115
		mu 33 4 112 113 114 115
		mu 34 4 112 113 114 115
		f 4 145 207 -209 -206
		mu 0 4 130 131 132 133
		mu 1 4 116 117 118 119
		mu 2 4 116 117 118 119
		mu 3 4 116 117 118 119
		mu 4 4 128 129 130 131
		mu 5 4 116 117 118 119
		mu 6 4 116 117 118 119
		mu 7 4 116 117 118 119
		mu 8 4 116 117 118 119
		mu 9 4 116 117 118 119
		mu 10 4 128 129 130 131
		mu 11 4 116 117 118 119
		mu 12 4 136 137 138 139
		mu 13 4 140 141 142 143
		mu 14 4 136 137 138 139
		mu 15 4 140 141 142 143
		mu 16 4 140 141 142 143
		mu 17 4 124 125 126 127
		mu 18 4 124 125 126 127
		mu 19 4 124 125 126 127
		mu 20 4 124 125 126 127
		mu 21 4 116 117 118 119
		mu 22 4 116 117 118 119
		mu 23 4 116 117 118 119
		mu 24 4 116 117 118 119
		mu 25 4 116 117 118 119
		mu 26 4 116 117 118 119
		mu 27 4 116 117 118 119
		mu 28 4 116 117 118 119
		mu 29 4 116 117 118 119
		mu 30 4 116 117 118 119
		mu 31 4 116 117 118 119
		mu 32 4 116 117 118 119
		mu 33 4 116 117 118 119
		mu 34 4 116 117 118 119
		f 4 -147 209 210 -208
		mu 0 4 134 135 136 137
		mu 1 4 120 121 122 123
		mu 2 4 120 121 122 123
		mu 3 4 120 121 122 123
		mu 4 4 132 133 134 135
		mu 5 4 120 121 122 123
		mu 6 4 120 121 122 123
		mu 7 4 120 121 122 123
		mu 8 4 120 121 122 123
		mu 9 4 120 121 122 123
		mu 10 4 132 133 134 135
		mu 11 4 120 121 122 123
		mu 12 4 140 141 142 143
		mu 13 4 144 145 146 147
		mu 14 4 140 141 142 143
		mu 15 4 144 145 146 147
		mu 16 4 144 145 146 147
		mu 17 4 128 129 130 131
		mu 18 4 128 129 130 131
		mu 19 4 128 129 130 131
		mu 20 4 128 129 130 131
		mu 21 4 120 121 122 123
		mu 22 4 120 121 122 123
		mu 23 4 120 121 122 123
		mu 24 4 120 121 122 123
		mu 25 4 120 121 122 123
		mu 26 4 120 121 122 123
		mu 27 4 120 121 122 123
		mu 28 4 120 121 122 123
		mu 29 4 120 121 122 123
		mu 30 4 120 121 122 123
		mu 31 4 120 121 122 123
		mu 32 4 120 121 122 123
		mu 33 4 120 121 122 123
		mu 34 4 120 121 122 123
		f 4 -148 204 211 -210
		mu 0 4 138 139 140 141
		mu 1 4 124 125 126 127
		mu 2 4 124 125 126 127
		mu 3 4 124 125 126 127
		mu 4 4 136 137 138 139
		mu 5 4 124 125 126 127
		mu 6 4 124 125 126 127
		mu 7 4 124 125 126 127
		mu 8 4 124 125 126 127
		mu 9 4 124 125 126 127
		mu 10 4 136 137 138 139
		mu 11 4 124 125 126 127
		mu 12 4 144 145 146 147
		mu 13 4 148 149 150 151
		mu 14 4 144 145 146 147
		mu 15 4 148 149 150 151
		mu 16 4 148 149 150 151
		mu 17 4 132 133 134 135
		mu 18 4 132 133 134 135
		mu 19 4 132 133 134 135
		mu 20 4 132 133 134 135
		mu 21 4 124 125 126 127
		mu 22 4 124 125 126 127
		mu 23 4 124 125 126 127
		mu 24 4 124 125 126 127
		mu 25 4 124 125 126 127
		mu 26 4 124 125 126 127
		mu 27 4 124 125 126 127
		mu 28 4 124 125 126 127
		mu 29 4 124 125 126 127
		mu 30 4 124 125 126 127
		mu 31 4 124 125 126 127
		mu 32 4 124 125 126 127
		mu 33 4 124 125 126 127
		mu 34 4 124 125 126 127
		f 4 -151 154 153 -156
		mu 0 4 17 25 21 29
		mu 1 4 3 11 7 15
		mu 2 4 3 11 7 15
		mu 3 4 3 11 7 15
		mu 4 4 15 23 19 27
		mu 5 4 3 11 7 15
		mu 6 4 3 11 7 15
		mu 7 4 3 11 7 15
		mu 8 4 3 11 7 15
		mu 9 4 3 11 7 15
		mu 10 4 15 23 19 27
		mu 11 4 3 11 7 15
		mu 12 4 23 31 27 35
		mu 13 4 27 35 31 39
		mu 14 4 23 31 27 35
		mu 15 4 27 35 31 39
		mu 16 4 27 35 31 39
		mu 17 4 11 19 15 23
		mu 18 4 11 19 15 23
		mu 19 4 11 19 15 23
		mu 20 4 11 19 15 23
		mu 21 4 3 11 7 15
		mu 22 4 3 11 7 15
		mu 23 4 3 11 7 15
		mu 24 4 3 11 7 15
		mu 25 4 3 11 7 15
		mu 26 4 3 11 7 15
		mu 27 4 3 11 7 15
		mu 28 4 3 11 7 15
		mu 29 4 3 11 7 15
		mu 30 4 3 11 7 15
		mu 31 4 3 11 7 15
		mu 32 4 3 11 7 15
		mu 33 4 3 11 7 15
		mu 34 4 3 11 7 15
		f 4 -159 162 161 -164
		mu 0 4 33 41 37 45
		mu 1 4 19 27 23 31
		mu 2 4 19 27 23 31
		mu 3 4 19 27 23 31
		mu 4 4 31 39 35 43
		mu 5 4 19 27 23 31
		mu 6 4 19 27 23 31
		mu 7 4 19 27 23 31
		mu 8 4 19 27 23 31
		mu 9 4 19 27 23 31
		mu 10 4 31 39 35 43
		mu 11 4 19 27 23 31
		mu 12 4 39 47 43 51
		mu 13 4 43 51 47 55
		mu 14 4 39 47 43 51
		mu 15 4 43 51 47 55
		mu 16 4 43 51 47 55
		mu 17 4 27 35 31 39
		mu 18 4 27 35 31 39
		mu 19 4 27 35 31 39
		mu 20 4 27 35 31 39
		mu 21 4 19 27 23 31
		mu 22 4 19 27 23 31
		mu 23 4 19 27 23 31
		mu 24 4 19 27 23 31
		mu 25 4 19 27 23 31
		mu 26 4 19 27 23 31
		mu 27 4 19 27 23 31
		mu 28 4 19 27 23 31
		mu 29 4 19 27 23 31
		mu 30 4 19 27 23 31
		mu 31 4 19 27 23 31
		mu 32 4 19 27 23 31
		mu 33 4 19 27 23 31
		mu 34 4 19 27 23 31
		f 4 -167 170 169 -172
		mu 0 4 49 57 53 61
		mu 1 4 35 43 39 47
		mu 2 4 35 43 39 47
		mu 3 4 35 43 39 47
		mu 4 4 47 55 51 59
		mu 5 4 35 43 39 47
		mu 6 4 35 43 39 47
		mu 7 4 35 43 39 47
		mu 8 4 35 43 39 47
		mu 9 4 35 43 39 47
		mu 10 4 47 55 51 59
		mu 11 4 35 43 39 47
		mu 12 4 55 63 59 67
		mu 13 4 59 67 63 71
		mu 14 4 55 63 59 67
		mu 15 4 59 67 63 71
		mu 16 4 59 67 63 71
		mu 17 4 43 51 47 55
		mu 18 4 43 51 47 55
		mu 19 4 43 51 47 55
		mu 20 4 43 51 47 55
		mu 21 4 35 43 39 47
		mu 22 4 35 43 39 47
		mu 23 4 35 43 39 47
		mu 24 4 35 43 39 47
		mu 25 4 35 43 39 47
		mu 26 4 35 43 39 47
		mu 27 4 35 43 39 47
		mu 28 4 35 43 39 47
		mu 29 4 35 43 39 47
		mu 30 4 35 43 39 47
		mu 31 4 35 43 39 47
		mu 32 4 35 43 39 47
		mu 33 4 35 43 39 47
		mu 34 4 35 43 39 47
		f 4 -175 178 177 -180
		mu 0 4 65 73 69 77
		mu 1 4 51 59 55 63
		mu 2 4 51 59 55 63
		mu 3 4 51 59 55 63
		mu 4 4 63 71 67 75
		mu 5 4 51 59 55 63
		mu 6 4 51 59 55 63
		mu 7 4 51 59 55 63
		mu 8 4 51 59 55 63
		mu 9 4 51 59 55 63
		mu 10 4 63 71 67 75
		mu 11 4 51 59 55 63
		mu 12 4 71 79 75 83
		mu 13 4 75 83 79 87
		mu 14 4 71 79 75 83
		mu 15 4 75 83 79 87
		mu 16 4 75 83 79 87
		mu 17 4 59 67 63 71
		mu 18 4 59 67 63 71
		mu 19 4 59 67 63 71
		mu 20 4 59 67 63 71
		mu 21 4 51 59 55 63
		mu 22 4 51 59 55 63
		mu 23 4 51 59 55 63
		mu 24 4 51 59 55 63
		mu 25 4 51 59 55 63
		mu 26 4 51 59 55 63
		mu 27 4 51 59 55 63
		mu 28 4 51 59 55 63
		mu 29 4 51 59 55 63
		mu 30 4 51 59 55 63
		mu 31 4 51 59 55 63
		mu 32 4 51 59 55 63
		mu 33 4 51 59 55 63
		mu 34 4 51 59 55 63
		f 4 184 -187 -188 182
		mu 0 4 85 89 93 81
		mu 1 4 71 75 79 67
		mu 2 4 71 75 79 67
		mu 3 4 71 75 79 67
		mu 4 4 83 87 91 79
		mu 5 4 71 75 79 67
		mu 6 4 71 75 79 67
		mu 7 4 71 75 79 67
		mu 8 4 71 75 79 67
		mu 9 4 71 75 79 67
		mu 10 4 83 87 91 79
		mu 11 4 71 75 79 67
		mu 12 4 91 95 99 87
		mu 13 4 95 99 103 91
		mu 14 4 91 95 99 87
		mu 15 4 95 99 103 91
		mu 16 4 95 99 103 91
		mu 17 4 79 83 87 75
		mu 18 4 79 83 87 75
		mu 19 4 79 83 87 75
		mu 20 4 79 83 87 75
		mu 21 4 71 75 79 67
		mu 22 4 71 75 79 67
		mu 23 4 71 75 79 67
		mu 24 4 71 75 79 67
		mu 25 4 71 75 79 67
		mu 26 4 71 75 79 67
		mu 27 4 71 75 79 67
		mu 28 4 71 75 79 67
		mu 29 4 71 75 79 67
		mu 30 4 71 75 79 67
		mu 31 4 71 75 79 67
		mu 32 4 71 75 79 67
		mu 33 4 71 75 79 67
		mu 34 4 71 75 79 67
		f 4 192 -195 -196 190
		mu 0 4 101 105 109 97
		mu 1 4 87 91 95 83
		mu 2 4 87 91 95 83
		mu 3 4 87 91 95 83
		mu 4 4 99 103 107 95
		mu 5 4 87 91 95 83
		mu 6 4 87 91 95 83
		mu 7 4 87 91 95 83
		mu 8 4 87 91 95 83
		mu 9 4 87 91 95 83
		mu 10 4 99 103 107 95
		mu 11 4 87 91 95 83
		mu 12 4 107 111 115 103
		mu 13 4 111 115 119 107
		mu 14 4 107 111 115 103
		mu 15 4 111 115 119 107
		mu 16 4 111 115 119 107
		mu 17 4 95 99 103 91
		mu 18 4 95 99 103 91
		mu 19 4 95 99 103 91
		mu 20 4 95 99 103 91
		mu 21 4 87 91 95 83
		mu 22 4 87 91 95 83
		mu 23 4 87 91 95 83
		mu 24 4 87 91 95 83
		mu 25 4 87 91 95 83
		mu 26 4 87 91 95 83
		mu 27 4 87 91 95 83
		mu 28 4 87 91 95 83
		mu 29 4 87 91 95 83
		mu 30 4 87 91 95 83
		mu 31 4 87 91 95 83
		mu 32 4 87 91 95 83
		mu 33 4 87 91 95 83
		mu 34 4 87 91 95 83
		f 4 200 -203 -204 198
		mu 0 4 117 121 125 113
		mu 1 4 103 107 111 99
		mu 2 4 103 107 111 99
		mu 3 4 103 107 111 99
		mu 4 4 115 119 123 111
		mu 5 4 103 107 111 99
		mu 6 4 103 107 111 99
		mu 7 4 103 107 111 99
		mu 8 4 103 107 111 99
		mu 9 4 103 107 111 99
		mu 10 4 115 119 123 111
		mu 11 4 103 107 111 99
		mu 12 4 123 127 131 119
		mu 13 4 127 131 135 123
		mu 14 4 123 127 131 119
		mu 15 4 127 131 135 123
		mu 16 4 127 131 135 123
		mu 17 4 111 115 119 107
		mu 18 4 111 115 119 107
		mu 19 4 111 115 119 107
		mu 20 4 111 115 119 107
		mu 21 4 103 107 111 99
		mu 22 4 103 107 111 99
		mu 23 4 103 107 111 99
		mu 24 4 103 107 111 99
		mu 25 4 103 107 111 99
		mu 26 4 103 107 111 99
		mu 27 4 103 107 111 99
		mu 28 4 103 107 111 99
		mu 29 4 103 107 111 99
		mu 30 4 103 107 111 99
		mu 31 4 103 107 111 99
		mu 32 4 103 107 111 99
		mu 33 4 103 107 111 99
		mu 34 4 103 107 111 99
		f 4 208 -211 -212 206
		mu 0 4 133 137 141 129
		mu 1 4 119 123 127 115
		mu 2 4 119 123 127 115
		mu 3 4 119 123 127 115
		mu 4 4 131 135 139 127
		mu 5 4 119 123 127 115
		mu 6 4 119 123 127 115
		mu 7 4 119 123 127 115
		mu 8 4 119 123 127 115
		mu 9 4 119 123 127 115
		mu 10 4 131 135 139 127
		mu 11 4 119 123 127 115
		mu 12 4 139 143 147 135
		mu 13 4 143 147 151 139
		mu 14 4 139 143 147 135
		mu 15 4 143 147 151 139
		mu 16 4 143 147 151 139
		mu 17 4 127 131 135 123
		mu 18 4 127 131 135 123
		mu 19 4 127 131 135 123
		mu 20 4 127 131 135 123
		mu 21 4 119 123 127 115
		mu 22 4 119 123 127 115
		mu 23 4 119 123 127 115
		mu 24 4 119 123 127 115
		mu 25 4 119 123 127 115
		mu 26 4 119 123 127 115
		mu 27 4 119 123 127 115
		mu 28 4 119 123 127 115
		mu 29 4 119 123 127 115
		mu 30 4 119 123 127 115
		mu 31 4 119 123 127 115
		mu 32 4 119 123 127 115
		mu 33 4 119 123 127 115
		mu 34 4 119 123 127 115
		f 4 1 213 -215 -213
		mu 0 4 2 3 143 142
		f 4 256 249 215 -249
		mu 0 4 167 168 5 144
		f 4 -3 217 218 -216
		mu 0 4 5 4 145 144
		f 4 -245 252 245 -218
		mu 0 4 4 163 164 145
		f 4 214 221 -223 -221
		mu 0 4 142 143 147 146
		f 4 255 248 223 -248
		mu 0 4 166 167 144 148
		f 4 -219 225 226 -224
		mu 0 4 144 145 149 148
		f 4 -246 253 246 -226
		mu 0 4 145 164 165 149
		f 4 -237 -7 212 219
		mu 0 4 152 151 2 142
		f 4 -238 -220 220 227
		mu 0 4 153 152 142 146
		f 4 222 224 -239 -228
		mu 0 4 146 147 154 153
		f 4 216 -240 -225 -222
		mu 0 4 143 155 154 147
		f 4 7 -241 -217 -214
		mu 0 4 3 156 155 143
		f 4 -235 -242 -8 -6
		mu 0 4 1 159 157 3
		f 4 -243 234 -1 -236
		mu 0 4 161 158 9 8
		f 4 -244 235 4 6
		mu 0 4 150 160 0 2
		f 4 -263 -265 266 267
		mu 0 4 174 175 176 177
		f 4 -254 -230 237 230
		mu 0 4 165 164 152 153
		f 4 238 231 -255 -231
		mu 0 4 153 154 166 165
		f 4 239 232 -256 -232
		mu 0 4 154 155 167 166
		f 4 270 272 -275 -276
		mu 0 4 178 179 180 181
		f 4 241 -251 -258 -234
		mu 0 4 157 159 171 169
		f 4 -259 250 242 -252
		mu 0 4 173 170 158 161
		f 4 -260 251 243 228
		mu 0 4 162 172 160 150
		f 4 -253 260 262 -262
		mu 0 4 164 163 175 174
		f 4 -229 263 264 -261
		mu 0 4 163 151 176 175
		f 4 236 265 -267 -264
		mu 0 4 151 152 177 176
		f 4 229 261 -268 -266
		mu 0 4 152 164 174 177
		f 4 240 269 -271 -269
		mu 0 4 155 156 179 178
		f 4 233 271 -273 -270
		mu 0 4 156 168 180 179
		f 4 -257 273 274 -272
		mu 0 4 168 167 181 180
		f 4 -233 268 275 -274
		mu 0 4 167 155 178 181;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr -s 35 ".pd";
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[1]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[2]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[3]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[4]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[5]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[6]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[7]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[8]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[9]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[10]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[11]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[12]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[13]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[14]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[15]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[16]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[17]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[18]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[19]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[20]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[21]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[22]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[23]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[24]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[25]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[26]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[27]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[28]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[29]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[30]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[31]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[32]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[33]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".pd[34]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Dummy";
	rename -uid "23D1868C-4360-AED3-3615-4990FB549874";
	setAttr ".t" -type "double3" 0 18.121340480344969 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "DummyShape" -p "Dummy";
	rename -uid "7E07C6CA-43A3-3BA5-20C7-12847082F906";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr -av ".iog[0].og[0].gid";
	setAttr -av ".iog[0].og[1].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "DummyShapeOrig" -p "Dummy";
	rename -uid "E1729850-44F8-49C6-CBF8-308E023C58EC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "8DC875E9-4A1E-67B5-2BE5-D38E575ED106";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "3FC1E43C-4DCA-E90B-6079-11A8EE8EC83E";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "1FC3D64C-4724-AC98-F908-63A64B725154";
createNode displayLayerManager -n "layerManager";
	rename -uid "6CF2F949-4668-2642-D3C9-B3B64AA11B31";
	setAttr -s 3 ".dli";
	setAttr ".dli[3]" 1;
	setAttr ".dli[4]" 2;
createNode displayLayer -n "defaultLayer";
	rename -uid "4085AE31-4CEA-0975-1FB7-E181F8E20DAE";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "CE578C59-45FD-6664-B8B9-4391911F1BE0";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "1D0F1E37-4A8A-A123-D6A9-679337D5783E";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "5CB01404-49E4-D27F-80CC-5590DD35CDFF";
	setAttr ".w" 10;
	setAttr ".h" 8;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube2";
	rename -uid "80215462-4720-5811-BDBF-3CABF39A8D0D";
	setAttr ".w" 6;
	setAttr ".h" 8;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube3";
	rename -uid "485774BA-41D5-6B47-1A0B-2D9BCAEA4729";
	setAttr ".w" 7;
	setAttr ".h" 6;
	setAttr ".d" 7;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "54C76ABA-4E87-6A38-5E51-09801C8F20AE";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1 0 1;
createNode polyCube -n "polyCube4";
	rename -uid "8D8EA23B-4BD4-C7A9-6418-8BB72A958F8A";
	setAttr ".w" 8;
	setAttr ".h" 5;
	setAttr ".d" 5;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry2";
	rename -uid "2667C77D-4AE1-F49D-EB9A-B89717893460";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 0 0 1;
createNode transformGeometry -n "transformGeometry3";
	rename -uid "CBBB4418-47D3-B1D5-C7F1-B88061D1E0DC";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 -8 -0.5 1;
createNode transformGeometry -n "transformGeometry4";
	rename -uid "1581101F-4397-622B-69AB-43A374D0BF73";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.70710678118654746 0.70710678118654768 0
		 0 -0.70710678118654768 0.70710678118654746 0 8 -5 -2 1;
createNode polyCube -n "polyCube5";
	rename -uid "254C7E88-43BD-5F96-FE35-E5BABE340230";
	setAttr ".w" 10;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube6";
	rename -uid "F9C7DB55-488F-B8D7-CF89-0D81FA44309D";
	setAttr ".w" 7;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "3E1FE795-4A57-0096-2DC6-70B3AE2BE52A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1118\n            -height 716\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n"
		+ "                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n"
		+ "                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n"
		+ "\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1118\\n    -height 716\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1118\\n    -height 716\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D359CC2C-4330-3F25-4DC2-51AEBFAC6971";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 120 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube7";
	rename -uid "559D9D51-47BD-434F-417F-9FAC6505AF5B";
	setAttr ".w" 20;
	setAttr ".h" 4;
	setAttr ".d" 18;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry5";
	rename -uid "E9E077C0-4E29-FA31-7748-D19AC1391C4C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry6";
	rename -uid "EC9EFFAE-46F3-4480-4BB4-D68D307AACDF";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry7";
	rename -uid "E2F4CE49-4274-17CB-7130-C7AE84E9341C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry8";
	rename -uid "6631E2D1-4860-AEF4-DB1C-7098D054CA4A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry9";
	rename -uid "1163723F-4DD1-B842-E152-5CAC6D624D8C";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-16 -2.7755575615628914e-17 1 0
		 -0.26976023601264199 0.96292752326766717 5.5511151231257827e-17 0 -0.96292752326766728 -0.26976023601264193 1.1102230246251565e-16 0
		 12 -20 0 1;
createNode transformGeometry -n "transformGeometry10";
	rename -uid "36814056-4C2E-DE67-205C-B98EC7CE1DB7";
	setAttr ".txf" -type "matrix" 0.88294759285892688 -1.3877787807814457e-16 -0.46947156278589086 0
		 -0.1266447595783457 0.96292752326766717 -0.23818415103641816 0 0.45206708919801941 0.26976023601264182 0.85021453876679487 0
		 8.6269065389189556 -20.005126449443157 7.0516877289946809 1;
createNode polyCube -n "polyCube8";
	rename -uid "6738B210-4080-16FE-2F51-0AA44FB0D2B0";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube9";
	rename -uid "C40BE38B-4975-7B08-D713-59897C1E67F0";
	setAttr ".w" 13;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube10";
	rename -uid "398A8CF3-423D-51BB-DFD7-16A4761E016E";
	setAttr ".w" 14;
	setAttr ".h" 10;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak1";
	rename -uid "C313D164-4FA5-DC5A-DFFE-60BC6EF213E9";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  7 -5 -1 7 -5 -1 7 -5 -1 7
		 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1;
createNode transformGeometry -n "transformGeometry11";
	rename -uid "1431F1F0-4C93-7C38-38B9-92BD65356955";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 0.080198924328858931 0
		 0 -0.080198924328858931 0.99677887845624713 0 -1 -3.0499999999999949 0.79999999999998117 1;
createNode transformGeometry -n "transformGeometry12";
	rename -uid "60349C85-4613-42DE-5B69-56B668AF13BA";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018336 0 0 1 0 0
		 0.38461756040018336 0 0.92307601649691418 0 0.99999999999999778 0 13 1;
createNode polyCube -n "polyCube11";
	rename -uid "ED30530E-4D51-D1A7-9F47-92AD1D4F4E58";
	setAttr ".w" 7;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube12";
	rename -uid "9A96DE03-43F3-3B18-9C72-418ED6F3B382";
	setAttr ".w" 14;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak2";
	rename -uid "33334318-4531-A1A4-0802-7CACC4E28DA7";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093;
createNode transformGeometry -n "transformGeometry13";
	rename -uid "B0B2AE7A-4047-F186-D1F6-F599466439B3";
	setAttr ".txf" -type "matrix" 0.85206933223181147 -0.41082766406220717 0.32434315702851696 0
		 0.38461756040018319 0.91171141897700703 0.14440090283216203 0 -0.35503124552896226 0.0017084929389307166 0.93485287385236782 0
		 -1.3 0.015643446504023089 -0.098768834059513783 1;
createNode polyCube -n "polyCube13";
	rename -uid "9563A64B-43D0-45BF-39B9-E6A5C9C06430";
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube14";
	rename -uid "EF19D4A1-4F7E-B8CD-8282-388E4678AAE7";
	setAttr ".w" 13;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak3";
	rename -uid "6D576173-45BA-8565-1798-84B6B017B978";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -3.5 -0.5 6.5 -3.5 -0.5
		 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5;
createNode transformGeometry -n "transformGeometry14";
	rename -uid "73F384E1-440B-A631-EE57-2D97854B1320";
	setAttr ".txf" -type "matrix" 0.96126169593831889 0 -0.27563735581699905 0 0 1 0 0
		 0.27563735581699905 0 0.96126169593831889 0 0.5 -12.499999999999998 11.5 1;
createNode polyCube -n "polyCube15";
	rename -uid "1888269C-410C-44B7-4915-4ABB8EDA545D";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry15";
	rename -uid "87CD74CE-4C81-39F2-7622-37ABB979F889";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-16 0 -1 0 0 1 0 0 1 0 2.2204460492503131e-16 0
		 11.499999999999979 -7.5 -0.99999999999999012 1;
createNode polyCube -n "polyCube16";
	rename -uid "6EE74D4C-4E8E-AA70-C5C7-5C8BC768DB57";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube17";
	rename -uid "8878502D-46DA-DDA8-1039-B7A7CD249DD4";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry16";
	rename -uid "4C0842CC-4A13-91C5-95FB-49AA6B865820";
	setAttr ".txf" -type "matrix" 0.94865541582805746 -0.31631140039539413 0 0 0.31631140039539413 0.94865541582805746 0 0
		 0 0 1 0 12.525672292085968 -14.841844299802286 -4 1;
createNode polyCube -n "polyCube18";
	rename -uid "EECFD6F9-4BD1-ACB9-F770-6C938581FF5A";
	setAttr ".w" 8;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube19";
	rename -uid "832ECCF9-41FE-590A-C000-199E07CA5835";
	setAttr ".h" 2;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube20";
	rename -uid "E381CF92-4DBB-5E80-EEE7-75B9B372EC9E";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube21";
	rename -uid "F962A912-4E24-4930-03AF-A68648612EE2";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube22";
	rename -uid "B892D7B7-4364-18F0-F668-B680F093092C";
	setAttr ".w" 5;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube23";
	rename -uid "9CFE9CB5-48DE-C5EF-AAC1-0C8AE95CE1EB";
	setAttr ".w" 6;
	setAttr ".h" 0.01;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry17";
	rename -uid "BB4F6B63-4859-0B98-652A-678E093BA62F";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4 -0.5 -1 1;
createNode transformGeometry -n "transformGeometry18";
	rename -uid "0EB95C3C-49D2-7B4E-B5B5-C28661B0243D";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 -2 -1 1;
createNode transformGeometry -n "transformGeometry19";
	rename -uid "5746B87C-4BF9-75CB-795B-EAB11D3E005E";
	setAttr ".txf" -type "matrix" 0.79999892814850848 -0.60000142913266252 0 0 0.60000142913266252 0.79999892814850848 0 0
		 0 0 1 0 2.2999980349376026 -4.1000041087574015 -1 1;
createNode transformGeometry -n "transformGeometry20";
	rename -uid "D1E40504-4CF2-BF08-21AB-188785E56B1B";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-16 1 0 0 -1 2.2204460492503131e-16 0 0
		 0 0 1 0 5.999999999999992 -5.4999999999999991 -1 1;
createNode transformGeometry -n "transformGeometry21";
	rename -uid "BD7A6EFF-4F93-AF13-B36D-CE840CFC740E";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 7.5 -3 -1 1;
createNode transformGeometry -n "transformGeometry22";
	rename -uid "606417B4-49C7-E882-F47A-6ABF96B731AD";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.86602540378443871 0.49999999999999994 0
		 0 -0.49999999999999994 0.86602540378443871 0 4 -1.25 -1 1;
createNode transformGeometry -n "transformGeometry23";
	rename -uid "29400EF9-4901-32F6-5A09-CF8C6B014F7D";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -17 -1 1;
createNode polyTweak -n "polyTweak4";
	rename -uid "29CDD8A8-4D14-BEF2-2931-BCA13CBF62C2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[0:3]" -type "float3"  -1 0 0 1 0 0 -1 0 0 1 0 0;
createNode transformGeometry -n "transformGeometry24";
	rename -uid "D72E8F73-4A5F-98E7-E544-84B95DC0C4D9";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -7.5 -11.5 1;
createNode transformGeometry -n "transformGeometry25";
	rename -uid "828F6D3A-4037-5063-5122-58BFD6251C9C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1.5 11.5 1;
createNode polyTweak -n "polyTweak5";
	rename -uid "8B3D4378-48B5-81F5-78E3-438A83A3390F";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -1.5 -1.5 6.5 -1.5 -1.5
		 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5;
createNode transformGeometry -n "transformGeometry26";
	rename -uid "5596D9A9-47D5-B5B8-AF0E-47B82DB0EB97";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018324 0 0 1 0 0
		 0.38461756040018324 0 0.92307601649691418 0 0.99999999999999978 0 13 1;
createNode transformGeometry -n "transformGeometry27";
	rename -uid "2BDC3B3E-486C-863E-3B6D-4BACA1607B15";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-16 0 0 1 0 0 -1.1102230246251565e-16 0 1 0
		 -2.2204460492503131e-16 0 0 1;
createNode polyTweak -n "polyTweak6";
	rename -uid "CCB74E53-47F8-F2CD-9D28-1A9C9595D025";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -2.5 -1.5 0 -2.5 -1.5 0
		 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5;
createNode transformGeometry -n "transformGeometry28";
	rename -uid "B6511720-4F37-D088-9FB2-898C38D27F54";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.98768834059513777 0.15643446504023087 0
		 0 -0.15643446504023087 0.98768834059513777 0 0 -18.699999999999999 12.699999999999999 1;
createNode transformGeometry -n "transformGeometry29";
	rename -uid "392882F0-46CD-F60F-33D8-1091A26B9F95";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -18.699999999999999 12.699999999999999 1;
createNode polyTweak -n "polyTweak7";
	rename -uid "E614DEAC-42E3-53CE-3F64-A9A09A54DF8D";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -3.5 0 0 -3.5 0 0 -3.5 0
		 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0;
createNode transformGeometry -n "transformGeometry30";
	rename -uid "CFD69E13-4F33-EC74-B5CA-828583EBD4D5";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -12.499999999999998 11 1;
createNode transformGeometry -n "transformGeometry31";
	rename -uid "9D765ED2-472F-4D44-5389-D992D179610E";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 0.38461756040018324 0 0 1 0 0
		 -0.38461756040018324 0 0.92307601649691418 0 5.4615702909401849 0 0.93847259198938104 1;
createNode transformGeometry -n "transformGeometry32";
	rename -uid "30D32989-491A-A765-150A-D59ABBA844EA";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-16 0 -6.9388939039072284e-18 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-17 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry33";
	rename -uid "A44105B6-4D94-3481-B4E6-DA9E268AC258";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-16 0 -6.9388939039072284e-18 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-17 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry34";
	rename -uid "86ECE2E6-42DD-4A14-B968-5DB94CB5DAAA";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-16 0 -6.9388939039072284e-18 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-17 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry35";
	rename -uid "CA99D8E4-4C1F-C762-EB58-C0A971D8BDD0";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-16 0 -6.9388939039072284e-18 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-17 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry36";
	rename -uid "D9DBB557-4A6A-D709-1CAD-BFA4E9268A76";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-16 0 -6.9388939039072284e-18 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-17 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry37";
	rename -uid "40BBD014-4BD5-6672-ED4E-6EA1F89B2E28";
	setAttr ".txf" -type "matrix" 1 0 1.1102230246251565e-16 0 -6.9388939039072284e-18 0.99677887845624713 0.080198924328858931 0
		 -5.5511151231257827e-17 -0.080198924328858931 0.99677887845624724 0 0.38461804389953613 -3.0499999523162842 14.123078346252441 1;
createNode transformGeometry -n "transformGeometry38";
	rename -uid "D49FA4B9-4AEE-8A36-7EF0-C1934BE46969";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry39";
	rename -uid "46E3DB6E-4F9A-DF0B-27A1-C6B62C6C6F59";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry40";
	rename -uid "39E81AFD-4D23-D118-9C41-84AA1FFC680C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry41";
	rename -uid "1E72E26A-4F3B-B7A2-DE2B-799468144F8A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry42";
	rename -uid "ED701055-4E8B-F5DA-13FE-AF8555FB8F1A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry43";
	rename -uid "F02C6DA8-43F2-9347-5C73-B6AC1CC237E8";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry44";
	rename -uid "D7D4F962-4421-8E33-31E6-26A6E0CC6737";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 -0.080198924328858931 0
		 0 0.080198924328858931 0.99677887845624713 0 0 -1.1424801121364967 -0.19911456345361245 1;
createNode transformGeometry -n "transformGeometry45";
	rename -uid "D0660701-4415-4B05-4EA3-CF881D9BAEC0";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.030845914622084619 0.99677887845624713 0.074029703596820554 0
		 0.38337866049027253 -0.080198924328858931 0.92010267645365451 0 -5.2908204681950295 1.1228312710267943 1.5021176062717214 1;
createNode transformGeometry -n "transformGeometry46";
	rename -uid "7AAA0379-4F05-C997-78D0-8685D3BE0528";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry47";
	rename -uid "88D074D8-4F7C-47D8-A486-63B28662117D";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry48";
	rename -uid "3372E96A-40CB-8480-842B-DFAD7DD3B53B";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry49";
	rename -uid "0ED7BCFF-4418-1AAC-754A-5EBD6EE84A24";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry50";
	rename -uid "C436AC16-4450-C1B8-AFDF-36B65C240D06";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode transformGeometry -n "transformGeometry51";
	rename -uid "BBEAF9E9-48AC-B1EA-F009-FFA53D405F35";
	setAttr ".txf" -type "matrix" 0.92307601649691429 0 -0.38461756040018319 0 0.080622980278660286 0.97778323675860612 0.19349386802906768 0
		 0.37607260312228985 -0.2096185629038218 0.90256825518459338 0 -1.6443677735011479 0.55542666349695757 2.453590095935748 1;
createNode polyCube -n "polyCube24";
	rename -uid "A240EF2B-40E3-C65B-678C-08B4636D5C21";
	setAttr ".w" 12;
	setAttr ".h" 9;
	setAttr ".d" 12;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube25";
	rename -uid "70CE1749-418E-FEB1-3E7C-9ABD21534D0A";
	setAttr ".w" 10;
	setAttr ".h" 3;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube26";
	rename -uid "E508D78D-41CD-B69F-13C2-B7B42A8CF99A";
	setAttr ".w" 6;
	setAttr ".h" 12;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube27";
	rename -uid "81205D2E-47AE-F32A-A780-748093631382";
	setAttr ".w" 8;
	setAttr ".h" 8;
	setAttr ".d" 22;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak8";
	rename -uid "3F94CF60-4EE7-A771-A1EA-39823B5FC1FB";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  8.8817842e-16 1.5 0 8.8817842e-16
		 1.5 0 8.8817842e-16 1.5 0 8.8817842e-16 1.5 0 8.8817842e-16 1.5 0 8.8817842e-16 1.5
		 0 8.8817842e-16 1.5 0 8.8817842e-16 1.5 0;
createNode transformGeometry -n "transformGeometry52";
	rename -uid "E2491441-426A-2AC1-9FB0-ACA329B64218";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 1 0 1;
createNode transformGeometry -n "transformGeometry53";
	rename -uid "6AC1074D-4D5E-FAE8-9CD2-F5893AE64E3A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -11 0 1;
createNode transformGeometry -n "transformGeometry54";
	rename -uid "82FC2D08-4756-0F3A-2D90-5FB6B8AAF1FD";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -18 4 1;
createNode transformGeometry -n "transformGeometry55";
	rename -uid "E06C9A47-4190-ABEB-0F3C-59854B67D17A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -18 -3.5 0 1;
createNode transformGeometry -n "transformGeometry56";
	rename -uid "4F66D127-4A26-B67E-C222-C78A3D8D1396";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -1 1;
createNode transformGeometry -n "transformGeometry57";
	rename -uid "16CB2CEE-4C7E-B65F-8641-95A6FEC5765A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -1 1;
createNode displayLayer -n "layer1";
	rename -uid "C9D462A5-4D4A-DC2B-E7F9-CBB0766BF300";
	setAttr ".v" no;
	setAttr ".do" 1;
createNode displayLayer -n "layer2";
	rename -uid "F09E0FF1-4746-6336-25B0-05B9D4DB9C8A";
	setAttr ".do" 2;
createNode polyPlane -n "polyPlane1";
	rename -uid "3877D582-420C-5279-E319-D4AB843EA620";
	setAttr ".w" 100;
	setAttr ".h" 100;
	setAttr ".cuv" 2;
createNode groupId -n "groupId210";
	rename -uid "3D0B5E63-4494-5552-CA6D-41AE76179922";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "C088F9D7-4F94-A156-192A-9C9A88859BE4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:149]";
createNode groupId -n "groupId213";
	rename -uid "87B33F77-45B0-D82C-612E-66BEC8F14F16";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "F22B6E2B-4D42-7441-C15E-0EAAC8A35779";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:23]";
createNode groupId -n "groupId216";
	rename -uid "6B59BB01-4882-E5D1-7626-3CBBA79F1A79";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "7DEE0184-4317-C219-F59E-0DB56369EE41";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:280]";
createNode groupId -n "groupId219";
	rename -uid "CD3D961C-4F0A-DD01-F9A1-E8ADFC29AC68";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "6F7CABD6-4922-7F35-DE8A-6199FB845030";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:253]";
createNode groupId -n "groupId224";
	rename -uid "9DF5ABC5-4D20-8ECC-393E-578AF541A96D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts15";
	rename -uid "6B68FB29-4CD3-B3D5-011A-DA848E76F737";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:207]";
createNode groupId -n "groupId227";
	rename -uid "87C8E313-4F28-C8F7-8E43-3D93824E60C2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "8F612ECA-46D6-D3EE-0DAD-A78CF84E852D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:340]";
createNode groupId -n "groupId230";
	rename -uid "AFC4D1D0-46E8-E6E0-5AE5-9693A0A25B03";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts21";
	rename -uid "A2AA7635-4F74-18C5-8A6D-92979F3A2F8E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:340]";
createNode skinCluster -n "skinCluster1";
	rename -uid "7F48190E-410E-5C28-0B6C-95B630239544";
	setAttr ".skm" 2;
	setAttr -s 200 ".wl";
	setAttr ".wl[0:199].w"
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		2 13 0.99000000022351742 14 0.0099999997764825821
		3 13 0.98010000044256451 14 0.0098999997809529304 15 0.0099999997764825821
		1 13 1
		1 13 1
		2 13 0.99000000022351742 14 0.0099999997764825821
		3 13 0.98010000044256451 14 0.0098999997809529304 15 0.0099999997764825821
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		2 13 0.99000000022351742 14 0.0099999997764825821
		2 13 0.99000000022351742 14 0.0099999997764825821
		1 13 1
		1 13 1
		2 13 0.99000000022351742 14 0.0099999997764825821
		2 13 0.99000000022351742 14 0.0099999997764825821
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		2 17 0.99000000022351742 18 0.0099999997764825821
		3 17 0.98010000044256451 18 0.0098999997809529304 19 0.0099999997764825821
		1 17 1
		1 17 1
		2 17 0.99000000022351742 18 0.0099999997764825821
		3 17 0.98010000044256451 18 0.0098999997809529304 19 0.0099999997764825821
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		2 17 0.99000000022351742 18 0.0099999997764825821
		2 17 0.99000000022351742 18 0.0099999997764825821
		1 17 1
		1 17 1
		2 17 0.99000000022351742 18 0.0099999997764825821
		2 17 0.99000000022351742 18 0.0099999997764825821
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -10.999999999999996 -1.998403138391183e-15 -4.884981308350688e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -18.000000000000004 -1.7763585334661548e-15 -1.5777218104420236e-30 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4408920985006242e-16 0 0 -4.4408920985006242e-16 0.99999999999999978 0 0
		 0 0 1 0 1.2212452847360253e-14 -36.000007629394553 8.7209895935339042e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214408 -0.16439898730535335 0 0 0.16439898730535335 0.98639392383214408 0 0
		 0 0 1 0 -17.261894921327158 -30.578219164384944 -1.38199746014171e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.7192471324942728e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.7192471324942728e-15 0 0
		 0 0 1 0 34.000007629394595 -17.999999999999908 -7.7091537226431104e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247804213e-06 -2.994222926207424e-07 -0.99999999999940581 0
		 -0.2747211278969825 0.96152394764093596 8.9278331431787287e-17 0 0.96152394764036508 0.27472112789681941 -1.0899135974441996e-06 0
		 4.6702780378531381 -16.345901720294645 17.999999999989289 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999940592 -1.1627350681068416e-12 -1.0899136849089665e-06 0
		 -2.5124902887767275e-14 0.99999999999940592 -1.0899135950987825e-06 0 1.089913684929747e-06 1.0899135951530265e-06 0.99999999999881206 0
		 -17.999999999989527 -15.000007629367252 -6.9999640328497588 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214353 0.16439898730535721 2.4497359760911441e-09 0
		 0.16439898730535724 0.9863939238321433 1.4698415856546866e-08 0 -4.1359030627651356e-25 1.4901162204490751e-08 -0.99999999999999978 0
		 -17.261894921327233 -30.578219164384883 -4.5565099814121511e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631564036977215e-10 -0.99999999999999978 2.4497359760911437e-09 0
		 -0.99999999999999956 4.1631571238424827e-10 1.4698415856546865e-08 0 -1.4698415857566734e-08 -2.4497359699719638e-09 -0.99999999999999978 0
		 34.000007621900863 -18.000000014154704 -4.5565098758950202e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693203743793652e-09 -3.2534560137809243e-10 -0.99999999999999956 0
		 -0.27657365741139139 0.96099272215042575 3.7029428008592319e-10 0 0.96099272215042586 0.27657365741139145 -2.4629809358876019e-09 0
		 4.7017523716722041 -16.336876213305111 -18.000000013372336 1;
	setAttr ".pm[11]" -type "matrix" 1 3.6583863758321502e-10 -2.4687521970548768e-09 0
		 -3.7170310235291724e-10 0.99999717760308426 -0.0023758758100394491 0 2.467876128546285e-09 0.0023758758100395063 0.99999717760308438 0
		 18.000000012614258 -15.004705718946092 -6.9548525207207001 1;
	setAttr ".pm[12]" -type "matrix" -7.8197106089471832e-16 -7.2875304467787601e-15 -0.99999999999999978 0
		 -0.99161147425390106 0.12925433891364821 -1.3273513283553228e-16 0 0.12925433891364824 0.99161147425390128 -7.2312523864193332e-15 0
		 10.907733782188084 -1.4217987141824244 8.0000000000000089 1;
	setAttr ".pm[13]" -type "matrix" 2.4565718295886198e-15 -6.5397203800527145e-15 1 0
		 -0.94953047617381503 0.31367479149132727 4.3501369248708397e-15 0 -0.31367479149132732 -0.94953047617381536 -5.5353187261920797e-15 0
		 5.9016241406275283 -1.2632052676650838 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2922971722798796e-13 -2.9338779689594943e-15 -0.99999999999999978 0
		 -0.24253388348224131 0.97014293553219344 2.8530102516174479e-14 0 0.97014293553219388 0.24253388348224145 -1.2598664247092364e-13 0
		 1.2126768190154615 -0.72760905204993054 8.0000000000001688 1;
	setAttr ".pm[15]" -type "matrix" -0.99999999999999978 2.8307817709566573e-14 1.2521447036012371e-13 0
		 2.8363569062480712e-14 0.99999999999999956 -1.4988010832475335e-15 0 -1.2509846405122354e-13 -1.4988010832403947e-15 -1.0000000000000002 0
		 8.0000000000001688 -7.6293947780851315e-06 2.9999999999989986 1;
	setAttr ".pm[16]" -type "matrix" -7.8197106089470639e-16 -7.2875304467787617e-15 -0.99999999999999978 0
		 -0.99161147425390117 0.12925433891364654 -1.6084662077247346e-16 0 0.1292543389136466 0.9916114742539015 -7.3395496111505686e-15 0
		 10.907733782188073 -1.4217987141825199 -8.000000000000016 1;
	setAttr ".pm[17]" -type "matrix" 2.4565718295886379e-15 4.5625098661988438e-15 0.99999999999999978 0
		 -0.94953047617381414 0.31367479149132982 8.9575865522611991e-16 0 -0.31367479149132993 -0.94953047617381447 5.1148844708537423e-15 0
		 5.9016241406275345 -1.2632052676651138 8.0000000000000142 1;
	setAttr ".pm[18]" -type "matrix" -1.7288783470699686e-14 -2.1121345273485405e-15 -1 0
		 -0.24253388348224719 0.97014293553219222 2.1497302381529649e-15 0 0.97014293553219255 0.24253388348224719 -1.7296932986024363e-14 0
		 1.2126768190143067 -0.72760905204996984 -7.9999999999999813 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 2.2517500859091247e-15 -1.7715685347081939e-14 0
		 -2.2746303284232958e-15 0.99999999999999978 -2.2759572004815319e-15 0 1.7629999893411907e-14 2.1926904736347228e-15 1 0
		 7.9999999999999805 -7.6293945235888275e-06 -3.0000000000001434 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 15 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 15 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 15 ".ifcl";
createNode tweak -n "tweak1";
	rename -uid "91F7A7D2-4575-67C6-4D03-C5A46A15E783";
	setAttr -s 9 ".vl[0].vt";
	setAttr ".vl[0].vt[176]" -type "float3" 0 -3.9339066e-06 -9.5367432e-07 ;
	setAttr ".vl[0].vt[177]" -type "float3" 0 -3.9339066e-06 0 ;
	setAttr ".vl[0].vt[182]" -type "float3" 0 -3.9339066e-06 0 ;
	setAttr ".vl[0].vt[183]" -type "float3" 0 -3.9339066e-06 0 ;
	setAttr ".vl[0].vt[192]" -type "float3" 0 -3.9339066e-06 0 ;
	setAttr ".vl[0].vt[193]" -type "float3" 0 -3.9339066e-06 0 ;
	setAttr ".vl[0].vt[198]" -type "float3" 0 -3.9339066e-06 0 ;
	setAttr ".vl[0].vt[199]" -type "float3" 0 -3.9339066e-06 0 ;
createNode objectSet -n "skinCluster1Set";
	rename -uid "2D027A9E-40EB-34A1-3DDD-ACA239BBF818";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	rename -uid "2627E029-4985-A7C1-36CC-F2B0C3A077B4";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	rename -uid "B42C269A-4AA7-7760-907E-13B60878111C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	rename -uid "32450954-4421-EB56-56A2-C1A12794CC5E";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId232";
	rename -uid "E2BB34BF-49EC-8D5C-F8F9-19871CCB7FA6";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts23";
	rename -uid "C7F0046F-4512-C2CC-F6FE-8BBC9E93F68E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose1";
	rename -uid "360A05B3-442A-373A-D554-95AE3BED6574";
	setAttr -s 22 ".wm";
	setAttr ".wm[3]" -type "matrix" 0.98639399999999999 0.16439899999999999 0 0 -0.16439899999999999 0.98639399999999999 0 0
		 0 0 1 0 12 33.000005999999999 0 1;
	setAttr ".wm[7]" -type "matrix" -4.1631600000000003e-10 -1 -1.46984e-08 0 -1 4.1631600000000003e-10 -2.4497399999999999e-09 0
		 2.4497399999999999e-09 1.46984e-08 -1 0 -18 34.000005999999999 0 1;
	setAttr -s 23 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0.70710678118654746 0.70710678118654757 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 11 1.9984031383911763e-15
		 4.8849813083506888e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 7 -2.2204460492503131e-16
		 -4.8849813083506888e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 18.000007629394549 2.4424868425270727e-15
		 -8.7209895935339058e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.70710678118654757 0.70710678118654746 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 15.000007629394531 -12
		 1.3819974421032562e-14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.64637489613019727 0.76301998247272451 1
		 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 6.0827627182006836 3.5527136788005009e-15
		 -6.1108209421442699e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.76301998247272562 0.64637489613019594 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 17.000007629394531 7.1054273576010019e-15
		 2.1111584702637742e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.56451805047156733 -0.42581676299622079 0.42581629889299571 0.56451743519600195 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.2801098892839882 5.4889182334241411e-15
		 6.3938931927024257e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.098076185867602791 0.70027213407761668 0.09807618586759384 0.70027213407768052 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 15.000007629394531 12
		 5.5517516683856443e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.76301998247272562 0.64637489613019583 4.8158703083621573e-09 5.6849442951405353e-09 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 6.0827627182006871 -7.1054273576010019e-15
		 -1.0551713146524013e-14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.76301998233817769 0.64637489628902434 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 17.000007629394535 -3.5527136788005009e-15
		 2.582690695277973e-14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.56492779255963022 0.42527237058981499 0.42527237056944889 0.56492779258668435 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.2742303381127904 5.0863888816716471e-15
		 -7.4498314596329297e-10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.097919463706565971 0.70029406582161557 0.097919463706067175 0.70029406582518272 1
		 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.62939453125e-06 -8
		 1.6940658945086007e-21 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.04579444766043847 0.70562232714354967 0.70562232714354489 -0.045794447660438699 1
		 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.04229736328125 2.2204460492503131e-16
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.97494140529563056 -0.22246180849795549 -1.4955151884505726e-15 6.5541123184852694e-15 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.2657685279846191 -6.6613381477509392e-16
		 3.0730973321624333e-13 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.68043512947647411 0.73280832048656075 4.6123184233190487e-14 4.9673292440708926e-14 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.1231036186218271 1.5543122344752192e-15
		 -1.7763568394002505e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.086395984379782506 -0.70180890125663697 0.086395984379782451 0.70180890125663753 1
		 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.62939453125e-06 8 -2.6645335650344812e-15 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.045794447660437922 0.70562232714354978 0.705622327143545 -0.045794447660438144 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.04229736328125 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.97494140529563045 -0.22246180849795602 -2.6060407897966072e-16 1.1421003393871913e-15 1
		 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.2657685279846191 2.2204460492503131e-16
		 4.8849813083506888e-14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.68043512947647533 0.73280832048655964 4.2720992212151009e-15 4.6009233204336085e-15 1
		 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0 0 0 0 4.1231036186218262 -5.5511151231257827e-16
		 8.8817841970012523e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.086395984379783269 0.70180890125663697 0.086395984379783228 0.70180890125663731 1
		 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 18.000007629394531 2.4424868425270817e-15
		 -8.7209893915251267e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.70710678118654757 0.70710678118654746 1
		 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.2801098823547354 3.5527136788005009e-15
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.098076185867602791 0.70027213407761668 0.09807618586759384 0.70027213407768052 1
		 1 1 yes;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.2742304801940909 7.1054273576010019e-15
		 -7.4498274216239224e-10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.097919463706565971 0.70029406582161557 0.097919463706067175 0.70029406582518272 1
		 1 1 yes;
	setAttr -s 20 ".m";
	setAttr -s 20 ".p";
	setAttr -s 2 ".g";
	setAttr ".g[4]" yes;
	setAttr ".g[8]" yes;
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster2";
	rename -uid "8D9B1D7A-4BE8-1C2D-1FCE-E8A375580CA3";
	setAttr ".skm" 2;
	setAttr -s 314 ".wl";
	setAttr ".wl[0:313].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -10.999999999999996 -1.998403138391183e-15 -4.884981308350688e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -18.000000000000004 -1.7763585334661548e-15 -1.5777218104420236e-30 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4408920985006242e-16 0 0 -4.4408920985006242e-16 0.99999999999999978 0 0
		 0 0 1 0 1.2212452847360253e-14 -36.000007629394553 8.7209895935339042e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214408 -0.16439898730535335 0 0 0.16439898730535335 0.98639392383214408 0 0
		 0 0 1 0 -17.261894921327158 -30.578219164384944 -1.38199746014171e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.7192471324942728e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.7192471324942728e-15 0 0
		 0 0 1 0 34.000007629394595 -17.999999999999908 -7.7091537226431104e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247804213e-06 -2.994222926207424e-07 -0.99999999999940581 0
		 -0.2747211278969825 0.96152394764093596 8.9278331431787287e-17 0 0.96152394764036508 0.27472112789681941 -1.0899135974441996e-06 0
		 4.6702780378531381 -16.345901720294645 17.999999999989289 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999940592 -1.1627350681068416e-12 -1.0899136849089665e-06 0
		 -2.5124902887767275e-14 0.99999999999940592 -1.0899135950987825e-06 0 1.089913684929747e-06 1.0899135951530265e-06 0.99999999999881206 0
		 -17.999999999989527 -15.000007629367252 -6.9999640328497588 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214353 0.16439898730535721 2.4497359760911441e-09 0
		 0.16439898730535724 0.9863939238321433 1.4698415856546866e-08 0 -4.1359030627651356e-25 1.4901162204490751e-08 -0.99999999999999978 0
		 -17.261894921327233 -30.578219164384883 -4.5565099814121511e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631564036977215e-10 -0.99999999999999978 2.4497359760911437e-09 0
		 -0.99999999999999956 4.1631571238424827e-10 1.4698415856546865e-08 0 -1.4698415857566734e-08 -2.4497359699719638e-09 -0.99999999999999978 0
		 34.000007621900863 -18.000000014154704 -4.5565098758950202e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693203743793652e-09 -3.2534560137809243e-10 -0.99999999999999956 0
		 -0.27657365741139139 0.96099272215042575 3.7029428008592319e-10 0 0.96099272215042586 0.27657365741139145 -2.4629809358876019e-09 0
		 4.7017523716722041 -16.336876213305111 -18.000000013372336 1;
	setAttr ".pm[11]" -type "matrix" 1 3.6583863758321502e-10 -2.4687521970548768e-09 0
		 -3.7170310235291724e-10 0.99999717760308426 -0.0023758758100394491 0 2.467876128546285e-09 0.0023758758100395063 0.99999717760308438 0
		 18.000000012614258 -15.004705718946092 -6.9548525207207001 1;
	setAttr ".pm[12]" -type "matrix" -7.8197106089471832e-16 -7.2875304467787601e-15 -0.99999999999999978 0
		 -0.99161147425390106 0.12925433891364821 -1.3273513283553228e-16 0 0.12925433891364824 0.99161147425390128 -7.2312523864193332e-15 0
		 10.907733782188084 -1.4217987141824244 8.0000000000000089 1;
	setAttr ".pm[13]" -type "matrix" 2.4565718295886198e-15 -6.5397203800527145e-15 1 0
		 -0.94953047617381503 0.31367479149132727 4.3501369248708397e-15 0 -0.31367479149132732 -0.94953047617381536 -5.5353187261920797e-15 0
		 5.9016241406275283 -1.2632052676650838 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2922971722798796e-13 -2.9338779689594943e-15 -0.99999999999999978 0
		 -0.24253388348224131 0.97014293553219344 2.8530102516174479e-14 0 0.97014293553219388 0.24253388348224145 -1.2598664247092364e-13 0
		 1.2126768190154615 -0.72760905204993054 8.0000000000001688 1;
	setAttr ".pm[15]" -type "matrix" -0.99999999999999978 2.8307817709566573e-14 1.2521447036012371e-13 0
		 2.8363569062480712e-14 0.99999999999999956 -1.4988010832475335e-15 0 -1.2509846405122354e-13 -1.4988010832403947e-15 -1.0000000000000002 0
		 8.0000000000001688 -7.6293947780851315e-06 2.9999999999989986 1;
	setAttr ".pm[16]" -type "matrix" -7.8197106089470639e-16 -7.2875304467787617e-15 -0.99999999999999978 0
		 -0.99161147425390117 0.12925433891364654 -1.6084662077247346e-16 0 0.1292543389136466 0.9916114742539015 -7.3395496111505686e-15 0
		 10.907733782188073 -1.4217987141825199 -8.000000000000016 1;
	setAttr ".pm[17]" -type "matrix" 2.4565718295886379e-15 4.5625098661988438e-15 0.99999999999999978 0
		 -0.94953047617381414 0.31367479149132982 8.9575865522611991e-16 0 -0.31367479149132993 -0.94953047617381447 5.1148844708537423e-15 0
		 5.9016241406275345 -1.2632052676651138 8.0000000000000142 1;
	setAttr ".pm[18]" -type "matrix" -1.7288783470699686e-14 -2.1121345273485405e-15 -1 0
		 -0.24253388348224719 0.97014293553219222 2.1497302381529649e-15 0 0.97014293553219255 0.24253388348224719 -1.7296932986024363e-14 0
		 1.2126768190143067 -0.72760905204996984 -7.9999999999999813 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 2.2517500859091247e-15 -1.7715685347081939e-14 0
		 -2.2746303284232958e-15 0.99999999999999978 -2.2759572004815319e-15 0 1.7629999893411907e-14 2.1926904736347228e-15 1 0
		 7.9999999999999805 -7.6293945235888275e-06 -3.0000000000001434 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 16 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 16 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 16 ".ifcl";
createNode tweak -n "tweak2";
	rename -uid "15407D74-4C9C-600D-6E7E-F7AEDE36CE51";
createNode objectSet -n "skinCluster2Set";
	rename -uid "E28F86AC-469D-ACA8-3F3D-81BA3AB1453F";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster2GroupId";
	rename -uid "A3B68A2C-4A65-A57E-E29D-F9A8E6EFA7A9";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster2GroupParts";
	rename -uid "29426EA2-44A4-C843-3909-DDB76D67FD1C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet2";
	rename -uid "C4D73EAD-434E-6BFE-4902-558BFFCD5F51";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId234";
	rename -uid "9D8B269E-4BE7-3808-509F-F691E7BA15E5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts25";
	rename -uid "33B3A9A8-4B58-752A-AFE6-76AB45FED22B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster3";
	rename -uid "A5F99C76-4517-CDF2-E2CA-DEB83063F1A0";
	setAttr ".skm" 2;
	setAttr -s 170 ".wl";
	setAttr ".wl[0:169].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -10.999999999999996 -1.998403138391183e-15 -4.884981308350688e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -18.000000000000004 -1.7763585334661548e-15 -1.5777218104420236e-30 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4408920985006242e-16 0 0 -4.4408920985006242e-16 0.99999999999999978 0 0
		 0 0 1 0 1.2212452847360253e-14 -36.000007629394553 8.7209895935339042e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214408 -0.16439898730535335 0 0 0.16439898730535335 0.98639392383214408 0 0
		 0 0 1 0 -17.261894921327158 -30.578219164384944 -1.38199746014171e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.7192471324942728e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.7192471324942728e-15 0 0
		 0 0 1 0 34.000007629394595 -17.999999999999908 -7.7091537226431104e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247804213e-06 -2.994222926207424e-07 -0.99999999999940581 0
		 -0.2747211278969825 0.96152394764093596 8.9278331431787287e-17 0 0.96152394764036508 0.27472112789681941 -1.0899135974441996e-06 0
		 4.6702780378531381 -16.345901720294645 17.999999999989289 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999940592 -1.1627350681068416e-12 -1.0899136849089665e-06 0
		 -2.5124902887767275e-14 0.99999999999940592 -1.0899135950987825e-06 0 1.089913684929747e-06 1.0899135951530265e-06 0.99999999999881206 0
		 -17.999999999989527 -15.000007629367252 -6.9999640328497588 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214353 0.16439898730535721 2.4497359760911441e-09 0
		 0.16439898730535724 0.9863939238321433 1.4698415856546866e-08 0 -4.1359030627651356e-25 1.4901162204490751e-08 -0.99999999999999978 0
		 -17.261894921327233 -30.578219164384883 -4.5565099814121511e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631564036977215e-10 -0.99999999999999978 2.4497359760911437e-09 0
		 -0.99999999999999956 4.1631571238424827e-10 1.4698415856546865e-08 0 -1.4698415857566734e-08 -2.4497359699719638e-09 -0.99999999999999978 0
		 34.000007621900863 -18.000000014154704 -4.5565098758950202e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693203743793652e-09 -3.2534560137809243e-10 -0.99999999999999956 0
		 -0.27657365741139139 0.96099272215042575 3.7029428008592319e-10 0 0.96099272215042586 0.27657365741139145 -2.4629809358876019e-09 0
		 4.7017523716722041 -16.336876213305111 -18.000000013372336 1;
	setAttr ".pm[11]" -type "matrix" 1 3.6583863758321502e-10 -2.4687521970548768e-09 0
		 -3.7170310235291724e-10 0.99999717760308426 -0.0023758758100394491 0 2.467876128546285e-09 0.0023758758100395063 0.99999717760308438 0
		 18.000000012614258 -15.004705718946092 -6.9548525207207001 1;
	setAttr ".pm[12]" -type "matrix" -7.8197106089471832e-16 -7.2875304467787601e-15 -0.99999999999999978 0
		 -0.99161147425390106 0.12925433891364821 -1.3273513283553228e-16 0 0.12925433891364824 0.99161147425390128 -7.2312523864193332e-15 0
		 10.907733782188084 -1.4217987141824244 8.0000000000000089 1;
	setAttr ".pm[13]" -type "matrix" 2.4565718295886198e-15 -6.5397203800527145e-15 1 0
		 -0.94953047617381503 0.31367479149132727 4.3501369248708397e-15 0 -0.31367479149132732 -0.94953047617381536 -5.5353187261920797e-15 0
		 5.9016241406275283 -1.2632052676650838 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2922971722798796e-13 -2.9338779689594943e-15 -0.99999999999999978 0
		 -0.24253388348224131 0.97014293553219344 2.8530102516174479e-14 0 0.97014293553219388 0.24253388348224145 -1.2598664247092364e-13 0
		 1.2126768190154615 -0.72760905204993054 8.0000000000001688 1;
	setAttr ".pm[15]" -type "matrix" -0.99999999999999978 2.8307817709566573e-14 1.2521447036012371e-13 0
		 2.8363569062480712e-14 0.99999999999999956 -1.4988010832475335e-15 0 -1.2509846405122354e-13 -1.4988010832403947e-15 -1.0000000000000002 0
		 8.0000000000001688 -7.6293947780851315e-06 2.9999999999989986 1;
	setAttr ".pm[16]" -type "matrix" -7.8197106089470639e-16 -7.2875304467787617e-15 -0.99999999999999978 0
		 -0.99161147425390117 0.12925433891364654 -1.6084662077247346e-16 0 0.1292543389136466 0.9916114742539015 -7.3395496111505686e-15 0
		 10.907733782188073 -1.4217987141825199 -8.000000000000016 1;
	setAttr ".pm[17]" -type "matrix" 2.4565718295886379e-15 4.5625098661988438e-15 0.99999999999999978 0
		 -0.94953047617381414 0.31367479149132982 8.9575865522611991e-16 0 -0.31367479149132993 -0.94953047617381447 5.1148844708537423e-15 0
		 5.9016241406275345 -1.2632052676651138 8.0000000000000142 1;
	setAttr ".pm[18]" -type "matrix" -1.7288783470699686e-14 -2.1121345273485405e-15 -1 0
		 -0.24253388348224719 0.97014293553219222 2.1497302381529649e-15 0 0.97014293553219255 0.24253388348224719 -1.7296932986024363e-14 0
		 1.2126768190143067 -0.72760905204996984 -7.9999999999999813 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 2.2517500859091247e-15 -1.7715685347081939e-14 0
		 -2.2746303284232958e-15 0.99999999999999978 -2.2759572004815319e-15 0 1.7629999893411907e-14 2.1926904736347228e-15 1 0
		 7.9999999999999805 -7.6293945235888275e-06 -3.0000000000001434 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 10 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 10 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".ucm" yes;
	setAttr ".wd" 1;
	setAttr -s 10 ".ifcl";
createNode tweak -n "tweak3";
	rename -uid "D3C6D223-4ACA-5C76-081D-1682B3CEAE82";
createNode objectSet -n "skinCluster3Set";
	rename -uid "A26073FF-4ADD-0715-04C8-A1989FC95BBD";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster3GroupId";
	rename -uid "E80CD2C8-438A-C3E5-4D4A-2086CB30D563";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster3GroupParts";
	rename -uid "2193097B-4F16-A4AE-D2C0-3E9F6ACBAAEC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet3";
	rename -uid "1D029873-4396-DB42-9CAF-44BA1FDDB194";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId236";
	rename -uid "A1D9AAC7-4637-BF3A-4268-D69586DB61DD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts27";
	rename -uid "B02A7936-4B81-112E-1785-1797EAB9BFB8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster4";
	rename -uid "BD662545-4E50-AAF0-9502-6B9EBDD560EC";
	setAttr -s 364 ".wl";
	setAttr ".wl[0:363].w"
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -10.999999999999996 -1.998403138391183e-15 -4.884981308350688e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -18.000000000000004 -1.7763585334661548e-15 -1.5777218104420236e-30 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4408920985006242e-16 0 0 -4.4408920985006242e-16 0.99999999999999978 0 0
		 0 0 1 0 1.2212452847360253e-14 -36.000007629394553 8.7209895935339042e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214408 -0.16439898730535335 0 0 0.16439898730535335 0.98639392383214408 0 0
		 0 0 1 0 -17.261894921327158 -30.578219164384944 -1.38199746014171e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.7192471324942728e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.7192471324942728e-15 0 0
		 0 0 1 0 34.000007629394595 -17.999999999999908 -7.7091537226431104e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247804213e-06 -2.994222926207424e-07 -0.99999999999940581 0
		 -0.2747211278969825 0.96152394764093596 8.9278331431787287e-17 0 0.96152394764036508 0.27472112789681941 -1.0899135974441996e-06 0
		 4.6702780378531381 -16.345901720294645 17.999999999989289 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999940592 -1.1627350681068416e-12 -1.0899136849089665e-06 0
		 -2.5124902887767275e-14 0.99999999999940592 -1.0899135950987825e-06 0 1.089913684929747e-06 1.0899135951530265e-06 0.99999999999881206 0
		 -17.999999999989527 -15.000007629367252 -6.9999640328497588 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214353 0.16439898730535721 2.4497359030833279e-09 0
		 0.16439898730535724 0.9863939238321433 1.4698415418499968e-08 0 4.1359030627651356e-25 1.4901161760401541e-08 -0.99999999999999978 0
		 -17.261894921327233 -30.578219164384883 -4.5565098456175792e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631564036977427e-10 -0.99999999999999978 2.4497359030833271e-09 0
		 -0.99999999999999956 4.163157123842461e-10 1.4698415418499966e-08 0 -1.4698415419519835e-08 -2.4497358969641473e-09 -0.99999999999999978 0
		 34.000007621900863 -18.000000014154704 -4.5565097401004482e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693203042193854e-09 -3.2534558118605259e-10 -0.99999999999999956 0
		 -0.27657365741139095 0.96099272215042586 3.7029428008592097e-10 0 0.96099272215042597 0.27657365741139101 -2.4629808628797853e-09 0
		 4.7017523716721898 -16.336876213305118 -18.000000013372333 1;
	setAttr ".pm[11]" -type "matrix" 1 3.6583863775667376e-10 -2.4687521240472666e-09 0
		 -3.7170310235291512e-10 0.99999717760308426 -0.002375875810039005 0 2.4678760555384684e-09 0.0023758758100390067 0.99999717760308438 0
		 18.000000012614255 -15.004705718946092 -6.9548525207207144 1;
	setAttr ".pm[12]" -type "matrix" -7.8197106089471832e-16 -7.2875304467787601e-15 -0.99999999999999978 0
		 -0.99161147425390106 0.12925433891364821 -1.3273513283553228e-16 0 0.12925433891364824 0.99161147425390128 -7.2312523864193332e-15 0
		 10.907733782188084 -1.4217987141824244 8.0000000000000089 1;
	setAttr ".pm[13]" -type "matrix" 2.4565718295886198e-15 -6.5397203800527145e-15 1 0
		 -0.94953047617381503 0.31367479149132727 4.3501369248708397e-15 0 -0.31367479149132732 -0.94953047617381536 -5.5353187261920797e-15 0
		 5.9016241406275283 -1.2632052676650838 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2922971722798794e-13 -2.9338779689594659e-15 -0.99999999999999978 0
		 -0.24253388348224164 0.97014293553219344 2.8530102516174523e-14 0 0.97014293553219377 0.24253388348224164 -1.2598664247092364e-13 0
		 1.2126768190154618 -0.72760905204993032 8.0000000000001688 1;
	setAttr ".pm[15]" -type "matrix" -0.99999999999999978 2.8280890833961466e-14 1.2522120202766604e-13 0
		 2.8335813486865124e-14 0.99999999999999956 -1.1934897514756154e-15 0 -1.2509846405122349e-13 -1.3045120539309953e-15 -1 0
		 8.0000000000001688 -7.6293947775644045e-06 2.9999999999989981 1;
	setAttr ".pm[16]" -type "matrix" -7.8197106089470639e-16 -7.2875304467787617e-15 -0.99999999999999978 0
		 -0.99161147425390117 0.12925433891364654 -1.6084662077247346e-16 0 0.1292543389136466 0.9916114742539015 -7.3395496111505686e-15 0
		 10.907733782188073 -1.4217987141825199 -8.000000000000016 1;
	setAttr ".pm[17]" -type "matrix" 2.4565718295886379e-15 4.5625098661988438e-15 0.99999999999999978 0
		 -0.94953047617381414 0.31367479149132982 8.9575865522611991e-16 0 -0.31367479149132993 -0.94953047617381447 5.1148844708537423e-15 0
		 5.9016241406275345 -1.2632052676651138 8.0000000000000142 1;
	setAttr ".pm[18]" -type "matrix" -1.7288783470699686e-14 -2.1121345273485405e-15 -1 0
		 -0.24253388348224719 0.97014293553219222 2.1497302381529649e-15 0 0.97014293553219255 0.24253388348224719 -1.7296932986024363e-14 0
		 1.2126768190143067 -0.72760905204996984 -7.9999999999999813 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 2.2517500859091247e-15 -1.7715685347081939e-14 0
		 -2.2746303284232958e-15 0.99999999999999978 -2.2759572004815319e-15 0 1.7629999893411907e-14 2.1926904736347228e-15 1 0
		 7.9999999999999805 -7.6293945235888275e-06 -3.0000000000001434 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 13 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 13 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 10;
	setAttr ".ucm" yes;
	setAttr -s 13 ".ifcl";
createNode tweak -n "tweak4";
	rename -uid "D6A463C7-4852-DEBF-BF13-FFB7780CD9BA";
createNode objectSet -n "skinCluster4Set";
	rename -uid "E65BC0FB-4211-1E7C-5215-968B16B84984";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster4GroupId";
	rename -uid "99D75391-4308-70DA-814C-038FBCF89034";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster4GroupParts";
	rename -uid "CB8CD997-4BB4-3AD6-A633-C68BA7D2D75C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet4";
	rename -uid "065F22E5-4D1B-F86B-C6F2-F7BA38455AB0";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId238";
	rename -uid "4481EAFE-47C8-B036-2D18-1A90E603E2C2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts29";
	rename -uid "2C24D5E4-4166-125B-2FF5-B6B4CF368B79";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster5";
	rename -uid "8683EE3B-458C-3E0F-8234-2582EE03CDFF";
	setAttr -s 223 ".wl";
	setAttr ".wl[0:222].w"
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -10.999999999999996 -1.998403138391183e-15 -4.884981308350688e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -18.000000000000004 -1.7763585334661548e-15 -1.5777218104420236e-30 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4408920985006242e-16 0 0 -4.4408920985006242e-16 0.99999999999999978 0 0
		 0 0 1 0 1.2212452847360253e-14 -36.000007629394553 8.7209895935339042e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214408 -0.16439898730535335 0 0 0.16439898730535335 0.98639392383214408 0 0
		 0 0 1 0 -17.261894921327158 -30.578219164384944 -1.38199746014171e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.7192471324942728e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.7192471324942728e-15 0 0
		 0 0 1 0 34.000007629394595 -17.999999999999908 -7.7091537226431104e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247804213e-06 -2.994222926207424e-07 -0.99999999999940581 0
		 -0.2747211278969825 0.96152394764093596 8.9278331431787287e-17 0 0.96152394764036508 0.27472112789681941 -1.0899135974441996e-06 0
		 4.6702780378531381 -16.345901720294645 17.999999999989289 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999940592 -1.1627350681068416e-12 -1.0899136849089665e-06 0
		 -2.5124902887767275e-14 0.99999999999940592 -1.0899135950987825e-06 0 1.089913684929747e-06 1.0899135951530265e-06 0.99999999999881206 0
		 -17.999999999989527 -15.000007629367252 -6.9999640328497588 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214353 0.16439898730535721 2.4497359030833279e-09 0
		 0.16439898730535724 0.9863939238321433 1.4698415418499968e-08 0 4.1359030627651356e-25 1.4901161760401541e-08 -0.99999999999999978 0
		 -17.261894921327233 -30.578219164384883 -4.5565098456175792e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631564036977427e-10 -0.99999999999999978 2.4497359030833271e-09 0
		 -0.99999999999999956 4.163157123842461e-10 1.4698415418499966e-08 0 -1.4698415419519835e-08 -2.4497358969641473e-09 -0.99999999999999978 0
		 34.000007621900863 -18.000000014154704 -4.5565097401004482e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693203042193854e-09 -3.2534558118605259e-10 -0.99999999999999956 0
		 -0.27657365741139095 0.96099272215042586 3.7029428008592097e-10 0 0.96099272215042597 0.27657365741139101 -2.4629808628797853e-09 0
		 4.7017523716721898 -16.336876213305118 -18.000000013372333 1;
	setAttr ".pm[11]" -type "matrix" 1 3.6583863775667376e-10 -2.4687521240472666e-09 0
		 -3.7170310235291512e-10 0.99999717760308426 -0.002375875810039005 0 2.4678760555384684e-09 0.0023758758100390067 0.99999717760308438 0
		 18.000000012614255 -15.004705718946092 -6.9548525207207144 1;
	setAttr ".pm[12]" -type "matrix" -7.8197106089471832e-16 -7.2875304467787601e-15 -0.99999999999999978 0
		 -0.99161147425390106 0.12925433891364821 -1.3273513283553228e-16 0 0.12925433891364824 0.99161147425390128 -7.2312523864193332e-15 0
		 10.907733782188084 -1.4217987141824244 8.0000000000000089 1;
	setAttr ".pm[13]" -type "matrix" 2.4565718295886198e-15 -6.5397203800527145e-15 1 0
		 -0.94953047617381503 0.31367479149132727 4.3501369248708397e-15 0 -0.31367479149132732 -0.94953047617381536 -5.5353187261920797e-15 0
		 5.9016241406275283 -1.2632052676650838 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2922971722798794e-13 -2.9338779689594659e-15 -0.99999999999999978 0
		 -0.24253388348224164 0.97014293553219344 2.8530102516174523e-14 0 0.97014293553219377 0.24253388348224164 -1.2598664247092364e-13 0
		 1.2126768190154618 -0.72760905204993032 8.0000000000001688 1;
	setAttr ".pm[15]" -type "matrix" -0.99999999999999978 2.8280890833961466e-14 1.2522120202766604e-13 0
		 2.8335813486865124e-14 0.99999999999999956 -1.1934897514756154e-15 0 -1.2509846405122349e-13 -1.3045120539309953e-15 -1 0
		 8.0000000000001688 -7.6293947775644045e-06 2.9999999999989981 1;
	setAttr ".pm[16]" -type "matrix" -7.8197106089470639e-16 -7.2875304467787617e-15 -0.99999999999999978 0
		 -0.99161147425390117 0.12925433891364654 -1.6084662077247346e-16 0 0.1292543389136466 0.9916114742539015 -7.3395496111505686e-15 0
		 10.907733782188073 -1.4217987141825199 -8.000000000000016 1;
	setAttr ".pm[17]" -type "matrix" 2.4565718295886379e-15 4.5625098661988438e-15 0.99999999999999978 0
		 -0.94953047617381414 0.31367479149132982 8.9575865522611991e-16 0 -0.31367479149132993 -0.94953047617381447 5.1148844708537423e-15 0
		 5.9016241406275345 -1.2632052676651138 8.0000000000000142 1;
	setAttr ".pm[18]" -type "matrix" -1.7288783470699686e-14 -2.1121345273485405e-15 -1 0
		 -0.24253388348224719 0.97014293553219222 2.1497302381529649e-15 0 0.97014293553219255 0.24253388348224719 -1.7296932986024363e-14 0
		 1.2126768190143067 -0.72760905204996984 -7.9999999999999813 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 2.2517500859091247e-15 -1.7715685347081939e-14 0
		 -2.2746303284232958e-15 0.99999999999999978 -2.2759572004815319e-15 0 1.7629999893411907e-14 2.1926904736347228e-15 1 0
		 7.9999999999999805 -7.6293945235888275e-06 -3.0000000000001434 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 13 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 13 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 10;
	setAttr ".ucm" yes;
	setAttr -s 13 ".ifcl";
createNode tweak -n "tweak5";
	rename -uid "CD94BCCF-4DB5-12EB-33D8-8C9063450272";
createNode objectSet -n "skinCluster5Set";
	rename -uid "207627A3-4BA1-498E-26C3-38AA86D17163";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster5GroupId";
	rename -uid "0B0329D9-4624-3321-1B5C-888BE99718C3";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster5GroupParts";
	rename -uid "3E969AF5-4BEB-FB3F-BC07-3B85A5FA693F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet5";
	rename -uid "80A836AE-4745-BAEE-05F1-63BEF31A85FB";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId240";
	rename -uid "83E1182E-4EE6-E4E6-B3D5-A1BCF35D0C25";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts31";
	rename -uid "451C5725-42DE-931C-9961-5483419CC20F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster6";
	rename -uid "5BD730E5-4FE1-AFFE-559E-BE8D4484D4B4";
	setAttr -s 262 ".wl";
	setAttr ".wl[0:261].w"
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -10.999999999999996 -1.998403138391183e-15 -4.884981308350688e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -18.000000000000004 -1.7763585334661548e-15 -1.5777218104420236e-30 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4408920985006242e-16 0 0 -4.4408920985006242e-16 0.99999999999999978 0 0
		 0 0 1 0 1.2212452847360253e-14 -36.000007629394553 8.7209895935339042e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214408 -0.16439898730535335 0 0 0.16439898730535335 0.98639392383214408 0 0
		 0 0 1 0 -17.261894921327158 -30.578219164384944 -1.38199746014171e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.7192471324942728e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.7192471324942728e-15 0 0
		 0 0 1 0 34.000007629394595 -17.999999999999908 -7.7091537226431104e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247804213e-06 -2.994222926207424e-07 -0.99999999999940581 0
		 -0.2747211278969825 0.96152394764093596 8.9278331431787287e-17 0 0.96152394764036508 0.27472112789681941 -1.0899135974441996e-06 0
		 4.6702780378531381 -16.345901720294645 17.999999999989289 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999940592 -1.1627350681068416e-12 -1.0899136849089665e-06 0
		 -2.5124902887767275e-14 0.99999999999940592 -1.0899135950987825e-06 0 1.089913684929747e-06 1.0899135951530265e-06 0.99999999999881206 0
		 -17.999999999989527 -15.000007629367252 -6.9999640328497588 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214353 0.16439898730535721 2.4497359030833279e-09 0
		 0.16439898730535724 0.9863939238321433 1.4698415418499968e-08 0 4.1359030627651356e-25 1.4901161760401541e-08 -0.99999999999999978 0
		 -17.261894921327233 -30.578219164384883 -4.5565098456175792e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631564036977427e-10 -0.99999999999999978 2.4497359030833271e-09 0
		 -0.99999999999999956 4.163157123842461e-10 1.4698415418499966e-08 0 -1.4698415419519835e-08 -2.4497358969641473e-09 -0.99999999999999978 0
		 34.000007621900863 -18.000000014154704 -4.5565097401004482e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693203042193854e-09 -3.2534558118605259e-10 -0.99999999999999956 0
		 -0.27657365741139095 0.96099272215042586 3.7029428008592097e-10 0 0.96099272215042597 0.27657365741139101 -2.4629808628797853e-09 0
		 4.7017523716721898 -16.336876213305118 -18.000000013372333 1;
	setAttr ".pm[11]" -type "matrix" 1 3.6583863775667376e-10 -2.4687521240472666e-09 0
		 -3.7170310235291512e-10 0.99999717760308426 -0.002375875810039005 0 2.4678760555384684e-09 0.0023758758100390067 0.99999717760308438 0
		 18.000000012614255 -15.004705718946092 -6.9548525207207144 1;
	setAttr ".pm[12]" -type "matrix" -7.8197106089471832e-16 -7.2875304467787601e-15 -0.99999999999999978 0
		 -0.99161147425390106 0.12925433891364821 -1.3273513283553228e-16 0 0.12925433891364824 0.99161147425390128 -7.2312523864193332e-15 0
		 10.907733782188084 -1.4217987141824244 8.0000000000000089 1;
	setAttr ".pm[13]" -type "matrix" 2.4565718295886198e-15 -6.5397203800527145e-15 1 0
		 -0.94953047617381503 0.31367479149132727 4.3501369248708397e-15 0 -0.31367479149132732 -0.94953047617381536 -5.5353187261920797e-15 0
		 5.9016241406275283 -1.2632052676650838 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2922971722798794e-13 -2.9338779689594659e-15 -0.99999999999999978 0
		 -0.24253388348224164 0.97014293553219344 2.8530102516174523e-14 0 0.97014293553219377 0.24253388348224164 -1.2598664247092364e-13 0
		 1.2126768190154618 -0.72760905204993032 8.0000000000001688 1;
	setAttr ".pm[15]" -type "matrix" -0.99999999999999978 2.8280890833961466e-14 1.2522120202766604e-13 0
		 2.8335813486865124e-14 0.99999999999999956 -1.1934897514756154e-15 0 -1.2509846405122349e-13 -1.3045120539309953e-15 -1 0
		 8.0000000000001688 -7.6293947775644045e-06 2.9999999999989981 1;
	setAttr ".pm[16]" -type "matrix" -7.8197106089470639e-16 -7.2875304467787617e-15 -0.99999999999999978 0
		 -0.99161147425390117 0.12925433891364654 -1.6084662077247346e-16 0 0.1292543389136466 0.9916114742539015 -7.3395496111505686e-15 0
		 10.907733782188073 -1.4217987141825199 -8.000000000000016 1;
	setAttr ".pm[17]" -type "matrix" 2.4565718295886379e-15 4.5625098661988438e-15 0.99999999999999978 0
		 -0.94953047617381414 0.31367479149132982 8.9575865522611991e-16 0 -0.31367479149132993 -0.94953047617381447 5.1148844708537423e-15 0
		 5.9016241406275345 -1.2632052676651138 8.0000000000000142 1;
	setAttr ".pm[18]" -type "matrix" -1.7288783470699686e-14 -2.1121345273485405e-15 -1 0
		 -0.24253388348224719 0.97014293553219222 2.1497302381529649e-15 0 0.97014293553219255 0.24253388348224719 -1.7296932986024363e-14 0
		 1.2126768190143067 -0.72760905204996984 -7.9999999999999813 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 2.2517500859091247e-15 -1.7715685347081939e-14 0
		 -2.2746303284232958e-15 0.99999999999999978 -2.2759572004815319e-15 0 1.7629999893411907e-14 2.1926904736347228e-15 1 0
		 7.9999999999999805 -7.6293945235888275e-06 -3.0000000000001434 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 13 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 13 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 10;
	setAttr ".ucm" yes;
	setAttr -s 13 ".ifcl";
createNode tweak -n "tweak6";
	rename -uid "BCD2E5CD-4262-7273-6B6F-46A570257319";
createNode objectSet -n "skinCluster6Set";
	rename -uid "77F0E257-4294-DD7E-E46F-89B0978EB3CE";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster6GroupId";
	rename -uid "EB28D626-483F-C6BC-910E-0D97933BC489";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster6GroupParts";
	rename -uid "D1960BC0-4B65-0A44-B4FD-BF8C94CF2C32";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet6";
	rename -uid "7B29A26E-4B45-F3A3-5B78-05B4B69FC09B";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId242";
	rename -uid "4CFCD48B-4F7F-4587-031D-38989C9B461E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts33";
	rename -uid "13F9AABC-4855-A964-CFBF-4C84E8533E39";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster7";
	rename -uid "0C9CBC72-4D54-15C2-44CA-6399EAB4DD8B";
	setAttr -s 364 ".wl";
	setAttr ".wl[0:363].w"
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -10.999999999999996 -1.998403138391183e-15 -4.884981308350688e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -18.000000000000004 -1.7763585334661548e-15 -1.5777218104420236e-30 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4408920985006242e-16 0 0 -4.4408920985006242e-16 0.99999999999999978 0 0
		 0 0 1 0 1.2212452847360253e-14 -36.000007629394553 8.7209895935339042e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214408 -0.16439898730535335 0 0 0.16439898730535335 0.98639392383214408 0 0
		 0 0 1 0 -17.261894921327158 -30.578219164384944 -1.38199746014171e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.7192471324942728e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.7192471324942728e-15 0 0
		 0 0 1 0 34.000007629394595 -17.999999999999908 -7.7091537226431104e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247804213e-06 -2.994222926207424e-07 -0.99999999999940581 0
		 -0.2747211278969825 0.96152394764093596 8.9278331431787287e-17 0 0.96152394764036508 0.27472112789681941 -1.0899135974441996e-06 0
		 4.6702780378531381 -16.345901720294645 17.999999999989289 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999940592 -1.1627350681068416e-12 -1.0899136849089665e-06 0
		 -2.5124902887767275e-14 0.99999999999940592 -1.0899135950987825e-06 0 1.089913684929747e-06 1.0899135951530265e-06 0.99999999999881206 0
		 -17.999999999989527 -15.000007629367252 -6.9999640328497588 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214353 0.16439898730535721 2.4497359030833279e-09 0
		 0.16439898730535724 0.9863939238321433 1.4698415418499968e-08 0 4.1359030627651356e-25 1.4901161760401541e-08 -0.99999999999999978 0
		 -17.261894921327233 -30.578219164384883 -4.5565098456175792e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631564036977427e-10 -0.99999999999999978 2.4497359030833271e-09 0
		 -0.99999999999999956 4.163157123842461e-10 1.4698415418499966e-08 0 -1.4698415419519835e-08 -2.4497358969641473e-09 -0.99999999999999978 0
		 34.000007621900863 -18.000000014154704 -4.5565097401004482e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693203042193854e-09 -3.2534558118605259e-10 -0.99999999999999956 0
		 -0.27657365741139095 0.96099272215042586 3.7029428008592097e-10 0 0.96099272215042597 0.27657365741139101 -2.4629808628797853e-09 0
		 4.7017523716721898 -16.336876213305118 -18.000000013372333 1;
	setAttr ".pm[11]" -type "matrix" 1 3.6583863775667376e-10 -2.4687521240472666e-09 0
		 -3.7170310235291512e-10 0.99999717760308426 -0.002375875810039005 0 2.4678760555384684e-09 0.0023758758100390067 0.99999717760308438 0
		 18.000000012614255 -15.004705718946092 -6.9548525207207144 1;
	setAttr ".pm[12]" -type "matrix" -7.8197106089471832e-16 -7.2875304467787601e-15 -0.99999999999999978 0
		 -0.99161147425390106 0.12925433891364821 -1.3273513283553228e-16 0 0.12925433891364824 0.99161147425390128 -7.2312523864193332e-15 0
		 10.907733782188084 -1.4217987141824244 8.0000000000000089 1;
	setAttr ".pm[13]" -type "matrix" 2.4565718295886198e-15 -6.5397203800527145e-15 1 0
		 -0.94953047617381503 0.31367479149132727 4.3501369248708397e-15 0 -0.31367479149132732 -0.94953047617381536 -5.5353187261920797e-15 0
		 5.9016241406275283 -1.2632052676650838 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2922971722798794e-13 -2.9338779689594659e-15 -0.99999999999999978 0
		 -0.24253388348224164 0.97014293553219344 2.8530102516174523e-14 0 0.97014293553219377 0.24253388348224164 -1.2598664247092364e-13 0
		 1.2126768190154618 -0.72760905204993032 8.0000000000001688 1;
	setAttr ".pm[15]" -type "matrix" -0.99999999999999978 2.8280890833961466e-14 1.2522120202766604e-13 0
		 2.8335813486865124e-14 0.99999999999999956 -1.1934897514756154e-15 0 -1.2509846405122349e-13 -1.3045120539309953e-15 -1 0
		 8.0000000000001688 -7.6293947775644045e-06 2.9999999999989981 1;
	setAttr ".pm[16]" -type "matrix" -7.8197106089470639e-16 -7.2875304467787617e-15 -0.99999999999999978 0
		 -0.99161147425390117 0.12925433891364654 -1.6084662077247346e-16 0 0.1292543389136466 0.9916114742539015 -7.3395496111505686e-15 0
		 10.907733782188073 -1.4217987141825199 -8.000000000000016 1;
	setAttr ".pm[17]" -type "matrix" 2.4565718295886379e-15 4.5625098661988438e-15 0.99999999999999978 0
		 -0.94953047617381414 0.31367479149132982 8.9575865522611991e-16 0 -0.31367479149132993 -0.94953047617381447 5.1148844708537423e-15 0
		 5.9016241406275345 -1.2632052676651138 8.0000000000000142 1;
	setAttr ".pm[18]" -type "matrix" -1.7288783470699686e-14 -2.1121345273485405e-15 -1 0
		 -0.24253388348224719 0.97014293553219222 2.1497302381529649e-15 0 0.97014293553219255 0.24253388348224719 -1.7296932986024363e-14 0
		 1.2126768190143067 -0.72760905204996984 -7.9999999999999813 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 2.2517500859091247e-15 -1.7715685347081939e-14 0
		 -2.2746303284232958e-15 0.99999999999999978 -2.2759572004815319e-15 0 1.7629999893411907e-14 2.1926904736347228e-15 1 0
		 7.9999999999999805 -7.6293945235888275e-06 -3.0000000000001434 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 13 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 13 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 10;
	setAttr ".ucm" yes;
	setAttr -s 13 ".ifcl";
createNode tweak -n "tweak7";
	rename -uid "4A39717B-4D89-ECAF-6E42-43AD0AFF7886";
createNode objectSet -n "skinCluster7Set";
	rename -uid "28790C78-4E26-7046-E785-EC8EC6BFE6A7";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster7GroupId";
	rename -uid "2E807D8C-45CF-953F-4B3E-47957BFBD1E0";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster7GroupParts";
	rename -uid "59A121EB-46F3-7260-DFB6-A2BB310BFA56";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet7";
	rename -uid "34DC0FCF-464F-B0D9-B79A-4DADAA6E725E";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId244";
	rename -uid "D98C2D27-4A53-D073-10CF-2AB1A54BBE31";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts35";
	rename -uid "04C614EC-4B86-8380-CCA5-BEA9130DA2C8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster8";
	rename -uid "26B3E1D5-4724-D6BB-F72B-0EAA41AF7FAC";
	setAttr -s 32 ".wl";
	setAttr ".wl[0:31].w"
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -10.999999999999996 -1.998403138391183e-15 -4.884981308350688e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-16 -0.99999999999999978 0 0 0.99999999999999978 2.2204460492503121e-16 0 0
		 0 0 1 0 -18.000000000000004 -1.7763585334661548e-15 -1.5777218104420236e-30 1;
	setAttr ".pm[3]" -type "matrix" 0.99999999999999978 4.4408920985006242e-16 0 0 -4.4408920985006242e-16 0.99999999999999978 0 0
		 0 0 1 0 1.2212452847360253e-14 -36.000007629394553 8.7209895935339042e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214408 -0.16439898730535335 0 0 0.16439898730535335 0.98639392383214408 0 0
		 0 0 1 0 -17.261894921327158 -30.578219164384944 -1.38199746014171e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.7192471324942728e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.7192471324942728e-15 0 0
		 0 0 1 0 34.000007629394595 -17.999999999999908 -7.7091537226431104e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247804213e-06 -2.994222926207424e-07 -0.99999999999940581 0
		 -0.2747211278969825 0.96152394764093596 8.9278331431787287e-17 0 0.96152394764036508 0.27472112789681941 -1.0899135974441996e-06 0
		 4.6702780378531381 -16.345901720294645 17.999999999989289 1;
	setAttr ".pm[7]" -type "matrix" 0.99999999999940592 -1.1627350681068416e-12 -1.0899136849089665e-06 0
		 -2.5124902887767275e-14 0.99999999999940592 -1.0899135950987825e-06 0 1.089913684929747e-06 1.0899135951530265e-06 0.99999999999881206 0
		 -17.999999999989527 -15.000007629367252 -6.9999640328497588 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214353 0.16439898730535721 2.4497359030833279e-09 0
		 0.16439898730535724 0.9863939238321433 1.4698415418499968e-08 0 4.1359030627651356e-25 1.4901161760401541e-08 -0.99999999999999978 0
		 -17.261894921327233 -30.578219164384883 -4.5565098456175792e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631564036977427e-10 -0.99999999999999978 2.4497359030833271e-09 0
		 -0.99999999999999956 4.163157123842461e-10 1.4698415418499966e-08 0 -1.4698415419519835e-08 -2.4497358969641473e-09 -0.99999999999999978 0
		 34.000007621900863 -18.000000014154704 -4.5565097401004482e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693203042193854e-09 -3.2534558118605259e-10 -0.99999999999999956 0
		 -0.27657365741139095 0.96099272215042586 3.7029428008592097e-10 0 0.96099272215042597 0.27657365741139101 -2.4629808628797853e-09 0
		 4.7017523716721898 -16.336876213305118 -18.000000013372333 1;
	setAttr ".pm[11]" -type "matrix" 1 3.6583863775667376e-10 -2.4687521240472666e-09 0
		 -3.7170310235291512e-10 0.99999717760308426 -0.002375875810039005 0 2.4678760555384684e-09 0.0023758758100390067 0.99999717760308438 0
		 18.000000012614255 -15.004705718946092 -6.9548525207207144 1;
	setAttr ".pm[12]" -type "matrix" -7.8197106089471832e-16 -7.2875304467787601e-15 -0.99999999999999978 0
		 -0.99161147425390106 0.12925433891364821 -1.3273513283553228e-16 0 0.12925433891364824 0.99161147425390128 -7.2312523864193332e-15 0
		 10.907733782188084 -1.4217987141824244 8.0000000000000089 1;
	setAttr ".pm[13]" -type "matrix" 2.4565718295886198e-15 -6.5397203800527145e-15 1 0
		 -0.94953047617381503 0.31367479149132727 4.3501369248708397e-15 0 -0.31367479149132732 -0.94953047617381536 -5.5353187261920797e-15 0
		 5.9016241406275283 -1.2632052676650838 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2922971722798794e-13 -2.9338779689594659e-15 -0.99999999999999978 0
		 -0.24253388348224164 0.97014293553219344 2.8530102516174523e-14 0 0.97014293553219377 0.24253388348224164 -1.2598664247092364e-13 0
		 1.2126768190154618 -0.72760905204993032 8.0000000000001688 1;
	setAttr ".pm[15]" -type "matrix" -0.99999999999999978 2.8280890833961466e-14 1.2522120202766604e-13 0
		 2.8335813486865124e-14 0.99999999999999956 -1.1934897514756154e-15 0 -1.2509846405122349e-13 -1.3045120539309953e-15 -1 0
		 8.0000000000001688 -7.6293947775644045e-06 2.9999999999989981 1;
	setAttr ".pm[16]" -type "matrix" -7.8197106089470639e-16 -7.2875304467787617e-15 -0.99999999999999978 0
		 -0.99161147425390117 0.12925433891364654 -1.6084662077247346e-16 0 0.1292543389136466 0.9916114742539015 -7.3395496111505686e-15 0
		 10.907733782188073 -1.4217987141825199 -8.000000000000016 1;
	setAttr ".pm[17]" -type "matrix" 2.4565718295886379e-15 4.5625098661988438e-15 0.99999999999999978 0
		 -0.94953047617381414 0.31367479149132982 8.9575865522611991e-16 0 -0.31367479149132993 -0.94953047617381447 5.1148844708537423e-15 0
		 5.9016241406275345 -1.2632052676651138 8.0000000000000142 1;
	setAttr ".pm[18]" -type "matrix" -1.7288783470699686e-14 -2.1121345273485405e-15 -1 0
		 -0.24253388348224719 0.97014293553219222 2.1497302381529649e-15 0 0.97014293553219255 0.24253388348224719 -1.7296932986024363e-14 0
		 1.2126768190143067 -0.72760905204996984 -7.9999999999999813 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 2.2517500859091247e-15 -1.7715685347081939e-14 0
		 -2.2746303284232958e-15 0.99999999999999978 -2.2759572004815319e-15 0 1.7629999893411907e-14 2.1926904736347228e-15 1 0
		 7.9999999999999805 -7.6293945235888275e-06 -3.0000000000001434 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 14 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 14 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 10;
	setAttr ".ucm" yes;
	setAttr -s 14 ".ifcl";
createNode tweak -n "tweak8";
	rename -uid "38FF703A-4C05-F367-DA27-7DABC08BD9AE";
createNode objectSet -n "skinCluster8Set";
	rename -uid "F9F40E12-434C-B37C-781F-D58F17284E41";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster8GroupId";
	rename -uid "B15B8811-4BD5-FE0D-EB85-75AC46FBACA4";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster8GroupParts";
	rename -uid "4118AF80-4B8F-5027-3D21-0599D2291F48";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet8";
	rename -uid "86C50481-4AAC-702B-8765-8299A5AF35C1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId246";
	rename -uid "0667B04C-4EE0-979C-E41E-24B83C25C8D5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts37";
	rename -uid "9083F48C-4530-690E-F329-A5B31E7CE883";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode lambert -n "lambert2";
	rename -uid "A1263BB9-4A05-D6B8-6905-DEB4D56CEDE1";
createNode shadingEngine -n "pSphere1SG";
	rename -uid "9AA41859-4B97-355D-D289-52A21C19C24B";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "9175F6F5-4C57-2191-D0A2-5E9B94216BB5";
createNode objectSet -n "pCubeShape20HiddenFacesSet";
	rename -uid "CC0968AB-47F3-AE67-4943-F7B06E497946";
	setAttr ".ihi" 0;
createNode objectSet -n "pCubeShape20HiddenFacesSet1";
	rename -uid "055450B7-4141-177A-8025-7D88A22C85EE";
	setAttr ".ihi" 0;
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "74B15D5F-4E3C-845C-7FDC-02A5A8EFAE8C";
	setAttr ".ics" -type "componentList" 4 "e[28:31]" "e[36]" "e[41]" "e[50:51]";
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "0A9F93F9-4716-6571-4383-3C989FBB70D7";
	setAttr ".ics" -type "componentList" 7 "f[0]" "f[5:6]" "f[12]" "f[15]" "f[20]" "f[23]" "f[29]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" 0 8.6210335026192944 -7.533938117639817 ;
	setAttr ".s" -type "double3" 1 0.43708684372602724 0.62373956709966494 ;
	setAttr ".pvt" -type "float3" 0 36.171238 -24.015671 ;
	setAttr ".rs" 56977;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -9.0396308898925781 23 -19.039632797241211 ;
	setAttr ".cbx" -type "double3" 9.0396308898925781 32.100406646728516 -13.92385196685791 ;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "2AA255ED-494C-C60D-87DE-DCA163519DB5";
	setAttr ".ics" -type "componentList" 10 "e[5]" "e[14]" "e[20]" "e[36]" "e[40]" "e[43]" "e[73]" "e[75:76]" "e[93]" "e[96:97]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "63AEE8E4-4105-62EC-FB70-E4A55A5B854A";
	setAttr ".ics" -type "componentList" 8 "e[0]" "e[10]" "e[14]" "e[22:24]" "e[51]" "e[53:54]" "e[67]" "e[69:70]";
	setAttr ".cv" yes;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "FBF5A0F5-4F3A-9128-5253-19AA1666446B";
	setAttr ".ics" -type "componentList" 5 "f[2:3]" "f[7]" "f[11]" "f[16]" "f[19:29]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".s" -type "double3" 0.92456639785305816 1 1 ;
	setAttr ".pvt" -type "float3" 0 30.580036 -19.964582 ;
	setAttr ".rs" 38902;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -9.0396318435668945 23 -26.968793869018555 ;
	setAttr ".cbx" -type "double3" 9.0396318435668945 38.160072326660156 -12.960369110107422 ;
createNode polyTweak -n "polyTweak10";
	rename -uid "C72A2E9C-40EA-F639-740F-E98E7B3DFDAB";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[28]" -type "float3" 0 2.2544339 -1.3576609 ;
	setAttr ".tk[29]" -type "float3" 0 2.2544339 -1.3576609 ;
	setAttr ".tk[34]" -type "float3" 0 2.2544339 -1.3576609 ;
	setAttr ".tk[35]" -type "float3" 0 2.2544339 -1.3576609 ;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "21A76A5C-438A-4FBF-3AC0-409F168CB932";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[7]" "e[18]" "e[45]" "e[68]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak11";
	rename -uid "CC6A8F82-4D5A-2877-D78D-CDA93BAF7D72";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[26]" -type "float3" 0.80369312 0 0 ;
	setAttr ".tk[27]" -type "float3" 1.0652641 0 0 ;
	setAttr ".tk[29]" -type "float3" 0.80369312 0 0 ;
	setAttr ".tk[30]" -type "float3" 0 0 -1.179075 ;
	setAttr ".tk[31]" -type "float3" 0.80369312 0 -1.179075 ;
	setAttr ".tk[33]" -type "float3" 0.80369312 0 0 ;
	setAttr ".tk[34]" -type "float3" 0 1.7380093 0 ;
	setAttr ".tk[35]" -type "float3" 0.80369312 1.7380093 0 ;
	setAttr ".tk[36]" -type "float3" -1.0652641 0 0 ;
	setAttr ".tk[38]" -type "float3" -0.80369312 0 0 ;
	setAttr ".tk[41]" -type "float3" -0.80369312 0 0 ;
	setAttr ".tk[42]" -type "float3" 0 0 -1.179075 ;
	setAttr ".tk[43]" -type "float3" -0.80369312 0 -1.179075 ;
	setAttr ".tk[44]" -type "float3" -0.80369312 0 0 ;
	setAttr ".tk[46]" -type "float3" 0 1.7380093 0 ;
	setAttr ".tk[47]" -type "float3" -0.80369312 1.7380093 0 ;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "D54111B5-451A-846F-9EB2-57813AA3FC10";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[34]" "e[36]" "e[38]" "e[40]" "e[60]" "e[62]" "e[64:65]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweak -n "polyTweak12";
	rename -uid "7045C5C2-443E-492B-6983-AE9C5D6C8083";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[0]" -type "float3" -5.9604645e-08 4.500001 0 ;
	setAttr ".tk[2]" -type "float3" -5.9604645e-08 2.500001 0 ;
	setAttr ".tk[8]" -type "float3" 0 -0.5 0 ;
	setAttr ".tk[9]" -type "float3" 0 -0.5 0 ;
	setAttr ".tk[10]" -type "float3" 0 1.5 0 ;
	setAttr ".tk[11]" -type "float3" 0 1.5 0 ;
	setAttr ".tk[12]" -type "float3" -5.9604645e-08 4.500001 0 ;
	setAttr ".tk[14]" -type "float3" -5.9604645e-08 2.500001 0 ;
	setAttr ".tk[20]" -type "float3" 0 -0.5 0 ;
	setAttr ".tk[21]" -type "float3" 0 -0.5 0 ;
	setAttr ".tk[22]" -type "float3" 0 1.5 0 ;
	setAttr ".tk[23]" -type "float3" 0 1.5 0 ;
	setAttr ".tk[34]" -type "float3" 0 0.7619915 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.7619915 0 ;
	setAttr ".tk[46]" -type "float3" 0 0.7619915 0 ;
	setAttr ".tk[47]" -type "float3" 0 0.7619915 0 ;
	setAttr ".tk[48]" -type "float3" 0 -9.5367432e-07 0 ;
	setAttr ".tk[49]" -type "float3" 0 -9.5367432e-07 0 ;
	setAttr ".tk[50]" -type "float3" 0 -9.5367432e-07 0 ;
	setAttr ".tk[51]" -type "float3" 0 -9.5367432e-07 0 ;
createNode polySplit -n "polySplit3";
	rename -uid "8972F119-4EB0-3332-A107-2C994501B1A8";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483578 -2147483600;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit4";
	rename -uid "6F6DE44D-4595-C00D-C016-0A8437AFFC28";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483571 -2147483586;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "DAC511BC-4044-ACDE-6AFB-399C28A920BE";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483574 -2147483595;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit6";
	rename -uid "D565EC47-40DE-94BA-E5F8-1D9653CF4B1C";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483580 -2147483613;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyNormal -n "polyNormal1";
	rename -uid "82ECDD79-4775-BD48-082D-079FAFEA90F8";
	setAttr ".ics" -type "componentList" 1 "f[0:11]";
	setAttr ".unm" no;
createNode groupId -n "groupId267";
	rename -uid "495C83AE-46FB-C3D2-E4AB-85B9774CB66F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts40";
	rename -uid "3CA7AEAD-4CD7-35D9-49E6-69BBFA17AD7A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:11]";
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "E98BA757-4064-31F0-3A80-2986D0688937";
	setAttr ".ics" -type "componentList" 1 "f[0:11]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".t" -type "double3" 0 5.8736277325529045 0 ;
	setAttr ".pvt" -type "float3" 0 40.958523 4.7683716e-07 ;
	setAttr ".rs" 34280;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -12.960332870483398 33.169773101806641 -12.999999046325684 ;
	setAttr ".cbx" -type "double3" 12.960332870483398 37 13 ;
createNode polyNormal -n "polyNormal2";
	rename -uid "7B801957-4F07-F743-A59A-53A7ECB03937";
	setAttr ".ics" -type "componentList" 1 "f[0:47]";
	setAttr ".unm" no;
createNode polyTweak -n "polyTweak13";
	rename -uid "C6FD6F06-47EA-FEB8-2CE6-2F9E97C8D2B6";
	setAttr ".uopa" yes;
	setAttr -s 48 ".tk[0:47]" -type "float3"  0 2.3841858e-07 0 0 2.3841858e-07
		 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07
		 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07
		 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07
		 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 2.3841858e-07
		 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 4.92896271 0 0 2.95660019 0 0 2.95660019
		 0 0 4.92896271 0 0 2.95660019 -3.3759861 0 2.95660019 0 0 4.92896271 0 0 4.92896271
		 -3.37598538 0 2.95660019 0 0 4.92896271 0 0 2.95660019 -3.3759861 0 4.92896271 -3.37598538
		 0 2.95660019 3.3759861 0 4.92896271 3.37598586 0 2.95660019 3.3759861 0 4.92896271
		 3.37598586 0 -0.87362671 0 0 -0.87362671 0 0 1.098735809 0 0 1.098735809 0 0 -0.87362671
		 0 0 -0.87362671 0 0 1.098735809 0 0 1.098735809 0;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "4B3A2AAD-4A51-9246-84A3-1E88766FBE92";
	setAttr ".dc" -type "componentList" 1 "f[0:11]";
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "A587137D-4E3D-3540-508D-32BBAA18580F";
	setAttr ".ics" -type "componentList" 9 "e[44]" "e[53]" "e[57]" "e[61]" "e[66]" "e[72]" "e[76]" "e[79:80]" "e[83]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 29;
	setAttr ".sv2" 25;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "E6BCAA99-4084-54D4-ADD9-8B9039C6DAA3";
	setAttr ".ics" -type "componentList" 8 "e[47]" "e[55]" "e[59]" "e[63]" "e[69]" "e[75]" "e[77:78]" "e[81:82]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 27;
	setAttr ".sv2" 33;
	setAttr ".d" 1;
createNode polyDelEdge -n "polyDelEdge3";
	rename -uid "48048610-4291-99DC-92FE-F3A97EA17F26";
	setAttr ".ics" -type "componentList" 8 "e[22]" "e[24]" "e[64]" "e[67]" "e[70]" "e[74]" "e[86]" "e[89]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak14";
	rename -uid "76F9A4C3-4D05-35EC-C7BB-F48CC3D808EB";
	setAttr ".uopa" yes;
	setAttr -s 48 ".tk[0:47]" -type "float3"  0.90686989 0 -0.9096452 -0.90686989
		 0 -0.9096452 -0.70996904 0 -0.71214187 0.70996904 0 -0.71214187 -0.70996904 0 0.36665279
		 -0.90686989 0 0.36665255 -0.90686989 0 0.90964526 -0.70996904 0 0.71214187 0.90686989
		 0 0.90964526 0.70996904 0 0.71214187 0.90686989 0 0.36665255 0.70996904 0 0.36665279
		 0.70996904 0 -0.36665368 0.90686989 0 -0.36665356 -0.90686989 0 -0.36665356 -0.70996904
		 0 -0.36665368 0.70996904 0 0.36665279 0.90686989 0 0.36665255 0.90686989 0 -0.36665356
		 0.70996904 0 -0.36665368 -0.70996904 0 -0.36665368 -0.90686989 0 -0.36665356 -0.90686989
		 0 0.36665255 -0.70996904 0 0.36665279 0.90686989 0 -0.9096452 0.70996904 0 -0.71214187
		 -0.70996904 0 -0.71214187 -0.90686989 0 -0.9096452 -0.70996904 0 0.60287976 -0.70996904
		 0 0.71214187 -0.90686989 0 0.90964526 -0.90686989 0 0.60287952 0.70996904 0 0.71214187
		 0.90686989 0 0.90964526 0.70996904 0 0.60287976 0.90686989 0 0.60287952 0.70996904
		 0 -0.60288048 0.90686989 0 -0.60288036 -0.70996904 0 -0.60288048 -0.90686989 0 -0.60288036
		 0.70996904 0 -0.36665368 0.70996904 0 0.36665279 0.90686989 0 -0.36665356 0.90686989
		 0 0.36665255 -0.70996904 0 -0.36665368 -0.70996904 0 0.36665279 -0.90686989 0 0.36665255
		 -0.90686989 0 -0.36665356;
createNode polyDelEdge -n "polyDelEdge4";
	rename -uid "CF75A4E2-475C-F945-9FC2-AEB418826F6E";
	setAttr ".ics" -type "componentList" 6 "e[20]" "e[23]" "e[58]" "e[60:62]" "e[71]" "e[74]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge5";
	rename -uid "B0CCE4B8-490F-53F1-1458-8BBD43E836BD";
	setAttr ".ics" -type "componentList" 7 "e[14]" "e[18]" "e[44]" "e[46]" "e[48]" "e[50]" "e[57:58]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge6";
	rename -uid "0B046B32-4369-DA7F-C462-EDAD64D64A5A";
	setAttr ".ics" -type "componentList" 7 "e[4]" "e[12]" "e[24]" "e[28]" "e[34]" "e[36]" "e[42:43]";
	setAttr ".cv" yes;
createNode objectSet -n "set1";
	rename -uid "CB2CAB91-4A26-6757-01A3-798E658DCC89";
	setAttr ".ihi" 0;
createNode objectSet -n "tweakSet9";
	rename -uid "63067198-4B27-D9D2-6509-30AC8AA80D13";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId248";
	rename -uid "6D438B8B-442F-9D42-59B7-4EA297DE5606";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts39";
	rename -uid "B8C156C5-4DE5-9386-AFF1-D6BA96CBC87E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode tweak -n "tweak9";
	rename -uid "C80FEDBC-41FA-21EE-259A-8691AB3C1E1B";
createNode polyDelEdge -n "polyDelEdge7";
	rename -uid "DC0410F3-413B-B81D-8972-E3B7F7A205D2";
	setAttr ".ics" -type "componentList" 12 "e[110]" "e[115]" "e[118]" "e[123]" "e[126]" "e[129]" "e[133]" "e[137]" "e[140]" "e[143]" "e[149]" "e[165]";
	setAttr ".cv" yes;
createNode deleteComponent -n "deleteComponent2";
	rename -uid "29EAF8B6-4970-3832-5DBC-C89F1276C1A8";
	setAttr ".dc" -type "componentList" 12 "f[41]" "f[43]" "f[45]" "f[47]" "f[49]" "f[51]" "f[53]" "f[55]" "f[57]" "f[59]" "f[64]" "f[79]";
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "04A2441E-4709-DC20-0AB4-B098DC1A800D";
	setAttr ".ics" -type "componentList" 1 "f[0:67]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 34.747142791748047 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -4.7683716e-07 38.571068 -0.27671623 ;
	setAttr ".rs" 35582;
	setAttr ".lt" -type "double3" -9.4368957093138306e-16 6.8278716014447127e-15 -0.67508955903050416 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -12.733916282653809 33.16977334022522 -13.08349609375 ;
	setAttr ".cbx" -type "double3" 12.733915328979492 43.972363471984863 12.530063629150391 ;
createNode polyTweak -n "polyTweak15";
	rename -uid "2A77DF2B-4500-48EA-4E2E-279817577E06";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk[124:147]" -type "float3"  -0.15250605 0.11757283 -0.22423199
		 -0.24501961 0.13010778 -0.15676251 -0.23772463 0 -0.15486258 -0.14827088 0 -0.22260389
		 -0.26060861 0.14645369 0.015123574 -0.25487226 0 0.011923641 -0.24600148 0.14173868
		 0.20869121 -0.15470541 0.11907582 0.25341344 -0.14985844 0 0.25065464 -0.23804531
		 0 0.20363712 8.1024325e-08 0.1041109 0.2621721 5.425597e-08 0 0.25934643 0.15470538
		 0.11907582 0.25341326 0.14985847 0 0.25065452 0.24600148 0.14173868 0.20869088 0.23804533
		 0 0.20363659 0.25487226 0 0.011923388 0.26060861 0.14645369 0.015123403 0.24501961
		 0.13010778 -0.15676248 0.23772463 0 -0.15486255 0.15250617 0.11757283 -0.22423199
		 0.14827088 0 -0.22260386 1.0128041e-08 0.11840066 -0.2621721 4.817825e-10 0 -0.2558409;
createNode polyDelEdge -n "polyDelEdge8";
	rename -uid "81640AAA-46BE-353B-14E4-68AFA63D3E65";
	setAttr ".ics" -type "componentList" 12 "e[224]" "e[229]" "e[234]" "e[239]" "e[243]" "e[247]" "e[253]" "e[257]" "e[261]" "e[263]" "e[282]" "e[294]";
	setAttr ".cv" yes;
createNode groupId -n "groupId268";
	rename -uid "DE70B1B9-4B8A-B02D-F129-A59A2E0EFFE8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts41";
	rename -uid "9902A1D4-4F2E-6323-0B5A-A8AE6975302A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 22 "e[78]" "e[82]" "e[85]" "e[89]" "e[92]" "e[95]" "e[99]" "e[102]" "e[105]" "e[108]" "e[110:112]" "e[115:116]" "e[118:120]" "e[123:124]" "e[126:127]" "e[129:130]" "e[132:134]" "e[137:138]" "e[140:141]" "e[143]" "e[149:150]" "e[164:165]";
createNode skinCluster -n "skinCluster9";
	rename -uid "B0285C92-4FE0-EAFF-54AD-7D8A4581AF1E";
	setAttr -s 138 ".wl";
	setAttr ".wl[0:137].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -11 -1.9984031383911763e-15 -4.8849813083506888e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -18 -1.776358533466145e-15 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -3.7747620953737949e-15 -36.000007629394531 8.7209893915251267e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214453 -0.16439898730535343 0 0 0.16439898730535343 0.98639392383214453 0 0
		 0 0 1 0 -17.261894921327137 -30.578219164384933 -1.3819974421032562e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.4416913763379837e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.4416913763379837e-15 0 0
		 0 0 1 0 34.00000766028554 -18.000000185345737 -7.7091534788882934e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247499207e-06 -2.9942229272749301e-07 -0.99999999999940548 0
		 -0.27472112789698266 0.96152394764093563 9.9739717725969859e-18 0 0.96152394764036475 0.27472112789681941 -1.0899135973298236e-06 0
		 4.6702780463397309 -16.345901749996973 18.000000185335107 1;
	setAttr ".pm[7]" -type "matrix" 0.9999999999994057 -1.1628765895575732e-12 -1.0899136848022152e-06 0
		 -2.4962331691727e-14 0.9999999999994057 -1.0899135945436703e-06 0 1.0899136847043486e-06 1.0899135943203584e-06 0.99999999999881184 0
		 -18.000000185335342 -15.000007662161799 -6.9999640261868867 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214375 0.16439898730535726 2.4497368521849414e-09 0
		 0.16439898730535729 0.98639392383214353 1.4698421113109649e-08 0 -4.1359030627651375e-25 1.4901167533561269e-08 -0.99999999999999978 0
		 -17.261894921327244 -30.578219164384862 -4.5565116109470115e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631522403611227e-10 -1 2.4497368521849414e-09 0
		 -0.99999999999999978 4.1631529605063993e-10 1.4698421113109649e-08 0 -1.4698421114129511e-08 -2.4497368460657649e-09 -0.99999999999999978 0
		 34.000007652791822 -18.000000199500565 -4.5565115054298801e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693209322700799e-09 -3.2534683058010462e-10 -0.99999999999999978 0
		 -0.27657365741139739 0.96099272215042419 3.7029343698581624e-10 0 0.96099272215042419 0.27657365741139722 -2.4629819348047793e-09 0
		 4.7017523802160115 -16.336876242991089 -18.00000019871819 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999956 3.6583736869134254e-10 -2.4687523309694251e-09 0
		 -3.7170209060962485e-10 0.99999717760308415 -0.0023758758100449989 0 2.4678762396832115e-09 0.0023758758100448896 0.99999717760308393 0
		 18.000000197960102 -15.004705710865565 -6.9548526572791962 1;
	setAttr ".pm[12]" -type "matrix" -3.5301351571464157e-16 -6.8955981264935312e-15 -1 0
		 -0.99161147425390128 0.12925433891364818 -5.2946000530483817e-16 0 0.12925433891364818 0.99161147425390128 -6.7316942088131252e-15 0
		 10.907733782188075 -1.4217987141824262 8.0000000000000053 1;
	setAttr ".pm[13]" -type "matrix" 2.6730615658509697e-15 -1.1075733536914241e-14 1 0
		 -0.94953047617381503 0.31367479149132715 6.000558110069543e-15 0 -0.3136747914913271 -0.94953047617381481 -9.8299630538315332e-15 0
		 5.9016242114745472 -1.2632053017725899 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2871897274151038e-13 -3.4855117479312523e-15 -1 0
		 -0.24253388348224064 0.97014293553219377 2.7849041457073148e-14 0 0.97014293553219355 0.24253388348224064 -1.2556946823042317e-13 0
		 1.2126768531177115 -0.72760905338516302 8.0000000000001652 1;
	setAttr ".pm[15]" -type "matrix" -1 2.7406441015163463e-14 1.2399783673555757e-13 0
		 2.7418830035030903e-14 0.99999999999999978 -1.7763568394037033e-15 0 -1.2390413369348544e-13 -1.7486012637811782e-15 -0.99999999999999956 0
		 8.0000000000001634 -7.6769425058733829e-06 2.9999998153119849 1;
	setAttr ".pm[16]" -type "matrix" -3.3807466072319389e-16 -6.6736223841650137e-15 -1 0
		 -0.99161147425390139 0.12925433891364663 -5.4322137893231732e-16 0 0.12925433891364663 0.9916114742539015 -6.729900444524443e-15 0
		 10.907733782188073 -1.4217987141825152 -7.9999999999999929 1;
	setAttr ".pm[17]" -type "matrix" 2.5902343882339624e-15 6.037260639988876e-15 1 0
		 -0.94953047617381414 0.31367479149133021 5.8163546186962406e-16 0 -0.31367479149133026 -0.94953047617381414 6.6136164986905321e-15 0
		 5.9016242114745845 -1.2632053017726599 7.9999999999999947 1;
	setAttr ".pm[18]" -type "matrix" -2.3654363560712546e-14 -2.1362752008768194e-15 -1 0
		 -0.24253388348224733 0.97014293553219233 3.6486269189619435e-15 0 0.97014293553219233 0.24253388348224728 -2.3534795120364372e-14 0
		 1.2126768531165262 -0.7276090533852021 -7.9999999999999689 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 3.704882469169215e-15 -2.3570574492240229e-14 0
		 -3.6902602823853861e-15 0.99999999999999978 -1.5265566588595037e-15 0 2.3645817422826884e-14 1.5265566588596768e-15 0.99999999999999978 0
		 7.9999999999999662 -7.6769422470462994e-06 -2.9999998153131311 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 34.747142791748047 0 1;
	setAttr -s 9 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 9 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 9 ".ifcl";
createNode objectSet -n "skinCluster9Set";
	rename -uid "84525566-41E5-807E-86D2-9CB272C28DEE";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster9GroupId";
	rename -uid "A4BC9CDA-449D-BBDE-4758-ACB2735DDC02";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster9GroupParts";
	rename -uid "4F40611B-40FE-2C15-D98B-FF8F63DB744C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster10";
	rename -uid "51BEA4E1-47F4-CF4C-94F7-C89FE5BF642B";
	setAttr -s 16 ".wl";
	setAttr ".wl[0:15].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -11 -1.9984031383911763e-15 -4.8849813083506888e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -18 -1.776358533466145e-15 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -3.7747620953737949e-15 -36.000007629394531 8.7209893915251267e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214453 -0.16439898730535343 0 0 0.16439898730535343 0.98639392383214453 0 0
		 0 0 1 0 -17.261894921327137 -30.578219164384933 -1.3819974421032562e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.4416913763379837e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.4416913763379837e-15 0 0
		 0 0 1 0 34.00000766028554 -18.000000185345737 -7.7091534788882934e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247499207e-06 -2.9942229272749301e-07 -0.99999999999940548 0
		 -0.27472112789698266 0.96152394764093563 9.9739717725969859e-18 0 0.96152394764036475 0.27472112789681941 -1.0899135973298236e-06 0
		 4.6702780463397309 -16.345901749996973 18.000000185335107 1;
	setAttr ".pm[7]" -type "matrix" 0.9999999999994057 -1.1628765895575732e-12 -1.0899136848022152e-06 0
		 -2.4962331691727e-14 0.9999999999994057 -1.0899135945436703e-06 0 1.0899136847043486e-06 1.0899135943203584e-06 0.99999999999881184 0
		 -18.000000185335342 -15.000007662161799 -6.9999640261868867 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214375 0.16439898730535726 2.4497368521849414e-09 0
		 0.16439898730535729 0.98639392383214353 1.4698421113109649e-08 0 -4.1359030627651375e-25 1.4901167533561269e-08 -0.99999999999999978 0
		 -17.261894921327244 -30.578219164384862 -4.5565116109470115e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631522403611227e-10 -1 2.4497368521849414e-09 0
		 -0.99999999999999978 4.1631529605063993e-10 1.4698421113109649e-08 0 -1.4698421114129511e-08 -2.4497368460657649e-09 -0.99999999999999978 0
		 34.000007652791822 -18.000000199500565 -4.5565115054298801e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693209322700799e-09 -3.2534683058010462e-10 -0.99999999999999978 0
		 -0.27657365741139739 0.96099272215042419 3.7029343698581624e-10 0 0.96099272215042419 0.27657365741139722 -2.4629819348047793e-09 0
		 4.7017523802160115 -16.336876242991089 -18.00000019871819 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999956 3.6583736869134254e-10 -2.4687523309694251e-09 0
		 -3.7170209060962485e-10 0.99999717760308415 -0.0023758758100449989 0 2.4678762396832115e-09 0.0023758758100448896 0.99999717760308393 0
		 18.000000197960102 -15.004705710865565 -6.9548526572791962 1;
	setAttr ".pm[12]" -type "matrix" -3.5301351571464157e-16 -6.8955981264935312e-15 -1 0
		 -0.99161147425390128 0.12925433891364818 -5.2946000530483817e-16 0 0.12925433891364818 0.99161147425390128 -6.7316942088131252e-15 0
		 10.907733782188075 -1.4217987141824262 8.0000000000000053 1;
	setAttr ".pm[13]" -type "matrix" 2.6730615658509697e-15 -1.1075733536914241e-14 1 0
		 -0.94953047617381503 0.31367479149132715 6.000558110069543e-15 0 -0.3136747914913271 -0.94953047617381481 -9.8299630538315332e-15 0
		 5.9016242114745472 -1.2632053017725899 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2871897274151038e-13 -3.4855117479312523e-15 -1 0
		 -0.24253388348224064 0.97014293553219377 2.7849041457073148e-14 0 0.97014293553219355 0.24253388348224064 -1.2556946823042317e-13 0
		 1.2126768531177115 -0.72760905338516302 8.0000000000001652 1;
	setAttr ".pm[15]" -type "matrix" -1 2.7406441015163463e-14 1.2399783673555757e-13 0
		 2.7418830035030903e-14 0.99999999999999978 -1.7763568394037033e-15 0 -1.2390413369348544e-13 -1.7486012637811782e-15 -0.99999999999999956 0
		 8.0000000000001634 -7.6769425058733829e-06 2.9999998153119849 1;
	setAttr ".pm[16]" -type "matrix" -3.3807466072319389e-16 -6.6736223841650137e-15 -1 0
		 -0.99161147425390139 0.12925433891364663 -5.4322137893231732e-16 0 0.12925433891364663 0.9916114742539015 -6.729900444524443e-15 0
		 10.907733782188073 -1.4217987141825152 -7.9999999999999929 1;
	setAttr ".pm[17]" -type "matrix" 2.5902343882339624e-15 6.037260639988876e-15 1 0
		 -0.94953047617381414 0.31367479149133021 5.8163546186962406e-16 0 -0.31367479149133026 -0.94953047617381414 6.6136164986905321e-15 0
		 5.9016242114745845 -1.2632053017726599 7.9999999999999947 1;
	setAttr ".pm[18]" -type "matrix" -2.3654363560712546e-14 -2.1362752008768194e-15 -1 0
		 -0.24253388348224733 0.97014293553219233 3.6486269189619435e-15 0 0.97014293553219233 0.24253388348224728 -2.3534795120364372e-14 0
		 1.2126768531165262 -0.7276090533852021 -7.9999999999999689 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 3.704882469169215e-15 -2.3570574492240229e-14 0
		 -3.6902602823853861e-15 0.99999999999999978 -1.5265566588595037e-15 0 2.3645817422826884e-14 1.5265566588596768e-15 0.99999999999999978 0
		 7.9999999999999662 -7.6769422470462994e-06 -2.9999998153131311 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 9 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 9 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 9 ".ifcl";
createNode tweak -n "tweak10";
	rename -uid "D02C4D0A-4C07-DD70-B564-1CA3EA5A1B12";
createNode objectSet -n "skinCluster10Set";
	rename -uid "EB4CA5D2-4F7C-6275-6B25-D5BFF17FAD23";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster10GroupId";
	rename -uid "17EF1769-4FF0-FF8F-FC26-31A6EC76A74C";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster10GroupParts";
	rename -uid "84C19FB1-4AA0-2992-105B-4680DEB2F9D2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet10";
	rename -uid "EB3DF683-4991-C623-A4FA-4D825C614E24";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId270";
	rename -uid "808F5CFD-4862-057A-D815-D78CD5CDF254";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts43";
	rename -uid "D914F44C-49B8-022E-0F8D-E488ECF8042B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster11";
	rename -uid "319EE03C-4BEF-A3AE-5702-E697CAB9D486";
	setAttr -s 160 ".wl";
	setAttr ".wl[0:159].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -11 -1.9984031383911763e-15 -4.8849813083506888e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -18 -1.776358533466145e-15 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -3.7747620953737949e-15 -36.000007629394531 8.7209893915251267e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214453 -0.16439898730535343 0 0 0.16439898730535343 0.98639392383214453 0 0
		 0 0 1 0 -17.261894921327137 -30.578219164384933 -1.3819974421032562e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.4416913763379837e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.4416913763379837e-15 0 0
		 0 0 1 0 34.00000766028554 -18.000000185345737 -7.7091534788882934e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247499207e-06 -2.9942229272749301e-07 -0.99999999999940548 0
		 -0.27472112789698266 0.96152394764093563 9.9739717725969859e-18 0 0.96152394764036475 0.27472112789681941 -1.0899135973298236e-06 0
		 4.6702780463397309 -16.345901749996973 18.000000185335107 1;
	setAttr ".pm[7]" -type "matrix" 0.9999999999994057 -1.1628765895575732e-12 -1.0899136848022152e-06 0
		 -2.4962331691727e-14 0.9999999999994057 -1.0899135945436703e-06 0 1.0899136847043486e-06 1.0899135943203584e-06 0.99999999999881184 0
		 -18.000000185335342 -15.000007662161799 -6.9999640261868867 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214375 0.16439898730535726 2.4497368521849414e-09 0
		 0.16439898730535729 0.98639392383214353 1.4698421113109649e-08 0 -4.1359030627651375e-25 1.4901167533561269e-08 -0.99999999999999978 0
		 -17.261894921327244 -30.578219164384862 -4.5565116109470115e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631522403611227e-10 -1 2.4497368521849414e-09 0
		 -0.99999999999999978 4.1631529605063993e-10 1.4698421113109649e-08 0 -1.4698421114129511e-08 -2.4497368460657649e-09 -0.99999999999999978 0
		 34.000007652791822 -18.000000199500565 -4.5565115054298801e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693209322700799e-09 -3.2534683058010462e-10 -0.99999999999999978 0
		 -0.27657365741139739 0.96099272215042419 3.7029343698581624e-10 0 0.96099272215042419 0.27657365741139722 -2.4629819348047793e-09 0
		 4.7017523802160115 -16.336876242991089 -18.00000019871819 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999956 3.6583736869134254e-10 -2.4687523309694251e-09 0
		 -3.7170209060962485e-10 0.99999717760308415 -0.0023758758100449989 0 2.4678762396832115e-09 0.0023758758100448896 0.99999717760308393 0
		 18.000000197960102 -15.004705710865565 -6.9548526572791962 1;
	setAttr ".pm[12]" -type "matrix" -3.5301351571464157e-16 -6.8955981264935312e-15 -1 0
		 -0.99161147425390128 0.12925433891364818 -5.2946000530483817e-16 0 0.12925433891364818 0.99161147425390128 -6.7316942088131252e-15 0
		 10.907733782188075 -1.4217987141824262 8.0000000000000053 1;
	setAttr ".pm[13]" -type "matrix" 2.6730615658509697e-15 -1.1075733536914241e-14 1 0
		 -0.94953047617381503 0.31367479149132715 6.000558110069543e-15 0 -0.3136747914913271 -0.94953047617381481 -9.8299630538315332e-15 0
		 5.9016242114745472 -1.2632053017725899 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2871897274151038e-13 -3.4855117479312523e-15 -1 0
		 -0.24253388348224064 0.97014293553219377 2.7849041457073148e-14 0 0.97014293553219355 0.24253388348224064 -1.2556946823042317e-13 0
		 1.2126768531177115 -0.72760905338516302 8.0000000000001652 1;
	setAttr ".pm[15]" -type "matrix" -1 2.7406441015163463e-14 1.2399783673555757e-13 0
		 2.7418830035030903e-14 0.99999999999999978 -1.7763568394037033e-15 0 -1.2390413369348544e-13 -1.7486012637811782e-15 -0.99999999999999956 0
		 8.0000000000001634 -7.6769425058733829e-06 2.9999998153119849 1;
	setAttr ".pm[16]" -type "matrix" -3.3807466072319389e-16 -6.6736223841650137e-15 -1 0
		 -0.99161147425390139 0.12925433891364663 -5.4322137893231732e-16 0 0.12925433891364663 0.9916114742539015 -6.729900444524443e-15 0
		 10.907733782188073 -1.4217987141825152 -7.9999999999999929 1;
	setAttr ".pm[17]" -type "matrix" 2.5902343882339624e-15 6.037260639988876e-15 1 0
		 -0.94953047617381414 0.31367479149133021 5.8163546186962406e-16 0 -0.31367479149133026 -0.94953047617381414 6.6136164986905321e-15 0
		 5.9016242114745845 -1.2632053017726599 7.9999999999999947 1;
	setAttr ".pm[18]" -type "matrix" -2.3654363560712546e-14 -2.1362752008768194e-15 -1 0
		 -0.24253388348224733 0.97014293553219233 3.6486269189619435e-15 0 0.97014293553219233 0.24253388348224728 -2.3534795120364372e-14 0
		 1.2126768531165262 -0.7276090533852021 -7.9999999999999689 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 3.704882469169215e-15 -2.3570574492240229e-14 0
		 -3.6902602823853861e-15 0.99999999999999978 -1.5265566588595037e-15 0 2.3645817422826884e-14 1.5265566588596768e-15 0.99999999999999978 0
		 7.9999999999999662 -7.6769422470462994e-06 -2.9999998153131311 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 16 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 16 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 16 ".ifcl";
createNode groupId -n "groupId271";
	rename -uid "1F9A4BBC-42E7-CA37-0682-119D3C74974A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts44";
	rename -uid "A2959922-4331-AF91-F015-23827065AB2B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:135]";
createNode tweak -n "tweak11";
	rename -uid "F5A2536E-44EA-DB26-4D11-4ABD9165BE57";
createNode objectSet -n "skinCluster11Set";
	rename -uid "138E373C-43DD-3F3D-D3BE-A9A3ABDE500D";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster11GroupId";
	rename -uid "0F46ECDE-43FB-33C4-C4F5-BC867D814219";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster11GroupParts";
	rename -uid "178BFC45-4473-CE74-C900-639FAF098326";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet11";
	rename -uid "D297F778-4014-C645-0911-7E9ED2EC8A38";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId273";
	rename -uid "B6DC4A4A-4E6E-5F8E-9465-8F88222A5AF0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts46";
	rename -uid "18921F5F-42B2-2B3E-B232-278196B5CEAE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster12";
	rename -uid "CA4712A4-4223-33E5-6014-268F4C637210";
	setAttr -s 64 ".wl";
	setAttr ".wl[0:63].w"
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 5 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 7 1
		1 7 1
		1 7 1
		1 7 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1
		1 6 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -11 -1.9984031383911763e-15 -4.8849813083506888e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -18 -1.776358533466145e-15 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -3.7747620953737949e-15 -36.000007629394531 8.7209893915251267e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214453 -0.16439898730535343 0 0 0.16439898730535343 0.98639392383214453 0 0
		 0 0 1 0 -17.261894921327137 -30.578219164384933 -1.3819974421032562e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.4416913763379837e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.4416913763379837e-15 0 0
		 0 0 1 0 34.00000766028554 -18.000000185345737 -7.7091534788882934e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247499207e-06 -2.9942229272749301e-07 -0.99999999999940548 0
		 -0.27472112789698266 0.96152394764093563 9.9739717725969859e-18 0 0.96152394764036475 0.27472112789681941 -1.0899135973298236e-06 0
		 4.6702780463397309 -16.345901749996973 18.000000185335107 1;
	setAttr ".pm[7]" -type "matrix" 0.9999999999994057 -1.1628765895575732e-12 -1.0899136848022152e-06 0
		 -2.4962331691727e-14 0.9999999999994057 -1.0899135945436703e-06 0 1.0899136847043486e-06 1.0899135943203584e-06 0.99999999999881184 0
		 -18.000000185335342 -15.000007662161799 -6.9999640261868867 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214375 0.16439898730535726 2.4497368521849414e-09 0
		 0.16439898730535729 0.98639392383214353 1.4698421113109649e-08 0 -4.1359030627651375e-25 1.4901167533561269e-08 -0.99999999999999978 0
		 -17.261894921327244 -30.578219164384862 -4.5565116109470115e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631522403611227e-10 -1 2.4497368521849414e-09 0
		 -0.99999999999999978 4.1631529605063993e-10 1.4698421113109649e-08 0 -1.4698421114129511e-08 -2.4497368460657649e-09 -0.99999999999999978 0
		 34.000007652791822 -18.000000199500565 -4.5565115054298801e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693209322700799e-09 -3.2534683058010462e-10 -0.99999999999999978 0
		 -0.27657365741139739 0.96099272215042419 3.7029343698581624e-10 0 0.96099272215042419 0.27657365741139722 -2.4629819348047793e-09 0
		 4.7017523802160115 -16.336876242991089 -18.00000019871819 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999956 3.6583736869134254e-10 -2.4687523309694251e-09 0
		 -3.7170209060962485e-10 0.99999717760308415 -0.0023758758100449989 0 2.4678762396832115e-09 0.0023758758100448896 0.99999717760308393 0
		 18.000000197960102 -15.004705710865565 -6.9548526572791962 1;
	setAttr ".pm[12]" -type "matrix" -3.5301351571464157e-16 -6.8955981264935312e-15 -1 0
		 -0.99161147425390128 0.12925433891364818 -5.2946000530483817e-16 0 0.12925433891364818 0.99161147425390128 -6.7316942088131252e-15 0
		 10.907733782188075 -1.4217987141824262 8.0000000000000053 1;
	setAttr ".pm[13]" -type "matrix" 2.6730615658509697e-15 -1.1075733536914241e-14 1 0
		 -0.94953047617381503 0.31367479149132715 6.000558110069543e-15 0 -0.3136747914913271 -0.94953047617381481 -9.8299630538315332e-15 0
		 5.9016242114745472 -1.2632053017725899 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2871897274151038e-13 -3.4855117479312523e-15 -1 0
		 -0.24253388348224064 0.97014293553219377 2.7849041457073148e-14 0 0.97014293553219355 0.24253388348224064 -1.2556946823042317e-13 0
		 1.2126768531177115 -0.72760905338516302 8.0000000000001652 1;
	setAttr ".pm[15]" -type "matrix" -1 2.7406441015163463e-14 1.2399783673555757e-13 0
		 2.7418830035030903e-14 0.99999999999999978 -1.7763568394037033e-15 0 -1.2390413369348544e-13 -1.7486012637811782e-15 -0.99999999999999956 0
		 8.0000000000001634 -7.6769425058733829e-06 2.9999998153119849 1;
	setAttr ".pm[16]" -type "matrix" -3.3807466072319389e-16 -6.6736223841650137e-15 -1 0
		 -0.99161147425390139 0.12925433891364663 -5.4322137893231732e-16 0 0.12925433891364663 0.9916114742539015 -6.729900444524443e-15 0
		 10.907733782188073 -1.4217987141825152 -7.9999999999999929 1;
	setAttr ".pm[17]" -type "matrix" 2.5902343882339624e-15 6.037260639988876e-15 1 0
		 -0.94953047617381414 0.31367479149133021 5.8163546186962406e-16 0 -0.31367479149133026 -0.94953047617381414 6.6136164986905321e-15 0
		 5.9016242114745845 -1.2632053017726599 7.9999999999999947 1;
	setAttr ".pm[18]" -type "matrix" -2.3654363560712546e-14 -2.1362752008768194e-15 -1 0
		 -0.24253388348224733 0.97014293553219233 3.6486269189619435e-15 0 0.97014293553219233 0.24253388348224728 -2.3534795120364372e-14 0
		 1.2126768531165262 -0.7276090533852021 -7.9999999999999689 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 3.704882469169215e-15 -2.3570574492240229e-14 0
		 -3.6902602823853861e-15 0.99999999999999978 -1.5265566588595037e-15 0 2.3645817422826884e-14 1.5265566588596768e-15 0.99999999999999978 0
		 7.9999999999999662 -7.6769422470462994e-06 -2.9999998153131311 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 9 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 9 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 9 ".ifcl";
createNode groupId -n "groupId274";
	rename -uid "02E9677F-4A85-A9E5-CEF2-98BC079C9988";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts47";
	rename -uid "05B4BC4D-40D0-6338-6F4F-0EBC4B93E8FE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:49]";
createNode tweak -n "tweak12";
	rename -uid "11BA5EE6-427C-17F9-A112-4281C8C38E9B";
createNode objectSet -n "skinCluster12Set";
	rename -uid "012AD038-4F59-3FF7-72F6-2286E52264EF";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster12GroupId";
	rename -uid "18709BE3-42D5-0715-98B2-6F80290F5E15";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster12GroupParts";
	rename -uid "A971CF31-4761-3755-4BC6-9E806A31F05E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet12";
	rename -uid "A74EAC8A-4E3F-8A16-444E-05AAE3890969";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId276";
	rename -uid "D0629154-4731-5531-A939-B292622B7CDC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts49";
	rename -uid "DFF08722-4D7B-CD63-5D44-BA95B4FFDABB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster13";
	rename -uid "8F2F5702-482B-EA06-68B0-4291357F09DE";
	setAttr -s 64 ".wl";
	setAttr ".wl[0:63].w"
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 9 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 11 1
		1 11 1
		1 11 1
		1 11 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1
		1 10 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -11 -1.9984031383911763e-15 -4.8849813083506888e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -18 -1.776358533466145e-15 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -3.7747620953737949e-15 -36.000007629394531 8.7209893915251267e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214453 -0.16439898730535343 0 0 0.16439898730535343 0.98639392383214453 0 0
		 0 0 1 0 -17.261894921327137 -30.578219164384933 -1.3819974421032562e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.4416913763379837e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.4416913763379837e-15 0 0
		 0 0 1 0 34.00000766028554 -18.000000185345737 -7.7091534788882934e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247499207e-06 -2.9942229272749301e-07 -0.99999999999940548 0
		 -0.27472112789698266 0.96152394764093563 9.9739717725969859e-18 0 0.96152394764036475 0.27472112789681941 -1.0899135973298236e-06 0
		 4.6702780463397309 -16.345901749996973 18.000000185335107 1;
	setAttr ".pm[7]" -type "matrix" 0.9999999999994057 -1.1628765895575732e-12 -1.0899136848022152e-06 0
		 -2.4962331691727e-14 0.9999999999994057 -1.0899135945436703e-06 0 1.0899136847043486e-06 1.0899135943203584e-06 0.99999999999881184 0
		 -18.000000185335342 -15.000007662161799 -6.9999640261868867 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214375 0.16439898730535726 2.4497368521849414e-09 0
		 0.16439898730535729 0.98639392383214353 1.4698421113109649e-08 0 -4.1359030627651375e-25 1.4901167533561269e-08 -0.99999999999999978 0
		 -17.261894921327244 -30.578219164384862 -4.5565116109470115e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631522403611227e-10 -1 2.4497368521849414e-09 0
		 -0.99999999999999978 4.1631529605063993e-10 1.4698421113109649e-08 0 -1.4698421114129511e-08 -2.4497368460657649e-09 -0.99999999999999978 0
		 34.000007652791822 -18.000000199500565 -4.5565115054298801e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693209322700799e-09 -3.2534683058010462e-10 -0.99999999999999978 0
		 -0.27657365741139739 0.96099272215042419 3.7029343698581624e-10 0 0.96099272215042419 0.27657365741139722 -2.4629819348047793e-09 0
		 4.7017523802160115 -16.336876242991089 -18.00000019871819 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999956 3.6583736869134254e-10 -2.4687523309694251e-09 0
		 -3.7170209060962485e-10 0.99999717760308415 -0.0023758758100449989 0 2.4678762396832115e-09 0.0023758758100448896 0.99999717760308393 0
		 18.000000197960102 -15.004705710865565 -6.9548526572791962 1;
	setAttr ".pm[12]" -type "matrix" -3.5301351571464157e-16 -6.8955981264935312e-15 -1 0
		 -0.99161147425390128 0.12925433891364818 -5.2946000530483817e-16 0 0.12925433891364818 0.99161147425390128 -6.7316942088131252e-15 0
		 10.907733782188075 -1.4217987141824262 8.0000000000000053 1;
	setAttr ".pm[13]" -type "matrix" 2.6730615658509697e-15 -1.1075733536914241e-14 1 0
		 -0.94953047617381503 0.31367479149132715 6.000558110069543e-15 0 -0.3136747914913271 -0.94953047617381481 -9.8299630538315332e-15 0
		 5.9016242114745472 -1.2632053017725899 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2871897274151038e-13 -3.4855117479312523e-15 -1 0
		 -0.24253388348224064 0.97014293553219377 2.7849041457073148e-14 0 0.97014293553219355 0.24253388348224064 -1.2556946823042317e-13 0
		 1.2126768531177115 -0.72760905338516302 8.0000000000001652 1;
	setAttr ".pm[15]" -type "matrix" -1 2.7406441015163463e-14 1.2399783673555757e-13 0
		 2.7418830035030903e-14 0.99999999999999978 -1.7763568394037033e-15 0 -1.2390413369348544e-13 -1.7486012637811782e-15 -0.99999999999999956 0
		 8.0000000000001634 -7.6769425058733829e-06 2.9999998153119849 1;
	setAttr ".pm[16]" -type "matrix" -3.3807466072319389e-16 -6.6736223841650137e-15 -1 0
		 -0.99161147425390139 0.12925433891364663 -5.4322137893231732e-16 0 0.12925433891364663 0.9916114742539015 -6.729900444524443e-15 0
		 10.907733782188073 -1.4217987141825152 -7.9999999999999929 1;
	setAttr ".pm[17]" -type "matrix" 2.5902343882339624e-15 6.037260639988876e-15 1 0
		 -0.94953047617381414 0.31367479149133021 5.8163546186962406e-16 0 -0.31367479149133026 -0.94953047617381414 6.6136164986905321e-15 0
		 5.9016242114745845 -1.2632053017726599 7.9999999999999947 1;
	setAttr ".pm[18]" -type "matrix" -2.3654363560712546e-14 -2.1362752008768194e-15 -1 0
		 -0.24253388348224733 0.97014293553219233 3.6486269189619435e-15 0 0.97014293553219233 0.24253388348224728 -2.3534795120364372e-14 0
		 1.2126768531165262 -0.7276090533852021 -7.9999999999999689 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 3.704882469169215e-15 -2.3570574492240229e-14 0
		 -3.6902602823853861e-15 0.99999999999999978 -1.5265566588595037e-15 0 2.3645817422826884e-14 1.5265566588596768e-15 0.99999999999999978 0
		 7.9999999999999662 -7.6769422470462994e-06 -2.9999998153131311 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 9 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 9 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 9 ".ifcl";
createNode groupId -n "groupId277";
	rename -uid "3BDF0E43-4D07-590A-41F5-EAA3210ED8DB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts50";
	rename -uid "31369713-4D3A-97BA-1AE0-FB891B58060E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:49]";
createNode tweak -n "tweak13";
	rename -uid "E1A6EA5A-448E-811F-2AE2-4090EA703063";
createNode objectSet -n "skinCluster13Set";
	rename -uid "E3E6F694-4577-2804-0F53-A8977A164545";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster13GroupId";
	rename -uid "A8A56B61-440B-D8F5-EEDB-46866652A964";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster13GroupParts";
	rename -uid "348A6BB2-492C-02D1-CA28-5689294990B9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet13";
	rename -uid "F7F0F1B1-4207-B13C-AC0D-0CBACEA38A0F";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId279";
	rename -uid "764B7DF3-4B9C-4F64-5F83-19AC98565C4A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts52";
	rename -uid "D4FAEE7E-4494-9091-39D3-289D7831A8E1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster14";
	rename -uid "E803553B-43C0-32CD-2E55-148222894A48";
	setAttr -s 260 ".wl";
	setAttr ".wl[0:259].w"
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 12 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		1 13 1
		2 14 0.97089999914169312 15 0.029100000858306885
		2 14 0.97089999914169312 15 0.029100000858306885
		2 14 0.97089999914169312 15 0.029100000858306885
		2 14 0.97089999914169312 15 0.029100000858306885
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 14 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 16 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		1 17 1
		2 18 0.97089999914169312 19 0.029100000858306885
		2 18 0.97089999914169312 19 0.029100000858306885
		2 18 0.97089999914169312 19 0.029100000858306885
		2 18 0.97089999914169312 19 0.029100000858306885
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 18 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1
		1 1 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -11 -1.9984031383911763e-15 -4.8849813083506888e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -18 -1.776358533466145e-15 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -3.7747620953737949e-15 -36.000007629394531 8.7209893915251267e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214453 -0.16439898730535343 0 0 0.16439898730535343 0.98639392383214453 0 0
		 0 0 1 0 -17.261894921327137 -30.578219164384933 -1.3819974421032562e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.4416913763379837e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.4416913763379837e-15 0 0
		 0 0 1 0 34.00000766028554 -18.000000185345737 -7.7091534788882934e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247499207e-06 -2.9942229272749301e-07 -0.99999999999940548 0
		 -0.27472112789698266 0.96152394764093563 9.9739717725969859e-18 0 0.96152394764036475 0.27472112789681941 -1.0899135973298236e-06 0
		 4.6702780463397309 -16.345901749996973 18.000000185335107 1;
	setAttr ".pm[7]" -type "matrix" 0.9999999999994057 -1.1628765895575732e-12 -1.0899136848022152e-06 0
		 -2.4962331691727e-14 0.9999999999994057 -1.0899135945436703e-06 0 1.0899136847043486e-06 1.0899135943203584e-06 0.99999999999881184 0
		 -18.000000185335342 -15.000007662161799 -6.9999640261868867 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214375 0.16439898730535726 2.4497368521849414e-09 0
		 0.16439898730535729 0.98639392383214353 1.4698421113109649e-08 0 -4.1359030627651375e-25 1.4901167533561269e-08 -0.99999999999999978 0
		 -17.261894921327244 -30.578219164384862 -4.5565116109470115e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631522403611227e-10 -1 2.4497368521849414e-09 0
		 -0.99999999999999978 4.1631529605063993e-10 1.4698421113109649e-08 0 -1.4698421114129511e-08 -2.4497368460657649e-09 -0.99999999999999978 0
		 34.000007652791822 -18.000000199500565 -4.5565115054298801e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693209322700799e-09 -3.2534683058010462e-10 -0.99999999999999978 0
		 -0.27657365741139739 0.96099272215042419 3.7029343698581624e-10 0 0.96099272215042419 0.27657365741139722 -2.4629819348047793e-09 0
		 4.7017523802160115 -16.336876242991089 -18.00000019871819 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999956 3.6583736869134254e-10 -2.4687523309694251e-09 0
		 -3.7170209060962485e-10 0.99999717760308415 -0.0023758758100449989 0 2.4678762396832115e-09 0.0023758758100448896 0.99999717760308393 0
		 18.000000197960102 -15.004705710865565 -6.9548526572791962 1;
	setAttr ".pm[12]" -type "matrix" -3.5301351571464157e-16 -6.8955981264935312e-15 -1 0
		 -0.99161147425390128 0.12925433891364818 -5.2946000530483817e-16 0 0.12925433891364818 0.99161147425390128 -6.7316942088131252e-15 0
		 10.907733782188075 -1.4217987141824262 8.0000000000000053 1;
	setAttr ".pm[13]" -type "matrix" 2.6730615658509697e-15 -1.1075733536914241e-14 1 0
		 -0.94953047617381503 0.31367479149132715 6.000558110069543e-15 0 -0.3136747914913271 -0.94953047617381481 -9.8299630538315332e-15 0
		 5.9016242114745472 -1.2632053017725899 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2871897274151038e-13 -3.4855117479312523e-15 -1 0
		 -0.24253388348224064 0.97014293553219377 2.7849041457073148e-14 0 0.97014293553219355 0.24253388348224064 -1.2556946823042317e-13 0
		 1.2126768531177115 -0.72760905338516302 8.0000000000001652 1;
	setAttr ".pm[15]" -type "matrix" -1 2.7406441015163463e-14 1.2399783673555757e-13 0
		 2.7418830035030903e-14 0.99999999999999978 -1.7763568394037033e-15 0 -1.2390413369348544e-13 -1.7486012637811782e-15 -0.99999999999999956 0
		 8.0000000000001634 -7.6769425058733829e-06 2.9999998153119849 1;
	setAttr ".pm[16]" -type "matrix" -3.3807466072319389e-16 -6.6736223841650137e-15 -1 0
		 -0.99161147425390139 0.12925433891364663 -5.4322137893231732e-16 0 0.12925433891364663 0.9916114742539015 -6.729900444524443e-15 0
		 10.907733782188073 -1.4217987141825152 -7.9999999999999929 1;
	setAttr ".pm[17]" -type "matrix" 2.5902343882339624e-15 6.037260639988876e-15 1 0
		 -0.94953047617381414 0.31367479149133021 5.8163546186962406e-16 0 -0.31367479149133026 -0.94953047617381414 6.6136164986905321e-15 0
		 5.9016242114745845 -1.2632053017726599 7.9999999999999947 1;
	setAttr ".pm[18]" -type "matrix" -2.3654363560712546e-14 -2.1362752008768194e-15 -1 0
		 -0.24253388348224733 0.97014293553219233 3.6486269189619435e-15 0 0.97014293553219233 0.24253388348224728 -2.3534795120364372e-14 0
		 1.2126768531165262 -0.7276090533852021 -7.9999999999999689 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 3.704882469169215e-15 -2.3570574492240229e-14 0
		 -3.6902602823853861e-15 0.99999999999999978 -1.5265566588595037e-15 0 2.3645817422826884e-14 1.5265566588596768e-15 0.99999999999999978 0
		 7.9999999999999662 -7.6769422470462994e-06 -2.9999998153131311 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 17 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 17 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 17 ".ifcl";
createNode groupId -n "groupId280";
	rename -uid "B6E661F8-45A3-4A48-267F-99BAFBF5FA9F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts53";
	rename -uid "FD887737-4C36-28D0-3CA9-2082B98E480B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:245]";
createNode groupId -n "groupId281";
	rename -uid "1FBD11F8-4CAE-CD55-7B4D-E196C450584B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts54";
	rename -uid "F7528A00-4410-742D-031C-B28103752F5D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[14:51]" "f[128:165]";
createNode tweak -n "tweak14";
	rename -uid "461CDB82-4C70-67B9-4C35-6D863099E6AC";
createNode objectSet -n "skinCluster14Set";
	rename -uid "38A4D868-4FFB-2EFE-34BF-8DAC85080A69";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster14GroupId";
	rename -uid "3DCE425C-4604-08FA-9F3E-49A206C2948B";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster14GroupParts";
	rename -uid "EB4D08BD-43E8-6366-F872-178B24D5BAC6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet14";
	rename -uid "13E42912-460E-78C5-5442-8C9350435C89";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId283";
	rename -uid "133FCC93-48EC-E90B-4334-E6A8817F8FBD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts56";
	rename -uid "503917DA-4B95-5FB1-196D-9D8C70D175C6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster15";
	rename -uid "B7BB033B-4F80-B6BF-31D8-1E80516FF94B";
	setAttr -s 88 ".wl";
	setAttr ".wl[0:87].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -11 -1.9984031383911763e-15 -4.8849813083506888e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -18 -1.776358533466145e-15 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -3.7747620953737949e-15 -36.000007629394531 8.7209893915251267e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214453 -0.16439898730535343 0 0 0.16439898730535343 0.98639392383214453 0 0
		 0 0 1 0 -17.261894921327137 -30.578219164384933 -1.3819974421032562e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.4416913763379837e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.4416913763379837e-15 0 0
		 0 0 1 0 34.00000766028554 -18.000000185345737 -7.7091534788882934e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247499207e-06 -2.9942229272749301e-07 -0.99999999999940548 0
		 -0.27472112789698266 0.96152394764093563 9.9739717725969859e-18 0 0.96152394764036475 0.27472112789681941 -1.0899135973298236e-06 0
		 4.6702780463397309 -16.345901749996973 18.000000185335107 1;
	setAttr ".pm[7]" -type "matrix" 0.9999999999994057 -1.1628765895575732e-12 -1.0899136848022152e-06 0
		 -2.4962331691727e-14 0.9999999999994057 -1.0899135945436703e-06 0 1.0899136847043486e-06 1.0899135943203584e-06 0.99999999999881184 0
		 -18.000000185335342 -15.000007662161799 -6.9999640261868867 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214375 0.16439898730535726 2.4497368521849414e-09 0
		 0.16439898730535729 0.98639392383214353 1.4698421113109649e-08 0 -4.1359030627651375e-25 1.4901167533561269e-08 -0.99999999999999978 0
		 -17.261894921327244 -30.578219164384862 -4.5565116109470115e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631522403611227e-10 -1 2.4497368521849414e-09 0
		 -0.99999999999999978 4.1631529605063993e-10 1.4698421113109649e-08 0 -1.4698421114129511e-08 -2.4497368460657649e-09 -0.99999999999999978 0
		 34.000007652791822 -18.000000199500565 -4.5565115054298801e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693209322700799e-09 -3.2534683058010462e-10 -0.99999999999999978 0
		 -0.27657365741139739 0.96099272215042419 3.7029343698581624e-10 0 0.96099272215042419 0.27657365741139722 -2.4629819348047793e-09 0
		 4.7017523802160115 -16.336876242991089 -18.00000019871819 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999956 3.6583736869134254e-10 -2.4687523309694251e-09 0
		 -3.7170209060962485e-10 0.99999717760308415 -0.0023758758100449989 0 2.4678762396832115e-09 0.0023758758100448896 0.99999717760308393 0
		 18.000000197960102 -15.004705710865565 -6.9548526572791962 1;
	setAttr ".pm[12]" -type "matrix" -3.5301351571464157e-16 -6.8955981264935312e-15 -1 0
		 -0.99161147425390128 0.12925433891364818 -5.2946000530483817e-16 0 0.12925433891364818 0.99161147425390128 -6.7316942088131252e-15 0
		 10.907733782188075 -1.4217987141824262 8.0000000000000053 1;
	setAttr ".pm[13]" -type "matrix" 2.6730615658509697e-15 -1.1075733536914241e-14 1 0
		 -0.94953047617381503 0.31367479149132715 6.000558110069543e-15 0 -0.3136747914913271 -0.94953047617381481 -9.8299630538315332e-15 0
		 5.9016242114745472 -1.2632053017725899 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2871897274151038e-13 -3.4855117479312523e-15 -1 0
		 -0.24253388348224064 0.97014293553219377 2.7849041457073148e-14 0 0.97014293553219355 0.24253388348224064 -1.2556946823042317e-13 0
		 1.2126768531177115 -0.72760905338516302 8.0000000000001652 1;
	setAttr ".pm[15]" -type "matrix" -1 2.7406441015163463e-14 1.2399783673555757e-13 0
		 2.7418830035030903e-14 0.99999999999999978 -1.7763568394037033e-15 0 -1.2390413369348544e-13 -1.7486012637811782e-15 -0.99999999999999956 0
		 8.0000000000001634 -7.6769425058733829e-06 2.9999998153119849 1;
	setAttr ".pm[16]" -type "matrix" -3.3807466072319389e-16 -6.6736223841650137e-15 -1 0
		 -0.99161147425390139 0.12925433891364663 -5.4322137893231732e-16 0 0.12925433891364663 0.9916114742539015 -6.729900444524443e-15 0
		 10.907733782188073 -1.4217987141825152 -7.9999999999999929 1;
	setAttr ".pm[17]" -type "matrix" 2.5902343882339624e-15 6.037260639988876e-15 1 0
		 -0.94953047617381414 0.31367479149133021 5.8163546186962406e-16 0 -0.31367479149133026 -0.94953047617381414 6.6136164986905321e-15 0
		 5.9016242114745845 -1.2632053017726599 7.9999999999999947 1;
	setAttr ".pm[18]" -type "matrix" -2.3654363560712546e-14 -2.1362752008768194e-15 -1 0
		 -0.24253388348224733 0.97014293553219233 3.6486269189619435e-15 0 0.97014293553219233 0.24253388348224728 -2.3534795120364372e-14 0
		 1.2126768531165262 -0.7276090533852021 -7.9999999999999689 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 3.704882469169215e-15 -2.3570574492240229e-14 0
		 -3.6902602823853861e-15 0.99999999999999978 -1.5265566588595037e-15 0 2.3645817422826884e-14 1.5265566588596768e-15 0.99999999999999978 0
		 7.9999999999999662 -7.6769422470462994e-06 -2.9999998153131311 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 12 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 12 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 12 ".ifcl";
createNode tweak -n "tweak15";
	rename -uid "386E502C-46D9-8609-EC33-F98DBFEA0920";
createNode objectSet -n "skinCluster15Set";
	rename -uid "292A466C-454D-C523-3CD5-7888A622B45D";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster15GroupId";
	rename -uid "DB9E0F82-466A-7AFC-3D5F-4BA59F48B462";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster15GroupParts";
	rename -uid "CA9EFA6A-4F0B-6CCF-86AC-68A005E9877E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet15";
	rename -uid "7B218C9E-457B-BEED-CEAE-3BBEAB18E429";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId285";
	rename -uid "3514AD6B-446F-C566-2241-D8A9C7A3AFFB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts58";
	rename -uid "0712897E-405B-650A-027C-78907887EBBF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode polyNormal -n "polyNormal3";
	rename -uid "070810AF-4D04-2B80-7E90-01BBD2A9C6A7";
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".unm" no;
createNode polyTweak -n "polyTweak16";
	rename -uid "F81271B9-4C81-37D6-995B-72935484D63C";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk";
	setAttr ".tk[75]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[76]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[79]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[81]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[82]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[85]" -type "float3" 0 -0.017047882 0 ;
	setAttr ".tk[87]" -type "float3" 0 -0.017047882 0 ;
	setAttr ".tk[89]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[92]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[93]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[95]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[97]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".tk[124]" -type "float3" -0.44799328 0.0024738312 -0.2360487 ;
	setAttr ".tk[125]" -type "float3" -0.67992067 -0.021787643 -0.10880303 ;
	setAttr ".tk[126]" -type "float3" -0.7458663 0.030092239 0.38857651 ;
	setAttr ".tk[127]" -type "float3" -0.68902004 -0.035913467 0.3489542 ;
	setAttr ".tk[128]" -type "float3" -0.49676561 0.0018177032 0.36548948 ;
	setAttr ".tk[129]" -type "float3" 1.3335386e-06 0.035913467 0.56961191 ;
	setAttr ".tk[130]" -type "float3" 0.49676675 0.0018177032 0.36549008 ;
	setAttr ".tk[131]" -type "float3" 0.68901908 -0.035909653 0.34896374 ;
	setAttr ".tk[132]" -type "float3" 0.74586731 0.030092239 0.38858086 ;
	setAttr ".tk[133]" -type "float3" 0.67992151 -0.021787643 -0.1088028 ;
	setAttr ".tk[134]" -type "float3" 0.44799328 0.0024738312 -0.23604965 ;
	setAttr ".tk[135]" -type "float3" 0 0.0095043182 -0.68134689 ;
createNode polyCube -n "polyCube28";
	rename -uid "49EA8F11-43FF-E8D7-DDDF-BCA35322F414";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit7";
	rename -uid "B0C293E4-4AD2-91AB-3CC4-45BC91D03D72";
	setAttr -s 5 ".e[0:4]"  0.25 0.25 0.75 0.75 0.25;
	setAttr -s 5 ".d[0:4]"  -2147483644 -2147483643 -2147483639 -2147483640 -2147483644;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit8";
	rename -uid "0A679618-4477-764D-3F54-738BD9D21AFD";
	setAttr -s 5 ".e[0:4]"  0.33333299 0.33333299 0.66666698 0.66666698
		 0.33333299;
	setAttr -s 5 ".d[0:4]"  -2147483636 -2147483635 -2147483639 -2147483640 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit9";
	rename -uid "AF2783D4-4D4E-90DE-17E6-A1A84B5A2131";
	setAttr -s 5 ".e[0:4]"  0.5 0.5 0.5 0.5 0.5;
	setAttr -s 5 ".d[0:4]"  -2147483628 -2147483627 -2147483639 -2147483640 -2147483628;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode skinCluster -n "skinCluster16";
	rename -uid "F5DABA2D-48B1-4FB8-75B6-ECB4A9BAEE87";
	setAttr -s 20 ".wl";
	setAttr ".wl[0:19].w"
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1
		1 2 1;
	setAttr -s 20 ".pm";
	setAttr ".pm[0]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -11 -1.9984031383911763e-15 -4.8849813083506888e-15 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 -18 -1.776358533466145e-15 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -3.7747620953737949e-15 -36.000007629394531 8.7209893915251267e-15 1;
	setAttr ".pm[4]" -type "matrix" 0.98639392383214453 -0.16439898730535343 0 0 0.16439898730535343 0.98639392383214453 0 0
		 0 0 1 0 -17.261894921327137 -30.578219164384933 -1.3819974421032562e-14 1;
	setAttr ".pm[5]" -type "matrix" -3.4416913763379837e-15 0.99999999999999978 0 0 -0.99999999999999978 -3.4416913763379837e-15 0 0
		 0 0 1 0 34.00000766028554 -18.000000185345737 -7.7091534788882934e-15 1;
	setAttr ".pm[6]" -type "matrix" -1.0479780247499207e-06 -2.9942229272749301e-07 -0.99999999999940548 0
		 -0.27472112789698266 0.96152394764093563 9.9739717725969859e-18 0 0.96152394764036475 0.27472112789681941 -1.0899135973298236e-06 0
		 4.6702780463397309 -16.345901749996973 18.000000185335107 1;
	setAttr ".pm[7]" -type "matrix" 0.9999999999994057 -1.1628765895575732e-12 -1.0899136848022152e-06 0
		 -2.4962331691727e-14 0.9999999999994057 -1.0899135945436703e-06 0 1.0899136847043486e-06 1.0899135943203584e-06 0.99999999999881184 0
		 -18.000000185335342 -15.000007662161799 -6.9999640261868867 1;
	setAttr ".pm[8]" -type "matrix" -0.98639392383214375 0.16439898730535726 2.4497368521849414e-09 0
		 0.16439898730535729 0.98639392383214353 1.4698421113109649e-08 0 -4.1359030627651375e-25 1.4901167533561269e-08 -0.99999999999999978 0
		 -17.261894921327244 -30.578219164384862 -4.5565116109470115e-07 1;
	setAttr ".pm[9]" -type "matrix" -4.1631522403611227e-10 -1 2.4497368521849414e-09 0
		 -0.99999999999999978 4.1631529605063993e-10 1.4698421113109649e-08 0 -1.4698421114129511e-08 -2.4497368460657649e-09 -0.99999999999999978 0
		 34.000007652791822 -18.000000199500565 -4.5565115054298801e-07 1;
	setAttr ".pm[10]" -type "matrix" -2.4693209322700799e-09 -3.2534683058010462e-10 -0.99999999999999978 0
		 -0.27657365741139739 0.96099272215042419 3.7029343698581624e-10 0 0.96099272215042419 0.27657365741139722 -2.4629819348047793e-09 0
		 4.7017523802160115 -16.336876242991089 -18.00000019871819 1;
	setAttr ".pm[11]" -type "matrix" 0.99999999999999956 3.6583736869134254e-10 -2.4687523309694251e-09 0
		 -3.7170209060962485e-10 0.99999717760308415 -0.0023758758100449989 0 2.4678762396832115e-09 0.0023758758100448896 0.99999717760308393 0
		 18.000000197960102 -15.004705710865565 -6.9548526572791962 1;
	setAttr ".pm[12]" -type "matrix" -3.5301351571464157e-16 -6.8955981264935312e-15 -1 0
		 -0.99161147425390128 0.12925433891364818 -5.2946000530483817e-16 0 0.12925433891364818 0.99161147425390128 -6.7316942088131252e-15 0
		 10.907733782188075 -1.4217987141824262 8.0000000000000053 1;
	setAttr ".pm[13]" -type "matrix" 2.6730615658509697e-15 -1.1075733536914241e-14 1 0
		 -0.94953047617381503 0.31367479149132715 6.000558110069543e-15 0 -0.3136747914913271 -0.94953047617381481 -9.8299630538315332e-15 0
		 5.9016242114745472 -1.2632053017725899 -8.0000000000000284 1;
	setAttr ".pm[14]" -type "matrix" -1.2871897274151038e-13 -3.4855117479312523e-15 -1 0
		 -0.24253388348224064 0.97014293553219377 2.7849041457073148e-14 0 0.97014293553219355 0.24253388348224064 -1.2556946823042317e-13 0
		 1.2126768531177115 -0.72760905338516302 8.0000000000001652 1;
	setAttr ".pm[15]" -type "matrix" -1 2.7406441015163463e-14 1.2399783673555757e-13 0
		 2.7418830035030903e-14 0.99999999999999978 -1.7763568394037033e-15 0 -1.2390413369348544e-13 -1.7486012637811782e-15 -0.99999999999999956 0
		 8.0000000000001634 -7.6769425058733829e-06 2.9999998153119849 1;
	setAttr ".pm[16]" -type "matrix" -3.3807466072319389e-16 -6.6736223841650137e-15 -1 0
		 -0.99161147425390139 0.12925433891364663 -5.4322137893231732e-16 0 0.12925433891364663 0.9916114742539015 -6.729900444524443e-15 0
		 10.907733782188073 -1.4217987141825152 -7.9999999999999929 1;
	setAttr ".pm[17]" -type "matrix" 2.5902343882339624e-15 6.037260639988876e-15 1 0
		 -0.94953047617381414 0.31367479149133021 5.8163546186962406e-16 0 -0.31367479149133026 -0.94953047617381414 6.6136164986905321e-15 0
		 5.9016242114745845 -1.2632053017726599 7.9999999999999947 1;
	setAttr ".pm[18]" -type "matrix" -2.3654363560712546e-14 -2.1362752008768194e-15 -1 0
		 -0.24253388348224733 0.97014293553219233 3.6486269189619435e-15 0 0.97014293553219233 0.24253388348224728 -2.3534795120364372e-14 0
		 1.2126768531165262 -0.7276090533852021 -7.9999999999999689 1;
	setAttr ".pm[19]" -type "matrix" 0.99999999999999978 3.704882469169215e-15 -2.3570574492240229e-14 0
		 -3.6902602823853861e-15 0.99999999999999978 -1.5265566588595037e-15 0 2.3645817422826884e-14 1.5265566588596768e-15 0.99999999999999978 0
		 7.9999999999999662 -7.6769422470462994e-06 -2.9999998153131311 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 18.121340480344969 0 1;
	setAttr -s 5 ".ma";
	setAttr -s 20 ".dpf[0:19]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 5 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 5 ".ifcl";
createNode tweak -n "tweak16";
	rename -uid "0B289C3C-4A3A-932A-F71D-76BBBDCFDECC";
createNode objectSet -n "skinCluster16Set";
	rename -uid "53DDE656-4D9F-9070-DF26-C68016226725";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster16GroupId";
	rename -uid "C60E2A9C-4CC9-E17E-5876-4A9855E95C09";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster16GroupParts";
	rename -uid "DEC000A4-4BFA-F350-39DA-D6B6C7CB0BFF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet16";
	rename -uid "0A3761CB-4F23-EEB3-12FD-3783DF5BFA25";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId287";
	rename -uid "6A795513-4521-2559-FF60-5E96CF2122D4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts60";
	rename -uid "6776E03F-49BC-6EDB-6B07-948994EB6DF9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".etmr" no;
	setAttr ".tmr" 4096;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 71 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 12 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHideFaceDataSet;
	setAttr -s 2 ".dnsm";
connectAttr "layer2.di" "CenterRoot.do";
connectAttr "CenterRoot.s" "CenterHip.is";
connectAttr "CenterHip.s" "CenterSpine.is";
connectAttr "CenterSpine.s" "CenterHead.is";
connectAttr "CenterSpine.s" "LeftCollar.is";
connectAttr "LeftCollar.s" "LeftShoulder.is";
connectAttr "LeftShoulder.s" "LeftElbow.is";
connectAttr "LeftElbow.s" "LeftHand.is";
connectAttr "CenterSpine.s" "RightCollar.is";
connectAttr "RightCollar.s" "RightShoulder.is";
connectAttr "RightShoulder.s" "RightElbow.is";
connectAttr "RightElbow.s" "RightHand.is";
connectAttr "CenterHip.s" "LeftHip.is";
connectAttr "LeftHip.s" "LeftKnee.is";
connectAttr "LeftKnee.s" "LeftFoot.is";
connectAttr "LeftFoot.s" "LeftToe.is";
connectAttr "CenterHip.s" "RightHip.is";
connectAttr "RightHip.s" "RightKnee.is";
connectAttr "RightKnee.s" "RightFoot.is";
connectAttr "RightFoot.s" "RightToe.is";
connectAttr "layer1.di" "BasicMechGrouped.do";
connectAttr "transformGeometry23.og" "baseShape.i";
connectAttr "transformGeometry24.og" "backSideShape.i";
connectAttr "transformGeometry15.og" "leftSideShape.i";
connectAttr "transformGeometry16.og" "leftNozzleShape.i";
connectAttr "transformGeometry25.og" "frontSideShape.i";
connectAttr "transformGeometry26.og" "frontLeftShape1.i";
connectAttr "transformGeometry45.og" "frontLeftShape2.i";
connectAttr "transformGeometry46.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCubeShape7.i"
		;
connectAttr "transformGeometry47.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8|pCubeShape8.i"
		;
connectAttr "transformGeometry48.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11|pCubeShape11.i"
		;
connectAttr "transformGeometry49.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10|pCubeShape10.i"
		;
connectAttr "transformGeometry50.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9|pCubeShape9.i"
		;
connectAttr "transformGeometry51.og" "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12|pCubeShape12.i"
		;
connectAttr "transformGeometry28.og" "jawMidShape.i";
connectAttr "transformGeometry29.og" "pCubeShape5.i";
connectAttr "transformGeometry30.og" "teethmidShape.i";
connectAttr "transformGeometry14.og" "teethLeftShape.i";
connectAttr "transformGeometry5.og" "hipsShape.i";
connectAttr "transformGeometry6.og" "upperLegLeftShape.i";
connectAttr "transformGeometry7.og" "lowerLegLeftShape.i";
connectAttr "transformGeometry8.og" "kneeLeftShape.i";
connectAttr "transformGeometry56.og" "panelLeftShape1.i";
connectAttr "transformGeometry57.og" "panelLeftShape2.i";
connectAttr "transformGeometry52.og" "armRightShape1.i";
connectAttr "transformGeometry53.og" "upperArmRightShape.i";
connectAttr "transformGeometry54.og" "lowerArmRightShape.i";
connectAttr "transformGeometry55.og" "armRightShape2.i";
connectAttr "skinCluster8.og[0]" "Cubic_Left_Arm_oldShape.i";
connectAttr "groupId213.id" "Cubic_Left_Arm_oldShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Cubic_Left_Arm_oldShape.iog.og[0].gco";
connectAttr "skinCluster8GroupId.id" "Cubic_Left_Arm_oldShape.iog.og[3].gid";
connectAttr "skinCluster8Set.mwc" "Cubic_Left_Arm_oldShape.iog.og[3].gco";
connectAttr "groupId246.id" "Cubic_Left_Arm_oldShape.iog.og[4].gid";
connectAttr "tweakSet8.mwc" "Cubic_Left_Arm_oldShape.iog.og[4].gco";
connectAttr "tweak8.vl[0].vt[0]" "Cubic_Left_Arm_oldShape.twl";
connectAttr "polyPlane1.out" "pPlaneShape1.i";
connectAttr "skinCluster1.og[0]" "Basic_LegsShape.i";
connectAttr "groupId210.id" "Basic_LegsShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Basic_LegsShape.iog.og[0].gco";
connectAttr "skinCluster1GroupId.id" "Basic_LegsShape.iog.og[4].gid";
connectAttr "skinCluster1Set.mwc" "Basic_LegsShape.iog.og[4].gco";
connectAttr "groupId232.id" "Basic_LegsShape.iog.og[5].gid";
connectAttr "tweakSet1.mwc" "Basic_LegsShape.iog.og[5].gco";
connectAttr "tweak1.vl[0].vt[0]" "Basic_LegsShape.twl";
connectAttr "groupId216.id" "Basic_TorsoShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Basic_TorsoShape.iog.og[0].gco";
connectAttr "skinCluster2GroupId.id" "Basic_TorsoShape.iog.og[23].gid";
connectAttr "skinCluster2Set.mwc" "Basic_TorsoShape.iog.og[23].gco";
connectAttr "groupId234.id" "Basic_TorsoShape.iog.og[24].gid";
connectAttr "tweakSet2.mwc" "Basic_TorsoShape.iog.og[24].gco";
connectAttr "skinCluster2.og[0]" "Basic_TorsoShape.i";
connectAttr "tweak2.vl[0].vt[0]" "Basic_TorsoShape.twl";
connectAttr "skinCluster3.og[0]" "Basic_BackShape.i";
connectAttr "skinCluster3GroupId.id" "Basic_BackShape.iog.og[13].gid";
connectAttr "skinCluster3Set.mwc" "Basic_BackShape.iog.og[13].gco";
connectAttr "groupId236.id" "Basic_BackShape.iog.og[14].gid";
connectAttr "tweakSet3.mwc" "Basic_BackShape.iog.og[14].gco";
connectAttr "tweak3.vl[0].vt[0]" "Basic_BackShape.twl";
connectAttr "skinCluster4.og[0]" "Basic_Right_ArmShape.i";
connectAttr "groupId227.id" "Basic_Right_ArmShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Basic_Right_ArmShape.iog.og[0].gco";
connectAttr "skinCluster4GroupId.id" "Basic_Right_ArmShape.iog.og[5].gid";
connectAttr "skinCluster4Set.mwc" "Basic_Right_ArmShape.iog.og[5].gco";
connectAttr "groupId238.id" "Basic_Right_ArmShape.iog.og[6].gid";
connectAttr "tweakSet4.mwc" "Basic_Right_ArmShape.iog.og[6].gco";
connectAttr "tweak4.vl[0].vt[0]" "Basic_Right_ArmShape.twl";
connectAttr "skinCluster7.og[0]" "Basic_Left_ArmShape.i";
connectAttr "groupId230.id" "Basic_Left_ArmShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Basic_Left_ArmShape.iog.og[0].gco";
connectAttr "skinCluster7GroupId.id" "Basic_Left_ArmShape.iog.og[7].gid";
connectAttr "skinCluster7Set.mwc" "Basic_Left_ArmShape.iog.og[7].gco";
connectAttr "groupId244.id" "Basic_Left_ArmShape.iog.og[8].gid";
connectAttr "tweakSet7.mwc" "Basic_Left_ArmShape.iog.og[8].gco";
connectAttr "tweak7.vl[0].vt[0]" "Basic_Left_ArmShape.twl";
connectAttr "groupId248.id" "Basic_HeadShape.iog.og[3].gid";
connectAttr "tweakSet9.mwc" "Basic_HeadShape.iog.og[3].gco";
connectAttr "groupId268.id" "Basic_HeadShape.iog.og[4].gid";
connectAttr "set1.mwc" "Basic_HeadShape.iog.og[4].gco";
connectAttr "skinCluster9GroupId.id" "Basic_HeadShape.iog.og[5].gid";
connectAttr "skinCluster9Set.mwc" "Basic_HeadShape.iog.og[5].gco";
connectAttr "polyNormal3.out" "Basic_HeadShape.i";
connectAttr "skinCluster5.og[0]" "Gun_Right_ArmShape.i";
connectAttr "groupId224.id" "Gun_Right_ArmShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Gun_Right_ArmShape.iog.og[0].gco";
connectAttr "skinCluster5GroupId.id" "Gun_Right_ArmShape.iog.og[11].gid";
connectAttr "skinCluster5Set.mwc" "Gun_Right_ArmShape.iog.og[11].gco";
connectAttr "groupId240.id" "Gun_Right_ArmShape.iog.og[12].gid";
connectAttr "tweakSet5.mwc" "Gun_Right_ArmShape.iog.og[12].gco";
connectAttr "tweak5.vl[0].vt[0]" "Gun_Right_ArmShape.twl";
connectAttr "skinCluster6.og[0]" "Drill_Right_ArmShape.i";
connectAttr "groupId219.id" "Drill_Right_ArmShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Drill_Right_ArmShape.iog.og[0].gco";
connectAttr "skinCluster6GroupId.id" "Drill_Right_ArmShape.iog.og[3].gid";
connectAttr "skinCluster6Set.mwc" "Drill_Right_ArmShape.iog.og[3].gco";
connectAttr "groupId242.id" "Drill_Right_ArmShape.iog.og[4].gid";
connectAttr "tweakSet6.mwc" "Drill_Right_ArmShape.iog.og[4].gco";
connectAttr "tweak6.vl[0].vt[0]" "Drill_Right_ArmShape.twl";
connectAttr "polySplit6.out" "Falcon_BackShape.i";
connectAttr "skinCluster15GroupId.id" "Cubic_BackShape.iog.og[0].gid";
connectAttr "skinCluster15Set.mwc" "Cubic_BackShape.iog.og[0].gco";
connectAttr "groupId285.id" "Cubic_BackShape.iog.og[1].gid";
connectAttr "tweakSet15.mwc" "Cubic_BackShape.iog.og[1].gco";
connectAttr "skinCluster15.og[0]" "Cubic_BackShape.i";
connectAttr "tweak15.vl[0].vt[0]" "Cubic_BackShape.twl";
connectAttr "groupId274.id" "Cubic_Left_ArmShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Cubic_Left_ArmShape.iog.og[0].gco";
connectAttr "skinCluster12GroupId.id" "Cubic_Left_ArmShape.iog.og[1].gid";
connectAttr "skinCluster12Set.mwc" "Cubic_Left_ArmShape.iog.og[1].gco";
connectAttr "groupId276.id" "Cubic_Left_ArmShape.iog.og[2].gid";
connectAttr "tweakSet12.mwc" "Cubic_Left_ArmShape.iog.og[2].gco";
connectAttr "skinCluster12.og[0]" "Cubic_Left_ArmShape.i";
connectAttr "tweak12.vl[0].vt[0]" "Cubic_Left_ArmShape.twl";
connectAttr "groupId277.id" "Cubic_Right_ArmShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Cubic_Right_ArmShape.iog.og[0].gco";
connectAttr "skinCluster13GroupId.id" "Cubic_Right_ArmShape.iog.og[1].gid";
connectAttr "skinCluster13Set.mwc" "Cubic_Right_ArmShape.iog.og[1].gco";
connectAttr "groupId279.id" "Cubic_Right_ArmShape.iog.og[2].gid";
connectAttr "tweakSet13.mwc" "Cubic_Right_ArmShape.iog.og[2].gco";
connectAttr "skinCluster13.og[0]" "Cubic_Right_ArmShape.i";
connectAttr "tweak13.vl[0].vt[0]" "Cubic_Right_ArmShape.twl";
connectAttr "skinCluster14.og[0]" "Cubic_LegsShape.i";
connectAttr "groupId280.id" "Cubic_LegsShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Cubic_LegsShape.iog.og[0].gco";
connectAttr "groupId281.id" "Cubic_LegsShape.iog.og[1].gid";
connectAttr "skinCluster14GroupId.id" "Cubic_LegsShape.iog.og[2].gid";
connectAttr "skinCluster14Set.mwc" "Cubic_LegsShape.iog.og[2].gco";
connectAttr "groupId283.id" "Cubic_LegsShape.iog.og[3].gid";
connectAttr "tweakSet14.mwc" "Cubic_LegsShape.iog.og[3].gco";
connectAttr "tweak14.vl[0].vt[0]" "Cubic_LegsShape.twl";
connectAttr "skinCluster10.og[0]" "Cubic_HeadShape.i";
connectAttr "groupId267.id" "Cubic_HeadShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Cubic_HeadShape.iog.og[0].gco";
connectAttr "skinCluster10GroupId.id" "Cubic_HeadShape.iog.og[1].gid";
connectAttr "skinCluster10Set.mwc" "Cubic_HeadShape.iog.og[1].gco";
connectAttr "groupId270.id" "Cubic_HeadShape.iog.og[2].gid";
connectAttr "tweakSet10.mwc" "Cubic_HeadShape.iog.og[2].gco";
connectAttr "tweak10.vl[0].vt[0]" "Cubic_HeadShape.twl";
connectAttr "polyDelEdge6.out" "Cubic_HeadShape4Orig.i";
connectAttr "skinCluster11.og[0]" "Cubic_TorsoShape.i";
connectAttr "groupId271.id" "Cubic_TorsoShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "Cubic_TorsoShape.iog.og[0].gco";
connectAttr "skinCluster11GroupId.id" "Cubic_TorsoShape.iog.og[1].gid";
connectAttr "skinCluster11Set.mwc" "Cubic_TorsoShape.iog.og[1].gco";
connectAttr "groupId273.id" "Cubic_TorsoShape.iog.og[2].gid";
connectAttr "tweakSet11.mwc" "Cubic_TorsoShape.iog.og[2].gco";
connectAttr "tweak11.vl[0].vt[0]" "Cubic_TorsoShape.twl";
connectAttr "skinCluster16GroupId.id" "DummyShape.iog.og[3].gid";
connectAttr "skinCluster16Set.mwc" "DummyShape.iog.og[3].gco";
connectAttr "groupId287.id" "DummyShape.iog.og[4].gid";
connectAttr "tweakSet16.mwc" "DummyShape.iog.og[4].gco";
connectAttr "skinCluster16.og[0]" "DummyShape.i";
connectAttr "tweak16.vl[0].vt[0]" "DummyShape.twl";
connectAttr "polySplit9.out" "DummyShapeOrig.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "pSphere1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "pSphere1SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube2.out" "transformGeometry1.ig";
connectAttr "transformGeometry1.og" "transformGeometry2.ig";
connectAttr "polyCube3.out" "transformGeometry3.ig";
connectAttr "polyCube4.out" "transformGeometry4.ig";
connectAttr "polyCube1.out" "transformGeometry5.ig";
connectAttr "transformGeometry2.og" "transformGeometry6.ig";
connectAttr "transformGeometry3.og" "transformGeometry7.ig";
connectAttr "transformGeometry4.og" "transformGeometry8.ig";
connectAttr "polyCube5.out" "transformGeometry9.ig";
connectAttr "polyCube6.out" "transformGeometry10.ig";
connectAttr "polyCube10.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "transformGeometry11.ig";
connectAttr "transformGeometry11.og" "transformGeometry12.ig";
connectAttr "polyCube12.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "transformGeometry13.ig";
connectAttr "polyCube14.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "transformGeometry14.ig";
connectAttr "polyCube15.out" "transformGeometry15.ig";
connectAttr "polyCube17.out" "transformGeometry16.ig";
connectAttr "polyCube18.out" "transformGeometry17.ig";
connectAttr "polyCube19.out" "transformGeometry18.ig";
connectAttr "polyCube22.out" "transformGeometry19.ig";
connectAttr "polyCube21.out" "transformGeometry20.ig";
connectAttr "polyCube20.out" "transformGeometry21.ig";
connectAttr "polyCube23.out" "transformGeometry22.ig";
connectAttr "polyCube7.out" "transformGeometry23.ig";
connectAttr "polyCube16.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "transformGeometry24.ig";
connectAttr "polyCube8.out" "transformGeometry25.ig";
connectAttr "polyCube9.out" "polyTweak5.ip";
connectAttr "polyTweak5.out" "transformGeometry26.ig";
connectAttr "transformGeometry12.og" "transformGeometry27.ig";
connectAttr "polyCube11.out" "polyTweak6.ip";
connectAttr "polyTweak6.out" "transformGeometry28.ig";
connectAttr "transformGeometry13.og" "transformGeometry29.ig";
connectAttr "polyCube13.out" "polyTweak7.ip";
connectAttr "polyTweak7.out" "transformGeometry30.ig";
connectAttr "transformGeometry27.og" "transformGeometry31.ig";
connectAttr "transformGeometry17.og" "transformGeometry32.ig";
connectAttr "transformGeometry18.og" "transformGeometry33.ig";
connectAttr "transformGeometry19.og" "transformGeometry34.ig";
connectAttr "transformGeometry20.og" "transformGeometry35.ig";
connectAttr "transformGeometry21.og" "transformGeometry36.ig";
connectAttr "transformGeometry22.og" "transformGeometry37.ig";
connectAttr "transformGeometry31.og" "transformGeometry38.ig";
connectAttr "transformGeometry32.og" "transformGeometry39.ig";
connectAttr "transformGeometry33.og" "transformGeometry40.ig";
connectAttr "transformGeometry34.og" "transformGeometry41.ig";
connectAttr "transformGeometry35.og" "transformGeometry42.ig";
connectAttr "transformGeometry36.og" "transformGeometry43.ig";
connectAttr "transformGeometry37.og" "transformGeometry44.ig";
connectAttr "transformGeometry38.og" "transformGeometry45.ig";
connectAttr "transformGeometry39.og" "transformGeometry46.ig";
connectAttr "transformGeometry40.og" "transformGeometry47.ig";
connectAttr "transformGeometry41.og" "transformGeometry48.ig";
connectAttr "transformGeometry42.og" "transformGeometry49.ig";
connectAttr "transformGeometry43.og" "transformGeometry50.ig";
connectAttr "transformGeometry44.og" "transformGeometry51.ig";
connectAttr "polyCube24.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "transformGeometry52.ig";
connectAttr "polyCube26.out" "transformGeometry53.ig";
connectAttr "polyCube27.out" "transformGeometry54.ig";
connectAttr "polyCube25.out" "transformGeometry55.ig";
connectAttr "transformGeometry9.og" "transformGeometry56.ig";
connectAttr "transformGeometry10.og" "transformGeometry57.ig";
connectAttr "layerManager.dli[3]" "layer1.id";
connectAttr "layerManager.dli[4]" "layer2.id";
connectAttr "Basic_LegsShapeOrig.w" "groupParts1.ig";
connectAttr "groupId210.id" "groupParts1.gi";
connectAttr "Cubic_Left_Arm_oldShapeOrig.w" "groupParts4.ig";
connectAttr "groupId213.id" "groupParts4.gi";
connectAttr "Basic_TorsoShapeOrig.w" "groupParts7.ig";
connectAttr "groupId216.id" "groupParts7.gi";
connectAttr "Drill_Right_ArmShapeOrig.w" "groupParts10.ig";
connectAttr "groupId219.id" "groupParts10.gi";
connectAttr "Gun_Right_ArmShapeOrig.w" "groupParts15.ig";
connectAttr "groupId224.id" "groupParts15.gi";
connectAttr "Basic_Right_ArmShapeOrig.w" "groupParts18.ig";
connectAttr "groupId227.id" "groupParts18.gi";
connectAttr "Basic_Left_ArmShapeOrig.w" "groupParts21.ig";
connectAttr "groupId230.id" "groupParts21.gi";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "CenterRoot.wm" "skinCluster1.ma[0]";
connectAttr "CenterHip.wm" "skinCluster1.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster1.ma[2]";
connectAttr "LeftShoulder.wm" "skinCluster1.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster1.ma[6]";
connectAttr "RightShoulder.wm" "skinCluster1.ma[9]";
connectAttr "RightElbow.wm" "skinCluster1.ma[10]";
connectAttr "LeftHip.wm" "skinCluster1.ma[12]";
connectAttr "LeftKnee.wm" "skinCluster1.ma[13]";
connectAttr "LeftFoot.wm" "skinCluster1.ma[14]";
connectAttr "LeftToe.wm" "skinCluster1.ma[15]";
connectAttr "RightHip.wm" "skinCluster1.ma[16]";
connectAttr "RightKnee.wm" "skinCluster1.ma[17]";
connectAttr "RightFoot.wm" "skinCluster1.ma[18]";
connectAttr "RightToe.wm" "skinCluster1.ma[19]";
connectAttr "CenterRoot.liw" "skinCluster1.lw[0]";
connectAttr "CenterHip.liw" "skinCluster1.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster1.lw[2]";
connectAttr "LeftShoulder.liw" "skinCluster1.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster1.lw[6]";
connectAttr "RightShoulder.liw" "skinCluster1.lw[9]";
connectAttr "RightElbow.liw" "skinCluster1.lw[10]";
connectAttr "LeftHip.liw" "skinCluster1.lw[12]";
connectAttr "LeftKnee.liw" "skinCluster1.lw[13]";
connectAttr "LeftFoot.liw" "skinCluster1.lw[14]";
connectAttr "LeftToe.liw" "skinCluster1.lw[15]";
connectAttr "RightHip.liw" "skinCluster1.lw[16]";
connectAttr "RightKnee.liw" "skinCluster1.lw[17]";
connectAttr "RightFoot.liw" "skinCluster1.lw[18]";
connectAttr "RightToe.liw" "skinCluster1.lw[19]";
connectAttr "CenterRoot.obcc" "skinCluster1.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster1.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster1.ifcl[2]";
connectAttr "LeftShoulder.obcc" "skinCluster1.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster1.ifcl[6]";
connectAttr "RightShoulder.obcc" "skinCluster1.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster1.ifcl[10]";
connectAttr "LeftHip.obcc" "skinCluster1.ifcl[12]";
connectAttr "LeftKnee.obcc" "skinCluster1.ifcl[13]";
connectAttr "LeftFoot.obcc" "skinCluster1.ifcl[14]";
connectAttr "LeftToe.obcc" "skinCluster1.ifcl[15]";
connectAttr "RightHip.obcc" "skinCluster1.ifcl[16]";
connectAttr "RightKnee.obcc" "skinCluster1.ifcl[17]";
connectAttr "RightFoot.obcc" "skinCluster1.ifcl[18]";
connectAttr "RightToe.obcc" "skinCluster1.ifcl[19]";
connectAttr "RightToe.msg" "skinCluster1.ptt";
connectAttr "groupParts23.og" "tweak1.ip[0].ig";
connectAttr "groupId232.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "Basic_LegsShape.iog.og[4]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId232.msg" "tweakSet1.gn" -na;
connectAttr "Basic_LegsShape.iog.og[5]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "groupParts1.og" "groupParts23.ig";
connectAttr "groupId232.id" "groupParts23.gi";
connectAttr "CenterRoot.msg" "bindPose1.m[0]";
connectAttr "CenterHip.msg" "bindPose1.m[1]";
connectAttr "CenterSpine.msg" "bindPose1.m[2]";
connectAttr "LeftCollar.msg" "bindPose1.m[4]";
connectAttr "LeftShoulder.msg" "bindPose1.m[5]";
connectAttr "LeftElbow.msg" "bindPose1.m[6]";
connectAttr "RightCollar.msg" "bindPose1.m[8]";
connectAttr "RightShoulder.msg" "bindPose1.m[9]";
connectAttr "RightElbow.msg" "bindPose1.m[10]";
connectAttr "LeftHip.msg" "bindPose1.m[12]";
connectAttr "LeftKnee.msg" "bindPose1.m[13]";
connectAttr "LeftFoot.msg" "bindPose1.m[14]";
connectAttr "LeftToe.msg" "bindPose1.m[15]";
connectAttr "RightHip.msg" "bindPose1.m[16]";
connectAttr "RightKnee.msg" "bindPose1.m[17]";
connectAttr "RightFoot.msg" "bindPose1.m[18]";
connectAttr "RightToe.msg" "bindPose1.m[19]";
connectAttr "CenterHead.msg" "bindPose1.m[20]";
connectAttr "LeftHand.msg" "bindPose1.m[21]";
connectAttr "RightHand.msg" "bindPose1.m[22]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[4]";
connectAttr "bindPose1.m[4]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[2]" "bindPose1.p[8]";
connectAttr "bindPose1.m[8]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[1]" "bindPose1.p[12]";
connectAttr "bindPose1.m[12]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "bindPose1.m[14]" "bindPose1.p[15]";
connectAttr "bindPose1.m[1]" "bindPose1.p[16]";
connectAttr "bindPose1.m[16]" "bindPose1.p[17]";
connectAttr "bindPose1.m[17]" "bindPose1.p[18]";
connectAttr "bindPose1.m[18]" "bindPose1.p[19]";
connectAttr "bindPose1.m[2]" "bindPose1.p[20]";
connectAttr "bindPose1.m[6]" "bindPose1.p[21]";
connectAttr "bindPose1.m[10]" "bindPose1.p[22]";
connectAttr "CenterRoot.bps" "bindPose1.wm[0]";
connectAttr "CenterHip.bps" "bindPose1.wm[1]";
connectAttr "CenterSpine.bps" "bindPose1.wm[2]";
connectAttr "LeftCollar.bps" "bindPose1.wm[4]";
connectAttr "LeftShoulder.bps" "bindPose1.wm[5]";
connectAttr "LeftElbow.bps" "bindPose1.wm[6]";
connectAttr "RightCollar.bps" "bindPose1.wm[8]";
connectAttr "RightShoulder.bps" "bindPose1.wm[9]";
connectAttr "RightElbow.bps" "bindPose1.wm[10]";
connectAttr "LeftHip.bps" "bindPose1.wm[12]";
connectAttr "LeftKnee.bps" "bindPose1.wm[13]";
connectAttr "LeftFoot.bps" "bindPose1.wm[14]";
connectAttr "LeftToe.bps" "bindPose1.wm[15]";
connectAttr "RightHip.bps" "bindPose1.wm[16]";
connectAttr "RightKnee.bps" "bindPose1.wm[17]";
connectAttr "RightFoot.bps" "bindPose1.wm[18]";
connectAttr "RightToe.bps" "bindPose1.wm[19]";
connectAttr "CenterHead.bps" "bindPose1.wm[20]";
connectAttr "LeftHand.bps" "bindPose1.wm[21]";
connectAttr "RightHand.bps" "bindPose1.wm[22]";
connectAttr "skinCluster2GroupParts.og" "skinCluster2.ip[0].ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster2.ma[0]";
connectAttr "CenterHip.wm" "skinCluster2.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster2.ma[2]";
connectAttr "CenterHead.wm" "skinCluster2.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster2.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster2.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster2.ma[6]";
connectAttr "LeftHand.wm" "skinCluster2.ma[7]";
connectAttr "RightCollar.wm" "skinCluster2.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster2.ma[9]";
connectAttr "RightElbow.wm" "skinCluster2.ma[10]";
connectAttr "RightHand.wm" "skinCluster2.ma[11]";
connectAttr "LeftHip.wm" "skinCluster2.ma[12]";
connectAttr "LeftKnee.wm" "skinCluster2.ma[13]";
connectAttr "RightHip.wm" "skinCluster2.ma[16]";
connectAttr "RightKnee.wm" "skinCluster2.ma[17]";
connectAttr "CenterRoot.liw" "skinCluster2.lw[0]";
connectAttr "CenterHip.liw" "skinCluster2.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster2.lw[2]";
connectAttr "CenterHead.liw" "skinCluster2.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster2.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster2.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster2.lw[6]";
connectAttr "LeftHand.liw" "skinCluster2.lw[7]";
connectAttr "RightCollar.liw" "skinCluster2.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster2.lw[9]";
connectAttr "RightElbow.liw" "skinCluster2.lw[10]";
connectAttr "RightHand.liw" "skinCluster2.lw[11]";
connectAttr "LeftHip.liw" "skinCluster2.lw[12]";
connectAttr "LeftKnee.liw" "skinCluster2.lw[13]";
connectAttr "RightHip.liw" "skinCluster2.lw[16]";
connectAttr "RightKnee.liw" "skinCluster2.lw[17]";
connectAttr "CenterRoot.obcc" "skinCluster2.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster2.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster2.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster2.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster2.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster2.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster2.ifcl[6]";
connectAttr "LeftHand.obcc" "skinCluster2.ifcl[7]";
connectAttr "RightCollar.obcc" "skinCluster2.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster2.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster2.ifcl[10]";
connectAttr "RightHand.obcc" "skinCluster2.ifcl[11]";
connectAttr "LeftHip.obcc" "skinCluster2.ifcl[12]";
connectAttr "LeftKnee.obcc" "skinCluster2.ifcl[13]";
connectAttr "RightHip.obcc" "skinCluster2.ifcl[16]";
connectAttr "RightKnee.obcc" "skinCluster2.ifcl[17]";
connectAttr "bindPose1.msg" "skinCluster2.bp";
connectAttr "CenterSpine.msg" "skinCluster2.ptt";
connectAttr "groupParts25.og" "tweak2.ip[0].ig";
connectAttr "groupId234.id" "tweak2.ip[0].gi";
connectAttr "skinCluster2GroupId.msg" "skinCluster2Set.gn" -na;
connectAttr "Basic_TorsoShape.iog.og[23]" "skinCluster2Set.dsm" -na;
connectAttr "skinCluster2.msg" "skinCluster2Set.ub[0]";
connectAttr "tweak2.og[0]" "skinCluster2GroupParts.ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2GroupParts.gi";
connectAttr "groupId234.msg" "tweakSet2.gn" -na;
connectAttr "Basic_TorsoShape.iog.og[24]" "tweakSet2.dsm" -na;
connectAttr "tweak2.msg" "tweakSet2.ub[0]";
connectAttr "groupParts7.og" "groupParts25.ig";
connectAttr "groupId234.id" "groupParts25.gi";
connectAttr "skinCluster3GroupParts.og" "skinCluster3.ip[0].ig";
connectAttr "skinCluster3GroupId.id" "skinCluster3.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster3.ma[0]";
connectAttr "CenterHip.wm" "skinCluster3.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster3.ma[2]";
connectAttr "CenterHead.wm" "skinCluster3.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster3.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster3.ma[5]";
connectAttr "RightCollar.wm" "skinCluster3.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster3.ma[9]";
connectAttr "LeftHip.wm" "skinCluster3.ma[12]";
connectAttr "RightHip.wm" "skinCluster3.ma[16]";
connectAttr "CenterRoot.liw" "skinCluster3.lw[0]";
connectAttr "CenterHip.liw" "skinCluster3.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster3.lw[2]";
connectAttr "CenterHead.liw" "skinCluster3.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster3.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster3.lw[5]";
connectAttr "RightCollar.liw" "skinCluster3.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster3.lw[9]";
connectAttr "LeftHip.liw" "skinCluster3.lw[12]";
connectAttr "RightHip.liw" "skinCluster3.lw[16]";
connectAttr "CenterRoot.obcc" "skinCluster3.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster3.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster3.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster3.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster3.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster3.ifcl[5]";
connectAttr "RightCollar.obcc" "skinCluster3.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster3.ifcl[9]";
connectAttr "LeftHip.obcc" "skinCluster3.ifcl[12]";
connectAttr "RightHip.obcc" "skinCluster3.ifcl[16]";
connectAttr "bindPose1.msg" "skinCluster3.bp";
connectAttr "CenterSpine.msg" "skinCluster3.ptt";
connectAttr "groupParts27.og" "tweak3.ip[0].ig";
connectAttr "groupId236.id" "tweak3.ip[0].gi";
connectAttr "skinCluster3GroupId.msg" "skinCluster3Set.gn" -na;
connectAttr "Basic_BackShape.iog.og[13]" "skinCluster3Set.dsm" -na;
connectAttr "skinCluster3.msg" "skinCluster3Set.ub[0]";
connectAttr "tweak3.og[0]" "skinCluster3GroupParts.ig";
connectAttr "skinCluster3GroupId.id" "skinCluster3GroupParts.gi";
connectAttr "groupId236.msg" "tweakSet3.gn" -na;
connectAttr "Basic_BackShape.iog.og[14]" "tweakSet3.dsm" -na;
connectAttr "tweak3.msg" "tweakSet3.ub[0]";
connectAttr "Basic_BackShapeOrig.w" "groupParts27.ig";
connectAttr "groupId236.id" "groupParts27.gi";
connectAttr "skinCluster4GroupParts.og" "skinCluster4.ip[0].ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster4.ma[0]";
connectAttr "CenterHip.wm" "skinCluster4.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster4.ma[2]";
connectAttr "CenterHead.wm" "skinCluster4.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster4.ma[4]";
connectAttr "RightCollar.wm" "skinCluster4.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster4.ma[9]";
connectAttr "RightElbow.wm" "skinCluster4.ma[10]";
connectAttr "RightHand.wm" "skinCluster4.ma[11]";
connectAttr "RightHip.wm" "skinCluster4.ma[16]";
connectAttr "RightKnee.wm" "skinCluster4.ma[17]";
connectAttr "RightFoot.wm" "skinCluster4.ma[18]";
connectAttr "RightToe.wm" "skinCluster4.ma[19]";
connectAttr "CenterRoot.liw" "skinCluster4.lw[0]";
connectAttr "CenterHip.liw" "skinCluster4.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster4.lw[2]";
connectAttr "CenterHead.liw" "skinCluster4.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster4.lw[4]";
connectAttr "RightCollar.liw" "skinCluster4.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster4.lw[9]";
connectAttr "RightElbow.liw" "skinCluster4.lw[10]";
connectAttr "RightHand.liw" "skinCluster4.lw[11]";
connectAttr "RightHip.liw" "skinCluster4.lw[16]";
connectAttr "RightKnee.liw" "skinCluster4.lw[17]";
connectAttr "RightFoot.liw" "skinCluster4.lw[18]";
connectAttr "RightToe.liw" "skinCluster4.lw[19]";
connectAttr "CenterRoot.obcc" "skinCluster4.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster4.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster4.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster4.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster4.ifcl[4]";
connectAttr "RightCollar.obcc" "skinCluster4.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster4.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster4.ifcl[10]";
connectAttr "RightHand.obcc" "skinCluster4.ifcl[11]";
connectAttr "RightHip.obcc" "skinCluster4.ifcl[16]";
connectAttr "RightKnee.obcc" "skinCluster4.ifcl[17]";
connectAttr "RightFoot.obcc" "skinCluster4.ifcl[18]";
connectAttr "RightToe.obcc" "skinCluster4.ifcl[19]";
connectAttr "bindPose1.msg" "skinCluster4.bp";
connectAttr "RightShoulder.msg" "skinCluster4.ptt";
connectAttr "groupParts29.og" "tweak4.ip[0].ig";
connectAttr "groupId238.id" "tweak4.ip[0].gi";
connectAttr "skinCluster4GroupId.msg" "skinCluster4Set.gn" -na;
connectAttr "Basic_Right_ArmShape.iog.og[5]" "skinCluster4Set.dsm" -na;
connectAttr "skinCluster4.msg" "skinCluster4Set.ub[0]";
connectAttr "tweak4.og[0]" "skinCluster4GroupParts.ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4GroupParts.gi";
connectAttr "groupId238.msg" "tweakSet4.gn" -na;
connectAttr "Basic_Right_ArmShape.iog.og[6]" "tweakSet4.dsm" -na;
connectAttr "tweak4.msg" "tweakSet4.ub[0]";
connectAttr "groupParts18.og" "groupParts29.ig";
connectAttr "groupId238.id" "groupParts29.gi";
connectAttr "skinCluster5GroupParts.og" "skinCluster5.ip[0].ig";
connectAttr "skinCluster5GroupId.id" "skinCluster5.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster5.ma[0]";
connectAttr "CenterHip.wm" "skinCluster5.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster5.ma[2]";
connectAttr "CenterHead.wm" "skinCluster5.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster5.ma[4]";
connectAttr "RightCollar.wm" "skinCluster5.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster5.ma[9]";
connectAttr "RightElbow.wm" "skinCluster5.ma[10]";
connectAttr "RightHand.wm" "skinCluster5.ma[11]";
connectAttr "RightHip.wm" "skinCluster5.ma[16]";
connectAttr "RightKnee.wm" "skinCluster5.ma[17]";
connectAttr "RightFoot.wm" "skinCluster5.ma[18]";
connectAttr "RightToe.wm" "skinCluster5.ma[19]";
connectAttr "CenterRoot.liw" "skinCluster5.lw[0]";
connectAttr "CenterHip.liw" "skinCluster5.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster5.lw[2]";
connectAttr "CenterHead.liw" "skinCluster5.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster5.lw[4]";
connectAttr "RightCollar.liw" "skinCluster5.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster5.lw[9]";
connectAttr "RightElbow.liw" "skinCluster5.lw[10]";
connectAttr "RightHand.liw" "skinCluster5.lw[11]";
connectAttr "RightHip.liw" "skinCluster5.lw[16]";
connectAttr "RightKnee.liw" "skinCluster5.lw[17]";
connectAttr "RightFoot.liw" "skinCluster5.lw[18]";
connectAttr "RightToe.liw" "skinCluster5.lw[19]";
connectAttr "CenterRoot.obcc" "skinCluster5.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster5.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster5.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster5.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster5.ifcl[4]";
connectAttr "RightCollar.obcc" "skinCluster5.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster5.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster5.ifcl[10]";
connectAttr "RightHand.obcc" "skinCluster5.ifcl[11]";
connectAttr "RightHip.obcc" "skinCluster5.ifcl[16]";
connectAttr "RightKnee.obcc" "skinCluster5.ifcl[17]";
connectAttr "RightFoot.obcc" "skinCluster5.ifcl[18]";
connectAttr "RightToe.obcc" "skinCluster5.ifcl[19]";
connectAttr "bindPose1.msg" "skinCluster5.bp";
connectAttr "RightShoulder.msg" "skinCluster5.ptt";
connectAttr "groupParts31.og" "tweak5.ip[0].ig";
connectAttr "groupId240.id" "tweak5.ip[0].gi";
connectAttr "skinCluster5GroupId.msg" "skinCluster5Set.gn" -na;
connectAttr "Gun_Right_ArmShape.iog.og[11]" "skinCluster5Set.dsm" -na;
connectAttr "skinCluster5.msg" "skinCluster5Set.ub[0]";
connectAttr "tweak5.og[0]" "skinCluster5GroupParts.ig";
connectAttr "skinCluster5GroupId.id" "skinCluster5GroupParts.gi";
connectAttr "groupId240.msg" "tweakSet5.gn" -na;
connectAttr "Gun_Right_ArmShape.iog.og[12]" "tweakSet5.dsm" -na;
connectAttr "tweak5.msg" "tweakSet5.ub[0]";
connectAttr "groupParts15.og" "groupParts31.ig";
connectAttr "groupId240.id" "groupParts31.gi";
connectAttr "skinCluster6GroupParts.og" "skinCluster6.ip[0].ig";
connectAttr "skinCluster6GroupId.id" "skinCluster6.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster6.ma[0]";
connectAttr "CenterHip.wm" "skinCluster6.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster6.ma[2]";
connectAttr "CenterHead.wm" "skinCluster6.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster6.ma[4]";
connectAttr "RightCollar.wm" "skinCluster6.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster6.ma[9]";
connectAttr "RightElbow.wm" "skinCluster6.ma[10]";
connectAttr "RightHand.wm" "skinCluster6.ma[11]";
connectAttr "RightHip.wm" "skinCluster6.ma[16]";
connectAttr "RightKnee.wm" "skinCluster6.ma[17]";
connectAttr "RightFoot.wm" "skinCluster6.ma[18]";
connectAttr "RightToe.wm" "skinCluster6.ma[19]";
connectAttr "CenterRoot.liw" "skinCluster6.lw[0]";
connectAttr "CenterHip.liw" "skinCluster6.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster6.lw[2]";
connectAttr "CenterHead.liw" "skinCluster6.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster6.lw[4]";
connectAttr "RightCollar.liw" "skinCluster6.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster6.lw[9]";
connectAttr "RightElbow.liw" "skinCluster6.lw[10]";
connectAttr "RightHand.liw" "skinCluster6.lw[11]";
connectAttr "RightHip.liw" "skinCluster6.lw[16]";
connectAttr "RightKnee.liw" "skinCluster6.lw[17]";
connectAttr "RightFoot.liw" "skinCluster6.lw[18]";
connectAttr "RightToe.liw" "skinCluster6.lw[19]";
connectAttr "CenterRoot.obcc" "skinCluster6.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster6.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster6.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster6.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster6.ifcl[4]";
connectAttr "RightCollar.obcc" "skinCluster6.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster6.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster6.ifcl[10]";
connectAttr "RightHand.obcc" "skinCluster6.ifcl[11]";
connectAttr "RightHip.obcc" "skinCluster6.ifcl[16]";
connectAttr "RightKnee.obcc" "skinCluster6.ifcl[17]";
connectAttr "RightFoot.obcc" "skinCluster6.ifcl[18]";
connectAttr "RightToe.obcc" "skinCluster6.ifcl[19]";
connectAttr "bindPose1.msg" "skinCluster6.bp";
connectAttr "RightShoulder.msg" "skinCluster6.ptt";
connectAttr "groupParts33.og" "tweak6.ip[0].ig";
connectAttr "groupId242.id" "tweak6.ip[0].gi";
connectAttr "skinCluster6GroupId.msg" "skinCluster6Set.gn" -na;
connectAttr "Drill_Right_ArmShape.iog.og[3]" "skinCluster6Set.dsm" -na;
connectAttr "skinCluster6.msg" "skinCluster6Set.ub[0]";
connectAttr "tweak6.og[0]" "skinCluster6GroupParts.ig";
connectAttr "skinCluster6GroupId.id" "skinCluster6GroupParts.gi";
connectAttr "groupId242.msg" "tweakSet6.gn" -na;
connectAttr "Drill_Right_ArmShape.iog.og[4]" "tweakSet6.dsm" -na;
connectAttr "tweak6.msg" "tweakSet6.ub[0]";
connectAttr "groupParts10.og" "groupParts33.ig";
connectAttr "groupId242.id" "groupParts33.gi";
connectAttr "skinCluster7GroupParts.og" "skinCluster7.ip[0].ig";
connectAttr "skinCluster7GroupId.id" "skinCluster7.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster7.ma[0]";
connectAttr "CenterHip.wm" "skinCluster7.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster7.ma[2]";
connectAttr "CenterHead.wm" "skinCluster7.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster7.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster7.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster7.ma[6]";
connectAttr "LeftHand.wm" "skinCluster7.ma[7]";
connectAttr "RightCollar.wm" "skinCluster7.ma[8]";
connectAttr "LeftHip.wm" "skinCluster7.ma[12]";
connectAttr "LeftKnee.wm" "skinCluster7.ma[13]";
connectAttr "LeftFoot.wm" "skinCluster7.ma[14]";
connectAttr "LeftToe.wm" "skinCluster7.ma[15]";
connectAttr "CenterRoot.liw" "skinCluster7.lw[0]";
connectAttr "CenterHip.liw" "skinCluster7.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster7.lw[2]";
connectAttr "CenterHead.liw" "skinCluster7.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster7.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster7.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster7.lw[6]";
connectAttr "LeftHand.liw" "skinCluster7.lw[7]";
connectAttr "RightCollar.liw" "skinCluster7.lw[8]";
connectAttr "LeftHip.liw" "skinCluster7.lw[12]";
connectAttr "LeftKnee.liw" "skinCluster7.lw[13]";
connectAttr "LeftFoot.liw" "skinCluster7.lw[14]";
connectAttr "LeftToe.liw" "skinCluster7.lw[15]";
connectAttr "CenterRoot.obcc" "skinCluster7.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster7.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster7.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster7.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster7.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster7.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster7.ifcl[6]";
connectAttr "LeftHand.obcc" "skinCluster7.ifcl[7]";
connectAttr "RightCollar.obcc" "skinCluster7.ifcl[8]";
connectAttr "LeftHip.obcc" "skinCluster7.ifcl[12]";
connectAttr "LeftKnee.obcc" "skinCluster7.ifcl[13]";
connectAttr "LeftFoot.obcc" "skinCluster7.ifcl[14]";
connectAttr "LeftToe.obcc" "skinCluster7.ifcl[15]";
connectAttr "bindPose1.msg" "skinCluster7.bp";
connectAttr "LeftElbow.msg" "skinCluster7.ptt";
connectAttr "groupParts35.og" "tweak7.ip[0].ig";
connectAttr "groupId244.id" "tweak7.ip[0].gi";
connectAttr "skinCluster7GroupId.msg" "skinCluster7Set.gn" -na;
connectAttr "Basic_Left_ArmShape.iog.og[7]" "skinCluster7Set.dsm" -na;
connectAttr "skinCluster7.msg" "skinCluster7Set.ub[0]";
connectAttr "tweak7.og[0]" "skinCluster7GroupParts.ig";
connectAttr "skinCluster7GroupId.id" "skinCluster7GroupParts.gi";
connectAttr "groupId244.msg" "tweakSet7.gn" -na;
connectAttr "Basic_Left_ArmShape.iog.og[8]" "tweakSet7.dsm" -na;
connectAttr "tweak7.msg" "tweakSet7.ub[0]";
connectAttr "groupParts21.og" "groupParts35.ig";
connectAttr "groupId244.id" "groupParts35.gi";
connectAttr "skinCluster8GroupParts.og" "skinCluster8.ip[0].ig";
connectAttr "skinCluster8GroupId.id" "skinCluster8.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster8.ma[0]";
connectAttr "CenterHip.wm" "skinCluster8.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster8.ma[2]";
connectAttr "CenterHead.wm" "skinCluster8.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster8.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster8.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster8.ma[6]";
connectAttr "LeftHand.wm" "skinCluster8.ma[7]";
connectAttr "RightCollar.wm" "skinCluster8.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster8.ma[9]";
connectAttr "LeftHip.wm" "skinCluster8.ma[12]";
connectAttr "LeftKnee.wm" "skinCluster8.ma[13]";
connectAttr "LeftFoot.wm" "skinCluster8.ma[14]";
connectAttr "LeftToe.wm" "skinCluster8.ma[15]";
connectAttr "CenterRoot.liw" "skinCluster8.lw[0]";
connectAttr "CenterHip.liw" "skinCluster8.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster8.lw[2]";
connectAttr "CenterHead.liw" "skinCluster8.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster8.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster8.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster8.lw[6]";
connectAttr "LeftHand.liw" "skinCluster8.lw[7]";
connectAttr "RightCollar.liw" "skinCluster8.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster8.lw[9]";
connectAttr "LeftHip.liw" "skinCluster8.lw[12]";
connectAttr "LeftKnee.liw" "skinCluster8.lw[13]";
connectAttr "LeftFoot.liw" "skinCluster8.lw[14]";
connectAttr "LeftToe.liw" "skinCluster8.lw[15]";
connectAttr "CenterRoot.obcc" "skinCluster8.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster8.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster8.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster8.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster8.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster8.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster8.ifcl[6]";
connectAttr "LeftHand.obcc" "skinCluster8.ifcl[7]";
connectAttr "RightCollar.obcc" "skinCluster8.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster8.ifcl[9]";
connectAttr "LeftHip.obcc" "skinCluster8.ifcl[12]";
connectAttr "LeftKnee.obcc" "skinCluster8.ifcl[13]";
connectAttr "LeftFoot.obcc" "skinCluster8.ifcl[14]";
connectAttr "LeftToe.obcc" "skinCluster8.ifcl[15]";
connectAttr "bindPose1.msg" "skinCluster8.bp";
connectAttr "LeftElbow.msg" "skinCluster8.ptt";
connectAttr "groupParts37.og" "tweak8.ip[0].ig";
connectAttr "groupId246.id" "tweak8.ip[0].gi";
connectAttr "skinCluster8GroupId.msg" "skinCluster8Set.gn" -na;
connectAttr "Cubic_Left_Arm_oldShape.iog.og[3]" "skinCluster8Set.dsm" -na;
connectAttr "skinCluster8.msg" "skinCluster8Set.ub[0]";
connectAttr "tweak8.og[0]" "skinCluster8GroupParts.ig";
connectAttr "skinCluster8GroupId.id" "skinCluster8GroupParts.gi";
connectAttr "groupId246.msg" "tweakSet8.gn" -na;
connectAttr "Cubic_Left_Arm_oldShape.iog.og[4]" "tweakSet8.dsm" -na;
connectAttr "tweak8.msg" "tweakSet8.ub[0]";
connectAttr "groupParts4.og" "groupParts37.ig";
connectAttr "groupId246.id" "groupParts37.gi";
connectAttr "lambert2.oc" "pSphere1SG.ss";
connectAttr "Basic_HeadShape.iog" "pSphere1SG.dsm" -na;
connectAttr "pSphere1SG.msg" "materialInfo1.sg";
connectAttr "lambert2.msg" "materialInfo1.m";
connectAttr "polySurfaceShape2.o" "polyCloseBorder1.ip";
connectAttr "polyCloseBorder1.out" "polyExtrudeFace2.ip";
connectAttr "Falcon_BackShape.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace2.out" "polyDelEdge1.ip";
connectAttr "polyDelEdge1.out" "polyDelEdge2.ip";
connectAttr "polyTweak10.out" "polyExtrudeFace3.ip";
connectAttr "Falcon_BackShape.wm" "polyExtrudeFace3.mp";
connectAttr "polyDelEdge2.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polySoftEdge1.ip";
connectAttr "Falcon_BackShape.wm" "polySoftEdge1.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polyBevel1.ip";
connectAttr "Falcon_BackShape.wm" "polyBevel1.mp";
connectAttr "polySoftEdge1.out" "polyTweak12.ip";
connectAttr "polyBevel1.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polySplit6.ip";
connectAttr "groupParts40.og" "polyNormal1.ip";
connectAttr "Cubic_HeadShape1.o" "groupParts40.ig";
connectAttr "groupId267.id" "groupParts40.gi";
connectAttr "polyNormal1.out" "polyExtrudeFace4.ip";
connectAttr "Cubic_HeadShape.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace4.out" "polyNormal2.ip";
connectAttr "polyNormal2.out" "polyTweak13.ip";
connectAttr "polyTweak13.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyBridgeEdge1.ip";
connectAttr "Cubic_HeadShape.wm" "polyBridgeEdge1.mp";
connectAttr "polyBridgeEdge1.out" "polyBridgeEdge2.ip";
connectAttr "Cubic_HeadShape.wm" "polyBridgeEdge2.mp";
connectAttr "polyTweak14.out" "polyDelEdge3.ip";
connectAttr "polyBridgeEdge2.out" "polyTweak14.ip";
connectAttr "polyDelEdge3.out" "polyDelEdge4.ip";
connectAttr "polyDelEdge4.out" "polyDelEdge5.ip";
connectAttr "polyDelEdge5.out" "polyDelEdge6.ip";
connectAttr "groupId268.msg" "set1.gn" -na;
connectAttr "Basic_HeadShape.iog.og[4]" "set1.dsm" -na;
connectAttr "groupId248.msg" "tweakSet9.gn" -na;
connectAttr "Basic_HeadShape.iog.og[3]" "tweakSet9.dsm" -na;
connectAttr "tweak9.msg" "tweakSet9.ub[0]";
connectAttr "Basic_HeadShapeOrig1.w" "groupParts39.ig";
connectAttr "groupId248.id" "groupParts39.gi";
connectAttr "groupParts39.og" "tweak9.ip[0].ig";
connectAttr "groupId248.id" "tweak9.ip[0].gi";
connectAttr "tweak9.og[0]" "polyDelEdge7.ip";
connectAttr "groupParts41.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polyExtrudeFace5.ip";
connectAttr "Basic_HeadShape.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak15.ip";
connectAttr "polyTweak15.out" "polyDelEdge8.ip";
connectAttr "polyDelEdge7.out" "groupParts41.ig";
connectAttr "groupId268.id" "groupParts41.gi";
connectAttr "skinCluster9GroupParts.og" "skinCluster9.ip[0].ig";
connectAttr "skinCluster9GroupId.id" "skinCluster9.ip[0].gi";
connectAttr "CenterHip.wm" "skinCluster9.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster9.ma[2]";
connectAttr "CenterHead.wm" "skinCluster9.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster9.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster9.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster9.ma[6]";
connectAttr "RightCollar.wm" "skinCluster9.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster9.ma[9]";
connectAttr "RightElbow.wm" "skinCluster9.ma[10]";
connectAttr "CenterHip.liw" "skinCluster9.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster9.lw[2]";
connectAttr "CenterHead.liw" "skinCluster9.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster9.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster9.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster9.lw[6]";
connectAttr "RightCollar.liw" "skinCluster9.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster9.lw[9]";
connectAttr "RightElbow.liw" "skinCluster9.lw[10]";
connectAttr "CenterHip.obcc" "skinCluster9.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster9.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster9.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster9.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster9.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster9.ifcl[6]";
connectAttr "RightCollar.obcc" "skinCluster9.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster9.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster9.ifcl[10]";
connectAttr "bindPose1.msg" "skinCluster9.bp";
connectAttr "skinCluster9GroupId.msg" "skinCluster9Set.gn" -na;
connectAttr "Basic_HeadShape.iog.og[5]" "skinCluster9Set.dsm" -na;
connectAttr "skinCluster9.msg" "skinCluster9Set.ub[0]";
connectAttr "polyDelEdge8.out" "skinCluster9GroupParts.ig";
connectAttr "skinCluster9GroupId.id" "skinCluster9GroupParts.gi";
connectAttr "skinCluster10GroupParts.og" "skinCluster10.ip[0].ig";
connectAttr "skinCluster10GroupId.id" "skinCluster10.ip[0].gi";
connectAttr "CenterHip.wm" "skinCluster10.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster10.ma[2]";
connectAttr "CenterHead.wm" "skinCluster10.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster10.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster10.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster10.ma[6]";
connectAttr "RightCollar.wm" "skinCluster10.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster10.ma[9]";
connectAttr "RightElbow.wm" "skinCluster10.ma[10]";
connectAttr "CenterHip.liw" "skinCluster10.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster10.lw[2]";
connectAttr "CenterHead.liw" "skinCluster10.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster10.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster10.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster10.lw[6]";
connectAttr "RightCollar.liw" "skinCluster10.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster10.lw[9]";
connectAttr "RightElbow.liw" "skinCluster10.lw[10]";
connectAttr "CenterHip.obcc" "skinCluster10.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster10.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster10.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster10.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster10.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster10.ifcl[6]";
connectAttr "RightCollar.obcc" "skinCluster10.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster10.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster10.ifcl[10]";
connectAttr "bindPose1.msg" "skinCluster10.bp";
connectAttr "groupParts43.og" "tweak10.ip[0].ig";
connectAttr "groupId270.id" "tweak10.ip[0].gi";
connectAttr "skinCluster10GroupId.msg" "skinCluster10Set.gn" -na;
connectAttr "Cubic_HeadShape.iog.og[1]" "skinCluster10Set.dsm" -na;
connectAttr "skinCluster10.msg" "skinCluster10Set.ub[0]";
connectAttr "tweak10.og[0]" "skinCluster10GroupParts.ig";
connectAttr "skinCluster10GroupId.id" "skinCluster10GroupParts.gi";
connectAttr "groupId270.msg" "tweakSet10.gn" -na;
connectAttr "Cubic_HeadShape.iog.og[2]" "tweakSet10.dsm" -na;
connectAttr "tweak10.msg" "tweakSet10.ub[0]";
connectAttr "Cubic_HeadShape4Orig.w" "groupParts43.ig";
connectAttr "groupId270.id" "groupParts43.gi";
connectAttr "skinCluster11GroupParts.og" "skinCluster11.ip[0].ig";
connectAttr "skinCluster11GroupId.id" "skinCluster11.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster11.ma[0]";
connectAttr "CenterHip.wm" "skinCluster11.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster11.ma[2]";
connectAttr "CenterHead.wm" "skinCluster11.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster11.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster11.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster11.ma[6]";
connectAttr "LeftHand.wm" "skinCluster11.ma[7]";
connectAttr "RightCollar.wm" "skinCluster11.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster11.ma[9]";
connectAttr "RightElbow.wm" "skinCluster11.ma[10]";
connectAttr "RightHand.wm" "skinCluster11.ma[11]";
connectAttr "LeftHip.wm" "skinCluster11.ma[12]";
connectAttr "LeftKnee.wm" "skinCluster11.ma[13]";
connectAttr "RightHip.wm" "skinCluster11.ma[16]";
connectAttr "RightKnee.wm" "skinCluster11.ma[17]";
connectAttr "CenterRoot.liw" "skinCluster11.lw[0]";
connectAttr "CenterHip.liw" "skinCluster11.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster11.lw[2]";
connectAttr "CenterHead.liw" "skinCluster11.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster11.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster11.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster11.lw[6]";
connectAttr "LeftHand.liw" "skinCluster11.lw[7]";
connectAttr "RightCollar.liw" "skinCluster11.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster11.lw[9]";
connectAttr "RightElbow.liw" "skinCluster11.lw[10]";
connectAttr "RightHand.liw" "skinCluster11.lw[11]";
connectAttr "LeftHip.liw" "skinCluster11.lw[12]";
connectAttr "LeftKnee.liw" "skinCluster11.lw[13]";
connectAttr "RightHip.liw" "skinCluster11.lw[16]";
connectAttr "RightKnee.liw" "skinCluster11.lw[17]";
connectAttr "CenterRoot.obcc" "skinCluster11.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster11.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster11.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster11.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster11.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster11.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster11.ifcl[6]";
connectAttr "LeftHand.obcc" "skinCluster11.ifcl[7]";
connectAttr "RightCollar.obcc" "skinCluster11.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster11.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster11.ifcl[10]";
connectAttr "RightHand.obcc" "skinCluster11.ifcl[11]";
connectAttr "LeftHip.obcc" "skinCluster11.ifcl[12]";
connectAttr "LeftKnee.obcc" "skinCluster11.ifcl[13]";
connectAttr "RightHip.obcc" "skinCluster11.ifcl[16]";
connectAttr "RightKnee.obcc" "skinCluster11.ifcl[17]";
connectAttr "bindPose1.msg" "skinCluster11.bp";
connectAttr "Cubic_TorsoShapeOrig.w" "groupParts44.ig";
connectAttr "groupId271.id" "groupParts44.gi";
connectAttr "groupParts46.og" "tweak11.ip[0].ig";
connectAttr "groupId273.id" "tweak11.ip[0].gi";
connectAttr "skinCluster11GroupId.msg" "skinCluster11Set.gn" -na;
connectAttr "Cubic_TorsoShape.iog.og[1]" "skinCluster11Set.dsm" -na;
connectAttr "skinCluster11.msg" "skinCluster11Set.ub[0]";
connectAttr "tweak11.og[0]" "skinCluster11GroupParts.ig";
connectAttr "skinCluster11GroupId.id" "skinCluster11GroupParts.gi";
connectAttr "groupId273.msg" "tweakSet11.gn" -na;
connectAttr "Cubic_TorsoShape.iog.og[2]" "tweakSet11.dsm" -na;
connectAttr "tweak11.msg" "tweakSet11.ub[0]";
connectAttr "groupParts44.og" "groupParts46.ig";
connectAttr "groupId273.id" "groupParts46.gi";
connectAttr "skinCluster12GroupParts.og" "skinCluster12.ip[0].ig";
connectAttr "skinCluster12GroupId.id" "skinCluster12.ip[0].gi";
connectAttr "CenterHip.wm" "skinCluster12.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster12.ma[2]";
connectAttr "CenterHead.wm" "skinCluster12.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster12.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster12.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster12.ma[6]";
connectAttr "LeftHand.wm" "skinCluster12.ma[7]";
connectAttr "LeftHip.wm" "skinCluster12.ma[12]";
connectAttr "LeftKnee.wm" "skinCluster12.ma[13]";
connectAttr "CenterHip.liw" "skinCluster12.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster12.lw[2]";
connectAttr "CenterHead.liw" "skinCluster12.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster12.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster12.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster12.lw[6]";
connectAttr "LeftHand.liw" "skinCluster12.lw[7]";
connectAttr "LeftHip.liw" "skinCluster12.lw[12]";
connectAttr "LeftKnee.liw" "skinCluster12.lw[13]";
connectAttr "CenterHip.obcc" "skinCluster12.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster12.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster12.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster12.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster12.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster12.ifcl[6]";
connectAttr "LeftHand.obcc" "skinCluster12.ifcl[7]";
connectAttr "LeftHip.obcc" "skinCluster12.ifcl[12]";
connectAttr "LeftKnee.obcc" "skinCluster12.ifcl[13]";
connectAttr "bindPose1.msg" "skinCluster12.bp";
connectAttr "Cubic_Left_ArmShapeOrig.w" "groupParts47.ig";
connectAttr "groupId274.id" "groupParts47.gi";
connectAttr "groupParts49.og" "tweak12.ip[0].ig";
connectAttr "groupId276.id" "tweak12.ip[0].gi";
connectAttr "skinCluster12GroupId.msg" "skinCluster12Set.gn" -na;
connectAttr "Cubic_Left_ArmShape.iog.og[1]" "skinCluster12Set.dsm" -na;
connectAttr "skinCluster12.msg" "skinCluster12Set.ub[0]";
connectAttr "tweak12.og[0]" "skinCluster12GroupParts.ig";
connectAttr "skinCluster12GroupId.id" "skinCluster12GroupParts.gi";
connectAttr "groupId276.msg" "tweakSet12.gn" -na;
connectAttr "Cubic_Left_ArmShape.iog.og[2]" "tweakSet12.dsm" -na;
connectAttr "tweak12.msg" "tweakSet12.ub[0]";
connectAttr "groupParts47.og" "groupParts49.ig";
connectAttr "groupId276.id" "groupParts49.gi";
connectAttr "skinCluster13GroupParts.og" "skinCluster13.ip[0].ig";
connectAttr "skinCluster13GroupId.id" "skinCluster13.ip[0].gi";
connectAttr "CenterHip.wm" "skinCluster13.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster13.ma[2]";
connectAttr "CenterHead.wm" "skinCluster13.ma[3]";
connectAttr "RightCollar.wm" "skinCluster13.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster13.ma[9]";
connectAttr "RightElbow.wm" "skinCluster13.ma[10]";
connectAttr "RightHand.wm" "skinCluster13.ma[11]";
connectAttr "RightHip.wm" "skinCluster13.ma[16]";
connectAttr "RightKnee.wm" "skinCluster13.ma[17]";
connectAttr "CenterHip.liw" "skinCluster13.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster13.lw[2]";
connectAttr "CenterHead.liw" "skinCluster13.lw[3]";
connectAttr "RightCollar.liw" "skinCluster13.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster13.lw[9]";
connectAttr "RightElbow.liw" "skinCluster13.lw[10]";
connectAttr "RightHand.liw" "skinCluster13.lw[11]";
connectAttr "RightHip.liw" "skinCluster13.lw[16]";
connectAttr "RightKnee.liw" "skinCluster13.lw[17]";
connectAttr "CenterHip.obcc" "skinCluster13.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster13.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster13.ifcl[3]";
connectAttr "RightCollar.obcc" "skinCluster13.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster13.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster13.ifcl[10]";
connectAttr "RightHand.obcc" "skinCluster13.ifcl[11]";
connectAttr "RightHip.obcc" "skinCluster13.ifcl[16]";
connectAttr "RightKnee.obcc" "skinCluster13.ifcl[17]";
connectAttr "bindPose1.msg" "skinCluster13.bp";
connectAttr "RightHand.msg" "skinCluster13.ptt";
connectAttr "Cubic_Right_ArmShapeOrig.w" "groupParts50.ig";
connectAttr "groupId277.id" "groupParts50.gi";
connectAttr "groupParts52.og" "tweak13.ip[0].ig";
connectAttr "groupId279.id" "tweak13.ip[0].gi";
connectAttr "skinCluster13GroupId.msg" "skinCluster13Set.gn" -na;
connectAttr "Cubic_Right_ArmShape.iog.og[1]" "skinCluster13Set.dsm" -na;
connectAttr "skinCluster13.msg" "skinCluster13Set.ub[0]";
connectAttr "tweak13.og[0]" "skinCluster13GroupParts.ig";
connectAttr "skinCluster13GroupId.id" "skinCluster13GroupParts.gi";
connectAttr "groupId279.msg" "tweakSet13.gn" -na;
connectAttr "Cubic_Right_ArmShape.iog.og[2]" "tweakSet13.dsm" -na;
connectAttr "tweak13.msg" "tweakSet13.ub[0]";
connectAttr "groupParts50.og" "groupParts52.ig";
connectAttr "groupId279.id" "groupParts52.gi";
connectAttr "skinCluster14GroupParts.og" "skinCluster14.ip[0].ig";
connectAttr "skinCluster14GroupId.id" "skinCluster14.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster14.ma[0]";
connectAttr "CenterHip.wm" "skinCluster14.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster14.ma[2]";
connectAttr "LeftShoulder.wm" "skinCluster14.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster14.ma[6]";
connectAttr "LeftHand.wm" "skinCluster14.ma[7]";
connectAttr "RightShoulder.wm" "skinCluster14.ma[9]";
connectAttr "RightElbow.wm" "skinCluster14.ma[10]";
connectAttr "RightHand.wm" "skinCluster14.ma[11]";
connectAttr "LeftHip.wm" "skinCluster14.ma[12]";
connectAttr "LeftKnee.wm" "skinCluster14.ma[13]";
connectAttr "LeftFoot.wm" "skinCluster14.ma[14]";
connectAttr "LeftToe.wm" "skinCluster14.ma[15]";
connectAttr "RightHip.wm" "skinCluster14.ma[16]";
connectAttr "RightKnee.wm" "skinCluster14.ma[17]";
connectAttr "RightFoot.wm" "skinCluster14.ma[18]";
connectAttr "RightToe.wm" "skinCluster14.ma[19]";
connectAttr "CenterRoot.liw" "skinCluster14.lw[0]";
connectAttr "CenterHip.liw" "skinCluster14.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster14.lw[2]";
connectAttr "LeftShoulder.liw" "skinCluster14.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster14.lw[6]";
connectAttr "LeftHand.liw" "skinCluster14.lw[7]";
connectAttr "RightShoulder.liw" "skinCluster14.lw[9]";
connectAttr "RightElbow.liw" "skinCluster14.lw[10]";
connectAttr "RightHand.liw" "skinCluster14.lw[11]";
connectAttr "LeftHip.liw" "skinCluster14.lw[12]";
connectAttr "LeftKnee.liw" "skinCluster14.lw[13]";
connectAttr "LeftFoot.liw" "skinCluster14.lw[14]";
connectAttr "LeftToe.liw" "skinCluster14.lw[15]";
connectAttr "RightHip.liw" "skinCluster14.lw[16]";
connectAttr "RightKnee.liw" "skinCluster14.lw[17]";
connectAttr "RightFoot.liw" "skinCluster14.lw[18]";
connectAttr "RightToe.liw" "skinCluster14.lw[19]";
connectAttr "CenterRoot.obcc" "skinCluster14.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster14.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster14.ifcl[2]";
connectAttr "LeftShoulder.obcc" "skinCluster14.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster14.ifcl[6]";
connectAttr "LeftHand.obcc" "skinCluster14.ifcl[7]";
connectAttr "RightShoulder.obcc" "skinCluster14.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster14.ifcl[10]";
connectAttr "RightHand.obcc" "skinCluster14.ifcl[11]";
connectAttr "LeftHip.obcc" "skinCluster14.ifcl[12]";
connectAttr "LeftKnee.obcc" "skinCluster14.ifcl[13]";
connectAttr "LeftFoot.obcc" "skinCluster14.ifcl[14]";
connectAttr "LeftToe.obcc" "skinCluster14.ifcl[15]";
connectAttr "RightHip.obcc" "skinCluster14.ifcl[16]";
connectAttr "RightKnee.obcc" "skinCluster14.ifcl[17]";
connectAttr "RightFoot.obcc" "skinCluster14.ifcl[18]";
connectAttr "RightToe.obcc" "skinCluster14.ifcl[19]";
connectAttr "bindPose1.msg" "skinCluster14.bp";
connectAttr "LeftFoot.msg" "skinCluster14.ptt";
connectAttr "Cubic_LegsShapeOrig.w" "groupParts53.ig";
connectAttr "groupId280.id" "groupParts53.gi";
connectAttr "groupParts53.og" "groupParts54.ig";
connectAttr "groupId281.id" "groupParts54.gi";
connectAttr "groupParts56.og" "tweak14.ip[0].ig";
connectAttr "groupId283.id" "tweak14.ip[0].gi";
connectAttr "skinCluster14GroupId.msg" "skinCluster14Set.gn" -na;
connectAttr "Cubic_LegsShape.iog.og[2]" "skinCluster14Set.dsm" -na;
connectAttr "skinCluster14.msg" "skinCluster14Set.ub[0]";
connectAttr "tweak14.og[0]" "skinCluster14GroupParts.ig";
connectAttr "skinCluster14GroupId.id" "skinCluster14GroupParts.gi";
connectAttr "groupId283.msg" "tweakSet14.gn" -na;
connectAttr "Cubic_LegsShape.iog.og[3]" "tweakSet14.dsm" -na;
connectAttr "tweak14.msg" "tweakSet14.ub[0]";
connectAttr "groupParts54.og" "groupParts56.ig";
connectAttr "groupId283.id" "groupParts56.gi";
connectAttr "skinCluster15GroupParts.og" "skinCluster15.ip[0].ig";
connectAttr "skinCluster15GroupId.id" "skinCluster15.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster15.ma[0]";
connectAttr "CenterHip.wm" "skinCluster15.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster15.ma[2]";
connectAttr "CenterHead.wm" "skinCluster15.ma[3]";
connectAttr "LeftCollar.wm" "skinCluster15.ma[4]";
connectAttr "LeftShoulder.wm" "skinCluster15.ma[5]";
connectAttr "LeftElbow.wm" "skinCluster15.ma[6]";
connectAttr "RightCollar.wm" "skinCluster15.ma[8]";
connectAttr "RightShoulder.wm" "skinCluster15.ma[9]";
connectAttr "RightElbow.wm" "skinCluster15.ma[10]";
connectAttr "LeftHip.wm" "skinCluster15.ma[12]";
connectAttr "RightHip.wm" "skinCluster15.ma[16]";
connectAttr "CenterRoot.liw" "skinCluster15.lw[0]";
connectAttr "CenterHip.liw" "skinCluster15.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster15.lw[2]";
connectAttr "CenterHead.liw" "skinCluster15.lw[3]";
connectAttr "LeftCollar.liw" "skinCluster15.lw[4]";
connectAttr "LeftShoulder.liw" "skinCluster15.lw[5]";
connectAttr "LeftElbow.liw" "skinCluster15.lw[6]";
connectAttr "RightCollar.liw" "skinCluster15.lw[8]";
connectAttr "RightShoulder.liw" "skinCluster15.lw[9]";
connectAttr "RightElbow.liw" "skinCluster15.lw[10]";
connectAttr "LeftHip.liw" "skinCluster15.lw[12]";
connectAttr "RightHip.liw" "skinCluster15.lw[16]";
connectAttr "CenterRoot.obcc" "skinCluster15.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster15.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster15.ifcl[2]";
connectAttr "CenterHead.obcc" "skinCluster15.ifcl[3]";
connectAttr "LeftCollar.obcc" "skinCluster15.ifcl[4]";
connectAttr "LeftShoulder.obcc" "skinCluster15.ifcl[5]";
connectAttr "LeftElbow.obcc" "skinCluster15.ifcl[6]";
connectAttr "RightCollar.obcc" "skinCluster15.ifcl[8]";
connectAttr "RightShoulder.obcc" "skinCluster15.ifcl[9]";
connectAttr "RightElbow.obcc" "skinCluster15.ifcl[10]";
connectAttr "LeftHip.obcc" "skinCluster15.ifcl[12]";
connectAttr "RightHip.obcc" "skinCluster15.ifcl[16]";
connectAttr "bindPose1.msg" "skinCluster15.bp";
connectAttr "groupParts58.og" "tweak15.ip[0].ig";
connectAttr "groupId285.id" "tweak15.ip[0].gi";
connectAttr "skinCluster15GroupId.msg" "skinCluster15Set.gn" -na;
connectAttr "Cubic_BackShape.iog.og[0]" "skinCluster15Set.dsm" -na;
connectAttr "skinCluster15.msg" "skinCluster15Set.ub[0]";
connectAttr "tweak15.og[0]" "skinCluster15GroupParts.ig";
connectAttr "skinCluster15GroupId.id" "skinCluster15GroupParts.gi";
connectAttr "groupId285.msg" "tweakSet15.gn" -na;
connectAttr "Cubic_BackShape.iog.og[1]" "tweakSet15.dsm" -na;
connectAttr "tweak15.msg" "tweakSet15.ub[0]";
connectAttr "Cubic_BackShapeOrig1.w" "groupParts58.ig";
connectAttr "groupId285.id" "groupParts58.gi";
connectAttr "polyTweak16.out" "polyNormal3.ip";
connectAttr "skinCluster9.og[0]" "polyTweak16.ip";
connectAttr "polyCube28.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polySplit9.ip";
connectAttr "skinCluster16GroupParts.og" "skinCluster16.ip[0].ig";
connectAttr "skinCluster16GroupId.id" "skinCluster16.ip[0].gi";
connectAttr "CenterRoot.wm" "skinCluster16.ma[0]";
connectAttr "CenterHip.wm" "skinCluster16.ma[1]";
connectAttr "CenterSpine.wm" "skinCluster16.ma[2]";
connectAttr "LeftHip.wm" "skinCluster16.ma[12]";
connectAttr "RightHip.wm" "skinCluster16.ma[16]";
connectAttr "CenterRoot.liw" "skinCluster16.lw[0]";
connectAttr "CenterHip.liw" "skinCluster16.lw[1]";
connectAttr "CenterSpine.liw" "skinCluster16.lw[2]";
connectAttr "LeftHip.liw" "skinCluster16.lw[12]";
connectAttr "RightHip.liw" "skinCluster16.lw[16]";
connectAttr "CenterRoot.obcc" "skinCluster16.ifcl[0]";
connectAttr "CenterHip.obcc" "skinCluster16.ifcl[1]";
connectAttr "CenterSpine.obcc" "skinCluster16.ifcl[2]";
connectAttr "LeftHip.obcc" "skinCluster16.ifcl[12]";
connectAttr "RightHip.obcc" "skinCluster16.ifcl[16]";
connectAttr "bindPose1.msg" "skinCluster16.bp";
connectAttr "groupParts60.og" "tweak16.ip[0].ig";
connectAttr "groupId287.id" "tweak16.ip[0].gi";
connectAttr "skinCluster16GroupId.msg" "skinCluster16Set.gn" -na;
connectAttr "DummyShape.iog.og[3]" "skinCluster16Set.dsm" -na;
connectAttr "skinCluster16.msg" "skinCluster16Set.ub[0]";
connectAttr "tweak16.og[0]" "skinCluster16GroupParts.ig";
connectAttr "skinCluster16GroupId.id" "skinCluster16GroupParts.gi";
connectAttr "groupId287.msg" "tweakSet16.gn" -na;
connectAttr "DummyShape.iog.og[4]" "tweakSet16.dsm" -na;
connectAttr "tweak16.msg" "tweakSet16.ub[0]";
connectAttr "DummyShapeOrig.w" "groupParts60.ig";
connectAttr "groupId287.id" "groupParts60.gi";
connectAttr "pSphere1SG.pa" ":renderPartition.st" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "hipsShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "upperLegLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "lowerLegLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "kneeLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "upperLegRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "lowerLegRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "kneeRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "panelLeftShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "panelRightShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "panelLeftShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "panelRightShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "baseShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontLeftShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontRightShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontLeftShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "jawMidShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethmidShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "leftSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "rightSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "backSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "leftNozzleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "rightNozzleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft2|pCube7|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "frontLeftShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCubeShape7.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube8|pCubeShape8.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube11|pCubeShape11.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube10|pCubeShape10.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube9|pCubeShape9.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube15|pCubeShape15.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube14|pCubeShape14.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube13|pCubeShape13.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|BasicMechGrouped|Basic_Torso|frontSide|frontLeft1|frontLeft3|pCube7|pCube12|pCubeShape12.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "armRightShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "armRightShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "upperArmRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "lowerArmRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "armLeftShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "upperArmLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "lowerArmLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "armLeftShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "Basic_BackShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pPlaneShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "Basic_LegsShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Cubic_Left_Arm_oldShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Basic_TorsoShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Drill_Right_ArmShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Gun_Right_ArmShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Basic_Right_ArmShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Basic_Left_ArmShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Cubic_BackShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "Falcon_BackShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "Cubic_HeadShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Cubic_TorsoShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Cubic_Left_ArmShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Cubic_Right_ArmShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "Cubic_LegsShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "DummyShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "groupId210.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId213.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId216.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId219.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId224.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId227.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId230.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId267.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId271.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId274.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId277.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId280.msg" ":initialShadingGroup.gn" -na;
connectAttr "pCubeShape20HiddenFacesSet.msg" ":defaultHideFaceDataSet.dnsm" -na;
connectAttr "pCubeShape20HiddenFacesSet1.msg" ":defaultHideFaceDataSet.dnsm" -na
		;
connectAttr "groupId281.msg" ":defaultLastHiddenSet.gn" -na;
connectAttr "Cubic_LegsShape.iog.og[1]" ":defaultLastHiddenSet.dsm" -na;
// End of Basic_Mecha.0025.ma
