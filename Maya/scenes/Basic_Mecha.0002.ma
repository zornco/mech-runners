//Maya ASCII 2017ff05 scene
//Name: Basic_Mecha.0002.ma
//Last modified: Fri, Feb 02, 2018 06:11:08 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "57F02CE5-4F31-A6EC-5D05-8897BB4CC603";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.23324524346539266 5.8304826396840674 7.4219839897799247 ;
	setAttr ".r" -type "double3" -38.138352730307126 -361.80000000025507 7.9553121699875523e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "8DA41C8D-4BCD-E8D6-9D20-B899EBCA6DC4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 9.4411216229749968;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "DEFBBE81-4D59-C1DB-60E7-FC96822C4ACA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BEC4DFEB-4727-263C-1835-5A976D6A6F5E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 25.971735514366536;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "90496FA8-4FA9-6A33-DE77-0AAC0D1B9DAD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "D197ED46-4D10-44A6-2CAA-A896A1A3D1A8";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 76.515081794942603;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "EE308D80-4811-9364-4FA8-00B92704BA63";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.2584937751254 -17.685995180364522 2.0615391731264463 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "5518C0C6-48E8-F804-B673-848FBDEA0A7C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.2584937751252;
	setAttr ".ow" 62.262485447458786;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 8.8817841970012523e-016 -17.685995180364522 2.0615391731262243 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "rootLegsJnt";
	rename -uid "404F0E79-428F-F754-F6EA-0ABC3B751C23";
	setAttr ".t" -type "double3" 0 -22 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 1.5865543914200564;
createNode joint -n "hipsJnt" -p "rootLegsJnt";
	rename -uid "F222B8CA-46BD-39D1-B6EF-BFB5F8060C79";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 4.9303806576313227e-032 2.2204460492503131e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
createNode joint -n "upperLegLeftJnt" -p "hipsJnt";
	rename -uid "292AC345-4BCF-8FC5-27D3-DCBBBE5F9382";
	setAttr ".t" -type "double3" 7.9999999999999982 8.8817841970012523e-016 1.9721522630525291e-031 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 -68.198590513648213 -89.999999999999972 ;
createNode joint -n "lowerLegLeftJnt" -p "upperLegLeftJnt";
	rename -uid "0E8B7813-4C3D-3D46-B1FF-44AE78FE8FE1";
	setAttr ".t" -type "double3" 5.385164807134502 -2.2463410722767539e-015 1.5378418617811802e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 21.801409486352004 ;
createNode joint -n "footLeftJnt" -p "lowerLegLeftJnt";
	rename -uid "0B3D773A-45B5-A820-0FAD-22A43471D445";
	setAttr ".t" -type "double3" 4.0000000000000027 4.4408920985005631e-016 7.460698725481052e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 68.198590513648085 ;
createNode joint -n "joint1" -p "footLeftJnt";
	rename -uid "E2F77DF6-499F-3965-E3CA-F3A215E21165";
	setAttr ".t" -type "double3" 5.3851648071344904 2.2204460492503131e-015 -7.7715611723760958e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 21.801409486351922 -90 0 ;
createNode joint -n "upperLegRightJnt" -p "hipsJnt";
	rename -uid "FDCED3EE-4275-E132-3983-19A04AE62FE4";
	setAttr ".t" -type "double3" -7.9999999999999982 -8.8817841970012523e-016 -1.9721522630525291e-031 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000043 -68.198590513648227 -90.000000000000071 ;
createNode joint -n "lowerLegRightJnt" -p "upperLegRightJnt";
	rename -uid "A0CA8E94-4496-707A-ACDF-8093C37D5E34";
	setAttr ".t" -type "double3" 5.3851648071345037 -7.3439423352200738e-016 -4.4408920985006222e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 21.801409486351794 ;
createNode joint -n "footRightJnt" -p "lowerLegRightJnt";
	rename -uid "A251C16D-4684-41C3-943D-26AE5B13DD4F";
	setAttr ".t" -type "double3" 3.9999999999999973 1.1102230246251565e-015 -4.3520742565306136e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 68.198590513648156 ;
createNode joint -n "joint2" -p "footRightJnt";
	rename -uid "6F3D4236-40B6-DEAC-4BF9-6284F0ABB3BB";
	setAttr ".t" -type "double3" 5.3851648071345037 2.2204460492503131e-016 5.340172748447003e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -21.801409486351837 89.999999999999972 0 ;
createNode transform -n "Basic_Torso";
	rename -uid "38150AC2-4E38-3106-AF69-2AB392F4167C";
	setAttr ".rp" -type "double3" 0 5 -1 ;
	setAttr ".sp" -type "double3" 0 5 -1 ;
createNode transform -n "base" -p "Basic_Torso";
	rename -uid "5687955E-4776-20EC-B431-EB86A683BF5F";
	setAttr ".t" -type "double3" 0 -17 -1 ;
createNode mesh -n "baseShape" -p "base";
	rename -uid "B20F1ADE-4BB5-52FE-2AD2-6FB2C764C8AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "backSide" -p "Basic_Torso";
	rename -uid "D459BBFE-443F-93C5-3C8E-498F2B118BF5";
	setAttr ".t" -type "double3" 0 -7.5 -11.5 ;
createNode mesh -n "backSideShape" -p "backSide";
	rename -uid "8C90774E-404D-A119-5C13-11B5F4956669";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.375 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -1 0 0 1 0 0 -1 0 0 1 0 0;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightSide" -p "Basic_Torso";
	rename -uid "FE9902B7-4B05-B77A-9454-A09CCB9ADCF3";
	setAttr ".s" -type "double3" -1 1 1 ;
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "rightSideShape" -p "rightSide";
	rename -uid "E0DE3407-41EB-E6C2-CEAD-018F87BE1007";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  13 -15 8 13 -15 -10 13 0 8 13 0 -10 10 0 8
		 10 0 -10 10 -15 8 10 -15 -10;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "rightNozzle" -p "rightSide";
	rename -uid "6E3AAC7B-4D89-5CFF-9166-978B87C5BE70";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "rightNozzleShape" -p "rightNozzle";
	rename -uid "EA8B82A3-45D0-60B5-8430-1C8E0660895D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  11.10254955 -15.94851685 -2.5 12.99986076 -16.58113861 -2.5
		 12.051483154 -13.10255051 -2.5 13.94879436 -13.73517227 -2.5 12.051483154 -13.10255051 -5.5
		 13.94879436 -13.73517227 -5.5 11.10254955 -15.94851685 -5.5 12.99986076 -16.58113861 -5.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftSide" -p "Basic_Torso";
	rename -uid "06C37FDC-4EEB-D5E2-F652-A48C6BCF8552";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 0 1.7763568394002505e-015 ;
createNode mesh -n "leftSideShape" -p "leftSide";
	rename -uid "1290F97F-4AEF-7373-D2B6-EFA594447191";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "leftNozzle" -p "leftSide";
	rename -uid "9E258720-4205-43CC-E9AA-B6ACF03285C0";
	setAttr ".rp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 
		0 ;
	setAttr ".sp" -type "double3" -1.7763568394002505e-015 3.5527136788005009e-015 0 ;
createNode mesh -n "leftNozzleShape" -p "leftNozzle";
	rename -uid "756ED554-4D39-2719-C8E7-15A011104AA8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontSide" -p "Basic_Torso";
	rename -uid "4EF332D3-40DD-D9F6-22A4-E29160E69659";
	setAttr ".t" -type "double3" 0 -1.5 11.5 ;
createNode mesh -n "frontSideShape" -p "frontSide";
	rename -uid "240FBBC4-4A64-C047-B4B0-A49AA879F1E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft1" -p "frontSide";
	rename -uid "DA5A97AE-4AB6-D808-737D-48A54CC76B56";
	setAttr ".t" -type "double3" 0.99999999999999978 1.5 1.5 ;
	setAttr ".r" -type "double3" 0 22.62 0 ;
createNode mesh -n "frontLeftShape1" -p "frontLeft1";
	rename -uid "8981AC15-446A-F817-6A7C-2BA9D2BE092E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  6.5 -1.5 -1.5 6.5 -1.5 -1.5 
		6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5 6.5 -1.5 -1.5;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontLeft2" -p "frontLeft1";
	rename -uid "EC447CBF-4B7A-68CE-5D43-3387389E5D50";
	setAttr ".t" -type "double3" 4.076952268705468 0 -12.384605774860068 ;
	setAttr ".r" -type "double3" 0 -22.620000000000015 0 ;
	setAttr ".rp" -type "double3" -4.4408920985006262e-015 0 0 ;
	setAttr ".rpt" -type "double3" 3.4161111052404769e-016 0 -1.7080450849257616e-015 ;
	setAttr ".sp" -type "double3" -4.4408920985006262e-015 0 0 ;
createNode mesh -n "frontLeftShape2" -p "frontLeft2";
	rename -uid "C4FBF2AC-4AEF-1CD1-8BE8-D1AA68F5582A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontRight1" -p "frontSide";
	rename -uid "4597C8EE-4951-8DB2-268E-CF8D4274CC89";
	setAttr ".t" -type "double3" -6.4230679510349358 0 -2.3846278917078987 ;
	setAttr ".r" -type "double3" 0 -22.620000000000015 0 ;
	setAttr ".rp" -type "double3" 6.5000000642046114 1.5 1.4999996746390591 ;
	setAttr ".rpt" -type "double3" -1.0769321131696776 0 2.3846282170688422 ;
	setAttr ".sp" -type "double3" 6.5000000642046114 1.5 1.4999996746390591 ;
createNode mesh -n "frontRightShape1" -p "frontRight1";
	rename -uid "65BFBE3B-40CA-3AE2-BC95-7FA64C9617C9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -6.5 -1.5 1.5 6.5 -1.5 1.5 -6.5 1.5 1.5 6.5 1.5 1.5
		 -6.5 1.5 -1.5 6.5 1.5 -1.5 -6.5 -1.5 -1.5 6.5 -1.5 -1.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "frontRight2" -p "frontRight1";
	rename -uid "1AFC761A-47A6-9152-9A65-908E48009E96";
	setAttr ".t" -type "double3" 2.4230477954991341 1.5 -10.884606100221008 ;
	setAttr ".r" -type "double3" 0 202.62 0 ;
	setAttr ".s" -type "double3" 1 1 -1 ;
	setAttr ".rp" -type "double3" -4.4408920985006262e-015 0 0 ;
	setAttr ".rpt" -type "double3" 8.5401730864772134e-015 0 -1.7080450849257608e-015 ;
	setAttr ".sp" -type "double3" -4.4408920985006262e-015 0 0 ;
createNode mesh -n "frontRightShape2" -p "frontRight2";
	rename -uid "C11B5542-4869-79C9-43CA-5481CFD5A579";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  0.076158881 -13.017788887 13.38278103 12.99922371 -13.017788887 7.99813509
		 0.38461804 -3.049999952 14.12307835 13.30768299 -3.049999952 8.73843193 -0.38213933 -2.88960218 12.28287315
		 12.54092503 -2.88960218 6.89822674 -0.69059849 -12.85739136 11.54257584 12.23246574 -12.85739136 6.1579299;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "jawMid" -p "frontSide";
	rename -uid "89DD58D2-4EBA-FEA5-4620-779AD1CDDDB9";
	setAttr ".t" -type "double3" 0 -17.2 1.2 ;
	setAttr ".r" -type "double3" 9 0 0 ;
createNode mesh -n "jawMidShape" -p "jawMid";
	rename -uid "BBF1F4BB-4B86-E8BB-08F6-36B6004C7BDD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 -2.5 -1.5 0 -2.5 -1.5 0 
		-2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5 0 -2.5 -1.5;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube6" -p "jawMid";
	rename -uid "244AC02B-40FF-1E35-DA16-9E87AD3FC4B0";
	setAttr ".r" -type "double3" 9 180 0 ;
	setAttr ".s" -type "double3" 1 1 -1 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "2D86932E-400D-DFAB-892B-19933F24CE01";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -15.15206146 1.20867193 -5.36155796 -3.22309065 -4.54291534 -0.82075399
		 -13.22897339 5.76722908 -4.63955355 -1.30000269 0.01564152 -0.098749459 -12.16387939 5.76210356 -7.4441123
		 -0.23490894 0.01051604 -2.90330839 -14.086967468 1.20354652 -8.16611671 -2.15799665 -4.54804087 -3.62531281;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube5" -p "jawMid";
	rename -uid "5C15DB6A-40CF-A28D-8F27-77A9656A0CDB";
	setAttr ".r" -type "double3" -9 0 0 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "39E10765-44E7-234D-22B2-F2BA48648DB0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethmid" -p "frontSide";
	rename -uid "298B6792-4E41-C0F5-614B-86BFC82313CE";
	setAttr ".t" -type "double3" 0 -10.999999999999998 -0.5 ;
createNode mesh -n "teethmidShape" -p "teethmid";
	rename -uid "FA1D085F-4E2E-5401-EC68-34BB66F7FDCB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 -3.5 0 0 -3.5 0 0 -3.5 
		0 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0 0 -3.5 0;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethLeft" -p "teethmid";
	rename -uid "BDFBD9ED-489C-8419-DE9B-FE899DE6FE67";
	setAttr ".t" -type "double3" 0 12.499999999999998 -11 ;
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethLeftShape" -p "teethLeft";
	rename -uid "85E2D2E6-4D90-2E60-D420-6A86E5506DE2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 3 ".pt";
	setAttr ".pt[1]" -type "float3" -1.4901161e-008 2.3436136 0 ;
	setAttr ".pt[3]" -type "float3" -1.4901161e-008 0 0 ;
	setAttr ".pt[7]" -type "float3" 0 2.3436136 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "teethRight" -p "teethmid";
	rename -uid "0BBCA9C6-4D6B-0074-7A2F-C0A378F8E720";
	setAttr ".t" -type "double3" 0 12.499999999999998 -11 ;
	setAttr ".s" -type "double3" -1 1 1 ;
	setAttr ".rp" -type "double3" 0 -12.499999999999998 11.5 ;
	setAttr ".sp" -type "double3" 0 -12.499999999999998 11.5 ;
createNode mesh -n "teethRightShape" -p "teethRight";
	rename -uid "2DC87D9D-46ED-BB5C-9CC4-77A69DB4A2E3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.625 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 3 ".pt";
	setAttr ".pt[1]" -type "float3" -1.4901161e-008 2.3436136 0 ;
	setAttr ".pt[3]" -type "float3" -1.4901161e-008 0 0 ;
	setAttr ".pt[7]" -type "float3" 0 2.3436136 0 ;
	setAttr -s 8 ".vt[0:7]"  0.5 -19.5 11.5 12.99640179 -19.5 7.91671467
		 0.5 -12.5 11.5 12.99640179 -12.5 7.91671467 0.22436264 -12.5 10.53873825 12.72076416 -12.5 6.95545292
		 0.22436264 -19.5 10.53873825 12.72076416 -19.5 6.95545292;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode joint -n "rootTorsoJnt";
	rename -uid "194C43E1-4939-2639-4A3A-7BA5CE3D0C71";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
createNode transform -n "Basic_Legs";
	rename -uid "0E955BC1-4964-2BAE-1301-8C89317036A7";
	setAttr ".rp" -type "double3" 0 -25.5 -0.26776695251464844 ;
	setAttr ".sp" -type "double3" 0 -25.5 -0.26776695251464844 ;
createNode transform -n "hips" -p "Basic_Legs";
	rename -uid "FF221B08-4D2C-B8C4-D3E0-6A9FB22B3148";
	setAttr ".rp" -type "double3" 0 -22 0 ;
	setAttr ".sp" -type "double3" 0 -22 0 ;
createNode mesh -n "hipsShape" -p "hips";
	rename -uid "77109185-401B-E0D8-99D4-9E8E8A364F90";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegLeft" -p "hips";
	rename -uid "42BBF380-45E3-7FA5-B896-15BC05AED86A";
	setAttr ".rp" -type "double3" 8 -22 0 ;
	setAttr ".sp" -type "double3" 8 -22 0 ;
createNode mesh -n "upperLegLeftShape" -p "upperLegLeft";
	rename -uid "5EA57348-475E-14C4-5813-288368A89BE5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegLeft" -p "upperLegLeft";
	rename -uid "228A07EC-48C7-D1B5-C59A-CA93721A23C5";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "lowerLegLeftShape" -p "lowerLegLeft";
	rename -uid "2F94FD93-495B-848A-C8EC-43804F051E67";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeLeft" -p "upperLegLeft";
	rename -uid "8C34651A-4401-EFE8-7775-988B897B7665";
	setAttr ".rp" -type "double3" 8 -27 -2 ;
	setAttr ".sp" -type "double3" 8 -27 -2 ;
createNode mesh -n "kneeLeftShape" -p "kneeLeft";
	rename -uid "DC8270F5-4118-DFD7-DFA5-80B685231A9C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "upperLegRight" -p "hips";
	rename -uid "9B70C2EC-4F64-7511-B019-008BE4895002";
	setAttr ".rp" -type "double3" -8 -22 0 ;
	setAttr ".sp" -type "double3" -8 -22 0 ;
createNode mesh -n "upperLegRightShape" -p "upperLegRight";
	rename -uid "5F05A66E-43B5-DCB9-ACE4-44B565BA7A15";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -10 -22 0 -22 -22 0 -10 -22 
		0 -22 -22 0 -10 -22 0 -22 -22 0 -10 -22 0 -22 -22 0;
	setAttr -s 8 ".vt[0:7]"  5 -5 3 11 -5 3 5 3 3 11 3 3 5 3 -3 11 3 -3
		 5 -5 -3 11 -5 -3;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "lowerLegRight" -p "upperLegRight";
	rename -uid "F8F9085D-4AAD-48B5-138A-9A97137A60CF";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "lowerLegRightShape" -p "lowerLegRight";
	rename -uid "CFEB7661-434F-3C86-89D4-ABBEC9BFE28F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -9 -22 0 -23 -22 0 -9 -22 
		0 -23 -22 0 -9 -22 0 -23 -22 0 -9 -22 0 -23 -22 0;
	setAttr -s 8 ".vt[0:7]"  4.5 -11 3 11.5 -11 3 4.5 -5 3 11.5 -5 3 4.5 -5 -4
		 11.5 -5 -4 4.5 -11 -4 11.5 -11 -4;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "kneeRight" -p "upperLegRight";
	rename -uid "8CE62129-4288-F4FE-0934-6FBF0BAFC905";
	setAttr ".rp" -type "double3" -8 -27 -2 ;
	setAttr ".sp" -type "double3" -8 -27 -2 ;
createNode mesh -n "kneeRightShape" -p "kneeRight";
	rename -uid "D4AE6C18-46FD-A0C5-74F7-55B272B7B015";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8 -22 0 -24 -22 0 -8 -22 
		0 -24 -22 0 -8 -22 0 -24 -22 0 -8 -22 0 -24 -22 0;
	setAttr -s 8 ".vt[0:7]"  4 -8.53553391 -2 12 -8.53553391 -2 4 -5 1.53553391
		 12 -5 1.53553391 4 -1.46446609 -2 12 -1.46446609 -2 4 -5 -5.53553391 12 -5 -5.53553391;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 4 1 -6 -1
		mu 0 4 0 2 3 1
		f 4 6 2 -8 -2
		mu 0 4 2 4 5 3
		f 4 8 3 -10 -3
		mu 0 4 4 6 7 5
		f 4 10 0 -12 -4
		mu 0 4 6 8 9 7
		f 4 5 7 9 11
		mu 0 4 1 3 11 10
		f 4 -9 -7 -5 -11
		mu 0 4 12 13 2 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft1" -p "hips";
	rename -uid "06472B54-43CE-28A3-B943-D9BDAB2EA2FA";
	setAttr ".t" -type "double3" 0 0 -1 ;
	setAttr ".rp" -type "double3" 12 -20 0 ;
	setAttr ".sp" -type "double3" 12 -20 0 ;
createNode mesh -n "panelLeftShape1" -p "panelLeft1";
	rename -uid "DBFD30E4-4851-2760-62DE-C197E02BF610";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelLeft2" -p "panelLeft1";
	rename -uid "E32EE436-4527-A180-E176-B59AE16172E1";
	setAttr ".rp" -type "double3" 11.5 -16.5 4.9999999999999964 ;
	setAttr ".sp" -type "double3" 11.5 -16.5 4.9999999999999964 ;
createNode mesh -n "panelLeftShape2" -p "panelLeft2";
	rename -uid "8B9BCF4D-47FC-F698-98BB-6DACEA0D097E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight1" -p "hips";
	rename -uid "F68B51CC-4D47-0E73-FF16-88A6A326CC32";
	setAttr ".t" -type "double3" 0 0 -1 ;
	setAttr ".rp" -type "double3" -12.000000000000004 -20 3.944304526105059e-031 ;
	setAttr ".sp" -type "double3" -12.000000000000004 -20 3.944304526105059e-031 ;
createNode mesh -n "panelRightShape1" -p "panelRight1";
	rename -uid "94705DCE-447C-DE62-7C01-7987E04AA3E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -7.462697 -20.005127 4.5 
		-17.462698 -20.005127 -5.5 -5.5743756 -20.264633 4.5 -15.574375 -20.264633 -5.5 -6.537303 
		-19.994873 5.5 -16.537302 -19.994873 -4.5 -8.4256248 -19.735367 5.5 -18.425625 -19.735367 
		-4.5;
	setAttr -s 8 ".vt[0:7]"  -5 -3.5 0.5 5 -3.5 0.5 -5 3.5 0.5 5 3.5 0.5
		 -5 3.5 -0.5 5 3.5 -0.5 -5 -3.5 -0.5 5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "panelRight2" -p "panelRight1";
	rename -uid "83C52A54-4F1F-904E-B235-1FA276B726FB";
	setAttr ".rp" -type "double3" -11.499999999999998 -16.5 5.0000000000000018 ;
	setAttr ".sp" -type "double3" -11.499999999999998 -16.5 5.0000000000000018 ;
createNode mesh -n "panelRightShape2" -p "panelRight2";
	rename -uid "3E1D1CC0-4F0D-A0E9-D73E-D4A0EA4779E2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -8.8865137 -19.740492 6.1672893 
		-9.7058802 -19.740492 9.4535904 -8 -20 4.5 -8.8193665 -20 7.7863011 -7.5479331 -20.26976 
		4.6497855 -8.3673 -20.26976 7.9360862 -8.4344463 -20.010252 6.3170743 -9.2538128 
		-20.010252 9.6033754;
	setAttr -s 8 ".vt[0:7]"  -3.5 -3.5 0.5 3.5 -3.5 0.5 -3.5 3.5 0.5 3.5 3.5 0.5
		 -3.5 3.5 -0.5 3.5 3.5 -0.5 -3.5 -3.5 -0.5 3.5 -3.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7";
	rename -uid "8F4E599B-471A-D845-EC15-50AA27EF9227";
createNode mesh -n "pCubeShape7" -p "pCube7";
	rename -uid "F23315E2-4483-29C2-F6AB-4CA8057855AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8" -p "pCube7";
	rename -uid "01186FCA-494E-584F-5C95-228E0CF99493";
	setAttr ".t" -type "double3" -3.5 -1.5 0 ;
	setAttr ".rp" -type "double3" -0.5 1 1 ;
	setAttr ".sp" -type "double3" -0.5 1 1 ;
createNode mesh -n "pCubeShape8" -p "pCube8";
	rename -uid "D96C6124-421A-9B22-8816-5CB8F1492E86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9";
	rename -uid "C5F9ED8E-4ADB-B6FE-0679-38816DDCE302";
	setAttr ".t" -type "double3" 3.5 -2.5 0 ;
	setAttr ".rp" -type "double3" 0.5 2 1 ;
	setAttr ".sp" -type "double3" 0.5 2 1 ;
createNode mesh -n "pCubeShape9" -p "pCube9";
	rename -uid "A6E870DD-4579-55CB-8072-7CB118EF453B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "37602909-428B-3375-0327-C0A8E1C6446B";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F595F706-4BE4-BB47-9CB0-D4902F5F1BED";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "E17A4C4F-4C43-D526-9699-3C823EB0C191";
createNode displayLayerManager -n "layerManager";
	rename -uid "A60411A8-4336-2154-8368-72BE843B39BB";
createNode displayLayer -n "defaultLayer";
	rename -uid "4085AE31-4CEA-0975-1FB7-E181F8E20DAE";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "67CAC659-452B-C273-57E3-6B807564F678";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "1D0F1E37-4A8A-A123-D6A9-679337D5783E";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "5CB01404-49E4-D27F-80CC-5590DD35CDFF";
	setAttr ".w" 10;
	setAttr ".h" 8;
	setAttr ".d" 10;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube2";
	rename -uid "80215462-4720-5811-BDBF-3CABF39A8D0D";
	setAttr ".w" 6;
	setAttr ".h" 8;
	setAttr ".d" 6;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube3";
	rename -uid "485774BA-41D5-6B47-1A0B-2D9BCAEA4729";
	setAttr ".w" 7;
	setAttr ".h" 6;
	setAttr ".d" 7;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "54C76ABA-4E87-6A38-5E51-09801C8F20AE";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -1 0 1;
createNode polyCube -n "polyCube4";
	rename -uid "8D8EA23B-4BD4-C7A9-6418-8BB72A958F8A";
	setAttr ".w" 8;
	setAttr ".h" 5;
	setAttr ".d" 5;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry2";
	rename -uid "2667C77D-4AE1-F49D-EB9A-B89717893460";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 0 0 1;
createNode transformGeometry -n "transformGeometry3";
	rename -uid "CBBB4418-47D3-B1D5-C7F1-B88061D1E0DC";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 8 -8 -0.5 1;
createNode transformGeometry -n "transformGeometry4";
	rename -uid "1581101F-4397-622B-69AB-43A374D0BF73";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.70710678118654746 0.70710678118654768 0
		 0 -0.70710678118654768 0.70710678118654746 0 8 -5 -2 1;
createNode polyCube -n "polyCube5";
	rename -uid "254C7E88-43BD-5F96-FE35-E5BABE340230";
	setAttr ".w" 10;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube6";
	rename -uid "F9C7DB55-488F-B8D7-CF89-0D81FA44309D";
	setAttr ".w" 7;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "3E1FE795-4A57-0096-2DC6-70B3AE2BE52A";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 455\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 454\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 455\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 916\n            -height 710\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n"
		+ "            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n"
		+ "                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n"
		+ "\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 916\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 916\\n    -height 710\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D359CC2C-4330-3F25-4DC2-51AEBFAC6971";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube7";
	rename -uid "559D9D51-47BD-434F-417F-9FAC6505AF5B";
	setAttr ".w" 20;
	setAttr ".h" 4;
	setAttr ".d" 18;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry5";
	rename -uid "E9E077C0-4E29-FA31-7748-D19AC1391C4C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry6";
	rename -uid "EC9EFFAE-46F3-4480-4BB4-D68D307AACDF";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry7";
	rename -uid "E2F4CE49-4274-17CB-7130-C7AE84E9341C";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry8";
	rename -uid "6631E2D1-4860-AEF4-DB1C-7098D054CA4A";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -22 0 1;
createNode transformGeometry -n "transformGeometry9";
	rename -uid "1163723F-4DD1-B842-E152-5CAC6D624D8C";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 -2.7755575615628914e-017 1 0
		 -0.26976023601264199 0.96292752326766717 5.5511151231257827e-017 0 -0.96292752326766728 -0.26976023601264193 1.1102230246251565e-016 0
		 12 -20 0 1;
createNode transformGeometry -n "transformGeometry10";
	rename -uid "36814056-4C2E-DE67-205C-B98EC7CE1DB7";
	setAttr ".txf" -type "matrix" 0.88294759285892688 -1.3877787807814457e-016 -0.46947156278589086 0
		 -0.1266447595783457 0.96292752326766717 -0.23818415103641816 0 0.45206708919801941 0.26976023601264182 0.85021453876679487 0
		 8.6269065389189556 -20.005126449443157 7.0516877289946809 1;
createNode polyCube -n "polyCube8";
	rename -uid "6738B210-4080-16FE-2F51-0AA44FB0D2B0";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube9";
	rename -uid "C40BE38B-4975-7B08-D713-59897C1E67F0";
	setAttr ".w" 13;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube10";
	rename -uid "398A8CF3-423D-51BB-DFD7-16A4761E016E";
	setAttr ".w" 14;
	setAttr ".h" 10;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak1";
	rename -uid "C313D164-4FA5-DC5A-DFFE-60BC6EF213E9";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  7 -5 -1 7 -5 -1 7 -5 -1 7
		 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1 7 -5 -1;
createNode transformGeometry -n "transformGeometry11";
	rename -uid "1431F1F0-4C93-7C38-38B9-92BD65356955";
	setAttr ".txf" -type "matrix" 1 0 0 0 0 0.99677887845624713 0.080198924328858931 0
		 0 -0.080198924328858931 0.99677887845624713 0 -1 -3.0499999999999949 0.79999999999998117 1;
createNode transformGeometry -n "transformGeometry12";
	rename -uid "60349C85-4613-42DE-5B69-56B668AF13BA";
	setAttr ".txf" -type "matrix" 0.92307601649691418 0 -0.38461756040018336 0 0 1 0 0
		 0.38461756040018336 0 0.92307601649691418 0 0.99999999999999778 0 13 1;
createNode polyCube -n "polyCube11";
	rename -uid "ED30530E-4D51-D1A7-9F47-92AD1D4F4E58";
	setAttr ".w" 7;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube12";
	rename -uid "9A96DE03-43F3-3B18-9C72-418ED6F3B382";
	setAttr ".w" 14;
	setAttr ".h" 5;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak2";
	rename -uid "33334318-4531-A1A4-0802-7CACC4E28DA7";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093 -6.99999523 -2.5 -1.49998093
		 -6.99999523 -2.5 -1.49998093;
createNode transformGeometry -n "transformGeometry13";
	rename -uid "B0B2AE7A-4047-F186-D1F6-F599466439B3";
	setAttr ".txf" -type "matrix" 0.85206933223181147 -0.41082766406220717 0.32434315702851696 0
		 0.38461756040018319 0.91171141897700703 0.14440090283216203 0 -0.35503124552896226 0.0017084929389307166 0.93485287385236782 0
		 -1.3 0.015643446504023089 -0.098768834059513783 1;
createNode polyCube -n "polyCube13";
	rename -uid "9563A64B-43D0-45BF-39B9-E6A5C9C06430";
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube14";
	rename -uid "EF19D4A1-4F7E-B8CD-8282-388E4678AAE7";
	setAttr ".w" 13;
	setAttr ".h" 7;
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak3";
	rename -uid "6D576173-45BA-8565-1798-84B6B017B978";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  6.5 -3.5 -0.5 6.5 -3.5 -0.5
		 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5 6.5 -3.5 -0.5;
createNode transformGeometry -n "transformGeometry14";
	rename -uid "73F384E1-440B-A631-EE57-2D97854B1320";
	setAttr ".txf" -type "matrix" 0.96126169593831889 0 -0.27563735581699905 0 0 1 0 0
		 0.27563735581699905 0 0.96126169593831889 0 0.5 -12.499999999999998 11.5 1;
createNode polyCube -n "polyCube15";
	rename -uid "1888269C-410C-44B7-4915-4ABB8EDA545D";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry15";
	rename -uid "87CD74CE-4C81-39F2-7622-37ABB979F889";
	setAttr ".txf" -type "matrix" 2.2204460492503131e-016 0 -1 0 0 1 0 0 1 0 2.2204460492503131e-016 0
		 11.499999999999979 -7.5 -0.99999999999999012 1;
createNode polyCube -n "polyCube16";
	rename -uid "6EE74D4C-4E8E-AA70-C5C7-5C8BC768DB57";
	setAttr ".w" 18;
	setAttr ".h" 15;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube17";
	rename -uid "8878502D-46DA-DDA8-1039-B7A7CD249DD4";
	setAttr ".w" 2;
	setAttr ".h" 3;
	setAttr ".d" 3;
	setAttr ".cuv" 4;
createNode transformGeometry -n "transformGeometry16";
	rename -uid "4C0842CC-4A13-91C5-95FB-49AA6B865820";
	setAttr ".txf" -type "matrix" 0.94865541582805746 -0.31631140039539413 0 0 0.31631140039539413 0.94865541582805746 0 0
		 0 0 1 0 12.525672292085968 -14.841844299802286 -4 1;
createNode polyCube -n "polyCube18";
	rename -uid "EECFD6F9-4BD1-ACB9-F770-6C938581FF5A";
	setAttr ".w" 8;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube19";
	rename -uid "832ECCF9-41FE-590A-C000-199E07CA5835";
	setAttr ".h" 2;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube20";
	rename -uid "E381CF92-4DBB-5E80-EEE7-75B9B372EC9E";
	setAttr ".h" 4;
	setAttr ".d" 2;
	setAttr ".cuv" 4;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 31 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "rootLegsJnt.s" "hipsJnt.is";
connectAttr "hipsJnt.s" "upperLegLeftJnt.is";
connectAttr "upperLegLeftJnt.s" "lowerLegLeftJnt.is";
connectAttr "lowerLegLeftJnt.s" "footLeftJnt.is";
connectAttr "footLeftJnt.s" "joint1.is";
connectAttr "hipsJnt.s" "upperLegRightJnt.is";
connectAttr "upperLegRightJnt.s" "lowerLegRightJnt.is";
connectAttr "lowerLegRightJnt.s" "footRightJnt.is";
connectAttr "footRightJnt.s" "joint2.is";
connectAttr "polyCube7.out" "baseShape.i";
connectAttr "polyCube16.out" "backSideShape.i";
connectAttr "transformGeometry15.og" "leftSideShape.i";
connectAttr "transformGeometry16.og" "leftNozzleShape.i";
connectAttr "polyCube8.out" "frontSideShape.i";
connectAttr "polyCube9.out" "frontLeftShape1.i";
connectAttr "transformGeometry12.og" "frontLeftShape2.i";
connectAttr "polyCube11.out" "jawMidShape.i";
connectAttr "transformGeometry13.og" "pCubeShape5.i";
connectAttr "polyCube13.out" "teethmidShape.i";
connectAttr "transformGeometry14.og" "teethLeftShape.i";
connectAttr "transformGeometry5.og" "hipsShape.i";
connectAttr "transformGeometry6.og" "upperLegLeftShape.i";
connectAttr "transformGeometry7.og" "lowerLegLeftShape.i";
connectAttr "transformGeometry8.og" "kneeLeftShape.i";
connectAttr "transformGeometry9.og" "panelLeftShape1.i";
connectAttr "transformGeometry10.og" "panelLeftShape2.i";
connectAttr "polyCube18.out" "pCubeShape7.i";
connectAttr "polyCube19.out" "pCubeShape8.i";
connectAttr "polyCube20.out" "pCubeShape9.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube2.out" "transformGeometry1.ig";
connectAttr "transformGeometry1.og" "transformGeometry2.ig";
connectAttr "polyCube3.out" "transformGeometry3.ig";
connectAttr "polyCube4.out" "transformGeometry4.ig";
connectAttr "polyCube1.out" "transformGeometry5.ig";
connectAttr "transformGeometry2.og" "transformGeometry6.ig";
connectAttr "transformGeometry3.og" "transformGeometry7.ig";
connectAttr "transformGeometry4.og" "transformGeometry8.ig";
connectAttr "polyCube5.out" "transformGeometry9.ig";
connectAttr "polyCube6.out" "transformGeometry10.ig";
connectAttr "polyCube10.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "transformGeometry11.ig";
connectAttr "transformGeometry11.og" "transformGeometry12.ig";
connectAttr "polyCube12.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "transformGeometry13.ig";
connectAttr "polyCube14.out" "polyTweak3.ip";
connectAttr "polyTweak3.out" "transformGeometry14.ig";
connectAttr "polyCube15.out" "transformGeometry15.ig";
connectAttr "polyCube17.out" "transformGeometry16.ig";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "hipsShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "upperLegLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "lowerLegLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "kneeLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "upperLegRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "lowerLegRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "kneeRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "panelLeftShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "panelRightShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "panelLeftShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "panelRightShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "baseShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontLeftShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontRightShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontLeftShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "frontRightShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "jawMidShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethmidShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethLeftShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "leftSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "rightSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "backSideShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "leftNozzleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "rightNozzleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "teethRightShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape9.iog" ":initialShadingGroup.dsm" -na;
// End of Basic_Mecha.0002.ma
